<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    

<section id="widget-grid" class="">

	
	<div class="row" id="sentmsg">
	<article class="col-sm-12">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i>
					</span>
					<h2>Parent List </h2>
				</header>
				<div>
			
					<div class="widget-body">
<c:if test="${not empty lists}">
 <c:forEach var="listValue" items="${lists}">
   <div class="col-xs-12 col-sm-6 col-md-3">
		            <div class="panel panel-purple text-align-center">
		            	
		                <div class="panel-heading">
		                    <h3 class="panel-title">
		                      ${listValue[1]}</h3>
		                </div>
		                <%-- <div class="panel-body">
		                    <h3 class="panel-title">
		                        ${listValue[2]}</h3>
		                </div> --%>
		                    <div class="panel-footer text-align-center" >
		                      <small>
		                           ${listValue[2]}</small>
		                        <!-- <small>1 month FREE trial -->
		                    </div>
		                 
		                
		                
		            </div>
		        </div>
	
 	</c:forEach>
 	 <div class="col-xs-12 col-sm-6 col-md-3">
		            <div class="panel panel-purple text-align-center">
		            	
		                <div class="panel-heading">
		                    <h3 class="panel-title">
		                      Admin</h3>
		                </div>
		                <%-- <div class="panel-body">
		                    <h3 class="panel-title">
		                        ${listValue[2]}</h3>
		                </div> --%>
		                    <div class="panel-footer text-align-center" >
		                      <small>
		                          XXXXXXXXX</small>
		                        <!-- <small>1 month FREE trial -->
		                    </div>
		                 
		                
		                
		            </div>
		        </div>
	</c:if>

	</div>
	</div>
	</div>
	</article>
	</div>
	
	</section>

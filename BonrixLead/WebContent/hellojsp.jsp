<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring pagination using data tables</title>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css">
<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	 
	/* $("#example").dataTable( {
        "bProcessing": false,
        "bServerSide": false,
        
        //"sAjaxSource": "springPaginationDataTables.web",
        "sAjaxSource": "getMediaListbyUid",
        "aaData": [
                    { "title": "Engine" }
                 
                ]             
        
    } ); */
	
	   $('#example').dataTable( {
	        "processing": false,
	        "serverSide": false,
	        "ajax": "getMediaListbyUid", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	           // $('td:eq(0)', nRow).append("<button class='editBut'><img >src='img/property32.png'>+"+aData[0]+"+</button>");
	            

	            $('td:eq(5)', nRow).html("<a target='_blank' href='"+aData[5]+"'>"+aData[5]+"</a>");
	            $('td:eq(7)', nRow).html('<a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Detail</a>');
	            
	            $('td:eq(8)', nRow).html('<a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a>');
			/* 	<td><a target="_blank" href=${baseURL}/${all.tinyurl}>${all.tinyurl} </a></td>
				<td>${all.uploadedon}</td>
				<td>${all.hitcnt}</td>
				<td><a href="./views/media/TinyHitList.jsp?mid=${all.mediaid}" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Detail</td>
				<td><a href="./views/quickMessage.jsp?url=${baseURL}/${all.tinyurl}" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</td> */
	        },
	        
	        
	    } );

} );

</script>
</head>
<body>
<form:form action="" method="GET">
<h2 >Spring MVC pagination using data tables<br><br></h2>

	<table id="example" class="display" cellspacing="0" width="100%">
          <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
                <th></th>
                <th></th>
              <th></th>
            </tr>
        </thead>   
    </table>

</form:form>
</body>
</html>
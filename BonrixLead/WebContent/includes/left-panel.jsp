<!-- User info -->
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.security.Principal"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<%@page import="org.springframework.security.core.authority.SimpleGrantedAuthority"%>
<%@page import="java.util.Collection"%>
<%@page import="org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
.fa-phone {color:red;}
/* .fa-plus{color:#26b2ad;}
.fa-exchange{color:#2b8a9a;} */
.fa-refresh{color:#f8b242;}
/* .fa-file{color:#4caf50;}
.fa-users{color:#2196f3;}
.fa-user{color:#9c27b0;}
.fa-mobile{color:#2b8a9a;}
.fa-envelope{color:#e91e63;}
.fa-bar-chart{color:#b71c1c;}
.fa-tachometer{color:red;} */
</style>


 
<div class="login-info">
	<span> <!-- User image size is adjusted inside CSS, it should stay as is -->


<% 
	Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long uid = currentUser.getUserid();
%>



			<a href="javascript:void(0);" id="show-shortcut2"> 

		 <span data-localize="${pageContext.request.userPrincipal.name}">
			${pageContext.request.userPrincipal.name} </span> <!-- <i class="fa fa-angle-down"></i> -->
		</a>

	</span>
</div>


<navigation> 
	
	
	<sec:authorize access="hasRole('ROLE_ADMIN')">
	<nav:item data-view="/tables/SaveUserAdmin" title="Company Manage" />	
	</sec:authorize>

	<sec:authorize access="hasRole('ROLE_COMPANY')">
	<nav:item  data-icon="fa fa-tachometer" data-view="/tables/ManagerDashboard" title="Dashboard"  />
		<nav:group data-icon="glyphicon glyphicon-tasks" title="Lead Managment">
		<nav:item data-view="/tables/AddLead" data-icon="fa fa-plus" title="New Lead" />
		<nav:item data-view="/tables/MisscallLeads" data-icon="fa fa-phone" title="Misscall Lead" />
		<!-- <nav:item data-view="/tables/AssignLead" data-icon="fa fa-exchange" title="Assign Lead" /> -->
		<nav:item data-view="/tables/AdvanceLeadAssignDel" data-icon="fa fa-exchange" title="Advance Lead Assign" />
		<nav:item data-view="/tables/uploadCSV" data-icon="fa fa-file" title="Create Bulk Lead" />
		<nav:item data-view="/tables/AutoDialSetting" data-icon="fa fa-refresh fa-spin fa-3x fa-fw" title="Auto Dail Setting" />
		<nav:item data-view="/tables/AddCategory" data-icon="fa fa-sitemap"  title="New Category" />
		<nav:item data-view="/tables/AddLeadState" data-icon="fa fa-th"  title="New Lead State" />
		<nav:item data-view="/tables/AddSuccessState" data-icon="fa fa-flag"  title="Lead Sub State" />
		<nav:item data-view="/tables/DefaultAssign" data-icon="fa fa-plus"  title="Default Lead Assign" />
		<nav:item data-view="/tables/AdvaneLeadSearchOpreation" data-icon="fa fa-search" title="Advance Lead Search" />
		<nav:item data-view="/tables/AddCatColumns" data-icon="fa fa-search" title="Extra Fields" />
		<nav:item data-view="/tables/scheduledLead" data-icon="fa fa-clock-o" title="Scheduled Lead" />
<!--    <nav:item data-view="/tables/IVRLead" data-icon="fa fa-download" title="IVR Management" />-->		
		<% if(uid==307){	%>	 
		<nav:item data-view="/tables/GubbaAPILead" data-icon="fa fa-plus" title="Payment Tracker Lead" />
		<% }else{ } %>
		
		<% if(uid==362){	%>	 
		<nav:item data-view="/tables/IndiaMartLead" data-icon="fa fa-server" title="IndiaMart Lead" />
		<% }else{ } %>
		
		<% if(uid==400){	%>	 
		<nav:item data-view="/tables/satcopIndiaMartLead" data-icon="fa fa-server" title="IndiaMart Lead" />
		<% }else{ } %>
		<% if(uid==442){	%>	 
		<nav:item data-view="/tables/KoshiFiberIndiaMartLead" data-icon="fa fa-server" title="IndiaMart Lead" />	
		<% }else{ } %>
		</nav:group>

       <!--  <nav:group data-icon="fa fa-users" title="Customer Managment">
		<nav:item data-view="/tables/AddContact" data-icon="glyphicon glyphicon-list-alt" title="Contact Managment " />
		<nav:item data-view="/tables/AddCustomer" data-icon="glyphicon glyphicon-user" title="Customer Managment " />
		</nav:group> -->
		
		<nav:group data-icon="fa fa-user " title="Account Managment">
		<nav:item data-view="/tables/changePassword" data-icon="fa fa-key" title="Manage Password" />
		<nav:item data-view="/tables/GenerateMasterPassWord" data-icon="fa fa-unlock" title="Transaction Password" />
		</nav:group>
	
		
		<nav:group data-icon="fa fa-users" title="Telecaller Managment">
			<nav:item data-view="/tables/SaveTallyCallerAdmin" data-icon="fa fa-user-plus"  title="New Telecaller" />
			<nav:item data-view="/tables/TelecallerLastAccess" data-icon="fa fa-clock-o"  title="Live Telecaller" />
		</nav:group>
		
		<nav:group data-icon="fa fa-mobile fa-2x" title="SMS Managment">
		<nav:item data-view="/tables/SendSMS" data-icon="fa fa-pencil" title="Compose SMS" />
		<nav:item data-view="/tables/smsTemplate" data-icon="fa fa-th-large" title="New SMS Template" />
		<nav:item data-view="/tables/smsSettings" data-icon="fa fa-cog fa-spin fa-3x fa-fw" title="New SMS Settings" />
		<nav:item data-view="/tables/SentSMSLog" data-icon="fa fa-file-text-o" title="SMS Log" />
		</nav:group>
		
		<nav:group data-icon="fa fa-users" title="Whatsapp Managment">
			<nav:item data-view="/tables/WhatsAppSettings" data-icon="fa fa-user-plus"  title="Whatsapp Settings" />
			<nav:item data-view="/tables/WhatsAppTemplate" data-icon="fa fa-clock-o"  title="Whatsapp Templates" />
		</nav:group>
		
	 	<nav:group data-icon="fa fa-envelope" title="Email Managment">
		<nav:item data-view="/tables/MEmailTemplate" data-icon="fa fa-plus" title="Email Template" />
		<!-- <nav:item data-view="/tables/SendCRMEmail" data-icon="fa fa-exchange" title="Send Email" /> -->
		<nav:item data-view="/tables/EmailSetting" data-icon="fa fa-cog fa-spin fa-3x fa-fw" title="Email Setting" />
		<nav:item data-view="/tables/EmailLog" data-icon="fa fa-check-circle" title="Email Log" />
		</nav:group> 
		
		<nav:group data-icon="fa fa-bar-chart" title="Reports">
		<nav:group data-icon="glyphicon glyphicon-tasks" title="Telecaller">
		<nav:item data-view="/tables/TelecallerReport" data-icon="fa fa-plus" title="Telecaller Report" />
		<nav:item data-view="/tables/TelecallerAllTimeReport" data-icon="fa fa-exchange" title="All Time Report" />
		<nav:item data-view="/tables/TelecallerDateWiseCallReport" data-icon="fa fa-file" title="Date Wise Report" />
		</nav:group> 
		
		<nav:group data-icon="fa fa-plus" title="Telecaller Lead Report">
		<nav:item data-view="/tables/TelecallerLeadCount" data-icon="fa fa-plus" title="Telecaller Count" />
		<nav:item data-view="/tables/TelecallerCatLeadCount" data-icon="fa fa-exchange" title="Category Count" />
		</nav:group>
		
		<nav:item data-view="/tables/CRMSucessCallReport" data-icon="fa fa-check-circle" title="Success Calls Log" />
		<nav:item data-view="/tables/CRMFailCallReport" data-icon="fa fa-times-circle" title="Fail Calls Log" />
		<!-- <nav:item data-view="/tables/CategoryReport" data-icon="fa fa-sitemap" title="Category Calls Log" /> -->
		<nav:item data-view="/tables/SuccessStateReport" data-icon="fa fa-sitemap" title="Category Calls Log" />
		<!-- <nav:item data-view="/tables/FollowUpReport" data-icon="glyphicon glyphicon-headphones" title="Followup Report" />
		<nav:item data-view="/tables/TelecallerWorkReport" data-icon="glyphicon glyphicon-pencil" title="Telecaller WorkReport" /> -->
		<nav:item data-view="/tables/AudioFileList" data-icon="glyphicon glyphicon-pencil" title="Audio Files" />
		<nav:item data-view="/tables/TeleLeadCount" data-icon="glyphicon glyphicon-pencil" title="Lead Summary" />
		</nav:group>
		
	</sec:authorize>

<span class="minifyme" data-action="minifyMenu"> <i
	class="fa fa-arrow-circle-left hit"></i>
</span>

<!DOCTYPE html>

<%@include file="/includes/SessionCheck.jsp"%>
<html lang=en-us data-ng-app=smartApp>
<head>
<meta charset=utf-8>
<title>Bonrix CRM</title>
<meta name=viewport content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel=stylesheet href=css/jquery.tag-editor.css>
<link rel=stylesheet type=text/css media=screen href=http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css></link>
<!-- <link rel=stylesheet type=text/css media=screen href="css/font-awesome.min.css" /> -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel=stylesheet type=text/css media=screen href="css/smartadmin-production.min.css"/>
<link rel=stylesheet type=text/css media=screen href=css/smartadmin-skins.min.css />
<link rel=stylesheet type=text/css media=screen href=css/demo.min.css />
<link href="http://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="css/rangeselect.css" media="screen" />
<link rel="shortcut icon" href=img/favicon/favicon.ico type=image/x-icon>
<link rel=icon href=img/favicon/favicon.ico type=image/x-icon>
<link rel=apple-touch-icon href=img/splash/sptouch-icon-iphone.png>
<link rel=apple-touch-icon sizes=76x76 href=img/splash/touch-icon-ipad.png>
<link rel=apple-touch-icon sizes=120x120 href=img/splash/touch-icon-iphone-retina.png>
<link rel=apple-touch-icon sizes=152x152 href=img/splash/touch-icon-ipad-retina.png>
<meta name=apple-mobile-web-app-capable content=yes>
<meta name=apple-mobile-web-app-status-bar-style content=black>
<link rel=apple-touch-startup-image href=img/splash/ipad-landscape.png media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel=apple-touch-startup-image href=img/splash/ipad-portrait.png media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel=apple-touch-startup-image href=img/splash/iphone.png media="screen and (max-device-width: 320px)">
<link rel="stylesheet" type="text/css" href="css/summernote.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<link rel=stylesheet type=text/css media=screen href="css/dataTables.jqueryui.min.css"/>
 <link rel="stylesheet" type="text/css" href="css/bootstrap-duallistbox.css">
</head>
<body  data-ng-controller=SmartAppController class="container53">
<header id=header data-ng-include="'includes/header.jsp'"></header>
<aside id=left-panel>
<span data-ng-include="'includes/left-panel.jsp'"></span>
</aside>
<div id=main role=main >
<div id=ribbon data-ng-include="'includes/ribbon.jsp'" data-ribbon></div>
<div id=content data-ng-view class="page page-home" > <i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom" ng-hide="!layout.loading" ></i> </div>
</div>
<div class=page-footer>
<span data-ng-include="'includes/footer.jsp'"></span>
</div>
<div id=shortcut>
<span data-ng-include="'includes/shortcut.jsp'"></span>
</div>

<script src=js/jquery.js></script>
<script src=http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js></script>
<script src=js/jquery.cookie.js></script>
<script src=http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js></script> 
<script src=js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js></script>
<script src=js/app.config.js></script>
<script src=js/plugin/jquery-touch/jquery.ui.touch-punch.min.js></script>
<script src=http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js></script>
<script src=js/notification/SmartNotification.min.js></script>
<script src=js/smartwidgets/jarvis.widget.min.js></script>
<script src=js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js></script>
<script src=js/plugin/jquery-validate/jquery.validate.min.js></script>
<script src=js/plugin/select2/select2.min.js></script>
<script src=js/jquery.blockUI.min.js></script>
<script src=js/jquery.caret.min.js></script>
<script src=js/jquery.tag-editor.min.js></script>
<script src=js/speech/moment.min.js></script>
<script src=js/plugin/msie-fix/jquery.mb.browser.min.js></script>
<script src=https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.min.js></script>
<script src=http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-route.min.js></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular-animate.min.js"></script>
<script src=js/libs/angular/ui-bootstrap-custom-tpls-0.11.0.js></script>
<script src=js/app.js></script>
<script src=js/app.js></script>
<script src=js/ng/ng.app.js></script>
<script src=js/ng/ng.controllers.js></script>
<script src=js/ng/ng.directives.js></script>
<script src=js/sms_counter.min.js></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="js/jquery.csvToTable.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script> 
<script src="js/API.js"></script> 
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
 <script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="js/easypiechart.js"></script>
<script src="js/easypiechart-data.js"></script>
<script src="js/jquery.bootstrap-duallistbox.js"></script>
<script src="js/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="js/summernote.min.js"></script> 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script> 
<!-- <script src="js/loding.js"></script>  -->
<script type=text/javascript>
var grafint=null; if($.cookie("dsender")==undefined){$.cookie("dsender","<%=currentUser.getDefaltsenderid()%>")};</script>
</body>
</html>
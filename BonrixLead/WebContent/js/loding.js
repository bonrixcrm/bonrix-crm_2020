 function ajaxindicatorstart(text)
   {
   	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
   	jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="img/ajax-loader.gif"><div>Please Wait.....</div></div><div class="bg"></div></div>');
   	}

   	jQuery('#resultLoading').css({
   		'width':'100%',
   		'height':'100%',
   		//'position':'fixed',
   		'margin-left':'0%',
   		'margin-top':'0%',
   		'z-index':'10000000',
   		'top':'20',
   		'left':'20',
   		'right':'0',
   		'bottom':'0'
   	});

   	jQuery('#resultLoading .bg').css({
   		'background':'#858585',
   		'opacity':'0.7',
   		'width':'100%',
   		'height':'100%',
   		'position':'absolute',
   		'top':'0'
   	});

   	jQuery('#resultLoading>div:first').css({
   		'width': '250px',
   		'height':'75px',
   		'text-align': 'center',
   		'position': 'fixed',
   		'margin-left':'50%',
   		'margin-top':'20%',
   		'top':'0',
   		'left':'0',
   		'right':'0',
   		'bottom':'0',
   		'font-size':'16px',
   		'z-index':'10',
   		'color':'#ffffff'

   	});

       jQuery('#resultLoading .bg').height('100%');
          jQuery('#resultLoading').fadeIn(300);
       jQuery('body').css('cursor', 'wait');
   }
   
   function ajaxindicatorstop()
   {
  	 jQuery('#resultLoading .bg').height('100%');
          jQuery('#resultLoading').fadeOut(300);
       jQuery('body').css('cursor', 'default');

   }
   jQuery(document).ajaxStart(function () {
  		//show ajax indicator
  ajaxindicatorstart('loading data.. please wait..');
  }).ajaxStop(function ()
  {
  //hide ajax indicator
  ajaxindicatorstop();
  }).ajaxError(function() {
     // alert( "Handler for .error() called." );
     window.location="home.html"
  })
   jQuery.ajax({
  	   global: false,
  	   // ajax stuff
  	});

<%

printAllThreadIds();

%>
<%!


public void printAllThreadIds() {
    Thread currentThread = Thread.currentThread();
    ThreadGroup threadGroup = getRootThreadGroup(currentThread);
    int allActiveThreads = threadGroup.activeCount();
    Thread[] allThreads = new Thread[allActiveThreads];
    threadGroup.enumerate(allThreads);

    for (int i = 0; i < allThreads.length; i++) {
        Thread thread = allThreads[i];
        long id = thread.getId();
        
        System.out.println(id+"==="+thread.getName()+"===");
    }
}

private static ThreadGroup getRootThreadGroup(Thread thread) {
    ThreadGroup rootGroup = thread.getThreadGroup();
    while (true) {
        ThreadGroup parentGroup = rootGroup.getParent();
        if (parentGroup == null) {
            break;
        }
        rootGroup = parentGroup;
    }
    return rootGroup;
}




%>
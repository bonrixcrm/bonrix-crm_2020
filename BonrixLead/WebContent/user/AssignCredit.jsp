<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="/includes/SessionCheck.jsp"%>
<style>
#validity{z-index:1151 !important;}
</style>		
			<script type="text/javascript">
			$(document).ready(function(){$("#uid").select2();$("#validity").datepicker({defaultDate:"+1y",dateFormat:"dd-mm-yy",prevText:'<i class="fa fa-chevron-left"></i>',nextText:'<i class="fa fa-chevron-right"></i>',onClose:function(a){}})});

				 
				function formSub(event) {
				
					var credit = $('#credit').val();					
					if(credit=="" ||  isNaN(credit)){
						alert("Please Eneter Numaric value in Credit  only [0..9]");
						return;
					}
					
					if($('#crdb').is(':checked')) { 
					credit=-credit;	
					};
					var validity = $('#validity').val();
					var uid = $('#uid').val();
					var stid = $('#stid').val();
					var comments=$('#comments').val();					
					$.ajax({
						url : "assignCredit",
						type : "POST",
						data : {'credit':credit,'validity' : validity,'stid':stid,'comments':comments,'uid':uid },
						success : function(msg) {
                
							if(msg==1){
						msg="Successfully "+credit+" Credited!";		
							}else if(msg==2){
								msg="Successfully "+credit+" Debited!";		
							}else if(msg==-2){
								msg="Child user don't have enough balance for Debit !";		
							}
							else{
								msg="You Don't Have Enough  Credit Balance !";			
							}
							$("#credit").val('');
				$.smallBox({
						title : msg,
						content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
						color : "#296191",
						iconSmall : "fa fa-thumbs-up bounce animated",
						timeout : 6000
					});
				
									},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
		

 <sql:query var="msg" dataSource="SMS">
                      <%-- SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> --%>
                     SELECT u.username,u.uid FROM users u WHERE u.parentid=<%=currentUser.getUserid()%>	</sql:query>
		
		
		<sql:query var="crd" dataSource="SMS">
                    SELECT * FROM servicetype WHERE stid IN(SELECT stid FROM smscredit WHERE uid=<%=currentUser.getUserid()%>)
		</sql:query>



<section id="widget-grid" class=""> 
	<div class="row">
		<article class="col-sm-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i>
					</span>
					<h2>Credit Topup </h2>
				</header>
				<!-- widget div-->
				<div>
					<!-- widget edit box -->
			
					<div class="widget-body">
						<!-- <textarea rows="10" cols="100" name="numbers" id="numbers" ></textarea> -->
							<form class="form-horizontal">
							
							<fieldset>
								
							<div class="form-group">
									<label class="col-md-2 control-label bld">User:</label>
									<div class="col-md-7">
									 <select name="uid" id="uid" class="col-md-12">										
										<c:forEach var="all" items="${msg.rows}">
									<option value=${all.uid}>${all.username}</option>
											</c:forEach>										
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label bld">Service:</label>
									<div class="col-md-7">										
									<select name="stid" id="stid" class="form-control">										
										<c:forEach var="all" items="${crd.rows}">
									<option value=${all.stid}>${all.type}</option> 												
											</c:forEach>								
										</select> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label bld">Credit:</label>
									<div class="col-md-4">
							<input type="text" class="form-control"	name="credit" id="credit" placeholder="credit">
										</div>
									<div class="col-md-4">	
										
									<label class="radio radio-inline">											
											<input type="radio" checked="checked"  class="radiobox" name="style-0a">
											<span>Credit</span> 											
										</label>
										<label class="radio radio-inline">
											<input type="radio" class="radiobox" id="crdb" name="style-0a">
											<span>Debit</span>  
										</label>										
										</div>									
								</div>
									
									
									<div class="form-group">
									<label class="col-md-2 control-label bld">Notes:</label>
									<div class="col-md-7">
										
										<textarea rows="2"
										 class="form-control" name="comments" id="comments"
										placeholder="Enter Notes or comment"></textarea>
									</div>
								</div>							
									<div class="form-group">
									<label class="col-md-2 control-label bld">Validity</label>
									<div class="col-md-7">
										
									<div class="input-group">
											<input class="form-control" value="01-01-2016" name="validity" id="validity" type="text" placeholder="Validity">
											
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
									</div>
								</div>
							
							<div class="form-group">
									<label class="col-md-2 control-label"></label>
									<div class="col-md-7">
										
										<button type="button" onclick="formSub()"
											class="btn btn-primary">TopUp</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button>
									</div>
								</div>
							
							</fieldset>
							</form>
					</div>

				</div>

			</div>

		</article>

		<!-- end row -->
	</div>

</section>

<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="/includes/SessionCheck.jsp"%>
<style>
#validity{z-index:1151 !important;}
</style>
<section id="widget-grid" class="ng-scope">
	<!-- START ROW -->
	
	<div class="row">

		<!-- NEW COL START -->
		<article
			class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
			
		
			<script type="text/javascript">
				$(document).ready(function() {

				//	$('#datepicker').datepicker();
					
					$("#validity").datepicker({
					    defaultDate: "+1y",
					     dateFormat: 'dd-mm-yy', 
					   // formate:'dd',
					  //  changeMonth: true,
					//    numberOfMonths: 3,
					    prevText: '<i class="fa fa-chevron-left"></i>',
					    nextText: '<i class="fa fa-chevron-right"></i>',
					    onClose: function (selectedDate) {
					    //    $("#to").datepicker("option", "maxDate", selectedDate);
					    }
				
					});
				});

				function formSub(event) {

					//alert("");
					var credit = $('#credit').val();
					var validity = $('#validity').val();
					var uid = $('#uid').val();
					var stid = $('#stid').val();
					var comments=$('#comments').val();
					
					
					$.ajax({
						url : "assignCredit",
						type : "POST",
						data : {
							'credit' : credit,
							'validity' : validity,
							'stid':stid,
							'comments':comments,
							'uid' : uid
							
							
						},

						success : function(response) {
							alert(response);
							getdata()
							//window.location = "login.jsp";

						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
			<div class="jarviswidget jarviswidget-sortable" id="wid-id-1"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false" role="widget" style="">

<sql:query var="msg" dataSource="SMS">
                                     
                      <%-- SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> --%>
                      SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> AND s.uid NOT IN (<%=currentUser.getUserid()%>)
                      

		</sql:query>
		
		
		<sql:query var="crd" dataSource="SMS">
                                     
                    
                    SELECT * FROM servicetype WHERE stid IN(SELECT stid FROM smscredit WHERE uid=<%=currentUser.getUserid()%>)

		</sql:query>


				<!-- widget div-->
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="well no-padding">

						<form name="register" id="register" class="smart-form client-form">
							<header>Credit Topup </header>

							<fieldset>
							<div class="row">
									<section class="col col-6">
										<label class="select"> <select name="uid" id="uid">
										
										<c:forEach var="all" items="${msg.rows}">
									<option value=${all.uid}>${all.username}</option> 
												
											</c:forEach>
										
											<!-- 	<option value="0" selected="" disabled="">Gender</option>
												<option value="Ahmedamad">Ahmedamad</option> -->
												
										</select> <i></i>
										</label>
									</section>
									<!-- <section class="col col-6">
										<label class="input"> <i
											class="icon-append fa fa-calendar"></i> <input type="text" name="request" placeholder="Request activation on" class="datepicker" data-dateformat='dd/mm/yy'>
											<input type="text" name="number" id="number"
											placeholder="Mobile Number">
										</label>
									</section> -->
								</div>
								
								
								<div class="row">
							
									<section class="col col-6">
										<label class="select"> <select name="stid" id="stid">
										
										<c:forEach var="all" items="${crd.rows}">
									<option value=${all.stid}>${all.type}</option> 
												
											</c:forEach>
										
											<!-- 	<option value="0" selected="" disabled="">Gender</option>
												<option value="Ahmedamad">Ahmedamad</option> -->
												
										</select> <i></i>
										</label>
									</section>
									<!-- <section class="col col-6">
										<label class="input"> <i
											class="icon-append fa fa-calendar"></i> <input type="text" name="request" placeholder="Request activation on" class="datepicker" data-dateformat='dd/mm/yy'>
											<input type="text" name="number" id="number"
											placeholder="Mobile Number">
										</label>
									</section> -->
								</div>
								
								
								
								<div class="row">
									<section class="col col-6">
										<label class="input"> <i
											class="icon-append fa fa-user"></i> <input type="credit"
											name="credit" id="credit" placeholder="credit">
											<b class="tooltip tooltip-bottom-right">Needed to enter
												the website</b>
										</label>
									</section>

									<section class=" col col-6">
										<!-- <label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="email"
											id="validity" name="validity" placeholder="validity">
											<b class="tooltip tooltip-bottom-right">Needed to verify
												your account</b>
										</label> -->
									</section>
										
								</div>
									<div class="row">

										<section class=" col col-6">
										 <label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="text"
											id="comments" name="comments" placeholder="Comments">
											<b class="tooltip tooltip-bottom-right">Write Your Comments</b>
										</label> 
									</section>
									<section class=" col col-6">
										<!-- <label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="email"
											id="validity" name="validity" placeholder="validity">
											<b class="tooltip tooltip-bottom-right">Needed to verify
												your account</b>
										</label> -->
									</section>
								</div>
																
							</fieldset>

											<%-- 	<c:forEach var="all" items="${msg.rows}">


												<tr role="row" class="odd">
													<td class="sorting_1">1</td>
													<td><span class="responsiveExpander"></span>${all.username}</td>
													<td>${all.mobilenumber}</td>
													<td>${all.email}</td>
													<td>${all.smscredit}</td>
													<td>${all.date}</td>
												</tr>
											</c:forEach> --%>

								
								<section class="col col-6">
									<div class="input-group">
											<input class="form-control" name="validity" id="validity" type="text" placeholder="Validity">
											
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
									
									
								</section>
								
								
									
									
								<section class="col col-6">

									<footer>
										<button type="button" onclick="formSub()"
											class="btn btn-primary">TopUp</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button>
									</footer>

								</section>
							</fieldset>
</section>


</form>

</div>

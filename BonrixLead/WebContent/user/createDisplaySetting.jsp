<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



	<sql:query var="msg" dataSource="SMS">

		<%-- SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> --%>
                      SELECT u.username,u.uid FROM users u;
                      

		</sql:query>
	

			<script type="text/javascript">
				$(document).ready(function() {
					$("#uid").select2();
				});

				function formSub(event) {
					var title = $('#title').val();
					var subTitle = $('#subTitle').val();
					var address = $('#address').val();
					var aboutus = $('#aboutus').val();
					var url = $('#url').val();
					var homeUrl = $('#homeUrl').val();
					var info2 = $('#info2').val();
					var info1 = $('#info1').val();
					var logo = $('#logo').val();
					$.ajax({
						url : "saveDisplaySetting",
						type : "GET",
						data : {
							'title' : title,
							'subTitle' : subTitle,
							'aboutus' : aboutus,
							'homeUrl' : homeUrl,
							'url' : url,
							'address' : address,
							'info2' : info2,
							'logo' : logo,
							'info1' : info1
						},
						success : function(response) {
							$.smallBox({
								title : response,
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 4000
							});
							$('#remoteModal').modal('hide');							

						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>

<section id="widget-grid" class="ng-scope">
<div class="row">

		<!-- NEW COL START -->
		<article
			class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
			<form action="saveDisplaySetting" id="registerr"
				class="smart-form client-form" method="post"
				enctype="multipart/form-data">
				<header>Create Display Setting </header>

				<fieldset>
					<div class="row">
						<section class="col col-6">
							 <select class="form-control" name="uid" id="uid">
									<option value=0>Select User</option>
									<c:forEach var="all" items="${msg.rows}">
										<option value="${all.username}" >${all.username}</option>

									</c:forEach>

									<!-- 	<option value="0" selected="" disabled="">Gender</option>
												<option value="Ahmedamad">Ahmedamad</option> -->

							</select>
						</section>

						<!-- 	<section class=" col col-6">
										<label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="email"
											id="email" name="email" placeholder="Email address">
											<b class="tooltip tooltip-bottom-right">Needed to verify
												your account</b>
										</label>
									</section> -->
					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <input type="text" name="title"
								id="title" placeholder="Title" />


							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <input type="text" name="subTitle"
								id="subTitle" placeholder="Sub Title">
							</label>
						</section>
					</div>


					<div class="row">
						<section class="col col-6">
							<label class="input"> <input type="text" name="url"
								id="url" placeholder="Url">
							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <input type="text" name="homeUrl"
								id="homeUrl" placeholder="Home Url" />
							</label>
						</section>

					</div>

					<div class="row">
						<section class="col col-6">
							<label class="input"> <input type="file" name="logo"
								id="logo" placeholder="Logo" />
							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <input type="file" name="img"
								id="img" placeholder="Image" />
							</label>
						</section>
					</div>


					<div class="row">
						<section class="col col-6">
							<label class="input"> <textarea rows="4" cols="34"
									placeholder="  Address" id="address" name="address"></textarea>


							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <textarea rows="4" cols="34"
									placeholder="  About us" id="aboutus" name="aboutus"></textarea>

							</label>
						</section>

					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <textarea rows="4" cols="34"
									placeholder="  Information" id="info1" name="info1"></textarea>


							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <textarea rows="4" cols="34"
									placeholder="  Information" id="info2" name="info2"></textarea>

							</label>
						</section>

					</div>
					<!-- 		<section>
									<label class="input"> <i class="icon-append fa fa-lock"></i>
										<input type="text" name="address" id="address"
										placeholder="Address"> <b
										class="tooltip tooltip-bottom-right">Enter Address</b>
									</label>
								</section> -->

					<section class="col col-6">

						<footer>

							<input type="submit" class="btn btn-primary" name="Register">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</footer>

					</section>

				</fieldset>
			</form>
		</article>
		</div>
		
</section>


<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section id="widget-grid" class="ng-scope">
	<!-- START ROW -->
	<style>
#datepicker {
	z-index: 1151 !important;
}
</style>

	<script type="text/javascript">
		function changeTag() {

			var msg = $('#template').val() + $('#tag').val();
			//alert("sms : " + msg);
			$('#template').val(msg)

		}
	</script>


	<sql:query var="msg" dataSource="SMS">

		<%-- SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> --%>
                      SELECT * from  users u;
                      

		</sql:query>
	<div class="row">

		<!-- NEW COL START -->
		<article
			class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

			<script type="text/javascript">
				$(document).ready(function() {

					$("#datepicker").datepicker({
						defaultDate : "+1y",
						dateFormat : 'dd-mm-yy',
						// formate:'dd',
						//  changeMonth: true,
						//    numberOfMonths: 3,
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
							//    $("#to").datepicker("option", "maxDate", selectedDate);
						}

					});
				});

				function formSub(event) {

					var name = $('#name').val();
					var template = $('#template').val();
					var subject = $('#subject').val();

					$
							.ajax({
								url : "saveEmailTemplate",
								type : "POST",
								data : {
									'name' : name,
									'template' : template,
									'subject' : subject

								},

								success : function(response) {

									$
											.smallBox({
												title : response,
												content : "<i class='fa fa-clock-o'></i> <i>Save Succefully.</i>",
												color : "#296191",
												iconSmall : "fa fa-thumbs-up bounce animated",
												timeout : 4000
											});
									$('#remoteModal').modal('hide');
									//window.location = "login.jsp";

								},
								error : function(xhr, status, error) {
									alert(xhr.responseText);
								}
							});
					return false;
				}
			</script>


			<form action="saveEmailTemplate" id="registerr"
				class="smart-form client-form" method="post"
				enctype="multipart/form-data">
				<header>Create Message Template </header>

				<fieldset>
					<!-- <div class="row">
						<section class="col col-3">
							<label class="select"> <select style="width: 267px;"
								class="select2" name="uid" id="uid">
									<option value=0>Select template</option>

									<option value="CreateUser">CreateUser</option>
									<option value="SenderName">SenderName</option>
									<option value="SenderName">Credit</option>
									<option value="SenderName">Template</option>

										<option value="0" selected="" disabled="">Gender</option>
												<option value="Ahmedamad">Ahmedamad</option>

							</select> <i></i>
							</label>
						</section>

							<section class=" col col-6">
										<label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="email"
											id="email" name="email" placeholder="Email address">
											<b class="tooltip tooltip-bottom-right">Needed to verify
												your account</b>
										</label>
									</section>
					</div> -->
					<div class="row">
						<section class="col col-6">
							<label class="input"> <input type="text" name="name"
								id="name" placeholder=" Template Name" />



							</label>
						</section>
					</div>
					<div>

						<section class="col col-6">
							<label class="input"> <input type="text" name="subject"
								id="subject" placeholder=" Subject" />



							</label>
						</section>

						<!-- <section class="col col-3">
							<label class="input"> <input type="text" name="subTitle"
								id="subTitle" placeholder="Sub Title">
							</label>
						</section> -->
					</div>

					<div class="row">
						<section class="col col-6" onchange="changeTag()">
							<label class="select"> <select style="width: 267px;"
								class="select2" name="tag" id="tag">
									<option value=0>Replace Tag</option>
									<option>&lt;PersonName&gt;</option>
									<option>&lt;UserName&gt;</option>
									<option>&lt;Password&gt;</option>
									<option>&lt;SenderName&gt;</option>

									<option>&lt;Address&gt;</option>
									<option>&lt;MobileNo&gt;</option>
									<option>&lt;RegDate&gt;</option>
									<option>&lt;SmsCredit&gt;</option>
									<!-- <option>&lt;SentSMS&gt;</option> -->
									<option>&lt;E-mailAddress&gt;</option>

									<!-- 	<option value="0" selected="" disabled="">Gender</option>
												<option value="Ahmedamad">Ahmedamad</option> -->

							</select> <i></i>
							</label>
						</section>
					</div>


					<div class="row">
						<section class="col col-6">
							<label class="input"> <textarea rows="4" cols="34"
									placeholder="  Template" id="template" name="template"></textarea>


							</label>
						</section>


					</div>


					<section class="col col-6">

						<footer>

							<input type="button" onclick="formSub()" class="btn btn-primary"
								name="Register" value="Save">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</footer>

					</section>

				</fieldset>
			</form>
</section>




</div>

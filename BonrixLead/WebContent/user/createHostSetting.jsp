<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section id="widget-grid" class="ng-scope">
	<!-- START ROW -->
	<style>
#datepicker {
	z-index: 1151 !important;
}
</style>

	<sql:query var="msg" dataSource="SMS">

		<%-- SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> --%>
                      SELECT u.username,u.uid FROM users u;
                      

		</sql:query>
	<div class="row">

		<!-- NEW COL START -->
		<article
			class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

			<script type="text/javascript">
				$(document).ready(function() {

					$("#datepicker").datepicker({
						defaultDate : "+1y",
						dateFormat : 'dd-mm-yy',
						// formate:'dd',
						//  changeMonth: true,
						//    numberOfMonths: 3,
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
							//    $("#to").datepicker("option", "maxDate", selectedDate);
						}

					});
				});

				function formSub(event) {
					
					var hostName = $('#hostName').val();
					var username = $('#username').val();
					var user = $('#user').val();
					var password = $('#password').val();
					var displayName = $('#displayName').val();
					var from = $('#from').val();
					

					$.ajax({
						url : "saveHostSetting",
						type : "POST",
						data : {
							'hostName' : hostName,
							'username' : username,
							'user' : user,
							'password' : password,
							'displayName' : displayName,
							'from' : from
							
						},

						success : function(response) {
							$.smallBox({
								title : response,
								/* content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>", */
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 4000
							});
							$('#remoteModal').modal('hide');
							//window.location = "login.jsp";

						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>


			<form action="" id="registerr"
				class="smart-form client-form">
				<header>Create Host Setting </header>

				<fieldset>
					<div class="row">
						<section class="col col-6">
							<label class="select"> <select style="width: 280px;"
								class="select2" name="user" id="user">
									<option value=0>Select User</option>
									<c:forEach var="all" items="${msg.rows}">
										<option value=${all.username}>${all.username}</option>

									</c:forEach>


							</select> <i></i>
							</label>
						</section>

								<section class="col col-6">
							<label class="input"> <input type="text" name="hostName"
								id="hostName" placeholder="Host Name">
							</label>
						</section>
					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <input type="text" name="username"
								id="username" placeholder="Username" />


							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <input type="text" name="password"
								id="password" placeholder="Password">
							</label>
						</section>
					</div>


					<div class="row">
						<section class="col col-6">
							<label class="input"> <input type="text" name="from"
								id="from" placeholder="From">
							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <input type="text"
								name="displayName" id="displayName" placeholder="DisplayName" />
							</label>
						</section>

					</div>

					<section class="col col-6">

						<footer>

							<input type="button" value="Save" class="btn btn-primary" name="Register" onclick="formSub()">
							<button type="button" class="btn btn-default" 
								data-dismiss="modal">Close</button>
						</footer>

					</section>

				</fieldset>
			</form>
			
</section>




</div>

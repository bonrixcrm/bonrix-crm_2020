
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>

	
<sql:query var="msg" dataSource="SMS">SELECT * FROM servicetype where isvisible=1 AND stid in (select stid from smscredit where uid=<%=currentUser.getUserid()%>); </sql:query>
			<script type="text/javascript">
				$(document).ready(function() {

					$('#datepicker').datepicker();
				});

				function formSub(event) {

					//alert("asda");
					var isactive = $('#isactive').val();
					var senderName = $('#senderName').val().toUpperCase();
					var uid = $('#uid').val();
					var sndrservice=$('#sndrservice').val();
					

					if(senderName.length!=6){
						
						alert("SenderName must be 6 Character long");
						return;
					}
					$.ajax({
						url : "createSenderName",
						type : "POST",
						data : {
							'isactive' : isactive,
							'senderName' : senderName,
							'uid' : "11",
							'sndrservice':sndrservice

						},

						success : function(response) {

							$.smallBox({
								title : response,
								content : "<i class='fa fa-clock-o'></i>",
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 4000
							});
							$('#remoteModal').modal('hide');

						},
						error : function(xhr, status, error) {
							alert("Something going Wrong!");
							//	alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
			<section id="widget-grid" class=""> 
	<div class="row">
		<article class="col-sm-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i>
					</span>
					<h2>Request SenderName</h2>
				</header>
				<!-- widget div-->
				<div>
					<!-- widget edit box -->
			
					<div class="widget-body">
						<!-- <textarea rows="10" cols="100" name="numbers" id="numbers" ></textarea> -->
							<form class="form-horizontal">
							
							<fieldset>

								<div class="form-group">

								
<label class="col-md-2 control-label bld">SenderName:</label>
									<div class="col-md-7">

											<!-- <div class="input-group" > -->
											<input class="form-control" id="senderName" name="senderName"
												type="text" placeholder="SenderName">
												</div>
												</div>
								<div class="form-group">
								
                   <label class="col-md-2 control-label bld">Service:</label>
									<div class="col-md-7">
	
												<select name="sndrservice" id="sndrservice" class="form-control">										
									<c:forEach var="all" items="${msg.rows}">
									<option value=${all.type}>${all.type}</option> 												
											</c:forEach>								
										</select> 
												
												</div>
											</div>
											<div class="form-group">
								
                   <label class="col-md-2 control-label bld"></label>
									<div class="col-md-7">
											
										
												<button class="btn btn-primary" type="button"
													onclick="formSub()">Submit</button>
												<button type="button" class="btn btn-primary"
													data-dismiss="modal">Close</button>

										
											</div>

										</div>
								
							

							</fieldset>
						</form>
					</div>
				</div>
				</div>
				</article>
				</div>
				</section>
				
			
		



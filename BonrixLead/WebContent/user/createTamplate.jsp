
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<section id="widget-grid" class="ng-scope">
	<div class="row">

		<article
			class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

			<script type="text/javascript">
			
				$(document).ready(function() {
					$('#datepicker').datepicker();
				});

				function formSub(event) {
					var tname = $('#tname').val();
					var tmessage = $('#tmessage').val();

					$.ajax({
						url : "createTamplate",
						type : "POST",
						data : {

							'tname' : tname,
							'tmessage' : tmessage,
							'istrans' : 'false'
						},

						success : function(response) {

					
											
											$.smallBox({
												title : response,
												content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
												color : "#296191",
												iconSmall : "fa fa-thumbs-up bounce animated",
												timeout : 4000
											});
											$('#remoteModal').modal('hide');
							//window.location = "login.jsp";

						},
						error : function(xhr, status, error) {
							
							alert("------>"+xhr.responseText);
						}
					});
					return false;
				}
			</script>
			<div class="jarviswidget jarviswidget-sortable" id="wid-id-1"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false" role="widget" style="">




				<!-- widget div-->
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="well no-padding">

						<form name="register" id="register" class="smart-form client-form">
							<header>Create template </header>

							<fieldset>
								<div class="row">
									<section class="col col-8">
										<label class="input"> <i
											class="icon-append fa fa-user"></i> <input type="text"
											name="tname" id="tname" placeholder="Tamplate Name">
											<b class="tooltip tooltip-bottom-right">Needed to enter
												the template Name</b>
										</label>
									</section>
								</div>
								<div class="row">


									<section class="col col-8">
										<label class="textarea"> <i
											class="icon-append fa fa-comment"></i> <textarea rows="5"
												name="tmessage" id="tmessage"  placeholder="Write template Message"></textarea>
										</label>
									</section>
								</div>
								<div>

									<section class="col col-6">
										<footer>
											<button type="button" onclick="formSub()"
												class="btn btn-primary">Assign</button>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
										</footer>
									</section>

								</div>


							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</article>
	</div>


</section>



</script>



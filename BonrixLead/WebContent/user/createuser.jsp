<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<style>
#datepicker {
	z-index: 1151 !important;
}
</style>



<script type="text/javascript">
			
			$(document).ready(function(){
				
				$("#minimumnumber").spinner({
				    min: 5,
				    max: 2500,
				    step: 25,
				    start: 1000,
				    numberFormat: "C"
				});
				

				$("#cutting").spinner({
				    min: 5,
				    max: 100,
				    step: 1,
				    start: 0,
				    numberFormat: "C"
				});
				
				
			});
			
				function formSub(event) {

					//alert("asda");
					var username = $('#username').val();
					var password = $('#password').val();
					var name = $('#name').val();
					var city = $('#city').val();
					var email = $('#email').val();
					var number = $('#number').val();
					var address = $('#address').val();
					var datepicker=$('#datepicker').val();
					var minimumnumber=$('#minimumnumber').val();
					var cutting=$('#cutting').val();
					
					if(username==""){
						alert("you must enter username.");
						return;
					}
					
					if($("#passwordConfirm").val()!=password || password ==""){
						alert("password does not match");
						return;
					}
					if($("#datepicker").val()=="" ){
						alert("Please Enter Date...!!!");
						return;
					}
					
					
					$.ajax({
						url : "registerUserAdmin",
						type : "GET",
						data : {
							'username' : username,
							'password' : password,
							'email' : email,
							'name' : name,
							'city' : city,
							'number' : number,
							'address' : address,
							'companyName' : $("#companyName").val(),
							'datepicker':datepicker,
							'minimumnumber':'0',
							'cutting':cutting
							
						},

						success : function(response) {
							$.smallBox({
								title : response,
								/* content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>", */
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 4000
							});
							 $('#remoteModal').modal('hide');
							//window.location = "login.jsp";

						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
<div class="jarviswidget jarviswidget-sortable" id="wid-id-1"
	data-widget-colorbutton="false" data-widget-editbutton="false"
	data-widget-custombutton="false" role="widget" style="">




	<!-- widget div-->
	<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
		<div class="well no-padding">

			<form name="register" id="register" class="smart-form client-form">
				<header>Create Company </header>

				<fieldset>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-user"></i>
								<input type="text" name="username" id="username"
								placeholder="Username"> <b
								class="tooltip tooltip-bottom-right">Username</b>
							</label>
						</section>

						<section class=" col col-6">
							<label class="input"> <i
								class="icon-append fa fa-envelope"></i> <input type="email"
								id="email" name="email" placeholder="Email address"> <b
								class="tooltip tooltip-bottom-right">Needed to verify your
									account</b>
							</label>
						</section>
					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-lock"></i>
								<input type="password" name="password" placeholder="Password"
								id="password"> <b class="tooltip tooltip-bottom-right">Don't
									forget your password</b>
							</label>
						</section>

						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-lock"></i>
								<input type="password" name="passwordConfirm"
								id="passwordConfirm" placeholder="Confirm password"> <b
								class="tooltip tooltip-bottom-right">Don't forget your
									password</b>
							</label>
						</section>
					</div>
					<section>
						<label class="input"> <i class="icon-append fa fa-lock"></i>
							<input type="text" name="address" id="address"
							placeholder="Address"> <b
							class="tooltip tooltip-bottom-right">Enter Address</b>
						</label>
					</section>

				</fieldset>

				<fieldset>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <input type="text" name="name"
								id="name" placeholder="Name">
							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <input type="text"
								name="companyName" id="companyName" placeholder="Company Name">
							</label>
						</section>
					</div>

					<div class="row">
						<section class="col col-6">
							<label class="input"> <!--  <select name="city" id="city">
												<option value="0" selected="" disabled="">Gender</option>
												<option value="Ahmedamad">Ahmedamad</option>
												<option value="Surat">Surat</option>
												<option value="Other">Prefer not to answer</option>
										</select> --> <input type="text" name="city" id="city"
								placeholder="City Name" />


							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <i
								class="icon-append fa fa-calendar"></i> <!-- <input type="text" name="request" placeholder="Request activation on" class="datepicker" data-dateformat='dd/mm/yy'> -->
								<input type="text" name="number" id="number"
								placeholder="Mobile Number">
							</label>
						</section>
					</div>
					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<div class="row">
							<%-- <section class="col col-6">

								<label>Staff Limit</label> <input class="spinner-both"
									style="height: 18px;" id="minimumnumber" name="minimumnumber"
									value="0">

							</section> --%>

							<section class="col col-6">

								<label>Telecaller Limit</label> <input class="spinner-both"
									style="height: 18px;" id="cutting" name="cutting" value="0">

							</section>

						</div>
					</sec:authorize>

					<sec:authorize access="!hasRole('ROLE_ADMIN')">
						<div class="row" style="display: none;">
							<section class="col col-6">

								<label>Minimum Number</label> <input class="spinner-both"
									style="height: 18px;" id="minimumnumber" name="minimumnumber"
									value="0">

							</section>

							<section class="col col-6">

								<label>Cutting(%)</label> <input class="spinner-both"
									style="height: 18px;" id="cutting" name="cutting" value="0">

							</section>

						</div>
					</sec:authorize>
					<input type="hidden" id="datepicker" value="25-03-2016"
						name="datepicker">
					<!-- <section class="col col-6">
									<label class="input"> 
										<input type="text" id="datepicker" name="datepicker"
											placeholder="Select a date" > <span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</label>
								</section> -->
					<section class="col col-6">

						<footer>
							<button type="button" onclick="formSub()" class="btn btn-primary">Register</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</footer>

					</section>
				</fieldset>



			</form>

		</div>
	</div>
</div>
	<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp" %>


	<sql:query var="msg" dataSource="SMS">
                           SELECT * FROM displaysetting WHERE id=<%=request.getParameter("id") %>
                           
                           </sql:query>
<%

String filePath="";
filePath = request.getServletContext().getRealPath("/");

filePath = filePath + "\\UploadFile\\logo\\";
%>

<section id="widget-grid" class="ng-scope">
	<!-- START ROW -->
	<style>
#datepicker {
	z-index: 1151 !important;
}
</style>

	<div class="row">

		<!-- NEW COL START -->
		<article
			class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

			<script type="text/javascript">
				$(document).ready(function() {

					$("#datepicker").datepicker({
						defaultDate : "+1y",
						dateFormat : 'dd-mm-yy',
						// formate:'dd',
						//  changeMonth: true,
						//    numberOfMonths: 3,
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
							//    $("#to").datepicker("option", "maxDate", selectedDate);
						}

					});
				});

				function formSub(event) {

					//alert("asda");
					var title = $('#title').val();
					var subTitle = $('#subTitle').val();
					var address = $('#address').val();
					var aboutus = $('#aboutus').val();
					var url = $('#url').val();
					var homeUrl = $('#homeUrl').val();
					var info2 = $('#info2').val();
					var info1 = $('#info1').val();
					var logo = $('#logo').val();

					$.ajax({
						url : "saveDisplaySetting",
						type : "GET",
						data : {
							'title' : title,
							'subTitle' : subTitle,
							'aboutus' : aboutus,
							'homeUrl' : homeUrl,
							'url' : url,
							'address' : address,
							'info2' : info2,
							'logo' : logo,
							'info1' : info1
						},

						success : function(response) {
							$.smallBox({
								title : response,
								/* content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>", */
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 4000
							});
							$('#remoteModal').modal('hide');
							//window.location = "login.jsp";

						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>


			<form action="updateDisplaySetting" id="registerr"
				class="smart-form client-form" method="post"
				enctype="multipart/form-data">
				<header>Update Display Setting </header>


				<c:forEach items="${msg.rows}" var="s">
					<input type="hidden" id="id" name="id" value="${s.id}">
					<fieldset>
						<div class="row">
							<section class="col col-6">
								<label class="input"> <input type="text" name="title"
									id="title" placeholder="Title" value="${s.user}"
									disabled="true" />


								</label>
							</section>

							<!-- 	<section class=" col col-6">
										<label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="email"
											id="email" name="email" placeholder="Email address">
											<b class="tooltip tooltip-bottom-right">Needed to verify
												your account</b>
										</label>
									</section> -->
						</div>
						<div class="row">
							<section class="col col-6">
								<label class="input"> <input type="text" name="title"
									id="title" placeholder="Title" value="${s.title}" />


								</label>
							</section>
							<section class="col col-6">
								<label class="input"> <input type="text" name="subTitle"
									id="subTitle" placeholder="Sub Title" value="${s.subTitle}">
								</label>
							</section>
						</div>


						<div class="row">
							<section class="col col-6">
								<label class="input"> <input type="text" name="url"
									id="url" placeholder="Url" value="${s.url}">
								</label>
							</section>
							<section class="col col-6">
								<label class="input"> <input type="text" name="homeUrl"
									value="${s.homeUrl}" id="homeUrl" placeholder="Home Url" />
								</label>
							</section>

						</div>

						<div class="row">
							<section class="col col-6">
								<img alt="" height="200" width="200"
									src="UploadFile/logo/${s.logoImage}"> <label
									class="input"> <input type="file" name="logo" id="logo"
									placeholder="Logo" />
								</label>
							</section>
							<section class="col col-6">
								<img alt="" height="200" width="200"
									src="UploadFile/logo/${s.image2}"> <label class="input">
									<input type="file" name="img" id="img" placeholder="Image" />
								</label>
							</section>
						</div>


						<div class="row">
							<section class="col col-6">
								<label class="input"> <textarea rows="4" cols="34"
										placeholder="  Address" id="address" name="address">${s.address}</textarea>


								</label>
							</section>
							<section class="col col-6">
								<label class="input"> <textarea rows="4" cols="34"
										placeholder="  About us" id="aboutus" name="aboutus">${s.aboutus}</textarea>

								</label>
							</section>

						</div>
						<div class="row">
							<section class="col col-6">
								<label class="input"> <textarea rows="4" cols="34"
										placeholder="  Information" id="info1" name="info1">${s.extraInfo1}</textarea>


								</label>
							</section>
							<section class="col col-6">
								<label class="input"> <textarea rows="4" cols="34"
										placeholder="  Information" id="info2" name="info2">${s.extraInfo2}</textarea>

								</label>
							</section>

						</div>

						<section class="col col-6">

							<footer>

								<input type="submit" class="btn btn-primary" name="Register">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Close</button>
							</footer>

						</section>
					</fieldset>
				</c:forEach>
			</form>
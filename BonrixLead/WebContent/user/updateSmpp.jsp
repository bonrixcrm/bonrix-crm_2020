<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section id="widget-grid" class="ng-scope">
	<!-- START ROW -->
	<style>
#datepicker {
	z-index: 1151 !important;
}
</style>

	<sql:query var="msg" dataSource="SMS">

		<%-- SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> --%>
                      SELECT smppname,systemid,isactive,isrunning,syspassowrd,systemtype,channelno,connecterror,maxconnect,isrunning,isactive,port,ip,stid FROM smppconfig WHERE stid=<%=request.getParameter("id")%>

	</sql:query>

	<script type="text/javascript">
		$(document).ready(function() {
			var s = $('#tt').attr('value');
			$('#channel').val(s)
			
			$("#datepicker").datepicker({
				defaultDate : "+1y",
				dateFormat : 'dd-mm-yy',
				// formate:'dd',
				//  changeMonth: true,
				//    numberOfMonths: 3,
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onClose : function(selectedDate) {
					//    $("#to").datepicker("option", "maxDate", selectedDate);
				}

			});
			
			/* $(function() {
			    $('input[name=selectNow]').on('click',function(event) {
			        selectByText( $.trim( $('input[name=selText]').val() ) );
			    }).click();
			}); */
			
		

		});

		
		
		function formSub(event) {

			var ip = $('#ip').val();
			var port = $('#port').val();
			var SystemId = $('#SystemId').val();
			var password = $('#password').val();
			var SmppName = $('#SmppName').val();
			var type = $('#type').val();
			var stid = $("#stid").val();
			var channel = $('#channel').val();
			var isrunning = $("#isrunning").val();
			var maxconnect=$("#maxconnect").val();
			
			
			$.ajax({
				url : "updateSmpp",
				type : "POST",
				data : {
					'ip' : ip,
					'port' : port,
					'SystemId' : SystemId,
					'password' : password,
					'SmppName' : SmppName,
					'type' : type,
					'channel' : channel,
					'stid' : stid,
					'isrunning':isrunning,
					'maxconnect':maxconnect
					

				},

				success : function(response) {
					$.smallBox({
						title : response,
						/* content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>", */
						color : "#296191",
						iconSmall : "fa fa-thumbs-up bounce animated",
						timeout : 4000
					});
					$('#remoteModal').modal('hide');
					//window.location = "login.jsp";

				},
				error : function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});

			return false;
		}
	</script>

		<c:forEach items="${msg.rows}" var="s">
			<input type="hidden" name="stid" id="stid" value="${s.stid}">
			<input type="hidden" name="tt" id="tt" value="${s.channelno}">
			
				
				

				



			

			
	
	
	
<section id="widget-grid" class=""> 
	<div class="row">
		<article class="col-sm-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i>
					</span>
					<h2>Edit SMPP </h2>
				</header>
				<!-- widget div-->
				<div>
					<!-- widget edit box -->
			
					<div class="widget-body">
						<!-- <textarea rows="10" cols="100" name="numbers" id="numbers" ></textarea> -->
							<form class="form-horizontal">
							
							<fieldset>
								
							<div class="form-group">
									<label class="col-md-2 control-label bld">Channel:</label>
									<div class="col-md-7">
								
										
										 <select class="col-md-12 form-control "
							class="select2" name="channel"  id="channel">
								<option value=0>Select Channel</option>
									
									
									<option value="1">Channel 1</option>
									<option value="2">Channel 2</option>
									<option value="3">Channel 3</option>
									<option value="4">Channel 4</option>


						</select> 
						
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label bld">Connected:</label>
									<div class="col-md-2">
						
							
							<input type="text"  class="form-control" name="isrunning" id="isrunning"
							placeholder="Running" value="${s.isrunning}"  />
										</div>
											<label class="col-md-3 control-label bld">Max Connect:</label>
									<div class="col-md-2">	
										
																				
											
								<input type="text"  class="form-control" name="maxconnect" id="maxconnect"
							placeholder="Running" value="${s.maxconnect}" />
							
											 											
										
																				
										</div>									
								</div>
							
							
							<div class="form-group">
									<label class="col-md-2 control-label bld">SMPP IP:</label>
									<div class="col-md-3">
										
							<input type="text" class="form-control" name="ip" id="ip"
							placeholder="IP" value="${s.ip}" />
									</div>
										<label class="col-md-2 control-label bld">PORT:</label>
									<div class="col-md-2">
						<input type="text" class="form-control" name="port"
							id="port" placeholder="Port" value="${s.port}">
										
									</div>
								</div>
								
								
							
								
							
							
							<div class="form-group">
									<label class="col-md-2 control-label bld">System Id:</label>
									<div class="col-md-7">
										
							<input type="text" class="form-control" name="SystemId"
							id="SystemId" placeholder="System Id" value="${s.systemid}" />
							
									</div>
								</div>		
							
							
									<div class="form-group">
									<label class="col-md-2 control-label bld">Password:</label>
									<div class="col-md-7">
										
										 <input class="form-control" type="text" name="password"
							id="password" placeholder="Password" value="${s.syspassowrd}">		
							
							
									</div>
								</div>		
									
									<div class="form-group">
									<label class="col-md-2 control-label bld">SystemType:</label>
									<div class="col-md-7">
										
										
										 <input class="form-control" type="text" name="type"
							id="type" placeholder="Type" value="${s.systemtype}">
							
									</div>
								</div>		
								
								
											
									<div class="form-group">
									<label class="col-md-2 control-label bld">SMPPName</label>
									<div class="col-md-7">
										
									<div class="input-group">
											
											 <input class="form-control" type="text" name="SmppName"
							id="SmppName" placeholder="SmppName" value="${s.smppname}" />
									</div>
									
									</div>
								</div>
							
							<div class="form-group">
									<label class="col-md-2 control-label"></label>
									<div class="col-md-7">
										
												<input type="button" value="Save" class="btn btn-primary"
							name="Register" onclick="formSub()">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							
							</fieldset>
							</form>
					</div>

				</div>

			</div>

		</article>

		<!-- end row -->
	</div>

</section>
</c:forEach>
	
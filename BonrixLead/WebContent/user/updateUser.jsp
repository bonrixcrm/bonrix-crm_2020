<%@page import="com.bonrix.sms.service.UserService"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<sql:query var="msg" dataSource="SMS" >
                      SELECT * FROM users WHERE uid=${param.uid}
</sql:query>

<script type="text/javascript">
$(document).ready(function() {
		$("#minimumnumber").spinner({
					    min: 5,
					    max: 2500,
					    step: 25,
					    start: 1000,
					    numberFormat: "C"
			});
		$("#cutting").spinner({
    				    min: 5,
					    max: 100,
					    step: 1,
					    start: 0,
					    numberFormat: "C"
			});
		$('body').on('hidden.bs.modal', '.modal', function () {
						$(this).removeData('bs.modal');
					});
		    });

	function formSub(event) {
				var username = $('#username').val();
				var password = $('#password').val();
				var name = $('#name').val();
				var city = $('#city').val();
				var email = $('#email').val();
				var number = $('#number').val();
				var address = $('#address').val();
				var datepicker=$('#datepicker').val();
				var cmpname= $("#companyName").val();
				var uid = $('#uid').val();
				var minimumnumber=$('#minimumnumber').val();
				var cutting=$('#cutting').val();
				
				if(minimumnumber==null){
					minimumnumber='0';
				}
					
					$.ajax({
						url : "updateUserAdmin",
						type : "GET",
						data : {
							'username' : username,
							'password' : password,
							'email' : email,
							'name' : name,
							'companyName' : cmpname,
							'city' : city,
							'number' : number,
							'uid' : uid,
							'datepicker':datepicker,
							'address' : address,
							'minimumnumber':minimumnumber,
							'cutting':cutting
						},
						success : function(response) {
							$.smallBox({
								title : response,
								/* content : "<i class='fa fa-clock-o'></i>", */
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 4000
							});
							$('#remoteModal4').modal('hide');
							//window.location = "login.jsp";
						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
</script>

<section id="widget-grid" class="ng-scope">
			<form name="register" id="register" class="smart-form client-form">
				<header>Update User</header>
				<c:forEach  items="${msg.rows}"  var="all">
				<input type="hidden" value="${all.uid}" name="uid" id="uid" />
				<fieldset>
					<div class="row">
						<section class="col col-6">
						<label class="input"> Username</label>
							<label class="input"> <i class="icon-append fa fa-user"></i>
								<input type="text" name="username" id="username"
								placeholder="Username" value="${all.username}"> <b
								class="tooltip tooltip-bottom-right">Username</b>
							</label>
						</section>

						<section class=" col col-6">
						<label class="input"> Email</label>
							<label class="input"> <i
								class="icon-append fa fa-envelope"></i> <input type="email"
								id="email" name="email" placeholder="Email address" value="${all.email}"> <b
								class="tooltip tooltip-bottom-right">Needed to verify your
									account</b>
							</label>
						</section>
					</div>
					<div class="row">
						<section class="col col-6">
						<label class="input"> Password</label>
							<label class="input"> <i class="icon-append fa fa-lock"></i>
								<input type="password" name="password" placeholder="Password"
								id="password" value="${all.password}"> <b class="tooltip tooltip-bottom-right">Don't
									forget your password</b>
							</label>
						</section>

						<section class="col col-6">
							<label class="input">Repeate Password</label>
							<label class="input"> <i class="icon-append fa fa-lock"></i>
								<input type="password" name="passwordConfirm"
								id="passwordConfirm" placeholder="Confirm password" value="${all.password}"> <b
								class="tooltip tooltip-bottom-right">Don't forget your
									password</b>
							</label>
						</section>
					</div>
					<section>
					<label class="input">Address</label>
						<label class="input"> <i class="icon-append fa fa-lock"></i>
							<input type="text" name="address" id="address"
							placeholder="Address" value="${all.address}"> <b
							class="tooltip tooltip-bottom-right">Enter Address</b>
						</label>
					</section>

				</fieldset>

				<fieldset>
					<div class="row">
						<section class="col col-6">
						<label class="input">Name</label>
							<label class="input"> <input type="text" name="name"
								id="name" placeholder="Name" value="${all.name}">
							</label>
						</section>
						<section class="col col-6">
						<label class="input">Company Name</label>
							<label class="input"> <input type="text"
								name="companyName" id="companyName" placeholder="Company Name" value="${all.cmpname}">
							</label>
						</section>
					</div>

					<div class="row">
						<section class="col col-6">
						<label class="input">City</label>
							<label class="input"> <!--  <select name="city" id="city">
												<option value="0" selected="" disabled="">Gender</option>
												<option value="Ahmedamad">Ahmedamad</option>
												<option value="Surat">Surat</option>
												<option value="Other">Prefer not to answer</option>
										</select> --> <input type="text" name="city" id="city"
								placeholder="City Name"  value="${all.city}" />


							</label>
						</section>
						<section class="col col-6">
						<label class="input">Mobile Number</label>
							<label class="input"> <i
								class="icon-append fa fa-calendar"></i> <!-- <input type="text" name="request" placeholder="Request activation on" class="datepicker" data-dateformat='dd/mm/yy'> -->
								<input type="text" name="number" id="number"
								placeholder="Mobile Number" value="${all.mobilenumber}">
							</label>
						</section>
					</div>
					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<div class="row">
							<section class="col col-6">
								<label>Telecaller Limit</label> <input class="spinner-both"
									style="height: 18px;" id="cutting" name="cutting" value="${all.limit_tally_caller}" >

							</section>

						</div>
					</sec:authorize>

					<sec:authorize access="!hasRole('ROLE_ADMIN')">
						<div class="row" style="display: none;">
							<section class="col col-6">
								<label>Minimum Number</label> 
								<input class="spinner-both"	style="height: 18px;" id="minimumnumber" name="minimumnumber" value="${all.min_number}">
							</section>

							<section class="col col-6">
								<label>Cutting(%)</label> <input class="spinner-both" style="height: 18px;" id="cutting" name="cutting" value="${all.cutting}">
							</section>

						</div>
					</sec:authorize>
					<input type="hidden" id="datepicker" value="25-03-2016"	name="datepicker">
					<section class="col col-6">
						<footer>
							<button type="button" onclick="formSub()" class="btn btn-primary">Register</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</footer>

					</section>
				</fieldset>
				</c:forEach>
			</form>
</section>

<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>

<%
String msg="";
if(request.getParameter("url")!=null){
	msg=request.getParameter("url");
}

%>
<sql:query var="grp" dataSource="SMS">SELECT * FROM groupname where uid=<%=currentUser.getUserid()%>; </sql:query>
<sql:query var="msg" dataSource="SMS">SELECT * FROM servicetype where type <> 'video'; </sql:query>
<sql:query var="sender" dataSource="SMS">SELECT * FROM sendername WHERE uid=<%=currentUser.getUserid()%></sql:query>
<sql:query var="template" dataSource="SMS">SELECT * FROM tamplate WHERE istrans=0 AND  uid=<%=currentUser.getUserid()%></sql:query>

<input type="hidden" id="urlid"
	value="<%=request.getParameter("url") %>" />

<link rel="stylesheet" href="css/jquery.tag-editor.css">
<link rel="stylesheet" href="css/jquery.timepicker.css">

<style>
table
{
    width:100%;
}
th{
   padding-left: 5%;
}
table, th, td {
    /* border: 1px solid black; */

    border-collapse: collapse;
}
td {
    padding-left: 15px;
    padding-right: 15px;
}
</style>
<script type="text/javascript">
var dupli=0;
var inval=0;	
   var numbers="";
   
  
  
   
	function formSub() {
		var message = $('#message').val();
		var senderId = $('#senderId').val();
		var stype = $('#stype').val();
		 if($("#grpid").val()==null){
			 alert("No group Selected");
			 return;	
		 }
		 var gid=$("#grpid").val().toString();
		
		var schedule=$('#dtpkr').val()+$('#clockpicker').val();
	var chkstatus=$('#isschedule').is(':checked');
	//alert(chkstatus);
		$.ajax({
			
			url : "sendMessageByGid",
			type : "POST",
			data : {
				'message' : encodeURIComponent(message),
				'senderId' : senderId,
				'serviceid' : stype,
				'gid':gid,'schedule':schedule,'isschedule':chkstatus			
				
			},
			beforeSend : function(response) {

				$('#sendit').attr('disabled', true);
				$.blockUI();
			},
			success : function(response) {
			//	alert(response);
				$('#sendit').attr('disabled', false);
				$("#message").val("");				
				$.unblockUI();					
$.smallBox({
	title : response,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
					});
		
			},
			error : function(xhr, status, error) {
				alert(xhr.responseText);
				$('#sendit').attr('disabled', false);
			}
		});
		return false;
	}
</script>

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-sm-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				
				
				<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Group</strong> <i>Message</i></h2>		
							
				<!-- 	<div class="widget-toolbar" role="menu">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div> -->
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
				
				
				<!-- widget div-->
				<div>
					<!-- widget edit box -->
					<div class="jarviswidget-editbox"></div>

					<div class="widget-body">
						<form class="form-horizontal">

							<fieldset>



								<div class="form-group">
								<table style="width: 100%;" ><thead><tr><th>
								<label class="bld">Group</label>
									
									</th>
									<th style="width: 25%;">
									<label class="bld">Service</label>
									</th>
									<th style="width: 25%;">
									<label class="bld">Sender</label>
									</th>
									<th style="width: 25%;">
									<label class="bld">Template</label>
									</th>
									</tr>
									</thead>
									<tbody>
									<tr> <td>
									
										<select name="grpid" id="grpid" multiple="multiple" style="width: 200px;" >
											<c:forEach var="all" items="${grp.rows}">
												<option value="${all.gid}" >${all.groupname}</option>

											</c:forEach>
										</select>
								
									</td>
									<td>
									
										<select name="stype" id="stype" class="form-control">
											<c:forEach var="all" items="${msg.rows}">
												<option value="${all.stid}"  >${all.type}</option>

											</c:forEach>
										</select>
									
									</td>
									<td>
								

										<select name="senderId" id="senderId" class="form-control">
											<c:forEach var="all" items="${sender.rows}">
												<option value="${all.sendername}" >${all.sendername}</option>
											</c:forEach>
										</select>
								
									
									</td>
									<td>
									
										
										<select name="template" id="template" onchange="changetemplate()" class="form-control">
												<option value=0>-select template-</option>
											<c:forEach var="all" items="${template.rows}">
												<option value="${all.tmessage}" >${all.tname}</option>
											</c:forEach>
										</select>
										
									

									</td>
								</tr>
							
									<!-- </table>
									</div> -->

                         
								<!-- 
								<div class="form-group">
									<label class="col-md-2 control-label">Password field</label>
									<div class="col-md-10">
										<input class="form-control" placeholder="Password field" type="password" value="mypassword">
									</div>
								</div> -->
                                <tr><td colspan="4">
								
									<label class="bld">Message</label>
									

										<textarea id="message" style="font: normal 14px sans-serif;"
											class="form-control" rows="4" maxlength="5000"></textarea>
										<span id="sms-counter" style="float: right;">
											<button type="button" class="btn btn-success  btn-xs">
												SMS Count:&nbsp;&nbsp;<span class="messages"
													style="font-size: 16px;"></span>&nbsp;&nbsp; | <b><span
													class="remaining"></span></b>
										</span>
										</button>
										<!-- SMS Count:<span class="messages"></span> <span style="float: right;" class="remaining" ></span></span> -->


									
								
                                </td>
                                </tr>
                                <tr><td colspan="1">
								
									
									<div class="col-md-1">

										<input type="button" id="sendit" value="SEND"
											onclick="formSub();" class="btn btn-primary btn-sm">
									</div>
								</td>
									
									<td colspan="3">
									<div class="col-md-2" >
										
										<label class="checkbox-inline">
											  <input type="checkbox"  id="isschedule"  name="colorCheckbox"value="red" class="checkbox style-0">
											  <span>Schedule </span>
										</label>
										</div>
									
										  
                   
                    <div class="col-md-2 row red box" style="width: 140px;display: none;" >
									   
									   <input type="text" id="dtpkr"  name="mydate" placeholder="Select a date"  class="form-control datepicker" data-dateformat="dd/mm/yy">
									    </div>
									    <div class="col-md-1 row red box" style="width: 140px;display: none;" >
									     <input type="text" id="clockpicker" name="mydate"  class="form-control" placeholder="Select a Time" >
									   
									   
                                    </div>
                                    	</td>
                                    	</tr>
                                    		</tbody>
                                    	</table>
								</div>

							</fieldset>
						</form>


					</div>

				</div>

			</div>

		</article>

		<!-- end row -->
	</div>

</section>

<style type="text/css">
border-style:solid;
border-color:#287EC7;
</style>
<script type="text/javascript">

function changetemplate(){
	$('#message').append($("#template").val());
  }
	            
loadScript("js/jquery.timepicker.min.js", runClockPicker);

function runClockPicker(){
	
	  $('#clockpicker').timepicker({ 'scrollDefault': 'now' });
	  
	  $("#grpid").select2({  placeholder: "Select Group"});
	  
}



	            $(document).ready(function(){
	            	
	            	
	        		$(".datepicker").datepicker({
	        	//	    defaultDate: "+1y",
	        		     dateFormat: 'dd-mm-yy', 
	        		   // formate:'dd',
	        		  //  changeMonth: true,
	        		//    numberOfMonths: 3,
	        		    prevText: '<i class="fa fa-chevron-left"></i>',
	        		    nextText: '<i class="fa fa-chevron-right"></i>',
	        		    onClose: function (selectedDate) {
	        		    //    $("#to").datepicker("option", "maxDate", selectedDate);
	        		    }
	        	
	        		});
	        		
	        		 $('input[type="checkbox"]').click(function(){
	        	            if($(this).attr("value")=="red"){
	        	                $(".red").toggle('slow');
	        	            }
	        	         
	        	        });
	        		 
	            	$('#message').countSms('#sms-counter');
	        	  /*   $('#numbers').tagEditor({	    	
	        	    	    placeholder: 'Enter Mobile nos ...',
	        	    	    clickDelete:'true',sortable:'false',
	        	            onChange: function(){ $("#totalmobile").html('<button  type="button" class="btn btn-primary  btn-xs">Total Mobile:<b>'+$("#numbers").tagEditor('getTags')[0].tags.length+'</b></button><button  type="button" class="btn btn-warning  btn-xs">Duplicate:<b>'+dupli+'</b></button><button  type="button" class="btn btn-danger  btn-xs">Invalid:<b>'+inval+"</b></buttton>");	            
	        	            },});     */        	
	            	
	            });

</script>





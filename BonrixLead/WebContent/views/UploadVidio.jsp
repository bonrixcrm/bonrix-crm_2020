


<%@include file="/includes/SessionCheck.jsp"%>
<section id="widget-grid" class="">

	<div class="row">

		<article class="col-sm-12">
			
			
			<p>
				<span class="label label-warning">
				NOTE</span> &nbsp; This plugins works only on Latest Chrome, Firefox, Safari, Opera &amp; Internet Explorer 10.
			</p>
			
			<div class="jarviswidget jarviswidget-color-blueLight" id="wid-id-0" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-cloud"></i> </span>
					<span id="loader"></span>
					<h2>Upload your Media here! </h2>
                         
				</header>

				<div>

					<div class="jarviswidget-editbox">

					</div>

					<div class="widget-body">


                                  <form  id="mydropzone" class="dropzone"
                                         enctype="multipart/form-data" action="UploadMedia" method="POST">
                                         </form>


					</div>

				</div>

			</div>

		</article>

	</div>



		<style>
			.s2 {
				color: #D14;
			}

			.c1 {
				color: #998;
				font-style: italic;
			}

			.mi {
				color: #099;
			}
		</style>


			
				</section>			





		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
		


<script type="text/javascript">


	pageSetUp();
	var pagefunction = function() {
		
		 
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 $('#remoteModal').on('hidden.bs.modal', function () {
			});
		 
			
		 
		Dropzone.autoDiscover = false;
		$("#mydropzone").dropzone({
			//url: "/file/post",
		//	addRemoveLinks : true,
			maxFilesize: 5000,
			dictResponseError: 'Error uploading file!',
			 init: function() {
				    this.on("success", function(file, responseText) {
				      // Handle the responseText here. For example, add the text to the preview element:
          //var removeButton = Dropzone.createElement("<button>Remove file2222</button>");
				    	//  var html2=$(responseText).val();
				    //	alert(responseText[0]);
				    	$("#loader").html("<b>Hurray! You have Successfully Uploaded File!  Orginal Size:<font class='btn btn-danger btn-sm'>"+bytesToSize(responseText[2])+"</font> Converted Size:<font class='btn btn-success btn-sm'>"+bytesToSize(responseText[1])+"</font></b>");
				    	
						//$("#message").val($("#message").val()+responseText);
						
		 $('#remoteModal').modal({remote:'./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+responseText[0],show:'true'});
		 
				    	  
				      file.previewTemplate.appendChild(document.createTextNode(responseText));
				    }),
				    this.on("addedfile", function(file) {
				     // alert(file[0]);
				     $("#loader").html("<b><img height=30 width=30 src='img/aj     ax-loader.gif'> Please Wait While We cooking your File! It may takes few minuts </b>");
				      });
				    
				  }
			
			
			
		});
		
		
	}
	
	
	function bytesToSize(bytes, precision)
	{  
	    var kilobyte = 1;
	    var megabyte = kilobyte * 1024;
	    var gigabyte = megabyte * 1024;
	    var terabyte = gigabyte * 1024;
	   
	    if ((bytes >= 0) && (bytes < kilobyte)) {
	        return bytes + ' B';
	 
	    } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
	        return (bytes / kilobyte).toFixed(precision) + ' KB';
	 
	    } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
	        return (bytes / megabyte).toFixed(precision) + ' MB';
	 
	    } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
	        return (bytes / gigabyte).toFixed(precision) + ' GB';
	 
	    } else if (bytes >= terabyte) {
	        return (bytes / terabyte).toFixed(precision) + ' TB';
	 
	    } else {
	        return bytes + ' B';
	    }
	}
	loadScript("js/plugin/dropzone/dropzone.min.js", pagefunction);

</script>

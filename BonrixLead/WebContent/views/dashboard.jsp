<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<sql:query var="msg" dataSource="SMS">
                     SELECT u.username,u.uid FROM users u WHERE u.parentid=<%=currentUser.getUserid()%> OR u.uid=<%=currentUser.getUserid()%>	</sql:query>

<div class="row" >
	<div class="col-xs-8 col-sm-5 col-md-6 col-lg-3">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-bar-chart-o fa-fw "></i> 
			Sent SMS Summary
		</h1>
	</div>
									<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
									 <select name="uid" id="uid" onchange="cu()" class="col-md-10">										
	<%if(currentUser.getUserid()==1){%><option value="0">--ALL--</option><%}%><c:forEach var="all" items="${msg.rows}"><option value=${all.uid} >${all.username}</option></c:forEach></select>
									</div>
									<div id="reportrange" class="btn bg-color-red txt-color-white no-border pull-right" >
          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i><span></span> <b class="caret"></b></div>
</div>
<section id="widget-grid" class="">
	<div class="row" id="sentmsg">
	        	  <div class="col-xs-12 col-sm-6 col-md-3"> <div class="panel panel-purple text-align-center"> <div class="panel-heading"> <h3 class="panel-title"> TOTAL</h3> </div> <div class="panel-footer text-align-center" > <h1 id="totalt"> 0</h1> </div> </div> </div>
		          <div class="col-xs-12 col-sm-6 col-md-2"> <div class="panel panel-green text-align-center"> <div class="panel-heading"> <h3 class="panel-title"> DELIVERD</h3> </div> <div class="panel-footer text-align-center" > <h1 id="delivrdt"> 0<span class="subscript"></span></h1> </div> </div> </div>
		          <div class="col-xs-12 col-sm-6 col-md-2"> <div class="panel panel-primary text-align-center"> <div class="panel-heading"> <h3 class="panel-title"> SUBMITED</h3> </div> <div class="panel-footer text-align-center" > <h1 id="submitedt"> 0</h1> </div> </div> </div>
		          <div class="col-xs-12 col-sm-6 col-md-2"> <div class="panel panel-red text-align-center"> <div class="panel-heading"> <h3 class="panel-title"> DND/REJECTED</h3> </div> <div class="panel-footer text-align-center" > <h1 id="rejectedt"> 0</h1> </div> </div> </div>
		<div class="col-xs-12 col-sm-6 col-md-3"><div class="panel panel-pink text-align-center"><div class="panel-heading"><h3 class="panel-title">TODAY'S</h3></div><div class="panel-footer text-align-center"><h1 id="todayt">0</h1></div></div></div>
	</div>
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
					<h2>Curve Graph</h2>
				</header>			
				<div>
					<div class="jarviswidget-editbox">
						<input type="text">
					</div>
					<div class="widget-body no-padding">
						<div id="sales-graph" class="chart no-padding"></div>
					</div>
				</div>
			</div>
		</article>
	</div>
	
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
					<h2>Bar Graph</h2>
				</header>			
				<div>
					<div class="jarviswidget-editbox">
						<input type="text">
					</div>
					<div class="widget-body no-padding">
						<div id="bgraph" class="chart no-padding"></div>
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="row">
	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
					<h2>Year Graph</h2>				
				</header>
				<div>
					<div class="jarviswidget-editbox">
					</div>
					<div class="widget-body no-padding">
						<div id="year-graph" class="chart no-padding"></div>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>

<script type="text/javascript">
var sd=moment().subtract(7,"days").format("YYYY-MM-DD");
var ed=moment().format("YYYY-MM-DD");
function reinitrange(){
	$("#uid").select2();
	var optionSet2={startDate:moment(),endDate:moment(),minDate:moment().subtract(24,"month"),maxDate:moment(),dateLimit:{days:365},opens:"left",ranges:{Today:[moment(),moment()],Yesterday:[moment().subtract(1,"days"),moment().subtract(1,"days")],"Last 7 Days":[moment().subtract(6,"days"),moment()],"Last 30 Days":[moment().subtract(29,"days"),moment()],"This Month":[moment().startOf("month"),moment().endOf("month")],"Last Month":[moment().subtract(1,"month").startOf("month"),moment().subtract(1,"month").endOf("month")]}};
	$("#reportrange span").html(moment().subtract(7,"days").format("MMMM D, YYYY")+" - "+moment().format("MMMM D, YYYY"));$("#reportrange").daterangepicker(optionSet2);$("#reportrange").on("apply.daterangepicker",function(b,a){ sd=a.startDate.format("YYYY-MM-DD"); ed=a.endDate.format("YYYY-MM-DD"); $("#reportrange span").html(a.startDate.format("MMMM D, YYYY")+" - "+a.endDate.format("MMMM D, YYYY")); pagefunction(sd,ed);});
}
function cu(){pagefunction(sd,ed);}
	pageSetUp();	
	var pagefunction = function(start,end) {
		var data="";	
		 $("#totalt").html('<i class="fa fa-refresh fa-spin"></i>');		 
		 $("#submitedt").html('<i class="fa fa-refresh fa-spin"></i>');
		 $("#rejectedt").html('<i class="fa fa-refresh fa-spin"></i>');
		 $("#delivrdt").html('<i class="fa fa-refresh fa-spin"></i>');
		 $("#todayt").html('<i class="fa fa-refresh fa-spin"></i>');		 
		$.getJSON("getSentSMSbyUid?start="+start+"&end="+end+"&uid="+$("#uid").val(),function(msg2){
		var	msg=eval(msg2);
		var ht="",dlvrd=0,rejectedt=0,totalt=0,submited=0,todayt=0;			
			 $.each(msg, function(index, element) {
				 totalt+=parseInt(element.TOTAL);
				 rejectedt+=parseInt(element.REJECTED);
				dlvrd+=parseInt(element.DELIVRD);
				submited+=parseInt(element.SUBMITED);
			      if(element.DATE==moment().format("YYYY-MM-DD")){
			    	  todayt+=parseInt(element.DELIVRD)+parseInt(element.SUBMITED)+parseInt(element.REJECTED); 	  
			      }	
			    });
			 $("#totalt").html(totalt);			 
			 $("#submitedt").html(submited);
			 $("#rejectedt").html(rejectedt);
			 $("#delivrdt").html(dlvrd);
			 $("#todayt").html(todayt);
			var mday=moment();
			Morris.Area({
				element : 'sales-graph',			
				data:msg,
				xkey : 'DATE',
				ykeys : ['SUBMITED','DELIVRD','REJECTED'],
				lineColors:['#075FB0','#11A42F','#C40D2E'],
				labels : ['SUBMITED','DELIVRD','REJECTED'],
				pointSize : 1,
				hideHover : 'always'
			});
			Morris.Bar({
				element : 'bgraph',			
				data:msg,
				xkey : 'DATE',
				ykeys : ['SUBMITED','DELIVRD','REJECTED'],
				barColors:['#075FB0','#11A42F','#C40D2E'],
				labels : ['SUBMITED','DELIVRD','REJECTED'],
				pointSize : 1,
				hideHover : 'false',
				
			});
		Morris.Line({
				  element: 'year-graph',
				  data: msg,
				  xkey: 'DATE',
				  ykeys : ['SUBMITED','DELIVRD','REJECTED'],
				  lineColors:['#075FB0','#11A42F','#C40D2E'],
					labels : ['SUBMITED','DELIVRD','REJECTED']
				});
		
			
		});
	};
	loadScript("js/plugin/morris/raphael.min.js", function(){
		loadScript("js/daterangepicker.js", function(){	
			reinitrange();
		loadScript("js/plugin/morris/morris.min.js", pagefunction(moment().subtract(6, 'days').format("YYYY-MM-DD"), moment().format("YYYY-MM-DD")));
		});
	});
</script>

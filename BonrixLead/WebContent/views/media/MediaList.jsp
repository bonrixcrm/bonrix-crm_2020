	<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/includes/SessionCheck.jsp" %>
	

	
			
		<!-- MODAL PLACE HOLDER -->
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
		
	



<!-- widget grid -->
<section id="widget-grid" class="">

	
			<!-- end widget -->

			<!-- Widget ID (each widget will need unique ID)-->

			<!-- end widget -->

		

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
				Uploaded Media Detail

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr role="row">
								<th data-hide="phone" class="sorting_asc" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-sort="ascending" aria-label="ID: activate to sort column ascending" style="width: 27px;">ID</th>
								<th data-class="expand" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" >Name</th>
								<th class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-label="Phone: activate to sort column ascending" >Type</th>
								<th data-hide="phone" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1"  >Original  Size</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Converted Size</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">TinyUrl</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Uploaded On</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Hit count</th>
								<th></th>
								
								</tr>
							</thead>
	
							
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">


var responsiveHelper_dt_basic = undefined;
var responsiveHelper_datatable_fixed_column = undefined;
var responsiveHelper_datatable_col_reorder = undefined;
var responsiveHelper_datatable_tabletools = undefined;

var mediatable=null;


	pageSetUp();
	
		function reloadtable(){
			
			  mediatable.ajax.reload();		
		}
	var pagefunction = function() {
		 
		 
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
		//  alert("sdgsd");
		//  $('#datatable_tabletools').dataTable.ajax.reload();
			reloadtable();
			//$route.reload()
			});
			
		
		
			
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};

	    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

	    

	};

	
	// load related plugins
	
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
			loadScript("js/plugin/datatables/dataTables.tableTools.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
					
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
			
				});
			});
		});
	});

	
	function getdata(){
		
		 if (mediatable)
			 mediatable.fnDestroy();
//		alert("getdata");
	      mediatable=	$('#datatable_tabletools').DataTable({
	        "processing": false,
	        "serverSide": false,
	        "bServerSide": false, 
	        "order": [[ 6, "desc" ]],
	        "bdestroy": true,
	        
	      //  "bDestroy": true,
	        "ajax": "getMediaListbyUid", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	            $('td:eq(5)', nRow).html("<a target='_blank' href='"+aData[5]+"'>"+aData[5]+"</a>");
	            $('td:eq(8)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Detail</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');
	            
	        },
	 /*        
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>", */
	   /*      "oTableTools": {
	        	 "aButtons": [
	             "copy",
	             "csv",
	             "xls",
	                {
	                    "sExtends": "pdf",
	                    "sTitle": "SmartSMS_PDF",
	                    "sPdfMessage": "SmartSMS PDF Export",
	                    "sPdfSize": "letter"
	                },
	             	{
	                	"sExtends": "print",
	                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
	            	}
	             ],
	            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
	        }, */
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
		
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
		
		
	}

</script>


<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>

<%
	String msg = "";
	if (request.getParameter("url") != null) {
		msg = request.getParameter("url");
	}
%>
<sql:query var="msg" dataSource="SMS">SELECT * FROM servicetype where isvisible=1 AND stid in (select stid from smscredit where uid=<%=currentUser.getUserid()%>); </sql:query>
<sql:query var="sender" dataSource="SMS">SELECT * FROM sendername WHERE uid=<%=currentUser.getUserid()%></sql:query>
<sql:query var="template" dataSource="SMS">SELECT * FROM tamplate WHERE istrans=0 AND  uid=<%=currentUser.getUserid()%></sql:query>

<input type="hidden" id="urlid"
	value="<%=request.getParameter("url")%>" />


<script type="text/javascript">
var dupli = 0, inval = 0, numbers = "";
function formSub() {
  numbers = $("#numbers").tagEditor("getTags")[0].tags;
  var a = $("#message").val(), d = $("#senderId").val(), b = $("#stype").val(), e = $("#dtpkr").val() + $("#clockpicker").val();
  if ("" == numbers) {
    alert("Enter Mobile number");
  } else {
    if ("0" == b) {
      alert("Select Service Type");
    } else {
      if ("" == a) {
        alert("Enter message");
      } else {
        return $.ajax({url:"sendMessage", type:"POST", data:{message:encodeURIComponent(a), numbers:numbers.toString(), senderId:d, serviceid:b, schedule:e}, beforeSend:function(c) {
          $("#sendit").attr("disabled", !0);
          $.blockUI();
        }, success:function(c) {
          $("#sendit").attr("disabled", !1);
          $("#message").val("");
          $("#numbers").tagEditor("destroy");
          inval = dupli = 0;
          $("#numbers").val("");
          inittags();
          $.unblockUI();
          var clr="#296191";
         if(c==1){ 
          $.smallBox({title:getErrormessage(c), content:"<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>", color:"#296191", iconSmall:"fa fa-thumbs-up bounce animated", timeout:4E3});
         }else{
        	 $.bigBox({	title : "Some Error",content : getErrormessage(c),color : "#C46A69",icon : "fa fa-warning shake animated",number : "1",timeout : 12000
 			});
	 
         }
        }, error:function(c, a, b) {
          $("#sendit").attr("disabled", !1);
        }}), !1;
      }
    }
  }
}
;

function getErrormessage(ec){

	if(ec=="1"){
	return "Message Send SuccessFully";
	}else if(ec=="-1"){
	return "Message Can't Blank";
	}else if(ec=="-2"){
	return "Message Not Sent !Enter Valid Number  !";
	}else if(ec=="-3"){
	return "Message Not Match With Template";
	}else if(ec=="-4"){
	return "insufficiant credit";
	}else{
	return ec;
	}
	}
</script>

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-sm-12">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i>
					</span>
					<h2>Quick Message</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox"></div>

					<div class="widget-body">
						<form class="form-horizontal">
							<fieldset>
								<div class="form-group">
									<label class="col-md-2 control-label bld">Mobile
										Numbers:</label>
									<div class="col-md-9">
										<textarea rows="4" class="form-control" name="numbers"
											id="numbers" placeholder="Mobile Numbers"></textarea>
										<div id="totalmobile" class="btn-group" style="float: right;">
											<button type="button" class="btn btn-primary  btn-xs">
												Total Mobile:<b>0</b>
											</button>
											<button type="button" class="btn btn-warning  btn-xs">
												Duplicate:<b>0</b>
											</button>
											<button type="button" class="btn btn-danger  btn-xs">
												Invalid:<b>0</b>
												</buttton>
										</div>
										<div class="note">
											<strong>Note:</strong> it automatically remove duplicate
											numbers.right click or CTRL + click for remove any number.
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label bld">Service:</label>
									<div class="col-md-2">
										<select name="stype" id="stype" class="form-control">
											<option value=0>-select service-</option>
											<c:forEach var="all" items="${msg.rows}">
												<option value=${all.stid}>${all.type}</option>

											</c:forEach>
										</select>
									</div>
									<label class="col-md-1 control-label bld"  style="width:50px; margin-left: 0px;margin-right: 0px;padding-left: 0px;padding-right: 0px;">Sender:</label>
									<div class="col-md-2"  >

										<select name="senderId" id="senderId" class="form-control">
											<!-- <option value=0 >INFORM</option> -->
											<c:forEach var="all" items="${sender.rows}">
												<option value="${all.sendername}"
													${all.sid == cookie.dsender.value ? 'selected' : ''}>${all.sendername}</option>
											</c:forEach>
										</select>
										<!-- 	<p class="note"><strong>Note:</strong>Sender Name not valid for Promotional SMS.</p> -->
									</div>
								<label class="col-md-2 control-label bld"  style="width:75px; margin-left: 0px;margin-right: 0px;padding-left: 1px;padding-right: 1px;">Template:</label>
										<div class="col-md-2" >
										
										<select name="template" id="template" onchange="changetemplate()" class="form-control">
											<!-- <option value=0 >INFORM</option> -->
												<option value=0>-select template-</option>
											<c:forEach var="all" items="${template.rows}">
												<option value="${all.tmessage}" >${all.tname}</option>
											</c:forEach>
										</select>
										
										</div>
						<div class="col-md-2" style="width: 85px;margin: 0px;padding: 0px;">
										
										<label class="checkbox-inline">
											  <input type="checkbox" name="colorCheckbox" value="red" class="checkbox style-0">
											  <span>Schedule </span>
										</label>
										</div>
									
								</div>
								<div class="form-group">
								<label class="col-md-3 control-label bld"></label>
                               
                               <div class="col-md-2 row red box" style="display: none;" >
										
									
									   
									   <input type="text" id="dtpkr"  name="mydate" placeholder="Select a date"  class="form-control datepicker" data-dateformat="dd/mm/yy">
									    </div>
									    <div class="col-md-2 row red box" style="display: none;" >
									     <input type="text" id="clockpicker" name="mydate"  class="form-control" placeholder="Select a time" >
									   
                                    </div>
                               </div>
								<div class="form-group">
									<label class="col-md-2 control-label bld">Message</label>
									<div class="col-md-9">

										<textarea id="message" style="font: normal 14px sans-serif;"
											class="form-control" rows="4" maxlength="5000"><%=msg%></textarea>
										<span id="sms-counter" style="float: right;">
											<button type="button" class="btn btn-success  btn-xs">
												SMS Count:&nbsp;&nbsp;<span class="messages"
													style="font-size: 16px;"></span>&nbsp;&nbsp; | <b><span
													class="remaining"></span></b>
										
										</button>
										</span>
										<!-- SMS Count:<span class="messages"></span> <span style="float: right;" class="remaining" ></span></span> -->


									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label"></label>
									<div class="col-md-7">

										<input type="button" id="sendit" value="SEND"
											onclick="formSub();" class="btn btn-primary btn-sm">
									</div>
								</div>

							</fieldset>
						</form>


					</div>

				</div>

			</div>

		</article>

	</div>

</section>

<style type="text/css">
border-style:solid;
border-color:#287EC7;
</style>
<script type="text/javascript">

loadScript("js/plugin/clockpicker/clockpicker.min.js", runClockPicker);

function runClockPicker(){
	$('#clockpicker').clockpicker({
		placement: 'top',
	    donetext: 'Done'
	});
}
function changetemplate(){
	$('#message').append($("#template").val());
  }
$(document).ready(function() {
		$(".datepicker").datepicker({		    
		     dateFormat: 'dd-mm-yy', 
		    prevText: '<i class="fa fa-chevron-left"></i>',
		    nextText: '<i class="fa fa-chevron-right"></i>',
		    onClose: function (selectedDate) {

		    }
	
		});
		
		 $('input[type="checkbox"]').click(function(){
	            if($(this).attr("value")=="red"){
	                $(".red").toggle('slow');
	            }
	         
	        });
		
		inittags();
		$('#message').countSms('#sms-counter');
		
		
		
	});

	function inittags() {
		$('#numbers')
				.tagEditor(
						{
							placeholder : 'Enter Mobile nos ...',
							clickDelete : 'true',
							sortable : 'false',
							beforeTagSave : function(field, editor, tags, tag, val) {
								 if (val.length!=10 && val.length!=12) {
									 $('#numbers').tagEditor('removeTag', val);
								 }
							},
							onChange : function() {
								$("#totalmobile")
										.html(
												'<button  type="button" class="btn btn-primary  btn-xs">Total Mobile:<b>'
														+ $("#numbers")
																.tagEditor(
																		'getTags')[0].tags.length
														+ '</b></button><button  type="button" class="btn btn-warning  btn-xs">Duplicate:<b>'
														+ dupli
														+ '</b></button><button  type="button" class="btn btn-danger  btn-xs">Invalid:<b>'
														+ inval
														+ "</b></buttton>");
							},
						});

	}
</script>





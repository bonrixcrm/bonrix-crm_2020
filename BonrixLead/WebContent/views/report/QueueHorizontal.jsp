
    <link href="css/nv.d3.css" rel="stylesheet" type="text/css">
  <!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js" charset="utf-8"></script>
    <script src="js/nv.d3.min.js"></script> -->
    

    <style>
        text {
            font: 12px sans-serif;font-weight: bolder;
        }
        svg {
            display: block;
        }
        #chart1, svg {
            margin: 0px;
            padding: 3px;
            margin:auto;
            height: 100%;
            width: 98%;
        }
    </style>


<section id="widget-grid" class="">

				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
			
				<header>
			SMS Channel Queue

				</header>

				<div>

				
					<div class="jarviswidget-editbox">
					
					</div>
				
					<div class="widget-body no-padding">
					<div id="chart1" class='with-3d-shadow with-transitions' style="height: 400px;">
    <svg></svg>
</div>
					</div>
					
					</div>
					
					</div>
					
					</section>




<script>

loadScript("https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js", function(){
	loadScript("js/nv.d3.min.js", function(){
	
		getdata();
		 
	//	 setTimeout("alert('Hi')", 1000);

	});
	});

      $(document).ready(function(){
    	  grafint= setInterval(getdata, 2000); 
      });

 function getdata(){

	 
	
	 $.get("getChannelSmsQueue",function(data){
		data=eval(data);
		  nv.addGraph(function() {
		    var chart = nv.models.multiBarHorizontalChart()
		        .x(function(d) { return d.label })
		        .y(function(d) { return d.value })
		        .margin({top: 30, right: 20, bottom: 50, left: 175}).barColor(d3.scale.category20().range())
		        .showValues(true)           //Show bar value next to each bar.
		        .tooltips(true)             //Show tooltips on hover.
		        .duration(350)
		        .showControls(true);        //Allow user to switch between "Grouped" and "Stacked" mode.

		    chart.yAxis
		        .tickFormat(d3.format(',.2f'));

		    d3.select('#chart1 svg')
		        .datum(data)
		        .call(chart);

		    nv.utils.windowResize(chart.update);

		    return chart;
		  });
		});
	
	
}
</script>


<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/includes/SessionCheck.jsp"%>
<sql:query var="msg" dataSource="SMS">SELECT * FROM servicetype where isvisible=1 AND stid in (select stid from smscredit where uid=<%=currentUser.getUserid()%>); </sql:query>


<link rel="stylesheet" type="text/css" href="css/rangeselect.css"
	media="screen" />
<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>

<style type="text/css">
table.dataTable tbody tr.selected2 {
  background-color: #B0BED9;
}
</style>
<input type="hidden" id="sdate"  />
<input type="hidden" id="edate" />
<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-slog" data-widget-editbutton="false">
			
					<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Scheduled</strong> <i>SMS Log</i></h2>		
					<div class="widget-toolbar" role="menu">
					<button id=deletebutton class="btn btn-primary"> Delete </button>
					</div>		
				
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

				<div>

				
					<div class="jarviswidget-editbox">
					
					</div>
				
					<div class="widget-body no-padding">
				
				<table id="sent_messages"
					class="table table-striped table-bordered table-hover" width="100%">
					<thead>
						<tr role="row">
							<th>ID</th>	
														
							<th >Mobile no</th>
							<th>Message</th>
							<th>ScheduleDate</th>
							<th >SenderName</th>							
							
							
							<th >Service</th>		
								<th>Credit</th>						
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</section>



<script type="text/javascript">

var mobile=$('#mobile').val();
var	fdate	=$('#fdate').val();
var	todate	=$('#todate').val();
var stype="0";
var mediatable=null;
	pageSetUp();	
	function dtpicker(){
		    reinitrange();
	}
	function reloadtable(){		
		 mobile=$('#mobile').val();
		fdate	=$('#sdate').val();
		todate	=$('#edate').val();
		stype  =$('#stype').val();
	   mediatable.ajax.reload();		
	}  
	
	
	function deletequeue(ids){
		
		$.get("DeleteQueueById?msgid="+ids,function(scc){
			alert(scc);
			reloadtable();
		});
		
	}
	
	function reinitrange(){
      	var optionSet2={startDate:moment(),endDate:moment(),minDate:moment(),maxDate:moment().add(24,"month"),dateLimit:{days:365},opens:"left",ranges:{Today:[moment(),moment()],Tomorrow:[moment().add(1,"days"),moment().add(1,"days")],"Next 7 Days":[moment(),moment().add(6,"days")],"Next 30 Days":[moment(),moment().add(29,"days")],"This Month":[moment().startOf("month"),moment().endOf("month")],"Next Month":[moment().add(1,"month").startOf("month"),moment().add(1,"month").endOf("month")]}};
    	$("#reportrange span").html(moment().subtract(7,"days").format("MMMM D, YYYY")+" - "+moment().add(7,"days").format("MMMM D, YYYY"));$("#reportrange").daterangepicker(optionSet2);$("#reportrange").on("apply.daterangepicker",function(b,a){  
            $("#sdate").val(a.startDate.format('DD-MM-YYYY'));
            $("#edate").val(a.endDate.format('DD-MM-YYYY'));    
            $("#reportrange span").html(a.startDate.format("MMMM D, YYYY")+" - "+a.endDate.format("MMMM D, YYYY"));
            reloadtable();});
		
	}
	var pagefunction = function() {

		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 $("#sdate").val(moment().format("DD-MM-YYYY"));
		 $("#edate").val(moment().add(7, 'days').format("DD-MM-YYYY"));
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			 reloadtable();
			});
		 	var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	};
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){					
				loadScript("js/daterangepicker.js",pagefunction)					
		});
	});
	function getdata(){
		fdate	=$('#sdate').val();
		todate	=$('#edate').val();
		
		
	      mediatable=	$('#sent_messages').DataTable({
	        "processing": false,
	        "serverSide": true,
	        "pageLength": 10,
	        "ordering": false,
	       // "bdestroy": true,
	         "aoColumns": [ {
                "className": 'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },null,null,null,null,null,null], 
	        "ajax": {"url":"getScheduleMessagebyUid",	      
	        "data": function ( d ) {
              d.m =mobile;
              d.fdate=fdate;
              d.todate=todate;
              d.stype=stype;
          }
	        }
	      , "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	   
	      },
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
				 var $cell=$('td:eq(2)', nRow);
                 $cell.text(ellipsis($cell.text(),100));
    		},
			"drawCallback" : function(oSettings) {
		
			}
		});
	      
	       $('#sent_messages tbody').on( 'click', 'tr', function () {
	          $(this).toggleClass('selected2');
	      } ); 
	        $('#sent_messages tbody').on('click', 'td.details-control', function () {
	          var tr = $(this).closest('tr');
	     
	          var row = mediatable.row( tr );
	   
	          if ( row.child.isShown() ) {
	              // This row is already open - close it
	              row.child.hide();
	              tr.removeClass('shown');
	          }
	          else { 
	              // Open this row
	              row.child( format(row.data()) ).show();
	              tr.addClass('shown');
	          }
	      } ); 
	        $('#deletebutton').click( function () {
	        	var ds=mediatable.rows('.selected2').data();
	        	var points = new Array();   
	        	
	        	for(var m=0;m<ds.length;m++){
	 //alert(ds[m].toString().split(",")[0] +' row(s) selected' );
	  points.push(ds[m].toString().split(",")[0]);
	        	}
	        	alert(points.toString());
	        	deletequeue(points.toString());
	        } );
	    
  var htm='<form class="form-inline"  role="form">'+
     '<div class="form-group"><div class="col-md-3">'
        +'<input type="text" id="mobile"  placeholder="Mobile" class="form-control"></div>'
     //+'<input type="text" id="fdate" style="width:100px;padding-left:1px;margin-left:1px;"  placeholder="From Date" class="dtpicker form-control"><input style="width:100px;padding-left:1px;margin-left:1px;" type="text" id="todate"  placeholder="To Date" class="dtpicker form-control">'
       //  +'<label for="email" class="glyphicon glyphicon-search" rel="tooltip" title="Mobile"></label>'
    // +'<span class="input-group-btn"><button class="btn btn-success" onclick="reloadtable();" type="button">Search</button></span></div>';
     htm+='<div class="col-md-4"><select name="stype" id="stype" class="form-control"><option value=0>select service</option>'
				+'<c:forEach var="all" items="${msg.rows}"><option value=${all.stid}>${all.type}</option></c:forEach></select></div>';
		
	htm+='<div class="col-md-4"><button class="btn btn-success" onclick="reloadtable();" type="button">Search</button></div></div>';			
				htm+='<div id="reportrange" class="pull-right btn bg-color-red txt-color-white no-border" >'
          +'<i class="glyphicon glyphicon-calendar fa fa-calendar"></i><span></span> <b class="caret"></b></div></form>';
      
          
          
   
 $(".dataTables_filter").html(htm);
 dtpicker();   
	}

	function format(a){return'<table class="grid_final"><tr><th style="width:50px;">Id:</th><td>'+a[0]+"</td></tr><tr><th>Mobile:</th><td>"+a[1]+"</td></tr><tr><th>Message:</th><td>"+a[2].replace(/(?:\r\n|\r|\n)/g, '<br />')+"</td></tr><tr><th>Length:</th><td>"+a[2].length+"(<b>"+a[10]+" Credit</b>)</td></tr><tr><th>SentOn:</th><td>"+a[5]+"</td></tr><tr><th>Service:</th><td>"+a[8]+"</td></tr><tr><th>SenderName:</th><td>"+a[9]+"</td></tr></table>"};
	
	function ellipsis(text, n) {
	    if(text.length>n)
	        return text.substring(0,n)+"...";
	    else
	        return text;
	}
	
	function htmlEscape(str) {
	    var ss= String(str)
	            .replace(/&/g, '&amp;')
	            .replace(/"/g, '&quot;')
	            .replace(/'/g, '&#39;')
	            .replace(/</g, '&lt;')
	            .replace(/>/g, '&gt;');
	    alert(ss);
	    return ss;
	}

</script>


<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/includes/SessionCheck.jsp"%>
<sql:query var="msg" dataSource="SMS">SELECT * FROM servicetype where isvisible=1 AND stid in (select stid from smscredit where uid=<%=currentUser.getUserid()%>); </sql:query>


<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>
<input type="hidden" id="sdate"  />
<input type="hidden" id="edate" />
<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-slog" data-widget-editbutton="false">
			
					<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Sent</strong> <i>SMS Log</i></h2>		
							
				
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

				<div>

				
					<div class="jarviswidget-editbox">
					
					</div>
				
					<div class="widget-body no-padding">
				
				<table id="sent_messages"
					class="table table-striped table-bordered table-hover" width="100%">
					<thead>
						<tr role="row">
							<th style="width: 27px;">ID</th>
							<th></th>
							<th>Message</th>
							<th style="width: 10%;">Mobile no</th>
							<th></th>
							<th style="width: 14%;">Sent On</th>
							<th>Status</th>
							<th style="width: 8%;">Status</th>
							<th style="width: 8%;">Service</th>
							<th style="width: 7%;">SenderName</th>
							<th style="width: 3%;">Credit</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</section>



<script type="text/javascript">

var mobile=$('#mobile').val();
var	fdate	=$('#fdate').val();
var	todate	=$('#todate').val();
var stype="0";
var isinit=null;
var sentsmslogtable=null;
	pageSetUp();	
	
	function reloadtable(){		
		 mobile=$('#mobile').val();
		fdate	=$('#sdate').val();
		todate	=$('#edate').val();
		stype  =$('#stype').val();
	   sentsmslogtable.ajax.reload();		
	}
	
	
	function reinitrange(){
		
      	var optionSet2={startDate:moment(),endDate:moment(),minDate:moment().subtract(24,"month"),maxDate:moment(),dateLimit:{days:365},opens:"left",ranges:{Today:[moment(),moment()],Yesterday:[moment().subtract(1,"days"),moment().subtract(1,"days")],"Last 7 Days":[moment().subtract(6,"days"),moment()],"Last 30 Days":[moment().subtract(29,"days"),moment()],"This Month":[moment().startOf("month"),moment().endOf("month")],"Last Month":[moment().subtract(1,"month").startOf("month"),moment().subtract(1,"month").endOf("month")]}};
    	$("#reportrange span").html(moment().subtract(7,"days").format("MMMM D, YYYY")+" - "+moment().format("MMMM D, YYYY"));$("#reportrange").daterangepicker(optionSet2);$("#reportrange").on("apply.daterangepicker",function(b,a){  
            $("#sdate").val(a.startDate.format('DD-MM-YYYY'));
            $("#edate").val(a.endDate.format('DD-MM-YYYY'));    
            $("#reportrange span").html(a.startDate.format("MMMM D, YYYY")+" - "+a.endDate.format("MMMM D, YYYY"));
            reloadtable();});
    	isinit="sdgsdag";
		
	}
	var pagefunction = function() {

		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 $("#edate").val(moment().format("DD-MM-YYYY"));
		 $("#sdate").val(moment().subtract(7, 'days').format("DD-MM-YYYY"));
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			 reloadtable();
			});
		 	var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	};
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){					
				loadScript("js/daterangepicker.js",pagefunction)					
		});
	});
	function getdata(){
		fdate	=$('#sdate').val();
		todate	=$('#edate').val();
		
		
	      sentsmslogtable=	$('#sent_messages').DataTable({
	        "processing": false,
	        "serverSide": true,
	        "pageLength": 10,
	        "ordering": false,
	       // "bdestroy": true,
	        "aoColumns": [ {
                "className": 'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },{"bVisible": false},null,null,{"bVisible": false},null,{"bVisible": false},null,null,null,null],
	        "ajax": {"url":"getSentMessagebyUid",	      
	        "data": function ( d ) {
              d.m =mobile;
              d.fdate=fdate;
              d.todate=todate;
              d.stype=stype;
          }
	        }
	      
	      , "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	       
	    	  $('td:eq(0)',nRow).html(iDataIndex+1);
	      },

			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
				 var $cell=$('td:eq(1)', nRow);
                 $cell.text(ellipsis($cell.text(),100));
    		},
			"drawCallback" : function(oSettings) {
			//	alert("adsgasdg");
				if(isinit==null){
				reinitrange();
				}
			}
		});
	      
	      $('#sent_messages tbody').on('click', 'td.details-control', function () {
	          var tr = $(this).closest('tr');
	     
	          var row = sentsmslogtable.row( tr );
	   
	          if ( row.child.isShown() ) {
	              // This row is already open - close it
	              row.child.hide();
	              tr.removeClass('shown');
	          }
	          else {
	              // Open this row
	              row.child( format(row.data()) ).show();
	              tr.addClass('shown');
	          }
	      } );
	    
	    
  var htm='<form class="form-inline"  role="form">'+
     '<div class="form-group"><div class="col-md-3">'
        +'<input type="text" id="mobile"  placeholder="Mobile" class="form-control"></div>'
     //+'<input type="text" id="fdate" style="width:100px;padding-left:1px;margin-left:1px;"  placeholder="From Date" class="dtpicker form-control"><input style="width:100px;padding-left:1px;margin-left:1px;" type="text" id="todate"  placeholder="To Date" class="dtpicker form-control">'
       //  +'<label for="email" class="glyphicon glyphicon-search" rel="tooltip" title="Mobile"></label>'
    // +'<span class="input-group-btn"><button class="btn btn-success" onclick="reloadtable();" type="button">Search</button></span></div>';
     htm+='<div class="col-md-4"><select name="stype" id="stype" class="form-control"><option value=0>select service</option>'
				+'<c:forEach var="all" items="${msg.rows}"><option value=${all.stid}>${all.type}</option></c:forEach></select></div>';
		
	htm+='<div class="col-md-4"><button class="btn btn-success" onclick="reloadtable();" type="button">Search</button><img src="img/xlsx-win-icon.png" title="Export as XLS" height=40 width=40 onclick="exportfile(2);" ><img title="Export as PDF" src="img/pdf.png" height=40 width=40 onclick="exportfile(1);" ></div></div>';			
				htm+='<div id="reportrange" class="pull-right btn bg-color-red txt-color-white no-border" >'
          +'<i class="glyphicon glyphicon-calendar fa fa-calendar"></i><span></span> <b class="caret"></b></div></form>';
      
 $(".dataTables_filter").html(htm);
 
	}

	function format(a){return'<table class="grid_final"><tr><th style="width:50px;">Id:</th><td>'+a[0]+"</td></tr><tr><th>Mobile:</th><td>"+a[3]+"</td></tr><tr><th>Message:</th><td>"+a[2].replace(/(?:\r\n|\r|\n)/g, '<br />')+"</td></tr><tr><th>Length:</th><td>"+a[2].length+"(<b>"+a[10]+" Credit</b>)</td></tr><tr><th>SentOn:</th><td>"+a[5]+"</td></tr><tr><th>Service:</th><td>"+a[8]+"</td></tr><tr><th>SenderName:</th><td>"+a[9]+"</td></tr></table>"};
	
	function ellipsis(text, n) {
	    if(text.length>n)
	        return text.substring(0,n)+"...";
	    else
	        return text;
	}
	
	function htmlEscape(str) {
	    var ss= String(str)
	            .replace(/&/g, '&amp;')
	            .replace(/"/g, '&quot;')
	            .replace(/'/g, '&#39;')
	            .replace(/</g, '&lt;')
	            .replace(/>/g, '&gt;');
	    alert(ss);
	    return ss;
	}
	
	function exportfile(t){
	//	fdate	=$('#sdate').val();
	//	todate	=$('#edate').val();
    if(t==1){
    	document.location.href = 'download-SMS-PDF?startDate='+$('#sdate').val()+'&endDate='+$('#edate').val()+'&status=sdg&service='+stype+'&require=pdf';	
    }else if(t==2){
    	document.location.href = 'download-SMS-PDF?startDate='+$('#sdate').val()+'&endDate='+$('#edate').val()+'&status=sdg&service='+stype+'&require=pd44f'; 	
    }
		
	}
</script>

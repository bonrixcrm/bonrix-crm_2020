<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp" %>
<sql:query var="msg" dataSource="SMS">
                     SELECT u.username,u.uid FROM users u;	</sql:query>
<style type="text/css">
td.details-control {
	background: url('img/details_open.png') no-repeat center center;
	cursor: pointer;
}
tr.shown td.details-control {
	background: url('img/details_close.png') no-repeat center center;
}
.grid_final {
	border-collapse: collapse;
	padding: 5px;
	width: 80%;
	background: #FFF;
	border: 1px solid #CCC;
	font-size: 13px;
	font-family: calibri, arial;
}
.grid_final tr th {
	border: 1px solid #CCCCCC;
	width: 100px: bold;
	padding-left: 50px;
	height: 20px;
}
.grid_final tr td {
	padding: 5px;
	border: #CCCCCC solid 1px;
	vertical-align: middle;
	text-align: left;
}
</style>

		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
<section id="widget-grid" class="">
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-sentlogadmin" data-widget-editbutton="false">
					<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Sent</strong> <i>SMS Log By User</i></h2>		
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
				<div>
					<div class="jarviswidget-editbox">
					</div>
					<div class="widget-body no-padding">
						<table id="sent_messages_admin" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr role="row">
								<th></th>
								<th style="width: 27px;">ID</th>
								<th>Message</th>
							<th style="width: 10%;">Mobile no</th>
							<th></th>
							<th style="width: 14%;">Sent On</th>
							<th>Status</th>
							<th style="width: 8%;">Status</th>
							<th style="width: 8%;">Service</th>
							<th style="width: 8%;">SenderName</th>
								<th  style="width: 8%;">UserName</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
</section>
<input type="hidden" id="fdate">
<input type="hidden" id="todate">
<script type="text/javascript">
var userId = {};
<c:forEach var="all" items="${msg.rows}">
userId[${all.uid}]='${all.username}'; 
</c:forEach>
function testit(){
	alert(userId[1]);
}

var bsparry=[];

var mobile=$('#mobile').val();
var	fdate	=$('#fdate').val();
var	todate	=$('#todate').val();
var uidd=$("#uid2").val();
var isinit2=null;
var adminlogtable=null;
	pageSetUp();
	
	var pagefunction = function() {
	
		getdataadmin2();

	};
	function reinitrange2(){
		$("#uid2").select2();
      	var optionSet2={startDate:moment(),endDate:moment(),minDate:moment().subtract(24,"month"),maxDate:moment(),dateLimit:{days:365},opens:"left",ranges:{Today:[moment(),moment()],Yesterday:[moment().subtract(1,"days"),moment().subtract(1,"days")],"Last 7 Days":[moment().subtract(6,"days"),moment()],"Last 30 Days":[moment().subtract(29,"days"),moment()],"This Month":[moment().startOf("month"),moment().endOf("month")],"Last Month":[moment().subtract(1,"month").startOf("month"),moment().subtract(1,"month").endOf("month")]}};
    	$("#reportrange span").html(moment().subtract(7,"days").format("MMMM D, YYYY")+" - "+moment().format("MMMM D, YYYY"));$("#reportrange").daterangepicker(optionSet2);$("#reportrange").on("apply.daterangepicker",function(b,a){  
            $("#fdate").val(a.startDate.format('DD-MM-YYYY'));
            $("#todate").val(a.endDate.format('DD-MM-YYYY'));    
            $("#reportrange span").html(a.startDate.format("MMMM D, YYYY")+" - "+a.endDate.format("MMMM D, YYYY"));
            sentsmslogtable();});
    	isinit2="adsga";
	}
	function sentsmslogtable(){
			 mobile=$('#mobile').val();
			fdate	=$('#fdate').val();
			todate	=$('#todate').val();
			uidd=$("#uid2").val();
		   adminlogtable.ajax.reload();
	}
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/daterangepicker.js", function(){
		loadScript("js/plugin/datatables/dataTables.bootstrap.min.js",pagefunction);
});
		
	});
	
	
	 function getdataadmin2(){
	      adminlogtable=	$('#sent_messages_admin').DataTable({
	        "processing": false,
	        "serverSide": true,	         
	        "pageLength": 10,
	        "ordering": false,
	        "aoColumns": [ {
                "className": 'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },  {"bVisible": false},null,null,{"bVisible": false},null,{"bVisible": false},null,null,null,null],	 
	        "ajax": {"url":"getSentMessagebyAdmin",	      
	      "data": function ( d ) {
              d.m =mobile;
              d.fdate=fdate;
              d.todate=todate;
              d.uid=uidd;       
          }
	        }	      
	      , "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	          $('td:eq(7)', nRow).html(userId[aData[10]]);
	          //  $('td:eq(8)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Detail</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');
	            
	        },
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {		
				 var $cell=$('td:eq(1)', nRow);
                 $cell.text(ellipsis($cell.text(),45));             
			},
			"drawCallback" : function(oSettings) {
				if(isinit2==null){
					reinitrange2();
					}
			}
		});
	      $('#sent_messages_admin tbody').on('click', 'td.details-control', function () {
	          var tr = $(this).closest('tr');	     
	          var row = adminlogtable.row( tr );	   
	          if ( row.child.isShown() ) {	              
	              row.child.hide();
	              tr.removeClass('shown');
	          }
	          else {	              
	              row.child( format(row.data()) ).show();
	              tr.addClass('shown');
	          }
	      } );

	      var selctbox='<select name="uid2" id="uid2" style="width:150px;" ><option value=0>--ALL--</option><c:forEach var="all" items="${msg.rows}"><option value=<c:out value="${all.uid}"/> ><c:out value="${all.username}"/></option></c:forEach></select>';
   
	      var htm2='<form class="form-inline" role="form" style="width:100%;">'+selctbox 
        +'<input type="text" id="mobile"  placeholder="Mobile" style="width:100px" class="form-control">'
      +'<button class="btn btn-success" onclick="sentsmslogtable();" type="button">Search</button>';
      htm2+='<div id="reportrange" class="pull-right btn bg-color-red txt-color-white no-border form-control" >'
          +'<i class="glyphicon glyphicon-calendar fa fa-calendar"></i><span></span> <b class="caret"></b></div></form>';
      //    alert(htm2);
 $(".dataTables_filter").html(htm2);

 
	}
	function format ( d ) {
	    // `d` is the original data object for the row
	    return '<table class="grid_final">'+
	        '<tr>'+
	            '<th style="width:50px;">Id:</th>'+
	            '<td>'+d[0]+'</td></tr><tr>'+
	            '<th>Mobile:</th>'+
	            '<td>'+d[3]+'</td>'+
	        '</tr><tr>'+
            '<th>Message:</th>'+
            '<td>'+d[2]+'</td>'+
        '</tr><tr>'+
        '<th>SentOn:</th>'+
        '<td>'+d[5]+'</td>'+
    '</tr><tr>'+
    '<th>Service:</th>'+
    '<td>'+d[8]+'</td>'+
'</tr><tr>'+
'<th>SenderName:</th>'+
'<td>'+d[9]+'</td>'+
'</tr></table>';
	}
	function ellipsis(text, n) {
	    if(text.length>n)
	        return text.substring(0,n)+"...";
	    else
	        return text;
	}
</script>

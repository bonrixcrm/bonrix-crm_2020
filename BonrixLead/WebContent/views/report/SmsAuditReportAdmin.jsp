	<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/includes/SessionCheck.jsp" %>
	
<sql:query var="msg" dataSource="SMS">
                      <%-- SELECT u.username,u.uid FROM smscredit s  RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%> --%>
                     SELECT u.username,u.uid FROM users u WHERE u.parentid=<%=currentUser.getUserid()%>;	</sql:query>

<style type="text/css">
td.details-control {
	background: url('img/details_open.png') no-repeat center center;
	cursor: pointer;
}

tr.shown td.details-control {
	background: url('img/details_close.png') no-repeat center center;
}

.grid_final {
	border-collapse: collapse;
	padding: 5px;
	width: 80%;
	background: #FFF;
	border: 1px solid #CCC;
	font-size: 13px;
	font-family: calibri, arial;
}

.grid_final tr th {
	border: 1px solid #CCCCCC;
	width: 100px: bold;
	padding-left: 50px;
	height: 20px;
}

.grid_final tr td {
	padding: 5px;
	border: #CCCCCC solid 1px;
	vertical-align: middle;
	text-align: left;
}

</style>

		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
		
	




<section id="widget-grid" class="">

				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-audit" data-widget-editbutton="false">
			<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>SMS Audit</strong> <i>Log</i></h2>		
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>


				<div>

				
					<div class="jarviswidget-editbox">
					
					</div>
				
					<div class="widget-body no-padding">
					
						<table id="smsaudit" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr role="row">
								<th></th>
								<th style="width: 7px;">ID</th>
								
								
								<th>UserName</th>
							<th style="width: 10%;">Mobile no</th>

							
							<th style="width: 7%;">Old Credit</th>
							<th style="width: 7%;">TOPUP</th>
							<th>dd</th>
							<th style="width: 12%;">Validity Date</th>

							<th style="width: 12%;">TransTime</th>
							<th style="width: 8%;">Service</th>
							<th style="width: 8%;">Notes</th>
							<th style="width: 8%;">TransType</th>
						    <!-- <th  style="width: 8%;">UserName</th> -->
								</tr>
							</thead>
	
							
						</table>

					</div>
				
				</div>
			
			</div>
		




</section>


<script type="text/javascript">




var mobile=$('#mobile').val();
var	fdate	=$('#fdate').val();
var	todate	=$('#todate').val();

var uidd=$("#uidaudit").val();

var smsaudittable2=null;


	pageSetUp();
	
	var pagefunction = function() {
		
		getdata();

	};
	
	function dtpicker(){
		
	
		
		 $( "#fdate" ).datepicker({
		      defaultDate: "+0d",
		      dateFormat: 'dd-mm-yy', 
		      prevText: '<i class="fa fa-chevron-left"></i>',
			    nextText: '<i class="fa fa-chevron-right"></i>',
		    //  changeMonth: true,
		      //numberOfMonths: 3,
		      onClose: function( selectedDate ) {
		        $( "#todate" ).datepicker( "option", "minDate", selectedDate );
		      }
		    });
		    $( "#todate" ).datepicker({
		      defaultDate: "+1d",
		      dateFormat: 'dd-mm-yy', 
		      prevText: '<i class="fa fa-chevron-left"></i>',
			    nextText: '<i class="fa fa-chevron-right"></i>',
		    //  changeMonth: true,
		   //   numberOfMonths: 3,
		      onClose: function( selectedDate ) {
		        $( "#fdate" ).datepicker( "option", "maxDate", selectedDate );
		      }
		    });
	}
	
	
	function reloadtable(){
			 mobile=$('#mobile').val();
			fdate	=$('#fdate').val();
			todate	=$('#todate').val();
			uidd=$("#uidaudit").val();
		   smsaudittable2.ajax.reload();
	}

	
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
				//	loadScript("js/fnReloadAjax.js", function(){
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
			//	});
	
		});
	});

	
	
	
	function getdata(){
		//{"bVisible": false}
	      smsaudittable2=	$('#smsaudit').DataTable({
	        "processing": false,
	        "serverSide": true,	         
	        "pageLength": 10,
	        "ordering": false,
	        "aoColumns": [ {
                "className": 'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },  null,null,null,null,null,{"bVisible": false},null,null,null,null,null],	 
	        "ajax": {"url":"getSmsAuditByAdmin",	      
	      "data": function ( d ) {
              d.m =mobile;
              d.fdate=fdate;
              d.todate=todate;
              d.uid=uidd;       
          }
	        }	      
	      , "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	         // $('td:eq(1)', nRow).html(aData[5]);
	          //  $('td:eq(8)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Detail</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');
	            
	        },
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {		
				 var $cell=$('td:eq(1)', nRow);
                 $cell.text(ellipsis($cell.text(),45));             
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
	    
	      
	      $('#smsaudit tbody').on('click', 'td.details-control', function () {
	          var tr = $(this).closest('tr');	     
	          var row = smsaudittable2.row( tr );	   
	          if ( row.child.isShown() ) {	              
	              row.child.hide();
	              tr.removeClass('shown');
	          }
	          else {	              
	              row.child( format(row.data()) ).show();
	              tr.addClass('shown');
	          }
	      } );

	      var selctbox='<select name="uidaudit" id="uidaudit" style="width:150px;" ><option value=-1 selected>-ALL-</option><c:forEach var="all" items="${msg.rows}"><option value=<c:out value="${all.uid}"/> ><c:out value="${all.username}"/></option></c:forEach></select>';
   
	      var htm='<form class="form-inline" role="form" style="width:100%;">'+selctbox 
        +'<input type="text" id="mobile"  placeholder="Mobile" style="width:100px" class="form-control"><input type="text" id="fdate" style="width:80px;padding-left:1px;margin-left:1px;"  placeholder="From Date" class="dtpicker form-control"><input style="width:80px;padding-left:1px;margin-left:1px;" type="text" id="todate"  placeholder="To Date" class="dtpicker form-control">'
      +'<button class="btn btn-success" onclick="reloadtable();" type="button">Search</button></form>';
     
   
 $(".dataTables_filter").html(htm); 
 $("#uidaudit").select2(); 
 dtpicker();   
	}

	
	function format ( d ) {
	    // `d` is the original data object for the row
	    return '<table class="grid_final">'+
	        '<tr>'+
	            '<th style="width:100px;">Id:</th>'+
	            '<td>'+d[0]+'</td></tr><tr>'+
	            '<th>User:</th>'+
	            '<td>'+d[2]+'</td>'+
	        '</tr><tr>'+
            '<th>TopUp:</th>'+
            '<td>'+d[5]+'</td>'+
        '</tr><tr>'+
        '<th>Old Credit:</th>'+
        '<td>'+d[4]+'</td>'+
    '</tr><tr>'+
    '<th>Service:</th>'+
    '<td>'+d[9]+'</td>'+
'</tr><tr>'+
'<th>Notes:</th>'+
'<td>'+d[10]+'</td>'+
'</tr></table>';
	}
	
	
	function ellipsis(text, n) {
	    if(text.length>n)
	        return text.substring(0,n)+"...";
	    else
	        return text;
	}
</script>

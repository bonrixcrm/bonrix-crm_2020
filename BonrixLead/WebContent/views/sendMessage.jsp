


<!-- widget grid -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<sql:query var="msg" dataSource="SMS">
                        SELECT * FROM servicetype where isvisible=1 AND stid in (select stid from smscredit where uid=<%=currentUser.getUserid()%>);
			</sql:query>

<sql:query var="sender" dataSource="SMS">
                         SELECT * FROM sendername WHERE uid=<%=currentUser.getUserid()%>
</sql:query>

<style type="text/css">


</style>
<link rel="stylesheet" href="css/jquery.timepicker.css">
<section id="widget-grid" class="">

	<div class="row">

<article class="col-sm-12">
 
 
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
			
				<!-- <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>CSV/EXCEL Upload Send Message</h2>

				</header> -->
				<header>
					<span class="widget-icon"> <i
						class="glyphicon glyphicon-stats txt-color-darken"></i>
					</span>
					<h2>CSV/EXCEL Upload Send Message</h2>

					<ul class="nav nav-tabs pull-right in" id="myTab">
						<li class="active"><a data-toggle="tab" href="#s1"><i
								class="fa fa-clock-o"></i> <span
								class="hidden-mobile hidden-tablet">Single Column</span></a></li>

						<li><a data-toggle="tab" href="#s2"><i
								class="fa fa-facebook"></i> <span
								class="hidden-mobile hidden-tablet">Multi Column</span></a></li>

					<!-- 	<li><a data-toggle="tab" href="#s3"><i
								class="fa fa-dollar"></i> <span
								class="hidden-mobile hidden-tablet">Revenue</span></a></li> -->
					</ul>

				</header>
				
				<!-- widget div-->
				<div>

					<div class="jarviswidget-editbox">

					</div>

					<div class="widget-body">


<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade active in padding-10 no-padding-bottom"
								id="s1">					
							<form  id="uploadcsvfrm" enctype="multipart/form-data"
						action="UploadSingleCSVXLS" method=post class="form-horizontal">
							
							<fieldset>
								
							
								
									
							
								<div class="form-group">
									<label class="col-md-2 control-label bld nopad">CSV/XLS:</label>
									<div class="col-md-3">
									
									<input type="file" class="btn btn-default"  name="file"
										onchange="this.parentNode.nextSibling.value = this.value"/>
							
									</div>
									
									
									<label class="col-md-1 control-label bld nopad">Service:</label>
									<div class="col-md-2">
									<select name="stype" id="stype" class="form-control">
									
												<c:forEach var="all" items="${msg.rows}">
													<option value=${all.stid}>${all.type}</option>

												</c:forEach>
										</select>
										</div>
										
									<div  class="progress" style="display: none;">
        <div class="bar"></div >
        <div class="percent">0%</div >
    </div>
    <div id="status"></div>
    
								</div>
									
								
								      <div class="form-group">
								      <label class="col-md-1 control-label bld"></label>
								       <label class="col-md-1 control-label bld nopad">SenderID:</label>
										<div class="col-md-2" >
										
										<select name="senderId"
											id="senderId" class="form-control">
												<!-- <option value=0>INFORM</option> -->
												<c:forEach var="all" items="${sender.rows}">
													<option value=${all.sendername}  ${all.sid == cookie.dsender.value ? 'selected' : ''}  >${all.sendername}</option>
												</c:forEach>
										</select>
									<!-- 	<p class="note"><strong>Note:</strong>Sender Name not valid for Promotional SMS.</p> -->
									</div>
									  <div title="Check this if you are sending Unicode Like Hindi,Gujarati,Marathi"  class="col-md-2" >
										<!-- 
										<label class="checkbox-inline">
											  <input  type="checkbox"  id="isunicodesingle"  name="isunicodesingle" value="isunicodesingle" class="checkbox style-0">
											  <span><strong>Unicode</strong> </span>
										</label> -->
										
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-default active">
												<input type="radio" name="isunicodesingle" checked="checked" value="normal" data-bv-field="isunicodesingle">
												English </label>
											<label class="btn btn-default">
												<input type="radio" name="isunicodesingle" value="unicode" data-bv-field="isunicodesingle">
												Unicode </label>
											
										</div>
										</div>
										
                                    <!--   <div class="col-md-2" >
										
										<label class="checkbox-inline">
											  <input type="checkbox"  id="isschedule"  name="isschedule" value="chkschedule" class="checkbox style-0">
											  <span  style="width: 100px;" class="bld nopad">Schedule </span>
											
										</label>
										</div>
           
                    <div class="col-md-2 row red box" style="width: 140px;display: none;" >
										
									
									   
									   <input type="text" id="dtpkr"  name="mydate" placeholder="Select a date"  class="form-control datepicker" data-dateformat="dd/mm/yy">
									    </div>
									    <div class="col-md-1 row red box" style="width: 140px;display: none;" >
									     <input type="text" id="clockpicker" name="mytime"  class="form-control" placeholder="Select a Time" >
									   
									   
                                    </div> -->
                                   
                                    </div>
	
								
								<div class="form-group">
									<label class="col-md-2 control-label bld nopad">Message</label>
									<div class="col-md-9">
										<input type="hidden" name="message" id="message" />
														<textarea id="emessage" name="emessage"  class="form-control"
											rows="4" 
											maxlength="5000"></textarea>
                                        <span id="sms-counter" style="font-weight: bold;">
										SMS Count:<span class="messages"></span> <span style="float: right;" class="remaining" ></span></span>

									</div>
								</div>
							
							<div class="form-group">
									<label class="col-md-2 control-label"></label>
									<div class="col-md-1">
										
										 								<input type="Submit" value="SEND" class="btn btn-primary">
									</div>	
									 <div class="col-md-1" style="width: 120px;" >
										
										<label class="checkbox-inline">
											  <input type="checkbox"  id="isschedule"  name="isschedule" value="chkschedule" class="checkbox style-0">
											  <span><strong> Schedule </strong></span>
										</label>
										</div>
           
                    <div class="col-md-2 row red box" style="width: 140px;display: none;" >
									   <input type="text" id="dtpkr"  name="mydate" placeholder="Select a date"  class="form-control datepicker" data-dateformat="dd/mm/yy">
									    </div>
									    <div class="col-md-1 row red box" style="width: 140px;display: none;" >
									     <input type="text" id="clockpicker" name="mytime"  class="form-control" placeholder="Select a Time" >
                                    </div>
								</div>
							
							</fieldset>
							</form>
				</div>
				

							<div class="tab-pane fade in padding-10 no-padding-bottom" id="s2">
								<!-- <div class="widget-body-toolbar bg-color-white"> -->

									
	<form  id="uploadmulticsvfrm" enctype="multipart/form-data"
						action="UploadMultiColumnCSV" method=post class="form-horizontal">
							
							<fieldset>
								
							
								
									
							
								<div class="form-group">
									<label class="col-md-2 control-label bld nopad">Multi Column CSV:</label>
									<div class="col-md-3">
									
									<input type="file" class="btn btn-default" name="file"
										onchange="this.parentNode.nextSibling.value = this.value"/>
							
									</div>
									
									
									<label class="col-md-1 control-label bld nopad">Service:</label>
									<div class="col-md-2">
									<select name="stype2" id="stype2" class="form-control">
									
												<c:forEach var="all" items="${msg.rows}">
													<option value=${all.stid}>${all.type}</option>

												</c:forEach>
										</select>
										</div>
										
									<div  class="progress" style="display: none;">
        <div class="bar"></div >
        <div class="percent">0%</div >
    </div>
    <div id="status"></div>
    
								</div>
									
								
								      <div class="form-group">
								        <label class="col-md-2 control-label bld nopad">Mobile No</label>
                                      <div class="col-md-2" >
										
										<select class="form-control" name="selmobile">
										<option value="A">A</option>
										<option value="B">B</option>
										<option value="C">C</option>
										<option value="D">D</option>
										<option value="E">E</option>
										<option value="F">F</option>
										<option value="G">G</option>
										<option value="H">H</option>
										 </select>
										</div>
								      
								      <label class="col-md-1 control-label bld nopad">SenderID:</label>
										<div class="col-md-2" >
										
										<select name="senderId2"
											id="senderId2" class="form-control">
												<!-- <option value=0>INFORM</option> -->
												<c:forEach var="all" items="${sender.rows}">
													<option value=${all.sendername}  ${all.sid == cookie.dsender.value ? 'selected' : ''}  >${all.sendername}</option>
												</c:forEach>
										</select>
									<!-- 	<p class="note"><strong>Note:</strong>Sender Name not valid for Promotional SMS.</p> -->
									</div>
									
									  <div class="col-md-2" >
										
								<!-- 		<label class="checkbox-inline">
											  <input type="checkbox"  id="isunicode"  name="isunicode" value="isunicode" class="checkbox style-0">
											  <span><strong>Unicode</strong> </span>
										</label> -->
											<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-default active">
												<input type="radio" name="isunicode" checked="checked" value="normal" data-bv-field="isunicode">
												English </label>
											<label class="btn btn-default">
												<input type="radio" name="isunicode" value="isunicode" data-bv-field="isunicode">
												Unicode </label>
											
										</div>
										</div>
									
  </div>
	
								
								<div class="form-group">
									<label class="col-md-2 control-label bld">Message</label>
									<div class="col-md-9">
										<input type="hidden" name="message2" id="message2" />
														<textarea id="emessage2" name="emessage2" type="text"  class="form-control"
											rows="4" 
											maxlength="5000">hello &lt;A&gt; your balance is &lt;B&gt; </textarea>
                                       <span id="sms-counter2" style="font-weight: bold;">
										SMS Count:<span class="messages"></span> <span style="float: right;" class="remaining" ></span></span>

									</div>
								</div>
							
							<div class="form-group">
									<label class="col-md-2 control-label"></label>
									<div class="col-md-1">
										
										 								<input type="Submit" value="SEND" class="btn btn-primary">
									</div>
									  <div class="col-md-1" style="width: 120px;" >
										
										<label class="checkbox-inline">
											  <input type="checkbox"  id="isschedule2"  name="isschedule2" value="chkschedule2" class="checkbox style-0">
											  <span>Schedule </span>
										</label>
										</div>
           
                    <div class="col-md-2 row red box" style="width: 140px;display: none;" >
									   <input type="text" id="dtpkr2"  name="mydate2" placeholder="Select a date"  class="form-control datepicker" data-dateformat="dd/mm/yy">
									    </div>
									    <div class="col-md-1 row red box" style="width: 140px;display: none;" >
									     <input type="text" id="clockpicker2" name="mytime2"  class="form-control" placeholder="Select a Time" >
                                    </div>
									<div class="col-md-7">
									<div class="note">
											
										<a target="_blank" href="js/samplecsvmulticolumn.csv">
										 <button type="button" class="btn btn-primary  btn-xs">
												<b>Sample CSV</b>
											</button></a> 
											
				<a target="_blank" href="js/sample-XLSX-mulitcolumn.xlsx"><button type="button" class="btn btn-primary  btn-xs"><b>Sample XLSX</b></button></a><a target="_blank" href="js/sample-XLS-multicolumn.xls"> <button type="button" class="btn btn-primary  btn-xs">
												<b>Sample XLS</b></button></a>
										
										</div>	

									</div>
								</div>
							
							</fieldset>
						

									</form>

							<!-- 	</div> -->
								

							</div>



				</div>



		
		
	



			</div>

		</div>

	</div>
	
	</article>

	<!-- end row -->
</div>

</section>




		
					<script src="js/jquery.form.js"></script>



					<script type="text/javascript">
					

		            
					loadScript("js/jquery.timepicker.min.js", runClockPicker);

					function runClockPicker(){
						
						  $('#clockpicker').timepicker({ 'scrollDefault': 'now' });
						  $('#clockpicker2').timepicker({ 'scrollDefault': 'now' });
					}
   function encodeit(){
	   
	   $("#message").val(encodeURIComponent($("#emessage").val()));
	   
   }
   
function encodeit2(){
	   
	   $("#message2").val(encodeURIComponent($("#emessage2").val()));
	   
   }
   
					
					
$(document).ready(function() {

							
						   	
			        		$(".datepicker").datepicker({
			        	//	    defaultDate: "+1y",
			        		     dateFormat: 'dd-mm-yy', 
			        		   // formate:'dd',
			        		  //  changeMonth: true,
			        		//    numberOfMonths: 3,
			        		    prevText: '<i class="fa fa-chevron-left"></i>',
			        		    nextText: '<i class="fa fa-chevron-right"></i>',
			        		    onClose: function (selectedDate) {
			        		    //    $("#to").datepicker("option", "maxDate", selectedDate);
			        		    }
			        	
			        		});
			        		
			        		 $('input[type="checkbox"]').click(function(){
			        	            if($(this).attr("value")=="chkschedule"){
			        	                $(".red").toggle('slow');
			        	            }
			        	            
			        	            if($(this).attr("value")=="chkschedule2"){
			        	                $(".red").toggle('slow');
			        	            }
			        	         
			        	        });
							$('#emessage').countSms('#sms-counter');
							$('#message2').countSms('#sms-counter2');
							
							var bar = $('.bar');
							var percent = $('.percent');
							var status = $('#status');
							$('#uploadcsvfrm').submit(function() {
								//alert("asdgasdgas");
								if($('#emessage').val()==""){
						    		alert("Can not sent Blank SMS !");
						    		
						    		return false;
						    	}
					
									    	encodeit();
							$(this).ajaxSubmit({
							    beforeSend: showRequest,
							    resetForm: true ,
							    uploadProgress: function(event, position, total, percentComplete) {
							        var percentVal = percentComplete + '%';
							        bar.width(percentVal)
							        percent.html(percentVal);
							        
							    },
							    success: function() {
							        var percentVal = '100%';
							        bar.width(percentVal)
							        percent.html(percentVal);
							    },error: function() {
							        alert("Please choose any file");
							    },
								complete: function(xhr) {
									//status.html(xhr.responseText);
									$.unblockUI();
									$.smallBox({
										title : xhr.responseText,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
														});
									
								}
							}); 
							return false;
							});
							
							
							function showRequest(formData, jqForm, options) { 
							    // formData is an array; here we use $.param to convert it to a string to display it 
							
											    	
						    	status.empty();
						        var percentVal = '0%';
						        bar.width(percentVal)
						        percent.html(percentVal);
						        $.blockUI({ message: percent }); 
							    return true; 
							} 
							
							
							$("#uploadmulticsvfrm").submit(function() {
						//		alert("asdgasdgas");
						
								if($('#emessage2').val()==""){
						    		alert("Can not sent Blank SMS !");
						    		return false;
						    	}
									    	encodeit2();
							$(this).ajaxSubmit({							
							    beforeSend: function() {
							        
							    	
							    	
							    	status.empty();
							        var percentVal = '0%';
							        bar.width(percentVal)
							        percent.html(percentVal);
							        $.blockUI({ message: percent }); 
							    },
							    resetForm: true ,
							    uploadProgress: function(event, position, total, percentComplete) {
							        var percentVal = percentComplete + '%';
							        bar.width(percentVal)
							        percent.html(percentVal);
							        
							    },
							    success: function() {
							        var percentVal = '100%';
							        bar.width(percentVal)
							        percent.html(percentVal);
							    },error: function() {
							        alert("Please choose any file");
							    },
								complete: function(xhr) {
									//status.html(xhr.responseText);
									$.unblockUI();
									$.smallBox({
										title : xhr.responseText,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
														});
									
								}
							}); 

							return false;
						});
});
						
					 function  uploadProgress(event, position, total, percentComplete) {
					        var percentVal = percentComplete + '%';
					        bar.width(percentVal)
					        percent.html(percentVal);
					    };
						
					
						function showResponse(responseText, statusText, xhr, $form)  {

							$.smallBox({
								title : responseText,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
												});
						//	alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
						 //       '\n\nThe output div should have already been updated with the responseText.'); 
						}
						

					</script>

					

					<!-- <textarea rows="10" cols="100" name="numbers" id="numbers" ></textarea> -->



		

	<!-- end row -->

</section>
<!-- end widget grid -->


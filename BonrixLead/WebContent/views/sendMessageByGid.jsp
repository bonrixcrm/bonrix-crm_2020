
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>

<%
String msg="";
if(request.getParameter("url")!=null){
	msg=request.getParameter("url");
}

%>
<sql:query var="msg" dataSource="SMS">SELECT * FROM servicetype where type <> 'video'; </sql:query>
<sql:query var="sender" dataSource="SMS">SELECT * FROM sendername WHERE uid=<%=currentUser.getUserid()%></sql:query>

<input type="hidden" id="urlid"
	value="<%=request.getParameter("url") %>" />

<link rel="stylesheet" href="css/jquery.tag-editor.css">
<link rel="stylesheet" href="css/jquery.timepicker.css">
<script type="text/javascript">
var dupli=0;
var inval=0;	
   var numbers="";
   
  
   var gid=<%=request.getParameter("gid")%>
   
  
	 
	 
	function formSub() {
		
		/* numbers = $('#numbers').tagEditor('getTags')[0].tags;
	//alert(numbers);s
		if(numbers==""){alert("Enter Mobile number");return;}; */
		var message = $('#message').val();
		var senderId = $('#senderId').val();
		var stype = $('#stype').val();
		
		var schedule=$('#dtpkr').val()+$('#clockpicker').val();
	var chkstatus=$('#isschedule').is(':checked');
	//alert(chkstatus);
		$.ajax({
			
			url : "sendMessageByGid",
			type : "POST",
			data : {
				'message' : encodeURIComponent(message),
				'senderId' : senderId,
				'serviceid' : stype,
				'gid':gid,'schedule':schedule,'isschedule':chkstatus			
				
			},
			beforeSend : function(response) {

				$('#sendit').attr('disabled', true);
				$.blockUI();
			},
			success : function(response) {
			//	alert(response);
				$('#sendit').attr('disabled', false);
				$("#message").val("");				
				$.unblockUI();					
$.smallBox({
	title : response,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
					});
		
			},
			error : function(xhr, status, error) {
				alert(xhr.responseText);
				$('#sendit').attr('disabled', false);
			}
		});
		return false;
	}
</script>

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-sm-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				
				
				<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Group</strong> <i>Message</i></h2>		
							
					<div class="widget-toolbar" role="menu">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
				
				
				<!-- widget div-->
				<div>
					<!-- widget edit box -->
					<div class="jarviswidget-editbox"></div>

					<div class="widget-body">
						<form class="form-horizontal">

							<fieldset>



								<div class="form-group">
									<label class="col-md-2 control-label bld">Service:</label>
									<div class="col-md-2">
										<select name="stype" id="stype" class="form-control">
											<c:forEach var="all" items="${msg.rows}">
												<option value=${all.stid}>${all.type}</option>

											</c:forEach>
										</select>
									</div>
									<label class="col-md-2 control-label bld">Sender Name:</label>
									<div class="col-md-3">

										<select name="senderId" id="senderId" class="form-control">
											<c:forEach var="all" items="${sender.rows}">
												<option value=${all.sendername} >${all.sendername}</option>
											</c:forEach>
										</select>
										<!-- 	<p class="note"><strong>Note:</strong>Sender Name not valid for Promotional SMS.</p> -->
									</div>
<div class="col-md-2" style="width: 85px;margin: 0px;padding: 0px;">
										
										<label class="checkbox-inline">
											  <input type="checkbox"  id="isschedule"  name="colorCheckbox"value="red" class="checkbox style-0">
											  <span>Schedule </span>
										</label>
										</div>
									
								</div>
                  <div class="form-group">
                   <label class="col-md-3 control-label bld"></label>
                    <div class="col-md-2 row red box" style="width: 140px;display: none;" >
										
									
									   
									   <input type="text" id="dtpkr"  name="mydate" placeholder="Select a date"  class="form-control datepicker" data-dateformat="dd/mm/yy">
									    </div>
									    <div class="col-md-1 row red box" style="width: 140px;display: none;" >
									     <input type="text" id="clockpicker" name="mydate"  class="form-control" placeholder="Select a Time" >
									   
									   
                                    </div></div>

								<!-- 
								<div class="form-group">
									<label class="col-md-2 control-label">Password field</label>
									<div class="col-md-10">
										<input class="form-control" placeholder="Password field" type="password" value="mypassword">
									</div>
								</div> -->

								<div class="form-group">
									<label class="col-md-2 control-label bld">Message</label>
									<div class="col-md-9">

										<textarea id="message" style="font: normal 14px sans-serif;"
											class="form-control" rows="4" maxlength="5000"><%=msg%></textarea>
										<span id="sms-counter" style="float: right;">
											<button type="button" class="btn btn-success  btn-xs">
												SMS Count:&nbsp;&nbsp;<span class="messages"
													style="font-size: 16px;"></span>&nbsp;&nbsp; | <b><span
													class="remaining"></span></b>
										</span>
										</button>
										<!-- SMS Count:<span class="messages"></span> <span style="float: right;" class="remaining" ></span></span> -->


									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label"></label>
									<div class="col-md-7">

										<input type="button" id="sendit" value="SEND"
											onclick="formSub();" class="btn btn-primary btn-sm">
									</div>
								</div>

							</fieldset>
						</form>


					</div>

				</div>

			</div>

		</article>

		<!-- end row -->
	</div>

</section>

<style type="text/css">
border-style:solid;
border-color:#287EC7;
</style>
<script type="text/javascript">


	            
loadScript("js/jquery.timepicker.min.js", runClockPicker);

function runClockPicker(){
	
	  $('#clockpicker').timepicker({ 'scrollDefault': 'now' });
}



	            $(document).ready(function(){
	            	
	            	
	        		$(".datepicker").datepicker({
	        	//	    defaultDate: "+1y",
	        		     dateFormat: 'dd-mm-yy', 
	        		   // formate:'dd',
	        		  //  changeMonth: true,
	        		//    numberOfMonths: 3,
	        		    prevText: '<i class="fa fa-chevron-left"></i>',
	        		    nextText: '<i class="fa fa-chevron-right"></i>',
	        		    onClose: function (selectedDate) {
	        		    //    $("#to").datepicker("option", "maxDate", selectedDate);
	        		    }
	        	
	        		});
	        		
	        		 $('input[type="checkbox"]').click(function(){
	        	            if($(this).attr("value")=="red"){
	        	                $(".red").toggle('slow');
	        	            }
	        	         
	        	        });
	        		 
	            	$('#message').countSms('#sms-counter');
	        	  /*   $('#numbers').tagEditor({	    	
	        	    	    placeholder: 'Enter Mobile nos ...',
	        	    	    clickDelete:'true',sortable:'false',
	        	            onChange: function(){ $("#totalmobile").html('<button  type="button" class="btn btn-primary  btn-xs">Total Mobile:<b>'+$("#numbers").tagEditor('getTags')[0].tags.length+'</b></button><button  type="button" class="btn btn-warning  btn-xs">Duplicate:<b>'+dupli+'</b></button><button  type="button" class="btn btn-danger  btn-xs">Invalid:<b>'+inval+"</b></buttton>");	            
	        	            },});     */        	
	            	
	            });

</script>





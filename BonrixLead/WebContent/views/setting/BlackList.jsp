<%@include file="/includes/SessionCheck.jsp" %>
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
<section id="widget-grid" class="">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-spam" data-widget-editbutton="false" style="width: 50%;margin: auto;">
<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Black Number</strong> <i>  List</i></h2>		
					<div class="widget-toolbar" role="menu">
					<a href="./views/setting/createBlackWhite.jsp" data-toggle="modal"
			data-target="#remoteModal" class="btn btn-primary button"> <i
			class="fa fa-circle-arrow-up fa-lg"></i> Add BlackList Number
		</a>
					</div>
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
				<div>
					<div class="jarviswidget-editbox">
					</div>
					<div class="widget-body no-padding">
						<table id="datatable_tabletools" style="margin: auto;" class="table table-striped table-bordered bld">
							<thead>
								<tr role="row">
								
								<th>BlackList</th>								
							
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			</section>
			
<script type="text/javascript">
var mediatable=null;
	pageSetUp();
		function reloadtable(){
			  mediatable.ajax.reload();		
		}
	var pagefunction = function() {
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			reloadtable();
			});
	};
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
			});
		});
	});	

	
	function getdata(){
		
		 if (mediatable)
			 mediatable.fnDestroy();
//		alert("getdata");
	      mediatable=	$('#datatable_tabletools').DataTable({
	    	  "processing": false,
	          "serverSide": true,
		        "pageLength": 10,
		        "ordering": false,
		        "ajax": "getBlockedNumbers",
	          "columns": [
	              
	              { "data": "number" }
	         
	          ]
	       
	      });
	      

	/*       $('#datatable_tabletools tbody').on( 'click', 'button', function () {
	          var data = mediatable.row( $(this).parents('tr') ).data();
	        
	          delhttp(data.bwid);
	          
	      } ); */
		
		
	}
/* 	
	function delhttp(id){
		var conf = confirm('Continue delete?');
		
		if(conf){
		$.get("deleteBlackWhiteList?bwid="+id,function(msg){
	
			reloadtable();
			//alert(msg);
		});
		}
		
	} */

</script>

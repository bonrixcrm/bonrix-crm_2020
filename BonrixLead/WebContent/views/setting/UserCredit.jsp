<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/includes/SessionCheck.jsp" %>
	
	
<div class="row">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>
	</div>
	</div>
	 <div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
<section id="widget-grid" class="">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-crd" data-widget-editbutton="false">
				<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>SMS Credit</strong></h2>		
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
				<div>
					<div class="jarviswidget-editbox">
					</div>
					<div class="widget-body no-padding">
						<table id="credit_tabletools" style="width: 100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr role="row">
								<th data-hide="phone" class="sorting_asc" tabindex="0" aria-controls="credit_tabletools" rowspan="1" colspan="1" aria-sort="ascending" aria-label="ID: activate to sort column ascending" style="width: 27px;">ID</th>
								<th data-hide="phone,tablet">Username</th>
								<th data-class="expand">SMS</th>
								<!-- <th class="sorting" tabindex="0" aria-controls="credit_tabletools" rowspan="1" colspan="1" aria-label="Phone: activate to sort column ascending" >Date</th> -->
								<th data-hide="phone">Validity</th>
								<th data-hide="phone,tablet" >Mobile Number</th>
								<th data-hide="phone,tablet" >Email</th>
								<th data-hide="phone,tablet" >Service</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
</section>


<div class="modal fade" id="updateModal" tabindex="-1" role="dialog"
				aria-labelledby="remoteModalLabel" aria-hidden="true"
				style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content" style="width: 800px;"></div>
				</div>
			</div>
<script type="text/javascript">
var credittable=null;
	pageSetUp();

	function reloadtable(){
		  credittable.ajax.reload();		
	}
	var pagefunction = function() {
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			 reloadtable();	
			});
		 
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	    
	    // custom toolbar
	    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
		};

	
	// load related plugins
	
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
			});
		});
	});

	
	function getdata(){
		
		 if (credittable)
			 credittable.fnDestroy();
		 
	      credittable=	$('#credit_tabletools').DataTable({
	        "processing": false,

           "bServerSide": false, 
           "order": [[ 3, "asc" ]],
	        "ajax": "getCreditDatabyUid", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	            
	        	$('td:eq(2)', nRow).html(aData[3]);
	        	
	        	$('td:eq(3)', nRow).html(aData[4]);
	        	$('td:eq(4)', nRow).html(aData[5]);
	        	$('td:eq(5)', nRow).html(aData[6]);
	        	$('td:eq(6)', nRow).html(aData[7]);
	            
	        },
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
	
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
		
	      $('<a href="./user/AssignCredit.jsp" data-toggle="modal"'
	  			+'data-target="#remoteModal" class="btn btn-primary"> <i class="fa fa-circle-arrow-up fa-lg"></i> TopUp Credit'
			+'</a><button id="refresh" onclick="reloadtable()"  class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-refresh"></i></button>').appendTo('div.dataTables_filter');	
	}

</script>

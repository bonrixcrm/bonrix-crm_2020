<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/includes/SessionCheck.jsp" %>
	



	
<div class="row">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

		<!-- Button trigger modal -->
		

		<!-- MODAL PLACE HOLDER -->
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>
		<!-- END MODAL -->

	</div>
	</div>
			
 		<!-- MODAL PLACE HOLDER -->
	 <div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
	
		
	



<!-- widget grid -->
<section id="widget-grid" class="">

			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
			
				<header>
				Credit List

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<table id="datatable_tabletools" style="width: 100%" class="table table-striped table-bordered table-hover" >
							<thead>
								<tr role="row">
								<th data-hide="phone" class="sorting_asc" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-sort="ascending" aria-label="ID: activate to sort column ascending" style="width: 27px;">ID</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Username</th>
								<th data-class="expand" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" >SMS</th>
								<!-- <th class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-label="Phone: activate to sort column ascending" >Date</th> -->
								<th data-hide="phone" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1"  >Validity</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Mobile Number</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Email</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Service</th>
								
								<th></th>
								
								</tr>
							</thead>
						
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog"
				aria-labelledby="remoteModalLabel" aria-hidden="true"
				style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content" style="width: 800px;"></div>
				</div>
			</div>
<script type="text/javascript">


var responsiveHelper_dt_basic = undefined;
var responsiveHelper_datatable_fixed_column = undefined;
var responsiveHelper_datatable_col_reorder = undefined;
var responsiveHelper_datatable_tabletools = undefined;

var mediatable=null;


	pageSetUp();
	
		
	
	function reloadtable(){
		
		mediatable.fnReloadAjax();	
	}
	var pagefunction = function() {
		 
		 
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			 reloadtable();	
			});
		 
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	    
	    // custom toolbar
	    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
		};

	
	// load related plugins
	
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
			loadScript("js/plugin/datatables/dataTables.tableTools.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
					loadScript("js/fnReloadAjax.js", function(){
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
				});
			});
			});
		});
	});

	
	function getdata(){
		
		 if (mediatable)
			 mediatable.fnDestroy();
		 
//		alert("getdata");
	      mediatable=	$('#datatable_tabletools').dataTable({
	        "processing": false,

           "bServerSide": false, 
       //    "aoColumns": [null,null,null,{"bVisible": false},null,null,null,null],
	        
	       // "bdestroy": true,
	        
	      //  "bDestroy": true,
	        "ajax": "getCreditDatabyUid", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	            
	        	$('td:eq(2)', nRow).html(aData[3]);
	        	
	        	$('td:eq(3)', nRow).html(aData[4]);
	        	$('td:eq(4)', nRow).html(aData[5]);
	        	$('td:eq(5)', nRow).html(aData[6]);
	        	$('td:eq(6)', nRow).html(aData[7]);
	        	//$('td:eq(2)', nRow).html(aData[1]);
	            //./user/updateCredit.jsp?scid=${all.scid}&userName=${all.username}&uid=${all.uid}
	            //$('td:eq(8)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./user/updateCredit.jsp?scid='+aData[0]+'&userName='+${all.username}+'&uid='+${all.uid}'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Edit</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');
	            $('td:eq(7)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./user/updateCredit.jsp?scid='+aData[0]+'&userName='+aData[1]+'&uid='+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Edit</a><a href="deleteCredit?scid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-danger btn-xs">Delete</a></div>');
	            
	        },
	 /*        
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>", */
	   /*      "oTableTools": {
	        	 "aButtons": [
	             "copy",
	             "csv",
	             "xls",
	                {
	                    "sExtends": "pdf",
	                    "sTitle": "SmartSMS_PDF",
	                    "sPdfMessage": "SmartSMS PDF Export",
	                    "sPdfSize": "letter"
	                },
	             	{
	                	"sExtends": "print",
	                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
	            	}
	             ],
	            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
	        }, */
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
	
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
		
	      $('<a href="./user/AssignCredit.jsp" data-toggle="modal"'
	  			+'data-target="#remoteModal" class="btn btn-primary"> <i class="fa fa-circle-arrow-up fa-lg"></i> Add Credit'
			+'</a><button id="refresh" onclick="reloadtable()"  class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-refresh"></i></button>').appendTo('div.dataTables_filter');	
	}

</script>

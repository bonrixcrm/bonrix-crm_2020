<%@include file="/includes/SessionCheck.jsp"%>
			<script type="text/javascript">
				function formSub(event) {
					var bwnumber = $('#bwnumber').val();
					var bwtype = $('#bwtype').val();
					$.ajax({
						url : "saveBlackWhiteNumber",
						type : "GET",
						data : {
							'bwnumber' : bwnumber,
							'type' : bwtype,
						},
						success : function(response) {
							if(response==1){
								alert("Added Successfully");
							}else{
								alert("Already Exist");	
							}
							 $('#remoteModal').modal('toggle');
						},
						error : function(xhr, status, error) {

							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
<section id="widget-grid" class="">
	<div class="row">
		<article class="col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-purple" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false" data-widget-sortable="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
					<h2>Add Black/White Number</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
					</div>
					<div class="widget-body">
                     <form class="form-inline" role="form">							
                               <fieldset>
								<div class="form-group">
									<label class="sr-only">Number</label>
									<input  class="form-control" id="bwnumber"  name="bwnumber" placeholder="Enter 10 digit number">
								</div>
								<div class="form-group"  >

										<select name="bwtype" id="bwtype" class="form-control">
											
											<option value="1">WhiteList</option>
											<option value="2">BlackList</option>
											
											
											
										</select>
										<!-- 	<p class="note"><strong>Note:</strong>Sender Name not valid for Promotional SMS.</p> -->
									</div>
								<button type="button"  onclick="formSub()" class="btn btn-primary">
									SAVE
								</button>
								<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			</article>
			</div>
		</section>



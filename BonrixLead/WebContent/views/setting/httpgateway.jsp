	<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/includes/SessionCheck.jsp" %>
	

	
			
		<!-- MODAL PLACE HOLDER -->
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
		
	



<!-- widget grid -->
<section id="widget-grid" class="">

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
				<a href="./views/setting/UrlSetting.jsp" data-toggle="modal"
					data-target="#remoteModal" class="btn btn-primary"> <i
					class="fa fa-circle-arrow-up fa-lg"></i> Create Gateway
				</a>

				<!-- MODAL PLACE HOLDER -->
				<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
					aria-labelledby="remoteModalLabel" aria-hidden="true"
					style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content"></div>
					</div>
				</div>

			</div>

		</div>
	</div>

</div>
						
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
			
				<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>				
               Http Gateway Setting
				</header>

				<!-- widget div-->
				<div>

					<div class="jarviswidget-editbox">

					</div>
					<div class="widget-body no-padding">

						<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr role="row">
								<th data-hide="phone" style="width: 27px;">ID</th>
								
								<th class="sorting" >Username</th>
								<th data-hide="phone" >API</th>
																
								<th data-hide="phone,tablet">priority</th>
								<th></th>
								
								<th data-hide="phone,tablet">Service</th>
								<th></th>
								
								</tr>
							</thead>
	
							
						</table>

					</div>
				</div>
			</div>
</section>
<script type="text/javascript">
var httptable;
pageSetUp();
	
		function reloadtable(){
			httptable.ajax.reload();
		}
	var pagefunction = function() {
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			reloadtable();
			});
		
	};

	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
				});
		});
	});

	
	function getdata(){
		
		 if (httptable)
			 httptable.fnDestroy();
	      httptable=	$('#datatable_tabletools').DataTable({
	        "processing": false,
	        "serverSide": false,
	        "bServerSide": false, 
	        "aoColumns": [null,null,null,null,{"bVisible": false},null,null],
	        "bdestroy": true,
	        "ajax": "findHttpGateway?t=0", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	           $('td:eq(2)', nRow).text(aData[2]);
	       $('td:eq(5)', nRow).html('<div class="btn-group btn-group-justified" style="width:100px;"><a href="#"  class="btn btn-primary btn-xs" onclick="delhttp('+aData[0]+')">Delete</a></div>');
	            
	        },
	 
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
		
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
		
		
	}
	
	function delhttp(id){
		var conf = confirm('Continue delete?');
		if(conf){
		$.get("deleteHttpGatewaybyid?hid="+id,function(msg){
			reloadtable();
		});
		}
		
	}

</script>

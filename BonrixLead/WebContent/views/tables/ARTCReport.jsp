<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
table.dataTable.select tbody tr,
table.dataTable thead th:first-child {
  cursor: pointer;
}
</style>
</head>

<script>
var table=null;
$(document).ready(function (){
	    table = $('#example').DataTable({
	         "ajax": {"url":"GetAutodialManageReport",	      
	 	        "data": function ( d ) {}
	 	        },
	   });
	    reloadtable();
   });

function reloadtable() {
    setInterval(function(){ table.ajax.reload(); }, 3000);
}
</script>
<body>
<!-- class="table table-striped table-bordered" -->
<div style="margin-top: 2%;" id="dataTbl">
<table id="example" class="table" cellspacing="0" width="100%">
   <thead>
      <tr>
        <!--  <th><input name="select_all" value="1" type="checkbox"></th> -->
         <th>Name</th>
         <th>User Name</th>
         <th>Start Time</th>
         <th>End Time</th>
         <th>Total Working Time</th>
      </tr>
   </thead>
</table></div>
</body>
</html>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
var dataTable=null;
$(document).ready(function() {
	          dataTable = $('#cat-grid').DataTable({
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetCatColumns",
	            "columnDefs": [{
	                    "targets": -1,
	                    "data": null,
	                    "defaultContent": "<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"     //<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;
	              }],
	                "order": [[ 0, 'desc' ]]
	        }); 
	  

$('#cat-grid tbody').on( 'click', '#edit-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $('#catid').val(data[0]);
    $('#catval').val(data[1]);
    console.log("leadId::"+ data[3])
    var editLeadId = data[3];
    
    $.ajax({
  	  url:'getLeadProcessState',
  	  type: 'GET',
  	  success: function(data) {
  		  var msg=data;
  	  		var html="<option value='Lead Process'>All Process State</option>";
  	  		var i=0;
  	  		for(i=0;i<msg.length;i++){
  	  			var val='"'+msg[i][1]+'"';
  	  		    html+="<option value="+msg[i][0]+">"+msg[i][1]+"</option>";
  	  		}
  	  		$('#editLPState').html(html);
  	  	    var $option = $('#editLPState').children('option[value="'+editLeadId +'"]');
	  	    $option.attr('selected', true)
  	  	 
  	  },
  	  error: function(e) {
  		console.log(e.message);
  	  }
  	});
} );


$('#cat-grid tbody').on( 'click', '#delete-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    
    $.SmartMessageBox({
        title : "Alert!",
        content : "Are your Sure To Delete Column ?",
        buttons : '[No][Yes]'
    }, function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "deleteCatCol",
        	  	  type: "get",
        	  	  data:"colid="+data[0]+"&catId="+data[3]+"&columnName="+data[2],
        	  	  success: function(html){
					  dataTable.ajax.reload();
        	  		$.smallBox({
        				title : "Column Successfully Deleted.",
        				color : "#00802b",
        				iconSmall : "fa fa-thumbs-up bounce animated",
        				timeout : 4000
        				});
        		  //dataTable.ajax.reload();
        	  	  }
        	  	});
        }
        if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
    });

});


});

$('#btn-addcol').on( 'click', function () {
	var uid =$('#uid').val();
	console.log('uid::'+uid)
	$.ajax({
		  url:'getCategoryByManager',
		  type: 'GET',
		  data: {uid:uid},
		  success: function(data) {
			  var msg=data;
		  		var html="<option value='0'>All Category</option>";
		  		var i=0;
		  		for(i=0;i<msg.length;i++)
		  			{
		  			    var val='"'+msg[i][1]+'"';
		  				html+="<option value="+msg[i][0]+">"+msg[i][1]+"</option>";
		  			}
		  		$('#catCol').html(html);
		  		//getLeadParameter();
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
});

dataTable.on( 'order.dt search.dt', function () {
	 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
       cell.innerHTML = i+1;
   } );
} ).draw(); 

function saveChangeCategory()
{
var catid=$('#catid').val();
var catname=$('#catval').val();
var uid=$('#uid').val();
var editLPState=$('#editLPState').val();

$.ajax({
	  url: 'UpdateSuccessState',
	  type: 'GET',
	  data: {
		  catid :catid,
		  catname:catname.replace(/ /g,"_"),
		  compid :uid,
		  leadStatId:editLPState
	   },
	  success: function(data) {
			$.smallBox({
				title : "State Successfullt Updated.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				});
			$('#myModal').modal('hide');
		    dataTable.ajax.reload();
		  
	  },error: function(e) {
		console.log(e.message);
	  }
	  
	});
}

function saveCategory(){
	var catId=$('#catCol').val();
	var catname=$('#colNameAdd').val();
	var uid=$('#uid').val();
	
	if(catId=='0'){
		 $.smallBox({
				title : "Select Category",
				color : "#C46A69",
				iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
				});
	}else{
		if(catname==''){
			 $.smallBox({
					title : "Enter ColumnName",
					color : "#C46A69",
					iconSmall : "fa fa-times fa-2x fadeInRight animated",
	                timeout : 4000
					});
		}else{
			var format = /[!@#$%^&*_+\-=\[\]{};':"\\|,.<>\/?]+/;
			if(format.test(catname)){
				$.smallBox({
					title : "Remove Special Characters",
					color : "#C46A69",
					iconSmall : "fa fa-times fa-2x fadeInRight animated",
	                timeout : 4000
					});
			} else {
				$.ajax({
					  url: 'saveCatColumns',
					  type: 'GET',
					  data: {catId:catId,columnName:catname,managerId:uid},
					  success: function(data) {
						  
						  if(data=='1'){
							  console.log('success'+ data)
							//  dataTable.ajax.reload();
							  $.smallBox({
									title : "Field Already Exist",
									color : "#e60000",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
									});
						  }else{
							  console.log('success'+ data)
							  $("#catNameAdd").val("");
							  $('#myModal1').modal('hide');
							  dataTable.ajax.reload();
							  $.smallBox({
									title : "Column Successfully Added.",
									color : "#296191",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
									});
							  $('#colNameAdd').val('');
						  }
					  },
					  error: function(e) {
						console.log(e.message);
					  }
					});
			}
			
		}
		
	}
}


</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Manage Extra Fields</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger" id="btn-addcol" data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Extra Field</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Category Name</th>
							<th>Field Name</th>
							<th>Action</th>
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Status</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         
        <table>
        <tr>
        <td>Id&nbsp;</td>
        <td> <input type="text" id="catid" class="form-control" disabled="disabled"><br></td>
        </tr>
        <tr>
        <td>Select LeadStat&nbsp;:&nbsp;</td>
        <td><div> <select class="form-control"  id="editLPState"></select></div><br></td>
        </tr>
        
        <tr>
        <td>Name&nbsp;</td>
        <td> <input type="text" id="catval" class="form-control"></td>
        </tr>
        <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        
        
        %>
        
        </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeCategory();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Fields</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         
        <table>
        <tr>
        <td>Select Category&nbsp;:&nbsp;</td>
        <td> <select class="form-control"  id="catCol"></select><br></td>
        </tr>
        	
        <tr>
        <td>Field Name&nbsp;:&nbsp;</td>
        <td> <input type="text" id="colNameAdd" class="form-control" placeholder="Enter Field Name"><br></td>
        </tr>
        
        <%
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser1 = (MediUser) auth.getPrincipal();
		long id1=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id1+"'>");
        %>
        
        </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveCategory();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Column</button>
      </div>
    </div>
  </div>
</div>

</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
 


<script>
var dataTable=null;
$(document).ready(function(){
	
	
	dataTable= $('#cat-grid').DataTable( {
	        "ajax": "GetContact",
	        "columns": [
	            { "data": "contactId" },
	            { "data": "contactPersonName" },
	            { "data": "emailId" },
	            { "data": "contactNo" },
	            { "data": " " },
	       ],
	       "columnDefs": [ {
	            "targets": -1,
	            "data":null,
	            "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal1'><span class='glyphicon glyphicon-edit' aria-hidden='true' ></span>   Edit   </button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>&nbsp;&nbsp;"
	        }],
	    } );
	  $('#cat-grid tbody').on( 'click', '#edit-btn', function () {
	    	
			 
		  var  data = dataTable.row( $(this).parents('tr') ).data();
			$('#cid').val(data["contactId"]);
			$('#cname').val(data["contactPersonName"]);
			$('#email').val(data["emailId"]);
			$('#cno').val(data["contactNo"])
			
		} );
	  
	  $('#cat-grid tbody').on( 'click', '#delete-btn', function () {
	    	
			 
		  var  data = dataTable.row( $(this).parents('tr') ).data();
			var cid=data["contactId"];
			$.ajax({
				url:'deleteContactInfo',
				type:'get',
				data:'cid='+cid,
				success:function(data)
				{
					dataTable.ajax.reload();
					$.smallBox({
		 				title : "Contact is Successfully Deleted.",
		 				color : "#296191",
		 				iconSmall : "fa fa-thumbs-up bounce animated",
		 				timeout : 4000
		 				});
				},
			error:function(e)
			{
				console.log(e.message);
			}
			})
			
			
		} );
	  dataTable.on( 'order.dt search.dt', function () {
			 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		        cell.innerHTML = i+1;
		    } );
		} ).draw();
	  
});

function saveChangeContact()
{
   var cid=	$('#cid').val();
   var cname=$('#cname').val();
   var email=$('#email').val();
   var cno=$('#cno').val();
   if(cno=="")
   {
   document.getElementById('cno').style.borderColor='red';
   }
   else
   {
   $.ajax({
	   url:'updateContactInfoDetail',
	   type:'get',
	   data:{id:cid,name:cname,email:email,cno:cno},
	   success:function(data){
		   var msg=data;
		   $('#myModal1').modal('hide');
		   dataTable.ajax.reload();
		   $.smallBox({
 				title : "Contact is Successfully Updated.",
 				color : "#296191",
 				iconSmall : "fa fa-thumbs-up bounce animated",
 				timeout : 4000
 				});		   
	   },
	   error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
	   
	   
	   
   });
   }
}

function addContactInfo()
{
		
	
	   var cname=$('#ncname').val();
	   var email=$('#nemail').val();
	   var cno=$('#ncno').val();
	   
	   if(cno=="")
	   {
	   document.getElementById('ncno').style.borderColor='red';
	   }
	   else
	   {	   $.ajax({
		   url:'addContactInfo',
		   type:'get',
		   data:{cname:cname,email:email,cno:cno},
		   success:function(data){
			   var msg=data;
			   $('#myModal').modal('hide');
	  	  		$("#ncname").val("");
	  	  		$("#nemail").val("");
	  	  		$("#ncno").val("");
			   dataTable.ajax.reload();
			   $.smallBox({
	  				title : "Contact is Successfully Added.",
	  				color : "#296191",
	  				iconSmall : "fa fa-thumbs-up bounce animated",
	  				timeout : 4000
	  				});

		   },
		   error: function(e) {
		 		//called when there is an error
		 		console.log(e.message);
		 	  } 
	   });
	   }
}
</script>

</head>

<body>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser" data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Contact Manage</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"   data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span> New Contact </button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>Contact ID</th>
							<th>Contact Person Name</th>
							<th>Email</th>
							<th>Mobile No</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>

			</div>

		</div>

	</div>
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:35%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Contact Info</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Id</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="cid" class="form-control" readonly="readonly"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Person Name</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="cname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Email Id</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="email" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Number</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="cno" class="form-control"></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeContact();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:35%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Contact Info</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Person Name</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="ncname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Email Id</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="nemail" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Number</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="ncno" class="form-control"></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="addContactInfo()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
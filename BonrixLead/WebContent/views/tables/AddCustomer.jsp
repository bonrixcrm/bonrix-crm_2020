<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%--   <%@include file="/index.jsp"%>  --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script src=js/jquery.js></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
<script src=js/fnReloadAjax.js></script>
 
 
<style>
#validity{z-index:1151 !important;}
</style>

<script>

var dataTable=null;
$(document).ready(function() {
	
		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
	         dataTable = $('#cat-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetLeadToAddCustomer",
	           /*  "columnDefs": [ {
	                "targets": -1,
	                "data": null,
	                "defaultContent": "<button>Click!</button>"
	            } ], */
 "fnCreatedRow": function( nRow, aData, iDataIndex ) {

	        	$.ajax({
	        	  	  url: "compareLeadContact",
	        	  	  type: "get",
	        	  	  data:"cno="+aData[ 3 ],
	        	  	  success: function(html){
	        	  	   var data=html;
	        	  	   if(data==0)
	        	  		   {
	        	  		 $('td:eq(5)', nRow).html('<button id="Add-btn" class="btn btn-success btn-xs">Add Customer</button>');
	        	  		   }
	        	  	   else
	        	  		   {
	        	  		 $('td:eq(5)', nRow).html('<button id="View-btn" class="btn btn-danger btn-xs" disabled>Add Customer</button>');
	        	  		   
	        	  		   }
	        	  	   

	        	  	  }
	        	  	}); 	
	        },
	        } );
	         $.fn.dataTableExt.sErrMode = "console";
	         $('#cat-grid tbody').on( 'click', '#Add-btn', function () {
	        	 var data = dataTable.row( $(this).parents('tr') ).data();
	        	// alert(data[3])
	        	 $.ajax({
	        	  	  url: "addLeadContact",
	        	  	  type: "get",
	        	  	  data:{name:data[1],email:data[2],cno:data[3]},
	        	  	  success: function(html){
	        	  	   // $("#results").append(html);
	        	  	   var data=html;
	        	  	
	        	 	 dataTable.ajax.reload();
	        	  	  }
	        	  	}); 

	        	} );    
} );




</script>
<script type="text/javascript">
</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Customer Manage</h2>
			<div class="widget-toolbar" role="menu">			
					</div>
		</header>
		<div>
			<div class="jarviswidget-editbox"></div>
				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Mobile No</th>
							<th>Created Date</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
			<ul>
		
		</div>
</html>

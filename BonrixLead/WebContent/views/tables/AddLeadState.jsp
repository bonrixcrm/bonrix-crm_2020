<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <%
        Authentication auth3 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser3 = (MediUser) auth3.getPrincipal();
		long id3=currentUser3.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id3+"'>");
        %>
<script>
var dataTable=null;
$(document).ready(function() {
	var uid=$('#uid').val();
	 dataTable = $('#cat-grid').DataTable( {
        "processing": true,
        "serverSide": false,
        "ajax": "GetAssignLeadState",
            "columnDefs": [ {
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'>  <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>    Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"
            }],
            "order": [[ 0, 'desc' ]]
    } );
	
	$('#cat-grid tbody').on( 'click', '#delete-btn', function () {
	    var data = dataTable.row( $(this).parents('tr') ).data();
	    $.ajax({
	  	  url: "deleteState",
	  	  type: "get",
	  	  data:"statetid="+data[ 0 ],
	  	  success: function(html){
	  	 dataTable.ajax.reload();		
	  	  }
	  	}); 
	} );
	
	$('#cat-grid tbody').on( 'click', '#edit-btn', function () {
	    var data = dataTable.row( $(this).parents('tr') ).data();
	    $('#stateid').val(data[0]);
	    $('#stateval').val(data[1]);
	} );
	dataTable.on( 'order.dt search.dt', function () {
		 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        cell.innerHTML = i+1;
	    } );
	} ).draw();
} );

function saveChangeState()
{
	var uid= $('#uid').val();
	var statename= $('#stateval').val();
	var stateid= $('#stateid').val();
	 $.ajax({
		  url: 'UpdateState',
		  type: 'GET',
		  data: {stateid :stateid,statename:statename.replace(/ /g,"_"),compid :uid},
		  success: function(data) {
			  $('#myModal').modal('hide');
			  dataTable.ajax.reload();
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		}); 
}

function saveLeadState()
{
	var statename=$('#stname').val();
	var uid=$('#uid').val();
	 $.ajax({
		  url: 'AddState',
		  type: 'GET',
		  data: {statename:statename.replace(/ /g,"_"),compid :uid},
		  success: function(data) {
			  $('#myModal1').modal('hide');
			  dataTable.ajax.reload();
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		}); 
}
</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Lead State Manage</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Lead State</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Name</th>
							<th></th>
							
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Lead State</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         
        <table>
        <tr>
        <td>Id</td>
        <td> <input type="text" id="stateid" class="form-control" disabled="disabled"><br></td>
        </tr>
        <tr>
        <td>Name</td>
        <td> <input type="text" id="stateval" class="form-control "></td>
        </tr>
        <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        
        
        %>
        
        </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary " onclick="saveChangeState();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span>    Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Lead State</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         
        <table>
        <tr>
        <td>Lead State Name</td>
        <td> <input type="text" id="stname" class="form-control"><br></td>
        </tr>
        <%
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser1 = (MediUser) auth.getPrincipal();
		long id1=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id1+"'>");
        %>
        
        </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveLeadState();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>    Add Lead State</button>
      </div>
    </div>
  </div>
</div>

</html>

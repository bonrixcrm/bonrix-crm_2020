<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script src=js/jquery.js></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
<script src=js/fnReloadAjax.js></script>
<script>
var dataTable=null;
var  dataTableDyn=null;
$(document).ready(function() {
 
	$.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
	    console.log(message);
	};
	var uid=$('#uid').val();
	$.ajax({
	  	  url: 'GetCategory',
	  	  type: 'GET',
	  	  data: 'compid='+uid,
	  	  success: function(data) {
	  		
	  		var msg=data;
	  		var html="<option value='0'>All Category</option>";
	  		var i=0;
	  		
	  		for(i=0;i<msg.length;i++)
	  			{
	  		
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#category').html(html);
	  		
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );
	
	
	$.ajax({
	  	  url: 'GetTelecaller',
	  	  type: 'GET',
	  	  success: function(data) {
	  		
	  		var msg=data;
	  		var html="<option value='Telecaller'>Select Telecaller</option>";
	  		var i=0;
	  		
	  		for(i=0;i<msg.length;i++)
	  			{
	  		
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#telecaller').html(html);
	  		
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );
    
	var html="";
	  $.ajax({
		  url: 'GetLeadParameter',
		  success: function(data) {
			//alert(data);  
			for(i=0;i<data.length;i++)
				{
				var st=data[i][4];
			//	alert(st)
				
				html+="<tr><td align='center'><input type='checkbox' class='group1' value='"+data[i][0]+"'/></td>";
				html+="<td>"+data[i][1]+"</td>";
				html+="<td class='hidden-phone'>"+data[i][2]+"</td><td class='hidden-phone'><span class='label label label-info'>"+data[i][3]+"</span></td>";
				html+="<td class='hidden-phone'>"+data[i][9]+"</td><td class='hidden-phone'>"+data[i][10]+"</td></tr>";
				
				}
			
			$('#tblbdy').html(html);
			dataTable=$('#tbl-grid').DataTable({ "bSort" : false});
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} ); 
    
} );

function GetLeadByCategory()
{
	var html="";
	var cat=$("#category").val();
	var assign=$("#AUAll").val();
//alert(cat+"  "+assign);  
	  $.ajax({
		  url: 'GetLeadByCategoryId',
		  data:{cat:cat,assign:assign},
		  success: function(data) {
			//alert("Hi"+data.length); 
			if(data.length!=0)
				{
				html+="<table id='tbl-grid'  class='table table-striped table-bordered' cellspacing='0' width='100%'>";
				html+="<thead><tr><th ><input type='checkbox' name='vehicle' class='my' value='All' onchange='selectALl()'></th>";
				 html+="<th>Name</th><th>Email</th><th>Mobile No.</th><!--  <th>Category</th> --><th>Company</th>";
				html+="<th>Location</th> </tr></thead><tbody id='tblbdy'>";
				
			for(i=0;i<data.length;i++)
				{
				var st=data[i][4];
			//	alert(st)
				
html+="<tr><td align='center'><input type='checkbox' class='group1' value='"+data[i][0]+"'/></td>";		
html+="<td>"+data[i][1]+"</td>";
html+="<td class='hidden-phone'>"+data[i][2]+"</td><td class='hidden-phone'><span class='label label label-info'>"+data[i][3]+"</span></td>";
html+="<td class='hidden-phone'>"+data[i][9]+"</td><td class='hidden-phone'>"+data[i][10]+"</td></tr>";			
				}
			html+="</tbody></table>";  
			
			$('#dataTbl').html(html);
			dataTable=null;
			dataTableDyn=$('#tbl-grid').DataTable({ "bSort" : false});
			dataTableDyn.ajax.reload();
				}
			else
				{
				//	alert("No Data Found");
					$('#tblbdy').html("");
					dataTable=null;
					dataTable=$('#tbl-grid').DataTable({ "bSort" : false});
					dataTable.ajax.reload();
				}
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );
}
function selectALl()
{
	var values = $('input:checkbox:checked.my').map(function () {
		return this.value;
		  }).get();
	
	if(values=='All')
	  $(".group1").prop('checked', true);
	else
	  $(".group1").prop('checked', false);
}

var checkVals=null;
function changeData()
{
	 checkVals = $('input:checkbox:checked.group1').map(function () {
		return this.value;
		  }).get(); // ["18", "55", "10"]
		// alert('Hello....'+checkVals);
		  var tcid=$("#telecaller").val();
		  console.log(checkVals);
		  var ct=0;
		  var i=0;
		  for(i=0;i<checkVals.length;i++)
			  {
			  	console.log(checkVals[i]);
			  	 $.ajax({
				  	  url: 'AssignLeadToTelecaller',
				  	  type: 'GET',
				  	  data:{tcid:tcid,Cvale:checkVals[i]},
				  	  success: function(data) {
				  		
				  		$.ajax({
							  url: 'GetLeadParameter',
							  success: function(data) {
								//alert(data); 
								ct++;
								$("#AUAll").val('All').change();
								$("#category").val('0').change();
								$("#telecaller").val('Telecaller').change();
								var html=null;
								html+="<table id='tbl-grid'  class='table table-striped table-bordered' cellspacing='0' width='100%'>";
								html+="<thead><tr><th ><input type='checkbox' name='vehicle' class='my' value='All' onchange='selectALl()'></th>";
								 html+="<th>Name</th><th>Email</th><th>Mobile No.</th><!--  <th>Category</th> --><th>Company</th>";
								html+="<th>Location</th> </tr></thead><tbody id='tblbdy'>";
								for(i=0;i<data.length;i++)
									{
									var st=data[i][4];
								//	alert(st)
									
									html+="<tr><td align='center'><input type='checkbox' class='group1' value='"+data[i][0]+"'/></td>";
									html+="<td>"+data[i][1]+"</td>";
									html+="<td class='hidden-phone'>"+data[i][2]+"</td><td class='hidden-phone'><span class='label label label-info'>"+data[i][3]+"</span></td>";
									html+="<td class='hidden-phone'>"+data[i][9]+"</td><td class='hidden-phone'>"+data[i][10]+"</td></tr>";
									
									}
								html+="</tbody></table>";
								$('#dataTbl').html(html);
								dataTable=$('#tbl-grid').DataTable({ "bSort" : false});
								dataTable.ajax.reload();
							  },
							 error: function(e) {
								//called when there is an error
								console.log(e.message);
							  }
							} ); 
			
				  		
				  	  },
					 error: function(e) {
				 		//called when there is an error
				 		console.log(e);
				 	  }
					} );
			
				  		
				  		
			  	if(ct!=0)
	  			{
	  			alert(ct+" Leads Assign Successfully...");
	  			}
			  }
		 
}

function CloseLead()
{
	var checkVal = $('input:checkbox:checked.group1').map(function () {
		return this.value;
		  }).get(); // ["18", "55", "10"]
		// alert('Hello....'+checkVal);
		  var tcid=$("#telecaller").val();
		  console.log(checkVal);
		  var i=0;
		  var ct=0;
		  for(i=0;i<checkVal.length;i++)
			  {
			  	console.log(checkVal[i]);
			  	 $.ajax({
				  	  url: 'CloseLead',
				  	  type: 'GET',
				  	  data:{Cvale:checkVal[i]},
				  	  success: function() {
				  		  
				  		ct++;
				  		alert(ct)
				  		 $.ajax({
							  url: 'GetLeadParameter',
							  success: function(data) {
								//alert(data); 
								$("#AUAll").val('All').change();
								$("#category").val('0').change();
								$("#telecaller").val('Telecaller').change();
								var html=null;
								html+="<table id='tbl-grid'  class='table table-striped table-bordered' cellspacing='0' width='100%'>";
								html+="<thead><tr><th ><input type='checkbox' name='vehicle' class='my' value='All' onchange='selectALl()'></th>";
								 html+="<th>Name</th><th>Email</th><th>Mobile No.</th><!--  <th>Category</th> --><th>Company</th>";
								html+="<th>Location</th> </tr></thead><tbody id='tblbdy'>";
								for(i=0;i<data.length;i++)
									{
									var st=data[i][4];
								//	alert(st)
									
									html+="<tr><td align='center'><input type='checkbox' class='group1' value='"+data[i][0]+"'/></td>";
									html+="<td>"+data[i][1]+"</td>";
									html+="<td class='hidden-phone'>"+data[i][2]+"</td><td class='hidden-phone'><span class='label label label-info'>"+data[i][3]+"</span></td>";
									html+="<td class='hidden-phone'>"+data[i][9]+"</td><td class='hidden-phone'>"+data[i][10]+"</td></tr>";
									
									}
								html+="</tbody></table>";
								$('#dataTbl').html(html);
								dataTable=$('#tbl-grid').DataTable({ "bSort" : false});
								dataTable.ajax.reload();
							  },
							 error: function(e) {
								//called when there is an error
								console.log(e.message);
							  }
							} ); 
			
				  		
				  	  },
					 error: function(e) {
				 		//called when there is an error
				 		console.log(e);
				 	  }
					} );
			  }
		  
		  if(!(ct==0))
			{
			alert(ct+" Leads Deleted Successfully...");
			}
		 
	
	}
</script>
<body>
<div class="row">
<div class="col-md-6"><b>Searching Criteria</b></div>
 <div class="col-md-6"><b>Change Lead Role</b></div>
</div>
<div class="row">

  <div class="col-md-6">
  <!-- style="width:15%;float:left;" -->
   <select class="form-control" style="float: left; width: 30%;" id="category" onchange="GetLeadByCategory()"></select>

<!-- style="width:15%;float:left;margin-left: 29px;" -->
<select class="form-control " style="float: left; width: 30%;" id="AUAll" onchange="GetLeadByCategory()">
<option value="All">All</option><option value="Assign">Assign</option><option value="Unassigned">Unassigned </option></select>
  </div>
  
  <div class="col-md-6">
  <!-- <select class="form-control"  id="telecaller" style="width: 30%;"></select> -->
 
   <select class="form-control"  id="telecaller" style="width: 30%;display: inline;"></select>
 
    <button type="button" class="btn btn-default" onclick="changeData()">Assign Leads</button>
  
 
    <button type="button" class="btn btn-default" onclick="CloseLead()">Delete Leads</button>
  
  

</div>
<!-- <button type="button" class="btn btn-success"  onclick="changeData()">Change</button>
  
  <button type="button" class="btn btn-success"  onclick="changeData()">Change</button> -->
  


<hr>
</div>
<!-- class="table table-striped table-bordered" -->
<div style="margin-top: 2%;" id="dataTbl">
<table id="tbl-grid"  class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                 <th ><input type='checkbox' name='vehicle' class="my" value='All' onchange="selectALl()"></th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile No.</th>
               <!--  <th>Category</th> -->
                <th>Company</th>
                <th>Location</th>
            </tr>
        </thead>
       <tbody id="tblbdy">
            </tbody></table></div>
            
             <%
        Authentication auth2 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser2 = (MediUser) auth2.getPrincipal();
		long id=currentUser2.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %>
</body>
</html>
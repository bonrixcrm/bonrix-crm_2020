<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
table.dataTable.select tbody tr,
table.dataTable thead th:first-child {
  cursor: pointer;
}
</style>
</head>

<script>
var cat=null;
var assign=null;
var tel=null;
var table=null;
var dueDate="";
var leadstate=null;
/*Data Table COde */
 
 
 $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
    console.log(message);
}

function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

$(document).ready(function (){
	var uid=$('#uid').val();
	$.ajax({
	  	  url: 'GetCategory',
	  	  type: 'GET',
	  	  data: 'compid='+uid,
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option value='0'>All Category</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#category').html(html);
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
	
	$.ajax({
	  	  url: 'GetTelecaller',
	  	  type: 'GET',
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option value='0'>All Telecaller</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#telecaller').html(html);
	  		$('#telecaller1').html(html);
	  		generate();
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
	
	$.ajax({
	  	  url: 'AssignLeadStateGet',
	  	  type: 'GET',
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option value='0'>All LeadState</option> <option value='None'>None</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][1]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#leadstate').html(html);
	  		generate();
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
   });
function reloadtable(){	
	 $('input:checkbox').removeAttr('checked');
	 cat=$("#category").val();
	 assign=$("#AUAll").val(); 
	 tel=$("#telecaller1").val(); 
	 leadstate=$("#leadstate").val();
	 dueDate=dueDate;
	 table.ajax.reload();
	}
   function generate()
   {
	     cat=$("#category").val();
		 assign=$("#AUAll").val(); 
		 tel=$("#telecaller1").val();
		 leadstate=$("#leadstate").val();
	   var rows_selected = [];
	    table = $('#example').DataTable({
	    	//"processing": true,
	        "serverSide": true, 
	        //  "searching": false,  
	        //  "bLengthChange": false, 
	          "bDestroy": true,
	         "ajax": {"url":"GetLeadParameterForSearch",	      
	 	        "data": function ( d ) {
	 	        	 d.cat =cat;
		             d.assign=assign;
		             d.tel=tel;
		             d.dueDate=dueDate;
		             d.leadstate=leadstate;
	           }
	 	        },
	 	        
	 	     "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
 	         'columnDefs': [{
	         'targets': 0,
	         'searchable':false,
	         'orderable':false,
	         'width':'1%',
	         'className': 'dt-body-center',
	         'render': function (data, type, full, meta){
	             return '<input type="checkbox">';
	         }
	      }],
	      'order': [1, 'asc'],
	      'rowCallback': function(row, data, dataIndex){
	         var rowId = data[0];
	         if($.inArray(rowId, rows_selected) !== -1){
	            $(row).find('input[type="checkbox"]').prop('checked', true);
	         }
	      }
	   });

	   $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
	      var $row = $(this).closest('tr');
	      var data = table.row($row).data();
	      var rowId = data[0];
	      var index = $.inArray(rowId, rows_selected);
	      if(this.checked && index === -1){
	         rows_selected.push(rowId);
	      } else if (!this.checked && index !== -1){
	         rows_selected.splice(index, 1);
	      }
	      updateDataTableSelectAllCtrl(table);
	      e.stopPropagation();
	   });

	   $('#example').on('click', 'tbody td, thead th:first-child', function(e){
	      $(this).parent().find('input[type="checkbox"]').trigger('click');
	   });

	   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
	      if(this.checked){
	         $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
	      } else {
	         $('#example tbody input[type="checkbox"]:checked').trigger('click');
	      }
	      // Prevent click event from propagating to parent
	      e.stopPropagation();
	   });

	   table.on('draw', function(){
	      updateDataTableSelectAllCtrl(table);
	   });
	   
	    $('#btn-close').on('click', function(e){
	      $.each(rows_selected, function(index, rowId){
	      var tcid=$("#telecaller").val();
	    	  $.ajax({
			  	  url: 'AssignLeadToTelecaller',
			  	  type: 'GET',
			  	  data:{tcid:tcid,Cvale:rowId},
			  	  success: function(data) {
			  		 location.reload();
				  		//reloadtable()
			  	  },
				 error: function(e) {
			 		console.log(e);
			 	  }
				} );
	      }); 
	       $.smallBox({
				title : "Lead Successfully Assign To "+$("#telecaller :selected").text()+".",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				}); 
	      e.preventDefault();
	   }); 
	    
	    
	    $('#btn-delete').on('click', function(e){
		      $.each(rows_selected, function(index, rowId){
		      var tcid=$("#telecaller").val();
		    	  $.ajax({
				  	  url: 'DeleteLeadBeforeAssign',
				  	  type: 'GET',
				  	  data:{tcid:tcid,Cvale:rowId},
				  	  success: function(data) {
				  		 location.reload();
					  		//reloadtable()
				  	  },
					 error: function(e) {
				 		console.log(e);
				 	  }
					} );
		      }); 
		       $.smallBox({
					title : "Lead Successfully Deleted",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
		      e.preventDefault();
		   });
   }



   function ajaxindicatorstart(text)
   {
   	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
   	jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="img/ajax-loader.gif"><div>Please Wait.....</div></div><div class="bg"></div></div>');
   	}

   	jQuery('#resultLoading').css({
   		'width':'100%',
   		'height':'100%',
   		//'position':'fixed',
   		'margin-left':'0%',
   		'margin-top':'0%',
   		'z-index':'10000000',
   		'top':'20',
   		'left':'20',
   		'right':'0',
   		'bottom':'0'
   	});

   	jQuery('#resultLoading .bg').css({
   		'background':'#858585',
   		'opacity':'0.7',
   		'width':'100%',
   		'height':'100%',
   		'position':'absolute',
   		'top':'0'
   	});

   	jQuery('#resultLoading>div:first').css({
   		'width': '250px',
   		'height':'75px',
   		'text-align': 'center',
   		'position': 'fixed',
   		'margin-left':'50%',
   		'margin-top':'20%',
   		'top':'0',
   		'left':'0',
   		'right':'0',
   		'bottom':'0',
   		'font-size':'16px',
   		'z-index':'10',
   		'color':'#ffffff'

   	});

       jQuery('#resultLoading .bg').height('100%');
          jQuery('#resultLoading').fadeIn(300);
       jQuery('body').css('cursor', 'wait');
   }
   
   function ajaxindicatorstop()
   {
  	 jQuery('#resultLoading .bg').height('100%');
          jQuery('#resultLoading').fadeOut(300);
       jQuery('body').css('cursor', 'default');

   }
   jQuery(document).ajaxStart(function () {
  		//show ajax indicator
  ajaxindicatorstart('loading data.. please wait..');
  }).ajaxStop(function ()
  {
  //hide ajax indicator
  ajaxindicatorstop();
  }).ajaxError(function() {
     // alert( "Handler for .error() called." );
    // window.location="home.html"
  })
   jQuery.ajax({
  	   global: false,
  	   // ajax stuff
  	});

  
</script>
<body>

<div class="row">
<div class="col-md-6" align="left"><b>Searching Criteria</b></div>
 <div class="col-md-6" align="center"><b>Change Lead Role</b></div>
</div>
<div class="row">

  <div class="col-md-7">
  <!-- style="width:15%;float:left;" -->
   <select class="form-control" style="float: left; width: 25%;" id="category" onchange="reloadtable()"></select>

<!-- style="width:15%;float:left;margin-left: 29px;" -->
<select class="form-control " style="float: left; width: 25%;" id="AUAll" onchange="reloadtable()">
<option value="Unassigned">Unassigned </option>
<option value="Assign">Assign</option>
<option value="All">Assign-Unassigned</option>
</select>

   <select class="form-control"  id="telecaller1" style="float: left; width: 25%;display: inline;" onchange="reloadtable()"></select>

	 <select class="form-control"  id="leadstate" style="width: 25%;display: inline;" onchange="reloadtable()"></select>
  </div>
  
  <div class="col-md-5">
  <!-- <select class="form-control"  id="telecaller" style="width: 30%;"></select> -->
 
    <select class="form-control"  id="telecaller" style="width: 30%;display: inline;"></select>
    <button type="button" id="btn-close" class="btn btn-default">Assign Leads</button>
    <button type="button" id="btn-delete" class="btn btn-default">Delete Leads</button>
</div>
<!-- <button type="button" class="btn btn-success"  onclick="changeData()">Change</button>
  
  <button type="button" class="btn btn-success"  onclick="changeData()">Change</button> -->

<hr>
</div>
<!-- class="table table-striped table-bordered" -->
<div style="margin-top: 2%;" id="dataTbl">
<table id="example" class="table" cellspacing="0" width="100%">
   <thead>
      <tr>
         <th><input name="select_all" value="1" type="checkbox"></th>
         <th>Name</th>
         <th>Email</th>
         <th>Mobile No.</th>
         <th>Category</th>
         <th>Lead State</th>
          <th>Telecaller</th>
      </tr>
   </thead>
  
</table></div>
            <%
        Authentication auth2 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser2 = (MediUser) auth2.getPrincipal();
		long id=currentUser2.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %> 
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
    <%@page import="org.springframework.security.core.Authentication"%>
    <%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %>
        
<div class="row">
   <div class="col-xs-4 col-sm-2">
     <select class="form-control" id="category" ></select>
   </div>
   
         <div class="col-xs-4 col-sm-3">
			<button type="button" class="btn btn-default pull-right" id="daterange-btn">
				<i class="fa fa-calendar"></i> chose Date Range <i class="fa fa-caret-down"></i>
			</button>
		</div>
		<div class="col-xs-4 col-sm-3">
			<button type="button" class="btn btn-danger" onclick="downloadAudioFiles()">Download</button>
		</div>
</div><br>
        
       
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Sr No.</th>
                <th>Telecaller Name</th>
                <th>Audio File</th>
                 <th>Audio File</th>
            </tr>
        </thead>
    </table>
	
</body>
<script>

var test_stdate = new Date();
var test_endate = new Date();

var stdate = test_stdate.getFullYear() + "-" + (test_stdate.getMonth() + 1)
		+ "-" + test_stdate.getDate();
var endate = test_endate.getFullYear() + "-" + (test_endate.getMonth() + 1)
		+ "-" + test_endate.getDate();

var date = new Date();
var getDate = "NA";

$('#daterange-btn').daterangepicker({
					format : "yyyy-mm-dd",
					ranges : {
						'Today' : [ moment(), moment() ],
						'Yesterday' : [ moment().subtract(1, 'days'),
								moment().subtract(1, 'days') ],
						'Last 7 Days' : [ moment().subtract(6, 'days'),
								moment() ],
						'Last 30 Days' : [ moment().subtract(29, 'days'),
								moment() ],
						'This Month' : [ moment().startOf('month'),
								moment().endOf('month') ],
						'Last Month' : [
								moment().subtract(1, 'month').startOf(
										'month'),
								moment().subtract(1, 'month')
										.endOf('month') ]
					},
					startDate : moment().subtract(29, 'days'),
					endDate : moment()
				},
				function(start, end) {
					$('#daterange-btn').text(
							start.format('MMMM D, YYYY') + ' - '
									+ end.format('MMMM D, YYYY'));
				}, getDateRangeSuccessLog);

$('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
	stdate = picker.startDate.format('YYYY-MM-DD');
	endate = picker.endDate.format('YYYY-MM-DD');
	// alert("stdate::"+stdate+" "+"endate::"+endate)
});
function getDateRangeSuccessLog() {

}


function downloadAudioFiles() {
	var sDate = stdate;
	var eDate = endate;
	var tallycallerId = $("#category").val();
	console.log("sDate::"+sDate+"---"+"eDate::"+eDate+"---"+"tallycallerId::"+tallycallerId)
	$.ajax({
  	  url: "downloadAudioFilesAPI?telecallerId="+tallycallerId+"&stDate="+sDate+"&endDate="+eDate,
  	  type: 'GET',
	  success: function(data) {
  		console.log(data)
		
		/*var url = 'http://crmdemo.bonrix.in/Sound/'+data;
  		console.log("url:::"+url)*/
  	//	window.location = url;
		
  		//var url = data;
		/*var url = "http://localhost:8080/spring-hib/Sound/"+$("#uid").val()+"tar.gz";
  		console.log("url:::"+url)
  		window.location = url;*/
		
		var url = "http://crmdemo.bonrix.in/Sound/"+$("#uid").val()+".tar.gz";
  		console.log("url:::"+url)
  		window.location = url;
		
			
		/*$.ajax({
  			url: "deleteTarFile?url="+url,
  	  	    type: 'GET',
  	  	    success: function(data) {
  	  	    console.log("data:::"+data)
  	  	  }
  		});*/
  	  }
    });
	
}

$(document).ready(function() {
    $('#example').DataTable( {
        "ajax": {
            "url": "getAudioFileAPI",
            "dataSrc": ""
        },
        "columns": [
        	{ "data": "srNo" },
        	{ "data": "Telecaller_Name" },
            { "data": "Audio_File_Name" },
            { "data": "Audio_File_Name" }
        ],
        "rowCallback": function( row, data, index ) {
	         $('td:eq(3)', row).html( '<a type="button" target="_blank" href="http://crmdemo.bonrix.in/Sound/'+$("#uid").val()+'/'+data["Audio_File_Name"]+'" class="btn btn-labeled btn-warning btn-sm"><span class="btn-label"><i class="glyphicon glyphicon-save"></i></span>Download</a>' );
         },
    });
    
    $.ajax({
    	  url: 'getTallyCallerByCompanyId',
    	  type: 'GET',
    	  success: function(data) {
    		var msg=data;
    		var html="<option value='0'>All Tellycaller</option>";
    		for(var i=0;i<msg.length;i++){
    			var val='"'+msg[i][0]+'"';
    				html+="<option value="+val+">"+msg[i][3]+" "+msg[i][4]+"</option>";
    			}
    		$('#category').html(html);
  		  }
  		});
    
});


   function ajaxindicatorstart(text){
   	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
   	jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="img/ajax-loader.gif"><div>Please Wait.....</div></div><div class="bg"></div></div>');
   	}

   	jQuery('#resultLoading').css({
   		'width':'100%',
   		'height':'100%',
   		//'position':'fixed',
   		'margin-left':'0%',
   		'margin-top':'0%',
   		'z-index':'10000000',
   		'top':'20',
   		'left':'20',
   		'right':'0',
   		'bottom':'0'
   	});

   	jQuery('#resultLoading .bg').css({
   		'background':'#858585',
   		'opacity':'0.7',
   		'width':'100%',
   		'height':'100%',
   		'position':'absolute',
   		'top':'0'
   	});

   	jQuery('#resultLoading>div:first').css({
   		'width': '250px',
   		'height':'75px',
   		'text-align': 'center',
   		'position': 'fixed',
   		'margin-left':'50%',
   		'margin-top':'20%',
   		'top':'0',
   		'left':'0',
   		'right':'0',
   		'bottom':'0',
   		'font-size':'16px',
   		'z-index':'10',
   		'color':'#ffffff'

   	});

       jQuery('#resultLoading .bg').height('100%');
          jQuery('#resultLoading').fadeIn(300);
       jQuery('body').css('cursor', 'wait');
   }
   
   function ajaxindicatorstop()
   {
  	 jQuery('#resultLoading .bg').height('100%');
          jQuery('#resultLoading').fadeOut(300);
       jQuery('body').css('cursor', 'default');

   }
   jQuery(document).ajaxStart(function () {
  		//show ajax indicator
  ajaxindicatorstart('loading data.. please wait..');
  }).ajaxStop(function (){
  //hide ajax indicator
  ajaxindicatorstop();
  }).ajaxError(function() {
     // alert( "Handler for .error() called." );
    // window.location="home.html"
  })
   jQuery.ajax({
  	   global: false,
  	   // ajax stuff
  	});

</script>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fail Call Report</title>
<!-- <link rel=stylesheet type=text/css media=screen href=https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css />
<link rel=stylesheet type=text/css media=screen href=https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css /> -->
<link rel=stylesheet type=text/css media=screen href="css/dataTables.jqueryui.min.css"/>

</head>
<!-- <script src="js/daterangepicker/daterangepicker.js"></script>
 --><body>
<div class="row">
  <div class="col-xs-4 col-sm-2">
     <select class="form-control"  id="category" ></select>
  </div>
  <div class="col-xs-4 col-sm-2">
    <select id="cat" class="form-control"></select>  
  </div>
  
  <div class="col-xs-4 col-sm-2">
    <select id="callType" class="form-control">
      <option value="OutBound">OutBound</option>
      <option value="InBound">InBound</option>
    </select>  
  </div>
  
  <div class="col-xs-4 col-sm-3">
       <!--  <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
     
           <button type="button" class="btn btn-default pull-right" id="daterange-btn"  >
          <i class="fa fa-calendar"></i> chose Date Range
          <i class="fa fa-caret-down"></i>
    </button>               
</div>
  
  <div class="col-xs-4 col-sm-3">
     <button type="button" class="btn btn-danger" onclick="reloadData()" style="float:left;">Search</button>
     <button type="button" class="btn btn-danger" onclick="exportLeads21()" style="margin-left:5px;">Export All</button>
  </div>

</div>
<br/>
<div class="row">
  <div class="col-xs-4 col-sm-2">
    <select id="failStatus" class="form-control">
      <option value="ALL">Select Fail Status</option>
      <option value="Busy">Busy</option>
      <option value="Call Waiting">Call Waiting</option>
      <option value="Switched Off">Switched Off</option>
      <option value="Network Error">Network Error</option>
      <option value="Number Does not Exist">Number Does not Exist</option>
      <option value="Ringing not Response">Ringing not Response</option>
      <option value="Not Reachable">Not Reachable</option>
      <option value="Switched Off">Switched Off</option>
      <option value="Disconnect by Cust">Disconnect by Cust</option>
      <option value="Call Back">Call Back</option>
      <option value="Wrong Number">Wrong Number</option>
      <option value="Lead">Lead</option>
      <option value="Ring In">Ring In</option>
      <option value="Not Interested">Not Interested</option> 
    </select>  
  </div>
  <div class="col-xs-4 col-sm-2">
    <select id="leadStatus" class="form-control">
      <option value="ALL">Select Lead Status</option>
      <option value="Open">Open</option>
      <option value="Close">Close</option>
    </select>  
  </div>
  
  <div class="col-xs-6 col-sm-2">
  <button type="button" class="btn btn-primary" onclick="openFailCallLeads()" >Open/Close Leads</button>
  </div>
   <div class="col-xs-4 col-sm-2">
   <button type="button" class="btn btn-primary" onclick="addAutoDial()" >Add To AutoDial</button>
  </div>
  
</div>


 <p><br></p>
<!--  <div id="Mytbl">
 -->  <div id="">
 <table id="example" class="table table-striped table-bordered" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
             <th>Sr No.</th> 
				<th>Name</th> 
                <th>Mobile No.</th> 
                <th>Calling Time</th>
                <th>Call Durations(S)</th>   
                <th>Telecaller</th> 
                <th>Call Status</th>   
                <th>Call Type</th> 
                <th>Category</th> 
                <th>Fail Status</th> 
                <th>Lead Status</th> 
            </tr>
            </thead>   
    </table> 
 
 
 </div>
</body>
<!-- <script src="js/bootstrap-datetimepicker.js"></script>
 -->
<!-- <script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>  -->
<script>

/* var stdate=new Date(); 
var endate=new Date(); */

var test_stdate = new Date();
var test_endate = new Date();

var stdate = test_stdate.getFullYear() + "-" +(test_stdate.getMonth()+1) + "-" +test_stdate.getDate();
var endate = test_endate.getFullYear() + "-" +(test_endate.getMonth()+1) + "-" +test_endate.getDate();

var date = new Date();
var getDate="NA";
var dataTable=null;
var tcId="3";
var callType="";
var cat="0";
/* $(function () {
	  $('#datetimepicker1').datetimepicker({
	   	  defaultDate: new Date(), 
	     format: 'YYYY-MM-DD'
	    });
}); */


$('#daterange-btn').daterangepicker({
        	format: "yyyy-mm-dd",
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
           startDate: moment().subtract(29, 'days'),
          endDate: moment() 
        },
        function (start, end) {
          $('#daterange-btn').text(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        },getDateRangeSuccessLog
    );

$('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
 	 stdate=picker.startDate.format('YYYY-MM-DD');
	 endate=picker.endDate.format('YYYY-MM-DD');
	 
	 //alert("stdate::"+stdate+" "+"endate::"+endate)
});
function getDateRangeSuccessLog()
{
	
}

function exportLeads21(){
	//alert("stdate::"+stdate+" "+"endate::"+endate)
  //  document.location.href = 'exportLeadsFailCallLog?stdate='+stdate+'&endate='+endate+' ';	
	var teleid = $('#category').val();
	var catId = $('#cat').val();
	var failStatus = $('#failStatus').val();
	var leadStatus = $('#leadStatus').val();
	var callType=$('#callType').val();
	
    $.SmartMessageBox({
		title : "Alert!",
		content : "Export Report With Extra Parameters",
		buttons : '[No][Yes]'
	}, function(ButtonPressed) {
		if (ButtonPressed === "Yes") {
			document.location.href = 'exportLeadsFailCallLog?stdate='+stdate+'&endate='+endate+'&extrap=1&teleid='+teleid+'&catId='+catId+'&failStatus='+failStatus+'&leadStatus='+leadStatus+'&callType='+callType;
		}
		if (ButtonPressed === "No") {
			document.location.href = 'exportLeadsFailCallLog?stdate='+stdate+'&endate='+endate+'&extrap=0&teleid='+teleid+'&catId='+catId+'&failStatus='+failStatus+'&leadStatus='+leadStatus+'&callType='+callType;
		}
	});
}

function addAutoDial(){
	  
	
    var catId = $('#cat').val();
    
    if(catId=="All"){
    alert("Please select Category For add to AutoDial");
    }else{
    	
    	alert(catId);

    $.ajax({
		url : "addLeadsToAutoDialManagerAPI",
		type : "GET",
		data : {
			'catId' : catId
		},
		success : function(response) {
		//	alert(response);
			if(response=='1')
		//	 dataTable.ajax.reload();
		  	 $.smallBox({
					title : "Lead Successfully Added To AutoDial.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					});
		},
		error : function(xhr, status, error) {
			alert("get error");
			alert(xhr.responseText);
		}
	});
}
}



$( document ).ready(function() {
	getCat();
$.ajax({
  	  url: 'getTallyCallerByCompanyId',
  	  type: 'GET',
  	  success: function(data) {
  		
  		var msg=data;
  		var html="<option value='All'>All Tellycaller</option>";
  		var i=0;
  		   
  		for(i=0;i<msg.length;i++)
  			{
  		
  			var val='"'+msg[i][0]+'"';
  				html+="<option value="+val+">"+msg[i][3]+" "+msg[i][4]+"</option>";
  			}
  		$('#category').html(html);
  		tcId=$('#category').val();
  		getDate=$('#datetimepicker1').data('date');
  		//alert(getDate)
  		if(cat.toString=="")
  			cat="0";
  		
  	//	alert("stdate::"+stdate+" "+"endate::"+endate)
  	
  		dataTable=  $('#example').DataTable({
	        "ajax": {
	            "url": "GetInOutBoundFailCallLog",
	            "data": function ( d ) {
		            // d.getDate=getDate;
		             d.endate=endate;
  	 	             d.stdate=stdate;
		             d.tcId=tcId;
		             d.callType=$("#callType").val();
		             d.cat=$('#cat').val();
		             d.leadStatus=$('#leadStatus').val();
		     		 d.failStatus=$('#failStatus').val();
	           },    
	        },   
	      /*   "rowCallback": function( row, data, index ) {
		         $('td:eq(8)', row).html( '<a type="button" target="_blank" href="http://lead.admarkgps.in/Sound/'+data[8]+'" class="btn btn-labeled btn-warning"><span class="btn-label"><i class="glyphicon glyphicon-save"></i></span>Download</a>' );

	          }, */
	 	       "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
	        "language": {
	            "emptyTable": "No Data Found."
	          } ,
	          "sPaginationType" : "full_numbers",
			  "oLanguage" : {
			  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
			  },
			  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
			   "sDom": 'T<"clear">lfrtip',
			  "oTableTools" : {
			  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			  }
	         
    });
 dataTable.on( 'order.dt search.dt', function () {
 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
     cell.innerHTML = i+1;
 } );
} ).draw(); 
 
 $('#example').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );
		} ) .DataTable();
  		
  	  },
	 error: function(e) {
 		//called when there is an error
 		console.log(e.message);
 	  }
	} );
	
	

	
});

function getCat()
{
	$.ajax({
		  url: 'GetCategoryById',
		  type: 'GET',
		  success: function(data) {
			
			var msg=data;
			var html="<option value='All'>All Category</option>";
			var i=0;
			
			for(i=0;i<msg.length;i++)
				{
			
				var val='"'+msg[i][0]+'"';
					html+="<option value="+val+">"+msg[i][1]+"</option>";
				}
			$('#cat').html(html);
			
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );	
	
}
	function reloadData()
	{
		//alert("fuyf")
		//getDate=$('#datetimepicker1').data('date');
	//	alert("stdate::"+stdate+" "+"endate::"+endate)
		stdate;
		endate;
		tcId=$('#category').val()
		callType=$("#callType").val();
		cat=$('#cat').val();
		leadStatus=$('#leadStatus').val();
		failStatus=$('#failStatus').val();
		dataTable.ajax.reload();
		
	}
	
	function openFailCallLeads(){
		var calltype=$("#callType").val();
		var leadStatus=$("#leadStatus").val();
		var failStatus=$("#failStatus").val();
		//alert("leadStatus:::"+leadStatus)
		
		if(leadStatus =='ALL'){
			$.smallBox({
				title : "Please Select Proper Lead Status",
				color : "#C46A69",
				iconSmall : "fa fa-times fa-2x fadeInRight animated",
				timeout : 4000
				});
		}else{
			var URL = "updateFailCallLeadState?endate="+endate+"&stdate="+stdate+"&tcId="+tcId+"&calltype="+calltype+"&cat="+cat+"&leadStatus="+leadStatus+"&failStatus="+failStatus
			console.log("URL:::"+URL);
			var stst = "";
			if(leadStatus=='Open'){
				stst='Closed';
			}else{
				stst='Opened';
			}
			
			$.ajax({
				  url: URL,
				  type: 'GET',
				  success: function(data) {
						$.smallBox({
							title : "Leads Successfully "+stst,
							color : "#296191",
							iconSmall : "fa fa-thumbs-up bounce animated",
							timeout : 4000
							});
					    dataTable.ajax.reload();
					  
				  },error: function(e) {
					console.log(e.message);
				  }
				  
				});
			reloadData();
		}
		
	}
</script>
</html>
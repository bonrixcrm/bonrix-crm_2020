<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- <link rel=stylesheet type=text/css media=screen href="css/dataTables.jqueryui.min.css"/> -->
<link rel="stylesheet" type="text/css"
	href="http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

</head>
<style>
table {
	table-layout: fixed;
}

td {
	overflow: hidden;
	text-overflow: ellipsis;
}
</style>
<!-- <script src="js/daterangepicker/daterangepicker.js"></script> -->
<body>
	<div class="row">
		<div class="col-xs-4 col-sm-2">
			<select class="form-control" id="category"></select>

		</div>

		<div class="col-xs-4 col-sm-2">
			<select id="cat" class="form-control">

			</select>
		</div>


		<div class="col-xs-4 col-sm-2">
			<select id="callType" class="form-control">
				<option value="OutBound">OutBound</option>
				<option value="InBound">InBound</option>

			</select>
		</div>

		<div class="col-xs-4 col-sm-3">
			<!--  <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>  -->

			<button type="button" class="btn btn-default pull-right"
				id="daterange-btn">
				<i class="fa fa-calendar"></i> chose Date Range <i
					class="fa fa-caret-down"></i>
			</button>
		</div>



		<div class="col-xs-4 col-sm-3">
			<button type="button" class="btn btn-danger" onclick="reloadData()" style="float:left;">Search</button>
			
			
			<button type="button" class="btn btn-danger" onclick="exportLeads21()" style="margin-left:5px;">Export All</button>

		</div>

	</div>

	<%
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id = currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='" + id + "'>");
	%>

	<p>
		<br>
	</p>
	<!--  <div id="Mytbl">
 -->
	<div id="">
		<table id="example" class="table table-striped table-bordered"
			style="font-weight: normal;" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Sr No.</th>
					<th>Name</th>
					<th>Mobile No.</th>
					<th>Email</th>
					<th>Remark</th>
					<th>State</th>
					<th>Category</th>
					<th>Calling Time</th>
					<th>Durations(S)</th>
					<th>Telecaller</th>
					<th>Lead State</th>
                    <th>Success State</th>
                    <th>Audio</th>
                     <th>FollowUps</th>
				</tr>
			</thead>
		</table>


	</div>
</body>

<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width: 70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">FollowUps Detail</h4>
      </div>
      <div class="modal-body" >
		   <table id="hist-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Calling Time</th>
							<th>Remark</th>
							<th>Shedule Time</th>
							<th>New Status</th>
							<th>Call Duration</th>
							<th>Call Status</th>
							<th>Fail Status</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<script>
	/* var stdate=new Date(); 
	 var endate=new Date(); */

	var test_stdate = new Date();
	var test_endate = new Date();

	var stdate = test_stdate.getFullYear() + "-" + (test_stdate.getMonth() + 1)
			+ "-" + test_stdate.getDate();
	var endate = test_endate.getFullYear() + "-" + (test_endate.getMonth() + 1)
			+ "-" + test_endate.getDate();

	var date = new Date();
	var getDate = "NA";
	var dataTable = null;
	var dtable=null;
	var tcId = "3";
	var callType = "";
	var cat = "0";

	/*  $(function () {
	 $('#datetimepicker1').datetimepicker({
	 defaultDate: new Date(), 
	 format: 'YYYY-MM-DD'
	 });
	 });  */

	$('#daterange-btn').daterangepicker({
						format : "yyyy-mm-dd",
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						},
						startDate : moment().subtract(29, 'days'),
						endDate : moment()
					},
					function(start, end) {
						$('#daterange-btn').text(
								start.format('MMMM D, YYYY') + ' - '
										+ end.format('MMMM D, YYYY'));
					}, getDateRangeSuccessLog);

	$('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
		stdate = picker.startDate.format('YYYY-MM-DD');
		endate = picker.endDate.format('YYYY-MM-DD');

		// alert("stdate::"+stdate+" "+"endate::"+endate)
		// dataTable.ajax.reload();
	});
	function getDateRangeSuccessLog() {

	}
	function exportLeads21(){
		$.SmartMessageBox({
			title : "Alert!",
			content : "Export Report With Extra Parameters",
			buttons : '[No][Yes]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "Yes") {
				document.location.href = 'exportLeadsCallLog?stdate='+stdate+'&endate='+endate+'&extrap=1 ';
			}
			if (ButtonPressed === "No") {
				document.location.href = 'exportLeadsCallLog?stdate='+stdate+'&endate='+endate+'&extrap=0 ';
			}
		});
	}
	
	function showfollowups(ld){
		var leadId=ld;
	//	console.log("leadId::"+leadId)
		$("#hist-grid").dataTable().fnDestroy();
		 /* dtable= $('#hist-grid').DataTable({
	 	        "processing": true,
	 	        "serverSide": true,
	 	        "bPaginate": true,
	 	        "ajax":{
	 	            "url": "getFollowupsHistory",
	 	            "type": "get",
	 	            "data": function ( d ) {
	 	        	   d.leadid=leadId;
	 	           }
	 	        }, 
	 	       "createdRow" : function(row, data,dataIndex) {
	 	    	   if(data[3]=='Jun 30, 1980 5:36:00 PM'){
	 	    		  $('td:eq(3)',row).html('NA');
	 	    	   }else{
	 	    		  $('td:eq(3)',row).html(data[3]);
	 	    	   }
				},
				"sPaginationType" : "full_numbers",
				"oLanguage" : {
					"sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
				},
				"sDom" : 'T<"clear">lfrtip',
		    "columnDefs": [{
	        "targets": -1,
	        "data":null,
	        "defaultContent": "<button id='play-btn' class='btn btn-success'>Audio File</button>"
	    }]
		   }); */
		   
		   dtable = $('#hist-grid').DataTable({
				"ajax" : {
			    "url"  : "getFollowupsHistory",
				"data" : function(d) {
					d.leadid=leadId;
				  },
				},
				"createdRow" : function(row, data,dataIndex) {
					if(data[3]=='Jun 30, 1980 5:36:00 PM'){
		 	    		  $('td:eq(3)',row).html('NA');
		 	    	   }else{
		 	    		  $('td:eq(3)',row).html(data[3]);
		 	    	   }
				},

				"lengthMenu" : [[10,25,50,100,250,500,1000,2000,5000,-1 ],[10,25,50,100,250,500,1000,2000,5000,"All" ]],
				"sPaginationType" : "full_numbers",
				"oLanguage" : {
					"sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
				},
				"sDom" : 'T<"clear">lfrtip',
				"columnDefs": [{
			        "targets": -1,
			        "data":null,
			        "defaultContent": "<button id='play-btn' class='btn btn-success'>Audio File</button>"
			    }]
			});
		 
	 	       $('#hist-grid tbody').on( 'click', '#play-btn', function () {
	 			  var  data = dtable.row( $(this).parents('tr') ).data();
	 			  console.log("sound::"+'Sound/'+data[8]+'')
	 		 	 var win = window.open('Sound/'+data[8]+''); 
	 			} );
	 	      dtable.on( 'order.dt search.dt', function () {
	 	    	 dtable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	 		    	     cell.innerHTML = i+1;	
	 		    	 });
	 		    	}).draw();
	 	      
	}

	$(document).ready(
			function() {
				getCat();
				$.ajax({
				    url : 'getTallyCallerByCompanyId',
					type : 'GET',
					success : function(data) {
								var date = new Date();
								var msg = data;
								var html = "<option value='All'>All Tellycaller</option>";
								var i = 0;
								for (i = 0; i < msg.length; i++) {
										var val = '"' + msg[i][0] + '"';
										html += "<option value="+val+">"+ msg[i][3] + " "+ msg[i][4] + "</option>";
									}
									$('#category').html(html);
									tcId = $('#category').val();
									getDate = $('#datetimepicker1').data('date');
										if (cat.toString == "")
											cat = "0";
										//  alert("stdate::"+stdate+" "+"endate::"+endate)
										dataTable = $('#example').DataTable({
															"ajax" : {
														    "url"  : "GetInOutBoundSuccessCallLog",
															"data" : function(d) {
																//  d.getDate=getDate;
																	d.endate = endate;
																	d.stdate = stdate;
																	d.tcId = tcId;
																	d.callType = $("#callType").val();
																	d.cat = $('#cat').val();
																},

															},
															"createdRow" : function(row, data,dataIndex) {
																$('td:eq(12)',row).html('<a type="button" target="_blank" href="http://crmdemo.bonrix.in/Sound/'+ $('#uid').val()+ "/"+ data[12]+ '" class="btn  btn-warning"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>');
																$('td:eq(13)', row).html('<button type="button" class="btn btn-primary" id="follow" data-toggle="modal" data-target="#myModal4" onclick="showfollowups('+data[13]+')">Follow Up </button>');
															},

															"lengthMenu" : [[10,25,50,100,250,500,1000,2000,5000,-1 ],[10,25,50,100,250,500,1000,2000,5000,"All" ]],
															"sPaginationType" : "full_numbers",
															"oLanguage" : {
																"sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
															},
															"sDom" : 'T<"clear">lfrtip',
															"oTableTools" : {
																"sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
																"aButtons" : [
																		{
																			"sExtends" : "pdf",
																			"sTitle" : "OutBound Success Call Log  Report" + " On "+ date.getDate()+ "-"+ (date.getMonth() + 1)+ "-"+ date.getFullYear()
																		},
																		{
																			"sExtends" : "csv",
																			"sTitle" : "OutBound Success Call Log  Report" + " On "+ date.getDate()+ "-"+ (date.getMonth() + 1)+ "-"+ date.getFullYear()
																		},
																		{
																			"sExtends" : "xls",
																			"sTitle" : "OutBound Success Call Log  Report" + " On "+ date.getDate()+ "-"+ (date.getMonth() + 1)+ "-"+ date.getFullYear()
																		} ],
															}
														});

										dataTable.on('order.dt search.dt',
													function() {
														dataTable.column(0,{search : 'applied',order : 'applied'}).nodes().each(
																			function(cell,i) {
																				cell.innerHTML = i + 1;
																			});
														}).draw();
										/*dataTable=  $('#example').DataTable( {
											
										  "ajax": {
										      "url": "GetInOutBoundSuccessCallLog",
										      "data": function ( d ) {
										           d.getDate=getDate;
										           d.tcId=tcId;
										           d.callType=$("#callType").val();
										           d.cat=$('#cat').val();
										     },    
										  },  
										      "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],

										  "rowCallback": function( row, data, index ) {
										       $('td:eq(10)', row).html( '<a type="button" target="_blank" href="http://crmdemo.bonrix.in/Sound/'+$('#uid').val()+"/"+data[10]+'" class="btn btn-labeled btn-warning"><span class="btn-label"><i class="glyphicon glyphicon-save"></i></span></a>' );

										    },
										  "language": {
										      "emptyTable": "No Data Found."
										    } ,
										    "sPaginationType" : "full_numbers",
										  "oLanguage" : {
										  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
										  },
										  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
										   "sDom": 'T<"clear">lfrtip',
										  "oTableTools" : {
										  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
										  },
										  "aButtons": [
											  {
												  "sExtends": "pdf",
												  "sTitle" : "All Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
												  },
												  {
													  "sExtends": "csv",
													  "sTitle" : "All Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
													},
													{
														  "sExtends": "xls",
														  "sTitle" : "All Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
													}
											  ],
										   
										});*/

										$('#example').on('error.dt',
														function(e, settings,techNote,message) {
															console.log('An error has been reported by DataTables: ',message);
														}).DataTable();
									},
									error : function(e) {
										//called when there is an error
										console.log(e.message);
									}
								});
					});

	function getCat() {
		$.ajax({
			url : 'GetCategoryById',
			type : 'GET',
			success : function(data) {
				var msg = data;
				var html = "<option value='All'>All Category</option>";
				var i = 0;
				for (i = 0; i < msg.length; i++) {
					var val = '"' + msg[i][0] + '"';
					html += "<option value="+val+">" + msg[i][1] + "</option>";
				}
				$('#cat').html(html);

			},
			error : function(e) {
				//called when there is an error
				console.log(e.message);
			}
		});

	}
	function reloadData() {
		//alert("stdate::"+stdate+" "+"endate::"+endate)
		stdate;
		endate;
		//	getDate=$('#datetimepicker1').data('date');
		tcId = $('#category').val()
		callType = $("#callType").val();
		cat = $('#cat').val();
		dataTable.ajax.reload();

	}
</script>
</html>
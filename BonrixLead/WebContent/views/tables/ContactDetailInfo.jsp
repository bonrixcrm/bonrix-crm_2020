<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script src=js/jquery.js></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
<script src=js/fnReloadAjax.js></script>
<script type="text/javascript">
$(document).ready(function() {
	//alert('Ready');
	
	         dataTable = $('#contact-grid').DataTable( {
	            "processing": true,
	            "serverSide": true,
	            "ajax": "GetContactInfoDetail",
	            //"ajax":{
	           //     url :"GetAssignCategory", // json datasource
            //    type: "get",  // method  , by default get
	                "columnDefs": [ {
	                    "targets": -1,
	                    "data": null,
	                    "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"
	                }],

	        } );
	  

$('#contact-grid tbody').on( 'click', '#edit-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
   // alert("'Edit is: "+ data[ 0 ] );
    $('#id').val(data[0]);
    $('#name').val(data[1]);
    $('#email').val(data[3]);
    $('#cno').val(data[2]);
} );

$('#contact-grid tbody').on( 'click', '#delete-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
   // alert("'Delete is: "+ data[ 0 ] );
    $.ajax({
  	  url: "deleteContactInfoDetail",
  	  type: "get",
  	  data:"cid="+data[ 0 ],
  	  success: function(html){
  	   // $("#results").append(html);
  	   alert('Hello');
  	 dataTable.ajax.reload();
  	  }
  	});
    
    
} );
} );


function updateContactInfoDetail()
{
	
	var id=$('#id').val();
   var name= $('#name').val();
   var email= $('#email').val();
   var cno= $('#cno').val();
//console.log(id+" "+name+" "+email+" "+cno);
	 $.ajax({
	  	  url: "updateContactInfoDetail",
	  	  type: "get",
	  	  data:{id:id,name:name,email:email,cno:cno},
	  	  success: function(html){
	  	   // $("#results").append(html);
	  	   alert('Hello');
	  	 dataTable.ajax.reload();
	  	  }
	  	});
}

function AddContactInfoDetail()
{
	   var name= $('#addname').val();
	   var email= $('#addemail').val();
	   var cno= $('#addcno').val();
	   
	   $.ajax({
		  	  url: "addContactInfo",
		  	  type: "get",
		  	  data:{name:name,email:email,cno:cno},
		  	  success: function(html){
		  	   // $("#results").append(html);
		  	   alert('Hello');
		  	 dataTable.ajax.reload();
		  	  }
		  	});
	   
}
</script>
<body>

				
	
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Contact Manage</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Contact</button>
					</div>
		</header>
		
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="contact-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead >


						<tr role="row">
							<th style="width:5%;">ID</th>
							<th>Contact Person Name</th>
							<th>Contact Number</th>
							<th>Email Id</th>
							<th>Action</th>
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>
	
	
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:35%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Contact Info</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         <input type="hidden" id="id" class="form-control">
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Person Name</b></div>
   <div class="col-xs-6 col-sm-6"> <input type="text" id="name" class="form-control"> </div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Email Id</b></div>
  <div class="col-xs-6 col-sm-6"> <input type="text" id="email" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Number</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="cno" class="form-control"></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="updateContactInfoDetail()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:35%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Contact Info</h4>
      </div>
      <div class="modal-body">
      <form method="get">
                <input type="hidden" id="id" class="form-control">
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Person Name</b></div>
   <div class="col-xs-6 col-sm-6"> <input type="text" id="addname" class="form-control"> </div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Email Id</b></div>
  <div class="col-xs-6 col-sm-6"> <input type="text" id="addemail" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-6"><b>Contact Number</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="addcno" class="form-control"></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="AddContactInfoDetail()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save Contact</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>
<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="/includes/SessionCheck.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Credit Management</title>
</head>
<body>
	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

		<!-- Button trigger modal -->
		<a href="./user/AssignCredit.jsp" data-toggle="modal"
			data-target="#remoteModal" class="btn btn-primary"> <i
			class="fa fa-circle-arrow-up fa-lg"></i> Assign Credit
		</a>

		<!-- MODAL PLACE HOLDER -->
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>
		<!-- END MODAL -->

	</div>
	<section id="widget-grid" class="ng-scope"> <!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article
			class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">


		<sql:query var="msg" dataSource="SMS">
                           
                           
                         SELECT s.scid,s.smscredit,DATE(s.validity) as date,u.uid,u.username,u.mobilenumber,u.email FROM smscredit s RIGHT JOIN users u ON s.uid=u.uid WHERE u.parentid=<%=currentUser.getUserid()%>

		</sql:query>


		<div
			class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable"
			id="wid-id-3" data-widget-editbutton="false" role="widget" style="">
			<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
			<header role="heading">
			<div class="jarviswidget-ctrls" role="menu">
				<a href="javascript:void(0);"
					class="button-icon jarviswidget-toggle-btn" rel="tooltip" title=""
					data-placement="bottom" data-original-title="Collapse"><i
					class="fa fa-minus "></i></a> <a href="javascript:void(0);"
					class="button-icon jarviswidget-fullscreen-btn" rel="tooltip"
					title="" data-placement="bottom" data-original-title="Fullscreen"><i
					class="fa fa-expand "></i></a> <a href="javascript:void(0);"
					class="button-icon jarviswidget-delete-btn" rel="tooltip" title=""
					data-placement="bottom" data-original-title="Delete"><i
					class="fa fa-times"></i></a>
			</div>
			<div class="widget-toolbar" role="menu">
				<a data-toggle="dropdown" class="dropdown-toggle color-box selector"
					href="javascript:void(0);"></a>
				<ul class="dropdown-menu arrow-box-up-right color-select pull-right">
					<li><span class="bg-color-green"
						data-widget-setstyle="jarviswidget-color-green" rel="tooltip"
						data-placement="left" data-original-title="Green Grass"></span></li>
					<li><span class="bg-color-greenDark"
						data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip"
						data-placement="top" data-original-title="Dark Green"></span></li>
					<li><span class="bg-color-greenLight"
						data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip"
						data-placement="top" data-original-title="Light Green"></span></li>
					<li><span class="bg-color-purple"
						data-widget-setstyle="jarviswidget-color-purple" rel="tooltip"
						data-placement="top" data-original-title="Purple"></span></li>
					<li><span class="bg-color-magenta"
						data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip"
						data-placement="top" data-original-title="Magenta"></span></li>
					<li><span class="bg-color-pink"
						data-widget-setstyle="jarviswidget-color-pink" rel="tooltip"
						data-placement="right" data-original-title="Pink"></span></li>
					<li><span class="bg-color-pinkDark"
						data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip"
						data-placement="left" data-original-title="Fade Pink"></span></li>
					<li><span class="bg-color-blueLight"
						data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip"
						data-placement="top" data-original-title="Light Blue"></span></li>
					<li><span class="bg-color-teal"
						data-widget-setstyle="jarviswidget-color-teal" rel="tooltip"
						data-placement="top" data-original-title="Teal"></span></li>
					<li><span class="bg-color-blue"
						data-widget-setstyle="jarviswidget-color-blue" rel="tooltip"
						data-placement="top" data-original-title="Ocean Blue"></span></li>
					<li><span class="bg-color-blueDark"
						data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip"
						data-placement="top" data-original-title="Night Sky"></span></li>
					<li><span class="bg-color-darken"
						data-widget-setstyle="jarviswidget-color-darken" rel="tooltip"
						data-placement="right" data-original-title="Night"></span></li>
					<li><span class="bg-color-yellow"
						data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip"
						data-placement="left" data-original-title="Day Light"></span></li>
					<li><span class="bg-color-orange"
						data-widget-setstyle="jarviswidget-color-orange" rel="tooltip"
						data-placement="bottom" data-original-title="Orange"></span></li>
					<li><span class="bg-color-orangeDark"
						data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip"
						data-placement="bottom" data-original-title="Dark Orange"></span></li>
					<li><span class="bg-color-red"
						data-widget-setstyle="jarviswidget-color-red" rel="tooltip"
						data-placement="bottom" data-original-title="Red Rose"></span></li>
					<li><span class="bg-color-redLight"
						data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip"
						data-placement="bottom" data-original-title="Light Red"></span></li>
					<li><span class="bg-color-white"
						data-widget-setstyle="jarviswidget-color-white" rel="tooltip"
						data-placement="right" data-original-title="Purity"></span></li>
					<li><a href="javascript:void(0);"
						class="jarviswidget-remove-colors" data-widget-setstyle=""
						rel="tooltip" data-placement="bottom"
						data-original-title="Reset widget color to default">Remove</a></li>
				</ul>
			</div>
			<span class="widget-icon"> <i class="fa fa-table"></i>
			</span>
			<h2>Export to PDF / Excel</h2>

			<span class="jarviswidget-loader"><i
				class="fa fa-refresh fa-spin"></i></span></header>

			<!-- widget div-->


			<!-- Widget ID (each widget will need unique ID)-->
			<!-- end widget -->
			<div class="modal fade" id="updateModal" tabindex="-1" role="dialog"
				aria-labelledby="remoteModalLabel" aria-hidden="true"
				style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content" style="width: 800px;"></div>
				</div>
			</div>
			<script type="text/javascript">
			function getdata(){
				
				 if (mediatable)
					 mediatable.fnDestroy();
//				alert("getdata");
			      mediatable=	$('#datatable_tabletools').dataTable({
			        "processing": false,
			        "serverSide": false,
			        "bServerSide": false, 
			        
			        "bdestroy": true,
			        
			      //  "bDestroy": true,
			        "ajax": "getMediaListbyUid", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
			            $('td:eq(5)', nRow).html("<a target='_blank' href='"+aData[5]+"'>"+aData[5]+"</a>");
			            $('td:eq(8)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Detail</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');
			            
			        },
			 /*        
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
							"t"+
							"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>", */
			   /*      "oTableTools": {
			        	 "aButtons": [
			             "copy",
			             "csv",
			             "xls",
			                {
			                    "sExtends": "pdf",
			                    "sTitle": "SmartSMS_PDF",
			                    "sPdfMessage": "SmartSMS PDF Export",
			                    "sPdfSize": "letter"
			                },
			             	{
			                	"sExtends": "print",
			                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
			            	}
			             ],
			            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
			        }, */
					"autoWidth" : true,	
					"rowCallback" : function(nRow) {
				
					},
					"drawCallback" : function(oSettings) {
				
					}
				});
				
				
			}
			</script>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#datepicker').datepicker();

					$('body').on('hidden.bs.modal', '.modal', function() {
						$(this).removeData('bs.modal');
					});

				});

				function formSub(event) {

					alert("asda");
					var username = $('#username').val();
					var password = $('#password').val();
					var name = $('#name').val();
					var city = $('#city').val();
					var email = $('#email').val();
					var number = $('#number').val();
					var address = $('#address').val();
					$.ajax({
						url : "registerUserAdmin",
						type : "GET",
						data : {
							'username' : username,
							'password' : password,
							'email' : email,
							'name' : name,
							'city' : city,
							'number' : number,
							'address' : address
						},

						success : function(response) {
							alert(response);
							getdata()
							window.location = "login.jsp";

						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
			<div class="jarviswidget jarviswidget-sortable" id="wid-id-1"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false" role="widget" style="">



				<header role="heading">
				<div class="jarviswidget-ctrls" role="menu">
					<a href="javascript:void(0);"
						class="button-icon jarviswidget-toggle-btn" rel="tooltip" title=""
						data-placement="bottom" data-original-title="Collapse"><i
						class="fa fa-minus "></i></a> <a href="javascript:void(0);"
						class="button-icon jarviswidget-fullscreen-btn" rel="tooltip"
						title="" data-placement="bottom" data-original-title="Fullscreen"><i
						class="fa fa-expand "></i></a> <a href="javascript:void(0);"
						class="button-icon jarviswidget-delete-btn" rel="tooltip" title=""
						data-placement="bottom" data-original-title="Delete"><i
						class="fa fa-times"></i></a>
				</div>
				<span class="widget-icon"> <i class="fa fa-edit"></i>
				</span>
				<h2>User Manage</h2>

				<span class="jarviswidget-loader"><i
					class="fa fa-refresh fa-spin"></i></span></header>

				<!-- widget div-->
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="well no-padding">

						<div role="content">

							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->

							</div>
							<!-- end widget edit box -->

							<!-- widget content -->
							<div class="widget-body no-padding">

								<div id="datatable_tabletools_wrapper"
									class="dataTables_wrapper form-inline no-footer">
									<div class="dt-toolbar">
										<div class="col-xs-12 col-sm-6">
											<div id="datatable_tabletools_filter"
												class="dataTables_filter">
												<label><span class="input-group-addon"><i
														class="glyphicon glyphicon-search"></i></span> <input
													type="search" class="form-control"
													aria-controls="datatable_tabletools"></label>
											</div>
										</div>
										<div class="col-sm-6 col-xs-6 hidden-xs">
											<div class="DTTT btn-group">
												<a class="btn btn-default DTTT_button_copy"
													id="ToolTables_datatable_tabletools_0"><span>Copy</span>
													<div
														style="position: absolute; left: 0px; top: 0px; width: 54px; height: 31px; z-index: 99;">
														<embed id="ZeroClipboard_TableToolsMovie_1"
															src="js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
															loop="false" menu="false" quality="best"
															bgcolor="#ffffff" width="54" height="31"
															name="ZeroClipboard_TableToolsMovie_1" align="middle"
															allowscriptaccess="always" allowfullscreen="false"
															type="application/x-shockwave-flash"
															pluginspage="http://www.macromedia.com/go/getflashplayer"
															flashvars="id=1&amp;width=54&amp;height=31"
															wmode="transparent">
													</div></a><a class="btn btn-default DTTT_button_csv"
													id="ToolTables_datatable_tabletools_1"><span>CSV</span>
													<div
														style="position: absolute; left: 0px; top: 0px; width: 48px; height: 31px; z-index: 99;">
														<embed id="ZeroClipboard_TableToolsMovie_2"
															src="js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
															loop="false" menu="false" quality="best"
															bgcolor="#ffffff" width="48" height="31"
															name="ZeroClipboard_TableToolsMovie_2" align="middle"
															allowscriptaccess="always" allowfullscreen="false"
															type="application/x-shockwave-flash"
															pluginspage="http://www.macromedia.com/go/getflashplayer"
															flashvars="id=2&amp;width=48&amp;height=31"
															wmode="transparent">
													</div></a><a class="btn btn-default DTTT_button_xls"
													id="ToolTables_datatable_tabletools_2"><span>Excel</span>
													<div
														style="position: absolute; left: 0px; top: 0px; width: 54px; height: 31px; z-index: 99;">
														<embed id="ZeroClipboard_TableToolsMovie_3"
															src="js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
															loop="false" menu="false" quality="best"
															bgcolor="#ffffff" width="54" height="31"
															name="ZeroClipboard_TableToolsMovie_3" align="middle"
															allowscriptaccess="always" allowfullscreen="false"
															type="application/x-shockwave-flash"
															pluginspage="http://www.macromedia.com/go/getflashplayer"
															flashvars="id=3&amp;width=54&amp;height=31"
															wmode="transparent">
													</div></a><a class="btn btn-default DTTT_button_pdf"
													id="ToolTables_datatable_tabletools_3"><span>PDF</span>
													<div>
														<style="position: absolute; left: 0px; top: 0px; width:48px; height: 31px; z-index: 99;">
														<embed id="ZeroClipboard_TableToolsMovie_4"
															src="js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
															loop="false" menu="false" quality="best"
															bgcolor="#ffffff" width="48" height="31"
															name="ZeroClipboard_TableToolsMovie_4" align="middle"
															allowscriptaccess="always" allowfullscreen="false"
															type="application/x-shockwave-flash"
															pluginspage="http://www.macromedia.com/go/getflashplayer"
															flashvars="id=4&amp;width=48&amp;height=31"
															wmode="transparent">
													</div></a><a class="btn btn-default DTTT_button_print"
													id="ToolTables_datatable_tabletools_4"
													title="View print view"><span>Print</span></a>
											</div>
										</div>
									</div>
									<table id="datatable_tabletools"
										class="table table-striped table-bordered table-hover dataTable no-footer"
										width="100%" role="grid"
										aria-describedby="datatable_tabletools_info"
										style="width: 100%;">
										<thead>
											<tr role="row">
												<th data-hide="phone" class="sorting_asc" tabindex="0"
													aria-controls="datatable_tabletools" rowspan="1"
													colspan="1" aria-sort="ascending"
													aria-label="ID: activate to sort column ascending"
													style="width: 27px;">ID</th>
												<th data-class="expand" class="sorting" tabindex="0"
													aria-controls="datatable_tabletools" rowspan="1"
													colspan="1"
													aria-label="Name: activate to sort column ascending"
													style="width: 73px;">Name</th>
												<th class="sorting" tabindex="0"
													aria-controls="datatable_tabletools" rowspan="1"
													colspan="1"
													aria-label="Phone: activate to sort column ascending"
													style="width: 118px;">Phone</th>
												<th data-hide="phone" class="sorting" tabindex="0"
													aria-controls="datatable_tabletools" rowspan="1"
													colspan="1"
													aria-label="Company: activate to sort column ascending"
													style="width: 303px;">Email</th>
												<th data-hide="phone,tablet" class="sorting" tabindex="0"
													aria-controls="datatable_tabletools" rowspan="1"
													colspan="1"
													aria-label="Zip: activate to sort column ascending"
													style="width: 79px;">Credit</th>
												<th data-hide="phone,tablet" class="sorting" tabindex="0"
													aria-controls="datatable_tabletools" rowspan="1"
													colspan="1"
													aria-label="City: activate to sort column ascending"
													style="width: 230px;">Date</th>
												<th data-hide="phone,tablet" class="sorting" tabindex="0"
													aria-controls="datatable_tabletools" rowspan="1"
													colspan="1"
													aria-label="City: activate to sort column ascending"
													style="width: 230px;">Action</th>

											</tr>
										</thead>
										<tbody>

											<c:forEach var="all" items="${msg.rows}">


												<tr role="row" class="odd">
													<td class="sorting_1">1</td>
													<td><span class="responsiveExpander"></span>${all.username}</td>
													<td>${all.mobilenumber}</td>
													<td>${all.email}</td>
													<td>${all.smscredit}</td>
													<td>${all.date}</td>
													<td><a class="btn btn-info btn-xs"
														href="./user/updateCredit.jsp?scid=${all.scid}&userName=${all.username}&uid=${all.uid}"
														data-toggle="modal" data-target="#updateModal"
														class="btn btn-primary btn-xs">Edit</a>&nbsp;<a
														class="btn btn-danger btn-xs"
														href="deleteCredit?scid=${all.scid}"
														onclick="return confirm('Are you sure?');">Delete</a></td>
												</tr>
											</c:forEach>

										</tbody>
									</table>

								</div>
								<!-- end widget content -->

							</div>
							<!-- end widget div -->

						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->
			</div>
		</article>
		<!-- END COL -->

	</div>

	<!-- END ROW --> <!-- START ROW --> </section>


</body>

</html>


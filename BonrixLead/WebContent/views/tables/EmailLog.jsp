<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<div class="row">
     <button type="button" class="btn btn-default pull-right" id="daterange-btn"  >
                    <i class="fa fa-calendar"></i> chose Date Range For Log
                    <i class="fa fa-caret-down"></i>
                  </button>
                  
                   
     <div class="col-lg-4" style="margin-left: 3px;"> 
  <div class="row"  >
    <div class="col-xs-6 col-sm-8">  <input type="text" id="searchData" class="form-control"></div> 
  <div class="col-xs-4 col-sm-2" ><button type="button" class="btn btn-primary" onclick="reloadtable()">Search</button>
  </div>
</div>
  </div>
  </div> 
  <br>
<!-- MODAL PLACE HOLDER -->
<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">


	<!-- end widget -->

	<!-- Widget ID (each widget will need unique ID)-->

	<!-- end widget -->



	<!-- Widget ID (each widget will need unique ID)-->
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
		data-widget-editbutton="false">
		<header>
			&nbsp;&nbsp;&nbsp;<b> Email Log</b>

		</header>



		<!-- widget div-->
		<div>

			<!-- widget edit box -->
			<div class="jarviswidget-editbox">
				<!-- This area used as dropdown edit box -->

			</div>
			<!-- end widget edit box -->

			<!-- widget content -->
			<div class="widget-body no-padding">
<table id="example" class="table table-striped table-bordered" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Customer Name</th>
                <th>Mobile No.</th>
                <th>Email</th>
                 <th>Mail Subject</th>
                <th>Send Date</th>
                 <th>Mail Status</th>
                 <th style="width: 136px;">Attachment</th>
                 <th>View</th>
            </tr>
            </thead>
            <tbody id="data" >
            
              </tbody>
       
       
    </table>   
					
				<table id="example"
					class="table table-striped table-bordered table-hover">
					<thead>


						<!-- <tr role="row">
							<th data-hide="phone" class="sorting_asc" tabindex="0"
								aria-controls="hostsettingtable_tabletools" rowspan="1" colspan="1"
								aria-sort="ascending"
								aria-label="ID: activate to sort column ascending"
								>Customer Name</th>
							<th data-class="expand" class="sorting" tabindex="0"
								aria-controls="hostsettingtable_tabletools" rowspan="1" colspan="1"
								aria-label="Name: activate to sort column ascending">Mobile No.</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="hostsettingtable_tabletools" rowspan="1" colspan="1">Email</th>
							<th data-hide="phone" class="sorting" tabindex="0"
								aria-controls="hostsettingtable_tabletools" rowspan="1" colspan="1">Mail Subject</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="hostsettingtable_tabletools" rowspan="1" colspan="1">Send Date
								</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="hostsettingtable_tabletools" rowspan="1" colspan="1">
								Mail Status</th>
						
							<th>Attachments</th>
							<th>View</th>



						</tr> -->
						
				</table>

			</div>
			<!-- end widget content -->

		</div>
		<!-- end widget div -->

	</div>
	<!-- end widget -->

	</article>
	<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->


<!-- View Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mail Body</h4>
      </div>
      <div class="modal-body" id="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</section>

<script src="js/daterangepicker.js"></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>

<script>
 var name="NA";
 var mobileNo="NA";
 var email="NA";
 var subject="NA"
 var stdate="NA";
 var endate="NA";
 var search="false";
 var dataTable=null;
 var date = new Date();
 $('#daterange-btn').daterangepicker(
	        {
	        	format: "yyyy-mm-dd",
	          ranges: {
	            'Today': [moment(), moment()],
	            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	            'This Month': [moment().startOf('month'), moment().endOf('month')],
	            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	          },
	           startDate: moment().subtract(29, 'days'),
	          endDate: moment() 
	        
	       
	        },
	        function (start, end) {
	          $('#daterange-btn').text(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	        }
	    );
 
 $('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
	     name=$("#searchData").val();
	      mobileNo=$("#searchData").val();
	      email=$("#searchData").val();
	      subject=$("#searchData").val();
		 stdate=picker.startDate.format('YYYY-MM-DD');
		 endate=picker.endDate.format('YYYY-MM-DD');
		 dataTable.ajax.reload();
	});
 
 
  $(document).ready(function() {
	 var date=new Date();
	     
	     dataTable=  $('#example').DataTable( {
		  "processing": true,
	        "serverSide": true, 
	          "searching": false,  
	         // "bLengthChange": false, 
	        
	        "ajax": {
	        	"url":"getEmailLogByCompany",	      
	 	        "data": function ( d ) {
	 	        	d.name=name;
	 	        	d.mobileNo=mobileNo;
	 	        	d.email=email;
	 	        	d.subject=subject;
	 	        	d.search=search;
	 	        	d.stdate=stdate;
	 	        	d.endate=endate;
	           }
	        }, 
	        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	        	var u=encodeURI(aData[7]);
		        	 $('td:eq(7)', nRow).html('<center><button type="button" onclick="ViewMessagw('+"\'"+u+"\'"+')"  data-toggle="modal" data-target="#myModal" class="btn btn-default btn-sm btn-round"><span class="glyphicon glyphicon-fullscreen"></span></button></center>');
		        	
	                    if(aData[5].toString()=="Success")
	                    	 $('td:eq(5)', nRow).html('<span class="label label-success">Success</span>');
	                    else 
	            		   $('td:eq(5)', nRow).html('<span class="label label-danger">Fail</span>');
	                    
	                    if(aData[6].toString()!="N/A")
	                    	{
	                    	var fileData=aData[6].toString();
	                    	 var files = fileData.substring(0, fileData.length - 1);
	                    	 console.log(files)
	                    	  var down_files =files.split(",");
	                    	  var concat_files="";
	                    	  for(var i=0;i<down_files.length;i++)
	                    		  {
	                    		 concat_files+='<a target="_blank" href="'+down_attach+"Attachments/"+down_files[i]+'"> <span class="glyphicon glyphicon-download-alt"></span></a>&nbsp;&nbsp;&nbsp;';
                    		 // concat_files+='<a target="_blank" href="http://crmdemo.bonrix.in/'+down_files[i]+'">dd</a>&nbsp;&nbsp;&nbsp;';

	                    		  }
		            		   $('td:eq(6)', nRow).html(concat_files);
		            		   concat_files="";
	                    	}
	                    else
	                    	$('td:eq(6)', nRow).html('<span class="label label-danger">No Attachment</span>');
	            	
		        }, 
	         "sPaginationType" : "full_numbers",
			  "oLanguage" : {
			  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
			  },
			   "sDom": 'T<"clear">lfrtip', 
			   
			  "oTableTools" : {
			  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			  "aButtons": [
				  {
	 				  "sExtends": "pdf",
	 				  "sTitle" : "Email Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 				  },
	 				  {
	 					  "sExtends": "csv",
	 					  "sTitle" : "Email Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 					},
	 					{
	 						  "sExtends": "xls",
	 						  "sTitle" : "Email Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 					}
				  ],
			  }     
	    } );
	  
	} );
 
 function ViewMessagw(text)
 {
	 $("#modal-body").html(decodeURI(text))
 }
  function reloadtable(){
	      name=$("#searchData").val();
	      mobileNo=$("#searchData").val();
	      email=$("#searchData").val();
	      subject=$("#searchData").val();
		 dataTable.ajax.reload();    
		}
</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<style>
/* body {padding-top:50px;} */

.box {
    border-radius: 3px;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    padding: 10px 50px;
    text-align: right;
    display: block;
    margin-top: 60px;
     background-color:white;
}
.box-icon {
  /*   background-color: #57a544; */
    border-radius: 50%;
    display: table;
    height: 100px;
    margin: 0 auto;
    width: 100px;
    margin-top: -61px;
}
/* .box-icon span {
    color: #fff;
    display: table-cell;
    text-align: center;
    vertical-align: middle;
} */
.info h4 {
    font-size: 26px;
    letter-spacing: 2px;
    /* text-transform: uppercase; */
}
/* .info > p {
    color: #717171;
    font-size: 16px;
    padding-top: 10px;
    text-align: justify;
}
.info > a {
    background-color: #03a9f4;
    border-radius: 2px;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    color: #fff;
    transition: all 0.5s ease 0s;
}
.info > a:hover {
    background-color: #0288d1;
    box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.16), 0 2px 5px 0 rgba(0, 0, 0, 0.12);
    color: #fff;
    transition: all 0.5s ease 0s;
}
 */</style>
</head>
<body>
<h2>Hello....</h2>
 <!-- <div class="container"> -->
    <div class="row"   >
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="background-color:#0a97b9;">
            <div class="box">
                <div class="box-icon">
                  <!--   <span class="fa fa-4x fa-html5"></span> -->
               <!--  <img src="img/eml.png" height="60%" width="60%" style="margin-top: 20px;margin-bottom: 20px;;margin-right:30px;margin-left: 23px;"/> -->
               <img src="img/email.png" height="110%" width="110%" style="margin-bottom: 40px;" />
                </div>
                <div class="info" >
                <br>
                    <h4 class="text-center">Send Email</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                    <a href="" class="btn">Link</a>
                </div>
            </div>
       <br>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="background-color:#47a877;">
            <div class="box">
                <div class="box-icon">
                    <!-- <span class="fa fa-4x fa-css3"></span> -->
                   <img src="img/email1.png" height="110%" width="110%" style="margin-bottom: 40px;"/>
                </div>
                <div class="info">
                <br>
                    <h4 class="text-center">Email Template</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                    <a href="" class="btn">Link</a>
                </div>
            </div>
            <br>
        </div>
	</div>
	
	<div class="row">
	
	 
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="background-color:#db5565;">
            <div class="box">
                <div class="box-icon">
                    <!-- <span class="fa fa-4x fa-css3"></span> -->
                   <img src="img/cemail.png" height="110%" width="110%" style="margin-bottom: 40px;"/>
                </div>
                <div class="info">
                <br>
                    <h4 class="text-center">Contact Email</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                    <a href="" class="btn">Link</a>
                </div>
            </div>
            <br>
        </div>
        
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="background-color:#6244a5;">
            <div class="box">
                <div class="box-icon">
                    <!-- <span class="fa fa-4x fa-css3"></span> -->
                   <img src="img/lemail.png" height="110%" width="110%" style="margin-bottom: 40px;"/>
                </div>
                <div class="info">
                <br>
                    <h4 class="text-center">Lead Email</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                    <a href="" class="btn">Link</a>
                </div>
            </div>
            <br>
        </div>
        
	</div>
	
	
	
	
	
	
	
	
<!-- </div> -->
</body>
</html>
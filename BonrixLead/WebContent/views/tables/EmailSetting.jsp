<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
var dataTable=null;
var finalCatId=null;
var updateId=null;
$(document).ready(function() {
	         dataTable = $('#cat-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "getCRMHostSettings",
	                "columnDefs": [ {
	                    "targets": -1,
	                    "data": null,
	                    "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"
	                }],
	        } );
$('#cat-grid tbody').on( 'click', '#edit-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    updateId=data[ 0 ];
    $('#host_email').val(data[1]);
    $('#host_pass').val(data[2]);
    $('#host_name').val(data[3]);
} );

$('#cat-grid tbody').on( 'click', '#delete-btn', function () {
	   var data = dataTable.row( $(this).parents('tr') ).data();
	   $.ajax({
		   url:'deleteCRMHostSetting',
		   type:'GET',
		  	  data:{id:data[0]},
		   success:function(data){
			   dataTable.ajax.reload();
		   },
		   error: function(e) {
		 		console.log(e.message);
		 	  } 
	   });
	 
	} );
} );
 dataTable.on( 'order.dt search.dt', function () {
	 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();  
 
function saveChangeHost()
{
	$.ajax({
		   url:'updateCRMHostSetting',
		   type:'POST',
		  	  data:{hostName:$("#host_name").val(),emailId:$("#host_email").val(),password:$("#host_pass").val(),id:updateId},
		   success:function(data){
			   dataTable.ajax.reload();
			   $('#myModal').modal('hide');
		   },
		   error: function(e) {
		 		console.log(e.message);
		 	  } 
	   });
}
	
function saveHost()
{
	$.ajax({
		   url:'addCRMHostSetting',
		   type:'GET',
		  	  data:{hostName:$("#add_host_name").val(),emailId:$("#add_host_email").val(),password:$("#add_host_pass").val()},
		   success:function(data){
			   dataTable.ajax.reload();
			   $('#myModal1').modal('hide');
		   },
		   error: function(e) {
		 		console.log(e.message);
		 	  } 
	   });
}

</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Host Settings</h2>
			<div class="widget-toolbar" role="menu">
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;New Host</button>
					</div>
		</header>
		<div>
			<div class="jarviswidget-editbox"></div>
			<div class="widget-body no-padding">
				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Email Id</th>
							<th>Password</th>
							<th>Host Name</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Host Settings</h4>
      </div>
      <div class="modal-body">
      <div class="row">
  <div class="col-xs-4">Email Id</div>
  <div class="col-xs-6"><input type="text" id="host_email"  class="form-control" name="host_email"></div>
</div>
<br>
<div class="row">
  <div class="col-xs-4">Password</div>
  <div class="col-xs-6"><input type="text" id="host_pass"  class="form-control" name="host_pass"></div>
</div>
<br>
<div class="row">
  <div class="col-xs-4">Host Name</div>
  <div class="col-xs-6"><input type="text" id="host_name"  class="form-control" name="host_name"></div>
</div>
          </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       <button type="button" class="btn btn-primary" onclick="saveChangeHost();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
      </div>
      <div class="modal-body">
        <div class="row">
  <div class="col-xs-4">Email Id</div>
  <div class="col-xs-6"><input type="text" id="add_host_email"  class="form-control" name="add_host_email"></div>
</div>
<br>
<div class="row">
  <div class="col-xs-4">Password</div>
  <div class="col-xs-6"><input type="text" id="add_host_pass"  class="form-control" name="add_host_pass"></div>
</div>
<br>
<div class="row">
  <div class="col-xs-4">Host Name</div>
  <div class="col-xs-6"><input type="text" id="add_host_name"  class="form-control" name="add_host_name"></div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveHost();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Host</button>
      </div>
    </div>
  </div>
</div>
</html>

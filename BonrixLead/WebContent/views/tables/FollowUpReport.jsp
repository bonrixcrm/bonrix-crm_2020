<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel=stylesheet type=text/css media=screen href="css/dataTables.jqueryui.min.css"/>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script> 
<script src="js/daterangepicker/daterangepicker.js"></script>
<button type="button" class="btn btn-default pull-right" id="daterange-btn"  >
                    <i class="fa fa-calendar"></i> chose Date Range For Log
                    <i class="fa fa-caret-down"></i>
                  </button>
                  <p><br></p>
<h2>Followup Report</h2>

<table id="example" class='table table-striped table-bordered' cellspacing="0" width="100%">
        <thead>
            <tr>
            	<th>Id</th>
                <th>Name</th>
                <th>Success Average</th>
                <th>Fail Average</th>
                <th>Over Access Time (Followup Count)</th>
            </tr>
        </thead>
    </table>
    
<script>

var startDt=''; 
var endDt='';
var table=null;
$(document).ready(function() {
	
	
	$('#daterange-btn').daterangepicker(
	        {
	          ranges: {
	            'Today': [moment(), moment()],
	            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	            'This Month': [moment().startOf('month'), moment().endOf('month')],
	            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	          },
	          startDate: moment().subtract(29, 'days'),
	          endDate: moment()
	        
	       
	        },
	        function (start, end) {
	          $('#daterange-btn').text(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	          startDt=start.format('MMMM D, YYYY');
	          endDt=end.format('MMMM D, YYYY');
	        }
	    );
	
	$('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
		 var stdate=picker.startDate.format('YYYY-MM-DD');
		 var endate=picker.endDate.format('YYYY-MM-DD');
		 reload(stdate,endate);
		});
	
	
	var fullDate = new Date()
	var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
	var currentDate =fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
	console.log(currentDate);
	 startDt=currentDate; 
	 endDt=currentDate;
   table= $('#example').DataTable( {
    		  "ajax": {"url":"getTelecallerFollowupReport",	      
    		        "data": function ( d ) {
    		              d.startDate =startDt;
    		              d.endDate=endDt;
    		          }
    		  	  },
        "columns": [
                    { "data": "username" },
                    { "data": "username" },
                    { "data": "successAVG" },
                    { "data": "failAVG" },
                    { "data": "OverAccessTime" }
        ],
        "sPaginationType" : "full_numbers",
		  "oLanguage" : {
		  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
		  },
		  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
		   "sDom": 'T<"clear">lfrtip',
		  "oTableTools" : { 
		  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
		  }
		  });    
   table.on( 'order.dt search.dt', function () {
	   table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
           cell.innerHTML = i+1;
       } );
      } ).draw(); 
  
    } );
    
function reload(startDt1,endDt1)
{	
	 startDt=startDt1;
	 endDt=endDt1;
	 table.ajax.reload();	        
}
</script>
</body>
</html>
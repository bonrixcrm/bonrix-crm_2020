
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/includes/SessionCheck.jsp"%>
<style type="text/css">

@-webkit-keyframes glowing {
  0% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -webkit-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
}

@-moz-keyframes glowing {
  0% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -moz-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
}

@-o-keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}

@keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}

.button {
  -webkit-animation: glowing 1500ms infinite;
  -moz-animation: glowing 1500ms infinite;
  -o-animation: glowing 1500ms infinite;
  animation: glowing 1500ms infinite;
}
</style>

<div class="row"  >
	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" >

		


		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width: 900px;"></div>
			</div>
		</div>

	</div>
	
	
	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" >

		<div class="modal fade" id=remoteModalMsg tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width: 900px;"></div>
			</div>
		</div>
		
		<div class="modal fade" id="remoteModalMsg2" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width: 600px;">
				
				
				</div>
				
			</div>
			
		</div>

	</div>
	
	
</div>

<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-gname"
		data-widget-editbutton="false" data-widget-deletebutton="false">
		

<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Group</strong> <i> Name List</i></h2>		
							
					<div class="widget-toolbar" role="menu">
						
					<a href="./group/createGroup.jsp" data-toggle="modal"
			data-target="#remoteModal" class="btn btn-primary button"> <i
			class="fa fa-circle-arrow-up fa-lg"></i> Create Group
		</a>
					</div>
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
		<div>

			<div class="jarviswidget-editbox">

			</div>
			<div class="widget-body no-padding">

				<table id="datatable_tabletools"
					class="table table-striped table-bordered table-hover" width="100%">
					<thead>
						<tr role="row">
							<th>Name</th>
							<th>Type</th>
							<th>Added On</th>
							<th>Members</th>
							<th>Action</th>

						</tr>
					</thead>
			

				</table>
				

			</div>

		</div>

	</div>



</section>
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>
<script type="text/javascript">
	var responsiveHelper_dt_basic = undefined;
	var responsiveHelper_datatable_fixed_column = undefined;
	var responsiveHelper_datatable_col_reorder = undefined;
	var responsiveHelper_datatable_tabletools = undefined;

	var mediatable = null;

	pageSetUp();


	function reloadtable2(){
	//	alert("readlod");
		  grouplisttable.ajax.reload();		
	}
	var pagefunction = function() {

		$('body').on('hidden.bs.modal', '.modal', function() {
			reloadtable2();
			$(this).removeData('bs.modal');
		
		});
		
		$('.modal').on('hidden.bs.modal', '.modal', function() {
			reloadtable2();
		
		});
		getdata();

	};

	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){			
				loadScript("js/daterangepicker.js",pagefunction)
			
		});
	});

	function getdata() {

		grouplisttable = $('#datatable_tabletools')
				.DataTable(
						{
				"processing" : false,"bServerSide" : false,"ajax" : "getGrupnames",
							"fnCreatedRow" : function(nRow, aData, iDataIndex) {
								$('td:eq(0)', nRow).html(aData[1]);
								$('td:eq(1)', nRow).html(aData[2]);
								$('td:eq(2)', nRow).html(aData[3]);
								$('td:eq(3)', nRow).html(aData[4]);
								$('td:eq(4)', nRow)
										.html(
												'<div class="btn-group btn-group-justified" style="width:300px;"><a href="views/tables/importContact2.jsp?gid='
													+ aData[0]
													+ '" data-toggle="modal" data-target="#remoteModalMsg2" class="btn btn-primary btn-sm">IMPORT </a>'+
												'<a href="views/sendMessageByGid.jsp?gid='
															+ aData[0]
															+ '" data-toggle="modal" data-target="#remoteModalMsg" class="btn btn-primary btn-sm">Send </a><a href="views/tables/listContect.jsp?gid='
														+ aData[0]
														+ '" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-sm">View </a><a href="./group/updateGroup.jsp?gid='
														+ aData[0]
														+ '" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-sm">Edit</a><a href="#"  class="btn btn-danger btn-sm" onclick="delhttp2('
														+ aData[0]
														+ ')">Delete</a></div>');
							},
							"autoWidth" : true,
							"rowCallback" : function(nRow) {
							},
							"drawCallback" : function(oSettings) {
							}
						});

	}

	function delhttp2(id) {
		var conf = confirm('Continue delete?');

		if (conf) {
			$.get("deleteGroup?gid=" + id, function(msg) {

				reloadtable2();
				
			});
		}

	}
</script>

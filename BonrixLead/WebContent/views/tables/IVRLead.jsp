<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
table.dataTable.select tbody tr,
table.dataTable thead th:first-child {
  cursor: pointer;
}
</style>
</head>
<script src="js/bootstrap-datetimepicker.js"></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>

<script src=js/fnReloadAjax.js></script>

<script>
var cat=null;
var assign=null;
var tel=null;
var table=null;
var dueDate=null;
$(function () {
    $('#datetimepicker1').datetimepicker({
   	  defaultDate: new Date(), 
   	 
     format: 'YYYY-MM-DD'
        /* disabledDates: [
            moment("2016-12-30"),
            new Date(2013, 11 - 1, 21),
            "01/01/2016 00:53"
        ] */
    });
});
/*Data Table COde */
 
function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

$(document).ready(function (){

	var uid=$('#uid').val();
	$("#telecaller1").hide();
	
	
	
	$.ajax({
	  	  url: 'GetTelecaller',
	  	  type: 'GET',
	  	  success: function(data) {
	  		  
	  		var msg=data;
	  		var html="<option value='0'>All Telecaller</option>";
	  		var i=0;
	  		
	  		for(i=0;i<msg.length;i++)
	  			{
	  		
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#telecaller').html(html);
	  		$('#telecaller1').html(html);
	  		generate();
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );
	
	
   });
function reloadtable(){	
	//alert("Reload...")
	  cat=$("#category").val();
	 assign=$("#AUAll").val(); 
	 tel=$("#telecaller1").val(); 
	 dueDate=$('#datetimepicker1').data('date');
	 table.ajax.reload(); 
	 
	}

   function generate()
   {
	     cat=$("#category").val();
		 assign=$("#AUAll").val(); 
		 tel=$("#telecaller1").val();
		 dueDate=$('#datetimepicker1').data('date');
	   var rows_selected = [];
	    table = $('#example').DataTable({
	
	         "ajax": {
	        	 "url":"GetLeadParameterForSearch",	      
	 	        "data": function ( d ) {
	 	        	d.cat =cat;
		             d.assign=assign;
		             d.tel=tel;
		             d.dueDate=dueDate;
	           }
	 	        },
	 	       "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
 	      'columnDefs': [{
	         'targets': 0,
	         'searchable':false,
	         'orderable':false,
	         'width':'1%',
	         'className': 'dt-body-center',
	         'render': function (data, type, full, meta){
	             return '<input type="checkbox">';
	         }
	      }],
	      'order': [1, 'asc'],
	      'rowCallback': function(row, data, dataIndex){
	         var rowId = data[0];

	         if($.inArray(rowId, rows_selected) !== -1){
	            $(row).find('input[type="checkbox"]').prop('checked', true);
	            $(row).addClass('selected');
	         }
	      }
	   });
	    table.on( 'order.dt search.dt', function () {
	    	table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	    	     cell.innerHTML = i+1;
	    	 } );
	    	} ).draw();

	   $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
	      var $row = $(this).closest('tr');

	      var data = table.row($row).data();

	      var rowId = data[0];

	      var index = $.inArray(rowId, rows_selected);

	      if(this.checked && index === -1){
	         rows_selected.push(rowId);

	      } else if (!this.checked && index !== -1){
	         rows_selected.splice(index, 1);
	      }

	     /*  if(this.checked){
	         $row.addClass('selected');
	      } else {
	         $row.removeClass('selected');
	      } */
	      updateDataTableSelectAllCtrl(table);

	      e.stopPropagation();
	   });

	   $('#example').on('click', 'tbody td, thead th:first-child', function(e){
	      $(this).parent().find('input[type="checkbox"]').trigger('click');
	   });

	   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
	      if(this.checked){
	         $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
	      } else {
	         $('#example tbody input[type="checkbox"]:checked').trigger('click');
	      }

	      // Prevent click event from propagating to parent
	      e.stopPropagation();
	   });

	   table.on('draw', function(){
	      updateDataTableSelectAllCtrl(table);
	   });
	    
	   $('#btn-delete').on('click', function(e){
		   var ct=parseInt(0);
		   $.each(rows_selected, function(index, rowId){
	         
	        $.ajax({
			  	  url: 'CloseLead',
			  	  type: 'GET',
			  	  data:{Cvale:rowId},
			  	  success: function() {
			  		
			  		reloadtable();
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e);
			 	  }
				} );
	         
	        
	      }); 
	       $.smallBox({
				title : "Lead(s) are Closed Successfully.",
				
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				}); 
				
	     // $('input[name="id\[\]"]', form).remove();
			 

	      e.preventDefault();
	   }); 

	   
	    $('#btn-close').on('click', function(e){
	    	//alert("Ho...")
	      

	      // Iterate over all selected checkboxes
	      $.each(rows_selected, function(index, rowId){
	    //  alert(rowId)
	      var tcid=$("#telecaller").val();
	    	  $.ajax({
			  	  url: 'AssignIVRLead',
			  	  type: 'GET',
			  	  data:{tcId:tcid,leadId:rowId},
			  	  success: function(data) {
				  		//reloadtable()
			  		location.reload();

			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e);
			 	  }
				} );
	    	  
	    	 
	      }); 
	      $.smallBox({
				title : "Lead Successfullt Assign To "+$("#telecaller :selected").text()+".",
				/* content : "<i class='fa fa-clock-o'></i>", */
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				});
	    
	    //  $('input[name="id\[\]"]', form).remove();
	       

	      e.preventDefault();
	   }); 
   }

</script>
<body>

<div class="row">
<div class="col-md-8"><b>Searching Criteria</b></div>
 <div class="col-md-4"><b>Change Lead Role</b></div>
</div>
<div class="row">

  <div class="col-md-8">
  <!-- style="width:15%;float:left;" -->
   <select class="form-control" style="float: left; width: 20%;" id="category" >

  <option value="49">IVR Fin JPR</option>
  <option value="50">IVR Real JPR</option>
   </select>

<!-- style="width:15%;float:left;margin-left: 29px;" -->
<select class="form-control " style="float: left; width: 20%;" id="AUAll" onchange="">
<option value="All">Assign-Unassign</option><option value="Unassigned">Unassigned </option>
<option value="Assign">Assign</option></select>

   <select class="form-control"   id="telecaller1" style="width: 30%;display: inline;"></select>

<div class="col-xs-4 col-sm-4" style="width: 25%;">
        <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  </div>
 <button type="button" class="btn btn-danger" onclick="reloadtable()">Search</button>
   </div>
  
  <div class="col-md-4">
  <!-- <select class="form-control"  id="telecaller" style="width: 30%;"></select> -->
 
   <select class="form-control"  id="telecaller" style="width: 40%;display: inline;"></select>
 
    <button type="button" id="btn-close" class="btn btn-default" >Assign Leads</button>

</div>

<hr>
</div>
<!-- class="table table-striped table-bordered" -->
<div style="margin-top: 2%;" id="dataTbl">
<table id="example" class="table" cellspacing="0" width="100%">
   <thead>
      <tr>
     
         <th><input name="select_all" value="1" type="checkbox"></th>
          <th>Id</th>
         <th>Name</th>
          <th>Mobile No.</th>
          <th>Category</th>
         <th>Process State</th>
         
      </tr>
   </thead>
  
</table></div>
            <%
        Authentication auth2 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser2 = (MediUser) auth2.getPrincipal();
		long id=currentUser2.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %> 
</body>
</html>
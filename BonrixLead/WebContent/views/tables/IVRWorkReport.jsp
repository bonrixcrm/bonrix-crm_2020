<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script src="js/bootstrap-datetimepicker.js"></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script> 
<body>
<h2>IVR Work Report</h2>
<div class="row">
  <div class="col-xs-4 col-sm-2">
     <select class="form-control"  id="category" ></select>
  
  </div>
  <div class="col-xs-4 col-sm-2">
        <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  </div>
  
  
  <div class="col-xs-4 col-sm-2">
 <button type="button" class="btn btn-danger" onclick="reloadData()">Search</button>
  
  </div>
  
  </div>
  <p><br></p>
<table id="example" class='table table-striped table-bordered' cellspacing="0" width="100%">
        <thead>
            <tr>
            	<th>Id</th>
                <th>Name</th>
                <th>Mobile NO.</th>
                <th>Calling Time</th>
                <th>Call Duration</th>
            </tr>
        </thead>
    </table>

</body>
<script>
var getDate="NA";
var dataTable=null;

var tcId="3";
$(function () {
    $('#datetimepicker1').datetimepicker({
   	  defaultDate: new Date(), 
   	 
     format: 'YYYY-MM-DD'
        /* disabledDates: [
            moment("2016-12-30"),
            new Date(2013, 11 - 1, 21),
            "01/01/2016 00:53"
        ] */
    });
});

$( document ).ready(function() {


$.ajax({
  	  url: 'getTallyCallerByCompanyId',
  	  type: 'GET',
  	  success: function(data) {
  		
  		var msg=data;
  		 var html="<option value='0'>Select Tellycaller</option>";
  		var i=0;
  		
  		for(i=0;i<msg.length;i++)
  			{
  		
  			var val='"'+msg[i][0]+'"';
  				html+="<option value="+val+">"+msg[i][3]+" "+msg[i][4]+"</option>";
  			}
  		$('#category').html(html); 
  		
  		tcId=$('#category').val();
  		getDate=$('#datetimepicker1').data('date');
  		dataTable=  $('#example').DataTable( {
	        "ajax": {
	            "url": "GetIVRWorkDetails",
	            "data": function ( d ) {
		             d.getDate=getDate;
		             d.tcId=tcId;
	           },    
	        },   
	        "language": {
	            "emptyTable": "Select Telecaller to Show Data"
	          }
	        
	        
	       
	        
    });
 dataTable.on( 'order.dt search.dt', function () {
 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
     cell.innerHTML = i+1;
 } );
} ).draw(); 
  		
  	  },
	 error: function(e) {
 		//called when there is an error
 		console.log(e.message);
 	  }
	} );
	
});

function reloadData()
{
	getDate=$('#datetimepicker1').data('date');
	tcId=$('#category').val()
	dataTable.ajax.reload();
	
}
</script>
</html>
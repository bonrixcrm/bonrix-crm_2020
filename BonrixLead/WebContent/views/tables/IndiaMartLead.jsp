<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head></head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>IndiaMart Lead</title>
<body>
 <div id="datewise">
            <h3>IndiaMart Leads</h3> 
            <div class="row">
  <div class="col-xs-4 col-sm-2">
        <div class='input-group date' id='startDate'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  
  </div>
  <div class="col-xs-4 col-sm-2">
        <div class='input-group date' id='endDate'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  
  </div>
  <div class="col-xs-4 col-sm-2">
 <button type="button" class="btn btn-danger" onclick="reloadData()">Get Leads</button> 
  </div> 
  </div>
  <br>
              <table id="records_table" class="display" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                 <th>Mobile Number</th>
                 <th>ALT Mobile Number</th> 
                 <th>Email</th> 
                <th>Entry Date</th>
                <th>Company Name</th>   
                <th>State</th>
                <th>City</th>   
                 <th>Requirement</th>                   
            </tr>
            </thead> 
            <tbody id="records"></tbody>  
    </table> 
               </div>  
</body>

 <script type="text/javascript">
$(function () {
    $('#startDate').datetimepicker({
   	  defaultDate: new Date(), 
     format: 'DD-MMM-YYYY'
          });
    
    $('#endDate').datetimepicker({
     	  defaultDate: new Date(), 
       format: 'DD-MMM-YYYY'
      });
});

function reloadData()
{
		$.ajax({
			  url: 'glypticartsIndaiMartLeads?startDate='+$('#startDate').data('date')+'&endDate='+$('#endDate').data('date'),
			  type: 'GET',
			  success: function(response) {
				 try {
					 data = $.parseJSON(response);
					 var trHTML = '';
				        $.each(data, function (i, item) {
				            trHTML += '<tr><td>' + item.firstName + '</td><td>' + item.mobileNo + '</td><td>' + item.altmobileNo + '</td><td>' + item.email + '</td><td>' + item.createDate + '</td><td>' + item.companyName + '</td><td>' + item.state + '</td><td>' + item.city + '</td><td>' + item.csvData + '</td></tr>';
				        });
				        $('#records').html(trHTML);
				         $('#records_table').DataTable( {
    	
        "sDom": 'T<"clear">lfrtip',
			  "oTableTools" : {
			  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			  }
    } );
				 	}
					catch(err) {
					 alert("It is advised to hit this API once in every 15 minutes,but it seems that you have crossed this limit. please try again after 15 minutes.");
					  $('#records').append("");
				}
			  },
			  error: function(e) {
				console.log(e.message);
			  }
			});	
} 
</script>
</html>
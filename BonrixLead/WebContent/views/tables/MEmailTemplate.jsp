<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
		<style type="text/css">			
			#textarea-1{
				width: 700px;
				height: 200px;
				padding: 10px;
				border: 2px solid #ddd;
			}
		</style>
		<style>

td.details-control {
    background: url('css/img/details.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('css/img/details_close.png') no-repeat center center;
}
</style>
	<!-- <script type="text/javascript" src="js/summernote.min.js"></script>  -->
		<script type="text/javascript">
		var table=null;
 			 	 $(document).ready(function() {
 			 		 $('#summernote1').summernote({ height: 200,}); 
 			 		 $('#summernote_edit').summernote({height: 200,}); 
 			 		table =  $('#example').DataTable( {
 			        "ajax": "getCRMEmailTemplate",
 			        "columns": [
 			            { "data": "id" },
 			            { "data": "tempname" },
 			            { "data": "subject" },
 			          { "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal2'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-warning' id='show-btn' data-toggle='modal' data-target='#myModal1'><span class='glyphicon  glyphicon-fullscreen' aria-hidden='true'></span>  Show  </button>"}
 			        ]
 			    } );
 				 
 			 		$('#example tbody').on( 'click', '#delete-btn', function () {
				        var data = table.row( $(this).parents('tr') ).data();
				        $.ajax({
							   url:'deleteCRMEmailTemplate',
							   type:'GET',
							   data:{id:data['id']},
							   success:function(){
								   table.ajax.reload();
							   },
							   error: function(e) {
							 		console.log(e.message);
							 	  } 
						   });
				     } );
 			 		
 				 $('#example tbody').on( 'click', '#show-btn', function () {
				        var data = table.row( $(this).parents('tr') ).data();
				        var fieldNameElement = document.getElementById('txt');
				        fieldNameElement.innerHTML = data[ 'temptext' ];
				     } );
 				 		 
 				 $('#example tbody').on( 'click', '#edit-btn', function () {
				         var data = table.row( $(this).parents('tr') ).data();
				      $('#summernote_edit').summernote('editor.reset');
				        $("#tid").val(data["id"]);
				        $("#tsubject").val(data["subject"]);
				        $("#tname").val(data["tempname"]);
				        $("#summernote_edit").summernote("code", data[ 'temptext' ]);
				     } );
 			} ); 
 			 
		function changeTempalteData()
		{ 
			 $.ajax({
				   url:'updateEmailTemplateDetail',
				   type:'POST',
				   data:{tid:$("#tid").val(),tsubject:$("#tsubject").val(),tText:$('#summernote_edit').summernote('code'),tempName:$("#tname").val()},
				   success:function(data){
					   table.ajax.reload();
				   },
				   error: function(e) {
				 		console.log(e.message);
				 	  } 
			   });
		}
		
		function addTemplate()
		{
			var tem=encodeURI($('#summernote1').summernote('code'));
			 $.ajax({
				   url:'AddEmailTemplateDetail',
				   type:'POST',
				   data:{tname:$("#tname_new").val(),tsubject:$("#tsubject1").val(),tText:tem},
				   success:function(){
					   table.ajax.reload();	  
				   },
				   error: function(e) {
				 		console.log(e.message);
				 	  } 
			   });
		}
		</script>
	</head>
	<body>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Email Template</h2>
			<div class="widget-toolbar" role="menu">
			<button class="btn btn-sm btn-danger"   data-toggle="modal" data-target="#myModal3"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span> New Template </button>
					</div>
		</header>
		<div>
			<div class="jarviswidget-editbox"></div>
			<div class="widget-body no-padding">
			<table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%"  style="background-color: white; ">
        <thead>
            <tr>
              <th width="1%">Id</th> 
              <th width="5%">Template Name</th>
                <th width="5%">Subject</th>
                <th width="10%">Action</th>   
            </tr>
        </thead>
        </table> 

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Template View</h4>
      </div>
      <div class="modal-body">
      <form method="get">
     <div id="txt" class="editable" contentEditable = true; ></div> 
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document" style=" width:90%" height="20%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Template Data</h4>
      </div>
      <div class="modal-body">
      <form method="get">
   <input type="hidden" id="tid" class="form-control" >
        <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Name</b></div>
   <div class="col-xs-4 col-sm-4"><input type="text" id="tname" class="form-control" ><br></div>
        </div>
        <hr>
        <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Subject</b></div>
   <div class="col-xs-4 col-sm-4"><input type="text" id="tsubject" class="form-control" ><br></div>
        </div>
        <hr>
        <div class="row">
   <div class="col-xs-6 col-sm-12">  <div class="summernote" id="summernote_edit"></div> </div>
        </div>
        <hr>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-default" data-dismiss="modal" onclick="changeTempalteData()">Change Data</button>
      </div>
    </div>
  </div>
</div>

	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document" style=" width:90%" height="20%">
    <div class="modal-content">
     <!-- <form method="get" onsubmit="" method="get"> -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Template</h4>
      </div>
      <div class="modal-body">
        <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Name</b></div>
   <div class="col-xs-4 col-sm-4"><input type="text" id="tname_new" class="form-control" ></div>
        </div>
        <hr>
        <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Subject</b></div>
   <div class="col-xs-4 col-sm-4"><input type="text" id="tsubject1" class="form-control" ></div>
        </div>  
        <div class="row">
   <div class="col-xs-6 col-sm-12">  <div class="summernote" id="summernote1"></div> </div>
        </div>
        <hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-default" data-dismiss="modal" onclick="addTemplate()" >Add Template</button> 
      </div>
    </div>
  </div>
</div>
			</div>
		</div>
	</div>
	</body>
</html>
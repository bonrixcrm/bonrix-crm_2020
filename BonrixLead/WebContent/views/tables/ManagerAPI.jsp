<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Category Manage</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Category</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Name</th>
							<th>Action</th>
							
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>
</body>
</html>
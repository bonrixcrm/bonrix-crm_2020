<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

 <style>
 .morris-hover{position:absolute;z-index:1000}.morris-hover.morris-default-style{border-radius:10px;padding:6px;color:#666;background:rgba(255,255,255,0.8);border:solid 2px rgba(230,230,230,0.8);font-family:sans-serif;font-size:12px;text-align:center}.morris-hover.morris-default-style .morris-hover-row-label{font-weight:bold;margin:0.25em 0}
.morris-hover.morris-default-style .morris-hover-point{white-space:nowrap;margin:0.1em 0}

 </style>
 
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
 <link href="css/img/styles.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet" />  
</head>
<script>
var catData="";
$(document).ready(function() {
	catChart();
	successFailChaert();
	DailyChaet();
	leadProcssChart();
	leadSuccessStateChart();
	leadFailStateChart();
	drawStacked();
	$("#bar-example2").attr("width",100);
	$.ajax({
		  url: 'BonrixDashBoard',
		  type: 'GET',
		  data: "leadProcessStatus=InProcess",
		  success: function(data) {
		  	$("#totalLead").html(data[0][0])
		  	$("#todayLead").html(data[0][1])
		  	$("#Inprocess").html(data[0][2])
		  	$("#closelLead").html(data[0][3])
		  	$("#OtotalLead").html(data[0][4])
		  	$("#Ototalfollow").html(data[0][5])
		  },
		 error: function(e) {
			console.log(e.message);
		  }
		} );
});
function drawStacked() {
	$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
		$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
			var data1="";
			 $.getJSON('GetTelecallerCallCount', function(data){
	var data = data,
	    config = {
	      data: data,
	      xkey: 'Month',
	      ykeys: ['MonthCount','SuccessCall', 'FailCall'],
	      labels: ['Total Calls','Success Calls', 'Fail Calls'],
	      fillOpacity: 0.6,
	      hideHover: 'auto',
	      behaveLikeLine: true,
	      resize: true,
	      pointFillColors:['#ffffff'],
	      pointStrokeColors: ['black'],
	      lineColors:['red','green'],
	      barColors:['#4285F4','#F4B400','#DB4437'],
	     /*  barColors:['#F27077','#9DD374'], */
	      xLabelAngle: 40
	  };
	config.element = 'stacked';
	config.stacked = true;
	Morris.Bar(config);
		});
	}); 
	});
  }
function catChart()
{
	 var dd 	=[];
	var dd1="";
	var catary=[];
	$.ajax({
		  url: 'GetCategoryLeadCount',
		  type: 'GET',
		  success: function(data) {
			  console.log(data)
			  for(i=0;i<data.length;i++)
	  			{
				for(var i=0;i<data.length;i++)
					{
				  var obj = {y: data[i][0], a: parseInt(data[i][1])};
				    dd.push(obj);
	  			}
	  			}
		  },
		 error: function(e) {
			console.log(e.message);
		  }
		} );
	$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
		$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
    Morris.Bar({
   	  barSizeRatio:0.30, 
        element: 'bar-example2',
        data:dd,
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Lead'],
        barColors:['#4DA74D'],
        hideHover: 'auto',
        resize: true ,
        xLabelAngle: 70
     }); 
		});
	}); 
	
}
 function successFailChaert()
{
	 $("#lbl21").html(" Success-Fail Lead Year - "+ (new Date()).getFullYear());
	 var data1="";
	 $.getJSON('GetMonthlySuccessFailLogCount', function(data){
		    $.each(data, function (index, value) {
		     data1=data;
		    });
		});
	$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
		$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
			Morris.Bar({
				  element: 'bar-example',
				  data: data1,
				  xkey: 'Month',
				  ykeys: ['SuccessCall', 'FailCall'],
				  labels: ['Success Calls', 'Fail Calls'],
				  barColors: ["#31C0BE", "#c7254e"] ,
				  xLabelAngle: 70
				});
	 });
	}); 
} 
 function DailyChaet()
 {
	 var monthNames = ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		];
		var d = new Date();
		$("#lbl2").html("Success Fail Leads By Month - "+monthNames[d.getMonth()]);
	 var data1="";
	 $.getJSON('GetDailySuccessFailLogCount', function(data){
		    $.each(data, function (index, value) {
		     data1=data;
		    });
		});
	$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
		$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
			 Morris.Bar({
				  element: 'area-example',
				  data: data1,
				   barColors: ["#1531B2","#B21516",  "#1AB244", "#B29215"], 
				  xkey: 'Month',
				  ykeys: ['SuccessCall', 'FailCall'],
				  labels: ['Success Calls', 'Fail Calls'],
				  xLabelAngle: 70
				}); 
			 
	 });
	}); 
 }
 
 function leadProcssChart()
 {
	 var data1="";
	 $.getJSON('GetCountByLeadState', function(data){
		    $.each(data, function (index, value) {
		     data1=data;
		    });
		});
	 
	 $.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
			$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
				 Morris.Line({
					  element: 'line-example',
					  data: data1,
					parseTime:false,
					   xkey: 'Property',
					  ykeys: ['Count'],
					  labels: ['Lead Count '],
					  lineColors: ['#c7254e'],
					  xLabelAngle: 40 
					  
					});
			});
		}); 
 }

 function leadSuccessStateChart()
 {
	 var data1="";
	 $.getJSON('GetCountBySuccessState', function(data){
		    $.each(data, function (index, value) {
		     data1=data;
		    });
		});
	 $.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
			$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
				 Morris.Line({
					  element: 'success-line-example',
					  data: data1,
					parseTime:false,
					   xkey: 'Property',
					  ykeys: ['Count'],
					  labels: ['Success State Count '],
					  lineColors: ['#31C0BE'],
					   xLabelAngle: 40 
					  
					});
			});
		}); 
 }
 
 function leadFailStateChart()
 {
	 var data1="";
	 $.getJSON('GetCountByFailState', function(data){
		    $.each(data, function (index, value) {
		     data1=data;
		    });
		});
	 $.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
			$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
				 Morris.Line({
					  element: 'fail-line-example',
					  data: data1,
					parseTime:false,
					   xkey: 'Property',
					  ykeys: ['Count'],
					  labels: ['Success State Count '],
					   xLabelAngle: 40 
					  
					});
			});
		}); 
 }
</script>
<body>
               	<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div><!--/.row-->
		
		<div class="row" >
			<div class="col-xs-12 col-md-6 col-lg-3" style=""  >
				<div class="panel panel-blue panel-widget ">
					<div class="row no-padding" style=" border:2px solid; border-color:#30A5FF;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
							<!--  <i class="fa fa-th-list fa-4x" style=" padding-top:1px;padding-left:5px;"></i>  -->
							<div style=" padding-top:-90px;padding-left:5px;"><img alt="scsd" src="img/lead.png" ></div>
							<!-- <span class=" glyphicon glyphicon-th-list " aria-hidden="true"></span> -->
						</div>
						<div class="col-sm-9 col-lg-7 widget-right" style="border-radius: 0px;" >
							<div class="large" id="totalLead">00</div>
							<div class="text-muted">Total Leads</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-orange panel-widget">
					<div class="row no-padding" style=" border:2px solid; border-color:#FFB53E;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
						<div style=" padding-top:-90px;padding-left:5px;"><img alt="scsd" src= "img/calendar177.png"></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large" id="todayLead">00</div>
							<div class="text-muted">Due Leads</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding" style=" border:2px solid; border-color:#1EBFAE;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
							<div style=" padding-top:-2px;padding-left:5px;"><img alt="scsd" src="img/clock125.png" ></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large" id="Inprocess">00</div>
							<div class="text-muted">Schedule Today</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-red panel-widget">
					<div class="row no-padding" style=" border:2px solid; border-color:#F9243F;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
								<div style=" padding-top:-90px;padding-left:5px;"><img alt="scsd" src="img/cross108.png" ></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large" id="closelLead">00</div>
							<div class="text-muted">Close Lead</div>
						</div>
					</div>
				</div>
			</div>
		</div>
                <hr />                
               
                <div class="row"> 
                               <div class="col-md-9 col-sm-12 col-xs-12">                     
                    <div class="panel panel-info">
                        <div class="panel-heading" id="lbl21">
                           
                        </div>
                        <div class="panel-body">
                            <div class="container-fluid">
  
  <div class="row">
    <div class="col-md-12">
        <div id="bar-example" ></div>
    </div>
  </div>
</div> 
                        </div>
                    </div>            
                </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">                       
                    <div class="panel panel-primary text-center no-boder bg-color-green">
                        <div class="panel-body">
                            <i class="fa fa-bar-chart-o fa-5x"></i>
                            <div class="row">
   							 <h3 style="color: white;" id="OtotalLead">00</h3>
  </div>
                        </div>
                        <div class="panel-footer back-footer-green">
                          Overall Leads
                            
                        </div>
                    </div>
                    <div class="panel panel-primary text-center no-boder bg-color-red">
                        <div class="panel-body">
                            <i class="fa fa-edit fa-5x"></i>
                            <h3 style="color: white;" id="Ototalfollow">00</h3>
                        </div>
                        <div class="panel-footer back-footer-red">
                            Overall Success FollowUps
                            
                        </div>
                    </div>                         
                        </div>
                
           </div>
                 
          <div class="col-lg-12" >
				<div class="panel panel-success">
					<div class="panel-heading" id="lbl2">Daily Success Fail Leads</div>
					<div class="panel-body" >
						 <div class="container-fluid2">
  <div class="row" >
    <div class="col-md-12">
        <div  id="area-example"  >
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>      
			
			<!--  -->
			<div class="col-lg-12" >
				<div class="panel panel-warning">
					<div class="panel-heading" id="lbl2">Lead State Summary</div>
					<div class="panel-body" >
						 <div class="container-fluid2">
  <div class="row" >
    <div class="col-md-12"> 
        <div  id="line-example"  >
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>      
               
               <div class="col-lg-12" >
				<div class="panel panel-warning">
					<div class="panel-heading" >Call Log By Telecaller</div>
					<div class="panel-body">
						 <div class="container-fluid">
  <div class="row" >
    <div class="col-md-12">
        <div  id="stacked" style="width:100%;heighr:100%">
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>
               
                 
            <!--  -->
			<div class="col-lg-12" >
				<div class="panel panel-success">
					<div class="panel-heading" id="lbl2">Success State Summary</div>
					<div class="panel-body" >
						 <div class="container-fluid2">
  <div class="row" >
    <div class="col-md-12"> 
        <div  id="success-line-example"  >
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>   
			     
		<!--  -->
		 <div class="col-lg-12" >
				<div class="panel panel-danger">
					<div class="panel-heading" id="lbl2">Fail State Summary</div>
					<div class="panel-body" >
						 <div class="container-fluid2">
  <div class="row" >
    <div class="col-md-12">
        <div  id="fail-line-example"  >
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>  
		<div class="col-lg-12" >
				<div class="panel panel-warning">
					<div class="panel-heading" >Category Wise Lead Overview</div>
					<div class="panel-body">
						 <div class="container-fluid">
  <div class="row" >
    <div class="col-md-12">
        <div  id="bar-example2" style="width:100%;heighr:100%">
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>
			
			
				<br><br>
			
</body>
</html>
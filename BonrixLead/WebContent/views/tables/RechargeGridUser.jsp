
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">


		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>

	</div>

</div>
<%@include file="/includes/SessionCheck.jsp"%>
<div class="modal fade" id="remoteModal2" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>
<div class="modal fade" id="remoteModal3" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 850px;"></div>
	</div>
</div>

<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-rechargegriduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Recharge Grid</h2>
			<div class="widget-toolbar" role="menu">
						<a href="./user/createuser.jsp"
				data-toggle="modal" data-target="#remoteModal"
				class="btn btn-danger"> <i class="fa fa-circle-arrow-up fa-lg"></i>
				Create User
			</a>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="rechargeusertable_tabletools"
					class="table table-striped table-bordered" style="margin: auto;text-align: center;" >
					<thead>


						<tr role="row">
							<th style="width: 2%;">ID</th>
							<th>Name</th>
							<th>UserName</th>
							<th>Mobile</th>
							<th>Status</th>
							<th data-hide="phone">RechargeIn</th>
							<th width="10%" data-hide="phone,tablet">RechargeOut</th>
							<th width="5%" data-hide="phone,tablet">NotificationIn</th>
							<th width="5%" data-hide="phone,tablet">NotificationOut</th>							
							<th width="5%">Action</th>

						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>


</section>

<script type="text/javascript">
var usertable=null;
	function reloadtable(){
	  usertable.ajax.reload();	
	}
	var pagefunction = function() {
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			 $(this).removeData('bs.modal');	
			 reloadtable();	
			});
		 $('#remoteModal2').on('hidden.bs.modal', function () {
			 $(this).removeData('bs.modal');
			
			 reloadtable();	
			});
		 $('#remoteModal3').on('hidden.bs.modal', function () {
			 $(this).removeData('bs.modal');
			
			 reloadtable();	
			});
	};
	
	
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
			loadScript("js/plugin/datatables/dataTables.tableTools.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){					
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
				});
			});
		});
	});

	
	function getdata(){
//	alert("asdgasdg");	
		 
//		alert("getdata");
	      usertable=	$('#rechargeusertable_tabletools').DataTable({
	        "processing": false,
	        "serverSide": false,
	        "bServerSide": false, 
	        
	        "bdestroy": true,
	        
	      //  "bDestroy": true,
	        "ajax": "getConnectedClientList","columns": [
	                                          { "data": "uid" },
	                                          { "data": "user" },
	                                          { "data": "username" },
	                                          { "data": "mobile" },
	                                          { "data": "isactive" },
	                                          { "data": "usercon" },
	                                          { "data": "usernotifycon" },
	                                          { "data": "providercon" },
	                                          { "data": "providernotifycon" }, {
	                                              "targets": -1,
	                                              "data": null,
	                                              "defaultContent": "<button>Click!</button>"
	                                          }
	                                          
	                                          
	                                      ], "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	         
	       	if(aData.isactive){
	        	$('td:eq(4)', nRow).html('<a class="btn btn-success btn-xs" href="#" onclick="deactiveit('+aData[0]+',0)" >Active</a>');
	        	}else
	        		{
	        		$('td:eq(4)', nRow).html('<a class="btn btn-danger btn-xs" href="#" onclick="deactiveit('+aData[0]+',1)">Deactive</a>');	
	        		}
	        	
	       	$('td:eq(9)', nRow)
			.html(
					'<div class="btn-group display-inline pull-right text-align-left hidden-tablet">	<button class="btn btn-xs btn-default dropdown-toggle"	data-toggle="dropdown">'
+'<i class="fa fa-cog fa-lg"></i></button><ul class="dropdown-menu dropdown-menu-xs pull-right"><li><a data-toggle="modal" data-target="#remoteModal" href="./user/updateUser.jsp?uid='+ aData.uid+ '">'
+'<i class="fa fa-file fa-lg fa-fw txt-color-greenLight"></i> <u>E</u>DIT</a></li><li><a data-toggle="modal" data-target="#remoteModal" href="./user/reSetUserPassword.jsp?uid='+ aData.uid+ '">'
+'<i class="fa fa-file fa-lg fa-fw txt-color-greenLight"></i> <u>R</u>ESET PASSWORD</a></li><li><a href="#" onclick="delhttp('+ aData.uid+ ')")"><i class="fa fa-times fa-lg fa-fw txt-color-red"></i> <u>D</u>elete</a>'
+'</li><li class="divider"></li><li class="text-align-center"><a href="javascript:void(0);">Cancel</a></li></ul></div>');
	        	
	        	//$('td:eq(7)', nRow).html('<a href="./views/tables/UserListAdmin.jsp?uids='+aData[0]+'" data-toggle="modal" data-target="#remoteModal2" >'+aData[7]+'</a>');
	        	//	 $('td:eq(8)', nRow).html('<a href="./user/updateUser.jsp?uid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" ><img height=24 width=24 title="Update User" src="img/edit_page.png"></a><a href="./user/reSetUserPassword.jsp?uid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" ><img title="Reset password" height=24 width=24 src="img/resetpass.png"></a>'	        		 
	        	//	 +'<a   onclick="delhttp('+ aData[0]+ ')");"><img title="Delete User" height=24 width=24 src="img/edit-delete.png"></a>');
	        		 //<a class="btn btn-danger btn-xs" href="deleteUser?uid=${all.uid}" onclick="return confirm('Are you sure?');">Delete</a>
	            
	        },
	         
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>", 
	         "oTableTools": {
	        	 "aButtons": [
	             "copy",
	             "csv",
	             "xls",
	                {
	                    "sExtends": "pdf",
	                    "sTitle": "SmartSMS_PDF",
	                    "sPdfMessage": "SmartSMS PDF Export",
	                    "sPdfSize": "letter"
	                },
	             	{
	                	"sExtends": "print",
	                	"sMessage": "Generated by  <i>(press Esc to close)</i>"
	            	}
	             ],
	            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
	        }, 
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
		
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
		
		
	}
	
	
	function smartconfirm(e) {
		$.SmartMessageBox({
			title : "Alert!",
			content : "Are your sure to Delete this user ?",
			buttons : '[No][Yes]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "Yes") {

				$.smallBox({
					title : "Callback function",
					content : "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",
					color : "#659265",
					iconSmall : "fa fa-check fa-2x fadeInRight animated",
					timeout : 4000
				});
			}
			if (ButtonPressed === "No") {
				$.smallBox({
					title : "Callback function",
					content : "<i class='fa fa-clock-o'></i> <i>You pressed No...</i>",
					color : "#C46A69",
					iconSmall : "fa fa-times fa-2x fadeInRight animated",
					timeout : 4000
				});
			}

		});
	};

	function delhttp(id){
		
		$.SmartMessageBox({
			title : "Alert!",
			content : "Are your sure to Delete this user ?",
			buttons : '[No][Yes]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "Yes") {

				
				$.get("deleteUser?uid="+id,function(msg){
					
					reloadtable();
					$.smallBox({
						title : "Success",
						content : "<i class='fa fa-clock-o'></i> <i>User Deleted Successfully...</i>",
						color : "#659265",
						iconSmall : "fa fa-check fa-2x fadeInRight animated",
						timeout : 4000
					});
				});
				
			}
			if (ButtonPressed === "No") {
				$.smallBox({
					title : "OK Sir",
					content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
					color : "#C46A69",
					iconSmall : "fa fa-times fa-2x fadeInRight animated",
					timeout : 4000
				});
			}

		});

		
	}
	
	
	function deactiveit(uid,act){
	
			$.SmartMessageBox({
				title : "Alert!",
				content : "Are your sure to "+act==1?"Active":"Deactive"+" this user ?",
				buttons : '[No][Yes]'
			}, function(ButtonPressed) {
				if (ButtonPressed === "Yes") {

					
					$.get("user/activedeactive?uid="+uid+"&isactive="+act,function(msg){
						
						reloadtable();
					  var stats="";	
						if(act==1){
							stats="<i class='fa fa-clock-o'></i> <i>User Activated Successfully...</i>"	
						}else{
							stats="<i class='fa fa-clock-o'></i> <i>User Deactivated Successfully...</i>"		
						}
						$.smallBox({
							title : "Success",
							content : stats,
							color : "#659265",
							iconSmall : "fa fa-check fa-2x fadeInRight animated",
							timeout : 4000
						});
					});
					
				}
				if (ButtonPressed === "No") {
					$.smallBox({
						title : "OK Sir",
						content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
						color : "#C46A69",
						iconSmall : "fa fa-times fa-2x fadeInRight animated",
						timeout : 4000
					});
				}

			});

			
		}
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
var dataTable=null;
var id=null;
$(document).ready(function() {
	$("#passwordfield").on("keyup",function(){
		if($(this).val())
	        $(".glyphicon-eye-open").show();
	    else
	        $(".glyphicon-eye-open").hide(); 
	    });
	$(".glyphicon-eye-open").mousedown(function(){
	                $("#passwordfield").attr('type','text');
	            }).mouseup(function(){
	            	$("#passwordfield").attr('type','password');
	            }).mouseout(function(){
	            	$("#passwordfield").attr('type','password');
	            }); 
	
	         dataTable = $('#cat-grid').DataTable({
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetTellyCaller",
	                "fnCreatedRow": function( nRow, aData, iDataIndex ) {
		                    if(aData[5]==true)
		                    $('td:eq(5)', nRow).html("<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm ' id='changepass-btn' data-toggle='modal' data-target='#myModal46'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  ChangePassword  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-primary' id='show-btn' data-toggle='modal' data-target='#myModal3'><span class='glyphicon glyphicon-fullscreen' aria-hidden='true'></span>  Show  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-warning' id='show-btn' data-toggle='modal' data-target='#myModal4'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>  Password  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-info' id='edit-btn' onclick='setADctive("+aData[5]+","+aData[0]+")'><span class='glyphicon glyphicon-user' aria-hidden='true'></span>  DeActive  </button>");
		                    else 
		                    	 $('td:eq(5)', nRow).html("<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm' id='changepass-btn' data-toggle='modal' data-target='#myModal46'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  ChangePassword  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-primary' id='show-btn' data-toggle='modal' data-target='#myModal3'><span class='glyphicon glyphicon-fullscreen' aria-hidden='true'></span>  Show  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-warning' id='show-btn' data-toggle='modal' data-target='#myModal4'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>  Password  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-info' id='edit-btn' onclick='setADctive("+aData[5]+","+aData[0]+")'><span class='glyphicon glyphicon-user' aria-hidden='true'></span>  Active  </button>");
			        }, 
	        });
	         
	         dataTable.on( 'order.dt search.dt', function () {
	        	 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	                cell.innerHTML = i+1;
	            } );
	        } ).draw();

$('#cat-grid tbody').on( 'click', '#edit-btn', function () {
   var data = dataTable.row( $(this).parents('tr') ).data();
   id=data[0];
   var fullname=data[2];
   var name=fullname.split(" ")
    $('#id').val(data[0]);
    $('#uname').val(data[1]);
    $('#fname').val(name[0]);
    $('#lname').val(name[1]);
    $('#email').val(data[3]);
    $('#cno').val(data[4]);
} );

$('#cat-grid tbody').on( 'click', '#changepass-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    id=data[0];
    console.log('id:::'+ id)
    $('#changepassid').val(data[0]);
       
});

$('#cat-grid tbody').on( 'click', '#show-btn', function () {
	var uname=$('#cmp').val();
    var data = dataTable.row( $(this).parents('tr') ).data();
    var fullname=data[2];
    var name=fullname.split(" ")
    $('#tid').text(data[0]);
    $('#tuname').text(data[1]);
    $('#tfname').text(name[0]);
    $('#tlname').text(name[1]);
    $('#temail').text(data[3]);
    $('#tcno').text(data[4]);
    $('#tcomp').text(uname);
    $('#tregdate').text(data[8]);
    $('#passwordfield').text(data[9])
} );
} );

function setADctive(val1,val2)
{
var ststus=val1;
var tcid=val2;
$.ajax({
	  url: 'ADuser',
	  type: 'GET',
	  data:{ststus:ststus,tcid:tcid},
	  success: function(data) {
		  dataTable.ajax.reload();
	  },
	  error: function(e) {
		  dataTable.ajax.reload();
		console.log(e.message);
	  }
	}); 
}
function saveChangeTallyCaller()
{
var uid=$('#uid').val();
var uname=$('#uname').val();
var fname=$('#fname').val();
var lname=$('#lname').val();
var email=$('#email').val();
var cno=$('#cno').val();
var comastr=id+","+uname+","+fname+","+lname+","+email+","+cno;

if(uname.includes('_')){
	$.smallBox({
		title : "Remove UnderScore(_) From UserName.",
		color : "#296191",
		iconSmall : "fa fa-thumbs-up bounce animated",
		timeout : 4000
		});
}else{
	 $.ajax({
		  url: 'UpdateTallycaller',
		  type: 'GET',
		  data:"comastr="+comastr,
		  success: function(data) {
			  $('#myModal').modal('hide');
			  dataTable.ajax.reload();
			  $.smallBox({
					title : "Telecaller Successfully Updated.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					});
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		}); 
}

}

function addTellyCaller(){
	var uname1=$('#uname1').val();
	var pass1=$('#pass1').val();
	var fname1=$('#fname1').val()==""?'N/A':$('#fname1').val();
	var lname1=$('#lname1').val()==""?'N/A':$('#lname1').val();
	var email1=$('#email1').val()==""?'N/A':$('#email1').val();
	var cno1=$('#cno1').val()==""?'N/A':$('#cno1').val();
	var comastr=uname1+","+pass1+","+fname1+","+lname1+","+email1+","+cno1;

//	alert("----"+ uname1.includes('_'))
	
	if(uname1.includes('_')){
		$.smallBox({
			title : "Remove UnderScore(_) From UserName.",
			color : "#296191",
			iconSmall : "fa fa-thumbs-up bounce animated",
			timeout : 4000
			});
	}else{
		if(uname1=="" || pass1=="")	{
			if(uname1=="")
				document.getElementById('uname1').style.borderColor='red';
			
			if(pass1=="")
				document.getElementById('pass1').style.borderColor='red';
		}else{
		document.getElementById('uname1').style.borderColor='';
		document.getElementById('pass1').style.borderColor='';	  
		$.ajax({
			  url: 'addTellyCaller',
			  type: 'GET',
			  data:"comastr="+comastr,
			  success: function(data) {
				  var msg=data.toString();
				if(msg=="0"){
				  $('#myModal1').modal('hide');
				  dataTable.ajax.reload();
				  $.smallBox({
	  				title : "Telecaller Successfully Added.",
	  				color : "#296191",
	  				iconSmall : "fa fa-thumbs-up bounce animated",
	  				timeout : 4000
	  				});
			     }else if(msg=="-1"){
					  //$('#myModal1').modal('hide');
					 // dataTable.ajax.reload();
					  $.smallBox({
		  				title : "Telecaller UserName Found. Try Another Username",
		  				color : "#b32400",
		  				iconSmall : "fa fa-thumbs-up bounce animated",
		  				timeout : 4000
		  				});
				  }else{
					  $('#myModal1').modal('hide');
					  dataTable.ajax.reload();
					  $.smallBox({
		  				title : "Telecaller Limit is Over.",
		  				color : "#b32400",
		  				iconSmall : "fa fa-thumbs-down bounce animated",
		  				timeout : 5000
		  				});
				  }
			  },
			  error: function(e) {
				console.log(e.message);
			  }
			});  
		}
	}
}
function closepopup()
{
	 $('#myModal').modal('hide');
}
function checkMasterPassword()
{
	$.ajax({
		  url: 'GetUserData',
		  type: 'GET',
		  data:'mstPass='+$("#mstpass").val(),
		  success: function(data) {
			  var res=data.toString();
			  if(res=="Success")
				  {
			  $('#myModal4').modal('hide');
			   $("#mstpass").val(""); 
			  $('#myModal5').modal('show');
				  }
			  else
				  {
				   $("#mstpass").val(""); 
				  $.smallBox({
      				title : "Invalid Master Password.",
      				color : "#8B0000",
      				iconSmall : "fa fa-thumbs-o-down",
      				timeout : 4000
      				});
				  }
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
}


function checkMasterPasswordNew()
{
	$.ajax({
		  url: 'GetUserData',
		  type: 'GET',
		  data:'mstPass='+$("#mstpassnw").val(),
		  success: function(data) {
			  var res=data.toString();
			  if(res=="Success")
				  {
			  $('#myModal46').modal('hide');
			   $("#mstpassnw").val(""); 
			  $('#myModal45').modal('show');
				  }
			  else
				  {
				   $("#mstpassnw").val(""); 
				  $.smallBox({
      				title : "Invalid Master Password.",
      				color : "#8B0000",
      				iconSmall : "fa fa-thumbs-o-down",
      				timeout : 4000
      				});
				  }
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
}

function changeTCPass(){
	var oldpass = $('#oldpass').val();
	var newpass = $('#newpass').val();
	var tcId = $('#changepassid').val();
	
	$.ajax({
		url : "tcallerChangePass",
		type : "GET",
		data : {
			'oldpassword' : oldpass,
			'newpassword':newpass,
			'tcId' :tcId
		},
		success : function(response) {
			//alert(response);
			 var res=response.toString();
			 
			 if(res == '1'){
				 $.smallBox({
	      				title : "Password Change Successfully.",
	      				color : "#005c28",
	      				iconSmall : "fa fa-thumbs-o-up",
	      				timeout : 4000
	      				});
				 $('#oldpass').val("");
				 $('#newpass').val("");
				 $('#myModal45').modal('hide');
			 }else if(res == '2'){
				 $.smallBox({
	      				title : "Invalid Old Password.",
	      				color : "#8B0000",
	      				iconSmall : "fa fa-thumbs-o-down",
	      				timeout : 4000
	      				});
				 
			 }

		},
		error : function(xhr, status, error) {
			alert("get error");
			alert(xhr.responseText);
		}
	});
	
	
	
}
</script>
</head>

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">


		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>

	</div>

</div>
<%@include file="/includes/SessionCheck.jsp"%>
<div class="modal fade" id="remoteModal2" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>
<div class="modal fade" id="remoteModal3" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 850px;"></div>
	</div>
</div>

<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Telecaller Management</h2>
			<div class="widget-toolbar" role="menu">
						
				
				<button class="btn btn-sm btn-danger"   data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span> Add Telecaller</button>
				
				
			</a>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table  id="cat-grid"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>User Name</th>
							<th>Name</th>
							<th>Email</th>
							<th>Mobile Number</th>
							<th>Action</th>
						
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Telecaller Data</h4>
      </div>
      <div class="modal-body">
      <form method="get">
       <div class="row">
  <div class="col-xs-6 col-sm-3">Id</div>
  <div class="col-xs-6 col-sm-3" id="catled"><input type="text" id="id" class="form-control" readonly="readonly"></div>
  <div class="col-xs-6 col-sm-3">User Name</div>
  <div class="col-xs-6 col-sm-3" ><input type="text" id="uname" class="form-control" ></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-3">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fname" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="lname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-3">Email</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="email" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Contact No</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cno" class="form-control"></div>
        </div>
        
        
        <%
        Authentication auth2 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser2 = (MediUser) auth2.getPrincipal();
		long id=currentUser2.getUserid();
		String uname=currentUser2.getUsername();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
		out.println("<input type='hidden' id='uname' value='"+uname+"'>");
        
        
        %>
        
       <!--  </table> -->
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeTallyCaller();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save Telecaller</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Telecaller </h4>
      </div>
      <div class="modal-body">
      <form method="get">
       <div class="row">
  <div class="col-xs-6 col-sm-3">User Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="uname1" class="form-control" ></div>
  <div class="col-xs-6 col-sm-3">Password</div>
  <div class="col-xs-6 col-sm-3" ><input type="password" id="pass1" class="form-control" ></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-3">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fname1" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="lname1" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-3">Email</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="email1" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Contact No</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cno1" class="form-control"></div>
        </div>
        
        
       <%--  <%
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser1 = (MediUser) auth1.getPrincipal();
		long id1=currentUser1.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        
        
        %> --%>
        
       <!--  </table> -->
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="addTellyCaller()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width: 60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Telecaller Full Detail</h4>
      </div>
      <div class="modal-body" >
      <form method="get">
       <table border="1" class="table table-bordered">
       <tr>
       <th class="">Id</th>
       <td><label id="tid"></label></td>
       <th class="">Username</th>
       <td><label id="tuname"></label></td>
       </tr>
       
       <tr>
       <th class="">First Name</th>
       <td><label id="tfname"></label></td>
       <th class="">Last Name</th>
       <td><label id="tlname"></label></td>
       </tr>
       
         <tr>
       <th class="">Mobile No</th>
       <td><label  id="tcno"></label></td>
       <th class="">Email Id</th>
       <td><label id="temail"></label></td>
       </tr>
       
         <tr>
       <th class="">Company Name</th>
       <td><label id="tcomp"></label></td>
       <th class="">Register Date</th>
       <td><label id="tregdate"></label></td>
       </tr>
       
        <!--  <tr>
       <th class="">Password</th>
       <td>
      
      <div> <input type="password" style="width: 90%;" class="form-control" id="passwordfield" readonly="readonly" placeholder="password">
             </div>        <div style="margin-left: 93%;margin-top: -10%;">   <span class="glyphicon glyphicon-eye-open"></span></div></td>
       <th class="">Register Date</th>
       <td><label id=""></label></td>
       </tr> -->
       <!-- 
         <tr align="center">
       <td><button type="button" class="btn btn-primary">Follow Up 10</button></td>
       <td><button type="button" class="btn btn-success">Attend Calls 20</button></td>
       <td><button type="button" class="btn btn-info">Assign Lead 5</button></td>
       <td><button type="button" class="btn btn-warning">Miss Calls 7</button></td>
       </tr>  -->
       
       </table>
        
        <%
        Authentication auth0 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser0 = (MediUser) auth2.getPrincipal();
		long id0=currentUser0.getUserid();
		String uname0=currentUser2.getUsername();
		out.println("<input type='hidden' id='uid' value='"+id0+"'>");
		out.println("<input type='hidden' id='cmp' value='"+uname0+"'>");
        
        
        %>
       
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">To View Telecaller Password Enter Your Transaction Password</h4>
      </div>
      <div class="modal-body">
      <form method="get">
       <div class="row">
  <div class="col-xs-6 col-sm-3">Master Password</div>
  <div class="col-xs-6 col-sm-3"><input type="password" style="width:150%;" id="mstpass" class="form-control" ></div>
  
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="checkMasterPassword()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span>View Password</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:35%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Telecaller Password</h4>
      </div>
      <div class="modal-body" >
      <form method="get">  
      <!--  <table border="0" class="table table-bordered"> -->
         <!--  <tr>
       <th class="">Password</th>
       <td>
      
      </td>
       <th class="">Register Date</th>
       <td><label id=""></label></td>
       </tr>  -->
       
       <!-- <tr>
       <td>Password</td>
       <td><div> <input type="password" style="width: 65%;" class="form-control" id="passwordfield" readonly="readonly" placeholder="password">
             </div>        <div style="margin-left: 93%;margin-top: -10%;">   <span class="glyphicon glyphicon-eye-open"></span></div></td>
       
       </tr>
      
       </table> -->
        
     <div class="row">
  <div class="col-xs-6 col-sm-3"><b>Password&nbsp;:&nbsp; </b></div>
  <div>
  <label id="passwordfield"></label>
  
             </div>       
  
        </div>
       
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="myModal45" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width: 56%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Telecaller Password</h4>
      </div>
      <div class="modal-body">
      <form method="get">
       <div class="row">
 
    <div class="col-xs-6 col-sm-3">Old Password</div>
    <div class="col-xs-6 col-sm-3" ><input type="password" id="oldpass" class="form-control" placeholder="Enter Old Password"></div>
  
   <div class="col-xs-6 col-sm-3">New Password</div>
   <div class="col-xs-6 col-sm-3"><input type="password" id="newpass" class="form-control" placeholder="Enter New Password"></div>
 
        </div>
  <div class="row">
 <!--  <div class="col-xs-6 col-sm-3">Id</div> -->
   <div class="col-xs-6 col-sm-3" id="catled"><input type="hidden" id="changepassid" class="form-control" readonly="readonly"></div>
  </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="changeTCPass()" ><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Change Password</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal46" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">To Change Telecaller Password Enter Your Transaction Password</h4>
      </div>
      <div class="modal-body">
      <form method="get">
       <div class="row">
  <div class="col-xs-6 col-sm-3">Master Password</div>
  <div class="col-xs-6 col-sm-3"><input type="password" style="width:160%;" id="mstpassnw" class="form-control" placeholder="Enter Transaction Password"></div>
  </div>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="checkMasterPasswordNew()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span>Change Password</button>
      </div>
    </div>
  </div>
</div>

</html>
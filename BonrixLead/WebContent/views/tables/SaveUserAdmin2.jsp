	<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<a href="./user/createuser.jsp" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary">
			<i class="fa fa-circle-arrow-up fa-lg"></i> 
			Create User
		</a>
		
		<!-- MODAL PLACE HOLDER -->
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>
		
	</div>

</div>


<%@include file="/includes/SessionCheck.jsp" %>
	

	
			
		<!-- MODAL PLACE HOLDER -->
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content" style="width:800px;"></div>
			</div>
		</div>
		
	
<section id="widget-grid" class="">

	
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
				
				<header>
				&nbsp;&nbsp;&nbsp;<b> User Detail</b>

				</header>


				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
							
							 
								<tr role="row">
								<th data-hide="phone" class="sorting_asc" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-sort="ascending" aria-label="ID: activate to sort column ascending" style="width: 27px;">ID</th>
								<th data-class="expand" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" >User Name</th>
								<th class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1" aria-label="Phone: activate to sort column ascending" >Name</th>
								<th data-hide="phone" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1"  >Email</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Mobile Number</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Active</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable_tabletools" rowspan="1" colspan="1">Date</th>
								<th></th>
								
								</tr>
							</thead>
				
							
						</table>

					</div>
				

				</div>
				

			</div>




</section>

<script type="text/javascript">

var mediatable=null;


	pageSetUp();
	
	

	function reloadtable(){
		
		
		  mediatable.ajax.reload();
	}
		
	var pagefunction = function() {
		 
		 
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
		//  alert("sdgsd");
		//  $('#datatable_tabletools').dataTable.ajax.reload();
		reloadtable();
			//$route.reload()
			});
			
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};

	
	    
	    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
	    	   
	  

	};


	
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
		loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
			loadScript("js/plugin/datatables/dataTables.tableTools.min.js", function(){
				loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
					
					loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
			});
			});
		});
	});

	
	function getdata(){

		mediatable=	$('#datatable_tabletools').DataTable({
	        "processing": false,
	        "serverSide": false,
	        "bServerSide": false, 
	        
	        "bdestroy": true,
	        
	      //  "bDestroy": true,
	        "ajax": "getUserList", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	         
	        	if(aData[5]){
	        	$('td:eq(5)', nRow).html('<a class="btn btn-success btn-xs" href="#">Active</a>');
	        	}else
	        		{
	        		$('td:eq(5)', nRow).html('<a class="btn btn-danger btn-xs" href="#">Deactive</a>');	
	        		}
	            //$('td:eq(7)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Detail</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');
	        		 $('td:eq(7)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./user/updateUser.jsp?uid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Edit</a><a href="deleteUser?uid='+aData[0]+'"  class="btn btn-danger btn-xs" onclick="return confirm("Are you sure?");">Delete</a></div>');
	        		 //<a class="btn btn-danger btn-xs" href="deleteUser?uid=${all.uid}" onclick="return confirm('Are you sure?');">Delete</a>
	            
	            
	            
	        },
	 /*        
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>", */
	   /*      "oTableTools": {
	        	 "aButtons": [
	             "copy",
	             "csv",
	             "xls",
	                {
	                    "sExtends": "pdf",
	                    "sTitle": "SmartSMS_PDF",
	                    "sPdfMessage": "SmartSMS PDF Export",
	                    "sPdfSize": "letter"
	                },
	             	{
	                	"sExtends": "print",
	                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
	            	}
	             ],
	            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
	        }, */
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
		
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
		
		
	}

</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

<script src=js/jquery.js></script> 
 <script src=js/jquery.dataTables.js></script> 

<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script> 
 
<style>
.box {
    border-radius: 3px;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    padding: 10px 50px;
    text-align: right;
    display: block;
    margin-top: 60px;
     background-color:white;
}
.box-icon {
    border-radius: 50%;
    display: table;
    height: 100px;
    margin: 0 auto;
    width: 100px;
    margin-top: -61px;
}

.info h4 {
    font-size: 26px;
    letter-spacing: 2px;
    }

</style>

<script>
var table =null;
$(document).ready(function() {
	//var table = $('#tbl-grid').DataTable({ "bSort" : false});
	$.ajax({
			  url: 'getLeadProcessState',
			  type: 'GET',
			  data: null,
			  success: function(data) {
			//	alert(data);  
				  var msg=data;
			  		var html="<option>Process State</option>";
			  		var i=0;
			  		
			  		for(i=0;i<msg.length;i++)
			  			{
			  			console.log('Id='+msg[i][0]);
			  			var val='"'+msg[i][1]+'"';
			  				html+="<option value="+val+">"+msg[i][1]+"</option>";
			  			}
			  		$('#pstate').html("<select class='form-control' id='lpstate' onChange='getLeadData()'>"+html+"</select>");
			  		//console.log("<select class='form-control' id='adcat'>"+html+"</select>");
			  },
			 error: function(e) {
				//called when there is an error
				console.log(e.message);
			  }
			} );

	$.ajax({
			  url: 'GetCategoryForSMS',
			  type: 'GET',
			  data: null,
			  success: function(data) {
			//	alert(data);  
				  var msg=data;
			  		var html="<option value='0'>Category</option>";
			  		var i=0;
			  		
			  		for(i=0;i<msg.length;i++)
			  			{
			  			console.log('Id='+msg[i][0]);
			  			var val='"'+msg[i][0]+'"';
			  				html+="<option value="+val+">"+msg[i][1]+"</option>";
			  			}
			  		$('#cat').html("<select class='form-control' id='catSMS' onChange='getLeadData()'>"+html+"</select>");
			  		//console.log("<select class='form-control' id='adcat'>"+html+"</select>");
			  },
			 error: function(e) {
				//called when there is an error
				console.log(e.message);
			  }
			} );
			
	
	$.ajax({
		  url: 'GetContactInfoDetailSMS',
		  type: 'GET',
		  data: null,
		  success: function(data) {
			//alert(data);  
			  var msg=data;
		  		var html="<option value='Template'>Email Address</option>";
		  		var i=0;
		  		
		  		for(i=0;i<msg.length;i++)
		  			{
		  			console.log('Id='+msg[i][0]);
		  			var val='"'+msg[i][2]+'"';
		  				html+="<option value="+val+">"+msg[i][1]+"-"+msg[i][3]+"</option>";
		  			}
		  		$('#cno').html("<select id='drop1'  class='form-control'>"+html+"</select>");
		  		//console.log("<select class='form-control' id='adcat'>"+html+"</select>");
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );
	
});


function getLeadData()
{
	//tblbdy
	//alert(' onChange=getLeadData()')
		var lstate= $('#lstate').val();

	var pstate= $('#lpstate').val();

	var cat= $('#catSMS').val();
	
	//alert(lstate+" "+pstate+" "+cat);
	var html="";
	  $.ajax({
		  url: 'getLeadDataSMS',
		  //type: 'GET',
		  data: {lstate:lstate,pstate:pstate,cat:cat},
		  success: function(data) {
			//alert(data);  
			for(i=0;i<data.length;i++)
				{
				var st=data[i][4];
			//	alert(st)
				if(st.localeCompare('Open'))
					{
				html+="<tr><td align='center'><input type='checkbox' class='group1' value='"+data[i][3]+"'/></td>";
			/* 	html+="<td>"+data[i][1]+" "+data[i][2]+"</td>"; */
				html+="<td class='hidden-phone'>"+data[i][3]+"</td><td class='hidden-phone'><span class='label label label-danger'>"+data[i][4]+"</span></td>";
				html+="<td class='hidden-phone'>"+data[i][5]+"</td><td class='hidden-phone'>"+data[i][6]+"</td>"+"<td class='hidden-phone'>"+data[i][7]+"</td></tr>";
					}
				else
					{
					html+="<tr><td align='center'><input type='checkbox' class='group1' value='"+data[i][3]+"'/></td>";
					/* html+="<td>"+data[i][1]+" "+data[i][2]+"</td>"; */
					html+="<td class='hidden-phone'>"+data[i][3]+"</td><td class='hidden-phone'><span class='label label label-info'>"+data[i][4]+"</span></td>";
					html+="<td class='hidden-phone'>"+data[i][5]+"</td><td class='hidden-phone'>"+data[i][6]+"</td>"+"</td><td class='hidden-phone'>"+data[i][7]+"</td></tr>";
					}
				}
			
			$('#tblbdy').html(html);
		$('#tbl-grid').DataTable({ "bSort" : false});
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );  
	
	
}
</script>
</head>
<body>
<div class="container" style="background-color: white; width:100%;">
<ul class="nav nav-tabs">
  <li class="active"><a href="#tab_a" data-toggle="tab">Send Email</a></li>
  <li><a href="#tab_b" data-toggle="tab">Contact Email</a></li>
  <li><a href="#tab_c" data-toggle="tab">Lead Email</a></li>
</ul>
<div class="tab-content" style="background-color: white;">
        <div class="tab-pane active fade in" id="tab_a" style="padding-left:200px;">
           <br>
             <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="background-color:#0a97b9;">
            <div class="box">
                <div class="box-icon">
                  <!--   <span class="fa fa-4x fa-html5"></span> -->
               <!--  <img src="img/eml.png" height="60%" width="60%" style="margin-top: 20px;margin-bottom: 20px;;margin-right:30px;margin-left: 23px;"/> -->
               <img src="img/email.png" height="110%" width="110%" style="margin-bottom: 40px;" />
                </div>
                <div class="info" >
                <br>
                    <h4 class="text-center"><font color="#0a97b9">Send Email</font></h4>
                    <br>
                   <div class="row" >
  <div class="col-xs-4 col-sm-4">Email Id</div>
  <div class="col-xs-6 col-sm-8"><input type="text"  class="form-control" id="manualMonNo"></div>
        </div>
        <hr><div class="row" >
  <div class="col-xs-4 col-sm-4">Email Template</div>
  <div class="col-xs-6 col-sm-8"><input type="text"  class="form-control" id="manualMonNo"></div>
        </div>
        <hr>
                  <button type="button" class="btn btn-info">Send</button>
                </div>
            </div>
       <br>
        </div>
          <br>
        </div> 
        <div class="tab-pane fade in " id="tab_b" style="padding-left:200px;">
        <br>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="background-color:#db5565;">
            <div class="box">
                <div class="box-icon">
                    <!-- <span class="fa fa-4x fa-css3"></span> -->
                   <img src="img/cemail.png" height="110%" width="110%" style="margin-bottom: 40px;"/>
                </div>
                <div class="info">
                <br>
                    <h4 class="text-center"><font color="#db5565">Send Contact Email</font></h4>
                    <br>
                   <div class="row" >
  <div class="col-xs-4 col-sm-4">Email Id</div>
  <div class="col-xs-6 col-sm-8" id="cno"></div>
        </div>
        <hr> <div class="row" >
  <div class="col-xs-4 col-sm-4">Email Template</div>
  <div class="col-xs-6 col-sm-8" ><input type="text"  class="form-control" id="manualMonNo"></div>
        </div>
        <hr>
                    <button type="button" class="btn btn-danger">Send</button>
                </div>
            </div>
            <br>
        </div>
        </div>
        <!-- style="padding-left:300px;" -->
        <div class="tab-pane fade in" id="tab_c" style="background-color: white; ">
        <br>
          <!--   <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 row"  > -->
            
              <div class="row"   >
        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="background-color:#6244a5;">
            <div class="box">
                <div class="box-icon">
                   <!--  <span class="fa fa-4x fa-css3"></span> -->
                   <img src="img/lemail.png" height="110%" width="110%" style="margin-bottom: 40px;"/>
                </div>
                <div class="info">
                <br>
                    <h4 class="text-center"><font color="#6244a5">Lead Email</font></h4>
                    <br>
                   <div class="row" >
  <div class="col-xs-5 col-sm-5">Lead State</div>
  <div class="col-xs-5 col-sm-7"><select id="lstate" class="form-control"  onChange="getLeadData()"><option>Lead State</option><option value="Open">Open</option><option value="Close">Close</option></select></div>
        </div>
        <hr>
        
                <div class="row" >
  <div class="col-xs-5 col-sm-5" >Peocess State</div>
  <div class="col-xs-5 col-sm-7" id="pstate"></div>
        </div>
        <hr>
        
                <div class="row" >
  <div class="col-xs-5 col-sm-5">Category</div>
  <div class="col-xs-5 col-sm-7" id="cat"></div>
        </div>
        <hr>
        
        <div class="row" >
  <div class="col-xs-5 col-sm-5">Template</div>
  <div class="col-xs-5 col-sm-7" id="tlist"></div>
        </div>
        <hr>
                  <button type="button" class="btn btn-primary" style="background-color: #6244a5; border-color: #6244a5;">Send</button>
                </div>
            </div>
       <br>
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" >
            <table id="tbl-grid"  class="table table-condensed table-striped table-bordered table-hover no-margin" cellspacing="0" width="100%" >
    <thead>
      <tr>
      <th ><input type='checkbox' name='vehicle' class="my" value='All' onchange="selectALl()"></th>
       <!--  <th >Id</th> -->
        <th >Name</th>
       <!--  <th>Mobile No.</th> -->
        <th> State</th>
        <th>Process State</th>
         <th>Category</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody id="tblbdy">
    
    </tbody>
  </table>
        </div>
	</div>
            
            <br>
           
        </div>
        </div>
    <br>
</div><!-- tab content -->
</div><!-- end of container -->
</body>
</html>
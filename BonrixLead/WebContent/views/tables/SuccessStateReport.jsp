<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- <link rel=stylesheet type=text/css media=screen href="css/dataTables.jqueryui.min.css"/> -->

</head>
<!-- <script src="js/daterangepicker/daterangepicker.js"></script> -->
<link rel="stylesheet" type="text/css"	href="http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style>
table {
	table-layout: fixed;
}

td {
	overflow: hidden;
	text-overflow: ellipsis;
}
</style>
<body>
	<div class="row">
		<div class="col-xs-4 col-sm-2">
			<select class="form-control" id="category"></select>

		</div>

		<div class="col-xs-4 col-sm-2">
			<select id="cat" class="form-control">

			</select>
		</div>
		
		<div class="col-xs-4 col-sm-2">
			<select id="LPState" class="form-control" onchange="setStatId()">

			</select>
		</div>

		<div class="col-xs-4 col-sm-2">
			<select id="callType" class="form-control">

			</select>
		</div>

		
		
		<div class="col-xs-4 col-sm-2" style="margin-left:60px;">
			<button type="button" class="btn btn-default pull-right" id="daterange-btn" >
				<i class="fa fa-calendar"></i> chose Date Range <i class="fa fa-caret-down"></i>
			</button>
			
		</div>

		 <div class="col-xs-4 col-sm-1">
			<button type="button" class="btn btn-danger" onclick="reloadData()">Search</button>
		</div> 
		<br>
		
		<div class="col-xs-4 col-sm-3" style="margin-top:5px; margin-left:855px;">
			<button type="button" class="btn btn-primary" onclick="addAutoDial()" style="float:left;">Add To AutoDial</button>
			<button type="button" class="btn btn-danger" onclick="exportLeads21()" style="margin-left:5px;">Export All</button>
		</div> 

	</div>

	<%
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id = currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='" + id + "'>");
	%>

	<p>
		<br>
	</p>
	<!--  <div id="Mytbl">
 -->
	<div id="" width="100%">
		<table id="example" class="table table-striped table-bordered"
			style="font-weight: normal; " cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Sr No.</th>
					<th>Name</th>
					<th>Mobile No.</th>
					<th>Email</th>
					<th>Remark</th>
					<th>State</th>
					<th>Category</th>
					<th>Calling Time</th>
					<th>Durations(S)</th>
					<th>Telecaller</th>
					<th>Call Status</th>
					<th>Fail Status</th>
					<th>Lead Status</th>
					<th>Audio</th>
					<th>FollowUp</th>
				</tr>
			</thead>
		</table>


	</div>
	
	
	
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width: 70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">FollowUps Detail</h4>
      </div>
      <div class="modal-body" >
		   <table id="hist-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Calling Time</th>
							<th>Remark</th>
							<th>Shedule Time</th>
							<th>New Status</th>
							<th>Call Duration</th>
							<th>Call Status</th>
							<th>Fail Status</th>
							<th>Action</th>
						</tr>
					</thead>


				</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

	
</body>

<script>
var dtable=null;
var test_stdate = new Date();
var test_endate = new Date();

var stdate = test_stdate.getFullYear() + "-" + (test_stdate.getMonth() + 1)
		+ "-" + test_stdate.getDate();
var endate = test_endate.getFullYear() + "-" + (test_endate.getMonth() + 1)
		+ "-" + test_endate.getDate();

var date = new Date();

	var getDate = "NA";
	var dataTable = null;
	var tcId = "3";
	//var callType="";
	var cat = "0";
	var lSate = "NA";
	$(function() {
		$('#datetimepicker1').datetimepicker({
			defaultDate : new Date(),
			format : 'YYYY-MM-DD'
		});
	});
	
	
	$('#daterange-btn').daterangepicker({
		format : "yyyy-mm-dd",
		ranges : {
			'Today' : [ moment(), moment() ],
			'Yesterday' : [ moment().subtract(1, 'days'),
					moment().subtract(1, 'days') ],
			'Last 7 Days' : [ moment().subtract(6, 'days'),
					moment() ],
			'Last 30 Days' : [ moment().subtract(29, 'days'),
					moment() ],
			'This Month' : [ moment().startOf('month'),
					moment().endOf('month') ],
			'Last Month' : [
					moment().subtract(1, 'month').startOf(
							'month'),
					moment().subtract(1, 'month')
							.endOf('month') ]
		},
		startDate : moment().subtract(29, 'days'),
		endDate : moment()
	},
	function(start, end) {
		$('#daterange-btn').text(
				start.format('MMMM D, YYYY') + ' - '
						+ end.format('MMMM D, YYYY'));
	}, getDateRangeSuccessLog);

$('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
stdate = picker.startDate.format('YYYY-MM-DD');
endate = picker.endDate.format('YYYY-MM-DD');
});
function getDateRangeSuccessLog() {

}

function exportLeads21(){
//	alert("stdate::"+stdate+" "+"endate::"+endate)
  //  document.location.href = 'exportAllLeadsCallLog?stdate='+stdate+'&endate='+endate+' ';	
    var teleid = $('#category').val();
    var catId = $('#cat').val();

    var LPState = $('#LPState').val();
    var succState = $('#callType').val();
    if(succState==null){
    	succState="All";
    }
  //  alert("succState::"+succState)
    $.SmartMessageBox({
		title : "Alert!",
		content : "Export Report With Extra Parameters",
		buttons : '[No][Yes]'
	}, function(ButtonPressed) {
		if (ButtonPressed === "Yes") {
			document.location.href = 'exportAllLeadsCallLog?stdate='+stdate+'&endate='+endate+'&extrap=1&teleid='+teleid+'&catId='+catId+'&leadProcessStatus='+LPState+'&successStatus='+succState;
		}
		if (ButtonPressed === "No") {
			document.location.href = 'exportAllLeadsCallLog?stdate='+stdate+'&endate='+endate+'&extrap=0&teleid='+teleid+'&catId='+catId+'&leadProcessStatus='+LPState+'&successStatus='+succState;
		}
	});
}

function showfollowups(ld){
	var leadId=ld;
	console.log("leadId::"+leadId)
	$("#hist-grid").dataTable().fnDestroy();
	   dtable = $('#hist-grid').DataTable({
			"ajax" : {
		    "url"  : "getFollowupsHistory",
			"data" : function(d) {
				d.leadid=leadId;
			  },
			},
			"createdRow" : function(row, data,dataIndex) {
				if(data[3]=='Jun 30, 1980 5:36:00 PM'){
	 	    		  $('td:eq(3)',row).html('NA');
	 	    	   }else{
	 	    		  $('td:eq(3)',row).html(data[3]);
	 	    	   }
			},

			"lengthMenu" : [[10,25,50,100,250,500,1000,2000,5000,-1 ],[10,25,50,100,250,500,1000,2000,5000,"All" ]],
			"sPaginationType" : "full_numbers",
			"oLanguage" : {
				"sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
			},
			"sDom" : 'T<"clear">lfrtip',
			"columnDefs": [{
		        "targets": -1,
		        "data":null,
		        "defaultContent": "<button id='play-btn' class='btn btn-success'>Audio File</button>"
		    }]
		});
	 
 	       $('#hist-grid tbody').on( 'click', '#play-btn', function () {
 			  var  data = dtable.row( $(this).parents('tr') ).data();
 			  console.log("sound::"+'Sound/'+data[8]+'')
 		 	 var win = window.open('Sound/'+data[8]+''); 
 			} );
 	      dtable.on( 'order.dt search.dt', function () {
 	    	 dtable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
 		    	     cell.innerHTML = i+1;	
 		    	 });
 		    	}).draw();
}


	$(document).ready(function() {
						getCat();
						getLeadstate();
						getSuccessStatus();

						$.ajax({
									url : 'getTallyCallerByCompanyId',
									type : 'GET',
									success : function(data) {
										var date = new Date();
										var msg = data;
										var html = "<option value='All'>All Tellycaller</option>";
										var i = 0;

										for (i = 0; i < msg.length; i++) {
											var val = '"' + msg[i][0] + '"';
											html += "<option value="+val+">"+ msg[i][3] + " "+ msg[i][4] + "</option>";
										}
										$('#category').html(html);
										tcId = $('#category').val();
										getDate = $('#datetimepicker1').data('date');
										if (cat.toString == "")
											cat = "0";

										/* dataTable=  $('#example').DataTable( {
										    "ajax": {
										    	"url":"GetInSuccessStatusCallLog",	      
										        "data": function ( d ) {
										         d.getDate=getDate;
										       d.tcId=tcId;
										       d.callType=$("#callType").val();
										       d.cat=$('#cat').val();
										       d.lSate=$('#LPState').val();
										       },
										       
										    },  
										    "createdRow": function( row, data, dataIndex ) {
											   $('td:eq(11)', row).html( '<a type="button" target="_blank" href="http://crmdemo.bonrix.in/Sound/'+$('#uid').val()+"/"+data[11]+'" class="btn  btn-warning"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>' );
										  	          },
										      
										       "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
										     "sPaginationType" : "full_numbers",
											  "oLanguage" : {
											  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
											  },
											  "sDom": 'T<"clear">lfrtip',
											  "oTableTools" : {
											  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
											  "aButtons": [
												  {
													  "sExtends": "pdf",
													  "sTitle" : "OutBound Success Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
													  },
													  {
														  "sExtends": "csv",
														  "sTitle" : "OutBound Success Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
														},
														{
															  "sExtends": "xls",
															  "sTitle" : "OutBound Success Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
														}
												  ],
											  } 
										} ); */

										dataTable.on('order.dt search.dt',function() {
													dataTable.column(0,{
													  search : 'applied',order : 'applied'
													}).nodes().each( function(cell,i) {
													cell.innerHTML = i + 1;
													});
												}).draw();

										$('#example').on('error.dt',function(e, settings,techNote,message) {
															console.log('An error has been reported by DataTables: ',message);
														}).DataTable();
									},
									error : function(e) {
										//called when there is an error
										console.log(e.message);
									}
								});
					});

	function getLeadstate() {
		$.ajax({
			url : 'getLeadProcessState',
			type : 'GET',
			success : function(data) {
				var msg = data;
				var html = "<option value='0'>All Lead State</option>";
				var i = 0;
				for (i = 0; i < msg.length; i++) {
					var val = '"' + msg[i][1] + '"';
					html += "<option value="+msg[i][0]+">" + msg[i][1] + "</option>";
				}
				$('#LPState').html(html);
			},error : function(e) {
				console.log(e.message);
			}
		});
	}
	
	function setStatId(){
		 var LPstate=$("#LPState").val();
		 console.log("Stat:::"+LPstate)
		      if(LPstate==0){
		    	  console.log("Stat:::"+LPstate)
		    	  $("#callType").empty();
		      }else{
		    	  $.ajax({
					  url:'getSuccessStatByleadstat?leadStatId='+LPstate,
					  type: 'GET',
					  dataType:"jsonp",
					  success: function(msg) {
					       var sthtml="<option value='All'>Select Sub State</option>";
						  for(i=0;i<msg.length;i++){
				  		     sthtml+="<option value='"+msg[i].statusName+"'>"+msg[i].statusName+"</option>";
				  		  }
						console.log("sthtml:::"+sthtml)
						alert("hello");
				  		$('#callType').html("<select class='form-control' id='succState'>"+sthtml+"</select>");
					  },error: function(e) {
						console.log(e.message);
					  }
					}); 
		      }
	 }

	function getSuccessStatus() {

		$.ajax({
			url : 'getLeadSuccessStatus',
			type : 'GET',
			success : function(data) {
				var msg = data;
				var html = "<option value='All'>All Success State</option>";
				var i = 0;
				for (i = 0; i < msg.length; i++) {
					var val = '"' + msg[i][1] + '"';
					html += "<option value="+val+">" + msg[i][1] + "</option>";
				}
				$('#callType').html(html);
			},
			error : function(e) {
				console.log(e.message);
			}
		});
	}
	function getCat() {
		$.ajax({
			url : 'GetCategoryById',
			type : 'GET',
			success : function(data) {

				var msg = data;
				var html = "<option value='All'>All Category</option>";
				var i = 0;

				for (i = 0; i < msg.length; i++) {

					var val = '"' + msg[i][0] + '"';
					html += "<option value="+val+">" + msg[i][1] + "</option>";
				}
				$('#cat').html(html);

			},
			error : function(e) {
				//called when there is an error
				console.log(e.message);
			}
		});

	}
	
	function addAutoDial(){
			  
		
	    var catId = $('#cat').val();
	    
	    if(catId=="All"){
	    alert("Please select Category For add to AutoDial");
	    }else{
	    	
	    	alert(catId);
	//    alert("addAutoDial:::"+ prmvalue)
	    $.ajax({
			url : "addLeadsToAutoDialManagerAPI",
			type : "GET",
			data : {
				'catId' : catId
			},
			success : function(response) {
			//	alert(response);
				if(response=='1')
			//	 dataTable.ajax.reload();
			  	 $.smallBox({
						title : "Lead Successfully Added To AutoDial.",
						color : "#296191",
						iconSmall : "fa fa-thumbs-up bounce animated",
						timeout : 4000
						});
			},
			error : function(xhr, status, error) {
				alert("get error");
				alert(xhr.responseText);
			}
		});
	}
	}
	
	function reloadData() {
		getDate = $('#datetimepicker1').data('date');
		tcId = $('#category').val();
		callType = $("#callType").val();
		cat = $('#cat').val();
		lSate = $('#LPState').val();
	//	dataTable.ajax.reload();
		tabledata();
	}

	function tabledata() {
		$("#example").dataTable().fnDestroy();
		var date = new Date();
	//	alert("stdate::"+stdate+" "+"endate::"+endate)
	//	alert("callType::"+$("#callType").val()+" "+"LpState::"+$('#LPState').val()+" "+"cat::"+$('#cat').val())
		
		dataTable = $('#example').removeAttr('width').DataTable({
		//  "scrollY":"500px",
		    "scrollX": true,
	        "scrollCollapse": true,
	     // "paging": false,
	         "columnDefs": [
	        	{ "width": 50, "targets": 0 },
	        	{ "width": 100, "targets": 1 },
	            { "width": 80, "targets": 2 },
	            { "width": 130, "targets": 3 },
	            { "width": 120, "targets": 4 },
	            { "width": 80, "targets": 5 },
	            { "width": 100, "targets": 6 },
	            { "width": 100, "targets": 7 },
	            { "width": 80, "targets": 8 },
	            { "width": 100, "targets": 9 },
	            { "width": 60, "targets": 10 },
	            { "width": 120, "targets": 11 },
	            { "width": 50, "targets": 12 },
	            { "width": 50, "targets": 13 },
	            { "width": 100, "targets": 14 }
	        ], 
	        fixedColumns: true,
							    "ajax" : {
								"url" : "GetInSuccessStatusCallLog",
								"data" : function(d) {
									d.getDate = getDate;
									d.endate = endate;
									d.stdate = stdate;
									d.tcId = tcId;
									d.callType = $("#callType").val();
									d.cat = $('#cat').val();
									d.lSate = $('#LPState').val();
								},
							},
							"createdRow" : function(row, data, dataIndex) {
								$('td:eq(13)', row).html('<a type="button" target="_blank" href="http://crmdemo.bonrix.in/Sound/'
														+ $('#uid').val()
														+ "/"
														+ data[13]
														+ '" class="btn  btn-warning"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>');
								$('td:eq(14)', row).html('<button type="button" class="btn btn-primary" id="follow" data-toggle="modal" data-target="#myModal4" onclick="showfollowups('+data[15]+')">Follow Up </button>');
							},

							"lengthMenu" : [
									[ 10, 25, 50, 100, 250, 500, 1000, 2000,
											5000, -1 ],
									[ 10, 25, 50, 100, 250, 500, 1000, 2000,
											5000, "All" ] ],
							"sPaginationType" : "full_numbers",
							"oLanguage" : {
								"sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
							},
							"sDom" : 'T<"clear">lfrtip',
							"oTableTools" : {
								"sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
								"aButtons" : [
										{
											"sExtends" : "pdf",
											"sTitle" : "OutBound Success Call Log  Report"
													+ " On "
													+ date.getDate()
													+ "-"
													+ (date.getMonth() + 1)
													+ "-" + date.getFullYear()
										},
										{
											"sExtends" : "csv",
											"sTitle" : "OutBound Success Call Log  Report"
													+ " On "
													+ date.getDate()
													+ "-"
													+ (date.getMonth() + 1)
													+ "-" + date.getFullYear()
										},
										{
											"sExtends" : "xls",
											"sTitle" : "OutBound Success Call Log  Report"
													+ " On "
													+ date.getDate()
													+ "-"
													+ (date.getMonth() + 1)
													+ "-" + date.getFullYear()
										} ],
							}
						});
		dataTable.on('order.dt search.dt',
				function() {
					dataTable.column(0,{search : 'applied',order : 'applied'}).nodes().each(
										function(cell,i) {
											cell.innerHTML = i + 1;
										});
					}).draw();

	}
</script>
</html>
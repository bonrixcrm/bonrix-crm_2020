<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.bonrix.sms.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- <script src=js/jquery.js></script> -->
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>

<style>
/* Background Gradient for Monochromatic Colors */
.gradient4
{
    background-color: #FF7373;
    /* For WebKit (Safari, Chrome, etc) */
    background: #FF7373 -webkit-gradient(linear, left top, left bottom, from(#D90000), to(#FF7373)) no-repeat;
    /* Mozilla,Firefox/Gecko */
    background: #FF7373 -moz-linear-gradient(top, #D90000, #FF7373) no-repeat;
    /* IE 5.5 - 7 */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#D90000, endColorstr=#FF7373) no-repeat;
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#D90000, endColorstr=#D90000)" no-repeat;
}

</style>
<script>
var dataTable=null;
var finalCatId=null;


$(document).ready(function() {
	
	
	         dataTable = $('#cat-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetTagForCompany",
	            //"ajax":{
	           //     url :"GetAssignCategory", // json datasource
            //    type: "get",  // method  , by default get
	                "columnDefs": [ {
	                    "targets": -1,
	                    "data": null,
	                    "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"
	                }],

	        } );
	  

$('#cat-grid tbody').on( 'click', '#edit-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
   // alert("'Edit is: "+ data[ 0 ] );
    $('#catid').val(data[0]);
    $('#catval').val(data[1]);
} );

$('#cat-grid tbody').on( 'click', '#delete-btn', function () {
	   var data = dataTable.row( $(this).parents('tr') ).data();
	   $.ajax({
		  	  url: "deleteTagToCompany",
		  	  type: "get",
		  	  data:"tagId="+data[ 0 ],
		  	  success: function(html){
		  		 dataTable.ajax.reload();
		 		 $.smallBox({
		  				title : "Tag Deleted Successfully.",
		  				color : "#296191",
		  				iconSmall : "fa fa-thumbs-up bounce animated",
		  				timeout : 4000
		  				});
		  	  }
		});
	 
	} );
} );
 dataTable.on( 'order.dt search.dt', function () {
	 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();  
 
 

function saveChangeCategory()
{
	
var tagId=$('#catid').val();
var tagName=$('#catval').val();
var uid=$('#uid').val();

if(tagName=="")
{
document.getElementById('catval').style.borderColor='red';
}
else
{
	document.getElementById('catval').style.borderColor='';
	
$.ajax({
	  url: 'updateTagToCompany',
	  type: 'GET',
	  data: {tagName :tagName,tagId:tagId},
	  success: function(data) {
		  $('#myModal').modal('hide');
		 
	 		  dataTable.ajax.reload();
	 		 $.smallBox({
	  				title : "Tag Updated Successfully.",
	  				color : "#296191",
	  				iconSmall : "fa fa-thumbs-up bounce animated",
	  				timeout : 4000
	  				});
	  },
	  error: function(e) {
		//called when there is an error
		console.log(e.message);
	  }
	});
}
}

function saveCategory()
{
	

	var catname=$('#catNameAdd').val();
	var uid=$('#uid').val();
	if(catname=="")
		{
	document.getElementById('catNameAdd').style.borderColor='red';
		}
	else
		{
		document.getElementById('catNameAdd').style.borderColor='';

	$.ajax({
		  url: 'AddTag',
		  type: 'GET',
		  data: {tagName:catname},
		  success: function(data) {
			  
			  $('#myModal1').modal('hide');
			  $("#catNameAdd").val("")
			  dataTable.ajax.reload();
			  $.smallBox({
	  				title : "Tag Added Deleted.",
	  				color : "#296191",
	  				iconSmall : "fa fa-thumbs-up bounce animated",
	  				timeout : 4000
	  				});
		  },
		  error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		});
		}
}

function closepopup()
{
	 
}
</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Tag Management</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Tag</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Name</th>
							<th>Action</th>
							
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modify Tag</h4>  
      </div>
      <div class="modal-body">
      <form method="get">
      
                  <input type="hidden" id="catid" class="form-control" disabled="disabled">
         
        <table>
        <tr>
        <td>Tag Name &nbsp;</td>
        <td> <input type="text" id="catval" class="form-control"></td>
        </tr>
        <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        
        
        %>
        
        </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeCategory();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
      </div>
      <form method="get" class="catForm" id="frmdemo"> 
      <div class="modal-body">
      
      
         
        <table>
        <tr>
        <td>Category Name&nbsp;<b style="color:red;">*</b>&nbsp;</td>
        <td> <input type="text" id="catNameAdd"  class="form-control" name="catNameAdd"></td>
        </tr>
        <%
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser1 = (MediUser) auth.getPrincipal();
		long id1=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id1+"'>");
        %>
        
        </table>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveCategory();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Category</button>
      </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are You Sure You want to Delete Category?</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
       <div class="row">
  <div class="col-xs-12 col-sm-12">
  <b>If You Delete Category It Will Delete Following Data.</b><br>
  <b class="text-danger">1. All Leads are Deleted Permanently.</b><br>
  <b class="text-danger">2. All Follow Ups of Leads are Deleted Permanently.</b><br>
  <b class="text-danger">3. Audio Files of Leads are also Deleted Permanently.</b><br><br>
  <b class="text-warning">If  <kbd>Yes</kbd>  Please Enter Transaction Password otherwise Click on  <kbd>No</kbd>  Button.</b>
  </div>
<!--   <div class="col-xs-6 col-sm-3"><input type="password" style="width:150%;" id="mstpass" class="form-control" ></div>
 -->        </div>
        
       <div class="row">
       <p><br></p>
  <div class="col-xs-6 col-sm-4">Transaction Password</div>
  <div class="col-xs-6 col-sm-3"><input type="password" style="width:150%;" id="mstpass" class="form-control" ></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-primary" onclick="deleteLeadData()">Yes</button>
      </div>
    </div>
  </div>
</div>
</html>

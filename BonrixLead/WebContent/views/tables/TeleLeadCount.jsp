<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
    <%@page import="org.springframework.security.core.Authentication"%>
    <%@page import="com.bonrix.sms.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %>
        
<div id="tableDiv"></div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">All Lead</h4>
      </div>
      <div class="modal-body">
      	
      	 <div class="widget-body no-padding">
				<table id="cat-grid1" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Mobile No</th>
							<th>Category</th>
							<th>Process State</th>
							<th>Created Date</th>
							<th>Schedule Date</th>
							<th>Tallycaller</th>
							<th>Action</th>
							
						</tr>
					</thead>
				</table>
		 </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <!--   <button type="button" class="btn btn-primary" onclick="saveLead();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Lead</button> -->
      </div>
    </div>
  </div>
</div>

<section id="widget-grid" class="">

<div class="row">
	<article class="col-xs-12 col-sm-4 col-md-5 col-lg-12">
			
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-6" data-widget-editbutton="false">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
					<h2>Pie Chart</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<div id="pie-chart" class="chart"></div>
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->

		</article>
</div>

</section>



<div class="modal fade" id="followupModal" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">All FollowUps</h4>
      </div>
      <div class="modal-body">
      	
      	 <div class="widget-body no-padding">
				<table id="followup-grid" name="followup"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Telecaller Name</th>
							<th>Calling Time</th>
							<th>Remark</th>
							<th>Schedule Time</th>
							<th>Process State</th>
							<th>Call Duration</th>
							<th>Call State</th>
							<th>FollowUp Type</th>
							<th>Fail State</th>
							<th>Success State</th>
						<!-- <th>FollowUp Device</th>
							<th>Responce</th> -->
						</tr>
					</thead>
				</table>
		 </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <!--   <button type="button" class="btn btn-primary" onclick="saveLead();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Lead</button> -->
      </div>
    </div>
  </div>
</div>
			
			
</body>
<script>
var count=0;
var data_pie = [];
var value_pie = [];
var total=0;
$(document).ready(function() {
	$.ajax({
		url : 'GetTelecallerSummaryByLeadState',
		type : 'GET',
		success : function(data) {
	//	alert("data::"+JSON.stringify(data))
		 var tableHeaders='';
         $.each(data.columns, function(i, val){
             tableHeaders += "<th>" + val + "</th>";
         });
         var table = $('#displayTable').DataTable();
         $("#tableDiv").empty();
         $("#tableDiv").append('<table id="displayTable" style="background-color:white;" class="table table-bordered" border=1 width="100%"><thead><tr>' + tableHeaders + '</tr></thead></table>');
         $('#displayTable').dataTable({	
         	 "data":data.data,
         	 "scrollX": true,
            });
		},
		"dataType": "json",
		error : function(e) {
			console.log(e.message);
		}
	});
	
	
	$.ajax({
		url : 'GetLeadChart',
		type : 'GET',
		success : function(data) {
	   // 	alert("data::"+JSON.stringify(data))
	    	var msg = eval(data);
	    	var msg2 = JSON.stringify(data);
	    	var msg3=null;
	    	for (var i = 0; i < msg.length; i++) {
	    		msg3=msg[i];
	    	}
	    	for(var key in msg3) {
	    		//console.log("Key: " + key +"---"+" value: " + msg3[key]);
	    		data_pie.push(key);
	    		value_pie.push(msg3[key]);
	    		total=total+ msg3[key];
	    		count++;
	    	}
	    	console.log("count:::"+count+"-----"+"total::::"+total);
		},
		error : function(e) {
			console.log(e.message);
		}
	});
	
});

pageSetUp();
var pagefunction = function() {
	console.log("page_count:::"+count+"  "+"data_pie:::"+data_pie+"--------"+"value_pie:::"+value_pie);
	
	if ($('#pie-chart').length) {
		var series =count;
		for (var i = 0; i < series; i++) {
			data_pie[i] = {
				label :  data_pie[i],
				data : value_pie[i]
			}
		}
		
		$.plot($("#pie-chart"), data_pie, {
			series : {
				pie : {
					show : true,
					innerRadius : 0.5,
					radius : 1,
					label : {
						show : false,
						radius : 2 / 3,
						formatter : function(label, series) {
							return '<div style="font-size:11px;text-align:center;padding:4px;color:white;">' + label + '<br/>' + series.percent + '%</div>';
						},
						threshold : 0.1
					}
				}
			},
			tooltip: {
				//pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> <br>of which woman users {point.custom}'
			},
			legend : {
				show : true,
				noColumns : 1, // number of colums in legend table
				labelFormatter : null, // fn: string -> string
				labelBoxBorderColor : "#000", // border color for the little label boxes
				container : null, // container (as jQuery object) to put legend in, null means default on top of graph
				position : "ne", // position of default legend container within plot
				margin : [5, 10], // distance from grid edge to default legend container within plot
				backgroundColor : "#efefef", // null means auto-detect
				backgroundOpacity : 1 // set to 0 to avoid background
			},
			grid : {
				hoverable : true,
				clickable : true
			},
		});
}
	
};


function devicehistory(telecallerId,leadState){
//	alert("telecallerId:::"+telecallerId+" "+"leadState::"+leadState)
	
	$("#myModal1").modal();
   $("#cat-grid1").dataTable().fnDestroy();
	dataTable = $('#cat-grid1').DataTable( {
        "processing": true,
        "serverSide": false,
        "bPaginate": true,
        "ajax" : {
			"url" : "GetLeadByLeadState",
			"data" : function(d) {
				d.telecallerId = telecallerId;
				d.leadState = leadState;
			},
		},
	    "order": [[ 0, 'desc' ]],
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        	
		//console.log(aData[7])
		var dd=aData[7];
		if(dd=="Jun 30, 1980 5:36:00 PM")
			 $('td:eq(7)', nRow).html( "<b><font color='red'>Not Set</font></b>" ); 
		
		
		$('td:eq(9)', nRow).html('<a class="btn btn-primary btn-xs" href="#" onclick=viewFollowups("'+aData[0]+'")>View FollowUps</a>');
        }
    } );
}

function viewFollowups(leadID){
	//alert(leadID)
	
	$("#followupModal").modal();
	   $("#followup-grid").dataTable().fnDestroy();
		dataTable = $('#followup-grid').DataTable( {
	        "processing": true,
	        "serverSide": false,
	        "bPaginate": true,
	        "ajax" : {
				"url" : "getFollowupsByLead",
				"data" : function(d) {
					d.leadId = leadID;
				},
			},
		    "order": [[ 0, 'desc' ]],
	        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
				var dd=aData[4];
	    		if(dd=="Jun 30, 1980 5:36:00 PM")
	    			 $('td:eq(4)', nRow).html( "<b><font color='red'>Not Set</font></b>" );
	        }
	    } );
	
}

function noDataAlert(){
	$.smallBox({
		title : "No Lead Found.",
		color : "#296191",
		iconSmall : "fa fa-thumbs-up bounce animated",
		timeout : 4000
		});
}



loadScript("js/plugin/flot/jquery.flot.cust.min.js", function(){
	loadScript("js/plugin/flot/jquery.flot.resize.min.js", function(){
		loadScript("js/plugin/flot/jquery.flot.fillbetween.min.js", function(){
			loadScript("js/plugin/flot/jquery.flot.orderBar.min.js", function(){
				loadScript("js/plugin/flot/jquery.flot.pie.min.js", function(){
					loadScript("js/plugin/flot/jquery.flot.tooltip.min.js", pagefunction);
				});
			});
		});
	});
});

</script>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TellyCaller Last Access</title>
<script>
var dataTable=null;
$(document).ready(function() {
	         dataTable = $('#cat-grid').DataTable({
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetTellyCallerLastAccess",
	            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$('td:eq(4)', nRow).html( "<b><font color='red'>"+aData[4]+"</font></b>" );
		            /*         if(aData[5]==true)
		                    $('td:eq(5)', nRow).html("<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm ' id='changepass-btn' data-toggle='modal' data-target='#myModal46'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  ChangePassword  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-primary' id='show-btn' data-toggle='modal' data-target='#myModal3'><span class='glyphicon glyphicon-fullscreen' aria-hidden='true'></span>  Show  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-warning' id='show-btn' data-toggle='modal' data-target='#myModal4'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>  Password  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-info' id='edit-btn' onclick='setADctive("+aData[5]+","+aData[0]+")'><span class='glyphicon glyphicon-user' aria-hidden='true'></span>  DeActive  </button>");
		                    else 
		                    	 $('td:eq(5)', nRow).html("<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm' id='changepass-btn' data-toggle='modal' data-target='#myModal46'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  ChangePassword  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-primary' id='show-btn' data-toggle='modal' data-target='#myModal3'><span class='glyphicon glyphicon-fullscreen' aria-hidden='true'></span>  Show  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-warning' id='show-btn' data-toggle='modal' data-target='#myModal4'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>  Password  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-info' id='edit-btn' onclick='setADctive("+aData[5]+","+aData[0]+")'><span class='glyphicon glyphicon-user' aria-hidden='true'></span>  Active  </button>");
			         */
	                }, 
	        });
	         
	         dataTable.on( 'order.dt search.dt', function () {
	        	 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	                cell.innerHTML = i+1;
	            } );
	        }).draw();
	         
	         setInterval( function () {
	        	 dataTable.ajax.reload();
	        	}, 60000 );
	         
});       

</script>
</head>

<%@include file="/includes/SessionCheck.jsp"%>

<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser" data-widget-editbutton="false">
		<header>
				<span class="widget-icon"> <i class="fa fa-table"></i></span>
				<h2>Telecaller LastAccess</h2>
		</header>
		<div>
			<div class="jarviswidget-editbox"></div>
			<div class="widget-body no-padding">
				<table  id="cat-grid"class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>User Name</th>
							<th>TelecallerName</th>
							<th>Mobile Number</th>
							<th>LastAccess Date</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</section>

</html>
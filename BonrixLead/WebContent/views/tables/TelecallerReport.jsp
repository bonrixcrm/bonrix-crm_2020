<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <!--<script src="http://cdn.anychart.com/js/7.10.1/anychart-bundle.min.js"></script>
 <script src="js/bootstrap-datetimepicker.js"></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
        <link rel="stylesheet" href="http://cdn.anychart.com/css/latest/anychart-ui.min.css" />-->

</head>
<body>
 <div id="todattime" class="tab-pane fade in active">
            <h3>Today Call Report</h3>
           
        </div>



</body>
<!--
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script> 
-->

        
<script type="text/javascript">
var successRate=null;
var failRate=null;
var successI=null;
var FailI=null;
var successArray="[";
var failArray="[";
var telecallerArray="[";

var successArray1="";
var failArray1="";
var telecallerArray1="";

var getDate="NA";
var dataTable=null;
var tcId="3";
$(document).ready(function() {
	//getAllTimeCallLog();
	getTodayTimeCallLog();
	//generateSuccessFailChart();
	
});
$(function () {
    $('#datetimepicker1').datetimepicker({
   	  defaultDate: new Date(), 
   	 
     format: 'YYYY-MM-DD'
        /* disabledDates: [
            moment("2016-12-30"),
            new Date(2013, 11 - 1, 21),
            "01/01/2016 00:53"
        ] */
    });
});



function getTodayTimeCallLog()
{
	
	$.ajax({
		  url: 'getTodayTimeCallLog',
		  type: 'GET',
		  success: function(Mydata) {
			  var html="";
			  var i=0;
			  var status="";
			  var calltype="";
			  var audioPath="";
			  var data=eval(Mydata);
			 
				 
				
			  html+=" <h3>Today Call Log</h3>";
			//  html+="<p><br></p>";
			  	html+="<table id='cat-grid1' name='demo' class='table table-striped table-bordered' >";
			  html+="<thead>";
					html+="<tr>";
						html+="<th>Telecaller Name</th>";
							html+="<th>Total Call</th>";
							html+="<th>Success Calll</th>";
							html+="<th>Fail Call</th>";
							html+="<th>Total Call Duration</th>";
							html+="<th>AVG Call Duration(Success)</th>";
						/* 	html+="<th>Success Statistic</th>	";
							html+="<th>Fail Statistic</th>	"; */
							html+="</tr>";
						html+="</thead>";
					html+="<tbody id='folloBody'>";
					
			  for(i=0;i<data.length;i++)
				  {
				
				  
				  //telecallerArray[i]=data[i][0];
				//  successArray[i]=data[i][2];
				//  failArray[i]=data[i][1];
				  
				  var result="";
				  var avg="";
				  var totalSeconds=data[i][4];
				  var hours   = Math.floor(totalSeconds / 3600);
				  var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
				  var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

				  // round seconds
				  seconds = Math.round(seconds * 100) / 100

				   result += (hours < 10 ? "0" + hours : hours);
				      result += ":" + (minutes < 10 ? "0" + minutes : minutes);
				      result += ":" + (seconds  < 10 ? "0" + seconds : seconds);
				      var calSum=Number(data[i][1])+Number(data[i][2]);
				     // console.log(calSum)
				     // avg=(Number(totalSeconds))/(Number(calSum));
				     avg=Number((totalSeconds)/Number(data[i][2]))
				  html+="<tr>";
				  html+="<td>"+data[i][0]+"</td>";
				  html+="<td>"+data[i][3]+"</td>";
				  html+="<td>"+data[i][2]+"</td>";
				  html+="<td>"+data[i][1]+"</td>"; 
				  html+="<td>"+result+"</td>";
				  html+="<td>"+avg+"</td>";
				/*   html+="<td><button type='button' data-toggle='modal' data-target='#successStatistic' class='btn btn-success' onclick='getStatistic(\"Success\","+data[i][5]+")' style='width:90px;'>Success</button></td>";
				  html+="<td><button type='button' data-toggle='modal' data-target='#successStatistic' onclick='getStatistic(\"Fail\","+data[i][5]+")' class='btn btn-danger' style='width:90px;'>Fail</span></button></td>"; */
				  html+="</tr>";
				  }
			  html+="</tbody></table>";
			  $("#todattime").html(html);
			
			
			  $('#cat-grid1').DataTable(  
					  {
					                  
					                
					  "sPaginationType" : "full_numbers",
					  "oLanguage" : {
					  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
					  },
					  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
					   "sDom": 'T<"clear">lfrtip',
					  "oTableTools" : {
					  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
					  }
					  });    
					  
			  
			 
		  },
		  error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		});	
}

 function getStatistic(type,id)
{
	 $.ajax({
		  url: 'getStatistic',
		  type: 'GET',
		  data:"type="+type+"&&id="+id,
		  success: function(Mydata) {
			  var html="";
			  html+="<table class='table table-bordered'>";
			  html+="<thead>";
			  html+="<tr>";
				html+="<th>Lead Process State</th>";
				html+="<th>Count</th>";
				html+="</tr>";	
			  html+="</thead>";
			  html+="<tbody>";
			  for(i=0;i<Mydata.length;i++)
				  {
			  html+="<tr>";
			  html+="<td>"+Mydata[i][0]+"</td>";
			  html+="<td>"+Mydata[i][1]+"</td>";
			  html+="</tr>";
				  }  
			  html+="</tbody>";
			  html+="</table>";
			  $("#successStatisticHeader").html(type +" Call Statistic")
			  $("#successStatisticBody").html(html);
		  },
		  error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		});	
} 



</script>
</html>
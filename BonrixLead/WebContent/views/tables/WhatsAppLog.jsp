<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WhatsApp Logs</title>
 <!--<script src="http://cdn.anychart.com/js/7.10.1/anychart-bundle.min.js"></script>
 <script src="js/bootstrap-datetimepicker.js"></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
        <link rel="stylesheet" href="http://cdn.anychart.com/css/latest/anychart-ui.min.css" />-->

</head>
<body>
 <div id="datewise">
            <h3>WhatsApp Logs</h3> 
            <div class="row">
  <div class="col-xs-4 col-sm-2">
     <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  </div>
  <div class="col-xs-4 col-sm-2">
        <button type="button" class="btn btn-danger" onclick="reloadData()">Search</button>
  </div>
  <div class="col-xs-4 col-sm-2">

  
  </div>
  
  </div>
  <br>
              <table id="example" class="table table-striped table-bordered" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Mobile No.</th>
                 <th>Template</th> 
                <th>Status</th>
                <th>Send Time</th>   
                <th>Telecaller</th>     
                            
            </tr>
            </thead>   
    </table> 
               </div>  
</body>


        
<script type="text/javascript">
var dataTable=null; 

$( document ).ready(function() {
	  $('#datetimepicker1').datetimepicker({
	   	  defaultDate: new Date(), 
	   	 format: 'YYYY-MM-DD'
	    });
	  dataTable= $('#example').DataTable( {
	        "bPaginate": true,
	        "ajax":{
	            "url": "getWhatsAppLogs",
	           " type": "get",
	           "data": function ( d ) {
	        	  d.date=$('#datetimepicker1').data('date');
	           }
	        }//, 
	   } );
});

function reloadData()
{
	dataTable.ajax.reload();
}





</script>
</html>
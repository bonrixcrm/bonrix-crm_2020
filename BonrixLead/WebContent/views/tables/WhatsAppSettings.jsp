<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WhatsApp Settings</title>
</head>

<style>
.truncate {
  max-width:50px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
<script src="http://crmdemo.bonrix.in/js/table2excel.js"></script>
<script>
var dataTable=null;
var fileName="WhatsApp Settings";
$(document).ready(function() {
	          dataTable = $('#API-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetWhatsAppSettingsByUserId",
	            columnDefs: [ 
	                {
	                    data: null,
	                    defaultContent: '<button class="btn btn-sm btn-warning" id="deactive-btn">Delete</button>',
	                    targets: -1
	                },
	                {targets:[2],className:"truncate"}
	            ]
	        } ); 
	          
	          $('#API-grid tbody').on( 'click', '#deactive-btn', function () {
	        		
	        	   var data = dataTable.row( $(this).parents('tr') ).data();
	        	 if (confirm('Are you sure ?')) {
	        		 $.ajax({
	   	       	  	  url: "deleteWhatsAppSettings",
	   	       	  	  type: "get",
	   	       	  	 data:{
	   	       	  	id:data[0]
	   	       	  		 }, 
	   	       	  	  success: function(){
	   	       		    $('#myModaladd').modal('hide');
	   	       		    dataTable.ajax.reload();
	   	        	  	 $.smallBox({
	   	        				title : "Setting Successfully deleted.",
	   	        				color : "#296191",
	   	        				iconSmall : "fa fa-thumbs-up bounce animated",
	   	        				timeout : 4000
	   	        				});

	   	       	  	  }
	   	       	  	});
	        	    }
	        	} ); 
	          
	          $(".exportToExcel").click(function(e){

					var table = $('#API-grid');
					//alert(table.length);
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename:fileName + ".xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							//columns : [0,1,2,3,4,5],
							preserveColors: preserveColors
							
						});
					}
				});
} ); 

function saveSetting()
{
	 $.ajax({
	  	  url: "saveWhatsAppSettings",
	  	  type: "get",
	  	 data:{
	  		whatsapp_number:$('#whatsapp_number').val(),
	  		token:$('#token').val(),
	  		phone_number_id:$('#phone_number_id').val(),
	  		whatsapp_business_account_id:$('#whatsapp_business_account_id').val(),
	  		media_url:'NA'
	  		 }, 
	  	  success: function(){
		    $('#myModaladd').modal('hide');
		    dataTable.ajax.reload();
 	  	 $.smallBox({
 				title : "Settings Successfully Saved.",
 				color : "#296191",
 				iconSmall : "fa fa-thumbs-up bounce animated",
 				timeout : 4000
 				});

	  	  }
	  	});
}
</script>
<body>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>WhatsApp API Manager</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModaladd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>New WhatsApp Setting</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">
 <button class="exportToExcel">Export to XLS</button>
				<table id="API-grid" class="display" style="width:100%">
<thead>
<tr>
<th>Id</th>
<th>WhatsApp Number</th>
 <th>Token</th> 
 <th>Phone Number Id</th> 
 <th>Business Account Id</th>
 <th>Media Url</th>
 <th>Action</th>
</tr>
</thead>
</table>
			</div>
		</div>
	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modify API Data</h4>
      </div>
      <div class="modal-body">
      <form method="get" id="updateform">
      
         
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>API Id</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="id" readonly="readonly" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Success Key</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="key" class="form-control"></div>
        </div>
         
         <hr>
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Provider</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="provider" class="form-control"></div>
        </div>
        <hr>
        
        
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>API</b></div>
   <div class="col-xs-6 col-sm-6"><textarea rows="10" cols="40" id="desc"></textarea></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="updateAPI()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>





<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New WhatsApp Setting</h4>
      </div>
      <div class="modal-body">
      <form method="post" id="addform">
      
         
         
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>WhatsApp Number</b></div>
  <div class="col-xs-6 col-sm-6"><input type="number" id="whatsapp_number" class="form-control"></div>
        </div>
        <hr>
        
        <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Token</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="token" class="form-control"></div>
        </div>
        <hr>
        
           <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Phone Number Id</b></div>
  <div class="col-xs-6 col-sm-6"><input type="number" id="phone_number_id" class="form-control"></div>
        </div>
        <hr>
        
           <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Business Account Id</b></div>
  <div class="col-xs-6 col-sm-6"><input type="number" id="whatsapp_business_account_id" class="form-control"></div>
        </div>
        <hr>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveSetting()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span>&nbsp;Save Setting</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
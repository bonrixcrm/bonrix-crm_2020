<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WhatsApp Settings</title>
</head>

<style>
.truncate {
  max-width:400px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
<script src="http://crmdemo.bonrix.in/js/table2excel.js"></script>
<script>
var dataTable=null;
var fileName="WhatsApp Template ";
$(document).ready(function() {
	
	let fileCount = 0;

    $('#addFileButton').click(function() {
        fileCount++;

        const fileInput = $('<input type="file" name="files[]">');
        const textBox = $('<input type="text" name="descriptions[]" placeholder="Enter description"><br>');
        const fileInputWrapper = $('<div></div>');

        fileInputWrapper.append(fileInput);
        fileInputWrapper.append(textBox);
        fileInputWrapper.append('<br>'); // Optional: Add a line break for better visual separation

        $('#fileInputsContainer').append(fileInputWrapper);
    });

    $('#mediaUploadFormWhatsapp').submit(function(event) {
        event.preventDefault(); // Prevent the default form submission

        const formData = new FormData(this);
        formData.append("templateId", $("#templateIdWhatsapp").val());
        
        // You can inspect the formData entries for debugging
       // for (let [key, value] of formData.entries()) {
        //    console.log(key, value);
        //}

        // Send the form data using AJAX
        $.ajax({
            url: 'upload', // Replace with your server upload URL
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {
                console.log('Upload successful:', response);
                alert('Upload successful!');
                // Clear the form data
                $('#mediaUploadForm')[0].reset();
                
                // Optionally, clear the dynamically added inputs
                $('#fileInputsContainer').empty();
            },
            error: function(error) {
                console.log('Upload error:', error);
                alert('Upload failed!');
            }
        });
    });
	 
	          dataTable = $('#API-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetWhatsAppTemplateByUserId",
	            columnDefs: [
	            	 {
		                    data: null,
		                    defaultContent: '<button class="btn btn-sm btn-warning" id="deactive-btn">Delete</button><button id="update-btn" id="upload-button" class="btn btn-success">Update</button>',
		                    targets: -1
		                },
	                 {targets:[2],className:"truncate"}
	            ],
	            "columns": [
	                { "data": 0 },
	                { "data": 1 },
	                { "data": 6 },
	                { "data": 2 },
	                { "data": 3 },
					 { "data": 5},
	                {
	                    "data": null,
	                    "render": function ( data, type, row, meta ) { 
	                    	if(row[5]=='IMAGE' || row[5]=='VIDEO' || row[5]=='DOCUMENT')
	                    		return '<button  onclick="setId('+"\'"+data[0]+"\'"+')" id="play-btn" id="upload-button" data-toggle="modal" data-target="#myModal1" class="btn btn-success">Upload Media</button><button  onclick="setId('+"\'"+data[0]+"\'"+')" id="play-btn" id="upload-button" data-toggle="modal" data-target="#myModal12" class="btn btn-success">Sub Template</button>';
	                    		else
	                    			return '';
	                    }
	                },
	                { "data": null },
	            ]
	        } ); 
	          
	          $('#API-grid tbody').on( 'click', '#update-btn', function () {
	        		
	        	   var data = dataTable.row( $(this).parents('tr') ).data();
	        	   $.ajax({
	       			url : 'UpdateWhatsAppTemplate?templateName='+data[1],
	       			type : 'GET',
	       			success : function(data) {
	       				alert("Success");
	       				dataTable.ajax.reload();
	       			},error : function(e) {
	       				alert("Success");
	       				console.log(e.message);
	       			}
	       		});
	        	} ); 
	          
	          $('#API-grid tbody').on( 'click', '#deactive-btn', function () {
	        		
	        	   var data = dataTable.row( $(this).parents('tr') ).data();
	        	 if (confirm('Are you sure ?')) {
	        		 $.ajax({
	   	       	  	  url: "deleteWhatsAppTemplateByUserId",
	   	       	  	  type: "get",
	   	       	  	 data:{
	   	       	  	templateId:data[0]
	   	       	  		 }, 
	   	       	  	  success: function(){
	   	       	  	dataTable.ajax.reload();
	   	        	  	 $.smallBox({
	   	        				title : "Template Successfully deleted.",
	   	        				color : "#296191",
	   	        				iconSmall : "fa fa-thumbs-up bounce animated",
	   	        				timeout : 4000
	   	        				});

	   	       	  	  }
	   	       	  	});
	        	    }
	        	} ); 
	        
	          
	          $(".exportToExcel").click(function(e){

					var table = $('#API-grid');
					//alert(table.length);
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename:fileName,
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							//columns : [0,1,2,3,4,5],
							preserveColors: preserveColors
							
						});
					}
				});
	          
	         
} ); 
function setId(id)
{
	$("#templateId").val(id);
	$("#templateIdWhatsapp").val(id);
	templateId=id;
}



$("#syscTemp").click(function(e){
	  $.ajax({
			url : 'SysWhatsAppTemplate',
			type : 'GET',
			success : function(data) {
				alert("Success");
				dataTable.ajax.reload();
			},error : function(e) {
				alert("Success");
				console.log(e.message);
			}
		});
	  
	});
	
$("#uploadButton").click(function () {
    var formData = new FormData($("#mediaUploadForm")[0]);
    formData.append("templateId", $("#templateId").val());
    $.ajax({
        type: "POST",
        url: "uploadWhatsAppMedia", // Replace with your Java servlet URL
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
        	  alert("Upload successful: " + response);
        	$('#myModal1').modal('hide');
          
        },
        error: function () {
        	alert("Upload failed.");
        }
    });
});
</script>
<body>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>WhatsApp Template Manager</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger" id="syscTemp"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Synchronize</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">
 <button class="exportToExcel">Export to XLS</button>
				<table id="API-grid" class="display" style="width:100%">
<thead>
<tr>
<th>Id</th>
<th>Template Name</th>
 <th>Header</th> 
 <th>Body</th> 
 <th>Status</th>
 <th>Media Type</th>
 <th>Media Url</th>
  <th>Action</th>
</tr>
</thead>
</table>
			</div>
		</div>
	</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document"  >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <h4 class="modal-title" id="myModalLabel">Upload Media</h4>
      </div>
      <div class="modal-body">
     
      
       <div class="row"  >

  <div class="col-xs-3 col-sm-3">
   <form id="mediaUploadForm" enctype="multipart/form-data">
   
    <input type="file" name="mediaFile" id="mediaFile">
      <input type="text" id="templateId" name="templateId" class="form-control" disabled="disabled"><br>
            <button type="button" class="btn btn-primary"  id="uploadButton"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Upload Media</button>
    
  </form>
  </div>
</div>
      
        
 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document"  >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <h4 class="modal-title" id="myModalLabel">Sub Template</h4>
      </div>
      <div class="modal-body">
     
      
       <div class="row"  >

  <div class="col-xs-4 col-sm-4">
  
  <form id="mediaUploadFormWhatsapp">
    <input type="hidden" id="templateIdWhatsapp" name="templateId" class="form-control"><br>
        <button type="button" class="form-control" id="addFileButton">New Media</button>
        <div id="fileInputsContainer"></div><br>
        <button type="submit" class="form-control">Submit</button>
    </form>
    
 <!--  <form id="mediaUploadForm" enctype="multipart/form-data">
   
    <input type="file" name="mediaFile" id="mediaFile">
      <input type="text" id="templateId" name="templateId" class="form-control" disabled="disabled"><br>
            <button type="button" class="btn btn-primary"  id="uploadButton"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Upload Media</button>
    
  </form>-->
  </div>
</div>
      
        
 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>
    </div>
  </div>
</div>
</body>
</html>
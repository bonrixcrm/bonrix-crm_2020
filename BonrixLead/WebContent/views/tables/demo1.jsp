<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- <link href="css/sajan/tellyCaller.css"/> -->
<style>
.feature-box {
    background-color: #fff;
    border: 1px solid #e3e3e3;
    border-radius: 3px;
    margin-bottom: 15px;
    min-height: 20px;
    padding: 19px;
    text-align: center;
}
.feature-box:hover {
    background-color: #101525;
    border: 1px solid #e3e3e3;
    color: #959fae;
}
.feature-icon i {
    font-size: 50px;
}
.feature-box .h4,
.feature-box .h5,
.feature-box .h6,
.feature-box h4,
.feature-box h5,
.feature-box h6 {
    margin-bottom: 2px;
    margin-top: 2px;
    font-size: 16px;
}
.content-title {
    border-bottom: 1px solid #e8e8e8;
    border-top: 1px solid #e8e8e8;
    padding: 10px 20px;
}
.content-title .main-title {
    font-size: 20px;
    font-weight: 300;
    margin: 0;
}
.inner-content {
    padding: 20px;
}
/* #main-content {
    margin-left: 230px;
	position:relative;
} */

/* Header CSS  */
 .hoe-right-header {
    background: #fff none repeat scroll 0 0;
    height: 50px;
    line-height: 50px;
    z-index: 101;
	margin-left: 30px;
	position: relative;
}
.hoe-right-header > ul {
    display: block;
    margin: 0;
    padding: 0;
}
.hoe-right-header li {
  display: inline;
  float: left;
}
.hoe-right-header > li  a {
    color: #7e7e7e;
    padding: 17px 15px;
}
.hoe-right-header li a:hover,
.hoe-right-header li a:focus {
    background: #efefef;
    text-decoration: none;
}
.hoe-right-header a i {
    color: #7e7e7e;
    font-size: 14px;
}
.right-navbar {
    float: right;
	padding-right: 7px !important;
}
.hoe-sidebar-toggle a:after {
    background: transparent;
    border: 0px solid #efefef;
    content: "\f0c9";
    font-family: FontAwesome;
    font-size: 14px;
    line-height: 50px;
    padding: 0px 17px;
    position: absolute;
    color: #7e7e7e;
    z-index: 1000;
}
.hoe-sidebar-toggle a:hover:after {
    background: #efefef;
}
.hoe-left-header > .hoe-sidebar-toggle > a {
    display: none
}
.left-navbar > li:first-child {
    padding-left: 50px;
} 
.left-navbar .hoe-rheader-submenu > ul {
  left: auto;
  position: absolute;
}
.right-navbar .hoe-rheader-submenu > ul.dropdown-menu {
  left: auto;
  position: absolute;
  right: 0;
}
.hoe-rh-mega > ul  > li{}
.hoe-rheader-submenu > ul > li {
  display: block;
  width: 100%;
  position: relative;
}
.hoe-rheader-submenu > ul > li > a{
  display:block
}
.hoe-rheader-submenu > .dropdown-menu:after {
  border-bottom: 8px solid #FFF;
  border-left: 8px solid transparent;
  border-right: 8px solid transparent;
  bottom: 100%;
  content: " ";
  height: 0;
  left: 18px;
  margin-left: -3px;
  pointer-events: none;
  position: absolute;
  width: 0;
}
.hoe-rheader-submenu > .dropdown-menu:before {
  border-bottom: 9px solid #CCC;
  border-left: 9px solid transparent;
  border-right: 8px solid transparent;
  bottom: 100%;
  content: " ";
  height: 0;
  left: 18px;
  margin-left: -3px;
  pointer-events: none;
  position: absolute;
  width: 0;
}

.right-navbar .hoe-rheader-submenu > ul.dropdown-menu:after {
  border-bottom: 8px solid #FFF;
  border-left: 8px solid transparent;
  border-right: 8px solid transparent;
  bottom: 100%;
  content: " ";
  height: 0;
  left: auto;
  margin-left: -3px;
  pointer-events: none;
  position: absolute;
  right: 12px;
  width: 0;
}
.right-navbar .hoe-rheader-submenu > ul.dropdown-menu:before {
  border-bottom: 9px solid #CCC ;
  border-left: 9px solid transparent;
  border-right: 8px solid transparent;
  bottom: 100%;
  content: " ";
  height: 0;
  left: auto;
  margin-left: -3px;
  pointer-events: none;
  position: absolute;
  right: 12px;
  width: 0;
}

.hoe-rheader-submenu > .dropdown-menu {
  border: 0 solid rgba(0, 0, 0, 0.15);
  border-radius: 0;
  margin: 1px 0 0;
  min-width: 200px;
  padding: 0;
}
.hoe-header-profile  a  img {
  height: 40px;
  margin-top: -2px;
  width: 40px;
}
.hoe-right-header .hoe-rheader-submenu > a {
  color: #7e7e7e;
  padding: 17px 15px;
}
.hoe-right-header .hoe-rheader-submenu > a > i.fa{display:unset;}
.hoe-right-header .hoe-rheader-submenu > .dropdown-menu > li { 
  border-bottom: 1px solid #f3f3f3;
}
.hoe-right-header .hoe-rheader-submenu > .dropdown-menu > li  > a{
	clear: both;
    color: #7e7e7e;
    display: block;
    font-family: "lato",sans-serif;
    font-weight: 400;
    line-height: 1.42857;
    padding: 6px 10px;
    white-space: nowrap;
}
.hoe-right-header .hoe-rheader-submenu .dropdown-menu > li > a i {
  display: inline-block;
  margin-right: 9px;
  width: 15px;
}
.hoe-right-header .hoe-rheader-submenu .dropdown-menu > li > a > .badge {
  border-radius: 12px !important;
  display: inline;
  font-size: 11px;
  font-weight: 300;
  height: 18px;
  margin-top: 1px;
  padding: 3px 6px;
  position: absolute;
  right: 3px;
  text-align: center;
  text-shadow: none;
  vertical-align: middle;
  margin-right: 10px;
}
.badge-danger {
  background-color: #e7604a;
  background-image: none;
}
.badge-success {
  background-color: #70ca63;
  background-image: none;
}
.hoe-right-header .hoe-rheader-submenu .dropdown-menu li.hoe-submenu-label {

}
.hoe-right-header .hoe-rheader-submenu .dropdown-menu li.hoe-submenu-label h3 {
  background: #eaedf2 none repeat scroll 0 0;
  color: #62878f;
  font-size: 13px;
  margin-bottom: 0;
  margin-top: 0;
  padding: 12px 8px;
}
.hoe-right-header .hoe-rheader-submenu .dropdown-menu li.hoe-submenu-label .bold {
  font-weight: 700 !important;
}
.hoe-right-header .hoe-rheader-submenu .dropdown-menu li.hoe-submenu-label h3 a {
  float: right;
}
.hoe-right-header .message-notification .dropdown-menu{
 width: 280px;
}
.hoe-right-header  .hoe-rheader-submenu > .dropdown-menu .notification-avatar {
  margin: 0 10px 0 0;
  width: 32px;
}
.hoe-right-header  .hoe-rheader-submenu > .dropdown-menu .notification-title {
  font-weight: 600;
  font-size: 13px;
  color:#62878f;
}
.hoe-right-header  .hoe-rheader-submenu > .dropdown-menu .notification-ago {
  font-size: 11px;
  padding-top: 5px;
  position: absolute;
  right: 7px;
}
.hoe-right-header .hoe-rheader-submenu > .dropdown-menu .notification-ago-1 {
  font-size: 11px;
  position: absolute;
  right: 6px;
  top: 2px;
}

.hoe-right-header  .hoe-rheader-submenu > .dropdown-menu .notification-message {
  font-size: 13px;
  margin: 0 0 2px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 260px;
}
.hoe-right-header  .hoe-rheader-submenu > .dropdown-menu .notification-avatar {
  margin: 0 10px 0 0;
  width: 32px;
}
.icon-circle {
  border: 1px solid #ccc;
  border-radius: 200px;
   margin: 2px; 
  padding: 10px 11px !important;
}
#hoeapp-wrapper[hoe-device-type="phone"] .hoe-right-header .icon-circle {
  border: 1px solid #ccc;
  border-radius: 200px;
   margin: 2px;
  padding: 7px 8px !important;
}

/*Menu CSS  */
/***
User Profile Sidebar by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
***/

body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding-left: -20px;
  
  background: #fff;
  min-height: 460px;
}
</style>
</head>
<body>
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="http://keenthemes.com/preview/metronic/theme/assets/admin/pages/media/profile/profile_user.jpg" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						Marcus Doe
					</div>
					<div class="profile-usertitle-job">
						Developer
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<button type="button" class="btn btn-success btn-sm">Follow</button>
					<button type="button" class="btn btn-danger btn-sm">Message</button>
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							Overview </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-user"></i>
							Account Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-ok"></i>
							Tasks </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-flag"></i>
							Help </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			   <!--Middle COntent  -->
			   <div class="hoe-right-header" hoe-position-type="relative">
               <!--  <span class="hoe-sidebar-toggle"><a href="#"></a></span> -->
                <ul class="left-navbar">
					<li class="dropdown hoe-rheader-submenu message-notification open">
						<a href="#" class="dropdown-toggle icon-circle" data-toggle="dropdown" aria-expanded="true">
							<i class="fa fa-envelope-o"></i>
							<span class="badge badge-danger">5</span>
						</a>
						<ul class="dropdown-menu ">
							<li class="hoe-submenu-label">
								<h3>You have <span class="bold">5</span> New Messages <a href="#">view all</a></h3>
							</li>
							<li>
								<a href="#" class="clearfix"> 
									<img src="images/avatar-1.jpg" class="img-circle notification-avatar" alt="Avatar">
									<span class="notification-title">Nanterey Reslaba</span>
									<span class="notification-ago  ">3 min ago</span>
									<p class="notification-message">Hi James, Don't go anywhere, i will be coming soon. </p>
								</a>
							</li> 
							<li>
								<a href="#" class="clearfix"> 
									<img src="images/avatar-2.jpg" class="img-circle notification-avatar" alt="Avatar">
									<span class="notification-title">Polly Paton</span>
									<span class="notification-ago  ">5 min ago</span>
									<p class="notification-message">Hi James, Everything is working just fine here. </p>
								</a>
							</li> 
							<li>
								<a href="#" class="clearfix"> 
									<img src="images/avatar-3.jpg" class="img-circle notification-avatar" alt="Avatar">
									<span class="notification-title">Smith Doe</span>
									<span class="notification-ago  ">8 min ago</span>
									<p class="notification-message">Please mail me the all files when you complete your task.</p>
								</a>
							</li> 
							<li>
								<a href="#" class="clearfix"> 
									<img src="images/avatar-4.jpg" class="img-circle notification-avatar" alt="Avatar">
									<span class="notification-title">Zoey Lombardo</span>
									<span class="notification-ago  ">15 min ago</span>
									<p class="notification-message">Hi James, How are you?</p>
								</a>
							</li> 
							<li>
								<a href="#" class="clearfix"> 
									<img src="images/avatar-5.jpg" class="img-circle notification-avatar" alt="Avatar">
									<span class="notification-title">Alan Doyle</span>
									<span class="notification-ago  ">30 min ago</span>
									<p class="notification-message">Call me when you complete your work.</p>
								</a>
							</li> 
						</ul>
					</li>
					<li class="dropdown hoe-rheader-submenu message-notification left-min-30">
						<a href="#" class="dropdown-toggle icon-circle" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-bell-o"></i>
							<span class="badge badge-success">6</span>
						</a>
						<ul class="dropdown-menu ">
							<li class="hoe-submenu-label">
								<h3><span class="bold">6 pending</span> Notification <a href="#">view all</a></h3>
							</li>
							<li>
								<a href="#" class="clearfix"> 
									 <i class="fa fa-database red-text"></i>
									<span class="notification-title">Database overload- Sav3090</span>
									<span class="notification-ago  ">3 min ago</span>
									<p class="notification-message">Database overload due to incorrect queries</p>
								</a>
							</li> 
							<li>
								<a href="#" class="clearfix"> 
									  <i class="fa fa-exchange green-text"></i>   
									<span class="notification-title">Installing App v1.2.1</span>
									<span class="notification-ago  ">60 % Done</span>
									<p class="notification-message"> 
										</p><div class="progress">
											<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:60%"> 60%
											</div>
										</div>
									<p></p>
								</a>
							</li> 
							<li>
								<a href="#" class="clearfix"> 
									 <i class="fa fa-exclamation-triangle red-text"></i>
									<span class="notification-title">Application Error - Sav3085</span>
									<span class="notification-ago  ">10 min ago</span>
									<p class="notification-message">failed to initialize the application due to error weblogic.application.moduleexception</p>
								</a>
							</li>
							<li>
								<a href="#" class="clearfix"> 
									 <i class="fa fa-server yellow-text"></i>
									<span class="notification-title">Server Status - Sav3080</span>
									<span class="notification-ago  ">30GB Free Space</span>
									<p class="notification-message"> 
										</p><div class="progress">
											<div class="progress-bar progress-bar-success" role="progressbar" style="width:40%">
												 
											</div>
											<div class="progress-bar progress-bar-warning" role="progressbar" style="width:10%">
												 
											</div>
											<div class="progress-bar progress-bar-danger" role="progressbar" style="width:20%">
												 
											</div>
										</div>
									<p></p>
								</a>
							</li> 
							<li>
								<a href="#" class="clearfix"> 
									 <i class="fa fa-cogs green-text"></i>
									<span class="notification-title">Application Configured</span>
									<span class="notification-ago  ">30 min ago</span>
									<p class="notification-message">Your setting is updated on server Sav3060</p>
								</a>
							</li>
							<li> 
								<a href="#" class="clearfix"> 
									 <i class="fa fa-server blue-text"></i>
									<span class="notification-title">Server Status</span>
									<span class="notification-ago  ">300GB Free Space</span>
									<p class="notification-message"> 
										</p><div class="progress">
											<div class="progress-bar progress-bar-info" role="progressbar" style="width:80%">
												Free Space
											</div> 
										</div>
									<p></p>
								</a>
							</li> 
							 
						</ul>
					</li> 
					<li class="dropdown hoe-rheader-submenu message-notification left-min-65">
						<a href="#" class="dropdown-toggle icon-circle" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-tasks"></i>
							<span class="badge badge-danger">4</span>
						</a>
						<ul class="dropdown-menu ">
							<li class="hoe-submenu-label">
								<h3> You have <span class="bold">4 </span>pending Task <a href="#">view all</a></h3>
							</li>
							<li>
								<a href="#" class="clearfix"> 
									<img alt="Avatar" class="img-circle notification-avatar" src="images/avatar-1.jpg">
									<span class="notification-title"> Smith Doe Assigned new task </span>
									<span class="notification-ago-1 ">5 min ago</span>
									<p class="notification-message">Provide an Overview key mention characteristics for selected Keyword.</p>
								</a>
							</li>
							<li>
								<a href="#" class="clearfix"> 
									<img alt="Avatar" class="img-circle notification-avatar" src="images/avatar-2.jpg">
									<span class="notification-title"> Polly Paton Done his work</span>
									<span class="notification-ago-1 ">10 min ago</span>
									<p class="notification-message">Polly Paton has completed his work. please assign her new task. </p>
								</a>
							</li>
							<li>
								<a href="#" class="clearfix"> 
									<img alt="Avatar" class="img-circle notification-avatar" src="images/avatar-5.jpg">
									<span class="notification-title">Alan Installing App v1.2.1</span>
									<span class="notification-ago-1 ">15 min ago</span>
									<p class="notification-message"> 
										Alan Installing App v1.2.1 on server Sav3080
										</p><div class="progress">
											<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:60%"> 60%
											</div>
										</div>
									<p></p>
								</a>
							</li>
							<li>
								<a href="#" class="clearfix"> 
									<img alt="Avatar" class="img-circle notification-avatar" src="images/avatar-4.jpg">
									<span class="notification-title"> Zoey Lombardo like your task</span>
									<span class="notification-ago-1 ">20 min ago</span>
									<p class="notification-message">Zoey Lombardo like your server installation task.</p>
								</a>
							</li>
							 
						</ul>
					</li>
					
					
				</ul> 
				
                <ul class="right-navbar">
					<li class="dropdown hoe-rheader-submenu hoe-header-profile">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span><img class="img-circle " src="images/avatar-1.jpg"></span>
                            <span>James <b>Bond</b> <i class=" fa fa-angle-down"></i></span>
						</a> 
						<ul class="dropdown-menu ">
							<li><a href="#"><i class="fa fa-user"></i>My Profile</a></li>
							<li><a href="#"><i class="fa fa-calendar"></i>My Calendar</a></li>
							<li><a href="#"><i class="fa fa-envelope"></i>My Inbox  <span class="badge badge-danger">
							3 </span></a></li>
							<li><a href="#"><i class="fa fa-rocket"></i>My Tasks <span class="badge badge-success">
							7 </span></a></li>
							<li><a href="#"><i class="fa fa-lock"></i>Lock Screen</a></li>
							<li><a href="#"><i class="fa fa-power-off"></i>Logout</a></li>
						</ul>
					</li>
					<li class="dropdown hoe-rheader-submenu hidden-xs">
						<a href="#" class="dropdown-toggle icon-circle" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a> 
					</li> 
				</ul>
				
            </div>
<!--  -->
                            
                            <section id="main-content">
				<div class="content-title">
                    <h3 class="main-title">Sidebar Menu</h3>
                    <span>Sidebar Features</span>
                </div>
                <div class="inner-content">
                    <div class="panel theme-panel">
                        <div class="panel-heading">
                            <span class="panel-title">
								SideBar Menu Feature <i class="fa fa-bullhorn"></i>
							</span>
                        </div>
                        <div class="panel-body">
                            <div class="row clearfix">
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-code"></i></span>
                                        <h4>One Code for All Device </h4>
                                        <span>One code used for all device</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-mobile"></i></span>
                                        <h4>Fully Responsive</h4>
                                        <span>Desktops, Tablets and Mobile</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-th-list"></i></span>
                                        <h4>Minimized Menu </h4>
                                        <span>Minimized Menu on Toggle</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-list-alt"></i></span>
                                        <h4>Responsive SubMenu</h4>
                                        <span>4 level SubMenu added</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-file-code-o"></i></span>
                                        <h4>Light weight</h4>
                                        <span>Using Minified CSS File </span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-css3"></i></span>
                                        <h4>Jquery/CSS3 animations</h4>
                                        <span>Using Jquery,CSS3,Animation</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-flag"></i></span>
                                        <h4>Vector icons</h4>
                                        <span>Using Font Awesome Web font</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-angellist"></i></span>
                                        <h4>Collapsible menu</h4>
                                        <span>Collapsible menu in Tablets </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-file-text"></i></span>
                                        <h4>Well documentation</h4>
                                        <span>Step by step instructions to use menu</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-life-ring"></i></span>
                                        <h4>Free Support</h4>
                                        <span>you Can contact me in my profile</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-pencil-square-o"></i></span>
                                        <h4>Easy to use/customizable</h4>
                                        <span>Well coded Easy to use in website</span>
                                    </div>
                                </div>
                                <div class="col-md-3 column">
                                    <div class="feature-box">
                                        <span class="feature-icon"><i class="fa fa-eye"></i></span>
                                        <h4>Coming Soon</h4>
                                        <span>Theme Option Coming Soon</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </section>
            </div>
		</div>
	</div>
</div>

<!--  -->

                            
</body>
</html>
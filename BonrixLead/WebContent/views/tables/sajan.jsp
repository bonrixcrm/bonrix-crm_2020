<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<style type="text/css">
    
    /* invalid input */
    .ex9_bvalidator_invalid{
        background-color: #FFFFAE;
    }
    
    /* error message */
    .ex9_bvalidator_errmsg {
        background-color:#333;
        font-size:10px;
        border:1px solid #999;
        color:#FFF;
        white-space:nowrap;
        padding-right: 10px;
        padding-left: 5px;
        font-family: Arial, Helvetica, sans-serif;
    }
    
    /* close icon */
    .ex9_bvalidator_errmsg .ex9_bvalidator_close_icon {
        margin-left: 5px;
        margin-top: -2px;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        color:#F96;
        cursor:pointer;
    }
    
    /* arrow */
    .ex9_bvalidator_errmsg em {
        display:block;
        height: 9px;
        width: 18px;
        position:absolute;
        bottom:-9px;
        left:5px;
        background-image: url(http://bojanmauser.from.hr/bvalidator/arrow.gif);
        background-repeat: no-repeat;
    }
</style>

<script type="text/javascript">            
    
    var options9 = {
        classNamePrefix: 'ex9_bvalidator_',
        validateOn: 'keyup',
        templateCloseIcon: '<table><tr><td>{message}</td><td><div class="{closeIconClass}">x</div></td></tr></table>'
    };
    
    $(document).ready(function () {
        $('#form9').bValidator(options9);
    });
    
</script>


<form id="form9" method="post">
    <p>Only digits:
        <input type="text" data-bvalidator="digit,required">
        <br>
        <br>
        Minimum value 10:
        <input type="text" data-bvalidator="min[10],required">
    </p>
    <p>
        <input type="submit" value="Submit">
    </p>
</form>
</body>
</html>

<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/includes/SessionCheck.jsp"%>
<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>
<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>

<section id="widget-grid" class="">

	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-smppp"
		data-widget-editbutton="false">
	
		
		
<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong> SMPP Setting</strong> </h2>		
							
					<div class="widget-toolbar" role="menu">
						<a href="./user/createSmpp.jsp" data-toggle="modal"
			data-target="#remoteModal" class="btn btn-primary">
			 <i
			class="fa fa-circle-arrow-up fa-lg"></i> Create SMPP
		</a>
					</div>
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
		<div>

			<div class="jarviswidget-editbox">

			</div>

			<div class="widget-body no-padding">

				<table id="smptable_tabletools"
					class="table table-striped table-bordered table-hover">
					<thead>


						<tr role="row">
							<th data-hide="phone" class="sorting_asc" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1"
								aria-sort="ascending"
								aria-label="ID: activate to sort column ascending"
								style="width: 27px;">ID</th>
							<th data-class="expand" class="sorting" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1"
								aria-label="Name: activate to sort column ascending">Name</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1">System
								ID</th>
							<th data-hide="phone" class="sorting" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1">Password</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1">Active
							</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1">
								Running</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1">System
								Type</th>
							<th>Ch
								</th>
							<th >Channel
								Error</th>
								<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="smptable_tabletools" rowspan="1" colspan="1">
								Ip</th>
								<th>
								Max Conn.</th>
                             <th>Port</th>
                             <th>TPS</th>
                             <th>Thread</th>
							<th>Action</th>



						</tr>
					</thead>

				</table>

			</div>

		</div>

	</div>
	

</section>

<script type="text/javascript">

	var smptable = null;

	pageSetUp();

	function reloadtable() {

		 smptable.ajax.reload();	
	}

	var pagefunction = function() {

		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
		getdata();
		$('#remoteModal').on('hidden.bs.modal', function() {
			reloadtable();
		});

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$("div.toolbar")
				.html(
						'<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

	};

	// load related plugins

	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
			function() {
				loadScript(
						"js/plugin/datatables/dataTables.colVis.min.js",
						function() {
							loadScript(
									"js/plugin/datatables/dataTables.tableTools.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
												function() {

													loadScript(
															"js/plugin/datatable-responsive/datatables.responsive.min.js",

														
																		pagefunction)
															
												});
									});
						});
			});

	function getdata() {
		if (smptable)
			smptable.fnDestroy();
		smptable = $('#smptable_tabletools')
				.DataTable(
						{
							"processing" : false,
							"serverSide" : false,
							"bServerSide" : false,

							//"bdestroy": true,

							//  "bDestroy": true,
							"ajax" : "getSmpp",
							"fnCreatedRow" : function(nRow, aData, iDataIndex) {
								/* $('td:eq(0)', nRow).html(aData[0]);
								$('td:eq(1)', nRow).html(aData[1]);
								$('td:eq(2)', nRow).html(aData[2]);
								$('td:eq(3)', nRow).html(aData[3]);
 */
								if (aData[4]) {
									$('td:eq(4)', nRow).html(
											'<a class="btn btn-success btn-xs" href="#" onclick="deactiveit('
													+ aData[0]
													+ ',0)" >Active</a>');
								} else {
									$('td:eq(4)', nRow).html(
											'<a class="btn btn-danger btn-xs" href="#" onclick="deactiveit('
													+ aData[0]
													+ ',1)">Deactive</a>');
								}

							/* 	var s="";
								for (i = 0; i < aData[4]; i++) {
									s=s+"A";
									for (i = 0; i < aData[4]; i++) {
										$('td:eq(5)', nRow).html(s);
									}
								} 
								
									 */
								/* $('td:eq(5)', nRow).html(aData[5]);
								$('td:eq(6)', nRow).html(aData[6]);
								$('td:eq(7)', nRow).html(aData[7]);
								$('td:eq(8)', nRow).html(aData[8]);
								$('td:eq(9)', nRow).html(aData[9]);
								$('td:eq(10)', nRow).html(aData[10]); */
								$('td:eq(14)', nRow)
										.html(
												'<div class="btn-group display-inline pull-right text-align-left hidden-tablet">	<button class="btn btn-xs btn-default dropdown-toggle"	data-toggle="dropdown">'
	+'<i class="fa fa-cog fa-lg"></i></button><ul class="dropdown-menu dropdown-menu-xs pull-right"><li><a data-toggle="modal" data-target="#remoteModal" href="./user/updateSmpp.jsp?id='+ aData[0]+ '">'
	+'<i class="fa fa-file fa-lg fa-fw txt-color-greenLight"></i> <u>E</u>DIT</a></li><li><a href="#" onclick="delhttp('
														+ aData[0]+ ')");"><i class="fa fa-times fa-lg fa-fw txt-color-red"></i> <u>D</u>elete</a>'
			+'</li><li class="divider"></li><li class="text-align-center"><a href="javascript:void(0);">Cancel</a></li></ul></div>');

							},
							"autoWidth" : true,
							"rowCallback" : function(nRow) {

							},
							"drawCallback" : function(oSettings) {

							}
						});

	}

	function delhttp(id) {
		var conf = confirm('Are you sure you want to delete this?');
		if (conf) {
			$.get("deleteSmpp?id=" + id, function(msg) {
				noAnswer();

				reloadtable();

			});
		}

	}
	function noAnswer() {

		$
				.smallBox({

					content : "<i class='fa fa-clock-o'></i> <i>Host Setting Delete Succefully.</i>",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
				});

	}
/* 	function deactiveit(uid, act) {

		$
				.SmartMessageBox(
						{
							title : "Alert!",
							content : "Are your sure to " + act == 1 ? "Active"
									: "Deactive" + " this user ?",
							buttons : '[No][Yes]'
						},
						function(ButtonPressed) {
							if (ButtonPressed === "Yes") {

								$
										.get(
												"user/activedeactive?uid="
														+ uid + "&isactive="
														+ act,
												function(msg) {

													reloadtable();
													var stats = "";
													if (act == 1) {
														stats = "<i class='fa fa-clock-o'></i> <i>User Activated Successfully...</i>"
													} else {
														stats = "<i class='fa fa-clock-o'></i> <i>User Deactivated Successfully...</i>"
													}
													$
															.smallBox({
																title : "Success",
																content : stats,
																color : "#659265",
																iconSmall : "fa fa-check fa-2x fadeInRight animated",
																timeout : 4000
															});
												});

							}
							if (ButtonPressed === "No") {
								$
										.smallBox({
											title : "OK Sir",
											content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
											color : "#C46A69",
											iconSmall : "fa fa-times fa-2x fadeInRight animated",
											timeout : 4000
										});
							}

						});

	}; */

	function deactiveit(uid, act) {

		$
				.SmartMessageBox(
						{
							title : "Alert!",
							content : "Are your sure to " + act == 1 ? "Active"
									: "Deactive" + " this user ?",
							buttons : '[No][Yes]'
						},
						function(ButtonPressed) {
							if (ButtonPressed === "Yes") {

								$
										.get(
												"activedeactiveSmpp?uid=" + uid
														+ "&isactive=" + act,
												function(msg) {

													reloadtable();
													var stats = "";
													if (act == 1) {
														stats = "<i class='fa fa-clock-o'></i> <i>User Activated Successfully...</i>"
													} else {
														stats = "<i class='fa fa-clock-o'></i> <i>User Deactivated Successfully...</i>"
													}
													$
															.smallBox({
																title : "Success",
																content : stats,
																color : "#659265",
																iconSmall : "fa fa-check fa-2x fadeInRight animated",
																timeout : 4000
															});
												});

							}
							if (ButtonPressed === "No") {
								$
										.smallBox({
											title : "OK Sir",
											content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
											color : "#C46A69",
											iconSmall : "fa fa-times fa-2x fadeInRight animated",
											timeout : 4000
										});
							}

						});

	}
</script>

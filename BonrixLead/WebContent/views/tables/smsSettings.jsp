<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<!--<script src=js/jquery.js></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>-->
<script>
var dataTable=null;
$(document).ready(function() {
	//alert('Ready');
	
	          dataTable = $('#API-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetAllAPI",
	             
	             "fnCreatedRow": function( nRow, aData, iDataIndex ) {
		   
		        	var btnStr= "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>"+
	                    "Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"+
	                    "&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-warning' id='deactive-btn'><span class='glyphicon glyphicon-eye-close' aria-hidden='true'></span>&nbsp; Active &nbsp;  </button>";
	            
	                    var btnStr1= "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>"+
	                    "Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"+
	                    "&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-warning' id='active-btn'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span> Deactive  </button>";
	                    
	                    if(aData[4]==true)
	                    $('td:eq(4)', nRow).html(btnStr1);
	                    else 
	                    	 $('td:eq(4)', nRow).html(btnStr);
	            	
		        }, 
		       

	        } ); 
	       

//} ); 
	           dataTable.on( 'order.dt search.dt', function () {
	          dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	              cell.innerHTML = i+1;
	          } );
	         } ).draw(); 

 $('#API-grid tbody').on( 'click', '#edit-btn', function () {
	
   var data = dataTable.row( $(this).parents('tr') ).data();
 //c  alert("'Edit is: "+ data[3]);
   $('#id').val(data[0]);
   $('#key').val(data[2]);
   $('#provider').val(data[3]);
   $('#desc').val(data[1]);
   
 	
  
} ); 


 $('#API-grid tbody').on( 'click', '#delete-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
   //  alert("'Delete is: "+ data[ 0 ] );
    $.ajax({
  	  url: "deleteAPI",
  	  type: "get",
  	  data:"id="+data[ 0 ],
  	  success: function(html){
  	 dataTable.ajax.reload();
  	  }
  	});
    
    
} );  

$('#API-grid tbody').on( 'click', '#deactive-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
     alert("'Active is: "+ data[ 0 ] ); 
    $.ajax({
  	  url: "activeAPI",
  	  type: "get",
  	  data:"id="+data[ 0 ],
  	  success: function(html){
  	   
  	 dataTable.ajax.reload();
  	  }
  	});
} );
} ); 

 function addAPI()
{
	var key=$('#addkey').val();
var provider=$('#addprovider').val();
var api=$('#addapi').val();
var final_API=encodeURI(api);
	$.ajax({
	  	  url: "addAPI",
	  	  type: "get",
	  	   data:{key:key,provider:provider,api:api}, 
	  	  success: function(){
			   $('#myModaladd').modal('hide');
	  	  	 dataTable.ajax.reload();
	  	  	 $.smallBox({
	  				title : "Message API Successfully Added.",
	  				color : "#296191",
	  				iconSmall : "fa fa-thumbs-up bounce animated",
	  				timeout : 4000
	  				});
					$("#addform").trigger('reset');
	  	  }
	  	});
} 

function updateAPI()
{
	   var id=$('#id').val();
	   var key=$('#key').val();
	   var provider=$('#provider').val();
	   var desc=$('#desc').val();
	   
	   $.ajax({
		  	  url: "updateAPI",
		  	  type: "get",
		  	   data:{id:id,key:key,provider:provider,desc:desc}, 
		  	  success: function(){
			    $('#myModal').modal('hide');
	  	  	 dataTable.ajax.reload();
	  	  	 $.smallBox({
	  				title : "API Successfully Updated.",
	  				color : "#296191",
	  				iconSmall : "fa fa-thumbs-up bounce animated",
	  				timeout : 4000
	  				});
		  	  }
		  	});
}
</script>
<body>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>API Manager</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModaladd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New API</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="API-grid" class="table table-striped table-bordered">
<thead>
<tr>
<th>Id</th>
<th style="width:500px;">API</th>
 <th>Success Key</th> 
 <th>Provider</th> 
 <th>Action</th>
</tr>
</thead>
</table>
			</div>
		</div>
	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modify API Data</h4>
      </div>
      <div class="modal-body">
      <form method="get" id="updateform">
      
         
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>API Id</b></div>
   <div class="col-xs-6 col-sm-6"><input type="text" id="id" readonly="readonly" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Success Key</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="key" class="form-control"></div>
        </div>
         
         <hr>
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Provider</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="provider" class="form-control"></div>
        </div>
        <hr>
        
        
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>API</b></div>
   <div class="col-xs-6 col-sm-6"><textarea rows="10" cols="40" id="desc"></textarea></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="updateAPI()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>





<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New API</h4>
      </div>
      <div class="modal-body">
      <form method="get" id="addform">
      
         
         
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Success Key</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="addkey" class="form-control"></div>
        </div>
        <hr>
        
        <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Provider</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="addprovider" class="form-control"></div>
        </div>
        <hr>
        
        
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>API</b></div>
   <div class="col-xs-6 col-sm-6"><textarea rows="8" cols="40" id="addapi"></textarea></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="addAPI()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span>&nbsp;Save API</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
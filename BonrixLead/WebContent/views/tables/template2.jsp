<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%
	String name = currentUser.getUsername();
%>
<c:set var="name" scope="session" value="<%=currentUser.getUsername()%>" />


	<script type="text/javascript">
				$(document).ready(function() {

					$('#datepicker').datepicker();
				});

				function formSub(event) {

					//alert("asda");
					/* 		var senderName = $('#senderName').val();
						var uid = $('#uid').val(); */

					
					var ip = $('#ip').val();
					$.ajax({
						url : "createApiKey",
						type : "POST",
						data : {
							'ip' : ip
						},
						success : function(response) {

/* 							alert(response); */

							/*  $('#datatable_tabletools tr').last().after('<tr><td>'+isactive+'</td><td>'+senderName+'</td></tr>'); */
							//window.location = "login.jsp";
						},
						error : function(xhr, status, error) {
							alert("***************");
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>


<c:if test="${name=='admin'}">

	<sql:query var="msg" dataSource="SMS">
                 	select * from tamplate
                           
                           </sql:query>


</c:if>


<c:if test="${name!='admin'}">

	<sql:query var="msg" dataSource="SMS">
                 	select * from tamplate where uid=<%=currentUser.getUserid()%>

	</sql:query>



</c:if>


<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
				<a href="./user/createTamplate.jsp" data-toggle="modal"
					data-target="#remoteModal" class="btn btn-primary"> <i
					class="fa fa-circle-arrow-up fa-lg"></i> Create Template
				</a>

				<!-- MODAL PLACE HOLDER -->
				<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
					aria-labelledby="remoteModalLabel" aria-hidden="true"
					style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content"></div>
					</div>
				</div>

			</div>

		</div>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		<ul id="sparks" class="">
			<li class="sparks-info">
				<h5>
					My Income <span class="txt-color-blue">$47,171</span>
				</h5>
				<div
					class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
					1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700,
					3631, 2471</div>
			</li>
			<li class="sparks-info">
				<h5>
					Site Traffic <span class="txt-color-purple"><i
						class="fa fa-arrow-circle-up" data-rel="bootstrap-tooltip"
						title="Increased"></i>&nbsp;45%</span>
				</h5>
				<div
					class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
					110,150,300,130,400,240,220,310,220,300, 270, 210</div>
			</li>
			<li class="sparks-info">
				<h5>
					Site Orders <span class="txt-color-greenDark"><i
						class="fa fa-shopping-cart"></i>&nbsp;2447</span>
				</h5>
				<div
					class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
					110,150,300,130,400,240,220,310,220,300, 270, 210</div>
			</li>
		</ul>
	</div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">



			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
				data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Export to PDF / Excel</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<table id="datatable_tabletools"
							class="table table-striped table-bordered table-hover"
							width="100%">
							<thead>
								<tr>
									
									<th data-class="expand">Tamplate</th>
									<th data-class="expand">Message</th>
									<th data-class="expand">Action</th>
									

								</tr>
							</thead>
							<tbody>

								<c:forEach var="all" items="${msg.rows}">

									<c:set var="isactive" scope="session" value="${all.isactive}" />

									<tr role="row" class="odd">
										<%-- <td class="sorting_1">${all.uid}</td> --%>
									
										<td>${all.tname}</td>
										<td>${all.tmessage}</td>
										

										<c:if test="${name=='admin'}">
											<c:if test="${isactive==false}">
												<td><a class="btn btn-info btn-xs"
													href="approveTamplate?tid=${all.tid}" 
													class="btn btn-primary btn-xs"
													onclick="return confirm('Are you sure?');"
													>Approve</a>&nbsp;&nbsp;&nbsp;&nbsp;<a
													class="btn btn-danger btn-xs"
													href="deleteTamplate?tid=${all.tid}"
													onclick="return confirm('Are you sure?');">Delete</a></td>
											</c:if>
											<c:if test="${isactive==true}">
												<td>
													<a href="#" onclick="return confirm('All ready Approved ?');" 	class="btn btn-success btn-xs"> Approved</a>
													&nbsp;<a
													class="btn btn-danger btn-xs"
													href="deleteTamplate?tid=${all.tid}"
													onclick="return confirm('Are you sure?');">Delete</a>
												</td>
											</c:if>
										</c:if>
										<%-- <td><a href="./views/media/TinyHitList.jsp?mid=${all.mediaid}" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Detail</td>
									<td><a href="./views/quickMessage.jsp?url=${baseURL}/${all.tinyurl}" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</td> --%>

									</tr>
								</c:forEach>


							</tbody>
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 * 
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 * 
	 */

	// PAGE RELATED SCRIPTS
	// pagefunction	
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
		 */

		/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic')
				.dataTable(
						{
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_dt_basic) {
									responsiveHelper_dt_basic = new ResponsiveDatatablesHelper(
											$('#dt_basic'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_dt_basic
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_dt_basic.respond();
							}
						});

		/* END BASIC */

		/* COLUMN FILTER  */
		var otable = $('#datatable_fixed_column')
				.DataTable(
						{
							//"bFilter": false,
							//"bInfo": false,
							//"bLengthChange": false
							//"bAutoWidth": false,
							//"bPaginate": false,
							//"bStateSave": true // saves sort state using localStorage
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_datatable_fixed_column) {
									responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper(
											$('#datatable_fixed_column'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_datatable_fixed_column
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_datatable_fixed_column
										.respond();
							}

						});

		// custom toolbar
		$("div.toolbar")
				.html(
						'<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

		// Apply the filter
		$("#datatable_fixed_column thead th input[type=text]").on(
				'keyup change',
				function() {

					otable.column($(this).parent().index() + ':visible')
							.search(this.value).draw();

				});
		/* END COLUMN FILTER */

		/* COLUMN SHOW - HIDE */
		$('#datatable_col_reorder')
				.dataTable(
						{
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_datatable_col_reorder) {
									responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper(
											$('#datatable_col_reorder'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_datatable_col_reorder
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_datatable_col_reorder
										.respond();
							}
						});

		/* END COLUMN SHOW - HIDE */

		/* TABLETOOLS */
		$('#datatable_tabletools')
				.dataTable(
						{

							// Tabletools options: 
							//   https://datatables.net/extensions/tabletools/button_options
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
							"oTableTools" : {
								"aButtons" : [
										"copy",
										"csv",
										"xls",
										{
											"sExtends" : "pdf",
											"sTitle" : "SmartAdmin_PDF",
											"sPdfMessage" : "SmartAdmin PDF Export",
											"sPdfSize" : "letter"
										},
										{
											"sExtends" : "print",
											"sMessage" : "Generated by SmartAdmin <i>(press Esc to close)</i>"
										} ],
								"sSwfPath" : "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
							},
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_datatable_tabletools) {
									responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper(
											$('#datatable_tabletools'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_datatable_tabletools
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_datatable_tabletools.respond();
							}
						});

		/* END TABLETOOLS */

	};

	// load related plugins

	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
			function() {
				loadScript(
						"js/plugin/datatables/dataTables.colVis.min.js",
						function() {
							loadScript(
									"js/plugin/datatables/dataTables.tableTools.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
												function() {
													loadScript(
															"js/plugin/datatable-responsive/datatables.responsive.min.js",
															pagefunction)
												});
									});
						});
			});
</script>

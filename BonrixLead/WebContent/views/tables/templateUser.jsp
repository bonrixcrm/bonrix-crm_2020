<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%
	String name = currentUser.getUsername();
%>
<c:set var="name" scope="session" value="<%=currentUser.getUsername()%>" />

	<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
					aria-labelledby="remoteModalLabel" aria-hidden="true"
					style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content"></div>
					</div>
				</div>


<section id="widget-grid" class="">

	<div class="row">

		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-templateuser"
				data-widget-editbutton="false">
				
				<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Template</strong> <i> List</i></h2>		
							
					<div class="widget-toolbar" role="menu">
					<a href="./user/createTamplate.jsp" data-toggle="modal"
					data-target="#remoteModal" class="btn btn-primary"> <i
					class="fa fa-circle-arrow-up fa-lg"></i> Create Template
		</a>
					</div>
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

				<div>

					<div class="jarviswidget-editbox">

					</div>

					<div class="widget-body no-padding">

						<table id="tempatetable_tabletools"
							class="table table-striped table-bordered table-hover"
							width="100%">
							<thead>
								<tr>

									<th data-class="expand">Sr no</th>
									<th data-class="expand">Title</th>
									<th data-class="expand">Template</th>
									<th data-class="expand">Action</th>
									<!-- <th data-class="expand">Status</th> -->
									<!-- <th >Action</th> -->

								</tr> 
							</thead>
							<tbody>
							</tbody>
						</table>

					</div>

				</div>

			</div>

		</article>

	</div>


</section>

<script type="text/javascript">

	var templatetable;
	pageSetUp();
	function getdata() {

		templatetable = $('#tempatetable_tabletools')
				.DataTable(
						{
							"processing" : false,
							"serverSide" : false,
							"bServerSide" : false,						
							"ajax" : "getTamplates?istrans=false",
							"fnCreatedRow" : function(nRow, aData, iDataIndex) {

								$('td:eq(0)', nRow).html(aData[0]);
								$('td:eq(1)', nRow).html(aData[1]);
								$('td:eq(2)', nRow).html(aData[2]);
								//$('td:eq(3)', nRow).html(aData[3]);
								$('td:eq(3)', nRow)
								.html(
										'<a href="#" id="tid" class="btn btn-primary btn-xs"  onclick="action('
												+ aData[0]
												+ ' )"> Action </a>');
							},
							"autoWidth" : true,
							"rowCallback" : function(nRow) {
							},
							"drawCallback" : function(oSettings) {
							}
						});
		
	
	}
	function action(e) {

		$
				.SmartMessageBox(
						{
							title : "Action",
							content : "This is a confirmation box for Perform action.",
							buttons : '[Close][Edit][Delete]'
						},
						function(ButtonPressed) {
							if (ButtonPressed === "Many") {

								//alert("many");
								$
										.smallBox({
											title : "Callback function",
											content : "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",

											color : "#659265",
											iconSmall : "fa fa-check fa-2x fadeInRight animated",
											timeout : 4000,

										});
							}

							if (ButtonPressed == "Delete") {

								var tid = $('#tid').val();
								var option = "2";
								$
										.ajax({
											url : "deleteTamplate",
											type : "POST",
											data : {
												'tid' : e,
												'option' : option
											},
											beforeSend : function(response) {
												$('#sendit').attr('disabled',
														true);
											},
											
											success : function(response) {
												$
														.smallBox({
															title : response
																	+ ButtonPressed+"d.",
															content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
															color : "#296191",
															iconSmall : "fa fa-thumbs-up bounce animated",
															timeout : 4000
														});

												reloadtable();

											},
											error : function(xhr, status, error) {
												alert(xhr.responseText);
												$('#sendit').attr('disabled',
														false);
											}
										});
								return false;
							}

							if (ButtonPressed == "Edit") {
								$("#remoteModal")
										.modal(
												{
													remote : "./user/updateTamplate.jsp?tid="
															+ e,
													show : 'true'
												});

							}

						});
	};

	function reloadtable() {

		templatetable.ajax.reload();
	}
	pageSetUp();

	var pagefunction = function() {
		
	getdata();
		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
	
		$('#remoteModal').on('hidden.bs.modal', function() {
			reloadtable();
		});


		$("div.toolbar")
				.html(
						'<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

		// Apply the filter
		$("#datatable_fixed_column thead th input[type=text]").on(
				'keyup change',
				function() {

					otable.column($(this).parent().index() + ':visible')
							.search(this.value).draw();

				});

	};


	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
			function() {
				loadScript(
						"js/plugin/datatables/dataTables.colVis.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
																	
																	function() {
																		loadScript(
																				"js/plugin/datatable-responsive/datatables.responsive.min.js",pagefunction)
																	});
						});
			});

	
	function delhttp(id) {
		var conf = confirm('Are you sure you want to delete this?');

		if (conf) {
			$.get("deleteTamplate?tid=" + id, function(msg) {

				reloadtable();
			});
		}

	}
</script>

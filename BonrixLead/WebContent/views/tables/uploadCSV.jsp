<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="/includes/SessionCheck.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	<%!
public long id =0;
%>
<%
Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
MediUser currentUser1 = (MediUser) auth1.getPrincipal();
 id = currentUser1.getUserid();
	out.println("<input type='hidden' id='uId' value='"+id+"'>"); 
%>
    <style>
    .tbl
    {
    background-color:white; 
    }
    </style>
    
<script>
var x=0;
var checkboxValues = [];
var fval =[];
var fname=[];
var filepath="";
var idcoma="";
var fieldcoma="";
var FileName="";
var cat="";

function addCSVLeadData(){
	var i=0;

	$('input[name="selectchk"]:checked').each(function() {
		   checkboxValues.push($(this).val());
		});
	
	 var indsys=$("#indSYS").is(':checked') ? 1 : 0;
	 var autodial=$("#autodial").is(':checked') ? 1 : 0;
	 idcoma="";
	 fieldcoma="";
	 for(i=0; i<x;i++){
		var id='#'+'field'+i;
		var chkval="";
		chkval=checkboxValues[i];
		 if($(' '+id+' option:selected').text()!='Field' ){
		 var res = id.substring(6, 7);
		 idcoma +=res+",";
		 fieldcoma +=$(' '+id+' option:selected').val()+",";
		  cat=$('#cat option:selected').val();
		}
		}

	 $.ajax({
	  	  url: 'AddCSVDataField',
	  	  type: 'GET',
	  	  data: {fieldname:fieldcoma,rowfield:idcoma,filepath:FileName,cat:cat,indsys:indsys,autodial:autodial},
         complete: function () {
         },
	  	   success: function(data) {
	  		$.smallBox({
				title : "Leads Successfully Genetated From File.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				});
		  location.reload();
	  	  },
		 error: function(e) {
			 $.smallBox({
					title : "Leads Successfully Genetated From File.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					});
			 // location.reload();
	 		console.log(e.message);
	 	  }
		} );   
}


function uploadFormData(){
	 var form = new FormData(document.getElementById('idUploadLogoForm'));
	 $.ajax({
	  url: "fileUpload",
	  data: form,
	  dataType: 'text',
	  processData: false,
	  contentType: false,
	  type: 'POST',
	  success: function (filename) {
		FileName=filename;

	 var fullPath =document.getElementById('ticket_exampleInputFile').value;
		var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		var filename = fullPath.substring(startIndex);
		if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
			filename = filename.substring(1);
		}
	var ext = filename.split('.').pop();
		var exlFileName="";
		ext.toString();
		
		if(ext=="csv" || ext=="CSV")
			{
			gen();
			}
		else
			{
			 var form = new FormData(document.getElementById('idUploadLogoForm'));

				$.ajax({
		 			 url: "fileUpload",
		 			  data: form,
		 			  dataType: 'text',
		 			  processData: false,
		 			  contentType: false,
		 			  type: 'POST',
				  	  success: function(exlFileName) {
				  		$.ajax({
					  		url: "ExcelToCsv",
					  	  	  type: "get",
					  	  	  data:"fileName="+exlFileName,
					  	  	beforeSend: function () {
						  		$.smallBox({
									title : "Please Wait....",
									color : "#296191",
									iconSmall : "fa fa-thumbs-up bounce animated"
									});
							},
						  	  success: function(filename) {
						  		var filenameExt = filename.substring(0,filename.lastIndexOf("."));
						  		filenameExt+=".csv";
						  		FileName=filenameExt;
						  		gen();
						  		  },
						  		 error: function(e) {
								 		console.log(e.message);
								 	  }
									} );
				  	},
			  		 error: function(e) {
					 		console.log(e.message);
					 	  }
						} );
			}
	  },
	  error: function (error) {
		  console.log(error) 
	  }
	 });
}

function gen()
{
	var myRowBody="";
	var mytable=null;
	var myRowFooter=null;
	   mytable= $('#CSVTable').CSVToTable('UploadFile//'+FileName, 
			 { 
		  tableClass:'table  table-bordered tbl'
			    }
			).bind("loadComplete",function() { 
				myRowBody += "<tr>";
	           
	             x = $("th").length;
	            var i=0;
	            var id;
	            for (i = 0; i < x; i++) { 
	            	myRowBody +="<td><div class='row'>"+
	        			  "<div class='col-lg-9'>"+
	        			   " <div class='input-group'>"+
	        			    " <span class='input-group-addon'>"+
	        			      "  <input type='checkbox' name='selectchk' value='"+i+"' id='chkrow"+i+"'>"+
	        			      "</span>"+
	        			    " <select class='form-control' ' id='field"+i+"' >"+
	        			    "  <option value='audi'>Field</option>"+
		        			 " <option value='firstName'>First Name</option>"+
		        			 " <option value='lastName'>Last Name</option>"+
		        			 " <option value='email'>Email</option>"+
		        			 " <option value='mobileNo'>Mobile No</option>"+
		        			 " <option value='companyName'>Company Name</option>"+
		        			 " <option value='website'>Web Url</option>"+
		        			 " <option value='Country'>Country</option>"+
		        			 " <option value='state'>State</option>"+
		        			 " <option value='city'>City</option>"+
		        			 " <option value='category'>Category</option>"+
		        			 " <option value='telecaller'>Telecaller</option>"+
	        			        			"</select>"+
	        			   " </div>"+
	        			  "</div>"+
	        			"</div>";
	        			myRowBody +="</td>";
	            	}
	            myRowBody +="</tr>";
	            $("#CSVTable tr:first").after(myRowBody);
	        	   myRowFooter = "<tr><td><b>Select Category</b></td><td id='catled'>ghk</td>"+
	        	  "<td colspan='"+x+"'><button type='button' class='btn btn-success' onclick='addCSVLeadData()'><span class='glyphicon glyphicon-plus'></span>&nbsp;&nbsp;&nbsp;Add All Lead</button></td></tr>";
	              $("#CSVTable tr:last").after(myRowFooter);
					var UID=$('#uId').val();
				 		$.ajax({
					  	  url: 'GetCategory',
					  	  type: 'GET',
					  	  data: 'compid='+UID,
					  	  success: function(data) {
					  		var msg=data;
					  		var html="<option value='0'>Select Category</option>";
					  		var i=0;
					  		for(i=0;i<msg.length;i++)
					  			{
					  			var val='"'+msg[i][0]+'"';
					  				html+="<option value="+val+">"+msg[i][1]+"</option>";
					  			}
					  		$('#catled').html("<select class='form-control' id='cat'>"+html+"</select>");
					  	  },
						 error: function(e) {
					 		console.log(e.message);
					 	  }
						} ); 
						});	
}
</script>

</head>
<body>
<form class="form-inline" name="idUploadLogoForm" id="idUploadLogoForm" >
  <div class="form-group">
    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
    <div class="input-group">
      <div class="input-group-addon"><span class="glyphicon glyphicon-paperclip"></span></div>
     <!--  <input type="file" class="form-control" id="csvfiledata"  name="ticket_exampleInputFile"> -->
     <input name="ticket_exampleInputFile" id="ticket_exampleInputFile" type="file"  class="form-control"  />
     
    </div>
  </div>  <!-- onclick="showCSVData()" -->
  <button type="submit" class="btn btn-primary" onclick="uploadFormData()"><span class="glyphicon glyphicon-leaf"></span>&nbsp;&nbsp;&nbsp;Show File Data</button>
</form>
<h1> File Data</h1>
<!-- Paste this code after body tag -->
	<div class="se-pre-con"></div>
	<!-- Ends -->
<div id="">   <input type="checkbox" id="indSYS" value="Bike"> <b>Use Indian Number System(Remove +91 Or 0)</b><br>
<input type="checkbox" id="autodial" value="Bike"> <b>Add Lead To AutoDail</b><br>
</div> 
<div id="CSVTable" ><p style="text-align: center"><b>Select File to Display Data</b></p></div>
</body>
</html>
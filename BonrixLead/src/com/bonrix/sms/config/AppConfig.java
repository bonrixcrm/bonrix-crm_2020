package com.bonrix.sms.config;

import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.GzipResourceResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;

import com.bonrix.sms.job.SMSPullerJob;
import com.bonrix.sms.service.LeadDataExcelBuilder;
import com.bonrix.sms.service.SmsExcelBuilder;
import com.bonrix.sms.service.SmsPdfBuilder;
import com.zaxxer.hikari.HikariDataSource;

@EnableWebMvc
@Configuration
@EnableScheduling
@EnableAsync
// @EnableMongoRepositories
@ComponentScan({ "com.bonrix.sms.*" })
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@Import({ SecurityConfig.class })
public class AppConfig extends WebMvcConfigurerAdapter {

	@Autowired
	Environment env;

	/*@Bean
	public FinanceJob financeJob()
	{
		return new  FinanceJob();
	}*/
	
	 static {
	        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Kolkata"));
	    }
	 
	@Bean
	public SessionFactory sessionFactory() {
		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
		builder.scanPackages("com.bonrix.sms.model").addProperties(getHibernateProperties());

		return builder.buildSessionFactory();
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	private Properties getHibernateProperties() {
		Properties prop = new Properties();
		// prop.put("hibernate.format_sql", "true");
		prop.put("hibernate.show_sql", "false");
		prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		prop.put("hibernate.hbm2ddl.auto", "update");
		prop.put("hibernate.jdbc.batch_size", "100");
		prop.put("hibernate.cache.use_second_level_cache", "true");
		prop.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
		prop.put("hibernate.cache.use_query_cache", "true");
		prop.put("hibernate.connection.useUnicode", "true");
		prop.put("hibernate.connection.charSet", "UTF-8");

		return prop;
	}

	@Bean(name = "dataSource")
	public DataSource dataSource() {

		final HikariDataSource ds = new HikariDataSource();
		ds.setMaximumPoolSize(49);
		ds.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		ds.addDataSourceProperty("url", env.getProperty("jdbc.url"));
		ds.addDataSourceProperty("user", env.getProperty("jdbc.username"));
		ds.addDataSourceProperty("password", env.getProperty("jdbc.password"));
		ds.addDataSourceProperty("cachePrepStmts", true);
		ds.addDataSourceProperty("prepStmtCacheSize", 500);
		ds.addDataSourceProperty("prepStmtCacheSqlLimit", 4096);
		// ds.addDataSourceProperty("useServerPrepStmts", false);
		return ds;
	}

	// <bean id="pdfSMSDownload" class="com.bonrix.service.SmsPdfBuilder" />

	@Bean(name = "pdfSMSDownload")
	public SmsPdfBuilder pdfSMSDownload() {
		return new SmsPdfBuilder();
	}
	
//	@Bean(name = "pdfInvoiceDownload")
//	public InvoicePDFBuilder pdfInvoiceDownload() {
//		return new InvoicePDFBuilder();
//	}

	@Bean(name = "excelSMSDownload")
	public SmsExcelBuilder excelSMSDownload() {
		return new SmsExcelBuilder();
	}
	
	@Bean(name = "excelAllLogDownload")
	public LeadDataExcelBuilder excelAllLogDownload() {
		return new LeadDataExcelBuilder();
	}
	
	
//	
//	@Bean
//	public CRMWebSocket Mytest() {
//		return new CRMWebSocket();
//	}
	/*
	 * @Bean(name="emailsender") public JavaMailSender javaMailService() {
	 * JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
	 * 
	 * javaMailSender.setHost(env.getProperty("smtp.host"));
	 * javaMailSender.setPort(Integer.parseInt(env.getProperty("smtp.port")));
	 * javaMailSender.setUsername(env.getProperty("smtp.username"));
	 * javaMailSender.setPassword(env.getProperty("smtp.password")); //
	 * javaMailSender.setProtocol("smtp");
	 * 
	 * 
	 * javaMailSender.setJavaMailProperties(getMailProperties());
	 * 
	 * return javaMailSender; }
	 * 
	 * 
	 * private Properties getMailProperties() { Properties properties = new
	 * Properties(); // properties.setProperty("mail.transport.protocol",
	 * "smtp"); properties.setProperty("mail.smtp.auth", "true");
	 * properties.setProperty("mail.smtp.starttls.enable", "true");
	 * properties.setProperty("mail.debug", "true");
	 * properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
	 * 
	 * return properties; }
	 */

	/*
	 * @Bean(name="dbConfig") public MyDbConfig getDbConfig(){
	 * 
	 * MyDbConfig dbConf = new MyDbConfig();
	 * dbConf.setDbHost(env.getProperty("db.host.url"));
	 * dbConf.setDbPort(env.getProperty("db.port.number"));
	 * dbConf.setDbService(env.getProperty("db.service.name"));
	 * dbConf.setDbUser(env.getProperty("db.user"));
	 * dbConf.setDbPassword(env.getProperty("db.password")); return dbConf; }
	 */

	@Bean
	public SMSPullerJob SMSPullerJob() {
		return new SMSPullerJob();
	}

	/*@Bean
	public WebSocketClient WebSocketClient() {
		return new WebSocketClient();
	}*/

	/*
	 * @Bean public com.bonrix.sms.job.PerformanceClientMain
	 * PerformanceClientMain() { return new
	 * com.bonrix.sms.job.PerformanceClientMain(); }
	 */

	@Bean
	public HibernateTransactionManager txManager() {
		return new HibernateTransactionManager(sessionFactory());
	}
	/*
	 * @Bean public MongoTemplate mongoTemplate() throws UnknownHostException{
	 * MongoTemplate mongoTemplate =new MongoTemplate(new
	 * MongoClient("127.0.0.1",27017) ,"abc"); return mongoTemplate; }
	 */

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		// viewResolver.set
		//registry.addViewController("/").setViewName("login");
		viewResolver.setOrder(2);
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public ResourceBundleViewResolver viewResolver1() {
		ResourceBundleViewResolver viewResolver = new ResourceBundleViewResolver();

		viewResolver.setOrder(1);
		viewResolver.setBasename("views");
		return viewResolver;
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {

		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("100000MB");
		factory.setMaxRequestSize("100KB");
		return factory.createMultipartConfig();
	}

	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(900000000000L);
		multipartResolver.setDefaultEncoding("UTF-8");
		return multipartResolver;
	}

	/*
	 * @Bean public WebContentInterceptor webContentInterceptor() {
	 * WebContentInterceptor interceptor = new WebContentInterceptor();
	 * interceptor.setCacheSeconds(31556926);
	 * interceptor.setUseExpiresHeader(true);
	 * interceptor.setAlwaysMustRevalidate(false);
	 * interceptor.setUseCacheControlHeader(true);
	 * interceptor.setUseCacheControlNoStore(true);
	 * 
	 * return interceptor; }
	 * 
	 * @Override public void addInterceptors(InterceptorRegistry registry) {
	 * registry.addInterceptor(webContentInterceptor()); }
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("/logs/**").addResourceLocations("/logs/").setCachePeriod(31556926)
		.resourceChain(true).addResolver(new GzipResourceResolver());
		registry.addResourceHandler("/css/**").addResourceLocations("/css/").setCachePeriod(31556926)
				.resourceChain(true).addResolver(new GzipResourceResolver());
		registry.addResourceHandler("/img/**").addResourceLocations("/img/").setCachePeriod(31556926)
				.resourceChain(true).addResolver(new GzipResourceResolver());
		registry.addResourceHandler("/js/**").addResourceLocations("/js/").setCachePeriod(31556926).resourceChain(true)
				.addResolver(new GzipResourceResolver());
		registry.addResourceHandler("/UploadFile/**").addResourceLocations("/UploadFile/");
		registry.addResourceHandler("/Sound/**").addResourceLocations("/Sound/");
		registry.addResourceHandler("/public/**").addResourceLocations("/public/");
		registry.addResourceHandler("/plugins/**").addResourceLocations("/plugins/");
		registry.addResourceHandler("/fonts/**").addResourceLocations("/fonts/").setCachePeriod(31556926)
				.resourceChain(true).addResolver(new GzipResourceResolver());
		registry.addResourceHandler("/Attachments/**").addResourceLocations("/Attachments/").setCachePeriod(31556926);
		registry.addResourceHandler(new String[] { "/WhatsAppMedia/**" }).addResourceLocations(new String[] { "/WhatsAppMedia/" }).setCachePeriod(Integer.valueOf(31556926))
		.resourceChain(true).addResolver(new GzipResourceResolver());
	}

	/*
	 * 
	 * @Bean PersistenceExceptionTranslationPostProcessor persistprocess(){
	 * return new PersistenceExceptionTranslationPostProcessor(); }
	 */
	/*
	 * 
	 * @Bean public MongoTemplate mongoTemplate() throws Exception {
	 * 
	 * Mongo mongo = new Mongo("127.0.0.1", 27017); MongoDbFactory
	 * mongoDbFactory=new SimpleMongoDbFactory(mongo, "test"); MongoTemplate
	 * mongoTemplate = new MongoTemplate(mongoDbFactory);
	 * 
	 * return mongoTemplate;
	 * 
	 * }
	 */

	/*
	 * @Bean JedisConnectionFactory jedisConnectionFactory() { return new
	 * JedisConnectionFactory();
	 * 
	 * }
	 * 
	 * @Bean RedisTemplate< String, Object > redisTemplate() { final
	 * RedisTemplate< String, Object > template = new RedisTemplate< String,
	 * Object >(); template.setConnectionFactory( jedisConnectionFactory() );
	 * template.setKeySerializer( new StringRedisSerializer() );
	 * template.setHashValueSerializer( new GenericToStringSerializer< Object >(
	 * Object.class ) ); template.setValueSerializer( new
	 * GenericToStringSerializer< Object >( Object.class ) ); return template; }
	 */

}

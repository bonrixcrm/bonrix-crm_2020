package com.bonrix.sms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.bonrix.sms.config.core.DDAuthenticationFailedHandler;
import com.bonrix.sms.config.core.DDAuthenticationSuccessHandler;

/**
 * @author Niraj Thakar
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("userDetailsService")

	UserDetailsService userDetailsService;

	/*
	 * @Autowired DataSource dataSource;
	 */

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/reseller/**").access("hasRole('ROLE_RESELLER')").antMatchers("/admin/**")
				.access("hasRole('ROLE_ADMIN')").and().formLogin().loginPage("/login")
				.successHandler(new DDAuthenticationSuccessHandler())
				.failureHandler(new DDAuthenticationFailedHandler()).failureUrl("/login?error")
				.usernameParameter("username").passwordParameter("password").and().sessionManagement()
				.invalidSessionUrl("/login?invalid").and().logout().logoutSuccessUrl("/login?logout")
				.logoutUrl("/j_spring_security_logout").invalidateHttpSession(true).deleteCookies().and()
				.exceptionHandling().accessDeniedPage("/403").and().csrf().disable();
	}

	/*
	 * @Bean public PersistentTokenRepository persistentTokenRepository() {
	 * JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
	 * db.setDataSource(dataSource); return db; }
	 */

	@Bean
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

}
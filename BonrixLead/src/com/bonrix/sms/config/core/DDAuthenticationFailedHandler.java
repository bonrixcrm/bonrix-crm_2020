package com.bonrix.sms.config.core;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.bonrix.sms.model.LoginLog;
import com.bonrix.sms.service.UserService;

public class DDAuthenticationFailedHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException auth) throws IOException {

		UserService calc = ApplicationContextHolder.getContext().getBean(UserService.class);

		LoginLog lg = new LoginLog();

		System.out.println(request.getRemoteAddr());
		lg.setIpaddress(request.getRemoteAddr());
		lg.setLoginon(new Date());
		lg.setLogouton(new Date());
		lg.setNotes(request.getParameter("username") + " Login Failed");
		lg.setUid(0);

		// userService.saveLoginLog(lg);

		calc.saveLoginLog(lg);

		// Authentication auth =
		// SecurityContextHolder.getContext().getAuthentication();
		/*
		 * if(StringUtils.hasText(details.getItemId())) { //TODO sanity and
		 * security check for itemId needed String redirectUrl = "item/" +
		 * details.getItemId(); response.sendRedirect(redirectUrl); }
		 */

		System.out.println("sucess login:::" + new Date());

		if (request.getParameter("type") != null & request.getParameter("type") != "ANDRO") {
			// if ("application/json".equals(request.getHeader("Content-Type")))
			// {
			/*
			 * USED if you want to AVOID redirect to LoginSuccessful.htm in JSON
			 * authentication
			 */
			response.getWriter().print("[{\"responseCode\":\"FAILED\"}]");
			response.getWriter().flush();
		} else {
			response.sendRedirect("");
		}

	}

	// @Override
	// public void onAuthenticationFailure(HttpServletRequest arg0,
	// HttpServletResponse arg1, AuthenticationException arg2)
	// throws IOException, ServletException {
	// // TODO Auto-generated method stub
	//
	// }

	// @Override
	// public void onAuthenticationFailure(HttpServletRequest arg0,
	// HttpServletResponse arg1, AuthenticationException arg2)
	// throws IOException, ServletException {
	// // TODO Auto-generated method stub
	//
	// }

}

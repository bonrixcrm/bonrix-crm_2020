package com.bonrix.sms.config.core;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.LoginLog;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.service.UserService;

public class DDAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {

		UserService calc = ApplicationContextHolder.getContext().getBean(UserService.class);

		MediUser currentUser = (MediUser) auth.getPrincipal();

		LoginLog lg = new LoginLog();

		System.out.println(request.getRemoteAddr());
		lg.setIpaddress(request.getRemoteAddr());
		lg.setLoginon(new Date());
		lg.setLogouton(new Date());
		lg.setNotes(currentUser.getUsername() + " Login Successfully");
		lg.setUid(currentUser.getUserid());

		// userService.saveLoginLog(lg);

		calc.saveLoginLog(lg);
		// Authentication auth =
		// SecurityContextHolder.getContext().getAuthentication();
		/*
		 * if(StringUtils.hasText(details.getItemId())) { //TODO sanity and
		 * security check for itemId needed String redirectUrl = "item/" +
		 * details.getItemId(); response.sendRedirect(redirectUrl); }
		 */

		System.out.println("sucess login:::" + new Date());

		if (request.getParameter("type") != null && request.getParameter("type") != "ANDRO") {
			// if ("application/json".equals(request.getHeader("Content-Type")))
			// {
			/*
			 * USED if you want to AVOID redirect to LoginSuccessful.htm in JSON
			 * authentication
			 */
			System.out.println("inside ANDRO::::::::");
			Long uid = currentUser.getUserid();
			String uuid = UUID.randomUUID().toString();
			System.out.println("uuid = " + uuid);

			ApiKey apiKey = new ApiKey();
			apiKey.setCreateDate(new Date());
			apiKey.setUid(uid);
			apiKey.setIp("*");
			apiKey.setKeyValue(uuid);
			apiKey.setIshidden(true);

			calc.saveApiKey(apiKey);

			response.getWriter().print("[{\"responseCode\":\"SUCCESS\",\"userid\":" + currentUser.getUserid()
					+ ",\"apikey\":\"" + uuid + "\"}]");
			response.getWriter().flush();
		} else {
			response.sendRedirect("./");
		}

	}

}

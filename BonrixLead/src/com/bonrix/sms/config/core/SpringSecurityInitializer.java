package com.bonrix.sms.config.core;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Niraj Thakar
 *
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
package com.bonrix.sms.config.core;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.bonrix.sms.utils.Mac;

public class WebAppInitializer implements WebApplicationInitializer {
	public void onStartup(ServletContext servletContext) throws ServletException {
		
		
		/*
		 * AnnotationConfigWebApplicationContext ctx = new
		 * AnnotationConfigWebApplicationContext();
		 * ctx.register(AppConfig.class); ctx.setServletContext(servletContext);
		 * ctx.refresh();
		 */
		/*
		 * Dynamic dynamic = servletContext.addServlet("dispatcher", new
		 * DispatcherServlet(ctx)); dynamic.addMapping("/");
		 * dynamic.setLoadOnStartup(1);
		 * dynamic.setMultipartConfig(ctx.getBean(MultipartConfigElement.class))
		 * ;
		 */
		/*if(!Mac.checkmac("7c4ec86481054856e987dc02f29a90f9d8972c12")){
			try {
			Runtime.getRuntime().exec("killall -9 java");
			} catch (IOException e) {
			e.printStackTrace();
			}
			return;
			}*/
		FilterRegistration.Dynamic fr = servletContext.addFilter("encodingFilter", new CharacterEncodingFilter());
		fr.setInitParameter("encoding", "UTF-8");
		fr.setInitParameter("forceEncoding", "true");
		fr.addMappingForUrlPatterns(null, true, "/*");
		fr.setInitParameter("", "");
		//fr.addMappingForServletNames(arg0, arg1, arg2);
		System.out.println("init::" + new Date());

		// new Thread(new StartSmppThread()).start();

	}
}

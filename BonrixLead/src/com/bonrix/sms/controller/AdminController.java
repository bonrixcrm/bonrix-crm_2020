package com.bonrix.sms.controller;

import com.bonrix.sms.config.SecurityConfig;
import com.bonrix.sms.config.core.ApplicationContextHolder;
import com.bonrix.sms.controller.AdminController;
import com.bonrix.sms.controller.CSVReader;
import com.bonrix.sms.dao.AdminDao;
import com.bonrix.sms.job.EmailSenderThread;
import com.bonrix.sms.model.AegeCustDetail;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.CategoryManager;
import com.bonrix.sms.model.Contacts;
import com.bonrix.sms.model.Crmemailsetting;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.DisplaySetting;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.ExtraFields;
import com.bonrix.sms.model.GroupName;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.Leadprocesstate;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.MessageAudit;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.MonthlyLogObject;
import com.bonrix.sms.model.PropertyPOJO;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.Sentsmslog;
import com.bonrix.sms.model.ServiceType;
import com.bonrix.sms.model.SmsCredit;
import com.bonrix.sms.model.Smssettings;
import com.bonrix.sms.model.Smstemplate;
import com.bonrix.sms.model.Successstatus;
import com.bonrix.sms.model.SystemNotification;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.Ticketmaster;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.bonrix.sms.utils.StringUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ning.http.client.AsyncHandler;
import com.ning.http.client.AsyncHttpClient;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dom4j.DocumentException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

@Controller
public class AdminController {
  @Autowired
  UserService userService;
  
  @Autowired
  AdminService adminService;
  
  @Autowired
  SMSService smsService;
  
  @Autowired
  AdminDao dao;
  
  @Autowired
  CommonService cservice;
  
  final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  
  final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
  
  final DateFormat readFormat = new SimpleDateFormat("dd-MM-yyyy");
  
  final DateFormat readFormat1 = new SimpleDateFormat("yyyy/MM/dd");
  
  final DateFormat readFormat3 = new SimpleDateFormat("yyyyMMdd");
  
  final DateFormat readMyFormat = new SimpleDateFormat("yyyy-MM-dd");
  
  final DateFormat scdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
  
  MediUser currentUser = null;
  
  @RequestMapping(value = {"/GetAutodialManageReport"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAutodialManageReport(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetAutodialManageReport is Called");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    String query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname), tallycaller.username,DATE_FORMAT(MIN(autodiallog.start_date),'%r'), DATE_FORMAT(MAX(autodiallog.end_date),'%r'),SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(autodiallog.end_date, autodiallog.start_date)))) AS totalhours , (SELECT COUNT(*) FROM followupshistory WHERE followupshistory.tallycallerId=tallycaller.tcallerid AND followupshistory.callStatus='Success' AND DATE(followupshistory.callingTime)=CURDATE()) AS success, (SELECT COUNT(*) FROM followupshistory WHERE followupshistory.tallycallerId=tallycaller.tcallerid AND followupshistory.callStatus='Fail' AND DATE(followupshistory.callingTime)=CURDATE()) AS fail, (SELECT COUNT(*) FROM followupshistory WHERE followupshistory.tallycallerId=tallycaller.tcallerid AND DATE(followupshistory.callingTime)=CURDATE()) AS Total   FROM autodiallog  JOIN tallycaller ON tallycaller.tcallerid=autodiallog.tc_id    \n WHERE autodiallog.comp_id = " + 
      
      Company_id + 
      " AND DATE(autodiallog.start_date)=CURDATE() GROUP BY autodiallog.tc_id;";
    List list = this.cservice.createSqlQuery(query);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/GetAutodialLeadreport"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAutodialLeadreport(HttpServletRequest request, HttpServletResponse response, @RequestParam("tcId") String tcId, @RequestParam("getDate") String getDate) throws ParseException {
    System.out.println("GetAutodialLeadreport is Called");
    String query = "SELECT  tallycaller.username,DATE_FORMAT(MIN(autodiallog.start_date),'%r'),DATE_FORMAT(MAX(autodiallog.end_date),'%r'),SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(autodiallog.end_date, autodiallog.start_date)))) AS totalhours FROM autodiallog  JOIN tallycaller ON tallycaller.tcallerid=autodiallog.tc_id WHERE autodiallog.tc_id = " + 
      
      tcId + " AND DATE(autodiallog.start_date)='" + getDate + "'";
    System.out.println(query);
    List list = this.cservice.createSqlQuery(query);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping({"/AddRemoveAutoDial"})
  @ResponseBody
  public String AddRemoveAutoDial(HttpServletRequest request, HttpServletResponse response, @RequestParam("autiDial") int autiDial) throws ParseException {
    System.out.println("AddRemoveAutoDial is Called.. :: " + autiDial);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("UPDATE lead SET lead.autoDial=" + autiDial + " WHERE lead.companyId=" + Company_id);
    this.cservice.createupdateSqlQuery(
        "UPDATE lead SET lead.autoDial=" + autiDial + " WHERE lead.companyId=" + Company_id);
    return "Success";
  }
  
  @RequestMapping({"/ResetDial"})
  @ResponseBody
  public String ResetDial(HttpServletRequest request, HttpServletResponse response) throws Exception {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    this.cservice.createupdateSqlQuery("UPDATE lead SET lead.dialState=0 WHERE lead.companyId=" + Company_id);
    return "Reset Successfully.";
  }
  
  @RequestMapping({"/ManageAutoDial"})
  @ResponseBody
  public String ManageAutoDial(HttpServletRequest request, HttpServletResponse response, @RequestParam("catId") int catId, @RequestParam("autiDial") int autiDial, @RequestParam("dial") String dial) throws ParseException {
    System.out.println("ManageAutoDial is Called.. :: " + dial);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    if (dial.equalsIgnoreCase("ALL")) {
      System.out.println("UPDATE lead SET lead.autoDial=" + autiDial + " WHERE lead.categoryId=" + catId + 
          " AND lead.companyId=" + Company_id);
      this.cservice.createupdateSqlQuery("UPDATE lead SET lead.autoDial=" + autiDial + " WHERE lead.categoryId=" + 
          catId + " AND lead.companyId=" + Company_id);
    } else {
      System.out.println("UPDATE lead SET lead.autoDial=" + autiDial + " WHERE lead.dialState=" + dial + 
          " AND lead.categoryId=" + catId + " AND lead.companyId=" + Company_id);
      this.cservice.createupdateSqlQuery("UPDATE lead SET lead.autoDial=" + autiDial + " WHERE lead.dialState=" + dial + 
          " AND lead.categoryId=" + catId + " AND lead.companyId=" + Company_id);
    } 
    return "Success";
  }
  
  @RequestMapping(value = {"/GetAutodialManageLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAutodialManageLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("cat") String category, @RequestParam("dial") String dial, @RequestParam("autodial") String autodial) throws ParseException {
    System.out.println("GetAutodialManageLead is Called");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("GetLead   " + Company_id + "  ");
    StringBuilder sb = new StringBuilder();
    StringBuilder ctsb = new StringBuilder();
    ctsb.append(
        "SELECT COUNT(*)  FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0");
    sb.append(
        "SELECT lead.leaadId,concat(lead.firstName , ' ' , lead.lastName) as Name,lead.email,lead.mobileNo,categorymanager.categortName ,lead.leadProcessStatus,lead.createDate,lead.sheduleDate,lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tallyCalletId  FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0  ");
    if (!category.equalsIgnoreCase("0") && !category.isEmpty()) {
      sb.append(" AND lead.categoryId =" + Integer.parseInt(category) + " ");
      ctsb.append(" AND lead.categoryId =" + Integer.parseInt(category) + " ");
    } 
    if (!autodial.isEmpty() && !autodial.equalsIgnoreCase("ALL")) {
      sb.append(" AND lead.autoDial =" + Integer.parseInt(autodial) + " ");
      ctsb.append(" AND lead.autoDial =" + Integer.parseInt(autodial) + " ");
    } 
    if (!dial.equalsIgnoreCase("ALL") && !dial.isEmpty()) {
      sb.append(" AND lead.dialState=" + Integer.parseInt(dial) + " ");
      ctsb.append(" AND lead.dialState =" + Integer.parseInt(dial) + " ");
    } 
    sb.append("AND lead.companyId=" + Company_id + " ");
    ctsb.append(" AND lead.companyId=" + Company_id + " ");
    System.out.println("Query : " + sb);
    System.out.println("Query : " + ctsb);
    String listData = "";
    String query = "";
    String queryCount = "";
    StringBuilder stringBuilder2 = ctsb;
    StringBuilder stringBuilder1 = sb;
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    String mob = "";
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    listData = adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    return listData.toString();
  }
  
  @RequestMapping(value = {"/GetInOutBoundSuccessFailCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetInOutBoundSuccessFailCallLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("callType") String callType, @RequestParam("cat") String catId, @RequestParam("tcId") String tcId, @RequestParam("lSate") String lSate) throws ParseException {
    System.out.println("GetInOutBoundSuccessFailCallLog is Called.. mmmmmmmm  " + callType);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    int i = 0;
    StringBuilder sb = new StringBuilder();
    sb.append(
        "SELECT followupshistory.followupsId,CONCAT(lead.`firstName`,' ', lead.`lastName`),lead.mobileNo,lead.email,   followupshistory.remark,lead.leadProcessStatus,categorymanager.categortName,   DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i'),followupshistory.callDuration,   CONCAT(tallycaller.firstname,tallycaller.lastname),followupshistory.callStatus,followupshistory.audoiFilePath   \t\t\t\t FROM followupshistory INNER JOIN lead ON followupshistory.leadId = lead.leaadId   \t\t\t\t INNER JOIN tallycaller ON tallycaller.tcallerid = followupshistory.tallycallerId   \t\t\t\t INNER JOIN categorymanager ON categorymanager.categotyId = lead.categoryId   \t\t\t\t WHERE followupshistory.companyId = " + 
        
        userId + "  ");
    if (!catId.equalsIgnoreCase("All") && !catId.isEmpty()) {
      i = 1;
      sb.append(" AND lead.categoryId=" + Integer.parseInt(catId));
    } 
    if (!tcId.equalsIgnoreCase("All") && !tcId.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.tallycallerId=" + Integer.parseInt(tcId));
    } 
    if (!callType.equalsIgnoreCase("All") && !callType.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.callStatus='" + callType + "'");
    } 
    if (!lSate.equalsIgnoreCase("All") && !lSate.isEmpty()) {
      i = 1;
      sb.append(" AND lead.leadProcessStatus='" + lSate + "'");
    } 
    System.out.println(sb);
    List list = adminService.GetLeadParameterForSearch("" + sb);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/GetInSuccessStatusCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetInSuccessStatusCallLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("callType") String callType, @RequestParam("cat") String catId, @RequestParam("tcId") String tcId, @RequestParam("stdate") String stdate, @RequestParam("endate") String endate, @RequestParam("lSate") String leadStateId) throws ParseException {
    System.out.println("GetInSuccessStatusCallLog is Called.. mmmmmmmm  " + callType);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    String lSate = "All";
    List<Object[]> leadProState = this.adminService.getLeadProStateById(Integer.parseInt(leadStateId));
    for (Object[] result : leadProState)
      lSate = result[2].toString(); 
    int i = 0;
    StringBuilder sb = new StringBuilder();
    sb.append(
        "SELECT followupshistory.followupsId,CONCAT(lead.`firstName`,' ', lead.`lastName`),lead.mobileNo,lead.email,   followupshistory.remark,lead.leadProcessStatus,categorymanager.categortName,   DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i'),followupshistory.callDuration,   CONCAT(tallycaller.firstname,tallycaller.lastname),followupshistory.callStatus,followupshistory.failstatus,lead.leadState,followupshistory.audoiFilePath,followupshistory.successStatus,lead.leaadId    FROM followupshistory INNER JOIN lead ON followupshistory.leadId = lead.leaadId    INNER JOIN tallycaller ON tallycaller.tcallerid = followupshistory.tallycallerId    INNER JOIN categorymanager ON categorymanager.categotyId = lead.categoryId    WHERE followupshistory.companyId = " + 
        
        userId + 
        "  AND (followupshistory.callingTime BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "',INTERVAL 1 DAY))  ");
    if (!catId.equalsIgnoreCase("All") && !catId.isEmpty()) {
      i = 1;
      sb.append(" AND lead.categoryId=" + Integer.parseInt(catId));
    } 
    if (!tcId.equalsIgnoreCase("All") && !tcId.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.tallycallerId=" + Integer.parseInt(tcId));
    } 
    if (!callType.equalsIgnoreCase("All") && !callType.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.successStatus='" + callType + "'");
    } 
    if (!lSate.equalsIgnoreCase("All") && !lSate.isEmpty()) {
      i = 1;
      sb.append(" AND lead.leadProcessStatus='" + lSate + "'");
    } 
    System.out.println(sb);
    List list = adminService.GetLeadParameterForSearch("" + sb);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping({"/test"})
  @ResponseBody
  public String test(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("test is Called..");
    this.adminService.sstest();
    return "Success";
  }
  
  @RequestMapping({"/addCRMHostSetting"})
  @ResponseBody
  public String addCRMHostSetting(HttpServletRequest request, HttpServletResponse response, @RequestParam("hostName") String hostName, @RequestParam("emailId") String emailId, @RequestParam("password") String password) throws ParseException {
    System.out.println("addCRMHostSetting is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    Crmemailsetting setting = new Crmemailsetting();
    setting.setEmailId(emailId);
    setting.setHostName(hostName);
    setting.setPasswd(password);
    setting.setCompanyId((int)id);
    this.adminService.addCRMHostSetting(setting);
    return "Success";
  }
  
  @RequestMapping({"/deleteCRMHostSetting"})
  @ResponseBody
  public String deleteCRMHostSetting(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") int id) throws ParseException {
    System.out.println("deleteCRMHostSetting is Called..");
    this.adminService.deleteCRMHostSetting(id);
    return "Success";
  }
  
  @RequestMapping({"/updateCRMHostSetting"})
  @ResponseBody
  public String updateCRMHostSetting(HttpServletRequest request, HttpServletResponse response, @RequestParam("hostName") String hostName, @RequestParam("emailId") String emailId, @RequestParam("password") String password, @RequestParam("id") int id) throws ParseException {
    System.out.println("updateCRMHostSetting is Called..");
    this.adminService.updateCRMHostSetting(hostName, emailId, password, id);
    return "Success";
  }
  
  @RequestMapping(value = {"/GenerateInvoicePDF"}, method = {RequestMethod.GET})
  @ResponseBody
  public ModelAndView GenerateInvoicePDF(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Map<String, Object> model = new HashMap<>();
    model.put("pdflList", "");
    return new ModelAndView("pdfInvoiceDownload", model);
  }
  
  @RequestMapping(value = {"/deleteTagToCompany"}, method = {RequestMethod.GET})
  @ResponseBody
  public String deleteTagToCompany(HttpServletRequest request, HttpServletResponse response, @RequestParam("tagId") int tagId) throws ParseException {
    System.out.println("updateTagToCompany is Called..");
    this.adminService.deleteTagToCompany(tagId);
    return "Success";
  }
  
  @RequestMapping(value = {"/updateTagToCompany"}, method = {RequestMethod.GET})
  @ResponseBody
  public String updateTagToCompany(HttpServletRequest request, HttpServletResponse response, @RequestParam("tagName") String tagName, @RequestParam("tagId") int tagId) throws ParseException {
    System.out.println("updateTagToCompany is Called..");
    this.adminService.updateTagToCompany(tagId, tagName);
    return "Success";
  }
  
  @RequestMapping(value = {"/GetTagForCompany"}, method = {RequestMethod.GET})
  @ResponseBody
  public String AddTag(HttpServletRequest request, HttpServletResponse response) {
    System.out.println("GetTagForCompany is Called....");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    System.out.println("User Id " + currentUser.getUserid());
    List list = this.adminService.getAllTag((int)id);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/addTagToLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String addTagToLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("tagList") String tagList, @RequestParam("leadId") int leadId) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    this.adminService.addTagToLead(leadId, tagList);
    return "Success";
  }
  
  @RequestMapping(value = {"/AddTag"}, method = {RequestMethod.GET})
  @ResponseBody
  public String AddTag(HttpServletRequest request, HttpServletResponse response, @RequestParam("tagName") String tagName) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    this.adminService.addTag(companyId, tagName);
    return "Success";
  }
  
  @RequestMapping(value = {"/getAllTag"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getAllTag(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    List list = this.adminService.getAllTag(companyId);
    return list;
  }
  
  @RequestMapping(value = {"/getTag"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getTag(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadId") int leadId) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    System.out.println("getTag is Called..");
    List list = this.adminService.getTag(leadId);
    return list;
  }
  
  @RequestMapping(value = {"/updateLeadComment"}, method = {RequestMethod.GET})
  @ResponseBody
  public String updateLeadComment(HttpServletRequest request, HttpServletResponse response, @RequestParam("comment") String comment, @RequestParam("Id") int leadId) throws ParseException, IOException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    this.adminService.updateLeadComment(comment, leadId);
    return "Success";
  }
  
  @RequestMapping(value = {"/GetIVRWorkDetails"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetIVRWorkDetails(HttpServletRequest request, HttpServletResponse response, @RequestParam("getDate") String getDate, @RequestParam("tcId") int tcId) throws ParseException, IOException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    List<Object[]> list = this.adminService.GetIVRWorkDetails(tcId, getDate);
    List<String[]> ary = (List)new ArrayList<>();
    for (int i = 0; i < list.size(); i++) {
      Object[] data = list.get(i);
      String[] str = { data[0].toString(), data[1].toString(), data[2].toString(), 
          data[3].toString(), data[4].toString(), data[5].toString() };
      ary.add(str);
    } 
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/AssignIVRLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public void AssignIVRLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadId") int leadId, @RequestParam("tcId") int tcId) throws ParseException, IOException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    int id = (int)currentUser.getUserid();
    this.adminService.assignLeadUpdate(Long.valueOf(leadId), Long.valueOf(tcId));
    this.adminService.assignIVRFOllowup(leadId, tcId);
  }
  
  @RequestMapping(value = {"/GetTelecallerDateWiseDetails"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTelecallerDateWiseDetails(HttpServletRequest request, HttpServletResponse response, @RequestParam("getDate") String getDate, @RequestParam("tcId") int tcId) throws ParseException, IOException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    int id = (int)currentUser.getUserid();
    List list = this.adminService.GetTelecallerDateWiseDetails(id, tcId, getDate);
    List<String[]> ary = (List)new ArrayList<>();
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/GetTelecallerWorkDetails"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTelecallerWorkDetails(HttpServletRequest request, HttpServletResponse response, @RequestParam("getDate") String getDate, @RequestParam("tcId") int tcId) throws ParseException, IOException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    List<Object[]> list = this.adminService.GetTelecallerWorkDetails(tcId, getDate);
    List<String[]> ary = (List)new ArrayList<>();
    if (list.size() != 0)
      for (int i = 0; i < list.size(); i++) {
        Object[] data = list.get(i);
        String[] str = { data[0].toString(), data[1].toString(), data[2].toString(), data[3].toString(), data[4].toString() };
        ary.add(str);
      }  
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(ary);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/getTelecallerFollowupReport"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getTelecallerFollowupReport(HttpServletRequest request, HttpServletResponse response, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    JSONArray jarray = new JSONArray();
    JSONObject responseDetailsJson = new JSONObject();
    JSONArray jsonArray = new JSONArray();
    List<Object[]> list = this.adminService.getTelecallerFollowupReport(companyId, startDate, endDate);
    for (int i = 0; i < list.size(); i++) {
      JSONObject leadmap = new JSONObject();
      Object[] result = list.get(i);
      leadmap.put("username", result[0]);
      leadmap.put("successAVG", result[1]);
      leadmap.put("failAVG", result[2]);
      leadmap.put("OverAccessTime", result[3]);
      jarray.add(leadmap);
    } 
    responseDetailsJson.put("data", jarray);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return responseDetailsJson.toString();
  }
  
  @RequestMapping(value = {"/exportLeads"}, method = {RequestMethod.GET})
  @ResponseBody
  public ModelAndView exportLeads(HttpServletRequest request, HttpServletResponse response, @RequestParam("lId") String ledId, @RequestParam("format") String format, @RequestParam("flag") String flag) throws ParseException, DocumentException, IOException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    if (format.equals("pdf") && flag.equals("All")) {
      List listq = null;
      if (Company_id == 273) {
        listq = this.adminService.GetLeadParameterForSearch(
            "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country, lead.companyName,lead.website,lead.csvData,'Not Set'  FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId  WHERE   lead.companyId=" + 
            
            Company_id);
      } else {
        listq = this.adminService.GetLeadParameterForSearch(
            "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country, lead.companyName,lead.website,lead.csvData,tallycaller.username,(SELECT remark FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS remark ,\n\t(SELECT callStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callStatus ,\n\t(SELECT successStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS successStatus ,\n\t(SELECT failStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS failStatus ,\n\t(SELECT callingTime FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callingTime ,\n\t(SELECT callDuration FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callDuration   FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId  INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE   lead.companyId=" + 
            
            Company_id);
      } 
      System.out.println("listq::" + listq);
      Map<String, Object> model = new HashMap<>();
      model.put("pdflList", listq);
      return new ModelAndView("pdfSMSDownload", model);
    } 
    if (format.equals("pdf") && flag.equals("Selected")) {
      String IN = "";
      byte b;
      int i;
      String[] arrayOfString;
      for (i = (arrayOfString = ledId.split(",")).length, b = 0; b < i; ) {
        String retval = arrayOfString[b];
        IN = String.valueOf(IN) + retval + ",";
        b++;
      } 
      IN = IN.substring(0, IN.length() - 1);
      List listq = null;
      if (Company_id == 273) {
        listq = this.adminService.GetLeadParameterForSearch(
            "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country, lead.companyName,lead.website,lead.csvData,'Not Set'  FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId   WHERE lead.leaadId In (" + 
            
            IN + ") AND   lead.companyId=" + Company_id);
      } else {
        listq = this.adminService.GetLeadParameterForSearch(
            "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country, lead.companyName,lead.website,lead.csvData,tallycaller.username,(SELECT remark FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS remark ,\n\t(SELECT callStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callStatus ,\n\t(SELECT successStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS successStatus ,\n\t(SELECT failStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS failStatus ,\n\t(SELECT callingTime FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callingTime ,\n\t(SELECT callDuration FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callDuration    FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId  INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE lead.leaadId In (" + 
            
            IN + ") AND   lead.companyId=" + Company_id);
      } 
      System.out.println("listq::" + listq);
      Map<String, Object> model = new HashMap<>();
      model.put("pdflList", listq);
      return new ModelAndView("pdfSMSDownload", model);
    } 
    if (format.equals("xls") && flag.equals("All")) {
      List listq = null;
      if (Company_id == 273) {
        listq = this.adminService.GetLeadParameterForSearch(
            "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country, lead.companyName,lead.website,lead.csvData,'Not Set'  FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId  WHERE   lead.companyId=" + 
            Company_id);
      } else {
    	
        listq = this.adminService.GetLeadParameterForSearch(
            "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country, lead.companyName,lead.website,lead.csvData,tallycaller.username,(SELECT remark FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS remark ,\n\t(SELECT callStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callStatus ,\n\t(SELECT successStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS successStatus ,\n\t(SELECT failStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS failStatus ,\n\t(SELECT callingTime FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callingTime ,\n\t(SELECT callDuration FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callDuration    FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId  INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE   lead.companyId=" + 
            
            Company_id);
      } 
      System.out.println("listq::" + listq);
      Map<String, Object> model = new HashMap<>();
      model.put("excelList", listq);
      model.put("type","All");
      return new ModelAndView("excelSMSDownload", model);
    } 
    if (format.equals("xls") && flag.equals("Selected")) {
      String IN = "";
      byte b;
      int i;
      String[] arrayOfString;
      for (i = (arrayOfString = ledId.split(",")).length, b = 0; b < i; ) {
        String retval = arrayOfString[b];
        IN = String.valueOf(IN) + retval + ",";
        b++;
      } 
      IN = IN.substring(0, IN.length() - 1);
      List listq = this.adminService.GetLeadParameterForSearch(
          "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country, lead.companyName,lead.website,lead.csvData,tallycaller.username,(SELECT remark FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS remark ,\n\t(SELECT callStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callStatus ,\n\t(SELECT successStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS successStatus ,\n\t(SELECT failStatus FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS failStatus ,\n\t(SELECT callingTime FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callingTime ,\n\t(SELECT callDuration FROM followupshistory WHERE followupshistory.`leadId`=lead.leaadId ORDER BY followupshistory.`callingTime` DESC LIMIT 1) AS callDuration   FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId  INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE lead.leaadId IN (" + 
          
          IN + ") AND   lead.companyId=" + Company_id);
      Map<String, Object> model = new HashMap<>();
      model.put("excelList", listq);
      model.put("type","All");
      return new ModelAndView("excelSMSDownload", model);
    } 
    return null;
  }
  
  
  @RequestMapping(value = {"/exportFollowsUp"}, method = {RequestMethod.GET})
  @ResponseBody
  public ModelAndView exportFollowsUp(HttpServletRequest request, HttpServletResponse response) throws ParseException, DocumentException, IOException,NullPointerException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    
   
    
   List leadId = this.adminService.GetLeadParameterForSearch("SELECT lead.leaadId from  lead where lead.companyId="+ Company_id);

   List listq = new ArrayList();
    List data=null;
   for(int i=0; i<leadId.size(); i++) {
          data=this.adminService.GetLeadParameterForSearch("SELECT\r\n" + 
          		"    `LEAD`.firstName,\r\n" + 
          		"    `LEAD`.lastName,\r\n" + 
          		"    `LEAD`.email,\r\n" + 
          		"    `LEAD`.leadProcessStatus,\r\n" + 
          		"    `LEAD`.leadState,\r\n" + 
          		"    `LEAD`.mobileNo,\r\n" + 
          		"    `LEAD`.CompanyName,\r\n" + 
          		"    CASE \n" + 
        		"        WHEN lead.sheduleDate = '1980-06-30 17:36:00' THEN 'NOT SET'\n" + 
        		"        ELSE lead.sheduleDate\n" + 
        		"    END AS sheduleDate,\n" + 
          		"    tallycaller.username,\r\n" + 
          		"    followupshistory.callingTime,\r\n" + 
          		"    followupshistory.newStatus,\r\n" + 
          		"    followupshistory.callDuration,\r\n" + 
          		"    followupshistory.callStatus,\r\n" + 
          		"    followupshistory.followsUpType,\r\n" + 
          		"    followupshistory.followupDevice,\r\n" + 
          		"    followupshistory.failstatus \r\n" + 
          		"FROM \r\n" + 
          		"    `LEAD` \r\n" + 
          		"INNER JOIN \r\n" + 
          		"    followupshistory \r\n" + 
          		"    ON lead.leaadId=followupshistory.leadId  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid\r\n" + 
          		"WHERE \r\n" + 
          		"    followupshistory.leadId="+leadId.get(i));
         
          listq.addAll(data);
   }
    
    System.out.println("total List"+listq.size());
    
   
    
    Map<String, Object> model = new HashMap<>();
    model.put("excelList", listq);
    model.put("type", "followUpData");
   
    return new ModelAndView("excelSMSDownload", model);
  }

  
  
  @RequestMapping(value = {"/transfreLeadsToNewCat"}, method = {RequestMethod.GET})
  @ResponseBody
  public String transfreLeadsToNewCat(HttpServletRequest request, HttpServletResponse response, @RequestParam("lId") int lId, @RequestParam("tcat") String tcat) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    Lead getlead = new Lead();
    getlead = this.adminService.GetLeadObjectByLeadId(lId);
    Lead lead = new Lead();
    lead.setcategoryId(tcat);
    lead.setCompanyId((int)Company_id);
    lead.setCreateDate(new Date());
    lead.setemail(getlead.getEmail());
    lead.setFirstName(getlead.getFirstName());
    lead.setLastName(getlead.getLastName());
    lead.setLeadProcessStatus("None");
    lead.setMobileNo(getlead.getMobileNo());
    lead.setLeadState("Open");
    lead.setSheduleDate(this.df.parse("1980-06-30 17:36:00"));
    lead.setTallyCalletId(getlead.getTallyCalletId());
    lead.setCsvData(getlead.getCsvData());
    lead.setLeadType(getlead.getLeadType());
    lead.setCountry(getlead.getCountry());
    lead.setState(getlead.getState());
    lead.setCity(getlead.getCity());
    lead.setCompanyName(getlead.getCompanyName());
    lead.setWebsite(getlead.getWebsite());
    this.adminService.AddLead(lead);
    return "Success : " + lead.getLeaadId();
  }
  
  @RequestMapping(value = {"/reOPenLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String reOPenLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("lId") String lId) throws ParseException {
    String query1 = "UPDATE lead SET lead.leadState='Open' WHERE lead.leaadId=" + lId;
    this.adminService.updateMasterPassword(query1);
    return "Success";
  }
  
  @RequestMapping(value = {"/transferLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String transferLead(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int ledId = Integer.parseInt(request.getParameter("lId"));
    int catId = Integer.parseInt(request.getParameter("catId"));
    this.adminService.updateMasterPassword("UPDATE lead SET lead.categoryId=" + catId + " WHERE lead.leaadId=" + ledId + 
        " AND lead.companyId=" + Company_id);
    return "Success";
  }
  
  @RequestMapping(value = {"/GetLeadByLeadId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetLeadByLeadId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int ledId = Integer.parseInt(request.getParameter("lId"));
    List list = this.adminService.GetLeadParameterForSearch(
        "SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo, lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,CONCAT(lead.city,',',lead.state,',',lead.Country),  CONCAT(lead.companyName,'(',lead.website,')'),lead.csvData FROM lead  INNER JOIN  categorymanager  ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId   WHERE lead.leaadId=" + 
        
        ledId + " AND lead.companyId=" + Company_id);
    return list;
  }
  
  @RequestMapping(value = {"/GetLeadParameterForAdvanceSearch"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadParameterForAdvanceSearch(HttpServletRequest request, HttpServletResponse response, @RequestParam("assign") String assinval, @RequestParam("lstate") String lstate, @RequestParam("lpstate") String lpstate, @RequestParam("tcaller") String tcaller, @RequestParam("comatag") String comatag, @RequestParam("cat") String category) throws ParseException {
    String orderBy = request.getParameter("orderBy");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int i = 0;
    StringBuilder sb = new StringBuilder();
    StringBuilder queryCount = new StringBuilder();
    JSONArray jarray = new JSONArray();
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    String mob = "";
    Date fdate = null;
    Date todate = null;
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    if (Company_id == 273) {
      sb.append(
          "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName ,lead.leadProcessStatus,lead.leadState,'Not Set',lead.tagName FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0  ");
      queryCount.append("SELECT count(*) FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0");
    } else {
      sb.append(
          "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName ,lead.leadProcessStatus,lead.leadState,CONCAT(tallycaller.firstname,' ' ,tallycaller.lastname),lead.tagName FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON  lead.tallyCalletId=tallycaller.tcallerid WHERE   0=0  ");
      queryCount.append("SELECT count(*) FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0");
    } 
    if (assinval.equalsIgnoreCase("All") && !assinval.isEmpty()) {
      sb.append(" AND lead.companyId=" + Company_id + " ");
      queryCount.append(" AND lead.companyId=" + Company_id + " ");
    } 
    if (assinval.equalsIgnoreCase("Assign") && !assinval.isEmpty()) {
      sb.append(" AND lead.tallyCalletId !=0  ");
      queryCount.append(" AND lead.tallyCalletId !=0  ");
    } 
    if (assinval.equalsIgnoreCase("Unassigned") && !assinval.isEmpty()) {
      sb.append(" AND lead.tallyCalletId =0  ");
      queryCount.append(" AND lead.tallyCalletId =0  ");
    } 
    if (!category.equalsIgnoreCase("0") && !category.isEmpty()) {
      sb.append(" AND lead.categoryId =" + Integer.parseInt(category) + "  ");
      queryCount.append(" AND lead.categoryId =" + Integer.parseInt(category) + "  ");
    } 
    if (!lpstate.equalsIgnoreCase("Lead Process") && !lpstate.isEmpty()) {
      sb.append(" AND lead.leadProcessStatus ='" + lpstate + "'  ");
      queryCount.append(" AND lead.leadProcessStatus ='" + lpstate + "'  ");
    } 
    if (!tcaller.equalsIgnoreCase("0") && !tcaller.isEmpty()) {
      sb.append(" AND lead.tallyCalletId =" + Integer.parseInt(tcaller) + "  ");
      queryCount.append(" AND lead.tallyCalletId =" + Integer.parseInt(tcaller) + "  ");
    } 
    if (!lstate.equalsIgnoreCase("All") && !lstate.isEmpty()) {
      i = 1;
      sb.append(" AND lead.leadState ='" + lstate + "' AND lead.companyId=" + Company_id + " ");
      queryCount.append(" AND lead.leadState ='" + lstate + "' AND lead.companyId=" + Company_id + " ");
    } 
    if (comatag.length() != 0) {
      i = 1;
      int c = 0;
      String finalcoma = comatag.replaceAll("\"", "");
      finalcoma.trim();
      String[] tags = finalcoma.split(",");
      for (c = 0; c < tags.length; c++) {
        if (c == 0)
          sb.append(" AND ( lead.tagName LIKE '%" + tags[c] + "%' "); 
        if (c == tags.length - 1) {
          sb.append(" OR lead.tagName LIKE '%" + tags[c] + "%' )");
        } else {
          sb.append(" OR  lead.tagName LIKE '%" + tags[c] + "%' ");
        } 
      } 
    } 
    System.out.println("Query : " + sb);
    String list = "";
    list = this.adminService.ConvertDataToWebJSON(queryCount.toString(), sb.toString(), page, listSize);
    if (list.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + list + ");"; 
      return list;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"/GetLeadParameterForAdvanceSearchData"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadParameterForAdvanceSearchData(HttpServletRequest request, HttpServletResponse response, @RequestParam("assign") String assinval, @RequestParam("lstate") String lstate, @RequestParam("lpstate") String lpstate, @RequestParam("tcaller") String tcaller, @RequestParam("comatag") String comatag, @RequestParam("cat") String category) throws ParseException {
    String name = request.getParameter("name");
    String mobileNo = request.getParameter("mobileNo");
    String email = request.getParameter("email");
    String tallycallerName = request.getParameter("tallycallerName");
    String orderBy = request.getParameter("orderBy");
    String catNme = request.getParameter("catNme");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int i = 0;
    StringBuilder sb = new StringBuilder();
    StringBuilder queryCount = new StringBuilder();
    JSONArray jarray = new JSONArray();
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    String mob = "";
    Date fdate = null;
    Date todate = null;
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    if (Company_id == 273) {
      sb.append(
          "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName ,lead.leadProcessStatus,lead.leadState,'Not Set',lead.tagName FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0  ");
      queryCount.append("SELECT count(*) FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0");
    } else {
      sb.append(
          "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName ,lead.leadProcessStatus,lead.leadState,CONCAT(tallycaller.firstname,' ' ,tallycaller.lastname),lead.tagName FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON  lead.tallyCalletId=tallycaller.tcallerid WHERE   0=0  ");
      queryCount.append("SELECT count(*) FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0");
    } 
    if (assinval.equalsIgnoreCase("All") && !assinval.isEmpty()) {
      sb.append(" AND lead.companyId=" + Company_id + " ");
      queryCount.append(" AND lead.companyId=" + Company_id + " ");
    } 
    if (assinval.equalsIgnoreCase("Assign") && !assinval.isEmpty()) {
      sb.append(" AND lead.tallyCalletId !=0  ");
      queryCount.append(" AND lead.tallyCalletId !=0  ");
    } 
    if (assinval.equalsIgnoreCase("Unassigned") && !assinval.isEmpty()) {
      sb.append(" AND lead.tallyCalletId =0  ");
      queryCount.append(" AND lead.tallyCalletId =0  ");
    } 
    if (!category.equalsIgnoreCase("0") && !category.isEmpty()) {
      sb.append(" AND lead.categoryId =" + Integer.parseInt(category) + "  ");
      queryCount.append(" AND lead.categoryId =" + Integer.parseInt(category) + "  ");
    } 
    if (!lpstate.equalsIgnoreCase("Lead Process") && !lpstate.isEmpty()) {
      sb.append(" AND lead.leadProcessStatus ='" + lpstate + "'  ");
      queryCount.append(" AND lead.leadProcessStatus ='" + lpstate + "'  ");
    } 
    if (!tcaller.equalsIgnoreCase("0") && !tcaller.isEmpty()) {
      sb.append(" AND lead.tallyCalletId =" + Integer.parseInt(tcaller) + "  ");
      queryCount.append(" AND lead.tallyCalletId =" + Integer.parseInt(tcaller) + "  ");
    } 
    if (!lstate.equalsIgnoreCase("All") && !lstate.isEmpty()) {
      i = 1;
      sb.append(" AND lead.leadState ='" + lstate + "' AND lead.companyId=" + Company_id + " ");
      queryCount.append(" AND lead.leadState ='" + lstate + "' AND lead.companyId=" + Company_id + " ");
    } 
    sb.append(" AND ( ");
    if (!name.equalsIgnoreCase("NA") || !name.equalsIgnoreCase("undefined") || !name.equalsIgnoreCase("null")) {
      i++;
      sb.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
    } 
    if (mobileNo != null && !mobileNo.equalsIgnoreCase("NA") && !mobileNo.equalsIgnoreCase("undefined") && !mobileNo.equalsIgnoreCase("null")) {
      i++;
      sb.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
    } 
    if (email != null && !email.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("undefined") && !email.equalsIgnoreCase("null")) {
      i++;
      sb.append(" OR lead.email LIKE '%" + email + "%' ");
    } 
    if (catNme != null && !catNme.equalsIgnoreCase("NA") && !catNme.equalsIgnoreCase("undefined") && !catNme.equalsIgnoreCase("null")) {
      i++;
      sb.append("  OR categorymanager.categortName LIKE '%" + catNme + "%' ");
    } 
    sb.append(" ) ");
    if (comatag.length() != 0) {
      i = 1;
      int c = 0;
      String finalcoma = comatag.replaceAll("\"", "");
      finalcoma.trim();
      String[] tags = finalcoma.split(",");
      for (c = 0; c < tags.length; c++) {
        if (c == 0)
          sb.append(" AND ( lead.tagName LIKE '%" + tags[c] + "%' "); 
        if (c == tags.length - 1) {
          sb.append(" OR lead.tagName LIKE '%" + tags[c] + "%' )");
        } else {
          sb.append(" OR  lead.tagName LIKE '%" + tags[c] + "%' ");
        } 
      } 
    } 
    System.out.println("Query : " + sb);
    String list = "";
    list = this.adminService.ConvertDataToWebJSON(queryCount.toString(), sb.toString(), page, listSize);
    if (list.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + list + ");"; 
      return list;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"/DeleteLeadHistoryByLeadId"}, method = {RequestMethod.GET})
  @ResponseBody
  public String DeleteLeadHistoryByLeadId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int ledId = Integer.parseInt(request.getParameter("lId"));
	List followuplist = adminService.GetLeadByCategoryId(
			"SELECT followupshistory.followupsId,followupshistory.audoiFilePath,followupshistory.leadId FROM followupshistory WHERE followupshistory.companyId="
					+ Integer.parseInt("" + Company_id) + " AND followupshistory.leadId=" + ledId + "");
    if (followuplist.size() != 0) {
      for (int j = 0; j < followuplist.size(); j++) {
    		Object[] followup = (Object[]) followuplist.get(j);
        System.out.println("FollowUp Is : " + followup[0] + "  Audio Path : " + followup[1]);
        try {
          String filePath = request.getServletContext().getRealPath("/");
          filePath = String.valueOf(filePath) + "\\Sound\\" + followup[1];
          System.out.println(filePath);
          boolean success = (new File(filePath)).delete();
          String query = "DELETE FROM followupshistory WHERE followupshistory.leadId=" + ledId;
          this.adminService.updateMasterPassword(query);
          String query1 = "DELETE FROM lead WHERE lead.leaadId=" + ledId;
          this.adminService.updateMasterPassword(query1);
        } catch (Exception e) {
          System.out.println("exception occoured" + e);
          System.out.println("File does not exist or you are trying to read a file that has been deleted");
        } 
      } 
    } else {
      String query1 = "DELETE FROM lead WHERE lead.leaadId=" + ledId;
      this.adminService.updateMasterPassword(query1);
    } 
    return "Success";
  }
  
  @RequestMapping(value = {"/DeleteLeadHistory"}, method = {RequestMethod.GET})
  @ResponseBody
  public String DeleteLeadHistory(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int catId = Integer.parseInt(request.getParameter("catId"));
    List<Object[]> list = this.adminService
      .GetLeadByCategoryId("SELECT lead.leaadId,lead.categoryId FROM lead WHERE lead.companyId=" + Company_id + 
        " AND lead.categoryId='" + catId + "'");
    if (list.size() != 0) {
      for (int i = 0; i < list.size(); i++) {
        Object[] result = list.get(i);
        int ledId = Integer.parseInt(request.getParameter("lId"));

        List followuplist = adminService.GetLeadByCategoryId(
				"SELECT followupshistory.followupsId,followupshistory.audoiFilePath,followupshistory.leadId FROM followupshistory WHERE followupshistory.companyId="
						+ Integer.parseInt("" + Company_id) + " AND followupshistory.leadId=" + ledId + "");
        if (followuplist.size() != 0) {
          for (int j = 0; j < followuplist.size(); j++) {
        	  Object[] followup = (Object[]) followuplist.get(j);
            try {
              String filePath = request.getServletContext().getRealPath("/");
              filePath = String.valueOf(filePath) + "\\Sound\\" + followup[1];
              boolean success = (new File(filePath)).delete();
              String query = "DELETE FROM followupshistory WHERE followupshistory.leadId=" + 
                Integer.parseInt((String)followup[2]);
              this.adminService.updateMasterPassword(query);
              String query1 = "DELETE FROM lead WHERE lead.leaadId=" + Integer.parseInt((String)followup[2]);
              this.adminService.updateMasterPassword(query1);
              String query2 = "DELETE FROM categorymanager WHERE categorymanager.categotyId=" + catId + 
                " AND categorymanager.companyId=" + Company_id;
              this.adminService.updateMasterPassword(query2);
            } catch (Exception e) {
              System.out.println("exception occoured" + e);
              System.out.println("File does not exist or you are trying to read a file that has been deleted");
            } 
          } 
        } else {
          String query1 = "DELETE FROM lead WHERE lead.leaadId=" + Integer.parseInt((String)result[0]);
          this.adminService.updateMasterPassword(query1);
          String query2 = "DELETE FROM categorymanager WHERE categorymanager.categotyId=" + catId + 
            " AND categorymanager.companyId=" + Company_id;
          this.adminService.updateMasterPassword(query2);
        } 
      } 
    } else {
      String query2 = "DELETE FROM categorymanager WHERE categorymanager.categotyId=" + catId + 
        " AND categorymanager.companyId=" + Company_id;
      this.adminService.updateMasterPassword(query2);
    } 
    return "Fail";
  }
  
  @RequestMapping(value = {"/GenerateMasterPassword"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GenerateMasterPassword(HttpServletRequest request, HttpServletResponse response, @RequestParam("mstPass") String mstPass, @RequestParam("OldmstPass") String OldmstPass, @RequestParam("status") String status) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int cont = 0;
    User user = new User();
    user = this.adminService.GetUserData(Company_id);
    String mst_Pass = user.getMasterPassword();
    if (status.equals("GEN_PASS")) {
      String MstPass = null;
      MstPass = user.getMasterPassword();
      if (MstPass != null)
        return "Success"; 
      return "Fail";
    } 
    if (status.equals("RE_GEN_MST_PASS")) {
      String oldPass = user.getMasterPassword();
      if (oldPass.equals(OldmstPass)) {
        String query = "UPDATE users SET users.masterPassword='" + mstPass + "' WHERE users.uid=" + Company_id;
        this.adminService.updateMasterPassword(query);
        cont = 1;
        return "Transaction Password Successfully Changed.";
      } 
      return "Invalid Old Transaction Password.";
    } 
    if (status.equals("GEN_MST_PASS")) {
      String query = "UPDATE users SET users.masterPassword='" + mstPass + "' WHERE users.uid=" + Company_id;
      this.adminService.updateMasterPassword(query);
      cont = 1;
      return "Transaction Password Successfully Generated.";
    } 
    if (cont == 1)
      return "Success111"; 
    return "Fail";
  }
  
  @RequestMapping(value = {"/GetUserData"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetUserData(HttpServletRequest request, HttpServletResponse response, @RequestParam("mstPass") String mstPass) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    User user = new User();
    user = this.adminService.GetUserData(Company_id);
    String mst_Pass = user.getMasterPassword();
    if (mst_Pass.equals(mstPass))
      return "Success"; 
    return "Fail";
  }
  
  @RequestMapping(value = {"/GetLeadParameterForSearch"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadParameterForSearch(HttpServletRequest request, HttpServletResponse response, @RequestParam("assign") String assinval, @RequestParam("cat") String category, @RequestParam("tel") String telecaller, @RequestParam("dueDate") String dueDate, @RequestParam("leadstate") String leadstate) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int i = 0;
    StringBuilder sb = new StringBuilder();
    StringBuilder ctsb = new StringBuilder();
    ctsb.append(
        "SELECT COUNT(*)  FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId LEFT JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid WHERE   0=0");
    sb.append(
        "SELECT lead.leaadId,concat(lead.firstName , ' ' , lead.lastName) as Name,lead.email,lead.mobileNo,categorymanager.categortName ,lead.leadProcessStatus,tallycaller.username,lead.createDate,lead.sheduleDate,lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tallyCalletId  FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId LEFT JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE   0=0  ");
    if (!assinval.equalsIgnoreCase("All") && !assinval.isEmpty()) {
      i = 1;
      sb.append(" AND lead.companyId=" + Company_id);
      ctsb.append(" AND lead.companyId=" + Company_id);
    } 
    if (assinval.equalsIgnoreCase("Assign") && !assinval.isEmpty()) {
      i = 1;
      sb.append(" AND lead.tallyCalletId !=0 ");
      ctsb.append(" AND lead.tallyCalletId !=0 ");
    } 
    if (assinval.equalsIgnoreCase("Unassigned") && !assinval.isEmpty()) {
      sb.append(" AND lead.tallyCalletId =0 ");
      ctsb.append(" AND lead.tallyCalletId =0 ");
    } 
    if (!category.equalsIgnoreCase("0") && !category.isEmpty()) {
      sb.append(" AND lead.categoryId =" + Integer.parseInt(category) + " ");
      ctsb.append(" AND lead.categoryId =" + Integer.parseInt(category) + " ");
    } 
    if (!telecaller.equalsIgnoreCase("0") && !telecaller.isEmpty()) {
      sb.append(" AND lead.tallyCalletId =" + Integer.parseInt(telecaller) + " ");
      ctsb.append(" AND lead.tallyCalletId =" + Integer.parseInt(telecaller) + " ");
    } 
    if (!dueDate.equalsIgnoreCase("") && !dueDate.isEmpty()) {
      sb.append(" AND DATE(lead.createDate)='" + dueDate + "'");
      ctsb.append(" AND DATE(lead.createDate)='" + dueDate + "'");
    } 
    if (!leadstate.equalsIgnoreCase("0") && !leadstate.isEmpty()) {
      sb.append(" AND lead.leadProcessStatus ='" + leadstate + "' ");
      ctsb.append(" AND lead.leadProcessStatus ='" + leadstate + "' ");
    } 
    sb.append("AND lead.companyId=" + Company_id + " AND lead.leadState='Open'");
    ctsb.append(" AND lead.companyId=" + Company_id + " AND lead.leadState='Open' ");
    String listData = "";
    String query = "";
    String queryCount = "";
    StringBuilder stringBuilder2 = ctsb;
    StringBuilder stringBuilder1 = sb;
    int page = 1;
    int listSize = 0, start = 0, len = 10;
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("query:::" + stringBuilder1);
    listData = adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    return listData.toString();
  }
  
  @RequestMapping(value = {"/AddSuccessState"}, method = {RequestMethod.GET})
  @ResponseBody
  public void AddSuccessState(HttpServletRequest request, HttpServletResponse response, @RequestParam("catname") String stateName, @RequestParam("compid") int CompId, @RequestParam("leadStateId") int leadStateId) throws ParseException {
    Successstatus sstatus = new Successstatus();
    sstatus.setCompId(CompId);
    sstatus.setStatusName(stateName);
    sstatus.setLeadStateId(leadStateId);
    this.adminService.AddSuccessState(stateName, CompId, leadStateId);
  }
  
  @RequestMapping(value = {"/deleteSuccessStae"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deleteSuccessStae(HttpServletRequest request, HttpServletResponse response, @RequestParam("catid") Long statusId) throws ParseException {
    this.adminService.deleteSuccessStae(statusId);
  }
  
  @RequestMapping(value = {"/UpdateSuccessState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String UpdateSuccessState(HttpServletRequest request, HttpServletResponse response, @RequestParam("catid") int ststeId, @RequestParam("catname") String stateName, @RequestParam("compid") int CompId, @RequestParam("leadStatId") int leadStatId) throws ParseException {
    Successstatus sstatus = new Successstatus();
    sstatus.setCompId(CompId);
    sstatus.setStatusName(stateName);
    sstatus.setId(ststeId);
    sstatus.setLeadStateId(leadStatId);
    this.adminService.UpdateSuccessState(ststeId, stateName, leadStatId);
    return "1";
  }
  
  @RequestMapping(value = {"/GetSuccessStatus"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetSuccessStatus(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    List list = this.adminService.GetSuccessStatus(id);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/GetLeadByAdvanceSearch"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadByAdvanceSearch(HttpServletRequest request, HttpServletResponse response, @RequestParam("cat") String category, @RequestParam("assign") String assinval, @RequestParam("telecaller") String tcId, @RequestParam("lstate") String lState) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    int i = 0;
    StringBuilder sb = new StringBuilder();
    sb.append(
        "SELECT lead.leaadId,concat(lead.firstName , ' ' , lead.lastName) as Name,lead.email,lead.mobileNo,categorymanager.categortName ,lead.leadProcessStatus,lead.createDate,lead.sheduleDate,lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tallyCalletId  FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE   0=0 ");
    if (!tcId.equalsIgnoreCase("Telecaller") && !tcId.isEmpty())
      sb.append(" AND lead.tallyCalletId =" + Integer.parseInt(tcId) + " "); 
    if (!assinval.equalsIgnoreCase("All") && !assinval.isEmpty()) {
      i = 1;
      sb.append(" AND lead.companyId=" + Company_id);
    } 
    if (assinval.equalsIgnoreCase("Assign") && !assinval.isEmpty()) {
      i = 1;
      sb.append(" AND lead.tallyCalletId !=0 ");
    } 
    if (assinval.equalsIgnoreCase("Unassigned") && !assinval.isEmpty())
      sb.append(" AND lead.tallyCalletId =0 "); 
    if (!category.equalsIgnoreCase("category") && !category.isEmpty())
      sb.append(" AND lead.categoryId =" + Integer.parseInt(category) + " "); 
    if (!lState.equalsIgnoreCase("Lstate") && !lState.isEmpty())
      sb.append(" AND lead.leadProcessStatus ='" + lState + "' "); 
    if (i != 1)
      sb.append("AND lead.companyId=" + Company_id); 
    System.out.println("Query : " + sb);
    List list = this.adminService.GetLeadByAdvanceSearch(""+sb);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/getEmailTemplateByCompanyId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getEmailTemplateByCompanyId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getEmailTemplateByCompanyId(userId);
    return list;
  }
  
  @RequestMapping(value = {"/getMessageTemplateByCompanyId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getMessageTemplateByCompanyId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getMessageTemplateByCompanyId(userId);
    return list;
  }
  
  @RequestMapping(value = {"/deleteEmailTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public List deleteEmailTemplate(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getMessageTemplateByCompanyId(userId);
    return list;
  }
  
  @RequestMapping(value = {"/deleteCRMEmailTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public String deleteCRMEmailTemplate(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    int id = Integer.parseInt(request.getParameter("id"));
    this.adminService.deleteCRMEmailTemplate(id);
    return "Success";
  }
  
  @RequestMapping({"/AddEmailTemplateDetail"})
  @ResponseBody
  public void AddEmailTemplateDetail(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    String tempName = request.getParameter("tname");
    String subject = request.getParameter("tsubject");
    String tText = URLDecoder.decode(request.getParameter("tText"));
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    this.adminService.AddEmailTemplateDetail(userId, tempName, subject, tText);
  }
  
  @RequestMapping(value = {"/GetCategoryLeadCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetCategoryLeadCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.GetCategoryLeadCount(userId);
    return list;
  }
  
  @RequestMapping(value = {"/GetMonthlyLeadCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetMonthlyLeadCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.GetMonthlyLeadCount(userId);
    return list;
  }
  
  @RequestMapping(value = {"/BonrixDashBoard"}, method = {RequestMethod.GET})
  @ResponseBody
  public List BonrixDashBoard(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    String leadProcessStatus = request.getParameter("leadProcessStatus");
    List list = this.adminService.GeneraetDashboard(leadProcessStatus, userId);
    return list;
  }
  
  @RequestMapping(value = {"/DefaultLeadAssignToTelecaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public String DefaultLeadAssignToTelecaller(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    int catId = Integer.parseInt(request.getParameter("catId"));
    int tcId = Integer.parseInt(request.getParameter("tcId"));
    String list = this.adminService.DefaultLeadAssignToTelecaller(catId, tcId);
    return list;
  }
  
  @RequestMapping(value = {"/AssignDefaultEmailTemp"}, method = {RequestMethod.GET})
  @ResponseBody
  public String AssignDefaultEmailTemp(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    int catId = Integer.parseInt(request.getParameter("catId"));
    int tempId = Integer.parseInt(request.getParameter("tempId"));
    String list = this.adminService.AssignDefaultEmailTemp(catId, tempId);
    return list;
  }
  
  @RequestMapping(value = {"/AssignDefaultMsgTemp"}, method = {RequestMethod.GET})
  @ResponseBody
  public String AssignDefaultMsgTemp(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    int catId = Integer.parseInt(request.getParameter("catId"));
    int tempId = Integer.parseInt(request.getParameter("tempId"));
    String list = this.adminService.AssignDefaultMsgTemp(catId, tempId);
    return list;
  }
  
  @RequestMapping(value = {"/getOpenCloseLeadCountByCategory"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getOpenCloseLeadCountByCategory(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getOpenCloseLeadCountByCategory is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getOpenCloseLeadCountByCategory(userId);
    return list;
  }
  
  @RequestMapping(value = {"/getOpenColseLeadCountByTelecaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getOpenColseLeadCountByTelecaller(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getOpenColseLeadCountByTelecaller is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getOpenColseLeadCountByTelecaller(userId);
    return list;
  }
  
  @RequestMapping(value = {"/getStatistic"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getStatistic(HttpServletRequest request, HttpServletResponse response, @RequestParam("type") String StatisticType, @RequestParam("id") String tcid) throws ParseException {
    System.out.println("getStatistic is Called..  " + StatisticType);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getStatistic(StatisticType, Integer.parseInt(tcid));
    return list;
  }
  
  @RequestMapping(value = {"/getTodayTimeCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getTodayTimeCallLog(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getTodayTimeCallLog is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getTodayTimeCallLog(userId);
    return list;
  }
  
  @RequestMapping(value = {"/getAllTimeCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getAllTimeCallLog(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getAllTimeCallLog is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.getAllTimeCallLog(userId);
    return list;
  }
  
  @RequestMapping(value = {"/GetFailCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetFailCallLog(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetFailCallLog is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.GetFailCallLog(userId);
    return list;
  }
  
  @RequestMapping(value = {"/GetInOutBoundFailCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetInOutBoundFailCallLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("callType") String callType, @RequestParam("endate") String endate, @RequestParam("stdate") String stdate, @RequestParam("cat") String catId, @RequestParam("tcId") String tcId, @RequestParam("leadStatus") String leadStatus, @RequestParam("failStatus") String failStatus) throws ParseException {
    System.out.println("GetInOutBoundFailCallLog is Called.. " + callType);
    System.out.println("stdate :: " + stdate + " " + "endate::" + endate);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    System.out.println("Hello Sudhir ");
    int i = 0;
    StringBuilder sb = new StringBuilder();
    sb.append(
        "SELECT followupshistory.followupsId,CONCAT(lead.firstName,' ',lead.lastName) AS name,lead.mobileNo,DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i %p'), followupshistory.callDuration,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),followupshistory.callStatus, followupshistory.followsUpType,categorymanager.categortName,followupshistory.failstatus,lead.leadState FROM followupshistory  INNER JOIN lead  ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId  INNER JOIN categorymanager ON categorymanager.categotyId=lead.categoryId WHERE followupshistory.callStatus='Fail' AND followupshistory.followsUpType='" + 
        callType + "' AND followupshistory.companyId=" + userId + 
        " AND (followupshistory.callingTime BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY)) AND  0=0  ");
    if (!catId.equalsIgnoreCase("All") && !catId.isEmpty()) {
      i = 1;
      sb.append(" AND lead.categoryId=" + Integer.parseInt(catId));
    } 
    if (!tcId.equalsIgnoreCase("All") && !tcId.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.tallycallerId=" + Integer.parseInt(tcId));
    } 
    if (!leadStatus.equalsIgnoreCase("All") && !leadStatus.isEmpty()) {
      i = 1;
      sb.append(" AND lead.leadState='" + leadStatus + "' ");
    } 
    if (!failStatus.equalsIgnoreCase("All") && !failStatus.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.failStatus='" + failStatus + "' ");
    } 
    System.out.println(sb);
    List list = this.adminService.GetLeadParameterForSearch(""+sb);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/updateFailCallLeadState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String updateFailCallLeadState(HttpServletRequest request, HttpServletResponse response, @RequestParam("calltype") String callType, @RequestParam("endate") String endate, @RequestParam("stdate") String stdate, @RequestParam("cat") String catId, @RequestParam("tcId") String tcId, @RequestParam("leadStatus") String leadStatus, @RequestParam("failStatus") String failStatus) throws ParseException {
    System.out.println("updateFailCallLeadState is Called.. " + callType);
    System.out.println("stdate :: " + stdate + " " + "endate::" + endate);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    System.out.println("Hello sudhir ");
    int i = 0;
    StringBuilder sb = new StringBuilder();
    sb.append(
        "SELECT lead.leaadId FROM followupshistory  INNER JOIN lead  ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId  INNER JOIN categorymanager ON categorymanager.categotyId=lead.categoryId WHERE followupshistory.callStatus='Fail' AND followupshistory.followsUpType='" + 
        callType + "' AND followupshistory.companyId=" + userId + 
        " AND (followupshistory.callingTime BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY)) AND  0=0  ");
    if (!catId.equalsIgnoreCase("All") && !catId.isEmpty()) {
      i = 1;
      sb.append(" AND lead.categoryId=" + Integer.parseInt(catId));
    } 
    if (!tcId.equalsIgnoreCase("All") && !tcId.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.tallycallerId=" + Integer.parseInt(tcId));
    } 
    if (!leadStatus.equalsIgnoreCase("All") && !leadStatus.isEmpty()) {
      i = 1;
      sb.append(" AND lead.leadState='" + leadStatus + "' ");
    } 
    if (!failStatus.equalsIgnoreCase("All") && !failStatus.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.failStatus='" + failStatus + "' ");
    } 
    List list = this.adminService.GetLeadParameterForSearch(""+sb);
    String LeadsCommaSeparated = String.join(",", new CharSequence[] { list.toString() });
    String str = "";
    for (int j = 0; j < list.size(); j++) {
      if (j != list.size()) {
        str = String.valueOf(str) + String.valueOf(list.get(j)) + ",";
      } else {
        str = String.valueOf(str) + String.valueOf(list.get(j));
      } 
    } 
    String newStr = str.replaceAll(",$", "");
    String qry = "";
    System.out.println("str::::" + str + "-------------" + "newStr:::::" + newStr);
    if (leadStatus.equals("Open")) {
      qry = "UPDATE lead SET lead.leadState='Close' WHERE lead.leaadId  IN (" + newStr + ")";
    } else {
      qry = "UPDATE lead SET lead.leadState='Open' WHERE lead.leaadId  IN (" + newStr + ")";
    } 
    int stat = this.adminService.updateFailCallLeadStat(qry);
    return "1";
  }
  
  @RequestMapping(value = {"/GetDateRangeFailCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetDateRangeFailCallLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("stdate") String stdate, @RequestParam("endate") String endate) throws ParseException {
    System.out.println("GetDateRangeFailCallLog is Called.. ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.GetDateRangeFailCallLog(userId, stdate, endate);
    return list;
  }
  
  @RequestMapping(value = {"/GetDateRangeSuccessCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetDateRangeSuccessCallLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("stdate") String stdate, @RequestParam("endate") String endate) throws ParseException {
    System.out.println("GetDateRangeSuccessCallLog is Called.. ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.GetDateRangeSuccessCallLog(userId, stdate, endate);
    return list;
  }
  
  @RequestMapping(value = {"/GetInOutBoundSuccessCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetInOutBoundSuccessCallLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("callType") String callType, @RequestParam("endate") String endate, @RequestParam("stdate") String stdate, @RequestParam("cat") String catId, @RequestParam("tcId") String tcId) throws ParseException {
    System.out.println("GetInOutBoundSuccessCallLog is Called.. mmmmmmmm  " + callType);
    System.out.println("stdate :: " + stdate + " " + "endate::" + endate);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    int i = 0;
    StringBuilder sb = new StringBuilder();
    sb.append(
        "SELECT followupshistory.followupsId,CONCAT(lead.`firstName`,' ', lead.`lastName`),lead.mobileNo,lead.`email`, followupshistory.remark,lead.leadProcessStatus,categorymanager.categortName, DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i'),followupshistory.callDuration, CONCAT(tallycaller.firstname,tallycaller.lastname),lead.leadState,followupshistory.successStatus,followupshistory.audoiFilePath,lead.leaadId  FROM followupshistory INNER JOIN lead ON followupshistory.leadId = lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid = followupshistory.tallycallerId  INNER JOIN categorymanager ON categorymanager.categotyId = lead.categoryId  WHERE followupshistory.callStatus = 'Success' AND followupshistory.followsUpType = '" + 
        
        callType + "' " + " AND followupshistory.companyId = " + userId + 
        " AND (followupshistory.callingTime BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY)) AND 0 = 0  ");
    if (!catId.equalsIgnoreCase("All") && !catId.isEmpty()) {
      i = 1;
      sb.append(" AND lead.categoryId=" + Integer.parseInt(catId));
    } 
    if (!tcId.equalsIgnoreCase("All") && !tcId.isEmpty()) {
      i = 1;
      sb.append(" AND followupshistory.tallycallerId=" + Integer.parseInt(tcId));
    } 
    System.out.println(sb);
    List list = this.adminService.GetLeadParameterForSearch(""+sb);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/GetSuccessCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetSuccessCallLog(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetSuccessCallLog is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List list = this.adminService.GetSuccessCallLog(userId);
    return list;
  }
  
  @RequestMapping(value = {"/SaveAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public String SaveAPI(HttpServletRequest request, HttpServletResponse response, @RequestParam("tid") int tid, @RequestParam("api") String API) throws ParseException {
    System.out.println("SaveAPI is Called..");
    ApiKey apiKey = new ApiKey();
    apiKey.setCreateDate(new Date());
    apiKey.setUid(tid);
    apiKey.setIp("*");
    apiKey.setKeyValue(API);
    apiKey.setIshidden(Boolean.valueOf(true));
    this.userService.saveApiKey(apiKey);
    return "Success";
  }
  
  @RequestMapping(value = {"/GenerateAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GenerateAPI(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GenerateAPI is Called..");
    UserService uservice = (UserService)ApplicationContextHolder.getContext().getBean(UserService.class);
    String uuid = UUID.randomUUID().toString();
    return uuid;
  }
  
  @RequestMapping({"/updateScheduledate"})
  @ResponseBody
  public String updateScheduledate(HttpServletRequest request, HttpServletResponse response, @RequestParam("tid") int tid, @RequestParam("shDate") String shDate) throws ParseException {
    System.out.println("updateEmailTemplateDetail is Called..");
    int rowcont = this.adminService.updateSheduleDate(tid, shDate);
    return "Success :: " + rowcont;
  }
  
  @RequestMapping({"/updateEmailTemplateDetail"})
  @ResponseBody
  public String updateEmailTemplateDetail(HttpServletRequest request, HttpServletResponse response, @RequestParam("tid") int tid, @RequestParam("tsubject") String tsubject, @RequestParam("tempName") String tempname) throws ParseException {
    System.out.println("updateEmailTemplateDetail is Called..");
    String text1 = request.getParameter("tText");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int agentid = (int)id;
    this.adminService.updateEmailTemplateDetail(tid, tsubject, text1, tempname);
    return "Success";
  }
  
  @RequestMapping(value = {"/getCRMEmailTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getCRMEmailTemplate(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getCRMEmailTemplate is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    String rol = "Company";
    List<Object[]> list = this.adminService.getCRMEmailTemplate(Id, rol);
    JSONObject responseDetailsJson = new JSONObject();
    if (list.size() != 0) {
      JSONArray jsonArray = new JSONArray();
      for (int i = 0; i < list.size(); i++) {
        JSONObject data = new JSONObject();
        Object[] result = list.get(i);
        data.put("id", result[0]);
        data.put("tempname", result[1]);
        data.put("temptext", result[2]);
        data.put("subject", result[3]);
        jsonArray.add(data);
        responseDetailsJson.put("data", jsonArray);
        System.out.println(responseDetailsJson.toString());
      } 
    } 
    return responseDetailsJson.toString();
  }
  
  @RequestMapping(value = {"/getEmailTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getEmailTemplate(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getEmailTemplate is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    String rol = "";
    int compId = 0;
    List list = null;
    UserRole role = new UserRole();
    role = this.adminService.getUserRole(uid);
    rol = role.getRole().toString();
    if (role.getRole().equalsIgnoreCase("ROLE_STAFF")) {
      rol = "staff_Id";
    } else {
      rol = "comp_Id";
    } 
    if (rol == "staff_Id") {
      User user = new User();
      user = this.adminService.getUserByUid(Long.valueOf(id));
      list = this.adminService.getLeadProcessState(user.getCompanyId());
    } else {
      list = this.adminService.getLeadProcessState(uid);
    } 
    return list;
  }
  
  @RequestMapping(value = {"/getTodaySentSMSLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getTodaySentSMSLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("endDate") String endDate, @RequestParam("startDate") String startDate) throws ParseException {
    System.out.println("getTodaySentSMSLog is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    String sdt = null;
    String edt = null;
    sdt = startDate;
    edt = endDate;
    System.out.println("Val : " + sdt + "  " + edt);
    UserRole role1 = new UserRole();
    role1 = this.adminService.getUserRole(Id);
    User user = new User();
    user = this.adminService.getUserByUid(Long.valueOf(id));
    String rol = role1.getRole().toString();
    rol = "comp_Id";
    List<Object[]> list = null;
    if (sdt.isEmpty() && edt.isEmpty()) {
      System.out.println("NULL is Called");
      String stDate = "";
      String endDte = "";
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      Date date1 = new Date();
      stDate = String.valueOf(dateFormat.format(date1)) + " " + "00:00:00";
      endDate = String.valueOf(dateFormat.format(date1)) + " " + "23:59:59";
      System.out.println(String.valueOf(id) + " " + stDate + " " + endDte);
      list = this.adminService.getTodaySentSMSLog((int)id, stDate, endDate, rol);
    } else if (!sdt.isEmpty() && !edt.isEmpty()) {
      sdt = String.valueOf(sdt) + " 00:00:00";
      edt = String.valueOf(edt) + " 23:59:29";
      System.out.println("NULL is not Called " + sdt + "  " + edt);
      list = this.adminService.getTodaySentSMSLog((int)id, sdt, edt, rol);
    } 
    System.out.println("Final Id : " + id + " :  " + list.size());
    JSONArray jsonArray = new JSONArray();
    JSONObject responseDetailsJson = new JSONObject();
    for (int i = 0; i < list.size(); i++) {
      JSONObject data = new JSONObject();
      Object[] result = list.get(i);
      data.put("id", result[0]);
      data.put("message", result[2].toString());
      data.put("mobileNo", result[1]);
      data.put("MSGStatus", result[4]);
      data.put("sendDate", result[3].toString());
      jsonArray.add(data);
    } 
    responseDetailsJson.put("data", jsonArray);
    System.out.println(responseDetailsJson.toString());
    return responseDetailsJson.toString();
  }
  
  @RequestMapping(value = {"/getSentSMSLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getSentSMSLog(HttpServletRequest request, HttpServletResponse response, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, @RequestParam("role") String role, @RequestParam("mobNo") int mobNo) throws ParseException {
    System.out.println("getSentSMSLog is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    int finalId = 0;
    UserRole role1 = new UserRole();
    role1 = this.adminService.getUserRole(Id);
    User user = new User();
    user = this.adminService.getUserByUid(Long.valueOf(id));
    String rol = role1.getRole().toString();
    if (role1.getRole().equalsIgnoreCase("ROLE_STAFF")) {
      rol = "staff_Id";
      finalId = user.getCompanyId();
    } else {
      finalId = user.getCompanyId();
      rol = "comp_Id";
    } 
    List<Object[]> list = this.adminService.getSentSMSLog(finalId, startDate, endDate, role, mobNo);
    JSONArray jsonArray = new JSONArray();
    JSONObject responseDetailsJson = new JSONObject();
    for (int i = 0; i < list.size(); i++) {
      JSONObject data = new JSONObject();
      Object[] result = list.get(i);
      data.put("id", result[0]);
      data.put("message", result[1]);
      data.put("mobileNo", result[2]);
      data.put("MSGStatus", result[3]);
      data.put("sendDate", result[4]);
      jsonArray.add(data);
    } 
    responseDetailsJson.put("data", jsonArray);
    System.out.println(responseDetailsJson.toString());
    return responseDetailsJson.toString();
  }
  
  @RequestMapping(value = {"/getLeadDataSMS"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getLeadDataSMS(HttpServletRequest request, HttpServletResponse response, @RequestParam("lstate") String lstate, @RequestParam("pstate") String pstate, @RequestParam("cat") int cat) throws ParseException {
    System.out.println("getLeadDataSMS is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("getLeadDataSMS : " + uid);
    String rol = "";
    int compId = 0;
    List list = null;
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    UserRole role = new UserRole();
    role = this.adminService.getUserRole(uid);
    rol = role.getRole().toString();
    if (role.getRole().equalsIgnoreCase("ROLE_STAFF")) {
      rol = "staff_Id";
    } else {
      rol = "comp_Id";
    } 
    System.out.println("Role : " + rol);
    if (rol == "staff_Id") {
      User user = new User();
      user = this.adminService.getUserByUid(Long.valueOf(id));
      list = this.adminService.getLeadDataSMS(user.getCompanyId(), cat, pstate, lstate);
      datatableJsonObject.setRecordsFiltered(list.size());
      datatableJsonObject.setRecordsTotal(list.size());
      datatableJsonObject.setData(list);
    } else {
      list = this.adminService.getLeadDataSMS(uid, cat, pstate, lstate);
      datatableJsonObject.setRecordsFiltered(list.size());
      datatableJsonObject.setRecordsTotal(list.size());
      datatableJsonObject.setData(list);
    } 
    return list;
  }
  
  @RequestMapping(value = {"/getLeadProcessState"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getLeadProcessState(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetCategoryForSMS is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("getLeadProcessState : " + uid);
    String rol = "";
    int compId = 0;
    List list = this.adminService.getLeadProcessState(uid);
    return list;
  }
  
  @RequestMapping(value = {"/getLeadSuccessStatus"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getLeadSuccessStatus(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetCategoryForSMS is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("getLeadProcessState : " + uid);
    String rol = "";
    int compId = 0;
    List list = this.adminService.getLeadSuccessStatus(uid);
    return list;
  }
  
  @RequestMapping(value = {"/GetCategoryForSMS"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetCategoryForSMS(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetCategoryForSMS is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("GetContactInfoDetail Id : " + uid);
    List list = this.adminService.getCategoryList(Long.valueOf(uid));
    return list;
  }
  
  @SuppressWarnings("resource")
	@RequestMapping(value = "/SendSingalMessageControl", method = RequestMethod.GET)
	public @ResponseBody void SendSingalMessageControl(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "no") String mob1, @RequestParam(value = "msg") String message)
			throws ParseException, UnsupportedEncodingException {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		final MediUser currentUser = (MediUser) auth.getPrincipal();
		final long id = currentUser.getUserid();
		final int staffId = (int) (long) id;

		long mob = Long.parseLong(mob1);
		System.out.println("SendSingalMessageControl is Called.." + message);

		// String msg =URLEncoder.encode(request.getParameter("msg"), "UTF-8");
		String msg = request.getParameter("msg");
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

		List list = null;
		String url1 = null;

		User users = new User();
		users = adminService.getUserByUid(id);

		System.out.println("Company Id here....!!!!" + users.getCompanyId());
		list = adminService.GetAPI(staffId);
		System.out.println(list.size());
		for (int i = 0; i < list.size(); i++) {

			JSONObject leadmap = new JSONObject();
			Object[] result = (Object[]) list.get(i);
			boolean status = (boolean) result[4];
			System.out.println("STATUS : " + status);
			if (status) {
				url1 = (String) result[1];
				url1 = url1.replace("<mobileno>", mob + "").replace("<message>", URLEncoder.encode(message));
				System.out.println("Mobile : " + mob + " \n" + "MSG : " + message);
				System.out.println("URL1 : : =>" + url1);
			}

		}

		if (url1 != null) {
			final Sentsmslog log = new Sentsmslog();
			String rol = "";
			UserRole role = new UserRole();
			role = adminService.getUserRole(staffId);
			if (role.getRole().equalsIgnoreCase("ROLE_STAFF")) {
				rol = "staff_Id";
				log.setTallycallerId(0);
				log.setStaffId(staffId);
				log.setCompayId(0);
			} else {
				rol = "comp_Id";
				log.setTallycallerId(0);
				log.setStaffId(0);
				log.setCompayId(staffId);
			}
			log.setMobileNo(mob);
			log.setMessage(msg);
			log.setSendDate(new Date());
			asyncHttpClient.prepareGet(url1).execute(new AsyncCompletionHandler<Response>() {
				@Override
				public Response onCompleted(Response responce) throws Exception {
					User user = new User();
					user = adminService.getUserByUid(currentUser.getUserid());
					System.out.println("USER_ID : " + user.getCompanyId());
					List list = adminService.GetAPI(user.getCompanyId());
					String ststus = "";
					int uid = 0;
					for (int i = 0; i < list.size(); i++) {
						JSONObject leadmap = new JSONObject();
						Object[] result = (Object[]) list.get(i);
						uid = (int) result[0];
						System.out.println("RESPONCE : " + result[2].toString());

						boolean status = (boolean) result[4];
						if (status == true) {
							ststus = result[2].toString();
						}
					}

					System.out.println("Status : " + ststus);
					String res = responce.getResponseBody().replaceAll(" ", "");
					if (res.contains(ststus)) {
						System.out.println("No Error in MSG");
						log.setMSGStatus("SUCCESS");
					} else {
						System.out.println("Error in MSG");
						log.setMSGStatus("FAIL");
					}
					try {
						System.out.println("RES : " + res);
						log.setAPIResponse(res);
						System.out.println(log.getAPIResponse() + "\n" + log.getMSGStatus());
						adminService.saveLog(log);

					} catch (Exception e) {
						System.out.println("Save Error : " + e);
					}
					System.out.println(responce.getResponseBody());
					return responce;
				}

				@Override
				public void onThrowable(Throwable t) {
					// Something wrong happened.
					t.printStackTrace();
					System.out.println("=======" + t.getMessage());
					log.setAPIResponse("N/A");
					log.setMSGStatus("FAIL");
					adminService.saveLog(log);
				}
			});
		}
	}
  
  @RequestMapping(value = {"/GetContactInfoDetailSMS"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetContactInfoDetailSMS(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetContactInfoDetail is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("GetContactInfoDetail Id : " + uid);
    String rol = "";
    int compId = 0;
    List list = null;
    UserRole role = new UserRole();
    role = this.adminService.getUserRole(uid);
    rol = role.getRole().toString();
    if (role.getRole().equalsIgnoreCase("ROLE_STAFF")) {
      rol = "staff_Id";
    } else {
      rol = "comp_Id";
    } 
    if (rol == "staff_Id") {
      User user = new User();
      user = this.adminService.getUserByUid(Long.valueOf(id));
      list = this.adminService.GetContactInfoDetailSMS(user.getCompanyId());
    } else {
      list = this.adminService.GetContactInfoDetailSMS(uid);
    } 
    return list;
  }
  
  @RequestMapping(value = {"/GetTemplateList"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetTemplateList(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    System.out.println("GetTemplateList is called :  " + Id);
    String rol = "";
    UserRole role = new UserRole();
    role = this.adminService.getUserRole(Id);
    if (role.getRole().equalsIgnoreCase("ROLE_STAFF")) {
      rol = "staff_Id";
    } else {
      rol = "comp_Id";
    } 
    List list = this.adminService.GetTemplate(Id, rol);
    return list;
  }
  
  @RequestMapping(value = {"/activeAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public void activeAPI(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") int APIid) throws ParseException {
    System.out.println("activeAPI is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    System.out.println("API : " + APIid + "   " + "UID : " + Id);
    this.adminService.activeAPI(APIid, Id);
  }
  
  @RequestMapping(value = {"/deleteAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deleteAPI(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") int id) throws ParseException {
    System.out.println("deleteAPI is called :  ");
    this.adminService.deleteAPI(id);
  }
  
  @RequestMapping(value = {"/updateAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public void updateAPI(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") int id, @RequestParam("key") String key, @RequestParam("provider") String provider, @RequestParam("desc") String desc) throws ParseException {
    System.out.println("updateAPI is called :  ");
    this.adminService.updateAPI(id, key, provider, desc);
  }
  
  @RequestMapping(value = {"/addAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public void addAPI(HttpServletRequest request, HttpServletResponse response, @RequestParam("key") String key, @RequestParam("provider") String provider, @RequestParam("api") String api) throws ParseException {
    System.out.println("addAPI is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    UserRole role = new UserRole();
    role = this.adminService.getUserRole(Id);
    Smssettings setings = new Smssettings();
    setings.setUrl(api);
    setings.setProvider(provider);
    setings.setResponce(key);
    setings.setStaff_Id(0);
    setings.setComp_Id(Id);
    setings.setStatus(true);
    this.adminService.addAPI(setings);
  }
  
  @RequestMapping(value = {"/GetAllAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAllAPI(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetAllAPI is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    List list = this.adminService.GetAllAPI(Id);
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/GetAPI"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAPI(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetAPI is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    List list = this.adminService.GetAPI(Id);
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/addTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public void addTemplate(HttpServletRequest request, HttpServletResponse response, @RequestParam("tempname") String tempname, @RequestParam("tempdesc") String tempdesc) throws ParseException {
    System.out.println("addTemplate is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    UserRole role = new UserRole();
    role = this.adminService.getUserRole(Id);
    Smstemplate temp = new Smstemplate();
    temp.setTemp_Name(tempname);
    temp.setTemp_Text(tempdesc);
    if (role.getRole().equalsIgnoreCase("ROLE_STAFF")) {
      temp.setStaff_Id(Id);
      temp.setComp_Id(0);
    } else {
      temp.setStaff_Id(0);
      temp.setComp_Id(Id);
    } 
    temp.setSTATUS(true);
    this.adminService.addTemplate(temp);
  }
  
  @RequestMapping(value = {"/activeTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public void activeTemplate(HttpServletRequest request, HttpServletResponse response, @RequestParam("tempid") int tempid) throws ParseException {
    System.out.println("deleteTemplate is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    this.adminService.activeTemplate(tempid);
  }
  
  @RequestMapping(value = {"/deactiveTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deactiveTemplate(HttpServletRequest request, HttpServletResponse response, @RequestParam("tempid") int tempid) throws ParseException {
    System.out.println("deleteTemplate is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    this.adminService.deactiveTemplate(tempid);
  }
  
  @RequestMapping(value = {"/deleteTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deleteTemplate(HttpServletRequest request, HttpServletResponse response, @RequestParam("tempid") int tempid) throws ParseException {
    System.out.println("deleteTemplate is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    this.adminService.deleteTemplate(tempid);
  }
  
  @RequestMapping(value = {"/updateTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public void updateTemplate(HttpServletRequest request, HttpServletResponse response, @RequestParam("tempid") int tempid, @RequestParam("tempname") String tempname, @RequestParam("tempdesc") String tempdesc) throws ParseException {
    System.out.println("updateTemplate is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    this.adminService.updateTemplate(tempid, tempname, tempdesc);
  }
  
  @RequestMapping(value = {"/GetTemplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTemplate(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetTemplate is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    String rol = "";
    UserRole role = new UserRole();
    role = this.adminService.getUserRole(Id);
    if (role.getRole().equalsIgnoreCase("ROLE_STAFF")) {
      rol = "staff_Id";
    } else {
      rol = "comp_Id";
    } 
    List list = this.adminService.GetTemplate(Id, rol);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/addImapSetting"}, method = {RequestMethod.GET})
  @ResponseBody
  public void addImapSetting(HttpServletRequest request, HttpServletResponse response, @RequestParam("email") String email, @RequestParam("pass") String password) throws ParseException {
    System.out.println("addImapSetting is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int staffId = (int)id;
    System.out.println("addImapSetting is called :  " + staffId);
    this.adminService.addImapSetting(email, password, staffId);
  }
  
  @RequestMapping(value = {"/UpdateImapSetting"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UpdateImapSetting(HttpServletRequest request, HttpServletResponse response, @RequestParam("email") String email, @RequestParam("pass") String password) throws ParseException {
    System.out.println("UpdateImapSetting is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int staffId = (int)id;
    System.out.println("UpdateImapSetting is called :  " + staffId);
    this.adminService.UpdateImapSetting(email, password, staffId);
  }
  
  @RequestMapping(value = {"/GetImapSettingByCompanyId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetImapSettingByCompanyId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetImapSettingByCompanyId is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    System.out.println("GetImapSettingByUid is called :  " + companyId);
    return this.adminService.GetImapSettingByCompanyId(companyId);
  }
  
  @RequestMapping(value = {"/GetImapSettingByStaffId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetImapSettingByStaffId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetImapSettingByUid is called :  ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int staffId = (int)id;
    System.out.println("GetImapSettingByUid is called :  " + staffId);
    return this.adminService.GetImapSettingByStaffId(staffId);
  }
  
  @RequestMapping(value = {"/GetTellyCallerBycompanyId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetTellyCallerBycompanyId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    return this.adminService.GetTellyCallerBycompanyId(companyId);
  }
  
  @RequestMapping(value = {"/addRegeX"}, method = {RequestMethod.GET})
  @ResponseBody
  public void addRegeX(HttpServletRequest request, HttpServletResponse response, @RequestParam("from") String from, @RequestParam("subject") String subject, @RequestParam("category") int category, @RequestParam("namepatten") String namepatten, @RequestParam("emailpatten") String emailpatten, @RequestParam("mobilepatten") String mobilepatten, @RequestParam("tallycallerId") int tallycallerId) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    System.out.println("addRegeX is Called..");
    this.adminService.addRegeX(from, subject, category, namepatten, emailpatten, mobilepatten, tallycallerId, companyId);
  }
  
  @RequestMapping(value = {"/GetTelecaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetTelecallerByStaffId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetTelecaller is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    System.out.println("GetTelecaller Comp Id " + Id);
    List list = this.adminService.GetTelecaller(Id);
    return list;
  }
  
  @RequestMapping(value = {"/GetLeadObjectByLeadId"}, method = {RequestMethod.GET})
  @ResponseBody
  public Lead GetLeadObjectByLeadId(HttpServletRequest request, HttpServletResponse response, @RequestParam("name") int leadId) throws ParseException {
    System.out.println("AddContactInfoDetail is Called..");
    int lid = Integer.parseInt(request.getParameter("leadId"));
    Lead lead = new Lead();
    lead = this.adminService.GetLeadObjectByLeadId(lid);
    return lead;
  }
  
  @RequestMapping(value = {"/GetTellyCallerByName"}, method = {RequestMethod.GET})
  @ResponseBody
  public Tallycaller GetTellyCallerByName(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    String tname = request.getParameter("tcallerName");
    Tallycaller tcaller = new Tallycaller();
    tcaller = this.adminService.GetTellyCallerByName(tname);
    return tcaller;
  }
  
  @RequestMapping(value = {"/DeleteStaff"}, method = {RequestMethod.GET})
  @ResponseBody
  public void DeleteStaff(HttpServletRequest request, HttpServletResponse response) throws ParseException, UnsupportedEncodingException {
    System.out.println("DeleteStaff is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    System.out.println();
    int sid = Integer.parseInt(request.getParameter("id"));
    this.adminService.DeleteStaff(sid);
  }
  
  @RequestMapping(value = {"/UpdateStaff"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UpdateStaff(HttpServletRequest request, HttpServletResponse response) throws ParseException, UnsupportedEncodingException {
    System.out.println("UpdateStaff is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    System.out.println();
    int sid = Integer.parseInt(request.getParameter("id"));
    String username = request.getParameter("username");
    String email = request.getParameter("email");
    String name = request.getParameter("name");
    String mob = request.getParameter("mobile");
    String city = request.getParameter("city");
    String password = "";
    this.adminService.UpdateStaff(sid, username, email, name, mob, password, city, companyId);
  }
  
  @RequestMapping(value = {"/AddStaff"}, method = {RequestMethod.GET})
  @ResponseBody
  public void AddStaff(HttpServletRequest request, HttpServletResponse response) throws ParseException, UnsupportedEncodingException {
    System.out.println("AddStaff is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    System.out.println();
    String username = request.getParameter("username");
    String email = request.getParameter("email");
    String name = request.getParameter("name");
    String mob = request.getParameter("mobile");
    String pass = request.getParameter("password");
    String city = request.getParameter("city");
    String password = (new SecurityConfig()).passwordEncoder().encode(pass);
    User user = this.adminService.AddStaff(username, email, name, mob, password, city, companyId);
    System.out.println("pss : " + password);
    System.out.println("dfd " + user.getCity());
    System.out.println("AddUser role is Called..");
    this.adminService.addRole(user.getUsername());
  }
  
  @RequestMapping(value = {"/GetStaff"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetStaff(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetStaff is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("GetStaff Id : " + uid);
    List list = this.adminService.GetStaff(uid);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/fileUpload"}, method = {RequestMethod.POST})
  @ResponseBody
  public String fileUpload(HttpServletRequest request, HttpServletResponse response, @RequestParam("ticket_exampleInputFile") MultipartFile file) throws ParseException {
    System.out.println("fileUpload is Called..");
    System.out.println("FIle Path : " + file);
    Date today = Calendar.getInstance().getTime();
    String SEP = System.getProperty("file.separator");
    String filePath = request.getServletContext().getRealPath("/");
    filePath = String.valueOf(filePath) + SEP + "UploadFile" + SEP;
    System.out.println(filePath);
    Format formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    String s = formatter.format(today);
    s.trim();
    String fileName = s;
    if (!file.isEmpty())
      try {
        fileName = String.valueOf(fileName) + "_" + file.getOriginalFilename();
        byte[] bytes = file.getBytes();
        BufferedOutputStream buffStream = new BufferedOutputStream(
            new FileOutputStream(new File(String.valueOf(filePath) + fileName)));
        buffStream.write(bytes);
        buffStream.close();
        System.out.println("S : " + s.trim());
        System.out.println("Name : " + fileName);
        return fileName;
      } catch (Exception e) {
        return "You failed to upload " + fileName + ": " + e.getMessage();
      }  
    return "Unable to upload. File is empty.";
  }
  
  @RequestMapping(value = {"/redirectedUrl"}, method = {RequestMethod.GET})
  public String redirection(Model model) {
    model.addAttribute("msg", "Spring Redirection Test");
    return "hellojsp";
  }
  
  @RequestMapping(value = {"/addFullTicket"}, method = {RequestMethod.GET})
  @ResponseBody
  public void addFullTicket(HttpServletRequest request, HttpServletResponse response) throws ParseException, UnsupportedEncodingException {
    System.out.println("addFullTicket is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int agentid = (int)id;
    Date dt = new Date();
    Calendar c = Calendar.getInstance();
    c.setTime(dt);
    c.add(5, 4);
    dt = c.getTime();
    AegeCustDetail acdetail = new AegeCustDetail();
    acdetail = this.adminService.getCompanyIdByAgentId(agentid);
    String email = request.getParameter("email");
    String subject = request.getParameter("subject");
    String type = request.getParameter("type");
    String status = request.getParameter("status");
    String priority = request.getParameter("priority");
    String group = request.getParameter("group");
    String agent = request.getParameter("agent");
    String filepath = request.getParameter("filepath");
    String desc = request.getParameter("desc");
    Ticketmaster tmst = new Ticketmaster();
    tmst.setCompanyId(acdetail.getCompanyId());
    tmst.setUserEmailId(email);
    tmst.setPriority(priority);
    tmst.setTicketStatus(status);
    tmst.setSource(desc);
    tmst.setTicketType(type);
    tmst.setAgentId(agentid);
    tmst.setDepartment(Integer.parseInt(group));
    tmst.setCreatedDate(new Date());
    tmst.setDueOn(dt);
    tmst.setIdDeleted(0);
    tmst.setFileName(filepath);
    tmst.setSubject(subject);
    this.adminService.addFullTicket(tmst);
  }
  
  @RequestMapping(value = {"/AddContactInfoDetail"}, method = {RequestMethod.GET})
  @ResponseBody
  public void AddContactInfoDetail(HttpServletRequest request, HttpServletResponse response, @RequestParam("name") String name, @RequestParam("email") String email, @RequestParam("cno") String cno) throws ParseException {
    System.out.println("AddContactInfoDetail is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int agentid = (int)id;
    this.adminService.AddContactInfoDetail(agentid, name, email, cno);
  }
  
  @RequestMapping(value = {"/deleteContactInfoDetail"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deleteContactInfoDetail(HttpServletRequest request, HttpServletResponse response, @RequestParam("cid") int id) throws ParseException {
    System.out.println("deleteContactInfoDetail is Called..");
    this.adminService.deleteContactInfoDetail(id);
  }
  
  @RequestMapping(value = {"/updateContactInfoDetail"}, method = {RequestMethod.GET})
  @ResponseBody
  public void updateContactInfoDetail(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") int id, @RequestParam("name") String name, @RequestParam("email") String email, @RequestParam("cno") String cno) throws ParseException {
    System.out.println("updateContactInfoDetail is Called..");
    this.adminService.updateContactInfoDetail(id, name, email, cno);
  }
  
  @RequestMapping(value = {"/GetContactInfoDetailList"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetContactInfoDetailList(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetContactInfoDetail is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("GetContactInfoDetail Id : " + uid);
    List list = this.adminService.GetContactInfoDetail(uid);
    return list;
  }
  
  @RequestMapping(value = {"/GetContactInfoDetail"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetContactInfoDetail(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetContactInfoDetail is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    System.out.println("GetContactInfoDetail Id : " + uid);
    List list = this.adminService.GetContactInfoDetail(uid);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/GetAllTickect"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAllTickect(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetAllTickect is called...");
    List l = this.adminService.GetAllTickect();
    Ticketmaster tc = new Ticketmaster();
    return (new Gson()).toJson(l);
  }
  
  @RequestMapping(value = {"/sendTicketWithStatus"}, method = {RequestMethod.GET})
  @ResponseBody
  public void sendTicketWithStatus(HttpServletRequest request, HttpServletResponse response, @RequestParam("tid") int tkid, @RequestParam("ststus") String status, @RequestParam("msg") String message) throws ParseException {
    System.out.println("sendTicketWithStatus is Called.." + tkid + " " + status);
    this.adminService.sendTicketWithStatus(tkid, status);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long agentId = currentUser.getUserid();
    this.adminService.addTicketchildMST(tkid, (int)agentId, message);
    Ticketmaster tmst = new Ticketmaster();
    tmst = this.adminService.getTicketObjectById(tkid);
    System.out.println("Demo : " + tmst.getSubject() + " " + tmst.getUserEmailId());
    EmailTemplate email = new EmailTemplate();
    try {
      email.setMessage(URLDecoder.decode(message, "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    } 
    email.setSubject(tmst.getSubject());
    (new Thread((Runnable)new EmailSenderThread(email, this.smsService.getHostSetting("admin"), tmst.getUserEmailId()))).start();
  }
  
  @RequestMapping(value = {"/addTicketchildMST"}, method = {RequestMethod.GET})
  @ResponseBody
  public void addTicketchildMST(HttpServletRequest request, HttpServletResponse response, @RequestParam("tickid") int tickId, @RequestParam("msg") String msg) throws ParseException {
    System.out.println("addTicketchildMST is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long agentId = currentUser.getUserid();
    this.adminService.addTicketchildMST(tickId, (int)agentId, msg);
    Ticketmaster tmst = new Ticketmaster();
    tmst = this.adminService.getTicketObjectById(tickId);
    System.out.println("Demo : " + tmst.getSubject() + " " + tmst.getUserEmailId());
    EmailTemplate email = new EmailTemplate();
    try {
      email.setMessage(URLDecoder.decode(msg, "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    } 
    email.setSubject(tmst.getSubject());
    (new Thread((Runnable)new EmailSenderThread(email, this.smsService.getHostSetting("admin"), tmst.getUserEmailId()))).start();
  }
  
  @RequestMapping(value = {"/getTicketIdByEmailId"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getTicketIdByEmailId(HttpServletRequest request, HttpServletResponse response, @RequestParam("email") String emailid) throws ParseException {
    System.out.println("getTicketIdByEmailId is called...");
    List l = this.adminService.getTicketIdByEmailId(emailid);
    System.out.println(l.size());
    return (new Gson()).toJson(l);
  }
  
  @RequestMapping(value = {"/GetTickectByEmailId"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTickectByEmailId(HttpServletRequest request, HttpServletResponse response, @RequestParam("email") String emailId, @RequestParam("todate") String todate, @RequestParam("fromdate") String fromdate, @RequestParam("status") String status) throws ParseException {
    System.out.println("GetTickectByEmailId " + todate + " " + fromdate);
    List hist = this.adminService.GetTickectByEmailId(emailId, String.valueOf(todate) + " 23:59:59", String.valueOf(fromdate) + " 00:00:00", status);
    System.out.println("L= " + hist.size());
    return (new Gson()).toJson(hist);
  }
  
  @RequestMapping(value = {"/GetContactInfo"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetContactInfo(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetContactInfo is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long agentId = currentUser.getUserid();
    List l = this.adminService.GetContactInfo((int)agentId);
    return l;
  }
  
  @RequestMapping(value = {"/updateTicketProprities"}, method = {RequestMethod.GET})
  @ResponseBody
  public void updateTicketProprities(HttpServletRequest request, HttpServletResponse response, @RequestParam("satatus") String satatus, @RequestParam("priority") String priority, @RequestParam("tickId") int tickId, @RequestParam("departmentId") int departmentId, @RequestParam("agentId") int agentdId) throws ParseException {
    System.out.println("updateTicketProprities is called...");
    this.adminService.updateTicketProprities(satatus, priority, tickId, departmentId, agentdId);
  }
  
  @RequestMapping(value = {"/GetTicketChatHistory"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTicketChatHistory(HttpServletRequest request, HttpServletResponse response, @RequestParam("tid") int tkid) throws ParseException {
    System.out.println("GetTicketChatHistory is called...");
    List l = this.adminService.GetTicketChatHistory(tkid);
    System.out.println(l.size());
    return (new Gson()).toJson(l);
  }
  
  @RequestMapping(value = {"/getContactInfoByEmail"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getContactInfoByEmail(HttpServletRequest request, HttpServletResponse response, @RequestParam("email") String email) throws ParseException {
    System.out.println("getContactInfoByEmail is called...");
    List l = this.adminService.getContactInfoByEmail(email);
    return l;
  }
  
  @RequestMapping(value = {"/GetTicketDetail"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTicketDetail(HttpServletRequest request, HttpServletResponse response, @RequestParam("tid") int tcid) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    System.out.println("GetTicketDetail is called...");
    List hist = this.adminService.GetTicketDetail(tcid);
    System.out.println((new Gson()).toJson(hist));
    return (new Gson()).toJson(hist);
  }
  
  @RequestMapping(value = {"/GetTickectByTicketId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetTickectByTicketId(HttpServletRequest request, HttpServletResponse response, @RequestParam("tid") int tcid) throws ParseException {
    System.out.println("GetTickectByTicketId is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List hist = this.adminService.GetTickectByTicketId(tcid);
    return hist;
  }
  
  @RequestMapping(value = {"/GetOverdueticket"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetOverdueticket(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetOverdueticket is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List ovetticket = this.adminService.GetOverdueticket();
    return (new Gson()).toJson(ovetticket);
  }
  
  @RequestMapping(value = {"/GetDueTodayCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetDueTodayCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetOverdueticketCount is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List dtodayicket = this.adminService.GetDueToday();
    return dtodayicket;
  }
  
  @RequestMapping(value = {"/GetDueToday"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetDueToday(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetOverdueticketCount is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List dtodayicket = this.adminService.GetDueToday();
    return (new Gson()).toJson(dtodayicket);
  }
  
  @RequestMapping(value = {"/GetOverdueticketCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetOverdueticketCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetOverdueticketCount is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List ovetticket = this.adminService.GetOverdueticket();
    return ovetticket;
  }
  
  @RequestMapping(value = {"/GetUnassignTickect"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetUnassignTickect(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetUnassignTickect is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List hist = this.adminService.GetUnassignTickect();
    return (new Gson()).toJson(hist);
  }
  
  @RequestMapping(value = {"/GetUnassignTickectCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetUnassignTickectCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetUnassignTickectCount is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List hist = this.adminService.GetUnassignTickect();
    return hist;
  }
  
  @RequestMapping(value = {"/GetOpenTickect"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetOpenTickect(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetOpenTickect is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List hist = this.adminService.GetOpenTickect();
    return (new Gson()).toJson(hist);
  }
  
  @RequestMapping(value = {"/GetOpenTickectCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetOpenTickectCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetOpenTickectCount is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List hist = this.adminService.GetOpenTickect();
    return hist;
  }
  
  @RequestMapping(value = {"/getRecentHistory"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getRecentHistory(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getRecentHistory is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List hist = this.adminService.getRecentHistory();
    return (new Gson()).toJson(hist);
  }
  
  @RequestMapping(value = {"/getRequestInfo"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getRequestInfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("email") String email) throws ParseException {
    System.out.println("getRequestInfo is called...");
    List l = this.adminService.getRequestInfo(email);
    return l;
  }
  
  @RequestMapping(value = {"/getAgent"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getAgent(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getAgent is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List l = this.adminService.getAgentByCompany((int)uid);
    System.out.println("User Id " + uid);
    System.out.println("Agent Sizze " + l.size());
    return l;
  }
  
  @RequestMapping(value = {"/getDepartment"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getDepartment(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("getDepartment is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Agent Id : " + uid);
    List l = this.adminService.getDepartment((int)uid);
    System.out.println(l.size());
    return l;
  }
  
  @RequestMapping(value = {"/addTicket"}, method = {RequestMethod.GET})
  @ResponseBody
  public int addTicket(HttpServletRequest request, HttpServletResponse response, @RequestParam("email") String email, @RequestParam("subject") String subject, @RequestParam("filepath") String filepath, @RequestParam("desc") String desc) throws ParseException {
    System.out.println("addTicket is Called..");
    Ticketmaster tmst = new Ticketmaster();
    tmst = this.adminService.addTicket(email, subject, filepath, desc);
    System.out.println("Return Ticket Id : " + tmst.getTicketId());
    EmailTemplate email1 = new EmailTemplate();
    try {
      email1.setMessage(URLDecoder.decode(tmst.getSource(), "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    } 
    email1.setSubject(tmst.getSubject());
    (new Thread((Runnable)new EmailSenderThread(email1, this.smsService.getHostSetting("admin"), tmst.getUserEmailId()))).start();
    return tmst.getTicketId();
  }
  
  @RequestMapping(value = {"/addLeadContact"}, method = {RequestMethod.GET})
  @ResponseBody
  public void addLeadContact(HttpServletRequest request, HttpServletResponse response, @RequestParam("name") String name, @RequestParam("email") String email, @RequestParam("cno") String cno) throws ParseException {
    System.out.println("addLeadContact is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    this.adminService.addContactInfo(name, email, cno, (int)uid);
    List<String> earray = new ArrayList<>();
  }
  
  @RequestMapping(value = {"/compareLeadContact"}, method = {RequestMethod.GET})
  @ResponseBody
  public int compareLeadContact(HttpServletRequest request, HttpServletResponse response, @RequestParam("cno") String cno) throws ParseException {
    System.out.println("compareLeadContact is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    int cinfo = this.adminService.compareLeadContact(cno, uid);
    return cinfo;
  }
  
  @RequestMapping(value = {"/GetLeadToAddCustomer"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadToAddCustomer(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetLeadToAddCustomer is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int uid = (int)id;
    List list = this.adminService.GetLeadToAddCustomer(uid);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping({"/addContactInfo"})
  @ResponseBody
  public void addContactInfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("cname") String cname, @RequestParam("email") String email, @RequestParam("cno") String cno) throws ParseException {
    System.out.println("addContactInfo is called...");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    this.adminService.addContactInfo(cname, email, cno, (int)uid);
  }
  
  @RequestMapping({"/deleteContactInfo"})
  @ResponseBody
  public String deleteContactInfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("cid") int cid) throws ParseException {
    System.out.println("deleteContactInfo is called...");
    String msg = this.adminService.deleteContactInfo(cid);
    return msg;
  }
  
  @RequestMapping({"/updateContactInfo"})
  @ResponseBody
  public String updateContactInfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("cid") int cid, @RequestParam("cname") String cname, @RequestParam("email") String email, @RequestParam("cno") long cno) throws ParseException {
    System.out.println("updateContactInfo is called...");
    String msg = this.adminService.updateContactInfo(cid, cname, email, cno);
    return msg;
  }
  
  @RequestMapping({"GetContact"})
  @ResponseBody
  public String GetContact(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("GetContact is Called : " + uid);
    List list = this.adminService.GetContact(uid);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/getFollowupsHistory"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getFollowupsHistory(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadid") int lead_id) throws ParseException {
    System.out.println("getFollowupsHistory is Called " + lead_id);
    List list = this.adminService.getFollowupsHistory(lead_id);
    System.out.println("list_size---" + list.size());
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/getfollowupsCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public int getfollowupsCount(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadid") int lead_id) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    System.out.println("getfollowupsCount is Called " + lead_id);
    int count = this.adminService.getfollowupsCount(lead_id);
    return count;
  }
  
  @RequestMapping(value = {"/AddCSVDataField"}, method = {RequestMethod.GET})
  @ResponseBody
  public String AddCSVDataField(HttpServletRequest request, HttpServletResponse response, @RequestParam("fieldname") String FieldnName, @RequestParam("rowfield") String RowField, @RequestParam("filepath") String FilePath, @RequestParam("cat") String cat, @RequestParam("indsys") String indSys, @RequestParam("autodial") String autodial) throws ParseException, IOException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long CompId = currentUser.getUserid();
    int telecallerId = 0;
    String SEP = System.getProperty("file.separator");
    String filePath = request.getServletContext().getRealPath("/");
    filePath = String.valueOf(filePath) + SEP + "UploadFile" + SEP;
    String[] row = null;
    int dplicaterow = 0;
    int newrow = 0;
    System.out.println("Path :::" + filePath + "   ----" + FilePath);
    CSVReader cr = new CSVReader(new FileReader(String.valueOf(filePath) + FilePath));
    String[] header = cr.readNext();
    String[] rwfiled = RowField.split(",");
    String[] filed = FieldnName.split(",");
    int len = filed.length;
    int cont = 0;
    int fnameIndex = 0;
    int lastnameIndex = 0;
    int emailIndex = 0;
    int mobIndex = 0;
    int webIndex = 0;
    int cnameIndex = 0;
    int contryIndex = 0;
    int stateIndex = 0;
    int cityIndex = 0;
    int tcallerIndex = 0;
    int catrgoryIndex = 0;
    String fnameVal = "N/A";
    String lastnameVal = "N/A";
    String emailVal = "N/A";
    String mobVal = "N/A";
    String webVal = "N/A";
    String cnameVal = "N/A";
    String contryVal = "N/A";
    String stateVal = "N/A";
    String cityVal = "N/A";
    String tcallerVal = "N/A";
    String catrgoryVal = "N/A";
    Map<Integer, String> hm = new HashMap<>();
    String[] filefiled = new String[header.length];
    for (int p1 = 0; p1 < header.length; p1++)
    	level1: filefiled[p1] = "" + p1;
    ArrayList<String> ar = new ArrayList<>();
    for (int i = 0; i < filefiled.length; i++) {
      if (!Arrays.<String>asList(rwfiled).contains(filefiled[i]))
        ar.add(filefiled[i]); 
    } 
    ArrayList<Integer> al = new ArrayList<>();
    for (String temp : ar)
      al.add(Integer.valueOf(Integer.parseInt(temp))); 
    for (int j = 0; j < len; j++) {
      if (filed[j].equalsIgnoreCase("firstName")) {
        fnameIndex = Integer.parseInt(rwfiled[j]);
        fnameVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("lastName")) {
        lastnameIndex = Integer.parseInt(rwfiled[j]);
        lastnameVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("email")) {
        emailIndex = Integer.parseInt(rwfiled[j]);
        emailVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("mobileNo")) {
        mobIndex = Integer.parseInt(rwfiled[j]);
        mobVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("website")) {
        webIndex = Integer.parseInt(rwfiled[j]);
        webVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("companyName")) {
        cnameIndex = Integer.parseInt(rwfiled[j]);
        cnameVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("Country")) {
        contryIndex = Integer.parseInt(rwfiled[j]);
        contryVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("state")) {
        stateIndex = Integer.parseInt(rwfiled[j]);
        stateVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("city")) {
        cityIndex = Integer.parseInt(rwfiled[j]);
        cityVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("city")) {
        cityIndex = Integer.parseInt(rwfiled[j]);
        cityVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("telecaller")) {
        System.out.println("IN telecaller");
        System.out.println("IN telecaller :: " + Integer.parseInt(rwfiled[j]));
        tcallerIndex = Integer.parseInt(rwfiled[j]);
        tcallerVal = "Selcted";
      } else if (filed[j].equalsIgnoreCase("category")) {
        catrgoryIndex = Integer.parseInt(rwfiled[j]);
        catrgoryVal = "Selcted";
      } 
    } 
    while ((row = cr.readNext()) != null) {
      JsonObject data = new JsonObject();
      String[] row1 = null;
      for (int k = 0; k < al.size(); k++) {
        List<ExtraFields> fields = this.adminService.getFieldsByUId((int)CompId, Integer.parseInt(cat));
        for (ExtraFields field : fields) {
          if (header[((Integer)al.get(k)).intValue()].toString().replaceAll("(\\r|\\n|\\t)", "").equalsIgnoreCase(field.getFieldName())) {
            data.addProperty(header[((Integer)al.get(k)).intValue()].toString().replaceAll("(\\r|\\n|\\t)", ""), row[((Integer)al.get(k)).intValue()].toString());
            continue;
          } 
          if (!data.has(field.getFieldName()))
            data.addProperty(field.getFieldName(), ""); 
        } 
      } 
      if (cont >= 0) {
        String fname = "N/A";
        String lastname = "N/A";
        String email = "N/A";
        String mob = "N/A";
        String web = "N/A";
        String cname = "N/A";
        String contry = "N/A";
        String state = "N/A";
        String city = "N/A";
        String category = "N/A";
        String tcaller = "N/A";
        if (fnameVal.equalsIgnoreCase("N/A")) {
          fname = "N/A";
        } else {
          fname = row[fnameIndex];
        } 
        if (lastnameVal.equalsIgnoreCase("N/A")) {
          lastname = "N/A";
        } else {
          lastname = row[lastnameIndex];
        } 
        if (mobVal.equalsIgnoreCase("N/A")) {
          mob = "N/A";
        } else {
          mob = row[mobIndex];
        } 
        if (emailVal.equalsIgnoreCase("N/A")) {
          email = "N/A";
        } else {
          email = row[emailIndex];
        } 
        if (webVal.equalsIgnoreCase("N/A")) {
          web = "N/A";
        } else {
          web = row[webIndex];
        } 
        if (cnameVal.equalsIgnoreCase("N/A")) {
          cname = "N/A";
        } else {
          cname = row[cnameIndex];
        } 
        if (contryVal.equalsIgnoreCase("N/A")) {
          contry = "N/A";
        } else {
          contry = row[contryIndex];
        } 
        if (stateVal.equalsIgnoreCase("N/A")) {
          state = "N/A";
        } else {
          state = row[stateIndex];
        } 
        if (cityVal.equalsIgnoreCase("N/A")) {
          cityVal = "N/A";
        } else {
          city = row[cityIndex];
        } 
        if (catrgoryVal.equalsIgnoreCase("N/A")) {
          catrgoryVal = "N/A";
        } else {
          category = row[catrgoryIndex];
        } 
        if (tcallerVal.equalsIgnoreCase("N/A")) {
          tcallerVal = "N/A";
        } else {
          List<Object[]> list = this.adminService.getTallyCallerByCompanyId((int)CompId);
          for (int ik = 0; ik < list.size(); ik++) {
            Object[] result = list.get(ik);
            String uname = result[1].toString();
            String tcname = row[tcallerIndex];
            if (uname.equals(tcname))
              telecallerId = Integer.parseInt(result[0].toString()); 
          } 
          tcaller = row[cityIndex];
        } 
        String mobNo1 = mob.replaceAll("[^0-9]", "");
        String subMob = null;
        if (indSys.toString().equalsIgnoreCase("1")) {
          System.out.println("mobNo::" + mob + "----" + "len::" + mob.length());
          if (mob.length() > 10) {
            String mobNo = mob.replaceAll("[^0-9]", "");
            String SubmobNo = null;
            if (mobNo.length() >= 10 && !Pattern.matches("[a-zA-Z]+", mobNo))
              SubmobNo = mobNo.substring(2, mobNo.length()); 
            subMob = SubmobNo;
          } else if (mob.length() == 10) {
            subMob = mob;
          } else {
            System.out.println("Discard Row Due to Invalid Mobile Number");
          } 
        } 
        if (subMob != null) {
          Lead lead = new Lead();
          List leadData1 = null;
          System.out.println("IF category :: " + CompId + " :: " + cat + " :: " + category + " :: " + subMob);
          if (!cat.equalsIgnoreCase("0") && category.equalsIgnoreCase("N/A")) {
            leadData1 = this.adminService.getLeadForCSV((int)CompId, cat, subMob);
            lead.setcategoryId(cat);
          } else if (cat.equalsIgnoreCase("0") && !category.equalsIgnoreCase("N/A")) {
            leadData1 = this.adminService.getLeadForCSV((int)CompId, category, subMob);
            lead.setcategoryId(category);
          } 
          String[] index = null;
          if (leadData1 != null && leadData1.size() == 0 && subMob.length() == 10) {
            newrow++;
            lead.setCompanyId((int)CompId);
            lead.setCreateDate(new Date());
            lead.setemail(email);
            lead.setFirstName(fname);
            lead.setLastName(lastname);
            lead.setLeadProcessStatus("None");
            lead.setMobileNo(subMob.trim());
            lead.setLeadState("Open");
            lead.setSheduleDate(this.scheduledf.parse("1980-06-30 17:36:00"));
            lead.setTallyCalletId(telecallerId);
            lead.setCsvData((new Gson()).toJson((JsonElement)data));
            lead.setLeadType("CSV");
            lead.setCountry(contry);
            lead.setState(state);
            lead.setCity(city);
            lead.setCompanyName(cname);
            lead.setWebsite(web);
            lead.setLeadcomment("N/A");
            lead.setTagName("N/A");
            lead.setNotification_flag("FALSE");
            lead.setAssignDate(new Date(1980, 6, 30, 17, 36, 0));
            if (autodial.toString().equalsIgnoreCase("1")) {
              lead.setAutoDial(true);
            } else {
              lead.setAutoDial(false);
            } 
            this.adminService.AddLead(lead);
            telecallerId = 0;
            fname = "N/A";
            lastname = "N/A";
            email = "N/A";
            mob = "N/A";
            web = "N/A";
            cname = "N/A";
            contry = "N/A";
            state = "N/A";
            city = "N/A";
            tcaller = "N/A";
          } 
        } 
      } else {
        dplicaterow++;
      } 
      cont++;
    } 
    cr.close();
    return "Success Row : " + newrow + " " + dplicaterow;
  }
  
  @RequestMapping(value = {"/assignLeadUpdate"}, method = {RequestMethod.GET})
  @ResponseBody
  public void assignLeadUpdate(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadid") Long lead_id, @RequestParam("tcid") Long TallyCaller_id) throws ParseException {
    System.out.println("UnassignLeadUpdate is Called " + lead_id + " " + TallyCaller_id);
    this.adminService.assignLeadUpdate(lead_id, TallyCaller_id);
  }
  
  @RequestMapping(value = {"/UnassignLeadUpdate"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UnassignLeadUpdate(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadid") Long lead_id) throws ParseException {
    System.out.println("UnassignLeadUpdate is Called " + lead_id);
    this.adminService.UnassignLeadUpdate(lead_id);
  }
  
  @RequestMapping(value = {"/getLeadForAssign"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getLeadForAssign(HttpServletRequest request, HttpServletResponse response, @RequestParam("tcid") Long Staff_id) throws ParseException {
    System.out.println("getLeadForAssign is Called " + Staff_id);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int companyId = (int)id;
    return (new Gson()).toJson(this.adminService.getLeadForAssign(Staff_id.longValue(), companyId));
  }
  
  @RequestMapping(value = {"/addTellyCaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public String addTellyCaller(HttpServletRequest request, HttpServletResponse response, @RequestParam("comastr") String ComaStr) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    System.out.println("AddTellyCaller :  " + ComaStr);
    String ststus = "";
    List<Object[]> list = new ArrayList();
    list = this.adminService.getTelecallerCount((int)id);
    System.out.println("listSize--- : " + list.size());
    String count = null;
    String username = null;
    for (Object[] result : list)
      count = result[0].toString().trim(); 
    System.out.println("count--- : " + count + "  " + "username-->>" + username);
    List<Object[]> list_limit = new ArrayList();
    list_limit = this.adminService.getUserLimit(Long.valueOf(id));
    System.out.println("listSize--- : " + list.size());
    String limit = null;
    String username1 = null;
    for (Object[] result_limit : list_limit) {
      limit = result_limit[0].toString().trim();
      username1 = result_limit[1].toString().trim();
    } 
    System.out.println("limit--- : " + limit);
    if (Integer.parseInt(count) >= Integer.parseInt(limit))
      return "1"; 
    String[] data = ComaStr.split(",");
    System.out.println(data[0]);
    System.out.println(data[1]);
    System.out.println(data[2]);
    System.out.println(data[3]);
    System.out.println(data[4]);
    Tallycaller tc = new Tallycaller();
    tc.setUsername(data[0]);
    tc.setPasswrd(data[1]);
    tc.setFirstname(data[2]);
    tc.setLastname(data[3]);
    tc.setMobno(data[5]);
    tc.setEmail(data[4]);
    tc.setCompanyId((int)id);
    tc.setActive(1);
    tc.setAllocate_staff_id(0);
    tc.setRegDate(new Date());
    try {
      this.adminService.addTallyCaller(tc);
      ststus = "0";
    } catch (Exception e) {
      ststus = "-1";
      System.out.println("Duplicate Entry Found ");
    } 
    return ststus;
  }
  
  @RequestMapping(value = {"/deletTellyCaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deletTellyCaller(HttpServletRequest request, HttpServletResponse response, @RequestParam("tcid") int tcId) throws ParseException {
    System.out.println("Delete TellyCaller is Called");
    this.adminService.deleteTellyCaller(tcId);
  }
  
  @RequestMapping(value = {"/UpdateTallycaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UpdateTallycaller(HttpServletRequest request, HttpServletResponse response, @RequestParam("comastr") String ComaStr) throws ParseException {
    System.out.println("Update TellyCaller :  " + ComaStr);
    this.adminService.UpdateTallycaller(ComaStr);
  }
  
  @RequestMapping(value = {"/GetTellyCaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTellyCaller(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    Gson gson = null;
    try {
      System.out.println("GettelluCaller Called  ");
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      MediUser currentUser = (MediUser)auth.getPrincipal();
      long id = currentUser.getUserid();
      System.out.println("GettelluCaller Called  " + id);
      List list = this.adminService.getTellyCaller(Long.valueOf(id));
      datatableJsonObject.setRecordsFiltered(list.size());
      datatableJsonObject.setRecordsTotal(list.size());
      datatableJsonObject.setData(list);
      gson = (new GsonBuilder()).setPrettyPrinting().create();
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/UpdateLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UpdateLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("comastr") String CommStr) throws ParseException {
    System.out.println("UpdateLead is Called::" + CommStr);
    String[] splitString = CommStr.split(",");
    int id = Integer.parseInt(splitString[0]);
    String fname = splitString[1];
    String lname = splitString[2];
    String email = splitString[3];
    String mobno = splitString[4];
    String cat = splitString[5];
    String sataus = splitString[6];
    String compName = splitString[7];
    String webAdd = splitString[8];
    String cont = splitString[9];
    String stat = splitString[10];
    String cty = splitString[11];
    String tcaller = splitString[12];
    String updatealtmobileNo = splitString[13];
    this.adminService.UpdateLead(id, fname, lname, email, mobno, cat, sataus, compName, webAdd, cont, stat, cty, 
        Integer.parseInt(tcaller), updatealtmobileNo);
  }
  
  @RequestMapping(value = {"/GetLeadState"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetLeadState(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetLeadLeadState is Called");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Id = (int)id;
    return this.adminService.GetLeadState(Long.valueOf(id));
  }
  
  @RequestMapping(value = {"/GetLeadCategoryName"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetLeadCategoryName(HttpServletRequest request, HttpServletResponse response, @RequestParam("lid") int leadId) throws ParseException {
    System.out.println("GetLeadCategoryName is Called");
    long id = leadId;
    System.out.println("Lead Id :" + id);
    return this.adminService.GetLeadCategoryName(Long.valueOf(id));
  }
  
  @RequestMapping(value = {"/deleteLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deleteLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadid") int leadId) throws ParseException {
    System.out.println("Delete Lead is Called");
    this.adminService.deleteLead(leadId);
  }
  
  @RequestMapping(value = {"/AssignLeadToTelecaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public void AssignLeadToTelecaller(HttpServletRequest request, HttpServletResponse response, @RequestParam("Cvale") String leadIds, @RequestParam("tcid") int tcid) throws ParseException {
    System.out.println("AssignLeadToTelecaller is Called");
    System.out.println("Lead IDS : " + leadIds);
    this.adminService.AssignLeadToTelecaller(Integer.parseInt(leadIds), tcid);
  }
  
  @RequestMapping(value = {"/DeleteLeadBeforeAssign"}, method = {RequestMethod.GET})
  @ResponseBody
  public void DeleteLeadBeforeAssign(HttpServletRequest request, HttpServletResponse response, @RequestParam("Cvale") String leadIds, @RequestParam("tcid") int tcid) throws ParseException {
    System.out.println("DeleteLeadBeforeAssign is Called");
    System.out.println("Lead IDS : " + leadIds);
    this.adminService.DeleteLeadBeforeAssign(Integer.parseInt(leadIds));
  }
  
  @RequestMapping(value = {"/CloseLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public void CloseLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("Cvale") String leadId) throws ParseException {
    System.out.println("CloseLead is Called");
    this.adminService.CloseLead(Integer.parseInt(leadId));
  }
  
  @RequestMapping(value = {"/GetLeadParameter"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetLeadParameter(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("GetLead   " + Company_id);
    List<Object[]> list = this.adminService.GetLeadParameter(Company_id);
    JSONArray jsonArray = new JSONArray();
    JSONObject responseDetailsJson = new JSONObject();
    for (int i = 0; i < list.size(); i++) {
      JSONObject data = new JSONObject();
      Object[] result = list.get(i);
      data.put("id", result[0]);
      data.put("name", result[1]);
      data.put("email", result[2]);
      data.put("mob", result[3]);
      data.put("cat", result[4]);
      data.put("creDate", result[5]);
      data.put("shDate", result[6]);
      data.put("pSatte", result[7]);
      data.put("CSV", result[8]);
      data.put("compWeb", result[9]);
      data.put("location", result[10]);
      jsonArray.add(data);
    } 
    responseDetailsJson.put("data", jsonArray);
    System.out.println(responseDetailsJson.toString());
    return list;
  }
  
  @RequestMapping(value = {"/GetLeadByCategoryId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetLeadByCategoryId(HttpServletRequest request, HttpServletResponse response, @RequestParam("cat") String category, @RequestParam("assign") String assinval) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    String query = null;
    List list = null;
    System.out.println("GetLeadByCategoryId   " + Company_id);
    if (!category.equalsIgnoreCase("0") && !assinval.equalsIgnoreCase("All")) {
      System.out.println("Both Selected Called : " + category + " " + Company_id);
      if (assinval.equalsIgnoreCase("All")) {
        query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
          Integer.parseInt(category) + 
          " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE lead.categoryId=" + 
          Integer.parseInt(category) + " AND lead.leadState='Open' AND  lead.companyId=" + Company_id;
      } else if (assinval.equalsIgnoreCase("Assign")) {
        query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
          Integer.parseInt(category) + 
          " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE lead.categoryId=" + 
          Integer.parseInt(category) + 
          " AND lead.leadState='Open' AND lead.tallyCalletId!=0 AND lead.companyId=" + Company_id;
      } else if (assinval.equalsIgnoreCase("Unassigned")) {
        query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
          Integer.parseInt(category) + 
          " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE lead.categoryId=" + 
          Integer.parseInt(category) + 
          " AND lead.tallyCalletId=0 AND lead.leadState='Open' AND lead.companyId=" + Company_id;
      } 
      System.out.println(query);
      list = this.adminService.GetLeadByCategoryId(query);
      System.out.println(query);
      System.out.println("List Size1 : " + list.size());
    } else if (!category.equalsIgnoreCase("0") && assinval.equalsIgnoreCase("All")) {
      System.out.println("Category Selected Called");
      query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
        Integer.parseInt(category) + 
        " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE lead.categoryId=" + 
        Integer.parseInt(category) + " AND lead.leadState='Open' AND lead.companyId=" + Company_id;
      list = this.adminService.GetLeadByCategoryId(query);
      System.out.println(query);
      System.out.println("List Size1 : " + list.size());
    } else if (category.equalsIgnoreCase("0") && !assinval.equalsIgnoreCase("All")) {
      System.out.println("Category Selected Called");
      if (assinval.equalsIgnoreCase("All")) {
        query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
          Integer.parseInt(category) + 
          " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE  lead.staffId=" + 
          Company_id;
      } else if (assinval.equalsIgnoreCase("Assign")) {
        query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
          Integer.parseInt(category) + 
          " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE lead.tallyCalletId!=0 AND lead.leadState='Open' AND lead.companyId=" + 
          Company_id;
      } else if (assinval.equalsIgnoreCase("Unassigned")) {
        query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
          Integer.parseInt(category) + 
          " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE lead.tallyCalletId=0 AND lead.leadState='Open' AND lead.companyId=" + 
          Company_id;
      } 
      System.out.println(query);
      list = this.adminService.GetLeadByCategoryId(query);
      System.out.println("List Size3 : " + list.size());
    } else if (assinval.equalsIgnoreCase("All") && category.equalsIgnoreCase("0")) {
      query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,(SELECT categortName FROM categorymanager WHERE categotyId=" + 
        Integer.parseInt(category) + 
        " )AS category,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead  WHERE  lead.leadState='Open' AND lead.companyId=" + 
        Company_id;
      System.out.println(query);
      list = this.adminService.GetLeadByCategoryId(query);
      System.out.println("List Size4 : " + list.size());
    } else {
      System.out.println("Both Not Selected Called");
    } 
    return list;
  }
  
  @RequestMapping(value = {"/GetLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLead(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("GetLead   " + Company_id);
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    try {
      if (request.getParameter("start") != null) {
        start = Integer.parseInt(request.getParameter("start"));
        System.out.println("STSRT IF : " + start);
      } else {
        System.out.println("STSRT ELSE: " + start);
      } 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        System.out.println("LENGTH IF: " + len);
        listSize = len;
      } else {
        System.out.println("LENGTH ELSE: " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("page::" + page + "----" + "listSize::" + listSize);
    List list = this.adminService.GetLead(Company_id, page, listSize);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"searchManagerLead"}, produces = {"application/javascript"})
  @ResponseBody
  public String searchManagerLead(HttpServletRequest request, HttpServletResponse response) throws Exception {
    System.out.println("searchManagerLead is called");
    String name = request.getParameter("name");
    String mobileNo = request.getParameter("mobileNo");
    String email = request.getParameter("email");
    String tallycallerName = request.getParameter("tallycallerName");
    String orderBy = request.getParameter("orderBy");
    String catNme = request.getParameter("catNme");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    JSONArray jarray = new JSONArray();
    int i = 0;
    String listData = "";
    String queryCount = "SELECT count(*) FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      Company_id;
    StringBuilder query = new StringBuilder();
    query.append("SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
        
        Company_id + " AND (");
    if (!name.equalsIgnoreCase("NA") || !name.equalsIgnoreCase("undefined") || !name.equalsIgnoreCase("null")) {
      i++;
      query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
    } 
    if (mobileNo != null && !mobileNo.equalsIgnoreCase("NA") && !mobileNo.equalsIgnoreCase("undefined") && !mobileNo.equalsIgnoreCase("null")) {
      i++;
      query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
    } 
    if (email != null && !email.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("undefined") && !email.equalsIgnoreCase("null")) {
      i++;
      query.append(" OR lead.email LIKE '%" + email + "%' ");
    } 
    if (catNme != null && !catNme.equalsIgnoreCase("NA") && !catNme.equalsIgnoreCase("undefined") && !catNme.equalsIgnoreCase("null")) {
      i++;
      query.append("  OR categorymanager.categortName LIKE '%" + catNme + "%' ");
    } 
    if (i == 0)
      query.append("  0=0 "); 
    query.append("   )");
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    String mob = "";
    Date fdate = null;
    Date todate = null;
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    listData = this.adminService.ConvertDataToWebJSON(queryCount, ""+query, page, listSize);
    if (listData.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + listData + ");"; 
      return listData;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"searchManagerLeadGubbaAPI"}, produces = {"application/javascript"})
  @ResponseBody
  public String searchManagerLeadGubbaAPI(HttpServletRequest request, HttpServletResponse response) throws Exception {
    System.out.println("searchManagerLead is called");
    String name = request.getParameter("name");
    String mobileNo = request.getParameter("mobileNo");
    String email = request.getParameter("email");
    String tallycallerName = request.getParameter("tallycallerName");
    String orderBy = request.getParameter("orderBy");
    String catNme = request.getParameter("catNme");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    JSONArray jarray = new JSONArray();
    int i = 0;
    String listData = "";
    String queryCount = "SELECT count(*) FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND lead.leadType='API' AND  lead.companyId=" + 
      Company_id;
    StringBuilder query = new StringBuilder();
    query.append("SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND lead.leadType='API' AND  lead.companyId=" + 
        
        Company_id + " AND (");
    if (!name.equalsIgnoreCase("NA") || !name.equalsIgnoreCase("undefined") || !name.equalsIgnoreCase("null")) {
      i++;
      query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
    } 
    if (mobileNo != null && !mobileNo.equalsIgnoreCase("NA") && !mobileNo.equalsIgnoreCase("undefined") && !mobileNo.equalsIgnoreCase("null")) {
      i++;
      query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
    } 
    if (email != null && !email.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("undefined") && !email.equalsIgnoreCase("null")) {
      i++;
      query.append(" OR lead.email LIKE '%" + email + "%' ");
    } 
    if (catNme != null && !catNme.equalsIgnoreCase("NA") && !catNme.equalsIgnoreCase("undefined") && !catNme.equalsIgnoreCase("null")) {
      i++;
      query.append("  OR categorymanager.categortName LIKE '%" + catNme + "%' ");
    } 
    if (i == 0)
      query.append("  0=0 "); 
    query.append("   )");
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    String mob = "";
    Date fdate = null;
    Date todate = null;
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    listData = this.adminService.ConvertDataToWebJSON(queryCount, ""+query, page, listSize);
    if (listData.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + listData + ");"; 
      return listData;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"/GetLeadNew"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadNew(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("GetLead   " + Company_id);
    JSONArray jarray = new JSONArray();
    String list = "";
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName,lead.altmobileNo FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id + " ORDER BY lead.createDate DESC";
    System.out.println(query);
    String queryCount = "SELECT count(*) FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id;
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    try {
      if (request.getParameter("start") != null) {
        start = Integer.parseInt(request.getParameter("start"));
        System.out.println("STSRT IF : " + start);
      } else {
        System.out.println("STSRT ELSE: " + start);
      } 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        System.out.println("LENGTH IF: " + len);
        listSize = len;
      } else {
        System.out.println("LENGTH ELSE: " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("page::" + page + "----" + "listSize::" + listSize);
    list = this.adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    if (list.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + list + ");"; 
      return list;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"/AddLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String AddLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("comastr") String ComaStr) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    User user = new User();
    user = this.adminService.getUserByUid(Long.valueOf(id));
    String[] data = ComaStr.split(",");
    int no = this.adminService.validateNoCategory(data[4], Integer.parseInt(data[5]));
    System.out.println("NUMBET -| " + no);
    if (no != 0)
      return "Lead is Already Available in Selected Category."; 
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    Date date = formatter.parse(data[6]);
    String sDate1 = "1980-06-30 17:36:00";
    Date date1 = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(sDate1);
    String csvJson = "{}";
    JSONObject jobj = new JSONObject();
    List<Object[]> exxtraparamsList = this.adminService.getExtraParamsByCat((int)id, Integer.parseInt(data[5]));
    if (exxtraparamsList.size() == 0) {
      csvJson = "{}";
    } else {
      for (Object[] obj : exxtraparamsList)
        jobj.put(obj[3].toString(), ""); 
      csvJson = jobj.toString();
    } 
    System.out.println("csvJson::-" + csvJson);
    Lead lead = new Lead();
    lead.setcategoryId(data[5]);
    lead.setstaffId(0);
    lead.setCreateDate(new Date());
    lead.setemail(data[3]);
    lead.setFirstName(data[1]);
    lead.setLastName(data[2]);
    lead.setLeadType("WEB");
    lead.setLeadProcessStatus("None");
    lead.setMobileNo(data[4]);
    lead.setLeadState("Open");
    lead.setSheduleDate((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse("1980-06-30 17:36:00"));
    lead.setCompanyId((int)id);
    lead.setTallyCalletId(Integer.parseInt(data[12]));
    lead.setCountry(data[7]);
    lead.setState(data[8]);
    lead.setCity(data[9]);
    lead.setCompanyName(data[10]);
    lead.setWebsite(data[11]);
    lead.setCsvData(csvJson);
    lead.setAltmobileNo(data[13]);
    this.adminService.AddLead(lead);
    return "Lead Successfully Added.";
  }
  
  @RequestMapping(value = {"/GetCategory"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetCategory(HttpServletRequest request, HttpServletResponse response, @RequestParam("compid") int Company_id) throws ParseException {
    System.out.println("GetCategory   " + Company_id);
    List s = this.adminService.GetCategory(Company_id);
    System.out.println("Gson Value : " + s);
    return s;
  }
  
  @RequestMapping(value = {"/GetCategoryById"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetCategoryById(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long companyId = currentUser.getUserid();
    int comId = (int)companyId;
    System.out.println("GetCategory   " + comId);
    List s = this.adminService.GetCategory(comId);
    System.out.println("Gson Value : " + s);
    return s;
  }
  
  @RequestMapping(value = {"/AddState"}, method = {RequestMethod.GET})
  @ResponseBody
  public void AddState(HttpServletRequest request, HttpServletResponse response, @RequestParam("statename") String stateName, @RequestParam("compid") int CompId) throws ParseException {
    System.out.println("Add State is Called");
    System.out.println("Cat Name : " + stateName);
    Leadprocesstate ldst = new Leadprocesstate();
    ldst.setstateName(stateName);
    ldst.setcompanyId(CompId);
    this.adminService.AddState(ldst);
  }
  
  @RequestMapping(value = {"/UpdateState"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UpdateState(HttpServletRequest request, HttpServletResponse response, @RequestParam("stateid") int SateId, @RequestParam("statename") String StateName, @RequestParam("compid") int CompId) throws ParseException {
    System.out.println("Update State is Called");
    System.out.println("SID= " + SateId + " " + "Name= " + StateName + " " + "UID= " + CompId);
    Leadprocesstate lps = new Leadprocesstate();
    lps.setstateId(SateId);
    lps.setstateName(StateName);
    lps.setcompanyId(CompId);
    this.adminService.UpdateLeadstate(lps);
  }
  
  @RequestMapping(value = {"/deleteState"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deleteState(HttpServletRequest request, HttpServletResponse response, @RequestParam("statetid") int StateId) throws ParseException {
    System.out.println("Delete State is Called");
    this.adminService.deleteState(StateId);
  }
  
  @RequestMapping(value = {"/GetAssignLeadState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAssignLeadState(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetAssignLeadState is Called ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    List list = this.adminService.GetAssignLeadState((int)id);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/AddCategory"}, method = {RequestMethod.GET})
  @ResponseBody
  public void AddCategory(HttpServletRequest request, HttpServletResponse response, @RequestParam("catname") String catName, @RequestParam("compid") Long CompId) throws ParseException {
    System.out.println("Add Cat is Called");
    System.out.println("Cat Name : " + catName);
    CategoryManager catmanager = new CategoryManager();
    catmanager.setCategortName(catName);
    catmanager.setCompanyId(CompId);
    this.adminService.AddCategory(catmanager);
  }
  
  @RequestMapping(value = {"/UpdateCategory"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UpdateCategory(HttpServletRequest request, HttpServletResponse response, @RequestParam("catid") Long CatId, @RequestParam("catname") String catName, @RequestParam("compid") Long CompId) throws ParseException {
    System.out.println("Update Cat is Called");
    CategoryManager catmanager = new CategoryManager();
    catmanager.setCategotyId(CatId);
    catmanager.setCategortName(catName);
    catmanager.setCompanyId(CompId);
    this.adminService.UpdateCategory(catmanager);
  }
  
  @RequestMapping(value = {"/deleteCategory"}, method = {RequestMethod.GET})
  @ResponseBody
  public void deleteCategory(HttpServletRequest request, HttpServletResponse response, @RequestParam("catid") Long CatId) throws ParseException {
    System.out.println("Delete Cat is Called");
    this.adminService.DeleteCategory(CatId);
  }
  
  @RequestMapping(value = {"/GetAssignCategory"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetAssignCategory(HttpServletRequest request, HttpServletResponse response) {
    System.out.println("GetAssignCategory is Called....");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    System.out.println("User Id " + currentUser.getUserid());
    List list = this.adminService.getCategoryList(Long.valueOf(id));
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/GetAssignTallyCallerStaff"}, method = {RequestMethod.GET})
  @ResponseBody
  public List GetAssignTallyCallerStaff(HttpServletRequest request, HttpServletResponse response, @RequestParam("tcid") Long TallyCaller_id) throws ParseException {
    System.out.println("GetAssignTallyCallerStaff   " + TallyCaller_id);
    List s = this.adminService.getAssignTellyCaller(TallyCaller_id);
    Gson gson = new Gson();
    String jsonCartList = gson.toJson(s);
    System.out.println("jsonCartList: " + jsonCartList);
    System.out.println("Gson Value : " + s);
    return s;
  }
  
  @RequestMapping(value = {"/UnassignTallyCallerStaff"}, method = {RequestMethod.GET})
  @ResponseBody
  public void UnassignTallyCallerStaff(HttpServletRequest request, HttpServletResponse response, @RequestParam("tcid") Long TallyCaller_id) throws ParseException {
    System.out.println(" TallyCaller ID : " + TallyCaller_id);
    System.out.println("Unassign Id" + TallyCaller_id);
    this.adminService.UnassignTallyCallerStaff(TallyCaller_id);
  }
  
  @RequestMapping(value = {"/assignTallyCallerStaff"}, method = {RequestMethod.GET})
  @ResponseBody
  public void assignTallyCallerStaff(HttpServletRequest request, HttpServletResponse response, @RequestParam("stffid") Long Staff_id, @RequestParam("tcid") Long TallyCaller_id) throws ParseException {
    System.out.println(" Staff ID : " + Staff_id);
    this.adminService.assignTallyCallerStaff(Staff_id, TallyCaller_id);
  }
  
  @RequestMapping(value = {"/getTallyCallerByCompanyId"}, method = {RequestMethod.GET})
  @ResponseBody
  public List getTallyCallerByCompanyId(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long companyId = currentUser.getUserid();
    int comId = (int)companyId;
    List list = this.adminService.getTallyCallerByCompanyId(comId);
    return list;
  }
  
  @RequestMapping(value = {"/getTallyCaller"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getTallyCaller(HttpServletRequest request, HttpServletResponse response, @RequestParam("tcid") Long TallyCaller_id) throws ParseException {
    return (new Gson()).toJson(this.adminService.getTeleCallerForAdmin(TallyCaller_id.longValue()));
  }
  
  @RequestMapping(value = {"registerUserAdmin"}, method = {RequestMethod.GET})
  @ResponseBody
  public String registerUserAdmin(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    String username = request.getParameter("username");
    String pass = request.getParameter("password");
    String email = request.getParameter("email");
    String companyName = request.getParameter("companyName");
    String name = request.getParameter("name");
    String password = (new SecurityConfig()).passwordEncoder().encode(pass);
    String address = request.getParameter("address");
    String number = request.getParameter("number");
    String city = request.getParameter("city");
    int minimumnumber = Integer.parseInt(request.getParameter("minimumnumber"));
    int cutting = Integer.parseInt(request.getParameter("cutting"));
    int stafflimit = Integer.parseInt(request.getParameter("minimumnumber"));
    int talcallimt = Integer.parseInt(request.getParameter("cutting"));
    User user = new User();
    DateFormat readFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date validity = readFormat.parse(request.getParameter("datepicker"));
    if (currentUser.getUserid() == 1L) {
      user.setCutting(cutting);
      user.setMinNumber(minimumnumber);
    } else {
      user.setCutting(currentUser.getCutting());
      user.setMinNumber(currentUser.getMinnumber());
    } 
    user.setActive(true);
    user.setAddBy(1L);
    user.setAddress(address);
    user.setCmpName(companyName);
    user.setCountry("India");
    user.setDefaultSenderId(1L);
    user.setDlrReport("YES");
    user.setEmail(email);
    user.setEnabled(true);
    user.setExpireDate(validity);
    user.setMaxUser(0L);
    user.setMobileNumber(number);
    user.setName(name);
    user.setParentId(currentUser.getUserid());
    user.setPassword(password);
    user.setPriority(1);
    user.setRegDate(new Date());
    user.setUsername(username);
    user.setCity(city);
    user.setLimit_staff(stafflimit);
    user.setLimit_tally_caller(talcallimt);
    user.setCmpName("N/A");
    user.setMasterPassword("12345");
    try {
      Long user2 = this.userService.saveRegUser(user);
      EmailTemplate emailTemplate = new EmailTemplate();
      emailTemplate.setUid(user2.longValue());
      emailTemplate.setMessage(
          "welcome USER your username is : <b> <UserName> </b> and <b>password </b>is : <Password> ");
      emailTemplate.setSubject("Welcome Message");
      emailTemplate.setTemplateName("CreateUser");
      emailTemplate.setIshidden(true);
      this.userService.saveEmailTemplate(emailTemplate);
      EmailTemplate emailTemplate2 = new EmailTemplate();
      emailTemplate2.setUid(user2.longValue());
      emailTemplate2.setMessage("SenderName: <b> <SenderName> </b> is <Action>");
      emailTemplate2.setSubject("you have requested SenderName:<SenderName>");
      emailTemplate2.setTemplateName("SenderName");
      emailTemplate2.setIshidden(true);
      this.userService.saveEmailTemplate(emailTemplate2);
      emailTemplate2 = new EmailTemplate();
      emailTemplate2.setUid(user2.longValue());
      emailTemplate2.setMessage("Template: <b> <Template> </b> is <Action>");
      emailTemplate2.setSubject("Template: <b> <Template> </b> is Request");
      emailTemplate2.setTemplateName("Template");
      emailTemplate2.setIshidden(true);
      this.userService.saveEmailTemplate(emailTemplate2);
      emailTemplate = new EmailTemplate();
      emailTemplate.setUid(user2.longValue());
      emailTemplate2.setIshidden(true);
      emailTemplate.setMessage(" Your SmsCredit is : <b> <SmsCredit> </b>  ");
      emailTemplate.setSubject("Credit Info");
      emailTemplate.setTemplateName("Credit");
      this.userService.saveEmailTemplate(emailTemplate);
      MessageTemplate messageTemplate = new MessageTemplate();
      messageTemplate.setMessage(
          "welcome USER your username is : <b> <UserName> </b> and <b>password </b>is : <Password> ");
      messageTemplate.setTemplateName("CreateUser");
      messageTemplate.setIshidden(true);
      messageTemplate.setUid(user2.longValue());
      this.userService.saveMessageTemplate(messageTemplate);
      MessageTemplate messageTemplate2 = new MessageTemplate();
      messageTemplate2.setMessage("SenderName: <b> <SenderName> </b> is <Action> ");
      messageTemplate2.setIshidden(true);
      messageTemplate2.setUid(user2.longValue());
      messageTemplate2.setTemplateName("SenderName");
      this.userService.saveMessageTemplate(messageTemplate2);
      messageTemplate2 = new MessageTemplate();
      messageTemplate2 = new MessageTemplate();
      messageTemplate2.setUid(user2.longValue());
      messageTemplate2.setIshidden(true);
      messageTemplate2.setMessage(" Your Template  <b> <Template> is Created And Status : </b>  ");
      messageTemplate2.setTemplateName("Template");
      this.userService.saveMessageTemplate(messageTemplate2);
      try {
        smsAndEmailSend(user, pass, "CreateUser", "");
      } catch (Exception ex) {
        System.out.println(ex.getMessage());
      } 
    } catch (Exception e) {
      e.printStackTrace();
      return "UserName Already Exists ";
    } 
    User usr = this.adminService.getUserByUserName(currentUser.getUsername());
    String logUser = usr.getUsername();
    System.out.println("User : " + usr.getUsername());
    String rol = "";
    try {
      if (logUser.equalsIgnoreCase("admin")) {
        rol = "ROLE_COMPANY";
      } else {
        rol = "ROLE_STAFF";
      } 
      UserRole role = new UserRole();
      role.setRole("ROLE_COMPANY");
      role.setUser(user);
      this.userService.addUserRole(role);
    } catch (Exception e) {
      System.out.println("Error in Role " + e);
    } 
    return String.valueOf(user.getUsername()) + " Created Successfully...!";
  }
  
  @RequestMapping(value = {"registerTallyCallerAdmin"}, method = {RequestMethod.GET})
  @ResponseBody
  public String registerTallyCallerAdmin(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    try {
      System.out.println(" is calledregisterTallyCallerAdmin");
      String username = request.getParameter("username");
      String pass = request.getParameter("password");
      String email = request.getParameter("email");
      String name = request.getParameter("name");
      String lname = request.getParameter("lname");
      String password = (new SecurityConfig()).passwordEncoder().encode(pass);
      String number = request.getParameter("number");
      System.out.println(String.valueOf(username) + " " + pass + " " + name + " " + lname + " " + number);
      Tallycaller tcaller = new Tallycaller();
      tcaller.setUsername(username);
      tcaller.setPasswrd(password);
      tcaller.setFirstname(name);
      tcaller.setLastname(lname);
      tcaller.setMobno(number);
      tcaller.setEmail(email);
      tcaller.setRegDate(new Date());
      this.adminService.addTallyCaller(tcaller);
    } catch (Exception e) {
      e.printStackTrace();
      return "UserName Already Exists ";
    } 
    return "success";
  }
  
  @RequestMapping(value = {"updateUserAdmin"}, method = {RequestMethod.GET})
  @ResponseBody
  public String updateUserAdmin(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println(".....regUser........");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    String username = request.getParameter("username");
    String pass = request.getParameter("password");
    String email = request.getParameter("email");
    String companyName = request.getParameter("companyName");
    String name = request.getParameter("name");
    String uid = request.getParameter("uid");
    Long uidl = Long.valueOf(Long.parseLong(uid));
    DateFormat readFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String address = request.getParameter("address");
    String number = request.getParameter("number");
    String city = request.getParameter("city");
    int minimumnumber = Integer.parseInt(request.getParameter("minimumnumber"));
    int cutting = Integer.parseInt(request.getParameter("cutting"));
    try {
      date = readFormat.parse(request.getParameter("datepicker"));
    } catch (ParseException e) {
      e.printStackTrace();
    } 
    if (currentUser.getUserid() == 1L) {
      List<User> ulst = this.adminService.getChildByUid(Long.parseLong(uid));
      for (int k = 0; k < ulst.size(); k++) {
        User usr = ulst.get(k);
        usr.setMinNumber(minimumnumber);
        usr.setCutting(cutting);
        this.userService.updateUser(usr);
        List<User> sulst = this.adminService.getChildByUid(usr.getUid());
        for (int l = 0; l < sulst.size(); l++) {
          User susr = ulst.get(l);
          susr.setMinNumber(minimumnumber);
          susr.setCutting(cutting);
          this.userService.updateUser(susr);
          List<User> ssulst = this.adminService.getChildByUid(usr.getUid());
          for (int m = 0; m < ssulst.size(); m++) {
            User ssusr = ulst.get(l);
            ssusr.setMinNumber(minimumnumber);
            ssusr.setCutting(cutting);
            this.userService.updateUser(ssusr);
            List<User> sssulst = this.adminService.getChildByUid(usr.getUid());
            for (int n = 0; n < sssulst.size(); n++) {
              User sssusr = ulst.get(l);
              sssusr.setMinNumber(minimumnumber);
              sssusr.setCutting(cutting);
              this.userService.updateUser(sssusr);
            } 
          } 
        } 
      } 
    } 
    User user = new User();
    user = this.adminService.getUserByUid(uidl);
    user.setUsername(username);
    user.setCutting(cutting);
    user.setMinNumber(minimumnumber);
    user.setActive(true);
    user.setAddBy(1L);
    user.setAddress(address);
    user.setCmpName(companyName);
    user.setCountry("India");
    user.setDefaultSenderId(1L);
    user.setDlrReport("YES");
    user.setEmail(email);
    user.setCity(city);
    user.setEnabled(true);
    user.setExpireDate(date);
    user.setMaxUser(0L);
    user.setMobileNumber(number);
    user.setName(name);
    user.setCity(city);
    user.setLimit_tally_caller(cutting);
    this.userService.updateUser(user);
    return String.valueOf(user.getUsername()) + " Updated Successfully...!!!";
  }
  
  @RequestMapping(value = {"assignCredit"}, method = {RequestMethod.POST})
  @ResponseBody
  public String assignCredit(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "uid", required = true) Long uid, @RequestParam(value = "stid", required = true) Long stid) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    System.out.println("In assignCredit" + currentUser.getParentid());
    Long credit = Long.valueOf(Long.parseLong(request.getParameter("credit")));
    String validity = request.getParameter("validity");
    String comments = request.getParameter("comments");
    SmsCredit smsCreditParent = new SmsCredit();
    String transtype = (credit.longValue() > 0L) ? "CREDIT" : "DEBIT";
    ServiceType sc = this.userService.getServiceTypeById(stid);
    User user = new User();
    smsCreditParent = this.adminService.getSmsCredit(currentUser.getUserid(), stid.longValue());
    if (smsCreditParent.getSmsCredit() > credit.longValue()) {
      smsCreditParent.setSmsCredit(smsCreditParent.getSmsCredit() - credit.longValue());
      SmsCredit childCredit = this.adminService.getSmsCredit(uid.longValue(), stid.longValue());
      if (childCredit != null) {
        SmsCredit smsCredit = new SmsCredit();
        smsCredit = this.adminService.getSmsCredit(uid.longValue(), stid.longValue());
        long tempcrd = smsCredit.getSmsCredit();
        smsCredit.setSmsCredit(credit.longValue() + smsCredit.getSmsCredit());
        smsCredit.setValidity(sdf.parse(validity));
        try {
          this.adminService.updateAssignCredit(smsCredit);
        } catch (Exception ex) {
          ex.printStackTrace();
          return "-2";
        } 
        this.adminService.updateAssignCredit(smsCreditParent);
        MessageAudit messageAudit = new MessageAudit();
        messageAudit.setComments(comments);
        messageAudit.setLastCridit(tempcrd);
        messageAudit.setLastValidity(new Date());
        messageAudit.setNewCridit(credit.longValue());
        messageAudit.setNewValidity(sdf.parse(validity));
        messageAudit.setServiceid(sc.getType());
        messageAudit.setUid(uid.longValue());
        messageAudit.setUserid(currentUser.getParentid());
        messageAudit.setTranstime(new Date());
        messageAudit.setTranstype(transtype);
        this.adminService.saveMessageAudit(messageAudit);
        user = this.adminService.getUserByUid(uid);
        User userparent = this.adminService.getUserByUid(Long.valueOf(currentUser.getUserid()));
        EmailTemplate et = new EmailTemplate();
        et.setMessage("Dear Client, Your Account UserName:<b>" + user.getUsername() + 
            "</b> is Credited with <b> " + credit + "</b>");
        et.setSubject("Account Credit Notification");
        if (StringUtils.isEmailValid(user.getEmail()))
          (new Thread((Runnable)new EmailSenderThread(et, this.smsService.getHostSetting("admin"), user.getEmail()))).start(); 
        if (StringUtils.isEmailValid(userparent.getEmail()))
          (new Thread((Runnable)new EmailSenderThread(et, this.smsService.getHostSetting("admin"), userparent.getEmail())))
            .start(); 
      } else {
        if (credit.longValue() < 0L)
          return "-2"; 
        SmsCredit smsCredit = new SmsCredit();
        smsCredit.setStId(stid.longValue());
        smsCredit.setSmsCredit(credit.longValue());
        smsCredit.setuId(uid.longValue());
        smsCredit.setValidity(sdf.parse(validity));
        this.adminService.updateAssignCredit(smsCredit);
        this.adminService.updateAssignCredit(smsCreditParent);
        MessageAudit messageAudit = new MessageAudit();
        messageAudit.setComments(comments);
        messageAudit.setLastCridit(0L);
        messageAudit.setLastValidity(new Date());
        messageAudit.setNewCridit(credit.longValue());
        messageAudit.setNewValidity(sdf.parse(validity));
        messageAudit.setServiceid(sc.getType());
        messageAudit.setUid(uid.longValue());
        messageAudit.setUserid(currentUser.getParentid());
        messageAudit.setTranstime(new Date());
        messageAudit.setTranstype(transtype);
        this.adminService.saveMessageAudit(messageAudit);
        user = this.adminService.getUserByUid(uid);
        User userparent = this.adminService.getUserByUid(Long.valueOf(currentUser.getUserid()));
        try {
          smsAndEmailSend(user, 
              " : " + credit.toString() + " Topup Successfully. And New Balance : " + credit, "Credit", 
              "");
          EmailTemplate et = new EmailTemplate();
          et.setMessage("Dear Client, Your Account UserName:<b>" + user.getUsername() + 
              "</b> is Credited with <b> " + credit + "</b>");
          et.setSubject("Account Credit Notification");
          if (StringUtils.isEmailValid(user.getEmail()))
            (new Thread((Runnable)new EmailSenderThread(et, this.smsService.getHostSetting("admin"), user.getEmail())))
              .start(); 
          if (StringUtils.isEmailValid(userparent.getEmail()))
            (new Thread((Runnable)new EmailSenderThread(et, this.smsService.getHostSetting("admin"), userparent.getEmail())))
              .start(); 
        } catch (Exception ex) {
          System.out.println("ERROR IN CREDIT:" + ex.getMessage());
        } 
      } 
    } else {
      return "-1";
    } 
    if (credit.longValue() < 0L)
      return "2"; 
    return "1";
  }
  
  @RequestMapping(value = {"deleteUser"}, method = {RequestMethod.GET})
  public String deleteUser(HttpServletRequest request, HttpServletResponse response) {
    Long id = Long.valueOf(Long.parseLong(request.getParameter("uid")));
    System.out.println("delete id" + id);
    this.adminService.deleteUserRole(Integer.parseInt(request.getParameter("uid")));
    List<SmsCredit> smsCredit = new ArrayList<>();
    smsCredit = this.adminService.getSmsCreditByUid(id);
    User user = this.adminService.getUserByUid(id);
    this.adminService.deleteUser(id);
    this.adminService.deleteEmailTemplate(id);
    this.adminService.deleteMessageTemplate(id);
    StringBuilder sb = new StringBuilder();
    sb.append("USER::" + user.getUsername() + "::" + user.getUid() + ":::is Deleted::");
    for (int ii = 0; ii < smsCredit.size(); ii++) {
      SmsCredit sc = smsCredit.get(ii);
      this.smsService.creditUpdate((int)sc.getSmsCredit(), Long.valueOf(user.getParentId()), Long.valueOf(sc.getStId()));
      sb.append("Service:" + sc.getStId() + "==Credited::" + sc.getSmsCredit() + "::");
    } 
    SystemNotification snf = new SystemNotification();
    snf.setSysLogCount("");
    snf.setSysLogDate(new Date());
    snf.setSysLogText(sb.toString());
    snf.setSysLogType("DELETEUSER");
    this.adminService.saveSystemNotification(snf);
    System.out.println("In...deleteUser");
    return "redirect:/#/tables/SaveUserAdmin";
  }
  
  @RequestMapping(value = {"deleteCredit"}, method = {RequestMethod.GET})
  public String deleteCredit(HttpServletRequest request, HttpServletResponse response) {
    Long id = Long.valueOf(Long.parseLong(request.getParameter("scid")));
    System.out.println("delete id" + id);
    this.adminService.deleteCredit(id);
    System.out.println("In...deleteUser");
    return "redirect:/#/tables/Credit";
  }
  
  @RequestMapping(value = {"createSenderName"}, method = {RequestMethod.POST})
  @ResponseBody
  public String createSenderName(HttpServletRequest request, HttpServletResponse response) {
    System.out.println("createSenderName : ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    Long id = Long.valueOf(currentUser.getUserid());
    int uid = Integer.parseInt(request.getParameter("uid"));
    String senderName = request.getParameter("senderName");
    String service = request.getParameter("sndrservice");
    SenderName senderName2 = new SenderName();
    if (currentUser.getUsername().equals("admin")) {
      senderName2.setUid(uid);
    } else {
      senderName2.setUid(id.longValue());
    } 
    senderName2.setSenderName(senderName);
    senderName2.setIsActive(0);
    senderName2.setService((service == null) ? "N/A" : service);
    senderName2.setSubmitDate(new Date());
    this.adminService.saveSenderName(senderName2);
    User user = new User();
    user = this.adminService.getUserByUid(Long.valueOf(currentUser.getParentid()));
    try {
      smsAndEmailSend(user, senderName, "SenderName", "Pennding");
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
    return "Sucessfully Submitted !";
  }
  
  @RequestMapping(value = {"updateSenderName"}, method = {RequestMethod.POST})
  @ResponseBody
  public String updateSenderName(HttpServletRequest request, HttpServletResponse response) {
    MediUser currentUser = (MediUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    if (currentUser == null)
      return "Invalid Access"; 
    Long id = Long.valueOf(Long.parseLong(request.getParameter("sid")));
    String senderName = request.getParameter("senderName");
    SenderName senderName2 = new SenderName();
    senderName2 = this.adminService.getSenderNameById(id);
    senderName2.setSenderName(senderName);
    this.adminService.updateSenderName(senderName2);
    return "Sucessfully Updated !";
  }
  
  @RequestMapping(value = {"approveSenderName"}, method = {RequestMethod.GET})
  @ResponseBody
  public String approveSenderName(HttpServletRequest request, HttpServletResponse response) {
    Long sid = Long.valueOf(Long.parseLong(request.getParameter("sid")));
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    Long id = Long.valueOf(currentUser.getUserid());
    System.out.println("approveSenderName +-----------" + sid + "-----" + request.getParameter("option"));
    int option = Integer.parseInt(request.getParameter("option"));
    SenderName senderName = new SenderName();
    senderName = this.adminService.getSenderNameById(sid);
    senderName.setIsActive(option);
    senderName.setApproveDate(new Date());
    this.adminService.saveSenderName(senderName);
    User user = new User();
    user = this.adminService.getUserByUid(Long.valueOf(senderName.getUid()));
    System.out.println("option : " + option);
    try {
      if (option == 0) {
        smsAndEmailSend(user, senderName.getSenderName(), "SenderName", "Pending...");
      } else if (option == 1) {
        smsAndEmailSend(user, senderName.getSenderName(), "SenderName", "Approved...");
      } else if (option == 2) {
        smsAndEmailSend(user, senderName.getSenderName(), "SenderName", "Rejected...");
      } 
    } catch (Exception exception) {}
    return String.valueOf(senderName.getSenderName()) + " SuccessFully ";
  }
  
  @RequestMapping(value = {"deleteSenderName"}, method = {RequestMethod.GET})
  @ResponseBody
  public String deleteSenderName(HttpServletRequest request, HttpServletResponse response) {
    System.out.println("-------------------------------------" + request.getParameter("tid"));
    Long sid = Long.valueOf(Long.parseLong(request.getParameter("sid")));
    System.out.println("deleteSenderName" + sid);
    this.adminService.deleteSenderName(sid);
    return " Record SuccessFully ";
  }
  
  @RequestMapping(value = {"createTamplate"}, method = {RequestMethod.POST})
  @ResponseBody
  public String createTamplate(HttpServletRequest request, HttpServletResponse response) {
    try {
      System.out.println("createSenderName : ");
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      MediUser currentUser = (MediUser)auth.getPrincipal();
      Long uid = Long.valueOf(currentUser.getUserid());
      String tname = request.getParameter("tname");
      String tmessage = request.getParameter("tmessage");
      Tamplate tamplate = new Tamplate();
      tamplate.setIsactive(0);
      tamplate.setMessage(tmessage);
      tamplate.setTname(tname);
      tamplate.setUid(uid.longValue());
      boolean istrans = request.getParameter("istrans").equalsIgnoreCase("true");
      tamplate.setIstrans(Boolean.valueOf(request.getParameter("istrans").equalsIgnoreCase("true")));
      this.adminService.saveOrUpdateTamplate(tamplate);
      User user = new User();
      user = this.adminService.getUserByUid(uid);
      System.out.println("------------------>" + user.getUsername());
      if (istrans)
        smsAndEmailSend(user, tname, "Template", "Pennding"); 
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return "Tamplate Created SuccessFully.!!!";
  }
  
  @RequestMapping(value = {"updateTamplate"}, method = {RequestMethod.POST})
  @ResponseBody
  public String s(HttpServletRequest request, HttpServletResponse response) {
    try {
      long tid = Long.parseLong(request.getParameter("tid"));
      Tamplate tamplate = new Tamplate();
      tamplate = this.adminService.getTamplateById(Long.valueOf(tid));
      String tname = request.getParameter("tname");
      String tmessage = request.getParameter("tmessage");
      tamplate.setMessage(tmessage);
      tamplate.setTname(tname);
      this.adminService.saveOrUpdateTamplate(tamplate);
    } catch (Exception e) {
      return "Already Exist";
    } 
    return "Template Updated SuccessFully !";
  }
  
  @RequestMapping(value = {"approveTamplate"}, method = {RequestMethod.GET})
  @ResponseBody
  public String approveTamplate(HttpServletRequest request, HttpServletResponse response) {
    System.out.println("option   : " + request.getParameter("option"));
    System.out.println("approveTamplate   : " + request.getParameter("tid"));
    Long tid = Long.valueOf(Long.parseLong(request.getParameter("tid")));
    Tamplate tamplate = new Tamplate();
    tamplate = this.adminService.getTamplateById(tid);
    tamplate.setIsactive(Integer.parseInt(request.getParameter("option")));
    this.adminService.saveOrUpdateTamplate(tamplate);
    User user = new User();
    user = this.adminService.getUserByUid(Long.valueOf(tamplate.getUid()));
    if (request.getParameter("option").equalsIgnoreCase("0")) {
      smsAndEmailSend(user, tamplate.getTname(), "Template", "Pending...");
    } else if (request.getParameter("option").equalsIgnoreCase("1")) {
      smsAndEmailSend(user, tamplate.getTname(), "Template", "Approved...");
    } else if (request.getParameter("option").equalsIgnoreCase("2")) {
      smsAndEmailSend(user, tamplate.getTname(), "Template", "Rejected...");
    } 
    return "Succeffuly..!!!";
  }
  
  @RequestMapping(value = {"deleteTamplate"}, method = {RequestMethod.POST})
  @ResponseBody
  public String deleteTamplate(HttpServletRequest request, HttpServletResponse response) {
    Long tid = Long.valueOf(Long.parseLong(request.getParameter("tid")));
    System.out.println("deleteTamplate" + tid);
    this.adminService.deleteTamplate(tid);
    return "Tamplate ";
  }
  
  @RequestMapping(value = {"createApiKey"}, method = {RequestMethod.POST})
  @ResponseBody
  public String createApiKey(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    Long uid = Long.valueOf(currentUser.getUserid());
    String uuid = UUID.randomUUID().toString();
    System.out.println("uuid = " + uuid);
    ApiKey apiKey = new ApiKey();
    apiKey.setCreateDate(new Date());
    apiKey.setUid(uid.longValue());
    apiKey.setIp(request.getParameter("ip"));
    apiKey.setKeyValue(uuid);
    apiKey.setIshidden(Boolean.valueOf(false));
    this.userService.saveApiKey(apiKey);
    return uuid;
  }
  
  @RequestMapping(value = {"getCreditbyUid"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getCreditbyUid(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    System.out.println("=:::::==:" + currentUser.getUserid());
    List crdList = this.adminService.getCreditByUid(currentUser.getUserid());
    DatatableJsonObject personJsonObject = new DatatableJsonObject();
    personJsonObject.setRecordsFiltered(crdList.size());
    personJsonObject.setRecordsTotal(crdList.size());
    personJsonObject.setData(crdList);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String json2 = gson.toJson(personJsonObject);
    return json2;
  }
  
  @RequestMapping(value = {"getUserList"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getUserList(HttpServletRequest request, HttpServletResponse response) {
    Long uids = Long.valueOf(0L);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    if (request.getParameter("uids") != null && currentUser.getUserid() == 1L) {
      uids = Long.valueOf(Long.parseLong(request.getParameter("uids")));
    } else {
      uids = Long.valueOf(currentUser.getUserid());
    } 
    List list = this.adminService.getUsersByUid(uids.longValue());
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"getTelecallerList"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getTelecallerList(HttpServletRequest request, HttpServletResponse response) {
    Long uids = Long.valueOf(0L);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    if (request.getParameter("uids") != null && currentUser.getUserid() == 1L) {
      uids = Long.valueOf(Long.parseLong(request.getParameter("uids")));
    } else {
      uids = Long.valueOf(currentUser.getUserid());
    } 
    List list = this.adminService.getTeleCallerByUid(uids.longValue());
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"getCreditDatabyUid"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getCreditDatabyUid(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    List list = this.adminService.getCreditByUid(currentUser.getUserid());
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"getSenderName"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getSenderName(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    List list = null;
    if (currentUser.getUsername().equalsIgnoreCase("admin")) {
      long l = 1L;
      list = this.adminService.getSenderNamesByUId(l);
    } else {
      list = this.adminService.getSenderNamesByUId(currentUser.getUserid());
    } 
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"getTamplates"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getTamplates(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    List list = null;
    if (currentUser.getUsername().equalsIgnoreCase("admin")) {
      long l = 0L;
      list = this.adminService.getTamplatesByUid(l, request.getParameter("istrans").equals("true"));
    } else {
      list = this.adminService.getTamplatesByUid(currentUser.getUserid(), 
          request.getParameter("istrans").equals("true"));
    } 
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"createGroup"}, method = {RequestMethod.GET})
  @ResponseBody
  public String createGroup(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    Long id = Long.valueOf(currentUser.getUserid());
    String groupName = request.getParameter("groupname");
    String type = request.getParameter("type");
    System.out.println("create group :=============: " + type + "-----" + groupName + "-----" + type);
    GroupName grouname = new GroupName();
    grouname.setGroupName(groupName);
    grouname.setType(type);
    grouname.setUid(id.longValue());
    grouname.setDate(new Date());
    try {
      this.adminService.saveGroup(grouname);
      return "SuccessFully Saved!";
    } catch (Exception ex) {
      return "Already Exist!";
    } 
  }
  
  @RequestMapping(value = {"updateGroup"}, method = {RequestMethod.POST})
  @ResponseBody
  public String updateGroup(HttpServletRequest request, HttpServletResponse response) {
    System.out.println(String.valueOf(request.getParameter("id")) + "---" + request.getParameter("gid"));
    Long id = Long.valueOf(Long.parseLong(request.getParameter("gid")));
    String groupName = request.getParameter("groupname");
    String type = request.getParameter("type");
    System.out.println("create group :=============: " + type + "-----" + groupName + "-----" + type);
    GroupName grouname = new GroupName();
    grouname = this.adminService.getGroupNameByGId(id);
    grouname.setGroupName(groupName);
    grouname.setType(type);
    grouname.setDate(new Date());
    this.adminService.updateGroup(grouname);
    return "Update Group Successfully";
  }
  
  @RequestMapping(value = {"deleteGroup"}, method = {RequestMethod.GET})
  @ResponseBody
  public String deleteGroup(HttpServletRequest request, HttpServletResponse response) {
    Long id = Long.valueOf(Long.parseLong(request.getParameter("gid")));
    System.out.println("delete gid" + id);
    this.adminService.deleteGroupByGroupId(id);
    return "1";
  }
  
  @RequestMapping(value = {"getGrupnames"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getGrupnames(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    List list = null;
    list = this.adminService.getGroupNamesByUId(currentUser.getUserid());
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"saveContect"}, method = {RequestMethod.POST})
  @ResponseBody
  public String saveContect(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile file) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    Long id = Long.valueOf(currentUser.getUserid());
    String gid = request.getParameter("gid");
    String name = request.getParameter("name");
    String number = request.getParameter("number");
    String email = request.getParameter("email");
    String fileName = null;
    String filePath = null;
    Gson gson = new Gson();
    JsonObject jo = new JsonObject();
    filePath = request.getServletContext().getRealPath("/");
    filePath = String.valueOf(filePath) + "\\UploadFile\\";
    System.out.println("filePath" + filePath);
    if (!file.isEmpty()) {
      try {
        fileName = file.getOriginalFilename();
        int invald = 0;
        if (fileName.endsWith(".csv") || fileName.endsWith(".CSV")) {
          System.out.println("in csv");
          File fileToCreate = new File(filePath, "file.xls");
          FileUtils.copyFile(convert(file), fileToCreate);
          int check = 0;
          String[] row = null;
          CSVReader csvReader = null;
          csvReader = new CSVReader(new FileReader(fileToCreate));
          List content = csvReader.readAll();
          List<Contacts> contacts = new ArrayList<>();
          for (Object object : content) {
            row = (String[])object;
            if (check == 0) {
              check = 1;
            } else {
              String numb = StringUtils.convertMobileno(row[0]);
              Contacts contact = new Contacts();
              if (row.length > 1) {
                contact.setName(row[1]);
              } else {
                contact.setName("N/A");
              } 
              contact.setNumber(numb);
              if (row.length > 2) {
                contact.setEmail(row[2]);
              } else {
                contact.setEmail("N/A");
              } 
              contact.setGid(Long.parseLong(gid));
              if (numb != null) {
                contacts.add(contact);
              } else {
                invald++;
              } 
            } 
            csvReader.close();
          } 
          int f = this.adminService.saveContects(contacts);
          jo.addProperty("invalid", Integer.valueOf(invald));
          jo.addProperty("duplicate", Integer.valueOf(f));
          jo.addProperty("success", Integer.valueOf(content.size() - f - invald));
          return jo.toString();
        } 
        if (fileName.endsWith(".xls") || fileName.endsWith(".XLS")) {
          try {
            File fileToCreate = new File(filePath, "file.xls");
            FileUtils.copyFile(convert(file), fileToCreate);
            FileInputStream files = new FileInputStream(fileToCreate);
            Workbook workbook = WorkbookFactory.create(files);
            System.out.println("file name:" + file);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            List<Contacts> contacts = new ArrayList<>();
            while (rowIterator.hasNext()) {
              String[] a = new String[4];
              int i = 0;
              Row row = rowIterator.next();
              Iterator<Cell> cellIterator = row.cellIterator();
              while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                cell.setCellType(1);
                a[i] = cell.getStringCellValue();
                i++;
                if (i > 3)
                  break; 
              } 
              String numb = StringUtils.convertMobileno(a[0]);
              Contacts contact = new Contacts();
              contact.setName(a[1]);
              contact.setNumber(numb);
              contact.setEmail(a[2]);
              contact.setGid(Long.parseLong(gid));
              if (numb != null) {
                contacts.add(contact);
                continue;
              } 
              invald++;
            } 
            int f = this.adminService.saveContects(contacts);
            jo.addProperty("invalid", Integer.valueOf(invald));
            jo.addProperty("duplicate", Integer.valueOf(f));
            jo.addProperty("success", Integer.valueOf(contacts.size() - f));
            return jo.toString();
          } catch (Exception e) {
            System.out.println("error" + e);
            e.printStackTrace();
          } 
        } else if (fileName.endsWith(".xlsx") || fileName.endsWith(".XLSX")) {
          try {
            File fileToCreate = new File(filePath, "file.xlsx");
            FileUtils.copyFile(convert(file), fileToCreate);
            FileInputStream files = new FileInputStream(fileToCreate);
            XSSFWorkbook workBook = new XSSFWorkbook(files);
            XSSFSheet sheet = workBook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            List<Contacts> contacts = new ArrayList<>();
            while (rowIterator.hasNext()) {
              String[] a = new String[4];
              int i = 0;
              Row row = rowIterator.next();
              Iterator<Cell> cellIterator = row.cellIterator();
              while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                cell.setCellType(1);
                try {
                  a[i] = cell.getStringCellValue();
                } catch (Exception ex) {
                  a[i] = "N/A";
                } 
                i++;
                if (i > 3)
                  break; 
              } 
              String numb = StringUtils.convertMobileno(a[0]);
              Contacts contact = new Contacts();
              contact.setName(a[1]);
              contact.setNumber(numb);
              contact.setEmail(a[2]);
              contact.setGid(Long.parseLong(gid));
              if (numb != null) {
                contacts.add(contact);
                continue;
              } 
              invald++;
            } 
            int f = this.adminService.saveContects(contacts);
            jo.addProperty("invalid", Integer.valueOf(invald));
            jo.addProperty("duplicate", Integer.valueOf(f));
            jo.addProperty("success", Integer.valueOf(contacts.size() - f));
            return jo.toString();
          } catch (Exception e) {
            System.out.println("error" + e);
            e.printStackTrace();
          } 
        } 
      } catch (Exception e) {
        e.printStackTrace();
        return gson.toJson("0");
      } 
    } else {
      Contacts contact = new Contacts();
      contact.setName(name);
      contact.setNumber(number);
      contact.setEmail(email);
      contact.setGid(Long.parseLong(gid));
      List<Contacts> contacts = new ArrayList<>();
      contacts.add(contact);
      this.adminService.saveContects(contacts);
      return "redirect:/#/tables/importContect";
    } 
    return "redirect:/#/tables/importContect";
  }
  
  @RequestMapping(value = {"saveContectSingle"}, method = {RequestMethod.POST})
  @ResponseBody
  public String saveContectSingle(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    Long id = Long.valueOf(currentUser.getUserid());
    String gid = request.getParameter("gid");
    String name = request.getParameter("name");
    String number = request.getParameter("number");
    String email = request.getParameter("email");
    String newno = StringUtils.convertMobileno(number);
    if (newno == null)
      return "2"; 
    Contacts contact = new Contacts();
    contact.setName(name);
    contact.setNumber(number);
    contact.setEmail(email);
    contact.setGid(Long.parseLong(gid));
    List<Contacts> contacts = new ArrayList<>();
    contacts.add(contact);
    if (this.adminService.saveContects(contacts) == 0)
      return "1"; 
    return "0";
  }
  
  @RequestMapping(value = {"saveContectCopyPaste"}, method = {RequestMethod.POST})
  @ResponseBody
  public String saveContectCopyPaste(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    Long id = Long.valueOf(currentUser.getUserid());
    String gid = request.getParameter("gid");
    String txt = request.getParameter("txt");
    String[] txts = txt.split("\n");
    JsonObject jo = new JsonObject();
    List<Contacts> contacts = new ArrayList<>();
    int invald = 0;
    for (int i = 0; i < txts.length; i++) {
      String[] vals = txts[i].split(",");
      Contacts contact = new Contacts();
      String numb = (vals[0].length() == 12) ? vals[0].substring(2) : vals[0];
      if (vals.length == 1) {
        contact.setNumber(numb);
        contact.setName("");
        contact.setEmail("");
      } 
      if (vals.length == 2) {
        contact.setNumber(numb);
        contact.setName(vals[1]);
        contact.setEmail("");
      } 
      if (vals.length == 3) {
        contact.setNumber(numb);
        contact.setName(vals[1]);
        contact.setEmail(vals[2]);
      } 
      contact.setGid(Long.parseLong(gid));
      if (numb.length() == 10) {
        contacts.add(contact);
      } else {
        invald++;
      } 
    } 
    int f = this.adminService.saveContects(contacts);
    jo.addProperty("success", Integer.valueOf(txts.length - f - invald));
    jo.addProperty("duplicate", Integer.valueOf(f));
    jo.addProperty("invalid", Integer.valueOf(invald));
    return jo.toString();
  }
  
  public File convert(MultipartFile file) throws IOException {
    File convFile = new File(file.getOriginalFilename());
    convFile.createNewFile();
    FileOutputStream fos = new FileOutputStream(convFile);
    fos.write(file.getBytes());
    fos.close();
    return convFile;
  }
  
  @RequestMapping(value = {"getContects"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getContects(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    int page = 1;
    int listSize = 10;
    int ttlpage = 10;
    int start = 0;
    int len = 0;
    String name = "";
    String number = "";
    String email = "";
    String gid = "";
    System.out.println("gid" + request.getParameter("gid"));
    if (request.getParameter("gid") != null)
      gid = request.getParameter("gid"); 
    if (request.getParameter("start") != null)
      start = Integer.parseInt(request.getParameter("start")); 
    if (request.getParameter("name") != null) {
      name = request.getParameter("name");
      System.out.println("name : " + name);
      if (name.equalsIgnoreCase("undefined") || name.equalsIgnoreCase(null))
        name = ""; 
    } 
    if (request.getParameter("mobile") != null) {
      number = request.getParameter("mobile");
      System.out.println("number : " + number);
      if (number.equalsIgnoreCase("undefined") || number.equalsIgnoreCase(null))
        number = ""; 
    } 
    if (request.getParameter("email") != null) {
      email = request.getParameter("email");
      System.out.println("email : " + email);
      if (email.equalsIgnoreCase("undefined") || email.equalsIgnoreCase(null))
        email = ""; 
    } 
    if (request.getParameter("length") != null) {
      len = Integer.parseInt(request.getParameter("length"));
      listSize = len;
    } 
    page = start / len + 1;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    String json = this.adminService.getContectsByGid(page, listSize, currentUser.getUserid(), name, email, number, gid);
    return json;
  }
  
  @RequestMapping(value = {"deleteContactbyid"}, method = {RequestMethod.GET})
  @ResponseBody
  public String deleteContactbyid(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    String cid = request.getParameter("cid");
    System.out.println("cid : " + cid);
    this.adminService.deleteContactbyid(Long.parseLong(cid));
    return "1";
  }
  
  @RequestMapping(value = {"getSmsAuditByAdmin"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getSmsAuditByAdmin(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    int uid = -1;
    String mob = "";
    Date fdate = null;
    Date todate = null;
    try {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("m") != null)
        mob = request.getParameter("m"); 
      if (!StringUtils.isEmpty(request.getParameter("fdate")))
        fdate = this.df.parse(String.valueOf(request.getParameter("fdate")) + " 00:00:00"); 
      if (!StringUtils.isEmpty(request.getParameter("todate")))
        todate = this.df.parse(String.valueOf(request.getParameter("todate")) + " 23:59:59"); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } 
      if (request.getParameter("uid") != null)
        uid = Integer.parseInt(request.getParameter("uid")); 
      page = start / len + 1;
      this.currentUser = (MediUser)auth.getPrincipal();
    } catch (Exception ex) {
      return "ERROR-505";
    } 
    System.out.println("In Sent message ------ > PAGE::" + page + "::LIST SIZE:" + listSize + "::::" + mob + "--" + 
        this.currentUser.getUserid() + ":::" + uid);
    if (this.currentUser.getUsername().equalsIgnoreCase("admin") && uid == -1) {
      uid = 0;
    } else if (uid == -1) {
      uid = (int)this.currentUser.getUserid();
    } 
    return this.adminService.getSmsAuditByAdmin(page, listSize, uid, fdate, todate, mob);
  }
  
  @RequestMapping(value = {"getDisplaySettings"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getDisplaySettings(HttpServletRequest request, HttpServletResponse response) {
    List list = null;
    list = this.adminService.getDisplaySetting();
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"saveDisplaySetting"}, method = {RequestMethod.POST}, produces = {"application/json"})
  public String saveDisplaySetting(HttpServletRequest request, HttpServletResponse response, @RequestParam("logo") MultipartFile logo, @RequestParam("img") MultipartFile image) throws IllegalStateException, IOException {
    System.out.println("save Display Setting");
    String url = request.getParameter("url");
    String userName = request.getParameter("uid");
    String title = request.getParameter("title");
    String subTitle = request.getParameter("subTitle");
    String homeUrl = request.getParameter("homeUrl");
    String address = request.getParameter("address");
    String aboutus = request.getParameter("aboutus");
    String info1 = request.getParameter("info1");
    String info2 = request.getParameter("info2");
    String filePath = "";
    filePath = request.getServletContext().getRealPath("/");
    filePath = String.valueOf(filePath) + "\\UploadFile\\logo\\";
    System.out.println("filePath" + filePath);
    DisplaySetting displaySetting = new DisplaySetting();
    displaySetting.setUserName(userName);
    displaySetting.setUrl(url);
    displaySetting.setTitle(title);
    displaySetting.setSubTitle(subTitle);
    displaySetting.setHomeurl(homeUrl);
    displaySetting.setAddress(address);
    displaySetting.setAboutus(aboutus);
    displaySetting.setExtraInfo1(info1);
    displaySetting.setExtraInfo2(info2);
    if (!logo.isEmpty()) {
      displaySetting.setLogoImage(String.valueOf(userName) + "-" + logo.getOriginalFilename());
      File f = new File(String.valueOf(filePath) + userName + "-" + logo.getOriginalFilename());
      logo.transferTo(f);
      System.out.println("file not empty" + userName + "-" + logo.getOriginalFilename());
    } 
    if (!image.isEmpty()) {
      displaySetting.setImage2(String.valueOf(userName) + "-" + image.getOriginalFilename());
      System.out.println("file not empty" + userName + "-" + image.getOriginalFilename());
      File f2 = new File(String.valueOf(filePath) + userName + "-" + image.getOriginalFilename());
      image.transferTo(f2);
    } 
    this.adminService.saveDisplaySetting(displaySetting);
    return "redirect:/#/tables/DisplaySetting";
  }
  
  @RequestMapping(value = {"updateDisplaySetting"}, method = {RequestMethod.POST}, produces = {"application/json"})
  public String updateDisplaySetting(HttpServletRequest request, HttpServletResponse response, @RequestParam("logo") MultipartFile logo, @RequestParam("img") MultipartFile image) throws IllegalStateException, IOException {
    System.out.println("Update Display Setting");
    String url = request.getParameter("url");
    String id = request.getParameter("id");
    DisplaySetting displaySetting = new DisplaySetting();
    String title = request.getParameter("title");
    String subTitle = request.getParameter("subTitle");
    String homeUrl = request.getParameter("homeUrl");
    String address = request.getParameter("address");
    String aboutus = request.getParameter("aboutus");
    String info1 = request.getParameter("info1");
    String info2 = request.getParameter("info2");
    String filePath = "";
    filePath = request.getServletContext().getRealPath("/");
    filePath = String.valueOf(filePath) + "\\UploadFile\\logo\\";
    System.out.println("filePath" + filePath);
    System.out.println(id);
    long l = Long.parseLong(id);
    displaySetting = this.adminService.getDisplaySettingById(l);
    displaySetting.setUrl(url);
    displaySetting.setTitle(title);
    displaySetting.setSubTitle(subTitle);
    displaySetting.setHomeurl(homeUrl);
    displaySetting.setAddress(address);
    displaySetting.setAboutus(aboutus);
    displaySetting.setExtraInfo1(info1);
    displaySetting.setExtraInfo2(info2);
    if (!logo.isEmpty()) {
      displaySetting.setLogoImage(String.valueOf(displaySetting.getUserName()) + "-" + logo.getOriginalFilename());
      File f = new File(String.valueOf(filePath) + displaySetting.getUserName() + "-" + logo.getOriginalFilename());
      logo.transferTo(f);
      System.out.println("file not empty" + displaySetting.getUserName() + "-" + logo.getOriginalFilename());
    } 
    if (!image.isEmpty()) {
      displaySetting.setImage2(String.valueOf(displaySetting.getUserName()) + "-" + image.getOriginalFilename());
      System.out.println("file not empty" + displaySetting.getUserName() + "-" + image.getOriginalFilename());
      File f2 = new File(String.valueOf(filePath) + displaySetting.getUserName() + "-" + image.getOriginalFilename());
      image.transferTo(f2);
    } 
    this.adminService.updateDisplaySetting(displaySetting);
    return "redirect:/#/tables/DisplaySetting";
  }
  
  @RequestMapping(value = {"deleteDisplaySetting"}, method = {RequestMethod.GET}, produces = {"application/json"})
  public String deleteDisplaySetting(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {
    System.out.println("Delete Display Setting");
    String id = request.getParameter("id");
    long l = Long.parseLong(id);
    this.adminService.deleteDisplaySettingById(l);
    return "redirect:/#/tables/DisplaySetting";
  }
  
  @RequestMapping(value = {"getCRMHostSettings"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getCRMHostSettings(HttpServletRequest request, HttpServletResponse response) {
    System.out.println(" getCRMHostSettings ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    int id = (int)currentUser.getUserid();
    List list = null;
    list = this.adminService.getCRMHostSettings(id);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"getHostSettings"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getHostSettings(HttpServletRequest request, HttpServletResponse response) {
    System.out.println(" getHostSettings ");
    List list = null;
    list = this.adminService.getHostSettings();
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"saveHostSetting"}, method = {RequestMethod.POST})
  @ResponseBody
  public String saveHostSetting(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {
    String message;
    System.out.println("saveHostSetting");
    String user = request.getParameter("user");
    String hostName = request.getParameter("hostName");
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String displayName = request.getParameter("displayName");
    String from = request.getParameter("from");
    HostSetting hostSetting = new HostSetting();
    hostSetting.setUser(user);
    hostSetting.setDisplayName(displayName);
    hostSetting.setFrom(from);
    hostSetting.setHostName(hostName);
    hostSetting.setPassword(password);
    hostSetting.setUsername(username);
    long l = this.adminService.saveHostSetting(hostSetting);
    if (l > 0L) {
      message = "Host Setting Save Successfully...!!!";
    } else {
      message = "ERROR : Host Setting Not Save ...!!!";
    } 
    return message;
  }
  
  @RequestMapping(value = {"updateHostSetting"}, method = {RequestMethod.POST})
  @ResponseBody
  public String updateHostSetting(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {
    String message;
    System.out.println("saveHostSetting");
    String user = request.getParameter("user");
    String hostName = request.getParameter("hostName");
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String displayName = request.getParameter("displayName");
    String from = request.getParameter("from");
    HostSetting hostSetting = new HostSetting();
    hostSetting = this.adminService.getHostSetting(user);
    hostSetting.setDisplayName(displayName);
    hostSetting.setFrom(from);
    hostSetting.setHostName(hostName);
    hostSetting.setPassword(password);
    hostSetting.setUsername(username);
    long l = this.adminService.updateHostSetting(hostSetting);
    if (l > 0L) {
      message = "Host Setting Update Successfully...!!!";
    } else {
      message = "ERROR : Host Setting Not Update ...!!!";
    } 
    return message;
  }
  
  @RequestMapping(value = {"deleteHostSetting"}, method = {RequestMethod.GET}, produces = {"application/json"})
  public String deleteHostSetting(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {
    System.out.println("deleteHostSetting");
    String id = request.getParameter("id");
    long l = Long.parseLong(id);
    this.adminService.deleteHostSettingById(l);
    return "redirect:/#/tables/DisplaySetting";
  }
  
  @RequestMapping(value = {"resetPasswordByAdmin"}, method = {RequestMethod.GET})
  @ResponseBody
  public String resetPasswordByAdmin(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    String username = request.getParameter("username");
    String pass = request.getParameter("password");
    String password = (new SecurityConfig()).passwordEncoder().encode(pass);
    System.out.println(String.valueOf(username) + password);
    this.adminService.resetPassword(username, password);
    return "Password Reset Succefully...!!!";
  }
  
  @RequestMapping(value = {"changePassword"}, method = {RequestMethod.GET})
  @ResponseBody
  public String changePassword(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    String pass = request.getParameter("password");
    String oldpass = request.getParameter("oldPassword");
    String password = (new SecurityConfig()).passwordEncoder().encode(pass);
    this.adminService.resetPassword(currentUser.getUsername(), password);
    return "Password Reset Succefully...!!!";
  }
  
  public boolean isvalidtemplate(String sms, Long userid) throws Exception {
    List<Tamplate> spamText = null;
    spamText = this.userService.getTemplateByuser(userid.longValue());
    if (spamText != null && spamText.size() > 0)
      for (Tamplate spmText : spamText) {
        Pattern pattern = Pattern.compile(spmText.getTname(), 2);
        Matcher matcher = pattern.matcher(sms);
        if (matcher.find())
          return true; 
      }  
    return false;
  }
  
  public void smsAndEmailSend(User user, String pass, String Type, String action) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    MessageTemplate messageTemplate = new MessageTemplate();
    messageTemplate = this.adminService.getMessageTemplate(currentUser.getUserid(), Type);
    String sms = messageTemplate.getMessage();
    if (sms.contains("<PersonName>"))
      sms = sms.replaceAll("\\<PersonName\\>", user.getName()); 
    if (sms.contains("<UserName>"))
      sms = sms.replaceAll("\\<UserName\\>", user.getUsername()); 
    if (sms.contains("<Password>"))
      sms = sms.replaceAll("\\<Password\\>", pass); 
    if (sms.contains("<SenderName>"))
      sms = sms.replaceAll("\\<SenderName\\>", pass); 
    if (sms.contains("<Template>"))
      sms = sms.replaceAll("\\<Template\\>", pass); 
    if (sms.contains("<Address>"))
      sms = sms.replaceAll("\\<Address\\>", user.getAddress()); 
    if (sms.contains("<MobileNo>"))
      sms = sms.replaceAll("\\<MobileNo\\>", user.getMobileNumber()); 
    if (sms.contains("<RegDate>"))
      sms = sms.replaceAll("\\<RegDate\\>", (new Date()).toString()); 
    if (sms.contains("<SmsCredit>"))
      sms = sms.replaceAll("\\<SmsCredit\\>", pass); 
    if (sms.contains("<mailAddress>"))
      sms = sms.replaceAll("\\<mailAddress\\>", user.getEmail()); 
    if (sms.contains("<Action>"))
      sms = sms.replaceAll("\\<Action\\>", action); 
    EmailTemplate emailTemplate = new EmailTemplate();
    emailTemplate = this.adminService.getEmailTemplate(currentUser.getUserid(), Type);
    HostSetting hostSetting = new HostSetting();
    hostSetting = this.adminService.getHostSetting("admin");
    String esms = emailTemplate.getMessage();
    String esubject = emailTemplate.getSubject();
    if (esms.contains("<PersonName>"))
      esms = esms.replaceAll("\\<PersonName\\>", user.getName()); 
    if (esms.contains("<UserName>"))
      esms = esms.replaceAll("\\<UserName\\>", user.getUsername()); 
    if (esms.contains("<Password>"))
      esms = esms.replaceAll("\\<Password\\>", pass); 
    if (esms.contains("<SenderName>")) {
      esms = esms.replaceAll("\\<SenderName\\>", pass);
      esubject = esubject.replaceAll("\\<SenderName\\>", pass);
    } 
    if (esms.contains("<Template>"))
      esms = esms.replaceAll("\\<Template\\>", pass); 
    if (esms.contains("<Address>"))
      esms = esms.replaceAll("\\<Address\\>", user.getAddress()); 
    if (esms.contains("<MobileNo>"))
      esms = esms.replaceAll("\\<MobileNo\\>", user.getMobileNumber()); 
    if (esms.contains("<RegDate>"))
      esms = esms.replaceAll("\\<RegDate\\>", (new Date()).toString()); 
    if (esms.contains("<SmsCredit>"))
      esms = esms.replaceAll("\\<SmsCredit\\>", pass); 
    if (esms.contains("<mailAddress>"))
      esms = esms.replaceAll("\\<mailAddress\\>", user.getEmail()); 
    System.out.println("action : : : :" + action + "       esms" + esms);
    if (esms.contains("<Action>")) {
      esms = esms.replaceAll("\\<Action\\>", action);
      System.out.println("sms==============>>>" + esms);
    } 
    emailTemplate.setMessage(esms);
    emailTemplate.setSubject(esubject);
    if (StringUtils.isEmailValid(user.getEmail()))
      (new Thread((Runnable)new EmailSenderThread(emailTemplate, hostSetting, user.getEmail()))).start(); 
  }
  
  @RequestMapping(value = {"getMessageTamplate"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getMessageTamplate(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Id  : " + uid);
    List<MessageTemplate> tamplate = new ArrayList<>();
    tamplate = this.adminService.getMessageTemplatebyUid(uid);
    System.out.println(tamplate.size());
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(tamplate.size());
    datatableJsonObject.setRecordsTotal(tamplate.size());
    datatableJsonObject.setData(tamplate);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"saveMessageTemplate"}, method = {RequestMethod.POST})
  @ResponseBody
  public String saveMessageTemplate(HttpServletRequest request, HttpServletResponse response) {
    String tname = request.getParameter("name");
    String template = request.getParameter("template");
    System.out.println("name : " + tname + "  template : " + template);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    MessageTemplate messageTemplate = new MessageTemplate();
    messageTemplate.setIshidden(false);
    messageTemplate.setMessage(template);
    messageTemplate.setTemplateName(tname);
    messageTemplate.setUid(uid);
    Long id = this.adminService.saveMessageTemplate(messageTemplate);
    return "MessageTemplate : " + tname + " Save Successfully.";
  }
  
  @RequestMapping(value = {"getEmailTamplate"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String getEmailTamplate(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("Id  : " + uid);
    List<EmailTemplate> tamplate = new ArrayList<>();
    tamplate = this.adminService.getEmailTemplatebyUid(uid);
    System.out.println(tamplate.size());
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(tamplate.size());
    datatableJsonObject.setRecordsTotal(tamplate.size());
    datatableJsonObject.setData(tamplate);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"saveEmailTemplate"}, method = {RequestMethod.POST})
  @ResponseBody
  public String saveEmailTemplate(HttpServletRequest request, HttpServletResponse response) {
    String tname = request.getParameter("name");
    String sub = request.getParameter("subject");
    String template = request.getParameter("template");
    System.out.println("name : " + tname + "  template : " + template);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    EmailTemplate emailTemplate = new EmailTemplate();
    emailTemplate.setUid(uid);
    emailTemplate.setIshidden(true);
    emailTemplate.setMessage(template);
    emailTemplate.setSubject(sub);
    emailTemplate.setTemplateName(tname);
    Long id = this.adminService.saveEmailTemplate(emailTemplate);
    return "EmailTemplate : " + tname + " Save Successfully.";
  }
  
  @RequestMapping(value = {"deleteMessageTemplate"}, method = {RequestMethod.GET})
  public String deleteMessageTemplate(HttpServletRequest request, HttpServletResponse response) {
    Long id = Long.valueOf(Long.parseLong(request.getParameter("id")));
    System.out.println("delete id" + id);
    this.adminService.deleteMessageTemplate(id);
    return "redirect:/#/tables/messageTemplate";
  }
  
  @RequestMapping(value = {"updateMessageTemplate"}, method = {RequestMethod.POST})
  @ResponseBody
  public String updateMessageTemplate(HttpServletRequest request, HttpServletResponse response) {
    String tname = request.getParameter("name");
    String template = request.getParameter("template");
    System.out.println("name : " + tname + "  template : " + template);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("---------------------->>>" + request.getParameter("id"));
    long id = Long.parseLong(request.getParameter("id"));
    MessageTemplate messageTemplate = new MessageTemplate();
    messageTemplate = this.adminService.getMessageTemplateById(id);
    messageTemplate.setMessage(template);
    messageTemplate.setTemplateName(tname);
    int i = this.adminService.updateMessageTemplate(messageTemplate);
    return "Update Successfully...";
  }
  
  @RequestMapping(value = {"updateEmailTemplate"}, method = {RequestMethod.POST})
  @ResponseBody
  public String updateEmailTemplate(HttpServletRequest request, HttpServletResponse response) {
    String tname = request.getParameter("name");
    String template = request.getParameter("template");
    System.out.println("name : " + tname + "  template : " + template);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long uid = currentUser.getUserid();
    System.out.println("---------------------->>>" + request.getParameter("id"));
    long id = Long.parseLong(request.getParameter("id"));
    EmailTemplate emailTemplate = new EmailTemplate();
    emailTemplate = this.adminService.getMessageEmailById(id);
    emailTemplate.setMessage(template);
    emailTemplate.setTemplateName(tname);
    int i = this.adminService.updateEmailTemplate(emailTemplate);
    return "Update Successfully...";
  }
  
  @RequestMapping(value = {"getEmailLogByCompany"}, produces = {"application/javascript"})
  @ResponseBody
  public String getEmailLogByCompany(HttpServletRequest request, HttpServletResponse response) throws Exception {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    int uid = (int)currentUser.getUserid();
    String name = request.getParameter("name");
    String mobileNo = request.getParameter("mobileNo");
    String email = request.getParameter("email");
    String subject = request.getParameter("subject");
    System.out.println("*******getEmailLogByCompany********");
    int i = 0;
    String listData = "";
    String query = "";
    String queryCount = "";
    String stdate = request.getParameter("stdate");
    String endate = request.getParameter("endate");
    System.out.println("stdate " + stdate);
    System.out.println("endate " + endate);
    System.out.println("***************");
    if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && name.equalsIgnoreCase("NA") && 
      mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA") && subject.equalsIgnoreCase("NA")) {
      queryCount = "SELECT  COUNT(*) FROM lead   JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE lead.companyId=" + 
        uid + 
        " AND emaillog.company_id=" + uid;
      query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status,emaillog.attached_file,emaillog.mail_body FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE lead.companyId=" + 
        uid + 
        " AND emaillog.company_id=" + uid + " ORDER BY emaillog.id DESC";
    } else if (!stdate.equalsIgnoreCase("NA") && !endate.equalsIgnoreCase("NA") && !name.equalsIgnoreCase("NA") && 
      !mobileNo.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("NA") && 
      !subject.equalsIgnoreCase("NA")) {
      queryCount = "SELECT  COUNT(*) FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.company_id=" + 
        uid + 
        " " + "  AND (lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + 
        "%' OR lead.mobileNo LIKE '%" + mobileNo + "%' OR lead.email LIKE '%" + email + 
        "%' OR emaillog.subject LIKE '%" + subject + "%')" + "  AND (emaillog.send_date BETWEEN '" + 
        stdate + "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY))";
      query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status, emaillog.attached_file,emaillog.mail_body FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.company_id=" + 
        
        uid + " " + "  AND (lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + 
        "%' OR lead.mobileNo LIKE '%" + mobileNo + "%' OR lead.email LIKE '%" + email + 
        "%' OR emaillog.subject LIKE '%" + subject + "%')" + "  AND (emaillog.send_date BETWEEN '" + 
        stdate + "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) ORDER BY emaillog.id DESC";
    } else if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && !name.equalsIgnoreCase("NA") && 
      !mobileNo.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("NA") && 
      !subject.equalsIgnoreCase("NA")) {
      queryCount = "SELECT  COUNT(*) FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.company_id=" + 
        uid + 
        " " + "  AND (lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + 
        "%' OR lead.mobileNo LIKE '%" + mobileNo + "%' OR lead.email LIKE '%" + email + 
        "%' OR emaillog.subject LIKE '%" + subject + "%')";
      query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status, emaillog.attached_file,emaillog.mail_body FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.company_id=" + 
        
        uid + " " + "  AND (lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + 
        "%' OR lead.mobileNo LIKE '%" + mobileNo + "%' OR lead.email LIKE '%" + email + 
        "%' OR emaillog.subject LIKE '%" + subject + "%') ORDER BY emaillog.id DESC" + "  ";
    } else {
      queryCount = "SELECT  COUNT(*)  FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE emaillog.company_id=" + 
        uid + 
        " " + "  AND (emaillog.send_date BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY))";
      query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status, emaillog.attached_file,emaillog.mail_body FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE emaillog.company_id=" + 
        
        uid + " " + "  AND (emaillog.send_date BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY)) ORDER BY emaillog.id DESC";
    } 
    System.out.println(queryCount);
    System.out.println(query);
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    String mob = "";
    Date fdate = null;
    Date todate = null;
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } else {
        System.out.println("LENGTH : " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("query -| " + query);
    listData = this.adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    System.out.println("DATA:: " + listData);
    return listData;
  }
  
  @RequestMapping(value = {"getAndroidCallLogByCompany"}, produces = {"application/javascript"})
  @ResponseBody
  public String getAndroidCallLogByCompany(HttpServletRequest request, HttpServletResponse response, @RequestParam("mobileNo") String mobileNo, @RequestParam("name") String name, @RequestParam("telecaller") String telecaller, @RequestParam("uname") String uname, @RequestParam("callDue") String callDue, @RequestParam("callSta") String callSta) throws Exception {
    System.out.println("*******getAndroidCallLogByCompany********");
    JSONArray jarray = new JSONArray();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    int uid = (int)currentUser.getUserid();
    Object[] result = null;
    int i = 0;
    String listData = "";
    String query = "";
    String queryCount = "";
    String stdate = request.getParameter("stdate");
    String endate = request.getParameter("endate");
    System.out.println("stdate " + stdate);
    System.out.println("endate " + endate);
    System.out.println("mobileNo " + mobileNo);
    System.out.println("name " + name);
    System.out.println("telecaller " + telecaller);
    System.out.println("uname " + uname);
    System.out.println("callDue " + callDue);
    System.out.println("callSta " + callSta);
    System.out.println("***************");
    if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && name.equalsIgnoreCase("NA") && 
      mobileNo.equalsIgnoreCase("NA") && telecaller.equalsIgnoreCase("NA") && uname.equalsIgnoreCase("NA") && 
      callDue.equalsIgnoreCase("NA") && callSta.equalsIgnoreCase("NA")) {
      queryCount = "SELECT  COUNT(*)  FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.companyId=" + 
        
        uid + " ORDER BY mobilecalllog.id DESC";
      query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration,mobilecalllog.callStatus,mobilecalllog.entryDate,1 FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.companyId=" + 
        
        uid + " ORDER BY mobilecalllog.id DESC";
    } else if (!name.equalsIgnoreCase("NA") && !mobileNo.equalsIgnoreCase("NA") && 
      !telecaller.equalsIgnoreCase("NA") && !uname.equalsIgnoreCase("NA") && 
      !callDue.equalsIgnoreCase("NA") && !callSta.equalsIgnoreCase("NA") && stdate.equalsIgnoreCase("NA") && 
      endate.equalsIgnoreCase("NA")) {
      queryCount = "SELECT  COUNT(*)  FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.companyId=" + 
        
        uid + " AND" + " (mobilecalllog.nane LIKE '%" + name + "%' OR mobilecalllog.callDuration LIKE '%" + 
        callDue + "%' OR mobilecalllog.callStatus LIKE '%" + callSta + "%' OR mobilecalllog.mobileNo='%" + 
        mobileNo + "%')" + "  ORDER BY mobilecalllog.id DESC";
      query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration,mobilecalllog.callStatus,mobilecalllog.entryDate,1 FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.companyId=" + 
        
        uid + " AND" + " (mobilecalllog.nane LIKE '%" + name + "%' OR mobilecalllog.callDuration LIKE '%" + 
        callDue + "%' OR mobilecalllog.callStatus LIKE '%" + callSta + "%' OR mobilecalllog.mobileNo='%" + 
        mobileNo + "%')" + "  ORDER BY mobilecalllog.id DESC";
    } else if (!stdate.equalsIgnoreCase("NA") && !endate.equalsIgnoreCase("NA") && !name.equalsIgnoreCase("NA") && 
      !mobileNo.equalsIgnoreCase("NA") && !telecaller.equalsIgnoreCase("NA") && 
      !uname.equalsIgnoreCase("NA") && !callDue.equalsIgnoreCase("NA") && 
      !callSta.equalsIgnoreCase("NA")) {
      queryCount = "SELECT  COUNT(*)  FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.companyId=" + 
        
        uid + " AND" + "  (mobilecalllog.entryDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY)) " + "  ORDER BY mobilecalllog.id DESC";
      query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration, mobilecalllog.callStatus,mobilecalllog.entryDate,1 FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId  WHERE mobilecalllog.companyId=" + 
        
        uid + " AND (mobilecalllog.nane LIKE '%" + name + 
        "%' OR mobilecalllog.callDuration LIKE '%" + callDue + "%' OR mobilecalllog.callStatus LIKE '%" + 
        callSta + "%'" + " OR mobilecalllog.mobileNo='%" + mobileNo + 
        "%') AND (mobilecalllog.entryDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY)) ORDER BY mobilecalllog.id DESC";
    } else {
      queryCount = "SELECT  COUNT(*)  FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.companyId=" + 
        
        uid + " AND" + "  (mobilecalllog.entryDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY)) " + "  ORDER BY mobilecalllog.id DESC";
      query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration,mobilecalllog.callStatus,mobilecalllog.entryDate,1 FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.companyId=" + 
        
        uid + " AND" + " (mobilecalllog.entryDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY))" + "  ORDER BY mobilecalllog.id DESC";
    } 
    System.out.println(queryCount);
    System.out.println(query);
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    String mob = "";
    Date fdate = null;
    Date todate = null;
    try {
      if (request.getParameter("start") != null)
        start = Integer.parseInt(request.getParameter("start")); 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        listSize = len;
      } else {
        System.out.println("LENGTH : " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    listData = this.adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    return listData;
  }
  
  @RequestMapping(value = {"geLeadByMobileNumber"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String geLeadByMobileNumber(HttpServletRequest request, HttpServletResponse response, @RequestParam("mobileNo") String mobileNo) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    List list = this.adminService.geLeadByMobileNumber((int)currentUser.getUserid(), mobileNo);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(list.size());
    datatableJsonObject.setRecordsTotal(list.size());
    datatableJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String jsons = gson.toJson(datatableJsonObject);
    return jsons;
  }
  
  @RequestMapping(value = {"/GetMonthlySuccessFailLogCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetMonthlySuccessFailLogCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetMonthlySuccessFailLogCount is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List<Object[]> list = this.adminService.GetMonthlySuccessFailLogCount(userId);
    String[] months = new String[12];
    months[0] = "January";
    months[1] = "February";
    months[2] = "March";
    months[3] = "April";
    months[4] = "May";
    months[5] = "June";
    months[6] = "July";
    months[7] = "August";
    months[8] = "September";
    months[9] = "October";
    months[10] = "November";
    months[11] = "December";
    String[] Addmonths = new String[12];
    List<MonthlyLogObject> l = new ArrayList<>();
    if (list.size() != 0)
      for (int j = 0; j < list.size(); j++) {
        JSONObject leadmap = new JSONObject();
        Object[] result = list.get(j);
        System.out.println(result[0] + " " + result[1] + " " + result[2] + " " + result[3]);
        Addmonths[j] = result[0].toString();
        MonthlyLogObject log = new MonthlyLogObject();
        log.setMonth(result[0].toString());
        log.setSuccessCall(Integer.parseInt(result[1].toString()));
        log.setFailCall(Integer.parseInt(result[2].toString()));
        log.setMonthCount(Integer.parseInt(result[3].toString()));
        l.add(log);
      }  
    List<String> mon = Arrays.asList(Addmonths);
    for (int i = 0; i < months.length; i++) {
      if (!mon.contains(months[i])) {
        MonthlyLogObject log = new MonthlyLogObject();
        log.setMonth(months[i]);
        log.setSuccessCall(0);
        log.setFailCall(0);
        log.setMonthCount(i);
        l.add(i, log);
      } 
    } 
    Collections.sort(l, (o1, o2) -> o1.getMonthCount() - o2.getMonthCount());
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(l);
  }
  
  @RequestMapping(value = {"/GetDailySuccessFailLogCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetDailySuccessFailLogCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetDailySuccessFailLogCount is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List<Object[]> list = this.adminService.GetDailySuccessFailLogCount(userId);
    Date d = new Date();
    String[] months = new String[31];
    months[0] = "1st";
    months[1] = "2nd";
    months[2] = "3rd";
    months[3] = "4th";
    months[4] = "5th";
    months[5] = "6th";
    months[6] = "7th";
    months[7] = "8th";
    months[8] = "9th";
    months[9] = "10th";
    months[10] = "11th";
    months[11] = "12th";
    months[12] = "13th";
    months[13] = "14th";
    months[14] = "15th";
    months[15] = "16th";
    months[16] = "17th";
    months[17] = "18th";
    months[18] = "19th";
    months[19] = "20th";
    months[20] = "21st";
    months[21] = "22nd";
    months[22] = "23rd";
    months[23] = "24th";
    months[24] = "25th";
    months[25] = "26th";
    months[26] = "27th";
    months[27] = "28th";
    months[28] = "29th";
    months[29] = "30th";
    months[30] = "31st";
    String[] Addmonths = new String[31];
    List<MonthlyLogObject> l = new ArrayList<>(31);
    if (list != null && 
      list.size() != 0)
      for (int j = 0; j < list.size(); j++) {
        JSONObject leadmap = new JSONObject();
        Object[] result = list.get(j);
        Addmonths[j] = months[Integer.parseInt(result[0].toString()) - 1];
        MonthlyLogObject log = new MonthlyLogObject();
        log.setMonth(months[Integer.parseInt(result[0].toString()) - 1]);
        log.setSuccessCall(Integer.parseInt(result[1].toString()));
        log.setFailCall(Integer.parseInt(result[2].toString()));
        log.setMonthCount(Integer.parseInt(result[3].toString()));
        l.add(log);
      }  
    List<String> mon = Arrays.asList(Addmonths);
    int i;
    for (i = 0; i < Addmonths.length; i++);
    for (i = 0; i < months.length; i++) {
      System.out.println(String.valueOf(Addmonths[i]) + "  ::  " + months[i]);
      if (!mon.contains(months[i])) {
        MonthlyLogObject log = new MonthlyLogObject();
        log.setMonth(months[i]);
        log.setSuccessCall(0);
        log.setFailCall(0);
        log.setMonthCount(i);
        l.add(i, log);
      } 
    } 
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(l);
  }
  
  @RequestMapping(value = {"/GetCountByLeadState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetCountByLeadState(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetCountByLeadState is Called..");
    JSONArray jarray = new JSONArray();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List<Object[]> list = this.adminService.GetCountByLeadState(userId);
    System.out.println(list.size());
    if (list != null && 
      list.size() != 0)
      for (int i = 0; i < list.size(); i++) {
        PropertyPOJO pojo = new PropertyPOJO();
        JSONObject leadmap = new JSONObject();
        Object[] result = list.get(i);
        pojo.setProperty(result[0].toString());
        pojo.setCount(Integer.parseInt(result[1].toString()));
        jarray.add(pojo);
      }  
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(jarray);
  }
  
  @RequestMapping(value = {"/GetCountBySuccessState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetCountBySuccessState(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetCountBySuccessState is Called..");
    JSONArray jarray = new JSONArray();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List<Object[]> list = this.adminService.GetCountBySuccessState(userId);
    System.out.println(list.size());
    if (list != null && 
      list.size() != 0)
      for (int i = 0; i < list.size(); i++) {
        PropertyPOJO pojo = new PropertyPOJO();
        JSONObject leadmap = new JSONObject();
        Object[] result = list.get(i);
        pojo.setProperty(result[0].toString());
        pojo.setCount(Integer.parseInt(result[1].toString()));
        jarray.add(pojo);
      }  
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(jarray);
  }
  
  @RequestMapping(value = {"/GetCountByFailState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetCountByFailState(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetCountByFailState is Called..");
    JSONArray jarray = new JSONArray();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List<Object[]> list = this.adminService.GetCountByFailState(userId);
    System.out.println(list.size());
    if (list != null && 
      list.size() != 0)
      for (int i = 0; i < list.size(); i++) {
        PropertyPOJO pojo = new PropertyPOJO();
        JSONObject leadmap = new JSONObject();
        Object[] result = list.get(i);
        pojo.setProperty(result[0].toString());
        pojo.setCount(Integer.parseInt(result[1].toString()));
        jarray.add(pojo);
      }  
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(jarray);
  }
  
  @RequestMapping(value = {"/GetTelecallerCallCount"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTelecallerCallCount(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetTelecallerCallCount is Called..");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int userId = (int)id;
    List<Object[]> list = this.adminService.GetTelecallerCallCount(userId);
    Date d = new Date();
    String[] Addmonths = new String[31];
    List<MonthlyLogObject> l = new ArrayList<>(31);
    List<Object[]> allTelList = this.adminService.getTallyCallerByCompanyId(userId);
    String[] months = new String[allTelList.size()];
    if (allTelList.size() != 0) {
      int i;
      for (i = 0; i < allTelList.size(); i++) {
        JSONObject leadmap = new JSONObject();
        Object[] result = allTelList.get(i);
        months[i] = result[1].toString();
      } 
      if (list != null && 
        list.size() != 0)
        for (i = 0; i < list.size(); i++) {
          JSONObject leadmap = new JSONObject();
          Object[] result = list.get(i);
          Addmonths[i] = result[0].toString();
          System.out.println(result[0] + " :: " + result[1] + " :: " + result[2]);
          MonthlyLogObject log = new MonthlyLogObject();
          log.setMonth(result[0].toString());
          log.setSuccessCall(Integer.parseInt(result[1].toString()));
          log.setFailCall(Integer.parseInt(result[2].toString()));
          log.setMonthCount(
              Integer.parseInt(result[1].toString()) + Integer.parseInt(result[2].toString()));
          l.add(log);
        }  
      List<String> mon = Arrays.asList(Addmonths);
      for (int j = 0; j < months.length; j++) {
        System.out.println(String.valueOf(Addmonths[j]) + "  ::  " + months[j]);
        if (!mon.contains(months[j])) {
          MonthlyLogObject log = new MonthlyLogObject();
          log.setMonth(months[j]);
          log.setSuccessCall(0);
          log.setFailCall(0);
          log.setMonthCount(0);
          l.add(j, log);
        } 
      } 
    } 
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(l);
  }
  
  @RequestMapping(value = {"ADuser"}, method = {RequestMethod.GET}, produces = {"application/json"})
  @ResponseBody
  public String ADuser(HttpServletRequest request, HttpServletResponse response, @RequestParam("ststus") String ststus, @RequestParam("tcid") int tcid) {
    if (ststus.equalsIgnoreCase("true")) {
      this.cservice.createupdateSqlQuery(
          "UPDATE tallycaller SET tallycaller.active='0' WHERE tallycaller.tcallerid=" + tcid);
    } else {
      this.cservice.createupdateSqlQuery(
          "UPDATE tallycaller SET tallycaller.active='1' WHERE tallycaller.tcallerid=" + tcid);
    } 
    return "Updated...";
  }
  
  @RequestMapping(value = {"/GetTelecallerSummaryByLeadState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTelecallerSummaryByLeadState(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetTelecallerSummaryByLeadState is Called ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int managerId = (int)id;
    JSONObject mainJson = new JSONObject();
    JSONArray DataArray = new JSONArray();
    JSONArray ColumnsArray = new JSONArray();
    JSONArray TeleName_ColumnsArray = new JSONArray();
    TeleName_ColumnsArray.add("Telecaller Name");
    ColumnsArray.add(TeleName_ColumnsArray);
    String telecaller = "";
    String telecallerId = "";
    String leadState = "";
    List<Object[]> leadByManager = new ArrayList();
    leadByManager = this.adminService.GetLeadStateByManager(managerId);
    List<Object[]> telecallerByManager = new ArrayList();
    telecallerByManager = this.adminService.getTallyCallerByManager(managerId);
    for (Object[] stateListresult : leadByManager) {
      JSONArray Auto_ColumnsArray = new JSONArray();
      leadState = stateListresult[1].toString().trim();
      Auto_ColumnsArray.add(leadState);
      ColumnsArray.add(Auto_ColumnsArray);
    } 
    for (Object[] telecallerListresult : telecallerByManager) {
      JSONArray jSONArray = new JSONArray();
      telecallerId = telecallerListresult[0].toString().trim();
      telecaller = telecallerListresult[1].toString().trim();
      jSONArray.add(telecaller);
      DataArray.add(jSONArray);
      for (Object[] stateListresult : leadByManager) {
        JSONArray Auto_ColumnsArray = new JSONArray();
        leadState = stateListresult[1].toString().trim();
        String telecallerLeadCount = this.adminService.getTallyCallerLeadCount(Integer.parseInt(telecallerId), 
            leadState);
        if (telecallerLeadCount.equalsIgnoreCase("0")) {
          jSONArray.add("<a href='#' onclick=noDataAlert()>" + telecallerLeadCount + "</a>");
          continue;
        } 
        jSONArray.add("<a href='#'  onclick=devicehistory('" + telecallerId + "',\"" + leadState + 
            "\")>" + telecallerLeadCount + "</a>");
      } 
    } 
    JSONArray Inner_DataArray = new JSONArray();
    Inner_DataArray.add("Total");
    String leadStateNew = "";
    for (Object[] stateListresult : leadByManager) {
      leadStateNew = stateListresult[1].toString().trim();
      String leadCount = this.adminService.getLeadStateCount(leadStateNew, managerId);
      Inner_DataArray.add(leadCount);
    } 
    DataArray.add(Inner_DataArray);
    mainJson.put("data", DataArray);
    mainJson.put("columns", ColumnsArray);
    return mainJson.toString();
  }
  
  @RequestMapping(value = {"/GetLeadByLeadState"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadByLeadState(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadState") String leadState, @RequestParam("telecallerId") int teleId) throws ParseException {
    List getLead = this.adminService.getLeadByLeadState(teleId, leadState);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(getLead.size());
    datatableJsonObject.setRecordsTotal(getLead.size());
    datatableJsonObject.setData(getLead);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/GetLeadChart"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadChart(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int managerId = (int)id;
    JSONArray mainArray = new JSONArray();
    JSONObject DataObj = new JSONObject();
    String leadState = "";
    List<Object[]> leadByManager = new ArrayList();
    leadByManager = this.adminService.GetLeadStateByManager(managerId);
    for (Object[] stateListresult : leadByManager) {
      leadState = stateListresult[1].toString().trim();
      String leadCount = this.adminService.getLeadStateCount(leadState, managerId);
      DataObj.put(leadState, leadCount);
    } 
    mainArray.add(DataObj);
    return mainArray.toString();
  }
  
  @RequestMapping(value = {"/getFollowupsByLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getFollowupsByLead(HttpServletRequest request, HttpServletResponse response, @RequestParam("leadId") int leadId) throws ParseException {
    List getLead = this.adminService.getFollowupsByLead(leadId);
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    datatableJsonObject.setRecordsFiltered(getLead.size());
    datatableJsonObject.setRecordsTotal(getLead.size());
    datatableJsonObject.setData(getLead);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"/getWhiteDetails"}, method = {RequestMethod.GET})
  @ResponseBody
  public String getWhiteDetails(HttpServletRequest request, HttpServletResponse response, @RequestParam("domain") String domain) throws ParseException {
    List<Object[]> getdetails = this.adminService.getWhiteDetails(domain);
    JSONArray mainArray = new JSONArray();
    JSONObject DataObj = new JSONObject();
    for (Object[] result : getdetails) {
      DataObj.put("logoImage", result[0].toString());
      DataObj.put("image2", result[1].toString());
    } 
    mainArray.add(DataObj);
    return mainArray.toString();
  }
  
  @RequestMapping(value = {"/exportLeadsCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public ModelAndView exportLeadsCallLog(HttpServletRequest request, HttpServletResponse response) throws ParseException, DocumentException, IOException {
    System.out.println("exportSuccessLeadsCallLog is Called....");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    String startDate = request.getParameter("stdate");
    String endDate = request.getParameter("endate");
    String extrap = request.getParameter("extrap");
    String teleid = request.getParameter("teleid");
    String catId = request.getParameter("catId");
    String callType=request.getParameter("callType");
    System.out.println("startDate::" + startDate + " " + "endDate::" + endDate + "--- teleid::" + teleid+"......."+callType);
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    StringBuilder sb = new StringBuilder("SELECT CONCAT(lead.firstName,' ', lead.lastName) cname,lead.mobileNo,lead.email,\tlead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.leadState,\tlead.leadProcessStatus,categorymanager.categortName, lead.createDate, \tCONCAT(tallycaller.firstname,tallycaller.lastname) telecallerName,\tDATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i') AS callTime,SEC_TO_TIME(followupshistory.callDuration) AS callduration,followupshistory.remark,case when followupshistory.sheduleTime='1980-06-30 17:36:00' then  'NA' else followupshistory.sheduleTime  END AS sheduleDate,lead.csvData\tFROM followupshistory INNER JOIN lead ON followupshistory.leadId = lead.leaadId   \tINNER JOIN tallycaller ON tallycaller.tcallerid = followupshistory.tallycallerId   \tINNER JOIN categorymanager ON categorymanager.categotyId = lead.categoryId   \tWHERE followupshistory.callStatus = 'Success' AND followupshistory.followsUpType = 'OutBound'   \tAND followupshistory.companyId = " + 
        
        Company_id + 
        " AND (followupshistory.callingTime BETWEEN '" + startDate + "' AND ADDDATE('" + endDate + 
        "', INTERVAL 1 DAY))");
    if (!teleid.equals("All"))
      sb.append(" AND followupshistory.tallycallerId=" + teleid); 
    if (!catId.equals("All"))
      sb.append(" AND lead.categoryId=" + catId); 
    
    sb.append(" AND followupshistory.followsUpType='" + callType + "' "); 
    System.out.println("Query : " + sb.toString());
    List listq = this.adminService.GetLeadParameterForSearch(sb.toString());
    Map<String, Object> model = new HashMap<>();
    model.put("type", "Success");
    model.put("extra", extrap);
    model.put("excelList", listq);
    return new ModelAndView("excelAllLogDownload", model);
  }
  
  @RequestMapping(value = {"/exportLeadsFailCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public ModelAndView exportLeadsFailCallLog(HttpServletRequest request, HttpServletResponse response) throws ParseException, DocumentException, IOException {
    System.out.println("exportLeadsFailCallLog is Called....");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    String startDate = request.getParameter("stdate");
    String endDate = request.getParameter("endate");
    String extrap = request.getParameter("extrap");
    String teleid = request.getParameter("teleid");
    String catId = request.getParameter("catId");
    String failStatus = request.getParameter("failStatus");
    String leadStatus = request.getParameter("leadStatus");
    String callType=request.getParameter("callType");
    System.out.println("startDate::" + startDate + " " + "endDate::" + endDate+"....................."+failStatus+"................"+callType);
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    StringBuilder sb = new StringBuilder("SELECT CONCAT(lead.firstName,' ', lead.lastName) cname,lead.mobileNo,lead.email,\tlead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.leadState,\tlead.leadProcessStatus,categorymanager.categortName, lead.createDate, \tCONCAT(tallycaller.firstname,tallycaller.lastname) telecallerName,\tDATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i') AS callTime,SEC_TO_TIME(followupshistory.callDuration) AS callduration,followupshistory.remark,followupshistory.failstatus, case when followupshistory.sheduleTime='1980-06-30 17:36:00' then 'NA' else followupshistory.sheduleTime end as sheduleTime,lead.csvData\tFROM followupshistory INNER JOIN lead ON followupshistory.leadId = lead.leaadId   \tINNER JOIN tallycaller ON tallycaller.tcallerid = followupshistory.tallycallerId   \tINNER JOIN categorymanager ON categorymanager.categotyId = lead.categoryId   \tWHERE followupshistory.callStatus = 'Fail' AND followupshistory.followsUpType = 'OutBound'   \tAND followupshistory.companyId = " + 
        Company_id + 
        " AND (followupshistory.callingTime BETWEEN '" + startDate + "' AND ADDDATE('" + endDate + 
        "', INTERVAL 1 DAY))");
    if (!teleid.equals("All"))
      sb.append(" AND followupshistory.tallycallerId=" + teleid); 
    if (!catId.equals("All"))
      sb.append(" AND lead.categoryId=" + catId); 
    if (!failStatus.equals("ALL"))
      sb.append(" AND followupshistory.failstatus='" + failStatus + "' "); 
    if (!leadStatus.equals("ALL"))
      sb.append(" AND lead.leadState='" + leadStatus + "' "); 
    
    sb.append(" AND followupshistory.followsUpType='" + callType + "' ");
    
    System.out.println("Query : " + sb.toString());
    List listq = this.adminService.GetLeadParameterForSearch(sb.toString());
    Map<String, Object> model = new HashMap<>();
    model.put("type", "Fail");
    model.put("extra", extrap);
    model.put("excelList", listq);
    return new ModelAndView("excelAllLogDownload", model);
  }
  
  @RequestMapping(value = {"/exportAllLeadsCallLog"}, method = {RequestMethod.GET})
  @ResponseBody
  public ModelAndView exportAllLeadsCallLog(HttpServletRequest request, HttpServletResponse response) throws ParseException, DocumentException, IOException {
    System.out.println("exportAllLeadsCallLog is Called....hiii");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    String startDate = request.getParameter("stdate");
    String endDate = request.getParameter("endate");
    String extrap = request.getParameter("extrap");
    String teleid = request.getParameter("teleid");
    String catId = request.getParameter("catId");
    String leadProcessStatus = request.getParameter("leadProcessStatus");
    String successStatus = request.getParameter("successStatus");
    System.out.println("successStatus::" + successStatus);
    String lSate = "All";
    List<Object[]> leadProState = this.adminService.getLeadProStateById(Integer.parseInt(leadProcessStatus));
    for (Object[] result : leadProState)
      lSate = result[2].toString(); 
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    StringBuilder sb = new StringBuilder("SELECT CONCAT(lead.firstName,' ', lead.lastName) cname,lead.mobileNo,lead.email,\tlead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.leadState,\tlead.leadProcessStatus,followupshistory.successStatus, lead.createDate, \tCONCAT(tallycaller.firstname,tallycaller.lastname) telecallerName,\tDATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i') AS callTime,SEC_TO_TIME(followupshistory.callDuration) AS callduration,followupshistory.remark,followupshistory.callStatus,followupshistory.failstatus,categorymanager.categortName,case when followupshistory.sheduleTime='1980-06-30 17:36:00' then 'NA' else  followupshistory.sheduleTime end as sheduleTime,lead.csvData\tFROM followupshistory INNER JOIN lead ON followupshistory.leadId = lead.leaadId   \tINNER JOIN tallycaller ON tallycaller.tcallerid = followupshistory.tallycallerId   \tINNER JOIN categorymanager ON categorymanager.categotyId = lead.categoryId   \tWHERE followupshistory.followsUpType = 'OutBound'   \tAND followupshistory.companyId = " + 
        
        Company_id + 
        " AND (followupshistory.callingTime BETWEEN '" + startDate + "' AND ADDDATE('" + endDate + 
        "', INTERVAL 1 DAY))");
    if (!teleid.equals("All"))
      sb.append(" AND followupshistory.tallycallerId=" + teleid); 
    if (!catId.equals("All"))
      sb.append(" AND lead.categoryId=" + catId); 
    if (!lSate.equals("All"))
      sb.append(" AND lead.leadProcessStatus='" + lSate + "' "); 
    if (!successStatus.equals("All"))
      sb.append(" AND followupshistory.successStatus='" + successStatus + "' "); 
    System.out.println("Query : " + sb.toString());
    List listq = this.adminService.GetLeadParameterForSearch(sb.toString());
    Map<String, Object> model = new HashMap<>();
    model.put("type", "All");
    model.put("extra", extrap);
    model.put("excelList", listq);
    return new ModelAndView("excelAllLogDownload", model);
  }
  
  @RequestMapping(value = {"/tcallerChangePass"}, method = {RequestMethod.GET})
  @ResponseBody
  public String tcallerChangePass(HttpServletRequest request, HttpServletResponse response, @RequestParam("tcId") int tcId, @RequestParam("newpassword") String newpassword, @RequestParam("oldpassword") String oldpassword) throws ParseException {
    Tallycaller caller = this.adminService.getTallycallerByIdMdel(tcId);
    String pass = "";
    if (caller != null)
      pass = caller.getPasswrd(); 
    if (pass.equals(oldpassword)) {
      int changeStat = this.adminService.tcallerChangePass(tcId, newpassword);
      if (changeStat == 1)
        return "1"; 
      return "0";
    } 
    return "2";
  }
  
  @RequestMapping(value = {"/AssignLeadStateGet"}, method = {RequestMethod.GET})
  @ResponseBody
  public List AssignLeadStateGet(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    System.out.println("GetAssignLeadState is Called ");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    List list = this.adminService.GetAssignLeadState((int)id);
    return list;
  }
  
  @RequestMapping(value = {"getTallyCallerLeadANDROAPI"}, produces = {"application/javascript"})
  @ResponseBody
  public String getTallyCallerLeadANDROAPI(HttpServletRequest request, HttpServletResponse response) throws Exception {
    System.out.println("getTallyCallerLeadANDROAPI is called");
    String searchParam = request.getParameter("searchParam");
    String tallycallerName = request.getParameter("tallycallerName");
    String api = request.getParameter("apikey");
    String limit = request.getParameter("limit");
    String pageNo = request.getParameter("pageNo");
    JSONObject mainjobj = new JSONObject();
    mainjobj.put("limit", limit.toString());
    mainjobj.put("page", pageNo.toString());
    JSONArray jarray = new JSONArray();
    Tallycaller tclaer = new Tallycaller();
    tclaer = this.adminService.GetTellyCallerByName(tallycallerName);
    if (tclaer == null)
      return "[{\"responseCode\":\"INVALID_TELECALLER\"}]"; 
    ApiKey apiKey = null;
    if (tclaer != null) {
      apiKey = this.userService.gettAPiByUid(tclaer.getTcallerid(), api, true);
      if (apiKey == null) {
        apiKey = this.userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
        if (apiKey == null)
          return "[{\"responseCode\":\"INVALID_KEY\"}]"; 
      } 
    } 
    Object[] result = null;
    String list = "";
    if (apiKey != null) {
      String query = "SELECT lead.leaadId,lead.firstName,lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,\n\tDATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),lead.tagName,DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),\n\tlead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState,lead.csvData,lead.leadState \n\t FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId \n\t INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid \n\t WHERE    0=0 AND lead.tallyCalletId=" + 
        
        tclaer.getTcallerid() + "  AND leadState='Open' AND ( lead.email LIKE'%" + searchParam + "%' OR \n" + 
        "\t lead.firstName LIKE '%" + searchParam + "%' OR lead.mobileNo LIKE '%" + searchParam + "%' OR lead.lastName LIKE '%" + searchParam + "%' \n" + 
        "\t ) ORDER BY lead.createDate DESC ";
      List<Object[]> list1 = this.adminService.AndroidLeadSearch(query, Integer.parseInt(limit), Integer.parseInt(pageNo));
      System.out.println("list1_Size :: " + list1.size());
      if (list1.size() != 0) {
        for (int i = 0; i < list1.size(); i++) {
          JSONObject leadmap = new JSONObject();
          result = list1.get(i);
          leadmap.put("leaadId", result[0]);
          leadmap.put("firstName", result[1]);
          leadmap.put("mobileNo", result[2]);
          leadmap.put("email", result[3]);
          leadmap.put("categoryName", result[4]);
          leadmap.put("leadProcessStatus", result[5]);
          leadmap.put("autoDial", result[6]);
          leadmap.put("leadType", result[7]);
          leadmap.put("sheduleDate", (result[8] == null) ? "N/A" : result[8]);
          leadmap.put("tagName", result[9]);
          leadmap.put("createDate", (result[10] == null) ? "N/A" : result[10]);
          if (result[11].toString().equalsIgnoreCase("1980-06-30 17:36:00")) {
            leadmap.put("sheduleDate", "Not Set");
          } else {
            leadmap.put("sheduleDate", (result[11] == null) ? "N/A" : result[11]);
          } 
          leadmap.put("lastName", result[12]);
          leadmap.put("companyName", result[13]);
          leadmap.put("website", result[14]);
          leadmap.put("Country", result[15]);
          leadmap.put("state", result[16]);
          leadmap.put("city", result[17]);
          leadmap.put("dialState", result[18]);
          leadmap.put("csvData", result[19]);
          leadmap.put("leadState", result[20]);
          leadmap.put("tag", "");
          jarray.add(leadmap);
        } 
        String totalRecord = this.adminService.getAndroTellyCount(tallycallerName);
        mainjobj.put("total_rec", totalRecord.toString());
        mainjobj.put("Data", jarray);
      } 
      return mainjobj.toString();
    } 
    return "[{\"responseCode\":\"INVALID_KEY\"}]";
  }
  
  @RequestMapping(value = {"/GetTellyCallerLastAccess"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetTellyCallerLastAccess(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
    Gson gson = null;
    try {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      MediUser currentUser = (MediUser)auth.getPrincipal();
      long id = currentUser.getUserid();
      List list = this.adminService.getTeleLastAccess((int)id);
      datatableJsonObject.setRecordsFiltered(list.size());
      datatableJsonObject.setRecordsTotal(list.size());
      datatableJsonObject.setData(list);
      gson = (new GsonBuilder()).setPrettyPrinting().create();
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return gson.toJson(datatableJsonObject);
  }
  
  @RequestMapping(value = {"GetCatAutoDialSingalLeadAPI"}, produces = {"application/javascript"})
  @ResponseBody
  public String GetCatAutoDialSingalLeadAPI(HttpServletRequest request, HttpServletResponse response) throws Exception {
    System.out.println("GetCatAutoDialSingalLeadAPI is called.....");
    String tallycallerId = request.getParameter("tallycallerId");
    String api = request.getParameter("apikey");
    int cat = Integer.parseInt(request.getParameter("catId"));
    JSONArray jarray = new JSONArray();
    ApiKey apiKey = null;
    apiKey = this.userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
    JSONObject tcallerWbJsom = new JSONObject();
    if (apiKey == null) {
      apiKey = this.userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
      if (apiKey == null)
        return "[{\"responseCode\":\"INVALID_KEY\"}]"; 
    } 
    if (apiKey != null) {
      apiKey = this.userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
      if (apiKey == null) {
        apiKey = this.userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
        if (apiKey == null)
          return "[{\"responseCode\":\"INVALID_KEY\"}]"; 
      } 
    } 
    if (apiKey != null) {
      int updateStat = this.adminService.updateTelecallerAccessDate(Integer.parseInt(tallycallerId), api);
      List<Object[]> list = this.adminService.GetCatAutoDialSingalLead(Integer.parseInt(tallycallerId), cat);
      if (list.size() != 0) {
        for (int i = 0; i < list.size(); i++) {
          JSONObject leadmap = new JSONObject();
          Object[] result = list.get(i);
          leadmap.put("leadId", result[0]);
          leadmap.put("categoryName", result[1]);
          leadmap.put("createDate", result[2]);
          leadmap.put("email", result[3]);
          leadmap.put("firstName", result[4]);
          leadmap.put("lastName", result[5]);
          leadmap.put("leadProcessStatus", result[6]);
          leadmap.put("leadState", result[7]);
          leadmap.put("mobileNo", result[8]);
          leadmap.put("csvData", (result[10] == null) ? "N/A" : result[10]);
          leadmap.put("leadType", result[11]);
          leadmap.put("autoDial", result[12]);
          leadmap.put("companyName", result[13]);
          leadmap.put("website", result[14]);
          leadmap.put("Country", result[15]);
          leadmap.put("state", result[16]);
          leadmap.put("city", result[17]);
          if (result[9].toString().equalsIgnoreCase("1980-06-30 17:36:00")) {
            leadmap.put("sheduleDate", "Not Set");
          } else {
            leadmap.put("sheduleDate", (result[9] == null) ? "N/A" : result[9]);
          } 
          jarray.add(leadmap);
          tcallerWbJsom.put("data", jarray);
        } 
      } else {
        if (request.getParameter("callback") != null)
          return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");"; 
        return "[{\"responceCode\":\"No Data Found\"}]";
      } 
    } 
    if (request.getParameter("callback") != null)
      return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");"; 
    return jarray.toJSONString();
  }
  
  @RequestMapping(value = {"gubbaLeadSave"}, produces = {"application/javascript"})
  @ResponseBody
  public String gubbaLeadSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    int id = (int)currentUser.getUserid();
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    Date date1 = (new SimpleDateFormat("dd/MM/yyyy")).parse(startDate);
    Date date2 = (new SimpleDateFormat("dd/MM/yyyy")).parse(endDate);
    String parsedStDate = this.scheduledf.format(date1);
    String parsedEnDate = this.scheduledf.format(date2);
    Date createDt = new Date();
    String createDtStr = this.scheduledf.format(createDt);
    Date createDtNew = this.scheduledf.parse(createDtStr);
    System.out.println("createDt :: " + createDt + "---" + "createDtNew::" + createDtNew + "--------" + this.scheduledf.format(createDtNew));
    Date startDateNew = this.scheduledf.parse(parsedStDate);
    Date endDateNew = this.scheduledf.parse(parsedEnDate);
    System.out.println("date1 :: " + date1 + "---" + "date2::" + date2 + "----createDtNew::" + createDtNew);
    List<Date> dates = new ArrayList<>();
    long interval = 86400000L;
    long endTime = date2.getTime();
    long curTime = date1.getTime();
    while (curTime <= endTime) {
      dates.add(new Date(curTime));
      curTime += interval;
    } 
    for (int s = 0; s < dates.size(); s++) {
      Date lDate = dates.get(s);
      String ds = (new SimpleDateFormat("dd/MM/yyyy")).format(lDate);
      Date createNew = (new SimpleDateFormat("dd/MM/yyyy")).parse(ds);
      String parsedCreateDate = this.scheduledf.format(createNew);
      String url = "https://erp.gubbacold.com/gubbaerp/ws/com.saksham.paymenttracker.CRMCallList?followupDate=" + ds;
      URL obj = new URL(url);
      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
      con.setRequestMethod("GET");
      String plainCredentials = "Bonrix:Bonrix@123";
      String base64Credentials = new String(Base64.encodeBase64(plainCredentials.getBytes()));
      con.setRequestProperty("Authorization", "Basic " + base64Credentials);
      con.setRequestProperty("User-Agent", "User-Agent");
      con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
      con.setRequestMethod("GET");
      con.setDoInput(true);
      con.setDoOutput(true);
      con.connect();
      int responseCode = con.getResponseCode();
      BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
      StringBuffer response1 = new StringBuffer();
      String inputLine;
      while ((inputLine = in.readLine()) != null)
        response1.append(inputLine); 
      in.close();
      JSONObject jo = (JSONObject)(new JSONParser()).parse(response1.toString());
      JSONArray ja = (JSONArray)jo.get("data");
      String msg = jo.get("status").toString();
      Tallycaller tclaer = new Tallycaller();
      if (msg.equals("Failed")) {
        System.out.println("No Data");
      } else {
        for (int i = 0; i < ja.size(); i++) {
          JSONObject json_data = (JSONObject)ja.get(i);
          String mobile = json_data.get("Mobile Number").toString().trim();
          String catID = "0";
          tclaer = this.adminService.GetTellyCallerByName(json_data.get("Telecaller Username").toString().trim());
          if (tclaer == null) {
            System.out.println("INVALID_TELECALLER");
          } else {
            if (json_data.get("Telecaller Username").toString().trim().equals("Tharala")) {
              catID = "2121";
            } else if (json_data.get("Telecaller Username").toString().trim().equals("Shashikala")) {
              catID = "2120";
            } else {
              catID = "0";
            } 
            System.out.println("catID:::" + catID);
            String query = "SELECT COUNT(*) FROM lead WHERE mobileNo=" + mobile + " AND companyId=" + id + " AND categoryId=" + catID + " ";
            int ldc = this.adminService.getCountTele(query);
            System.out.println("mobile:::" + mobile + "--- ldc:::" + ldc);
            if (ldc == 0) {
              System.out.println("IF");
              Lead ld = new Lead();
              ld.setAssignDate(new Date());
              ld.setAutoDial(true);
              ld.setcategoryId(catID);
              ld.setCity(json_data.get("City").toString().trim());
              ld.setCompanyId(id);
              ld.setCompanyName(json_data.get("Company Name").toString().trim());
              ld.setCountry(json_data.get("Country").toString().trim());
              ld.setCreateDate(createDtNew);
              ld.setCsvData("{}");
              ld.setDialState(false);
              ld.setemail(json_data.get("Email Id").toString().trim());
              ld.setFirstName(json_data.get("First Name").toString().trim());
              ld.setLastName(json_data.get("Last Name").toString().trim());
              ld.setLeadProcessStatus("None");
              ld.setLeadcomment("N/A");
              ld.setLeadState("Open");
              ld.setLeadType("API");
              ld.setMobileNo(json_data.get("Mobile Number").toString().trim());
              ld.setNotification_flag("TRUE");
              ld.setSheduleDate(this.scheduledf.parse(json_data.get("Schedule Date").toString().trim()));
              ld.setStaffId(0);
              ld.setState(json_data.get("Stae").toString().trim());
              ld.setTagName("");
              ld.setTallyCalletId(tclaer.getTcallerid());
              ld.setWebsite("N/A");
              this.adminService.AddLead(ld);
            } else {
              System.out.println("ELSE");
              String queryNew = " UPDATE lead SET categoryId=" + catID + ",firstName='" + json_data.get("First Name").toString().trim() + "',lastName='" + json_data.get("Last Name").toString().trim() + "',email='" + json_data.get("Email Id").toString().trim() + "',companyName='" + json_data.get("Company Name").toString().trim() + "'," + 
                "Country='" + json_data.get("Country").toString().trim() + "',createDate='" + this.scheduledf.format(createDtNew) + "',state='" + json_data.get("Stae").toString().trim() + "',city='" + json_data.get("City").toString().trim() + "',sheduleDate='" + json_data.get("Schedule Date").toString().trim() + "' " + 
                ",leadType='API'  WHERE mobileNo='" + mobile + "' AND companyId=" + id + " AND categoryId=" + catID;
              System.out.println("queryNew::" + queryNew);
              this.adminService.updateLead(queryNew);
            } 
          } 
        } 
      } 
    } 
    return "1";
  }
  
  @RequestMapping(value = {"/GetLeadNewGubba"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadNewGubba(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("GetLeadGubba:: " + Company_id);
    JSONArray jarray = new JSONArray();
    String list = "";
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id + " AND lead.leadType='API' ORDER BY lead.createDate DESC";
    System.out.println("GubbaGetLead::" + query);
    String queryCount = "SELECT count(*) FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id + " AND lead.`leadType`='API' ";
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    try {
      if (request.getParameter("start") != null) {
        start = Integer.parseInt(request.getParameter("start"));
        System.out.println("STSRT IF : " + start);
      } else {
        System.out.println("STSRT ELSE: " + start);
      } 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        System.out.println("LENGTH IF: " + len);
        listSize = len;
      } else {
        System.out.println("LENGTH ELSE: " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("page::" + page + "----" + "listSize::" + listSize);
    list = this.adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    if (list.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + list + ");"; 
      return list;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"/GetLeadSatcopIndiaMart"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadSatcopIndiaMart(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("GetLeadSatcopIndiaMart:: " + Company_id);
    JSONArray jarray = new JSONArray();
    String list = "";
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id + " AND lead.leadType='IndiaMart' ORDER BY lead.createDate DESC";
    System.out.println("GubbaGetLead::" + query);
    String queryCount = "SELECT count(*) FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id + " AND lead.`leadType`='IndiaMart' ";
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    try {
      if (request.getParameter("start") != null) {
        start = Integer.parseInt(request.getParameter("start"));
        System.out.println("STSRT IF : " + start);
      } else {
        System.out.println("STSRT ELSE: " + start);
      } 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        System.out.println("LENGTH IF: " + len);
        listSize = len;
      } else {
        System.out.println("LENGTH ELSE: " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("page::" + page + "----" + "listSize::" + listSize);
    list = this.adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    if (list.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + list + ");"; 
      return list;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"satcopJustDialLead"}, produces = {"application/javascript"})
  @ResponseBody
  public String satcopJustDialLead(HttpServletRequest request, HttpServletResponse response) throws Exception {
    String email = request.getParameter("email");
    String firstName = request.getParameter("firstName");
    String lastName = request.getParameter("lastName");
    String mobileNo = request.getParameter("mobileNo");
    String country = request.getParameter("country");
    String state = request.getParameter("state");
    String city = request.getParameter("city");
    String companyName = request.getParameter("companyName");
    String prefix = request.getParameter("prefix");
    Date createDt = new Date();
    String createDtStr = this.scheduledf.format(createDt);
    Date createDtNew = this.scheduledf.parse(createDtStr);
    String catId = "3132";
    int compId = 400;
    String query = "SELECT COUNT(*) FROM lead WHERE mobileNo=" + mobileNo + " AND companyId=" + compId + " AND categoryId=" + catId + " ";
    int ldc = this.adminService.getCountTele(query);
    if (ldc == 0) {
      SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
      Lead lead = new Lead();
      lead.setcategoryId(catId);
      lead.setstaffId(0);
      lead.setCreateDate(new Date());
      lead.setemail(email.equalsIgnoreCase("") ? "N/A" : email);
      lead.setFirstName((String.valueOf(prefix) + " " + firstName.equalsIgnoreCase("") == null) ? "N/A" : firstName);
      lead.setLastName(lastName.equalsIgnoreCase("") ? "N/A" : lastName);
      lead.setLeadType("JustDial");
      lead.setLeadProcessStatus("None");
      lead.setMobileNo(mobileNo);
      lead.setLeadState("Open");
      lead.setSheduleDate(this.df.parse("1980-06-30 17:36:00"));
      lead.setCompanyId(compId);
      lead.setTallyCalletId(0);
      lead.setCountry(country);
      lead.setState(state);
      lead.setCity(city);
      lead.setCompanyName(companyName);
      lead.setWebsite("N/A");
      lead.setLeadcomment("N/A");
      lead.setTagName("N/A");
      lead.setNotification_flag("FALSE");
      lead.setCsvData("{}");
      lead.setAssignDate(new Date(1980, 6, 30, 17, 36, 0));
      lead.setAltmobileNo("");
      this.adminService.AddLead(lead);
    } else {
      String queryNew = " UPDATE lead SET firstName='" + firstName + "',lastName='" + lastName + "',email='" + email + "',companyName='" + companyName + "'," + 
        "Country='" + country + "',createDate='" + this.scheduledf.format(createDtNew) + "',state='" + state + "',city='" + city + "' " + 
        ",leadType='JustDial'  WHERE mobileNo='" + mobileNo + "' AND companyId=" + compId + " AND categoryId=" + catId;
      this.adminService.updateLead(queryNew);
    } 
    return "RECEIVED";
  }
  
  @RequestMapping(value = {"glypticArtsIndiaMartLeadSave"}, produces = {"application/javascript"})
  @ResponseBody
  public String glypticArtsIndiaMartLeadSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    int id = (int)currentUser.getUserid();
    String startDate = "01-SEP-2020";
    String endDate = "06-SEP-2020";
    String url = "https://mapi.indiamart.com/wservce/enquiry/listing/GLUSR_MOBILE/7055660088/GLUSR_MOBILE_KEY/MTYwNjc0MzU0Ny45MTgjNTMxNjQ4Mw==/Start_Time/<startDate>/End_Time/<endDate>/";
    url = url.replace("<startDate>", startDate).replace("<endDate>", endDate);
    System.out.println("urlNew::" + url);
    URL obj = new URL(url);
    HttpURLConnection con = (HttpURLConnection)obj.openConnection();
    con.setRequestProperty("User-Agent", "User-Agent");
    con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    con.setRequestMethod("GET");
    con.setDoInput(true);
    con.setDoOutput(true);
    con.connect();
    int responseCode = con.getResponseCode();
    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    StringBuffer response1 = new StringBuffer();
    String inputLine;
    while ((inputLine = in.readLine()) != null)
      response1.append(inputLine); 
    in.close();
    DateFormat formatter = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
    SimpleDateFormat parser = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
    Tallycaller tclaer = new Tallycaller();
    if (response1.toString() == null)
      return "No Data Found"; 
    JSONArray ja = (JSONArray)(new JSONParser()).parse(response1.toString());
    String msg = "ABC";
    if (msg.equals("Failed")) {
      System.out.println("No Data");
    } else {
      System.out.println("Size ::" + ja.size());
      for (int i = 0; i < ja.size(); i++) {
        JSONObject json_data = (JSONObject)ja.get(i);
        String mobile = json_data.get("MOB").toString().trim();
        String catID = "2426";
        System.out.println("catID:::" + catID);
        String query = "SELECT COUNT(*) FROM lead WHERE mobileNo=" + mobile + " AND companyId=" + id + " AND categoryId=" + catID + " ";
        int ldc = this.adminService.getCountTele(query);
        System.out.println("mobile:::" + mobile + "--- ldc:::" + ldc);
        if (ldc == 0) {
          System.out.println("IF");
          String email = "";
          String companyName = "";
          if (json_data.get("SENDEREMAIL").toString() == null) {
            email = "";
          } else {
            email = json_data.get("SENDEREMAIL").toString().trim();
          } 
          if (json_data.get("GLUSR_USR_COMPANYNAME").toString().trim() == null) {
            companyName = "";
          } else {
            companyName = json_data.get("GLUSR_USR_COMPANYNAME").toString().trim();
          } 
        } else {
          System.out.println("Else");
        } 
      } 
    } 
    return "1";
  }
  
  @RequestMapping(value = {"/GetScheduledManagerLead"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetScheduledManagerLead(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    String stdate = request.getParameter("stdate");
    String endate = request.getParameter("endate");
    String tcID = request.getParameter("telecallerID");
    if (tcID.isEmpty())
      tcID = "0"; 
    System.out.println("stdate::" + stdate + "----endate::" + endate + "---tcID::" + tcID);
    JSONArray jarray = new JSONArray();
    String list = "";
    StringBuilder query = new StringBuilder();
    StringBuilder queryCount = new StringBuilder();
    query.append(
        "SELECT lead.leaadId,lead.firstName,lead.email,lead.mobileNo,categorymanager.categortName,lead.leadProcessStatus,\r\n       DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),tallycaller.username,lead.autoDial,lead.leadType,lead.tagName,\r\n       DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),lead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState,\r\n       lead.csvData,lead.altmobileNo FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    INNER JOIN followupshistory ON lead.tallyCalletId=followupshistory.tallycallerId  WHERE 0=0 AND leadState='Open' AND lead.companyId=" + 
        
        id + " " + 
        " AND lead.sheduleDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY) ");
    if (!tcID.equals("0"))
      query.append("AND lead.tallyCalletId=" + tcID); 
    query.append(" GROUP BY lead.leaadId ORDER BY lead.sheduleDate ASC ");
    System.out.println("query:::" + query);
    queryCount.append("SELECT count(*)  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid     WHERE    0=0  AND leadState='Open' AND lead.companyId=" + 
        
        id + " AND lead.sheduleDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
        "', INTERVAL 1 DAY) ");
    if (!tcID.equals("0"))
      queryCount.append("AND lead.tallyCalletId=" + tcID); 
    queryCount.append(" ORDER BY lead.sheduleDate ASC ");
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    try {
      if (request.getParameter("start") != null) {
        start = Integer.parseInt(request.getParameter("start"));
        System.out.println("STSRT IF : " + start);
      } else {
        System.out.println("STSRT ELSE: " + start);
      } 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        System.out.println("LENGTH IF: " + len);
        listSize = len;
      } else {
        System.out.println("LENGTH ELSE: " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("page::" + page + "----" + "listSize::" + listSize);
    list = this.adminService.ConvertDataToWebJSON(queryCount.toString(), query.toString(), page, listSize);
    if (list.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + list + ");"; 
      return list;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  @RequestMapping(value = {"/GetLeadGlypticarts"}, method = {RequestMethod.GET})
  @ResponseBody
  public String GetLeadGlypticarts(HttpServletRequest request, HttpServletResponse response) throws ParseException {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    MediUser currentUser = (MediUser)auth.getPrincipal();
    long id = currentUser.getUserid();
    int Company_id = (int)id;
    System.out.println("GetLeadGubba:: " + Company_id);
    JSONArray jarray = new JSONArray();
    String list = "";
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id + " AND lead.leadType='IndiaMart' ORDER BY lead.createDate DESC";
    System.out.println("GubbaGetLead::" + query);
    String queryCount = "SELECT count(*) FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      Company_id + " AND lead.`leadType`='IndiaMart' ";
    int page = 1;
    int listSize = 10;
    int start = 0;
    int len = 0;
    try {
      if (request.getParameter("start") != null) {
        start = Integer.parseInt(request.getParameter("start"));
        System.out.println("STSRT IF : " + start);
      } else {
        System.out.println("STSRT ELSE: " + start);
      } 
      if (request.getParameter("length") != null) {
        len = Integer.parseInt(request.getParameter("length"));
        System.out.println("LENGTH IF: " + len);
        listSize = len;
      } else {
        System.out.println("LENGTH ELSE: " + len);
      } 
      page = start / len + 1;
    } catch (Exception exception) {}
    System.out.println("page::" + page + "----" + "listSize::" + listSize);
    list = this.adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
    if (list.length() != 0) {
      if (request.getParameter("callback") != null)
        return String.valueOf(request.getParameter("callback")) + "(" + list + ");"; 
      return list;
    } 
    return String.valueOf(request.getParameter("callback")) + "(" + jarray.toJSONString() + ");";
  }
  
  
  
  @RequestMapping(value = "/exportAllLeads", method = RequestMethod.GET)
	public @ResponseBody ModelAndView exportAllLeads1(HttpServletRequest request, HttpServletResponse response)
	        throws ParseException, DocumentException, IOException{
		System.out.println("exportAllLeads is Called........");
	    try {
	        
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        MediUser currentUser = (MediUser) auth.getPrincipal();
	        String extrap = request.getParameter("extrap");
	        long id=  currentUser.getUserid();
	     
	        List listq=null;
	    
	        listq = adminService.GetAllLeads("SELECT \n" + 
	        		"    CONCAT(lead.firstName,' ', lead.lastName) AS cname,\n" + 
//	        		"    lead.categoryId,\n" + 
	        		"    categorymanager.categortName,lead.email,lead.mobileNo,lead.Country,lead.state,lead.city,lead.companyName,\n" + 
	        		"    CASE \n" + 
	        		"        WHEN lead.sheduleDate = '1980-06-30 17:36:00' THEN 'NOT SET'\n" + 
	        		"        ELSE lead.sheduleDate\n" + 
	        		"    END AS sheduleDate,\n" + 
//	        		"    lead.tallyCalletId,\n" + 
	        		"    tallycaller.username,lead.leadProcessStatus,lead.leadState,lead.website \n" + 
	        		"FROM  LEAD \n" + 
	        		"INNER JOIN  tallycaller ON lead.tallyCalletId = tallycaller.tcallerid \n" + 
	        		"INNER JOIN  categorymanager ON lead.categoryId = categorymanager.categotyId  \n" + 
	        		"WHERE lead.companyId ="+id);
	        		//select CONCAT(lead.firstName,' ', lead.lastName) cname,categoryId,email,mobileNo,Country,state,city,companyName,sheduleDate,tallyCalletId,leadProcessStatus,leadState,website from lead where companyId="+270
	        Map<String, Object> model = new HashMap<>();
	        model.put("type", "AllLeadData");
	        model.put("extra", extrap);
	        model.put("excelList", listq);

	        return new ModelAndView("excelAllLogDownload", model);

	    } catch (Exception e) {
	        // Log the exception
	        System.out.println("Error occurred");
	        e.printStackTrace();
	        // Handle the exception or return an error view
	        return new ModelAndView("errorView");
	    }
	}

  @RequestMapping(value = "/addLeadsToAutoDialManagerAPI", method = RequestMethod.GET)
	public  ResponseEntity<Integer> addLeadsToAutoDialManagerAPI(@RequestParam("catId") String catId)
	        throws ParseException, DocumentException, IOException{
		System.out.println("addToAutoDial is Called........");

		try {
		
		System.out.println(catId);
		adminService.AddAutoDial("update lead set autoDial=1 where categoryId="+catId);
		 return ResponseEntity.ok(1); // Return 1 on success
	    } catch (Exception e) {
	        e.printStackTrace(); // Log the stack trace for debugging
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(0); // Return 0 on error
	    }
		
  }
}


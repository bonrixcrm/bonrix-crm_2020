package com.bonrix.sms.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bonrix.sms.model.AndroidLog;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.SystemParameter;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.WebSocketObj;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.bonrix.sms.utils.AndroidResponceHashMap;
import com.bonrix.sms.utils.CRMHashMap;
import com.google.gson.Gson;

import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.MultimediaInfo;
@Controller
@MultipartConfig  
public class AndroidController {

	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	SMSService smsService;
	

	@Autowired
	CommonService cservice;

	static CRMHashMap map = new CRMHashMap();
	private static Logger LOG = LoggerFactory.getLogger("SMPPINFO");
	final SimpleDateFormat df4 = new SimpleDateFormat("yyyyMMddHHmmss");

	@RequestMapping(value = "/AutoDialDisconnectRequest", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String AutoDialDisconnectRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId, @RequestParam(value = "mobileNo") String mobileNo,
			@RequestParam(value = "waitresponse") String waitresponse, @RequestParam(value = "apikey") String apikey,
			@RequestParam(value = "callback") String callback) throws ParseException, InterruptedException {

		int counter = 0;
		// AndroidResponceHashMap.getInstance().RemoveClient(telecallId);
		WebSocketObj wsobj = CRMHashMap.getInstance().connectedClient.get(telecallId);
		String sendstr = "[{\"Type\":\"MOBILE_NO_AUTO_DIAL_DISCONNECT_REQUEST\",\"Status\":\"Mobile Number Send For End Call\",\"TelecallerId\":\""
				+ telecallId + "\",\"MobileNo\":\"" + mobileNo + "\"}]";
		LOG.info("SENDING:" + sendstr);
		String resp = AndroidResponceHashMap.getInstance().getClient(telecallId);

		if (resp == null) {
			try {
				wsobj.getSkt().send(sendstr);
			} catch (Exception e) {
				return callback + "(0);";
			}

		} else {
			JSONObject jobj = new JSONObject();
			JSONObject j2 = new JSONObject(resp);
			jobj.put("fid", j2.getString("FID"));
			jobj.put("Callstatus", j2.getJSONObject("TelecallerReqInfo").getString("Callstatus"));
			jobj.put("Audiofilename", j2.getJSONObject("TelecallerReqInfo").getString("Audiofilename"));
			jobj.put("Callduration", j2.getJSONObject("TelecallerReqInfo").getString("Callduration"));
			/*String status=adminService.updateCallStatus(Integer.parseInt(j2.getString("FID")),j2.getJSONObject("TelecallerReqInfo").getString("Callstatus"));
			System.out.println(status);
			LOG.info("AutoDialDisconnectRequest:: "+status);*/
			AndroidResponceHashMap.getInstance().RemoveClient(telecallId);
			return callback + "(" + jobj.toString() + ");";

		}

		String fid = "";
		String res = "";
		while (true) {
			resp = AndroidResponceHashMap.getInstance().getClient(telecallId);
		
			if (resp != null) {
				JSONObject jobj = new JSONObject();
				JSONObject j2 = new JSONObject(resp);
				jobj.put("fid", j2.getString("FID"));
				jobj.put("Callstatus", j2.getJSONObject("TelecallerReqInfo").getString("Callstatus"));
				jobj.put("Audiofilename", j2.getJSONObject("TelecallerReqInfo").getString("Audiofilename"));
				jobj.put("Callduration", j2.getJSONObject("TelecallerReqInfo").getString("Callduration"));
				res = jobj.toString();
				String status=adminService.updateCallStatus(Integer.parseInt(j2.getString("FID")),j2.getJSONObject("TelecallerReqInfo").getString("Callstatus"));
				System.out.println(status);
				LOG.info("AutoDialDisconnectRequest:: "+status);
				String delCLient = AndroidResponceHashMap.getInstance().RemoveClient(telecallId);

				System.out.println("AutoDialDisconnectRequest Remove Android Client : " + delCLient);
				break;
			}
			if (counter++ > 30) {
				resp = "Connection Error";
				break;
			}
			Thread.sleep(1000);
		}
		return callback + "(" + res + ");";
	}

	@RequestMapping(value = "/AutoDialRequest", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String AutoDialRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId, @RequestParam(value = "mobileNo") String mobileNo,
			@RequestParam(value = "waitresponse") String waitresponse, @RequestParam(value = "lid") Integer lid,
			@RequestParam(value = "callback") String callback) throws ParseException, InterruptedException {

		AndroidResponceHashMap.getInstance().sendsuccess.remove(telecallId);
		AndroidResponceHashMap.getInstance().RemoveClient(telecallId);
		WebSocketObj wsobj = CRMHashMap.getInstance().connectedClient.get(telecallId);

		if (wsobj == null || wsobj.getSkt() == null || !wsobj.getSkt().isOpen()) {
			return callback + "(0)";
		}

		Followupshistory fhist = new Followupshistory();

		fhist.setTallycallerId(Integer.parseInt(telecallId));
		fhist.setLeadId(lid);

		Tallycaller tc = adminService.GetTellyCallerById(Integer.parseInt(telecallId));

		fhist.setCompanyId(tc.getCompanyId());
		fhist.setCallingTime(new Date());
		fhist.setCallStatus("NA");
		fhist.setAudoiFilePath("N/A");
		fhist.setSheduleTime(df4.parse("19800630173600"));
		fhist.setFollowsUpType("OutBound");
		fhist.setFailstatus("None");
		fhist.setSuccessStatus("None");
		fhist = (Followupshistory) cservice.saveObject(fhist);

		String sendstr = "[{\"FID\":\"" + fhist.getFollowupsId()
				+ "\",\"Type\":\"MOBILE_NO_AUTO_DIAL_REQUEST\",\"Status\":\"Mobile Number Send For Make Call\",\"TelecallerId\":\""
				+ telecallId + "\",\"MobileNo\":\"" + mobileNo + "\"}]";
		LOG.info("SENDING:" + sendstr);
		/*SystemParameter param=(SystemParameter)cservice.getSysParam();
		
		if(param.getIsactive()==1)
		{
		AndroidLog log=new AndroidLog();
		log.setCretaeDate(new Date());
		log.setFolloId(fhist.getFollowupsId());
		log.setLeadid(0);
		log.setRequest(sendstr);
		log.setResponce("N/A");
		log.setTeleId(Integer.parseInt(telecallId));
		log=(AndroidLog)cservice.saveObject(log);
		LOG.info("Android Log Info -| "+log.getId());
		}*/

		try {
			wsobj.getSkt().send(sendstr);
			return callback + "(" + fhist.getFollowupsId() + ")";
		} catch (Exception e) {
			e.printStackTrace();
			return callback + "(0)";
		}

		// for (Map.Entry<String, WebSocketObj> entry :
		// CRMHashMap.getInstance().connectedClient.entrySet()) {
		// WebSocketObj wskt = entry.getValue();
		// String tcId = wskt.getTcalletId();
		// if (telecallId.equalsIgnoreCase(wskt.tcalletId.toString())) {
		//
		// WebSocket skt = wskt.getSkt();
		// System.out.println("Sending Mobile Number Packet For Calling.... : "
		// + skt + " : " + entry.getKey());
		// String msg = "Hello Sajna";
		// // skt1.sendMessage("Hello Sajna","MOBILE_NO_SEND");
		// try {
		// skt.send(
		// "[{\"Type\":\"MOBILE_NO_AUTO_DIAL_REQUEST\",\"Status\":\"Mobile
		// Number Send For Make Call\",\"TelecallerId\":\""
		// + telecallId + "\",\"MobileNo\":\"" + mobileNo + "\"}]");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }
		// }

		// androidMap.getInstance().AddClient(telecallId,null);

		//////////////////////////////////////////
		// No need of called Phone Info for now //
		///////////////////////////////////////////
		// int counter = 0;
		// String resp = null;
		// while (true) {
		// resp =
		// AndroidResponceHashMap.getInstance().sendsuccess.get(telecallId);
		// System.out.println("AutoDialRequest Responce : " + resp);
		// if (resp != null) {
		// System.out.println("AutoDialRequest Responce IN : " + resp);
		//
		// AndroidResponceHashMap.getInstance().sendsuccess.remove(telecallId);
		// //androidMap.getClient(telecallId);
		// // Thread.sleep(1000);
		// // String delCLient= androidMap.RemoveClient(telecallId);
		// // System.out.println("AutoDialRequest Remove Android Client :
		// // "+delCLient);
		// break;
		// }
		// if (counter++ > 4) {
		// resp =
		// "{'Callstatus':'Connectio_Error','Callduration':'0','Audiofilename':'N/A','lastcallno':'N/A'}";
		// break;
		// }
		//
		// Thread.sleep(10000);
		// }
		// return callback + "(1)";

	}

	@RequestMapping(value = "/ReceiveCallRequest", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String ReceiveCallRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId, @RequestParam(value = "mobileNo") String mobileNo)
			throws ParseException, InterruptedException {
		System.out.println(" : " + telecallId + " : " + mobileNo);
		AndroidResponceHashMap.getInstance().AddClient(telecallId, "N/A");
		// androidMap.getInstance().AddClient(telecallId,null);
		int counter = 0;
		String resp = null;
		while (true) {
			resp = AndroidResponceHashMap.getInstance().getClient(telecallId);
			System.out.println("Responce1  : " + resp);
			if (resp != "N/A") {
				String client = AndroidResponceHashMap.getInstance().getClient(telecallId);
				Thread.sleep(5000);
				String delCLient = AndroidResponceHashMap.getInstance().RemoveClient(telecallId);
				System.out.println("Remove Android Client : " + delCLient);
				break;
			}
			Thread.sleep(5000);
		}
		return resp;
	}

	@RequestMapping(value = "/ReceivedCallDisconnectRequest", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String ReceivedCallDisconnectRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId, @RequestParam(value = "mobileNo") String mobileNo)
			throws ParseException, InterruptedException {
		System.out.println("In ReceivedCallDisconnectRequest....");
		System.out.println(" : " + telecallId + " : " + mobileNo);
		AndroidResponceHashMap.getInstance().AddClient(telecallId, "N/A");
		// androidMap.getInstance().AddClient(telecallId,null);
		int counter = 0;
		String resp = null;
		while (true) {
			resp = AndroidResponceHashMap.getInstance().getClient(telecallId);
			// System.out.println("Responce : "+resp);
			if (resp != "N/A") {
				String client = AndroidResponceHashMap.getInstance().getClient(telecallId);
				Thread.sleep(1000);
				String delCLient = AndroidResponceHashMap.getInstance().RemoveClient(telecallId);
				System.out.println("Remove Android Client : " + delCLient);
				break;
			}
			Thread.sleep(5000);
		}
		return resp;
	}

	// http://localhost:8191/spring-hib/uploadAudioFIleAPI
	@RequestMapping(value = "uploadAudioFIleAPI", method = RequestMethod.POST)
	public @ResponseBody String uploadAudioFIleAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("file") MultipartFile file,
			@RequestParam("tellcallerid") int teleId) throws Exception {
		System.out.println("uploadAudioFIleAPI is Called..");
		LOG.info("Hello API ................");
		System.out.println("FIle Path : " + file);
		String fileName = null;
		String filePath = null;
		String userName=null;
		Tallycaller caller=adminService.getTallycallerByIdMdel(teleId);
		
		User user=null;
		if(caller!=null)
		{
		 user=adminService.getComapnyByTeclId(caller.getCompanyId());
		System.out.println("USER_NAME :: "+user.getUid());
		}
		if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				System.out.println("FILE NAME :: "+file.getOriginalFilename());
				filePath = request.getServletContext().getRealPath("//");
				filePath = filePath + SEP + "Sound" + SEP;
				filePath=filePath.concat(Long.toString(user.getUid()))+SEP;
				System.out.println(filePath);
				File directory = new File(filePath);
				if (! directory.exists()){
					   directory.mkdir();
					}
				
				BufferedOutputStream buffStream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath + fileName)));

				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				
				String[] fileData=fileName.split("_");
				System.out.println("FILE ARRAY SIZE :: "+fileData+" :: "+fileData[4].substring(0,fileData[4].indexOf(".")));
				
				Followupshistory hst=adminService.getFollowupsHistoryByFID(Integer.parseInt(fileData[4].substring(0,fileData[4].indexOf("."))));
				if(hst.getCallStatus().equalsIgnoreCase("Success") || hst.getCallStatus().equalsIgnoreCase("NA") || hst.getAudoiFilePath()==null || hst.getAudoiFilePath().equalsIgnoreCase("N/A") || hst.getAudoiFilePath().equalsIgnoreCase("") || hst.getAudoiFilePath().equalsIgnoreCase("NA") || hst.getCallDuration()==0)
				{/*bonrix123_7284045500_2017-10-07_13-17-09_4829.3gp*/
					System.out.println("In Un-Complet Follow Up");
					File Addiofile=new File(filePath + fileName);
					Encoder encoder = new Encoder();
					MultimediaInfo minfo;
					minfo = encoder.getInfo(Addiofile);
					System.out.println(fileName+"    Call Durations :: "+ minfo.getDuration()/1000);
					long due=minfo.getDuration();
					int cdue=(int)(long)due/1000;
					int updateRow=adminService.updateFollowUpByAudioFile(Integer.parseInt(fileData[4].substring(0,fileData[4].indexOf("."))),fileName,cdue);
					System.out.println("AUDIO UPDATED ROW :: "+updateRow);
				}
				else
				{
					System.out.println("In Complet Follow Up");
				}
				String s = "You have successfully uploaded " + filePath;
				;
				return "[{\"responseCode\":\"You have successfully uploaded => " + s + "\"}]";
				// return "You have successfully uploaded " + fileName;
			} catch (Exception e) {
				return "[{\"responseCode\":\"You failed to upload\"}]";
				// "You failed to upload " + fileName + ": " + e.getMessage();
			}
		} else {
			return "[{\"responseCode\":\"Unable to upload. File is empty.\"}]";
		}
	}

	// http://localhost:8191/spring-hib/AddTelecallerToConnectedList?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "AddTelecallerToConnectedList", produces = "application/javascript")
	public @ResponseBody String AddTelecallerToConnectedList(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("AddTelecallerToConnectedList is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		System.out.println("tallycallerId : " + tallycallerId);
		ArrayList<String> numbers = new ArrayList<String>();
		String getTelecaller = null;
		System.out.println("==> " + getTelecaller);
		String no = "";
		if (api != null) {

			no = RandomStringUtils.randomNumeric(5);

			System.out.println("Size : " + map.getClientSize());
			WebSocketObj webSocketLogTemp = new WebSocketObj();
			webSocketLogTemp.setTcalletId(tallycallerId);
			webSocketLogTemp.setClientCode("" + no);
			webSocketLogTemp.setSkt(null);
			webSocketLogTemp.setStatus(false);
			map.AddClient("" + Integer.parseInt(tallycallerId), webSocketLogTemp);
			System.out.println("====> " + map.getClient(tallycallerId));
			CRMHashMap.getInstance().otpauth.put(Integer.parseInt(no), Integer.parseInt(tallycallerId));

			// obj.set
			// if(map.getClientSize()==10)
			// {
			// if(getentry==null)
			/// // {
			// for(Map.Entry<String, ArrayList<String>> entry :
			/// CRMHashMap.getInstance().connectedClient.entrySet())
			// {
			// numbers.add(entry.getKey());
			// System.out.println(entry.getKey()+" "+entry.getValue());

			// }

			/*
			 * no=rand.nextInt(100000); if(numbers.contains(no))
			 * no=rand.nextInt(100000);
			 * 
			 * webSocketLogTemp.add(tallycallerId); webSocketLogTemp.add(""+no);
			 * webSocketLogTemp.add("N/A");
			 * webSocketLogTemp.add("Disconnected");
			 */
			// webSocketLog.put(Integer.parseInt(tallycallerId),webSocketLogTemp);
			// map.AddClient(""+Integer.parseInt(tallycallerId),
			// webSocketLogTemp);
			// System.out.println(map.getClient(tallycallerId));
			System.out.println("======================");
			// for(Map.Entry<String, ArrayList<String>> entry :
			// CRMHashMap.getInstance().connectedClient.entrySet())
			// {
			// numbers.add(entry.getKey());
			// System.out.println(entry.getKey()+" "+entry.getValue());

			// }
			// }
			// else

			// {
			/*
			 * webSocketLogTemp.add(tallycallerId);
			 * webSocketLogTemp.add(getentry.get(1));
			 * webSocketLogTemp.add(getentry.get(2));
			 * webSocketLogTemp.add("Updated");
			 * 
			 * webSocketLog.put(Integer.parseInt(tallycallerId),webSocketLogTemp
			 * ); map.AddClient(""+Integer.parseInt(tallycallerId),
			 * webSocketLogTemp); for(Map.Entry<String, ArrayList<String>> entry
			 * : CRMHashMap.getInstance().connectedClient.entrySet()) { //
			 * numbers.add(entry.getKey());
			 * System.out.println(entry.getKey()+"  "+entry.getValue());
			 * 
			 * }
			 */
			// }
		} else {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "([{\"responceCode\":\"" + "" + no + "\"}]);";
			} else {
				return "[{\"responceCode\":\"No Data Found\"}]";
			}
		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responceCode\":\"" + no + "\"}]);";
		} else {
			return "[{\"responceCode\":\"Data Successfully Added.\"}]";
		}

	}

	// http://localhost:8191/spring-hib/getEntryFromTelecallerList?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getEntryFromTelecallerList", produces = "application/javascript")
	public @ResponseBody String getEntryFromTelecallerList(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getEntryFromTelecallerList is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		int no = 0;
		if (api != null) {
			WebSocketObj getentry = map.getClient(tallycallerId);
			JSONObject leadmap = new JSONObject();
			if (getentry != null) {
				String obj = "N/A";
				leadmap.put("responseCode", "List");
				leadmap.put("telecalerId", getentry.getTcalletId());
				leadmap.put("UniqueId", getentry.clientCode);
				leadmap.put("SessionObj", getentry.getSkt());
				jarray.add(leadmap);

				System.out.println(jarray.toJSONString());

			} else {
				// return "[{\"responceCode\":\"No Data Found\"}]";
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

			}

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/getEntryFromTelecallerList?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "removeEntryFromTelecallerList", produces = "application/javascript")
	public @ResponseBody String removeEntryFromTelecallerList(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("removeEntryFromTelecallerList is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		ArrayList<String> numbers = new ArrayList<String>();

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		int no = 0;
		if (api != null) {
			for (Map.Entry<String, WebSocketObj> entry : CRMHashMap.getInstance().connectedClient.entrySet()) {
				WebSocketObj wskt = entry.getValue();
				String tcId = wskt.getTcalletId();
				System.out.println("Hello : " + tallycallerId + "  " + wskt.tcalletId.toString());
				if (tallycallerId.equalsIgnoreCase(wskt.tcalletId.toString())) {

					WebSocket skt = wskt.getSkt();
					System.out.println("Sending Mobile Number Packet For Call End....  : " + skt);
					String msg = "Hello Sajna";
					// skt1.sendMessage("Hello Sajna","MOBILE_NO_SEND");
					try {
						skt.send(
								"{\"Type\":\"Logout_Request\",\"Status\":\"Logout Request is Sended.\",\"TelecallerId\":\""
										+ wskt.tcalletId.toString() + "\"}");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("Socket Error : " + e.getMessage());
						WebSocketObj getentry = map.RemoveClient(tallycallerId);
						AndroidResponceHashMap.getInstance().RemoveClient(tallycallerId);
						// e.printStackTrace();
					}

				}

			}

			// WebSocketObj getentry =map.RemoveClient(tallycallerId);
			// androidMap.RemoveClient(tallycallerId);
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responceCode\":\"Telecaller removed Successfully.\"}]);";
		} else {
			return jarray.toJSONString();
		}

	}
	
		
}

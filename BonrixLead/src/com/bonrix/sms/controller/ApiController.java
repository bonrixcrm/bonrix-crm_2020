package com.bonrix.sms.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.zip.GZIPOutputStream;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
//import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.config.Task;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bonrix.sms.config.SecurityConfig;
import com.bonrix.sms.config.core.ApplicationContextHolder;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.AutoDialLog;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.ExtraFields;
import com.bonrix.sms.model.FileNameJSON;
import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.MobileCallLog;
import com.bonrix.sms.model.Sentsmslog;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.WebSocketObj;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.bonrix.sms.utils.AndroidResponceHashMap;
import com.bonrix.sms.utils.CRMHashMap;
import com.bonrix.sms.utils.CRMSendSMS;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

@Controller
@MultipartConfig
public class ApiController {
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	SMSService smsService;

	@Autowired
	CommonService cservice;

	final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMddHHmm");
	final SimpleDateFormat df4 = new SimpleDateFormat("yyyyMMddHHmmss");
	final SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	final SimpleDateFormat datef = new SimpleDateFormat("yyyy-MM-dd");
	static Random rand = new Random();

	// static CRMHashMap map = new CRMHashMap();
	private static Logger LOG = LoggerFactory.getLogger("SMPPINFO");
	static HashMap<Integer, ArrayList<String>> webSocketLog = new HashMap<Integer, ArrayList<String>>();
	static AndroidResponceHashMap androidMap = new AndroidResponceHashMap();
	ConsoleHandler consoleHandler = new ConsoleHandler();

	String httpResponse = "";

	@RequestMapping(value = "BonrixLeadApi", produces = "application/javascript")
	public @ResponseBody String BonrixLeadApi(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "fname", required = false, defaultValue = "NA") String fname,
			@RequestParam(value = "lname", required = false, defaultValue = "NA") String lname,
			@RequestParam(value = "emaid", required = false, defaultValue = "NA") String emaid,
			@RequestParam(value = "mobile") String mobile, @RequestParam(value = "categoryid") String categoryid,
			@RequestParam(value = "schedule", required = false, defaultValue = "19800630173600") String schedule,
			@RequestParam(value = "company", required = false, defaultValue = "NA") String company,
			@RequestParam(value = "weurl", required = false, defaultValue = "NA") String weurl,
			@RequestParam(value = "telecallerusername") String telecallerusername,
			@RequestParam(value = "country", required = false, defaultValue = "NA") String country,
			@RequestParam(value = "state", required = false, defaultValue = "NA") String state,
			@RequestParam(value = "city", required = false, defaultValue = "NA") String city,
			@RequestParam(value = "admin") String admin) throws Exception {
//BonrixLeadApi?mobile=<>&categoryid=557/6&admin=suryodaya.in@gmail.com
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(telecallerusername);
		User user = userService.findByUserName(admin);
		if (tclaer == null)
			return "[{\"responseCode\":\"Invalid Telecaller.\"}]";
		else if (user == null)
			return "[{\"responseCode\":\"Invalid User.\"}]";
		else {
			int catSize = cservice.createSqlQuery(
					"SELECT categorymanager.categotyId FROM categorymanager WHERE categorymanager.categotyId="
							+ Integer.parseInt(categoryid) + " AND categorymanager.companyId=" + user.getUid() + "")
					.size();
			if (catSize == 0)
				return "[{\"responseCode\":\"Invalid Category.\"}]";

			int no = adminService.validateNoCategory(mobile, Integer.parseInt(categoryid));
			System.out.println("NUMBET -| " + no);
			if (no != 0)
				return "[{\"responseCode\":\"Lead is Already Available in Selected Category..\"}]";

			Lead lead = new Lead();
			lead.setFirstName(fname);
			lead.setLastName(lname);
			lead.setemail(emaid);
			lead.setMobileNo(mobile);
			lead.setCategoryId(categoryid);
			lead.setSheduleDate(df4.parse(schedule));// 20181215101010
			lead.setCompanyName(company);
			lead.setWebsite(weurl);
			lead.setTallyCalletId(tclaer.getTcallerid());
			lead.setCountry(country);
			lead.setCity(city);
			lead.setState(state);
			lead.setCompanyId((int) user.getUid());
			lead.setCreateDate(new Date());
			lead.setLeadProcessStatus("None");
			lead.setLeadState("Open");
			lead.setCsvData("N/A");
			lead.setLeadType("API");
			lead.setNotification_flag("FALSE");
			lead.setAssignDate(new Date());
			lead.setAutoDial(true);
			adminService.saveObject(lead);
			return "[{\"responseCode\":\"Lead Successfully Saved.\"}]";
		}
	}

	@RequestMapping(value = "addsalesroboLead", produces = "application/javascript")
	public @ResponseBody String addsalesroboLead(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "fname", required = false, defaultValue = "NA") String fname,
			@RequestParam(value = "lname", required = false, defaultValue = "NA") String lname,
			@RequestParam(value = "emaid", required = false, defaultValue = "NA") String emaid,
			@RequestParam(value = "mobile") String mobile, @RequestParam(value = "categoryid") String categoryid,
			@RequestParam(value = "schedule") String schedule,
			@RequestParam(value = "company", required = false, defaultValue = "NA") String company,
			@RequestParam(value = "weurl", required = false, defaultValue = "NA") String weurl,
			@RequestParam(value = "telecallerusername") String telecallerusername,
			@RequestParam(value = "country", required = false, defaultValue = "NA") String country,
			@RequestParam(value = "state", required = false, defaultValue = "NA") String state,
			@RequestParam(value = "city", required = false, defaultValue = "NA") String city,
			@RequestParam(value = "admin") String admin) throws Exception {

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(telecallerusername);
		User user = userService.findByUserName(admin);
		if (tclaer == null)
			return "[{\"responseCode\":\"Invalid Telecaller.\"}]";
		else if (user == null)
			return "[{\"responseCode\":\"Invalid User.\"}]";
		else {
			int catSize = cservice.createSqlQuery(
					"SELECT categorymanager.categotyId FROM categorymanager WHERE categorymanager.categotyId="
							+ Integer.parseInt(categoryid) + " AND categorymanager.companyId=" + user.getUid() + "")
					.size();
			if (catSize == 0)
				return "[{\"responseCode\":\"Invalid Category.\"}]";

			int no = adminService.validateNoCategory(mobile, Integer.parseInt(categoryid));
			System.out.println("NUMBET -| " + no);
			if (no != 0)
				return "[{\"responseCode\":\"Lead is Already Available in Selected Category..\"}]";

			Lead lead = new Lead();
			lead.setFirstName(fname);
			lead.setLastName(lname);
			lead.setemail(emaid);
			lead.setMobileNo(mobile);
			lead.setCategoryId(categoryid);
			lead.setSheduleDate(df4.parse(schedule));// 20181215101010
			lead.setCompanyName(company);
			lead.setWebsite(weurl);
			lead.setTallyCalletId(tclaer.getTcallerid());
			lead.setCountry(country);
			lead.setCity(city);
			lead.setState(state);
			lead.setCompanyId((int) user.getUid());
			lead.setCreateDate(new Date());
			lead.setLeadProcessStatus("None");
			lead.setLeadState("Open");
			lead.setCsvData("N/A");
			lead.setLeadType("API");
			lead.setNotification_flag("FALSE");
			lead.setAssignDate(new Date());
			adminService.saveObject(lead);
			return "[{\"responseCode\":\"Lead Successfully Saved.\"}]";
		}
	}

	@RequestMapping(value = "SingleCatAutoDial", produces = "application/javascript")
	public @ResponseBody String hello(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerId") int tallycallerId, @RequestParam(value = "leadId") String lIds,
			@RequestParam(value = "apikey") String api) throws Exception {

		System.out.println("hello is Called");
		ApiKey apiKey = null;

		apiKey = userService.gettAPiByUid(tallycallerId, api, true);
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tallycallerId, api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			cservice.createupdateQuery("UPDATE lead SET lead.autoDial=1 WHERE lead.leaadId IN (" + lIds + ")");
		}
		return "[{\"responseCode\":\"Leads Successfully Added To AutoDial\"}]";

	}

	@RequestMapping(value = "AutoDialLog", produces = "application/javascript")
	public @ResponseBody String AutoDialLog(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerName") String tallycallerName, @RequestParam(value = "apikey") String api,
			@RequestParam(value = "responseId") int responseId) throws Exception {

		System.out.println("AutoDialLog is Called");
		ApiKey apiKey = null;

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		int logId = 0;
		if (apiKey != null) {
			if (responseId == 0) {
				AutoDialLog log = new AutoDialLog();
				log.setEnd_date(null);
				log.setStart_date(new Date());
				log.setTc_id(tclaer.getTcallerid());
				log.setComp_id(tclaer.getCompanyId());
				logId = cservice.saveRetObject(log);
			} else {
				AutoDialLog log = new AutoDialLog();
				log = adminService.GetAutoDial(responseId);
				log.setEnd_date(new Date());
				logId = cservice.updateObject(log);
			}
		}

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseId\":\"" + logId + "\"}])";
		else
			return "[{\"responseId\":\"" + logId + "\"}]";
	}

	@RequestMapping(value = "getScheduleWiseLead", produces = "application/javascript")
	public @ResponseBody String getScheduleWiseLead(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerName") String tallycallerName, @RequestParam(value = "apikey") String api)
			throws Exception {

		System.out.println("getScheduleWiseLead is Called");
		ApiKey apiKey = null;

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		String json = null;
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			Type type = new TypeToken<List<Task>>() {
			}.getType();
			json = new GsonBuilder().serializeNulls().create()
					.toJson(adminService.getScheduleWiseLead(tclaer.getTcallerid()), type);
			System.out.println(json);
		}
		return json;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getEmailLogByTelecaller", produces = "application/javascript")
	public @ResponseBody String getEmailLogByTelecaller(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		System.out.println("*******getEmailLogByTelecaller********");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;

		if (apiKey != null) {

			int i = 0;
			String listData = "";
			String query = "";
			String queryCount = "";
			String stdate = request.getParameter("stdate");
			String endate = request.getParameter("endate");
			// System.out.println("stdate " + stdate);
			// System.out.println("endate " + endate);
			System.out.println("***************");

			if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && name.equalsIgnoreCase("NA")
					&& mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")
					&& subject.equalsIgnoreCase("NA")) {
				queryCount = "SELECT  COUNT(*) FROM lead "
						+ "  JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE lead.tallyCalletId="
						+ tclaer.getTcallerid() + " AND emaillog.telecallet_id=" + tclaer.getTcallerid() + "";

				query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status,emaillog.attached_file,emaillog.mail_body"
						+ " FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE lead.tallyCalletId="
						+ tclaer.getTcallerid() + " AND emaillog.telecallet_id=" + tclaer.getTcallerid()
						+ " ORDER BY emaillog.id DESC";
			} else if (!stdate.equalsIgnoreCase("NA") && !endate.equalsIgnoreCase("NA") && !name.equalsIgnoreCase("NA")
					&& !mobileNo.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("NA")
					&& !subject.equalsIgnoreCase("NA")) {
				queryCount = "SELECT  COUNT(*) "
						+ "FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.telecallet_id="
						+ tclaer.getTcallerid() + " " + "  AND (lead.firstName LIKE '%" + name
						+ "%' OR lead.lastName LIKE '%" + name + "%' OR lead.mobileNo LIKE '%" + mobileNo
						+ "%' OR lead.email LIKE '%" + email + "%' OR emaillog.subject LIKE '%" + subject + "%')"
						+ "  AND (emaillog.send_date BETWEEN '" + stdate + "' AND ADDDATE('" + endate
						+ "', INTERVAL 1 DAY))";

				query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status,"
						+ " emaillog.attached_file,emaillog.mail_body FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.telecallet_id="
						+ tclaer.getTcallerid() + " " + "  AND (lead.firstName LIKE '%" + name
						+ "%' OR lead.lastName LIKE '%" + name + "%' OR lead.mobileNo LIKE '%" + mobileNo
						+ "%' OR lead.email LIKE '%" + email + "%' OR emaillog.subject LIKE '%" + subject + "%')"
						+ "  AND (emaillog.send_date BETWEEN '" + stdate + "' AND ADDDATE('" + endate
						+ "', INTERVAL 1 DAY)) ORDER BY emaillog.id DESC";
			} else if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && !name.equalsIgnoreCase("NA")
					&& !mobileNo.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("NA")
					&& !subject.equalsIgnoreCase("NA")) {
				queryCount = "SELECT  COUNT(*) "
						+ "FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.telecallet_id="
						+ tclaer.getTcallerid() + " " + "  AND (lead.firstName LIKE '%" + name
						+ "%' OR lead.lastName LIKE '%" + name + "%' OR lead.mobileNo LIKE '%" + mobileNo
						+ "%' OR lead.email LIKE '%" + email + "%' OR emaillog.subject LIKE '%" + subject + "%')";

				query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status,"
						+ " emaillog.attached_file,emaillog.mail_body FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE  emaillog.telecallet_id="
						+ tclaer.getTcallerid() + " " + "  AND (lead.firstName LIKE '%" + name
						+ "%' OR lead.lastName LIKE '%" + name + "%' OR lead.mobileNo LIKE '%" + mobileNo
						+ "%' OR lead.email LIKE '%" + email + "%' OR emaillog.subject LIKE '%" + subject
						+ "%') ORDER BY emaillog.id DESC" + "  ";
			} else {
				queryCount = "SELECT  COUNT(*) "
						+ " FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE emaillog.telecallet_id=3 "
						+ "  AND (emaillog.send_date BETWEEN '" + stdate + "' AND ADDDATE('" + endate
						+ "', INTERVAL 1 DAY))";

				query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS nam,lead.mobileNo,lead.email,emaillog.subject,emaillog.send_date,emaillog.status,"
						+ " emaillog.attached_file,emaillog.mail_body FROM lead INNER JOIN emaillog ON lead.leaadId=emaillog.lead_id WHERE emaillog.telecallet_id="
						+ tclaer.getTcallerid() + " " + "  AND (emaillog.send_date BETWEEN '" + stdate
						+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) ORDER BY emaillog.id DESC";
			}

			// System.out.println("" + queryCount);
			// System.out.println("" + query);
			int page = 1;
			int listSize = 10;
			int start = 0;
			int len = 0;
			String mob = "";
			Date fdate = null;
			Date todate = null;
			try {

				if (request.getParameter("start") != null) {
					start = Integer.parseInt(request.getParameter("start"));
				}

				else {// System.out.println("STSRT : "+start);
				}

				if (request.getParameter("length") != null) {
					len = Integer.parseInt(request.getParameter("length"));
					listSize = len;
				} else {
					// System.out.println("LENGTH : " + len);
				}
				page = start / len + 1;
			} catch (Exception ex) {

			}
			listData = adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
			if (listData.length() != 0) {

				if (request.getParameter("callback") != null) {
					// System.out.println(request.getParameter("callback") + "("
					// + listData + ");");
					return request.getParameter("callback") + "(" + listData + ");";
				} else {
					// System.out.println("IN LIST");
					return listData;
				}
			} else {
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
			}
		}
		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
	}

	@RequestMapping(value = "getLeadCountByMobileNo", produces = "application/javascript")
	public @ResponseBody String getLeadCountByMobileNo(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "mobileNo") String mobileNo,
			@RequestParam(value = "tallycallerName") String tallycallerName, @RequestParam(value = "apikey") String api)
			throws Exception {

		System.out.println("getLeadCountByMobileNo is Called");
		ApiKey apiKey = null;
		int leadCount = 0;
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		String rerurnData = "[{\"Status\":\"False\",\"Message\":\"\",\"Data\":[{\"response\":\"Error In Data\"}]}]";

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		// System.out.println(tallycallerName + " " + tclaer.getUsername());
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			leadCount = adminService.getLeadCountByMobileNo(mobileNo, tclaer.getCompanyId());

			rerurnData = "[{\"Status\":\"True\",\"Message\":\"\",\"LeadCount\":\"" + leadCount + "\"}]";

		}
		return rerurnData;

	}

	@RequestMapping(value = "AddMobileCallLogAPI", produces = "application/javascript")
	public @ResponseBody String AddMobileCallLogAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "mobileNo") String mobileNo, @RequestParam(value = "callTime") String callTime,
			@RequestParam(value = "audioFile") String audioFile, @RequestParam(value = "callDuration") int callDuration,
			@RequestParam(value = "name") String name, @RequestParam(value = "callStatus") String callStatus,
			@RequestParam(value = "tallycallerName") String tallycallerName, @RequestParam(value = "apikey") String api)
			throws Exception {

		// System.out.println("AddMobileCallLogAPI is Called");
		ApiKey apiKey = null;
		int leadCount = 0;
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		String rerurnData = "[{\"Status\":\"False\",\"Message\":\"\",\"Data\":[{\"response\":\"Error In Data\"}]}]";

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		// System.out.println(tallycallerName + " " + tclaer.getUsername());
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			leadCount = adminService.getLeadCountByMobileNo(mobileNo, tclaer.getCompanyId());
			MobileCallLog log = new MobileCallLog();
			log.setAudioFile(audioFile);
			log.setCallDuration(callDuration);
			log.setCallStatus(callStatus);
			log.setCompanyId(tclaer.getCompanyId());
			log.setCallTime(df.parse(callTime));
			log.setLeadCount(leadCount);
			log.setMobileNo(mobileNo);
			log.setNane(name);
			log.setEntryDate(new Date());
			log.setTelecallerId(tclaer.getTcallerid());
			adminService.saveMobileCallLog(log);
			rerurnData = "[{\"Status\":\"True\",\"Message\":\"\",\"Data\":[{\"response\":\"Log Successfully Saved.\",\"LeadCount\":\""
					+ leadCount + "\"}]}]";

		}
		return rerurnData;

	}

	// http://localhost:8191/spring-hib/getLeadBymobileNoAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getLeadBymobileNoAPI", produces = "application/javascript")
	public @ResponseBody String getLeadBymobileNoAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String mobNo = request.getParameter("mobNo");
		String api = request.getParameter("apikey");
		String tallycallerName = request.getParameter("tallycallerName");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		System.out.println("getLeadByCompanyIdNDmobileNo is called...");

		ApiKey apiKey = null;
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		// System.out.println(tallycallerName + " " + tclaer.getUsername());
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		Object[] result = null;
		if (apiKey != null) {
			List list = adminService.getLeadByCompanyIdNDmobileNo(mobNo, (int) (long) tclaer.getCompanyId());
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();

					result = (Object[]) list.get(i);
					/*
					 * System.out.println("My : "+result[0]+" "+result[1]+" +"result[2]"+
					 * "+result[3]+" "+result[4]"+ "+result[5] +"result[6]"+ "+result[7]"+
					 * "+result[8]"+"result[9] +"result[10]"+ "+result[11]"+ "+result[12]);
					 */
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					jarray.add(leadmap);

				}

			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";
			}

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/GetAndroidContactDetailsAPI?mobileno=9173186596&apikey=XXXXXX
	@RequestMapping(value = "GetAndroidContactDetailsAPI")
	public @ResponseBody String GetAndroidContactDetailsAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String mobile = request.getParameter("mobileno");
		String api = request.getParameter("apikey");
		String tallycallerName = request.getParameter("tallycallerName");
		System.out.println("GetAndroidContactDetailsAPI is Called");
		/*
		 * Tallycaller telecaller = new Tallycaller();
		 * telecaller=adminService.validateTelecallerDesktopAPI(api);
		 */
		// System.out.println(telecaller);
		String name = null, category = "";
		ApiKey apiKey = null;
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		// System.out.println(tallycallerName + " " + tclaer.getUsername());
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			List list = null;
			Object[] result = null;

			list = adminService.GetContactDetaails(tclaer.getCompanyId(), mobile);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {

					result = (Object[]) list.get(i);
					name = (String) result[0];
					category += (String) result[1] + ",";
					// System.out.println(result[0] + " " + result[1]);
				}
				// System.out.println(name + "(" + category + ")");
			} else
				return "No Lead Found";
		}
		// return name+"("+category+")";
		return "[{\"IncommingCallInfo\":\"" + name + "(" + category + ")\"}]";

	}

	// http://localhost:8191/spring-hib/uploadAudioFIleAPI
	@RequestMapping(value = "uploadDesktopAudioFiles", method = RequestMethod.POST)
	public @ResponseBody String uploadDesktopAudioFiles(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("file") MultipartFile file,
			@RequestParam(value = "tallycallerName", required = false) String tallycallerName) throws Exception {

		System.out.println("uploadAudioFIleAPI is Called..");
		LOG.info("Hello API ................");
		System.out.println("FIle Path : " + file);
		String fileName = null;
		String filePath = null;
		Tallycaller caller = adminService.GetTellyCallerByName(tallycallerName);
		User user = null;
		if (caller != null) {
			user = adminService.getComapnyByTeclId(caller.getCompanyId());
		}
		if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				System.out.println("FILE NAME :: " + file.getOriginalFilename());
				filePath = request.getServletContext().getRealPath("//");
				filePath = filePath + SEP + "Sound" + SEP;
				filePath = filePath.concat(Long.toString(caller.getCompanyId())) + SEP;
				System.out.println(filePath);
				File directory = new File(filePath);
				if (!directory.exists()) {
					directory.mkdir();
				}

				BufferedOutputStream buffStream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath + fileName)));

				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				String s = "You have successfully uploaded " + filePath;
				;
				return "[{\"responseCode\":\"You have successfully uploaded => " + s + "\"}]";
				// return "You have successfully uploaded " + fileName;
			} catch (Exception e) {
				return "[{\"responseCode\":\"You failed to upload\"}]";
				// "You failed to upload " + fileName + ": " + e.getMessage();
			}
		} else {
			return "[{\"responseCode\":\"Unable to upload. File is empty.\"}]";
		}

	}

	// http://localhost:8191/spring-hib/uploadAudioFIleAPI
	@RequestMapping(value = "uploadCRMAndroidFile", method = RequestMethod.POST)
	public @ResponseBody String uploadCRMAndroidFile(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("file") MultipartFile file, @RequestParam("tellcallerid") String teleId) throws Exception {

		System.out.println("uploadAudioFIleAPI is Called..");
		LOG.info("Hello API ................");
		System.out.println("FIle Path : " + file);
		String fileName = null;
		String filePath = null;
		String userName = null;
		Tallycaller caller = adminService.getTallycallerByIdMdel(Integer.parseInt(teleId));

		User user = null;
		if (caller != null) {
			user = adminService.getComapnyByTeclId(caller.getCompanyId());
			// System.out.println("USER_NAME :: "+user.getUid());
		}
		if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				System.out.println("FILE NAME :: " + file.getOriginalFilename());
				filePath = request.getServletContext().getRealPath("//");
				filePath = filePath + SEP + "Sound" + SEP;
				filePath = filePath.concat(Long.toString(user.getUid())) + SEP;
				System.out.println(filePath);
				File directory = new File(filePath);
				if (!directory.exists()) {
					directory.mkdir();
				}

				BufferedOutputStream buffStream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath + fileName)));

				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				String s = "You have successfully uploaded " + filePath;
				;
				return "[{\"responseCode\":\"You have successfully uploaded => " + s + "\"}]";
				// return "You have successfully uploaded " + fileName;
			} catch (Exception e) {
				return "[{\"responseCode\":\"You failed to upload\"}]";
				// "You failed to upload " + fileName + ": " + e.getMessage();
			}
		} else {
			return "[{\"responseCode\":\"Unable to upload. File is empty.\"}]";
		}

	}

	// http://localhost:8191/spring-hib/uploadAudioFIleAPI
	@RequestMapping(value = "uploadAttachment", method = RequestMethod.POST, produces = "application/javascript")
	public @ResponseBody String uploadAttachment(HttpServletRequest request, HttpServletResponse res,
			@RequestParam("file") MultipartFile file) throws Exception {
		System.out.println("uploadAttachment is Called..");
		System.out.println("FIle Path : " + file);
		String fileName = null;
		String filePath = null;
		if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HHmmssSSS");
				fileName = sdf.format(new Date()) + "_" + file.getOriginalFilename().replaceAll(" ", "");
				System.out.println(sdf.format(new Date()));
				byte[] bytes = file.getBytes();

				filePath = request.getServletContext().getRealPath("/");
				filePath = filePath + SEP + "Attachments" + SEP;
				BufferedOutputStream buffStream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath + fileName)));

				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				String s = "You have successfully uploaded " + filePath;

				res.setHeader("Access-Control-Allow-Origin", "*");
				res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
				res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
				res.setHeader("Access-Control-Allow-Credentials", "true");
				res.setHeader("Content-Type", "application/json");

				return "[{\"responseCode\":\"" + fileName + "\"}]";
			} catch (Exception e) {
				return "[{\"responseCode\":\"You failed to upload\"}]";
			}
		} else {
			return "[{\"responseCode\":\"Unable to upload. File is empty.\"}]";
		}
	}

	@RequestMapping(value = "UpdateNotificationSatus", produces = "application/javascript")
	public @ResponseBody String UpdateNotificationSatus(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("UpdateNotificationSatus is Called..");
		int notiId = Integer.parseInt(request.getParameter("leadId"));
		String api = request.getParameter("apikey");
		String tallycallerName = request.getParameter("tallycallerName");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {
			adminService.UpdateNotificationSatus(notiId);
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "[{\"responseCode\":\"Success\"}]";
		else
			return "Success";
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getFaicallLogWEBAPI", produces = "application/javascript")
	public @ResponseBody String getFaicallLogWEBAPI(HttpServletRequest request, HttpServletResponse response)throws Exception {

		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		String failstate = request.getParameter("failstate");
		// String catId = request.getParameter("catId");
		// String tallycallerId = request.getParameter("tallycallerId");
		String logDate = request.getParameter("logDate");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		System.out.println("***************");
		System.out.println("name :: " + name);
		System.out.println("mobileNo :: " + mobileNo);
		System.out.println("email :: " + email);
		System.out.println("tallycallerName :: " + tallycallerName);
		System.out.println("failstate :: " + failstate);
		System.out.println("api :: " + api);
		System.out.println("logDate :: " + logDate);

		System.out.println("*************** :: " + logDate);
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		String list = "";
		if (apiKey != null) {
			if (failstate.equalsIgnoreCase("NA")) {
				StringBuilder queryCount = new StringBuilder();
				queryCount.append("SELECT count(*) " + " FROM lead INNER JOIN categorymanager "
						+ " ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory "
						+ " ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
						+ tclaer.getUsername() + "'  AND lead.companyId=" + tclaer.getCompanyId() + "  "
						+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
						+ " AND leadState='Open'  AND (followupshistory.failstatus IS NOT NULL  )  AND DATE(followupshistory.`callingTime`)='"
						+ logDate + "'");

				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,"
								+ "	 followupshistory.callingTime,lead.leadState, lead.autoDial,lead.createDate,followupshistory.newStatus,followupshistory.sheduleTime FROM lead INNER JOIN categorymanager "
								+ "	ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory "
								+ "	ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
								+ tclaer.getUsername() + "'  AND lead.companyId=tallycaller.`companyId`    "
								+ "	AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
								+ "	AND leadState='Open'  AND (followupshistory.failstatus IS NOT NULL ) AND DATE(followupshistory.`callingTime`)='"
								+ logDate + "' ");

				System.out.println("" + query);

				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
					}

					else {
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						listSize = len;
					} else {
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}

				list = adminService.ConvertDataToWebJSON("" + queryCount, "" + query, page, listSize);
				if (list.length() != 0) {

					if (request.getParameter("callback") != null) {
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						return list;
					}

				} else {
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}

			} else if ((!failstate.equalsIgnoreCase("N/A") || !failstate.equalsIgnoreCase("NA"))) {
				System.out.println("In ELSE IF SAJAN...");
				StringBuilder queryCount = new StringBuilder();
				queryCount.append("SELECT count(*) FROM lead INNER JOIN categorymanager "
						+ " ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory"
						+ " ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
						+ tclaer.getUsername() + "'  AND lead.companyId=" + tclaer.getCompanyId() + "  "
						+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
						+ " AND leadState='Open'   ");
				StringBuilder query = new StringBuilder();

				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,"
								+ " lead.createDate,lead.leadState, lead.autoDial,followupshistory.callingTime,followupshistory.newStatus FROM lead INNER JOIN categorymanager "
								+ " ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory"
								+ " ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
								+ tclaer.getUsername() + "'  AND lead.companyId=" + tclaer.getCompanyId() + "  "
								+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
								+ " AND leadState='Open'  AND   ");
				query.append("  followupshistory.`failstatus`='" + failstate
						+ "' AND DATE(followupshistory.`callingTime`)='" + logDate + "' ");
				queryCount.append(" AND followupshistory.`failstatus`='" + failstate
						+ "' AND DATE(followupshistory.`callingTime`)='" + logDate + "' ");
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
					}

					else {
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						listSize = len;
					} else {
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}

				list = adminService.ConvertDataToWebJSON("" + queryCount, "" + query, page, listSize);
				if (list.length() != 0) {

					if (request.getParameter("callback") != null) {
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						return list;
					}

				} else {
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				}
			}

		}
		return request.getParameter("callback") + "(" + jarray.toJSONString() + ")";
	}

	// http://localhost:8191/spring-hib/GetNotiSheduleLeads?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "GetNotiSheduleLeads", produces = "application/javascript")
	public @ResponseBody String GetNotiSheduleLeads(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String leadIds = request.getParameter("leadIds").substring(0, request.getParameter("leadIds").length() - 1);
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		// String tallycallerId = request.getParameter("tallycallerId");
		System.out.println("***************");
		System.out.println(" GetNotiSheduleLeads " + leadIds);
		System.out.println("***************");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;

		if (apiKey != null) {
			String list = "";
			String query = "SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,lead.sheduleDate,lead.autoDial,lead.leadType,date_format(lead.sheduleDate,'%Y-%m-%d %T'),lead.tagName "
					+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
					+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
					+ "    WHERE    0=0 AND lead.leaadId IN(" + leadIds + ") AND leadState='Open' ";

			// System.out.println("" + query);

			String queryCount = "SELECT count(*) from lead " + "   WHERE  lead.leaadId IN(" + leadIds + ")"
					+ "   AND lead.leadState='Open'";
			// List list = adminService.getTelecallerStateLeadCount(""+query);
			int page = 1;
			int listSize = 10;
			int start = 0;
			int len = 0;
			String mob = "";
			Date fdate = null;
			Date todate = null;
			try {

				if (request.getParameter("start") != null) {
					start = Integer.parseInt(request.getParameter("start"));
					// System.out.println("STSRT : " + start);
				}

				else {
					// System.out.println("STSRT : " + start);
				}

				if (request.getParameter("length") != null) {
					len = Integer.parseInt(request.getParameter("length"));
					// System.out.println("LENGTH : " + len);
					listSize = len;
				} else {
					// System.out.println("LENGTH : " + len);
				}
				page = start / len + 1;
			} catch (Exception ex) {

			}
			list = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
			if (list.length() != 0) {

				if (request.getParameter("callback") != null) {
					// System.out.println(request.getParameter("callback") + "("
					// + list + ");");
					return request.getParameter("callback") + "(" + list + ");";
				} else {
					// System.out.println("IN LIST");
					return list;
				}

			} else {
				// return "[{\"responceCode\":\"No Data Found\"}]";
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

			}

		}
		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getNotification", produces = "application/javascript")
	public @ResponseBody String getNotification(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getNotification is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {

			List list = adminService.getNotification(tclaer.getTcallerid());
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("time", df.parse(result[0].toString()));
					leadmap.put("notification", result[1]);
					leadmap.put("id", result[2]);
					jarray.add(leadmap);
				}
			}
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		else
			return jarray.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "addNotification", produces = "application/javascript")
	public @ResponseBody String addNotification(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addNotification is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String html_body = request.getParameter("html_body");
		String api = request.getParameter("apikey");
		// System.out.println("html_body : " + html_body);
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {

			adminService.addNotification(tclaer.getTcallerid(), html_body);
			// adminService.addNotification(html_body,tclaer.getTcallerid())
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Tag Successfully Added To Lead.\"}]);";
		else
			return "[{\"responceCode\":\"Tag Successfully Added To Lead.\"}]";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "updateNotificationFlag", produces = "application/javascript")
	public @ResponseBody String updateNotificationFlag(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("updateNotificationFlag is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String html_body = request.getParameter("html_body");
		int leadId = Integer.parseInt(request.getParameter("leadId"));
		String api = request.getParameter("apikey");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {

			adminService.updateNotificationFlag(leadId);
			// adminService.addNotification(html_body,tclaer.getTcallerid())
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Tag Successfully Added To Lead.\"}]);";
		else
			return "[{\"responceCode\":\"Tag Successfully Added To Lead.\"}]";
	}

	@RequestMapping(value = "getNotificationLead", produces = "application/javascript")
	public @ResponseBody String getNotificationLead(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUidForNotification(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		Object[] result = null;

		if (apiKey != null) {
			String list = "";
			String query = "SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,"
					+ " lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate, '%Y-%m-%d %T'),lead.tagName,lead.notification_flag FROM lead "
					+ " INNER JOIN categorymanager ON lead.categoryId = categorymanager.categotyId "
					+ " INNER JOIN tallycaller  ON lead.tallyCalletId = tallycaller.tcallerid WHERE 0 = 0 "
					+ " AND tallycaller.username = '" + tallycallerName
					+ "' AND lead.leadState ='Open' AND DATE(lead.sheduleDate)=DATE(NOW()) AND MINUTE(lead.sheduleDate)=MINUTE(NOW()) AND lead.notification_flag='TRUE' ";

			String queryCount = "SELECT count(*) from lead "
					+ " INNER JOIN categorymanager ON lead.categoryId = categorymanager.categotyId "
					+ " INNER JOIN tallycaller  ON lead.tallyCalletId = tallycaller.tcallerid "
					+ " WHERE 0 = 0  AND tallycaller.username = '" + tallycallerName + "' AND lead.leadState ='Open' "
					+ " AND DATE(lead.sheduleDate)=DATE(NOW()) AND MINUTE(lead.sheduleDate)=MINUTE(NOW()) AND lead.notification_flag='TRUE' "
					+ "ORDER BY lead.leaadId ASC";

			int page = 1;
			int listSize = 10;
			int start = 0;
			int len = 0;
			String mob = "";
			Date fdate = null;
			Date todate = null;
			try {

				if (request.getParameter("start") != null) {
					start = Integer.parseInt(request.getParameter("start"));
					// System.out.println("STSRT : "+start);
				}

				else {
					// System.out.println("STSRT : "+start);
				}

				if (request.getParameter("length") != null) {
					len = Integer.parseInt(request.getParameter("length"));
					// System.out.println("LENGTH : "+len);
					listSize = len;
				} else {
					// System.out.println("LENGTH : "+len);
				}
				page = start / len + 1;
			} catch (Exception ex) {

			}
			list = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
			if (list.length() != 0) {
				if (request.getParameter("callback") != null) {
					return request.getParameter("callback") + "(" + list + ");";
				} else {
					System.out.println("IN LIST");
					return list;
				}

			} else {
				// return "[{\"responceCode\":\"No Data Found\"}]";
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
			}
		}
		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getTelecallerLeadReport", produces = "application/javascript")
	public @ResponseBody String getTelecallerLeadReport(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		String flag = request.getParameter("flag");
		String mobileNo = request.getParameter("mobileNo");
		// String searchStr = request.getParameter("searchStr");
		System.out.println("*******getTelecallerLeadReport********" + mobileNo);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;

		if (apiKey != null) {

			// System.out.println("In Else Condition");
			int i = 0;
			String listData = "";
			String query = "";
			String queryCount = "";

			if (flag.equalsIgnoreCase("AllCallLog")) {
				String stdate = request.getParameter("stdate");
				String endate = request.getParameter("endate");
				// System.out.println("stdate " + stdate);
				// System.out.println("endate " + endate);
				// System.out.println("***************");
				if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA")) {
					queryCount = "SELECT  COUNT(*) FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " ";

					query = "SELECT  CONCAT(tallycaller.firstname,' ',tallycaller.lastname),CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,followupshistory.callDuration,followupshistory.callingTime,"
							+ " followupshistory.callStatus,followupshistory.remark,lead.leadState,followupshistory.successStatus,lead.leadProcessStatus FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " ";
				} else {
					queryCount = "SELECT  COUNT(*) FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " " + "AND (followupshistory.callingTime BETWEEN '" + stdate
							+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) ";

					query = "SELECT  CONCAT(tallycaller.firstname,' ',tallycaller.lastname),CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,followupshistory.callDuration,followupshistory.callingTime,"
							+ " followupshistory.callStatus,followupshistory.remark,lead.leadState,followupshistory.successStatus,lead.leadProcessStatus FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " " + "AND (followupshistory.callingTime BETWEEN '" + stdate
							+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) OR lead.mobileNo LIKE '%" + mobileNo
							+ "%' ";

					/*
					 * if (!mobileNo.equalsIgnoreCase("NA")) { i++; //
					 * query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
					 * query=query+" "+" OR lead.mobileNo LIKE '%" + mobileNo + "%' "; }
					 */

				}

				/*
				 * if(!searchStr.equalsIgnoreCase("NA")) {
				 * queryCount=queryCount+" "+"AND lead.mobileNo="+searchStr+" ";
				 * query=query+" "+"AND lead.mobileNo="+searchStr+" "; }
				 */
			}

			if (flag.equalsIgnoreCase("SuccessFailCallLog")) {
				String stdate = request.getParameter("stdate");
				String endate = request.getParameter("endate");
				String ststus = request.getParameter("ststus");
				// System.out.println("stdate " + stdate);
				// System.out.println("endate " + endate);
				// System.out.println("***************");
				if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA")) {
					queryCount = "SELECT  COUNT(*) FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " AND followupshistory.callStatus='" + ststus + "' ";
					// System.out.println(queryCount);
					query = "SELECT  CONCAT(tallycaller.firstname,' ',tallycaller.lastname),CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,followupshistory.callDuration,followupshistory.callingTime,"
							+ " followupshistory.callStatus,followupshistory.remark,lead.leadState,followupshistory.successStatus,lead.leadProcessStatus FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " AND followupshistory.callStatus='" + ststus + "' ";
				} else {
					queryCount = "SELECT  COUNT(*) FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " " + "AND (followupshistory.callingTime BETWEEN '" + stdate
							+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) AND followupshistory.callStatus='"
							+ ststus + "' ";
					// System.out.println(queryCount);

					query = "SELECT  CONCAT(tallycaller.firstname,' ',tallycaller.lastname),CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,followupshistory.callDuration,followupshistory.callingTime,"
							+ " followupshistory.callStatus,followupshistory.remark,lead.leadstate,followupshistory.successStatus,lead.leadProcessStatus FROM followupshistory "
							+ "  INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid "
							+ "  INNER JOIN lead  ON lead.leaadId= followupshistory.leadId WHERE   followupshistory.tallycallerId="
							+ tclaer.getTcallerid() + " " + "AND (followupshistory.callingTime BETWEEN '" + stdate
							+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) AND followupshistory.callStatus='"
							+ ststus + "' OR lead.mobileNo LIKE '%" + mobileNo + "%' ";
				}
			}

			// System.out.println("" + queryCount);
			// System.out.println("" + query);
			int page = 1;
			int listSize = 10;
			int start = 0;
			int len = 0;
			String mob = "";
			Date fdate = null;
			Date todate = null;
			try {

				if (request.getParameter("start") != null) {
					start = Integer.parseInt(request.getParameter("start"));
					// System.out.println("STSRT : "+start);
				}

				else {// System.out.println("STSRT : "+start);
				}

				if (request.getParameter("length") != null) {
					len = Integer.parseInt(request.getParameter("length"));
					// System.out.println("LENGTH : "+len);
					listSize = len;
				} else {
					// System.out.println("LENGTH : " + len);
				}
				page = start / len + 1;
			} catch (Exception ex) {

			}

			System.out.println("query---->>" + query);
			listData = adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
			if (listData.length() != 0) {
				if (request.getParameter("callback") != null) {
					// System.out.println(request.getParameter("callback") + "("+ listData + ");");
					return request.getParameter("callback") + "(" + listData + ");";
				} else {
					// System.out.println("IN LIST");
					return listData;
				}
			} else {
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
			}
		}
		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
	}

	// http://localhost:8191/spring-hib/getTodayLeads?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "addLeadTagAPI", produces = "application/javascript")
	public @ResponseBody String addLeadTagAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addLeadTagAPI is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String tagList = request.getParameter("tagList");
		int leadId = Integer.parseInt(request.getParameter("leadId"));
		String api = request.getParameter("apikey");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {

			adminService.addTagToLead(leadId, tagList);
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Tag Successfully Added To Lead.\"}]);";
		else
			return "[{\"responceCode\":\"Tag Successfully Added To Lead.\"}]";
	}

	// http://localhost:8191/spring-hib/addTagAPI?tallycallerName=abc&tagName=a,b,c&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "addTagAPI", produces = "application/javascript")
	public @ResponseBody String addTagAPI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("addTagAPI is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String tagName = request.getParameter("tagName");
		String api = request.getParameter("apikey");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {

			adminService.addTag(tclaer.getCompanyId(), tagName);
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Tag Successfully Added.\"}]);";
		else
			return "[{\"responceCode\":\"Tag Successfully Added.\"}]";
	}

	// http://localhost:8191/spring-hib/AssignLeadToTelecallerAPI?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/updateLeadLevelCommnetAPI", produces = "application/javascript")
	public @ResponseBody String updateLeadLevelCommnetAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerId") String telecallId, @RequestParam(value = "leadId") int leadId,
			@RequestParam(value = "comment") String comment) throws ParseException, InterruptedException {
		System.out.println("updateLeadLevelCommnetAPI is caled...");
		String api = request.getParameter("apikey");

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			adminService.updateLeadComment(comment, leadId);
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Comment Successfully Updated.\"}]);";
		else
			return "[{\"responceCode\":\"Comment Successfully Updated.\"}]";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getAllTagAPI", produces = "application/javascript")
	public @ResponseBody String getAllTagAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getAllTagAPI is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {
			List list = adminService.getAllTag(tclaer.getCompanyId());
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("tagId", result[0]);
					leadmap.put("tagName", result[1]);
					jarray.add(leadmap);
				}
			} else
				return "[{\"responceCode\":\"No Data Found\"}]";
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		else
			return jarray.toJSONString();

	}

	// http://localhost:8191/spring-hib/AssignLeadToTelecallerAPI?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/AssignLeadToTelecallerAPI", produces = "application/javascript")
	public @ResponseBody String AssignLeadToTelecallerAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId) throws ParseException, InterruptedException {
		System.out.println("AssignLeadToTelecallerAPI is caled...");
		String api = request.getParameter("apikey");
		String tallycallerName = request.getParameter("tallycallerName");
		Tallycaller tclaer = new Tallycaller();
		String leadId = request.getParameter("leadId");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			adminService.AssignLeadToTelecaller(Integer.parseInt(telecallId), Integer.parseInt(leadId));
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Lead Assign Successfully.\"}]);";
		else
			return "[{\"responceCode\":\"Lead Assign Successfully.\"}]";

	}

	// http://localhost:8191/spring-hib/getIVRDataAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getIVRDataAPI", produces = "application/javascript")
	public @ResponseBody String getIVRDataAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String flag = request.getParameter("flag");
		// String endate = request.getParameter("endate");
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		// String tallycallerId = request.getParameter("tallycallerId");
		System.out.println("*******getDueTodayLeadWEBAPI********");
		// System.out.println(" name " + name);
		// System.out.println("mobileNo " + mobileNo);
		// System.out.println("email " + email);
		// System.out.println("stdate "+datef.parse(stdate));
		// System.out.println("endate "+datef.parse(endate));
		// System.out.println("***************");
		int trlecaller = 0;
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;

		if (apiKey != null) {
			if (flag.equalsIgnoreCase("Success")) {
				trlecaller = tclaer.getTcallerid();
			}

			if (name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")) {
				String list = "";
				String query = "SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.leadState,categorymanager.categortName,lead.leadProcessStatus,lead.createDate,lead.email, "
						+ "lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city "
						+ "FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ "INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "   WHERE  tallycaller.tcallerid=" + trlecaller
						+ "  AND lead.leadState='Open' AND lead.leadType='IVRWEB' ORDER BY lead.leaadId ASC";

				// System.out.println("" + query);

				String queryCount = "SELECT count(*) from lead " + "   WHERE  lead.tallyCalletId=" + trlecaller
						+ "   AND lead.leadState='Open'  AND leadState='Open' AND lead.leadType='IVRWEB' "
						+ "ORDER BY lead.leaadId ASC";
				// List list =
				// adminService.getTelecallerStateLeadCount(""+query);
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					}

					else {
						// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				list = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (list.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + list + ");");
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						// System.out.println("IN LIST");
						return list;
					}

				} else {
					// return "[{\"responceCode\":\"No Data Found\"}]";
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}

			} else {
				// System.out.println("In Else Condition");
				int i = 0;
				String listData = "";
				String queryCount = "SELECT count(*) from lead " + "   WHERE  lead.tallyCalletId=" + trlecaller
						+ "   AND lead.leadState='Open'";

				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T') "
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ " WHERE    0=0 AND tallycaller.tcallerid='" + trlecaller
								+ "' AND lead.leadState='Open' AND lead.leadType='IVRWEB'  AND ( ");

				if (!name.equalsIgnoreCase("NA")) {
					i++;
					query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
				}
				if (!mobileNo.equalsIgnoreCase("NA")) {
					i++;
					query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
				}
				if (!email.equalsIgnoreCase("NA")) {
					i++;
					query.append(" OR lead.email LIKE '%" + email + "%' ");
				}

				if (i == 0) {
					query.append("  0=0 ");
				}

				query.append("   )");
				query.append(" ORDER BY lead.leaadId ASC");
				// System.out.println("" + query);

				// System.out.println("" + query);

				// List list =
				// adminService.getTelecallerStateLeadCount(""+query);
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					}

					else {
						// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						// System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				listData = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (listData.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + listData + ");");
						return request.getParameter("callback") + "(" + listData + ");";
					} else {
						System.out.println("IN LIST");
						return listData;
					}

				} else {
					// return "[{\"responceCode\":\"No Data Found\"}]";
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}

			}

		}

		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

	}

	// http://localhost:8191/spring-hib/getTallyCallerLeadWEBAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getDueTodayLeadWEBAPI", produces = "application/javascript")
	public @ResponseBody String getDueTodayLeadWEBAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String stdate = request.getParameter("stdate");
		String endate = request.getParameter("endate");
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		// String tallycallerId = request.getParameter("tallycallerId");
		System.out.println("*******getDueTodayLeadWEBAPI********");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;

		if (apiKey != null) {
			if (name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")) {
				String list = "";
				String query = "SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,date_format(lead.sheduleDate,'%Y-%m-%d %T') "
						+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName + "' AND leadState='Open' "
						+ "   AND lead.createDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate
						+ "', INTERVAL 1 DAY) " + "ORDER BY lead.leaadId DESC";
				String queryCount = "SELECT count(*) from lead "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "   WHERE  lead.tallyCalletId=" + tclaer.getTcallerid()
						+ "   AND lead.leadState='Open' AND tallycaller.username='" + tallycallerName
						+ "' AND leadState='Open' " + "   AND lead.createDate BETWEEN '" + stdate + "' AND ADDDATE('"
						+ endate + "', INTERVAL 1 DAY) " + "ORDER BY lead.leaadId DESC";
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {
					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
					} else {
					}
					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						listSize = len;
					} else {
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				list = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (list.length() != 0) {
					if (request.getParameter("callback") != null) {
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						System.out.println("IN LIST");
						return list;
					}

				} else {
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				}

			} else {
				int i = 0;
				String listData = "";
				String queryCount = "SELECT count(*) from lead " + "   WHERE  lead.tallyCalletId="
						+ tclaer.getTcallerid() + "   AND lead.leadState='Open'";

				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T') "
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ " WHERE    0=0 AND tallycaller.username='" + tallycallerName
								+ "' AND leadState='Open' AND ( ");

				if (!name.equalsIgnoreCase("NA")) {
					i++;
					query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
				}
				if (!mobileNo.equalsIgnoreCase("NA")) {
					i++;
					query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
				}
				if (!email.equalsIgnoreCase("NA")) {
					i++;
					query.append(" OR lead.email LIKE '%" + email + "%' ");
				}

				if (i == 0) {
					query.append("  0=0 ");
				}

				query.append("   )");
				query.append(" ORDER BY lead.createDate DESC");
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
					} else {
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						listSize = len;
					} else {
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				listData = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (listData.length() != 0) {

					if (request.getParameter("callback") != null) {
						return request.getParameter("callback") + "(" + listData + ");";
					} else {
						return listData;
					}

				} else {
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				}
			}
		}
		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
	}

	// http://localhost:8191/spring-hib/AddContactbyLeadIdAPI?tallycallerName=&demo123&leadId=320&apikey=7635b67e-3d02-48c4-92a4-833edae7b2b9
	@RequestMapping(value = "AddContactbyLeadIdAPI")
	public @ResponseBody String AddContactbyLeadIdAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String leadId = request.getParameter("leadId");
		String api = request.getParameter("apikey");
		System.out.println("AddContactbyLeadIdAPI is Called");
		/*
		 * Tallycaller user = new Tallycaller();
		 * user=adminService.validateDesktopAPI(api); System.out.println(user);
		 */
		String name = null, category = "";

		ApiKey apiKey = null;

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			{
				List list = adminService.GetLeadByLeadIdAPI(Integer.parseInt(leadId));
				if (list.size() != 0) {

					for (int i = 0; i < list.size(); i++) {
						JSONObject leadmap = new JSONObject();
						Object[] result = (Object[]) list.get(i);

						List lst = null;
						lst = adminService.getContactInfoByEmail("" + result[3]);
						// System.out.println(list.size());
						if (lst.size() != 0)
							return request.getParameter("callback")
									+ "([{\"responceCode\":\"Data Allready exists.\"}]);";

						if (api != null && lst.size() == 0) {
							adminService.addContactInfo("" + result[4] + " " + result[5], "" + result[3],
									result[8].toString(), tclaer.getCompanyId());

							return request.getParameter("callback")
									+ "([{\"responceCode\":\"Contact Successfully Added.\"}]);";
						} else
							return request.getParameter("callback") + "([{\"responceCode\":\"Error In Data.\"}]);";

					}
				}
			}
		}
		return "SUccess";
	}

	// http://localhost:8191/spring-hib/addContactInfoAPI?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "addContactInfoAPI", produces = "application/javascript")
	public @ResponseBody String addContactInfoAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addContactInfoAPI is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String emailId = request.getParameter("emailId");
		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String api = request.getParameter("apikey");

		ApiKey apiKey = null;

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		int no = 0;
		if (api != null) {

			adminService.addContactInfo(name, emailId, mobileNo, tclaer.getCompanyId());

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responceCode\":\"Contact Successfully Added.\"}]);";
		} else {
			return "[{\"responceCode\":\"Contact Successfully Added.\"}]";
		}

	}

	// http://localhost:8191/spring-hib/GetAllAutoDialLeadAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetAllAutoDialLeadAPI", produces = "application/javascript")
	public @ResponseBody String GetAllAutoDialLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("GetAllAutoDialLeadAPI(OnrArg) is called.....");
		String limit = request.getParameter("limit");
		String tallycallerId = request.getParameter("tallycallerId");
		String pageNo = request.getParameter("pageNo");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		List list = null;
		if (apiKey != null) {

			list = adminService.GetAllAutoDialLead(Integer.parseInt(tallycallerId));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tag", result[18] == null ? "N/A" : result[18]);
					leadmap.put("dialState", result[19]);
					jarray.add(leadmap);
					tcallerWbJsom.put("data", jarray);
				}

			} else {

			}

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/GetAllAutoDialLeadTestAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetAllAutoDialLeadTestAPI", produces = "application/javascript")
	public @ResponseBody String GetAllAutoDialLeadTestAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* response.addHeader("Access-Control-Allow-Origin", "*"); */

		System.out.println("GetAllAutoDialLeadTestAPI is called.....");
		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		// System.out.println("***************");
		// System.out.println(" name " + name);
		// System.out.println("mobileNo " + mobileNo);
		// System.out.println("email " + email);
		// System.out.println("***************");
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {

			if (name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")) {
				String list = "";
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					}

					else {
						// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						// System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				String queryCOunt = "SELECT count(*) from lead   WHERE  lead.tallyCalletId="
						+ Integer.parseInt(tallycallerId) + "  AND autoDial=1 AND lead.leadState='Open'";

				String query = "SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.leadState,categorymanager.categortName,lead.leadProcessStatus,lead.createDate,lead.email, "
						+ "lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city "
						+ "FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ "INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "   WHERE  tallycaller.tcallerid=" + Integer.parseInt(tallycallerId)
						+ "  AND autoDial=1 AND lead.leadState='Open' ORDER BY sheduleDate ASC";

				list = adminService.ConvertDataToWebJSON(queryCOunt, query, page, listSize);
				if (list.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + list + ");");
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						// System.out.println("IN LIST");
						return list;
					}

				}

			} else {
				String queryCount = "SELECT count(*) from lead   WHERE  lead.tallyCalletId="
						+ Integer.parseInt(tallycallerId) + "  AND autoDial=1 AND lead.leadState='Open'";
				int i = 0;
				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.leadState,categorymanager.categortName,lead.leadProcessStatus,lead.createDate,lead.email, "
								+ "lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city "
								+ "FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ "INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ "   WHERE  tallycaller.tcallerid=" + Integer.parseInt(tallycallerId)
								+ "  AND autoDial=1 AND lead.leadState='Open'AND ( ");

				if (!name.equalsIgnoreCase("NA")) {
					i++;
					query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
				}
				if (!mobileNo.equalsIgnoreCase("NA")) {
					i++;
					query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
				}
				if (!email.equalsIgnoreCase("NA")) {
					i++;
					query.append(" OR lead.email LIKE '%" + email + "%' ");
				}

				if (i == 0) {
					query.append("  0=0 ");
				}

				query.append("   ) ORDER BY sheduleDate ASC");

				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				String listData = "";
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					}

					else {
						// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						// System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				listData = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (listData.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + listData + ");");
						return request.getParameter("callback") + "(" + listData + ");";
					} else {
						// System.out.println("IN LIST");
						return listData;
					}

				} else {
					// return "[{\"responceCode\":\"No Data Found\"}]";
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}

			}
		}
		return null;

	}

	// http://localhost:8191/spring-hib/TelecallerStartWorkRequest?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/TelecallerStartWorkRequest", produces = "application/javascript")
	public @ResponseBody String TelecallerStartWorkRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId) throws ParseException, InterruptedException {
		System.out.println("TelecallerStartWorkRequest is caled...");
		String api = request.getParameter("apikey");
		String tallycallerName = request.getParameter("tallycallerName");
		Tallycaller tclaer = new Tallycaller();

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			adminService.AddTelecallerStartWork(Integer.parseInt(telecallId));
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Start Work.\"}]);";
		else
			return "[{\"responceCode\":\"Start WOrk\"}]";

	}

	// http://localhost:8191/spring-hib/TelecallerEndWorkRequest?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/TelecallerEndWorkRequest", produces = "application/javascript")
	public @ResponseBody String TelecallerEndWorkRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId) throws ParseException, InterruptedException {

		String api = request.getParameter("apikey");

		System.out.println("TelecallerStartEndWorkRequest is caled..:" + telecallId + " " + "api::" + api);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, true);
		System.out.println("API COntroller_apiKey::" + apiKey);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			adminService.AddTelecallerEndWork(Integer.parseInt(telecallId));
		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCode\":\"End Work.\"}]);";
		} else {
			return "[{\"responseCode\":\"End Work.\"}]";
		}
	}

	// http://localhost:8191/spring-hib/TelecallerStartBreakRequest?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/TelecallerStartBreakRequest", method = RequestMethod.GET)
	public @ResponseBody String TelecallerStartBreakRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId) throws ParseException, InterruptedException {
		System.out.println("TelecallerStartWorkRequest is caled...");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			adminService.AddTelecallerBreakTime(Integer.parseInt(telecallId));
		}

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "(Break Time);";
		else
			return "[{\"responceCode\":\"Break Time\"}]";
	}

	// http://localhost:8191/spring-hib/TelecallerEndBreakRequest?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/TelecallerEndBreakRequest", method = RequestMethod.GET)
	public @ResponseBody String TelecallerEndBreakRequest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallId") String telecallId) throws ParseException, InterruptedException {
		System.out.println("TelecallerStartWorkRequest is caled...");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(telecallId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			adminService.AddTelecallerEndBreakTime(Integer.parseInt(telecallId));
		}
		// return "Break Time";

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "(End Break Time);";
		else
			return "[{\"responceCode\":\"End Break Time\"}]";
	}

	// http://localhost:8191/spring-hib/AddLeadByBonrixWebResponceAPI?tallycallerId=3&&userName=Demo
	// Bonrix&&catName=Download Inquiry - SMPP Reseller
	// Inquiry&&firstName=bonric&&lastName=system&&email=bonrix@gmail.com&&companytName=aaa&&catName=Category7&&mobileNo=9426045500&webSite=www.abc.com&company=abbc
	// pvt
	// ltd&country=India&state=Gujarat&city=City&ocity=ocity&remark=dcsd&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "AddLeadByIdeaWebPanelAPI", produces = "application/javascript")
	public @ResponseBody String AddLeadByIdeaWebPanelAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("AddLeadByIdeaWebPanelAPI is called");
		String tallycallerid = request.getParameter("tallycallerId");
		String userName = request.getParameter("userName");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String companytName = "Bonrix Software System";
		String catId = request.getParameter("catId");
		String catName = request.getParameter("catName");
		String mobileNo = request.getParameter("mobileNo");
		String webSite = request.getParameter("webSite");
		String company = request.getParameter("company");
		String country = request.getParameter("country");
		String state = request.getParameter("state");
		String city = request.getParameter("city");
		String ocity = request.getParameter("ocity");
		String remark = request.getParameter("remark");
		String api = request.getParameter("apikey");
		String redirect = request.getParameter("redirect");

		/*
		 * System.out.println(request.getParameter("tallycallerId"));
		 * System.out.println(userName); System.out.println(firstName);
		 * System.out.println(lastName); System.out.println(email);
		 * System.out.println(companytName); System.out.println(catName);
		 * System.out.println(mobileNo); System.out.println(webSite);
		 * System.out.println(company); System.out.println(country);
		 * System.out.println(state); System.out.println(city);
		 * System.out.println(ocity); System.out.println(remark);
		 */
		int ik = 0;
		User user = null;
		user = adminService.getUidByUserName(userName);
		String mapiKey = null;
		if (user == null) {
			return "[{\"responseCode\":\"INVALID_USER\"}]";
		}

		if (user != null) {
			mapiKey = user.getApiKey();
			// System.out.println(mapiKey);
			if (!mapiKey.equalsIgnoreCase(api)) {
				// System.out.println("In IF");
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}

		}
		String city1 = null;
		if (mapiKey != null) {
			if (city.equalsIgnoreCase("City")) {
				// System.out.println("NullisCalled");
				city1 = ocity;
			} else {
				// System.out.println("NullisnotCalled");
				city1 = city;
			}

			// List Catlist=adminService.getCategoryList(new Long(catId));
			// Object catObj=(Object[])Catlist.get(0);
			ik = adminService.addLeadData(userName, firstName, lastName, email, companytName, catId, mobileNo, webSite,
					company, country, state, city1, remark);
			// System.out.println("RowCount:" + ik);
		}
		if (ik != 0)
			return "Lead Successfully Inserted";
		else
			return "Error In Lead Insertion ";

		/*
		 * if(ik!=0) { System.out.println("InSMS"); AsyncHttpClient asyncHttpClient =
		 * new AsyncHttpClient(); List list = null; String url1 = null; String
		 * message="New Lead Assign To You.\nName : "+firstName+"\nEmail : "
		 * +email+"\nContact No : "+mobileNo+"\nCategory : "+catName+""; final
		 * Tallycaller tcaler1 =
		 * adminService.GetTellyCallerById(Integer.parseInt(tallycallerid));
		 * 
		 * list = adminService.GetAPI(tcaler1.getCompanyId());
		 * System.out.println(list.size()); for (int i = 0; i < list.size(); i++) {
		 * 
		 * JSONObject leadmap = new JSONObject(); Object[] result = (Object[])
		 * list.get(i); // uid=(int) result[0]; boolean status = (boolean) result[4];
		 * System.out.println("STATUS:" + status); if (status) { url1 = (String)
		 * result[1];
		 * 
		 * url1 = url1.replace("<mobileno>", tcaler1.getMobno() +
		 * "").replace("<message>", URLEncoder.encode(message)); //
		 * System.out.println("Mobile : "+mob+" \n"+"MSG : "+message);
		 * 
		 * System.out.println("SMSURL1:"+url1);
		 * 
		 * }
		 * 
		 * } String name=firstName+" "+lastName; CRMSendSMS sendsms=new CRMSendSMS();
		 * sendsms.SendWelcomeSMS(adminService,name,tcaler1.getCompanyId(),
		 * Integer.parseInt(catId),mobileNo,tallycallerid); if (url1 != null) {
		 * 
		 * // final Tallycaller //
		 * tcaler1=adminService.GetTellyCallerById(Integer.parseInt( tallycallerId));
		 * final Sentsmslog log = new Sentsmslog(); log.setTallycallerId(0);
		 * log.setStaffId(0); log.setCompayId(0);
		 * 
		 * 
		 * log.setMobileNo(Long.valueOf(tcaler1.getMobno())); log.setMessage(message);
		 * log.setSendDate(new Date());
		 * 
		 * // System.out.println(log.getMobileNo()+"\n"+log.getMessage()+"\n"+log.
		 * getSendDate()+"\n"+log.getTallycallerId()+"\n"+log.getCompayId()+"\n"
		 * +log.getStaffId()); System.out.println("CallingURL:"+url1);
		 * asyncHttpClient.prepareGet(url1).execute(new
		 * AsyncCompletionHandler<Response>() {
		 * 
		 * 
		 * 
		 * @Override public Response onCompleted(Response responce) throws Exception {
		 * // TODO Auto-generated method stub
		 * 
		 * // System.out.println("Comp Id : "+tcaler.getCompanyId());
		 * 
		 * User user = new User(); user =
		 * adminService.getUserByUid(Integer.toUnsignedLong(tcaler1.getCompanyId ()));
		 * 
		 * List list = adminService.GetAPI(user.getCompanyId()); String ststus = ""; //
		 * System.out.println("List Size : "+list.size()); int uid = 0; //
		 * System.out.println(list.size()); for (int i = 0; i < list.size(); i++) {
		 * JSONObject leadmap = new JSONObject(); Object[] result = (Object[])
		 * list.get(i); uid = (int) result[0]; boolean status = (boolean) result[4]; if
		 * (status == true) { ststus = result[2].toString(); //
		 * System.out.println("URL ID : "+uid); } }
		 * 
		 * // System.out.println("Status : "+ststus); String res =
		 * responce.getResponseBody().replaceAll(" ", ""); if (res.contains(ststus)) {
		 * System.out.println("NoErrorinMSG"); log.setMSGStatus("SUCCESS"); } else {
		 * System.out.println("ErrorinMSG");
		 * 
		 * log.setMSGStatus("FAIL"); }
		 * 
		 * // if(res.length()>255) // { //
		 * log.setAPIResponse(res.toString().substring(0, 254)); // } // else // { //
		 * log.setAPIResponse(res.toString()); // }
		 * 
		 * try { log.setAPIResponse(res); System.out.println(log.getAPIResponse() + "\n"
		 * + log.getMSGStatus());
		 * 
		 * adminService.saveLog(log);
		 * 
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * System.out.println("SaveError:" + e); }
		 * System.out.println(responce.getResponseBody()); return responce; }
		 * 
		 * @Override public void onThrowable(Throwable t) { // Something wrong happened.
		 * t.printStackTrace(); System.out.println("======="+t.getMessage()); //
		 * System.out.println(statusCode+"::"+new Date());
		 * log.setAPIResponse(t.getMessage().substring(0, 100).trim());
		 * log.setMSGStatus("FAIL"); adminService.saveLog(log); }
		 * 
		 * }); } else { return "SMS Url Not Generated.. "+url1; }
		 * 
		 * }
		 */
		/*
		 * else { System.out.println("Not in SMS"); return
		 * "Lead not Successfully Inserted RowCount:"+ik;
		 * 
		 * }
		 */

		// String redirectUrl = request.getScheme() + "://www.yahoo.com";

	}

	// http://localhost:8191/spring-hib/getTodayLeads?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getTodayLeads", produces = "application/javascript")
	public @ResponseBody String getTodayLeads(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getTodayLeads is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			List list = adminService.getDueTodayLead(Integer.parseInt(tallycallerId));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tagName ", "NA");
					leadmap.put("dialState", result[18]);
					jarray.add(leadmap);
				}
			}
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		else
			return jarray.toJSONString();

	}

	// @RequestMapping(value = "/AutoDialTestDisconnectRequest", method =
	// RequestMethod.GET)
	// public @ResponseBody String
	// AutoDialTestDisconnectRequest(HttpServletRequest request,
	// HttpServletResponse response,
	// @RequestParam(value = "telecallId") String telecallId,
	// @RequestParam(value = "mobileNo") String mobileNo,
	// @RequestParam(value = "waitresponse") String waitresponse) throws
	// ParseException, InterruptedException {
	//
	// System.out.println(" AutoDialTestDisconnectRequest...... ");
	// System.out.println(" : " + telecallId + " : " + mobileNo);
	//
	// for (Map.Entry<String, WebSocketObj> entry :
	// CRMHashMap.getInstance().connectedClient.entrySet()) {
	// WebSocketObj wskt = entry.getValue();
	//
	// WebSocket skt = wskt.getSkt();
	//
	//
	// // skt1.sendMessage("Hello Sajna","MOBILE_NO_SEND");
	// String
	// sendstr="[{\"Type\":\"MOBILE_NO_AUTO_DIAL_DISCONNECT_REQUEST\",\"Status\":\"Mobile
	// Number Send For End Call\",\"TelecallerId\":\""
	// + telecallId + "\",\"MobileNo\":\"" + mobileNo + "\"}]";
	//
	// LOG.info("SENDING:"+sendstr);
	// try {
	// skt.send(sendstr);
	//
	//
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }
	//
	// return "AutoDialTestDisconnectRequest is End..";
	// }

	// http://localhost:8191/spring-hib/getCallStatusLead?tallycallerName=Bonrix&apikey=XXXXXX

	@RequestMapping(value = "getCallStatusLead", produces = "application/javascript")
	public @ResponseBody String getCallStatusLead(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String status = request.getParameter("status");
		String catId = request.getParameter("catId");
		String lstate = request.getParameter("lstate");
		String sStaus = request.getParameter("sStaus");
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		if (apiKey != null) {
			// System.out.println(status);
			StringBuilder query = new StringBuilder();
			if (status.equals("Success")) {
				query.append(
						"SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,"
								+ "	 lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city ,"
								+ "	 followupshistory.callingTime,followupshistory.newStatus,followupshistory.successStatus"
								+ "	 FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ "	 INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ "	 INNER JOIN followupshistory ON lead.leaadId=followupshistory.leadId "
								+ "	 WHERE  tallycaller.username= '" + tallycallerName + "'  AND lead.companyId="
								+ tclaer.getCompanyId() + ""
								+ "	 AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId)"
								+ "	 AND leadState='Open'  AND followupshistory.successStatus !='N/A' AND followupshistory.successStatus IS NOT NULL ");

				if (!catId.equals("0")) {
					query.append(" AND lead.categoryId='" + catId + "' ");
				}

				if (!sStaus.equals("sstate")) {
					query.append(" AND followupshistory.successStatus='" + sStaus + "'");
				}

				if (!lstate.equals("lstate")) {
					query.append(" AND lead.leadProcessStatus='" + lstate + "'");
				}

				query.append(" GROUP BY lead.leaadId ");

			}

			if (status.equals("Fail")) {
				query.append(
						"SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,"
								+ "	 lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city ,"
								+ "	 followupshistory.callingTime,followupshistory.newStatus,followupshistory.failstatus"
								+ "	 FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ "	 INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ "	 INNER JOIN followupshistory ON lead.leaadId=followupshistory.leadId "
								+ "	 WHERE  tallycaller.username= '" + tallycallerName + "'  AND lead.companyId="
								+ tclaer.getCompanyId() + ""
								+ "	 AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId)"
								+ "	 AND leadState='Open'  AND followupshistory.failstatus !='N/A' AND followupshistory.failstatus IS NOT NULL ");

				if (!catId.equals("0")) {
					query.append(" AND lead.categoryId='" + catId + "' ");
				}

				if (!sStaus.equals("sstate")) {
					query.append(" AND followupshistory.failstatus='" + sStaus + "'");
				}

				if (!lstate.equals("lstate")) {
					query.append(" AND lead.leadProcessStatus='" + lstate + "'");
				}

				query.append(" GROUP BY lead.leaadId ");

			}

			// System.out.println(query);
			List list = adminService.getTelecallerStateLeadCount("" + query);
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();

					result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("lstcall", result[18]);
					leadmap.put("calst", result[19]);
					leadmap.put("sst", result[20]);
					jarray.add(leadmap);

				}

			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";

			}

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/getUnattenedLeadsAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getUnattenedLeadsAPI", produces = "application/javascript")
	public @ResponseBody String getUnattenedLeadsAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		if (apiKey != null) {
			String query = " SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,"
					+ " lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState "
					+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
					+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
					+ "   WHERE  tallycaller.username= '" + tallycallerName + "'  AND lead.companyId="
					+ tclaer.getCompanyId() + " "
					+ "   AND lead.leaadId NOT IN (SELECT followupshistory.leadId FROM followupshistory)"
					+ "   AND leadState='Open'";
			System.out.println("query_:"+query);
			List list = adminService.getTelecallerStateLeadCount(query);
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();

					result = (Object[]) list.get(i);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tagName", "NA");
					leadmap.put("dialState", result[18]);
					jarray.add(leadmap);

				}

			} else {

				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";

			}

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/getTallyCallerLeadWEBAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getTallyCallerLeadWEBAPI", produces = "application/javascript")
	public @ResponseBody String getTallyCallerLeadWEBAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getTallyCallerLeadWEBAPI is called");
		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String tallycallerName = request.getParameter("tallycallerName");
		String orderBy = request.getParameter("orderBy");
		String catNme = request.getParameter("catNme");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;

		if (apiKey != null) {

			if (name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")
					&& orderBy.equalsIgnoreCase("NA")) {
				String list = "";
				String query = "SELECT lead.leaadId,lead.firstName,lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),lead.tagName,DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),lead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState,lead.csvData "
						+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName
						+ "' AND leadState='Open' ORDER BY lead.createDate DESC ";

				System.out.println("" + query);

				String queryCount = "SELECT count(*) "
						+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName
						+ "' AND leadState='Open' ORDER BY lead.createDate DESC ";
				// List list =
				// adminService.getTelecallerStateLeadCount(""+query);
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					}

					else {
						// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						// System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				list = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (list.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + list + ");");
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						// System.out.println("IN LIST");
						return list;
					}

				} else {
					// return "[{\"responceCode\":\"No Data Found\"}]";
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}

			} else if (!orderBy.equalsIgnoreCase("NA") && name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA")
					&& email.equalsIgnoreCase("NA")) {
				String list = "";
				String query = "SELECT lead.leaadId,lead.firstName,lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),lead.tagName,DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),lead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState "
						+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName + "' AND leadState='Open' "
						+ orderBy.replaceAll("_", " ");

				System.out.println("ORDER By :: " + query);

				String queryCount = "SELECT count(*) "
						+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName + "' AND leadState='Open' "
						+ orderBy.replaceAll("_", " ");

				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					} else {
						// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						// System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				list = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (list.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + list + ");");
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						// System.out.println("IN LIST");
						return list;
					}

				} else {
					// return "[{\"responceCode\":\"No Data Found\"}]";
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}
			} else {
				// System.out.println("In Else Condition");
				int i = 0;
				String listData = "";
				String queryCount = "SELECT count(*) from lead " + "   WHERE  lead.tallyCalletId="
						+ tclaer.getTcallerid() + "   AND lead.leadState='Open'";

				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,lead.firstName,lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),lead.tagName,DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),lead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState "
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ " WHERE    0=0 AND tallycaller.username='" + tallycallerName
								+ "' AND leadState='Open' AND ( ");

				if ((!name.equalsIgnoreCase("NA")) || (!name.equalsIgnoreCase("undefined"))
						|| (!name.equalsIgnoreCase("null"))) {
					i++;
					query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
				}
				if (mobileNo != null && !mobileNo.equalsIgnoreCase("NA") && !mobileNo.equalsIgnoreCase("undefined")
						&& !mobileNo.equalsIgnoreCase("null")) {
					i++;
					query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
				}
				if (email != null && !email.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("undefined")
						&& !email.equalsIgnoreCase("null")) {
					i++;
					query.append(" OR lead.email LIKE '%" + email + "%' ");
				}
				if (catNme != null && !catNme.equalsIgnoreCase("NA") && !catNme.equalsIgnoreCase("undefined")
						&& !catNme.equalsIgnoreCase("null")) {
					i++;
					query.append("  OR categorymanager.categortName LIKE '%" + catNme + "%' ");
				}

				if (i == 0) {
					query.append("  0=0 ");
				}

				query.append("   )");
				System.out.println("" + query);

				System.out.println("" + query);

				// List list =
				// adminService.getTelecallerStateLeadCount(""+query);
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					}

					else {
						// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						// System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}
				listData = adminService.ConvertDataToWebJSON(queryCount, "" + query, page, listSize);
				if (listData.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + listData + ");");
						return request.getParameter("callback") + "(" + listData + ");";
					} else {
						// System.out.println("IN LIST");
						return listData;
					}

				} else {
					// return "[{\"responceCode\":\"No Data Found\"}]";
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}

			}

		}

		/*
		 * if (request.getParameter("callback") != null && list.length() != 0) { return
		 * request.getParameter("callback") + "(" + jarray.toJSONString() + ");"; } else
		 * if(list.length() != 0) { return jarray.toJSONString(); } else
		 */
		// return null;
		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

	}

	// http://localhost:8191/spring-hib/getCategoryLeadSearchWEBAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getCategoryLeadSearchWEBAPI", produces = "application/javascript")
	public @ResponseBody String getCategoryLeadSearchWEBAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		String ledstate = request.getParameter("ledstate");
		String catId = request.getParameter("catId");
		String tallycallerId = request.getParameter("tallycallerId");
		String dial = request.getParameter("dial");
		System.out.println("getCategoryLeadSearchWEBAPI : " + tallycallerName + "  " + ledstate + " " + catId);
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		/*
		 * System.out.println("***************"); System.out.println(" name " + name);
		 * System.out.println("mobileNo " + mobileNo); System.out.println("email " +
		 * email); System.out.println("***************");
		 */
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		String list = "";
		if (apiKey != null) {
			if (name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")
					&& dial.equalsIgnoreCase("ALL")) {
				StringBuilder queryCount = new StringBuilder();
				queryCount.append("SELECT count(*) from lead " + "   WHERE  lead.tallyCalletId=" + tclaer.getTcallerid()
						+ "   AND lead.leadState='Open'");
				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,date_format(lead.sheduleDate,'%Y-%m-%d %T'),lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState,lead.csvData"
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName
								+ "' AND leadState='Open' ");
				if (!catId.equalsIgnoreCase("0")) {
					query.append(" AND lead.categoryId=" + Integer.parseInt(catId) + " ");
					queryCount.append(" AND lead.categoryId=" + Integer.parseInt(catId) + " ");
				}
				if (!ledstate.equalsIgnoreCase("None")) {
					query.append(" AND lead.leadProcessStatus='" + ledstate + "'");
					queryCount.append(" AND lead.leadProcessStatus='" + ledstate + "'");
				}
				// System.out.println("" + query);

				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {

					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
						// System.out.println("STSRT : " + start);
					}

					else {
						/// System.out.println("STSRT : " + start);
					}

					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						// System.out.println("LENGTH : " + len);
						listSize = len;
					} else {
						// System.out.println("LENGTH : " + len);
					}
					page = start / len + 1;
				} catch (Exception ex) {

				}

				list = adminService.ConvertDataToWebJSON("" + queryCount, "" + query, page, listSize);
				if (list.length() != 0) {

					if (request.getParameter("callback") != null) {
						// System.out.println(request.getParameter("callback") +
						// "(" + list + ");");
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						// System.out.println("IN LIST");
						return list;
					}

				} else {
					// return "[{\"responceCode\":\"No Data Found\"}]";
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				}

			}

			else {
				StringBuilder queryCount = new StringBuilder();
				queryCount.append("SELECT count(*) from lead " + "   WHERE  lead.tallyCalletId=" + tclaer.getTcallerid()
						+ "   AND lead.leadState='Open'");
				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,date_format(lead.sheduleDate,'%Y-%m-%d %T')"
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName
								+ "' AND leadState='Open' AND ( 0=0 ");

				int i = 0;
				if (!name.equalsIgnoreCase("NA")) {
					query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
				}
				if (!mobileNo.equalsIgnoreCase("NA")) {
					query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
				}
				if (!email.equalsIgnoreCase("NA")) {
					query.append(" OR lead.email LIKE '%" + email + "%' ");
				}
				if (!catId.equalsIgnoreCase("0")) {
					query.append(" AND lead.categoryId=" + Integer.parseInt(catId) + " ");
					queryCount.append(" AND lead.categoryId=" + Integer.parseInt(catId) + " ");
				}
				if (!ledstate.equalsIgnoreCase("None")) {
					query.append(" AND lead.leadProcessStatus='" + ledstate + "'");
					queryCount.append(" AND lead.leadProcessStatus='" + ledstate + "'");
				}
				if (!dial.equalsIgnoreCase("ALL")) {
					query.append(" AND lead.dialState=" + Integer.parseInt(dial) + "");
					queryCount.append(" AND lead.dialState=" + Integer.parseInt(dial) + "");
				}

				query.append("   AND  0=0 )");

				System.out.println("SAJAN :: " + query);
				System.out.println("SAJAN :: " + queryCount);
				int page = 1;
				int listSize = 10;
				int start = 0;
				int len = 0;
				String mob = "";
				Date fdate = null;
				Date todate = null;
				try {
					if (request.getParameter("start") != null) {
						start = Integer.parseInt(request.getParameter("start"));
					}
					if (request.getParameter("length") != null) {
						len = Integer.parseInt(request.getParameter("length"));
						listSize = len;
					}
					page = start / len + 1;
				} catch (Exception ex) {
				}
				list = adminService.ConvertDataToWebJSON("" + queryCount, "" + query, page, listSize);
				if (list.length() != 0) {
					if (request.getParameter("callback") != null) {
						return request.getParameter("callback") + "(" + list + ");";
					} else {
						return list;
					}
				} else {
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				}
			}
		}
		return null;
	}

	// http://localhost:8191/spring-hib/getTallyCallerLeadAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getCategoryLeadSearchAPI", produces = "application/javascript")
	public @ResponseBody String getCategoryLeadSearchAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		String ledstate = request.getParameter("ledstate");
		String catId = request.getParameter("catId");
		String tallycallerId = request.getParameter("tallycallerId");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		if (apiKey != null) {
			StringBuilder sb = new StringBuilder();
			sb.append(
					"SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,"
							+ " lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city "
							+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
							+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
							+ "    WHERE    0=0 AND tallycaller.username='" + tallycallerName
							+ "' AND leadState='Open' ");
			if (!catId.equalsIgnoreCase("0")) {
				sb.append(" AND lead.categoryId=" + Integer.parseInt(catId) + " ");
			}
			if (!ledstate.equalsIgnoreCase("None")) {
				sb.append(" AND lead.leadProcessStatus='" + ledstate + "'");
			}
			// System.out.println("" + sb);
			List list = adminService.getTelecallerStateLeadCount("" + sb);
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();

					result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					jarray.add(leadmap);

				}

			} else {
				// return "[{\"responceCode\":\"No Data Found\"}]";
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

			}

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/getEntryFromTelecallerList?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getTelecallerStateLeadCount", produces = "application/javascript")
	public @ResponseBody String getTelecallerStateLeadCount(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getTelecallerStateLeadCount is Called..");
		// String ledstate = request.getParameter("ledstate");
		String catId = request.getParameter("catId");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			int i1 = 0;
			Tallycaller tcaler = new Tallycaller();
			tcaler = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT lead.leadProcessStatus,COUNT(lead.leaadId) FROM lead WHERE   0=0 ");
			if (!catId.equalsIgnoreCase("0")) {
				sb.append(" AND lead.companyId=" + tcaler.getCompanyId() + " AND lead.tallyCalletId=" + tallycallerId
						+ " AND lead.categoryId=" + Integer.parseInt(catId)
						+ " AND lead.leadState='Open' GROUP BY lead.leadProcessStatus");
			}
			if (catId.equalsIgnoreCase("0")) {
				i1 = 1;
				sb.append(" AND lead.companyId=" + tcaler.getCompanyId() + " AND lead.tallyCalletId=" + tallycallerId
						+ " AND lead.leadState='Open' GROUP BY lead.leadProcessStatus");
			}
			// if(i1==0)
			// sb.append(" AND lead.companyId="+tcaler.getCompanyId()+" AND
			// lead.tallyCalletId="+tallycallerId+" AND
			// lead.categoryId="+Integer.parseInt(catId)+" GROUP BY
			// lead.leadProcessStatus");

			// System.out.println("" + sb);
			List list = adminService.getTelecallerStateLeadCount("" + sb);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);

					leadmap.put("leadProcessState", result[0]);
					leadmap.put("Count", result[1]);

					jarray.add(leadmap);

				}
				// System.out.println(jarray.toJSONString());
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return jarray.toJSONString();
			} else {
				JSONObject leadmap = new JSONObject();

				leadmap.put("leadProcessState", "0");
				leadmap.put("Count", "0");

				jarray.add(leadmap);
			}

		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		else
			return jarray.toJSONString();

	}

	// http://localhost:8191/spring-hib/getTodayCallLog?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getTodayCallLog", produces = "application/javascript")
	public @ResponseBody String getTodayCallLog(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getTodayCallLog is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		ArrayList<String> numbers = new ArrayList<String>();

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		int no = 0;
		if (api != null) {
			Tallycaller tcaler = new Tallycaller();
			tcaler = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
			List list = adminService.getTodayTimeCallLog(tcaler.getCompanyId());
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);
					String tcId = result[5].toString();
					if (tcId.equalsIgnoreCase(tallycallerId)) {
						leadmap.put("TelecallerName", result[0]);
						leadmap.put("FailCount", result[1]);
						leadmap.put("SuccessCount", result[2]);
						leadmap.put("TotalCount", result[3]);
						leadmap.put("TotalCallDuration", result[4]);
						leadmap.put("TelecallerId", result[5]);
						jarray.add(leadmap);
					}

				}

			} else {
				JSONObject leadmap = new JSONObject();

				leadmap.put("TelecallerName", "N/A");
				leadmap.put("FailCount", "N/A");
				leadmap.put("SuccessCount", "N/A");
				leadmap.put("TotalCount", "N/A");
				leadmap.put("TotalCallDuration", "N/A");
				leadmap.put("TelecallerId", "N/A");
				jarray.add(leadmap);

			}
		}
		// System.out.println(jarray.toJSONString());
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		else
			return jarray.toJSONString();

	}

	// http://localhost:8191/spring-hib/GetConnectedTelecallerList?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetConnectedTelecallerList", produces = "application/javascript")
	public @ResponseBody String GetConnectedTelecallerList(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("GetConnectedTelecallerList is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		ArrayList<String> numbers = new ArrayList<String>();

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		int no = 0;
		if (api != null) {

			JSONObject leadmap = new JSONObject();

			for (Map.Entry<String, WebSocketObj> entry : CRMHashMap.getInstance().connectedClient.entrySet()) {

				// System.out.println(entry.getKey()+" "+entry.getValue());
				// numbers=entry.getValue();
				// leadmap.put("telecalerId", numbers.get(0));
				// leadmap.put("UniqueId", numbers.get(1));
				// leadmap.put("SessionObj", numbers.get(2)== null ? "N/A" :
				// numbers.get(2));
				// jarray.add(leadmap);

			}

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/getTemplateAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getEmailTemplateAPI", produces = "application/javascript")
	public @ResponseBody String getEmailTemplateAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getEmailTemplateAPI is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			Tallycaller tcaler = new Tallycaller();
			tcaler = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
			List list = adminService.getEmailTemplateAPI(tcaler.getCompanyId());
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("id", result[0]);
					leadmap.put("tempName", result[1]);
					leadmap.put("message", result[2]);
					leadmap.put("subject", result[3]);
					jarray.add(leadmap);
				}

			} else {
				return "[{\"responseCode\":\"NO_DATA_FOUND\"}]";
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/SendSingalMessageAPI?tcid=3&&mobno=9173186596&msg=hello&apikey=2d14f15c-9d78-488e-8a34-3f4a3337ff27
	@SuppressWarnings("resource")
	@RequestMapping(value = "/SendSingalMessageAPI", produces = "application/javascript", method = RequestMethod.GET)
	public @ResponseBody String SendSingalMessageAPI(HttpServletRequest request, HttpServletResponse response)
			throws ParseException, UnsupportedEncodingException {
		String mob = request.getParameter("mobno");
		String message = request.getParameter("msg");
		String telecallerId = request.getParameter("tcid");
		String msg = request.getParameter("msg");
		String api = request.getParameter("apikey");
		final Tallycaller tcaler = adminService.GetTellyCallerById(Integer.parseInt(telecallerId));
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		ApiKey apival = null;
		apival = adminService.ValidateAPI(api);
		if (apival == null) {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
			} else {
				return "[{\'responseCode\':\'INVALID_KEY\'}]";
			}
		} else {
			List list = null;
			String url1 = null;

			list = adminService.GetAPI(tcaler.getCompanyId());
			for (int i = 0; i < list.size(); i++) {
				JSONObject leadmap = new JSONObject();
				Object[] result = (Object[]) list.get(i);
				boolean status = (boolean) result[4];
				if (status) {
					url1 = (String) result[1];
					url1 = url1.replace("<mobileno>", mob + "").replace("<message>", URLEncoder.encode(message));
				}

			}

			if (url1 != null) {
				final Sentsmslog log = new Sentsmslog();
				log.setTallycallerId(Integer.parseInt(telecallerId));
				log.setStaffId(0);
				log.setCompayId(tcaler.getCompanyId());
				log.setMobileNo(new Long(mob));
				log.setMessage(msg);
				log.setSendDate(new Date());
				asyncHttpClient.prepareGet(url1).execute(new AsyncCompletionHandler<Response>() {
					@Override
					public Response onCompleted(Response responce) throws Exception {
						List list = adminService.GetAPI(tcaler.getCompanyId());
						String ststus = "";
						int uid = 0;
						for (int i = 0; i < list.size(); i++) {
							JSONObject leadmap = new JSONObject();
							Object[] result = (Object[]) list.get(i);
							uid = (int) result[0];
							boolean status = (boolean) result[4];
							if (status == true) {
								ststus = result[2].toString();
							}
						}
						String res = responce.getResponseBody();
						if (res.contains(ststus) && !res.contains("invalidnumber") && !res.contains("INVALID_USER")
								&& !res.contains("INVALID_KEY")) {
							log.setMSGStatus("SUCCESS");
						} else {
							log.setMSGStatus("FAIL");
						}

						try {
							log.setAPIResponse(res);
							adminService.saveLog(log);

						} catch (Exception e) {
							System.out.println("Save Error : " + e);
						}
						return responce;
					}

					@Override
					public void onThrowable(Throwable t) {
						// Something wrong happened.
						t.printStackTrace();
						log.setAPIResponse(t.getMessage().substring(0, 100).trim());
						log.setMSGStatus("FAIL");
						adminService.saveLog(log);
					}

				});
			}
		}
		return request.getParameter("callback") + "( [{\'responseCode\':\'MESSAGE_SEND_SUCCESSFULLY\'}])";
	}

	// http://localhost:8191/spring-hib/getTeleCallerById?Id=9173186596
	@RequestMapping(value = "getTeleCallerById", produces = "application/javascript")
	public @ResponseBody String getTeleCallerById(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getTeleCallerById  is Called.. => ");
		String tcId = request.getParameter("tallycallerId");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		List list = adminService.getTeleCallerByUid(new Long(tcId));

		if (list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				JSONObject leadmap = new JSONObject();
				Object[] result = (Object[]) list.get(i);
				leadmap.put("tcallerid", result[0]);
				leadmap.put("username", result[1]);
				leadmap.put("firstname", result[2]);
				leadmap.put("email", result[3]);
				leadmap.put("mobno", result[4]);
				leadmap.put("active", result[5]);
				leadmap.put("regDate", result[6]);
				leadmap.put("companyId", result[7]);
				leadmap.put("lastname", result[7]);
				jarray.add(leadmap);
			}

		} else {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

		}

		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

	}

	// http://localhost:8191/spring-hib/getLeadIdAndCategoryName?MobNo=9173186596
	@RequestMapping(value = "getLeadIdAndCategoryName", produces = "application/javascript")
	public @ResponseBody String getLeadIdAndCategoryName(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getLeadIdAndCategoryName  is Called.. => ");
		String mobno = request.getParameter("MobNo");
		String tcId = request.getParameter("tcId");
		Tallycaller tcaler = adminService.GetTellyCallerById(Integer.parseInt(tcId));

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		List list = adminService.getLeadIdAndCategoryName(mobno, tcaler.getCompanyId());
		if (list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				JSONObject leadmap = new JSONObject();
				Object[] result = (Object[]) list.get(i);
				leadmap.put("leaadId", result[0]);
				leadmap.put("catName", result[1]);

				jarray.add(leadmap);
				/*
				 * tcallerWbJsom.put("data", jarray); System.out.println(tcallerWbJsom);
				 */
			}

		} else {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		}

		return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

	}

	// http://localhost:8191/spring-hib/addLeadDesktopAPI?comastringData=comastringData&tcId=3&apikey=XXXXXX
	@RequestMapping(value = "addLeadDesktopAPI", produces = "application/javascript")
	public @ResponseBody String addLeadDesktopAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String ContactNo = request.getParameter("ContactNo");
		String tcId = request.getParameter("tcId");
		String API = request.getParameter("apikey");
		String ComaStr = request.getParameter("comastringData");
		Tallycaller tcaler = adminService.GetTellyCallerById(Integer.parseInt(tcId));

		Tallycaller apival = new Tallycaller();
		apival = adminService.validateTelecallerDesktopAPI(API);
		/*
		 * if (request.getParameter("callback") != null) { return
		 * request.getParameter("callback") +
		 * "( [{\'responseCode\':\'INVALID_INVALID_KEY\'}])"; } else { return
		 * "[{\'responseCode\':\'INVALID_INVALID_KEY\'}]"; }
		 */
		// org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		if (apival != null) {
			// System.out.println("AddLead : " + ComaStr);
			String[] data = ComaStr.split(",");
			/*
			 * System.out.println(data[0]); System.out.println(data[1]);
			 * System.out.println(data[2]); System.out.println(data[3]);
			 * System.out.println(data[4]); System.out.println(data[5]);
			 * System.out.println(data[6]); System.out.println(data[7]);
			 * System.out.println(data[8]);
			 * 
			 * System.out.println(data[9]);
			 * 
			 * System.out.println(data[10]);
			 * 
			 * System.out.println(data[11]);
			 * 
			 * System.out.println(data[12]);
			 */
			Lead lead = new Lead();
			lead.setcategoryId(data[5]);
			lead.setstaffId(0);
			lead.setCreateDate(new Date());
			lead.setemail(data[3]);
			lead.setFirstName(data[1]);
			lead.setLastName(data[2]);
			lead.setLeadType("WEB");
			lead.setLeadProcessStatus("None");
			lead.setMobileNo(data[4]);
			lead.setLeadState("Open");
			lead.setSheduleDate(df.parse("1980-06-30 17:36:00"));
			lead.setCompanyId(tcaler.getCompanyId());
			lead.setTallyCalletId(apival.getTcallerid());
			lead.setCountry(data[7]);
			lead.setState(data[8]);
			lead.setCity(data[9]);
			lead.setCompanyName(data[10]);
			lead.setWebsite(data[11]);

			adminService.AddLead(lead);
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'Lead Successfully Inseted\'}])";
			} else {
				return "[{\'responseCode\':\'Lead Successfully Inseted\'}]";
			}
		} else {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
			} else {
				return "[{\'responseCode\':\'INVALID_KEY\'}]";
			}
		}

	}

	// http://localhost:8191/spring-hib/GetContactDetaailsAPI?comastringData=comastringData&tcId=3&apikey=XXXXXX
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "addLeadAPI", produces = "application/javascript")
	public @ResponseBody String addLeadAPI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String tcId = request.getParameter("tcId");
		String API = request.getParameter("apikey");
		String ComaStr = request.getParameter("comastringData");
		Tallycaller tcaler = adminService.GetTellyCallerById(Integer.parseInt(tcId));

		ApiKey apival = null;
		apival = adminService.ValidateAPI(API);
		if (apival != null) {
			String[] data = ComaStr.split(",");
			int no = adminService.validateNoCategory(data[4], Integer.parseInt(data[5]));
			if (no != 0) {
				if (request.getParameter("callback") != null) {
					return request.getParameter("callback")
							+ "( [{\"responseCode\":\"Lead is Already Available in Selected Category.\"}])";
				} else {
					return "[{\"responseCode\":\"Lead is Already Available in Selected Category.\"}]";
				}
			} else {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date date = formatter.parse(data[6]);
				Lead lead = new Lead();
				lead.setcategoryId(data[5]);
				lead.setstaffId(0);
				lead.setCreateDate(new Date());
				lead.setemail(data[3].equalsIgnoreCase("") ? "N/A" : data[3]);
				lead.setFirstName(data[1].equalsIgnoreCase("") ? "N/A" : data[1]);
				lead.setLastName(data[2].equalsIgnoreCase("") ? "N/A" : data[2]);
				lead.setLeadType("WEB");
				lead.setLeadProcessStatus("None");
				lead.setMobileNo(data[4]);
				lead.setLeadState("Open");
				lead.setSheduleDate(df.parse("1980-06-30 17:36:00"));
				lead.setCompanyId(tcaler.getCompanyId());
				lead.setTallyCalletId(Integer.parseInt(data[12]));
				lead.setCountry(data[7]);
				lead.setState(data[8]);
				lead.setCity(data[9]);
				lead.setCompanyName(data[10]);
				lead.setWebsite(data[11]);
				lead.setLeadcomment("N/A");
				lead.setTagName("N/A");
				lead.setNotification_flag("FALSE");
				lead.setCsvData("N/A");
				lead.setAssignDate(new Date(1980, 06, 30, 17, 36, 00));
				adminService.AddLead(lead);
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "( [{\"responseCode\":\"Lead Successfully Added.\"}])";
				else
					return "[{\"responseCode\":\"Lead Successfully Added.\"}]";

			}

		} else {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\"responseCode\":\"INVALID_KEY\"}])";
			} else {
			}
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "( [{\"responseCode\":\"Lead Successfully Added.\"}])";
		else
			return "[{\"responseCode\":\"Lead Successfully Added.\"}]";
	}

	// http://localhost:8191/spring-hib/getAllLeadOfContactNoAPI?mobileno=9173186596&categotyId=demo&apikey=XXXXXX
	@RequestMapping(value = "getAllLeadOfContactNoAPI", produces = "application/javascript")
	public @ResponseBody String getAllLeadOfContactNoAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getAllLeadOfContactNoAPI is Called..");

		String ContactNo = request.getParameter("ContactNo");
		String tcId = request.getParameter("tid");
		String api = request.getParameter("apikey");

		Tallycaller telecaller = new Tallycaller();

		ApiKey apival = null;
		apival = adminService.ValidateAPI(api);
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		if (apival == null) {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
			} else {
				return "[{\'responseCode\':\'INVALID_KEY\'}]";
			}
		} else {
			List list = adminService.getAllLeadOfContactNo(ContactNo, tcId);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leaadId", result[0]);
					leadmap.put("firstName", result[1]);
					leadmap.put("mobileNo", result[2]);
					leadmap.put("leadState", result[3]);
					leadmap.put("email", result[4]);
					leadmap.put("categoryName", result[5]);
					leadmap.put("sheduleDate", result[6]);
					leadmap.put("autoDial", result[7]);
					leadmap.put("leadProcessStatus", result[8]);

					jarray.add(leadmap);
					/*
					 * tcallerWbJsom.put("data", jarray); System.out.println(tcallerWbJsom);
					 */
				}

			} else {
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

				// return request.getParameter("callback") +
				// "([{\"responceCode\":\"No Data Found\"}]);";
				// return "[{\"responceCode\":\"No Data Found\"}]";
			}

			// System.out.println(jarray.toJSONString());
		}
		/*
		 * telecaller = adminService.validateTelecallerDesktopAPI(API); if (telecaller
		 * == null) { if (request.getParameter("callback") != null) { return
		 * request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
		 * } else { return "[{\'responseCode\':\'INVALID_KEY\'}]"; } } else { // List
		 * list //
		 * =adminService.getAllLeadOfContactNo(ContactNo,String.valueOf(apival.getUid())
		 * ); List list = adminService.getAllLeadOfContactNo(ContactNo, tcId); if
		 * (list.size() != 0) { for (int i = 0; i < list.size(); i++) { JSONObject
		 * leadmap = new JSONObject(); Object[] result = (Object[]) list.get(i);
		 * leadmap.put("leaadId", result[0]); leadmap.put("firstName", result[1]);
		 * leadmap.put("mobileNo", result[2]); leadmap.put("leadState", result[3]);
		 * leadmap.put("email", result[4]); leadmap.put("categoryName", result[5]);
		 * leadmap.put("sheduleDate", result[6]); leadmap.put("autoDial", result[7]);
		 * 
		 * jarray.add(leadmap);
		 * 
		 * tcallerWbJsom.put("data", jarray); System.out.println(tcallerWbJsom);
		 * 
		 * }
		 * 
		 * } else { return request.getParameter("callback") + "(" +
		 * jarray.toJSONString() + ");";
		 * 
		 * // return request.getParameter("callback") + //
		 * "([{\"responceCode\":\"No Data Found\"}]);"; // return
		 * "[{\"responceCode\":\"No Data Found\"}]"; }
		 * 
		 * // System.out.println(jarray.toJSONString()); }
		 */
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return "";
		}

	}

	// http://localhost:8191/spring-hib/ValidateLeadAPI?mobileno=9173186596&categotyId=demo&apikey=XXXXXX
	@RequestMapping(value = "ValidateLeadAPI", produces = "application/javascript")
	public @ResponseBody String ValidateLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String mobile = request.getParameter("mobileno");
		String api = request.getParameter("apikey");
		String categotyId = request.getParameter("categotyId");
		System.out.println("ValidateLeadAPI is Called");
		ApiKey apival = null;
		apival = adminService.ValidateAPI(api);
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		if (apival == null) {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
			} else {
				return "[{\'responseCode\':\'INVALID_KEY\'}]";
			}
		} else {
			List list = adminService.ValidateLead(mobile, categotyId, apival.getUid());
			// System.out.println("List SIze : " + list.size());
			String len = "" + list.size();
			if (request.getParameter("callback") != null)
				return request.getParameter("callback") + " ( [{\'listSize\':\'" + len + "\'}])";
			else
				return "[{\'responseCode\':\'INVALID_REQUEST\'}]";

		}

	}

	// http://localhost:8191/spring-hib/APITelecallerLoginAPI?mobileno=9173186596&apikey=XXXXXX
	@RequestMapping(value = "APITelecallerLoginAPI", produces = "application/javascript")
	public @ResponseBody String APITelecallerLoginAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String mobile = request.getParameter("mobileno");
		String api = request.getParameter("apikey");
		System.out.println("APITelecallerLoginAPI is Called");
		Tallycaller user = new Tallycaller();
		user = adminService.validateDesktopAPI(api);
		// System.out.println(user);
		String name = null, category = "";
		String ret = "";
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		if (user == null) {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
			} else {
				return "[{\'responseCode\':\'INVALID_KEY\'}]";
			}
		} else {
			List list = adminService.getTeleCallerByUid(new Long(user.getTcallerid()));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("tcallerid", result[0]);
					leadmap.put("username", result[1]);
					leadmap.put("firstname", result[2]);
					leadmap.put("apikey", result[9]);
					jarray.add(leadmap);
					/*
					 * tcallerWbJsom.put("data", jarray); System.out.println(tcallerWbJsom);
					 */
				}

			} else
				return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/ValidateTelecallerAPI?mobileno=9173186596&apikey=XXXXXX
	@RequestMapping(value = "ValidateTelecallerAPI", produces = "application/javascript")
	public @ResponseBody String ValidateTelecallerAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String mobile = request.getParameter("mobileno");
		String api = request.getParameter("apikey");
		System.out.println("GetContactDetaailsAPI is Called");
		ApiKey apival = null;
		apival = adminService.ValidateAPI(api);
		// System.out.println(apival);
		String name = null, category = "";
		String ret = "";
		if (apival == null) {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
			} else {
				return "";
			}
		} else {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "( [{\'responseCode\':\'INVALID_KEY\'}])";
			} else {
				return "";
			}
		}
	}

	// http://localhost:8191/spring-hib/GetContactDetaailsAPI?mobileno=9173186596&apikey=XXXXXX
	@RequestMapping(value = "GetContactDetaailsAPI")
	public @ResponseBody String GetContactDetaailsAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String mobile = request.getParameter("mobileno");
		String api = request.getParameter("apikey");
		System.out.println("GetContactDetaailsAPI is Called");
		Tallycaller telecaller = new Tallycaller();
		telecaller = adminService.validateTelecallerDesktopAPI(api);
		// System.out.println(telecaller);
		String name = null, category = "";
		if (telecaller == null)
			return "Invalid API Key";
		else {
			List list = null;
			Object[] result = null;

			list = adminService.GetContactDetaails(telecaller.getCompanyId(), mobile);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {

					result = (Object[]) list.get(i);
					name = (String) result[0];
					category += (String) result[1] + ",";
					// System.out.println(result[0] + " " + result[1]);
				}
				// System.out.println(name + "(" + category + ")");
			} else
				return "No Lead Found";
		}
		return name + "(" + category + ")";
	}

	@RequestMapping(value = "/GetLeadDetailByMobileNoAPI")
	public @ResponseBody String GetLeadDetailByMobileNoAPI(HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		// GetLeadByLeadIdAPI
		System.out.println("GetLeadDetailByMobileNoAPI is called.....");
		String mobNo = request.getParameter("mobNo");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		/*
		 * if (apiKey != null) { apiKey =
		 * userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		 * 
		 * if (apiKey == null) { apiKey =
		 * userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false); if
		 * (apiKey == null) { return "[{\"responseCode\":\"INVALID_KEY\"}]"; } } }
		 */

		if (apiKey != null) {

			List list = adminService.GetLeadDetailByMobileNoAPI(mobNo);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					jarray.add(leadmap);
					tcallerWbJsom.put("data", jarray);
					// System.out.println(tcallerWbJsom);
				}

			} else
				return "[{\"responceCode\":\"No Data Found\"}]";

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/AddFailFollowUpWebAPI?tallycallerId=3&&mobNo=9173186596&&tempId=1,4&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "AddFailFollowUpWebAPI", produces = "application/javascript")
	public @ResponseBody String AddFailFollowUpWebAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("sendSMSAPI is Called..");

		final String tallycallerId = request.getParameter("tallycallerId");

		String api = request.getParameter("apikey");
		// String msg =URLEncoder.encode(request.getParameter("message"),
		// "UTF-8");
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			// System.out.println("My URL : ");
			asyncHttpClient.prepareGet("").execute(new AsyncCompletionHandler<Response>() {

				final Tallycaller tcaler1 = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));

				@Override
				public Response onCompleted(Response responce) throws Exception {
					// TODO Auto-generated method stub

					return responce;
				}
			});
		}
		return null;
	}

	// http://localhost:8191/spring-hib/AddLeadByBonrixWebResponceAPI?tallycallerId=3&&userName=Demo
	// Bonrix&&catName=Download Inquiry - SMPP Reseller
	// Inquiry&&firstName=bonric&&lastName=system&&email=bonrix@gmail.com&&companytName=aaa&&catName=Category7&&mobileNo=9426045500&webSite=www.abc.com&company=abbc
	// pvt
	// ltd&country=India&state=Gujarat&city=City&ocity=ocity&remark=dcsd&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "AddLeadByBonrixWebResponceAPI", produces = "application/javascript")
	public @ResponseBody String AddLeadByBonrixWebResponceAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("AddLeadByBonrixWebResponceAPIiscalled");
		String tallycallerid = request.getParameter("tallycallerId");
		String userName = request.getParameter("userName");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String companytName = "Bonrix Software System";
		String catId = request.getParameter("catId");
		String catName = request.getParameter("catName");
		String mobileNo = request.getParameter("mobileNo");
		String webSite = request.getParameter("webSite");
		String company = request.getParameter("company");
		String country = request.getParameter("country");
		String state = request.getParameter("state");
		String city = request.getParameter("city");
		String ocity = request.getParameter("ocity");
		String remark = request.getParameter("remark");
		String api = request.getParameter("apikey");
		String redirect = request.getParameter("redirect");

		/*
		 * System.out.println(request.getParameter("tallycallerId"));
		 * System.out.println(userName); System.out.println(firstName);
		 * System.out.println(lastName); System.out.println(email);
		 * System.out.println(companytName); System.out.println(catName);
		 * System.out.println(mobileNo); System.out.println(webSite);
		 * System.out.println(company); System.out.println(country);
		 * System.out.println(state); System.out.println(city);
		 * System.out.println(ocity); System.out.println(remark);
		 */

		User user = null;
		user = adminService.getUidByUserName(userName);
		String mapiKey = null;
		if (user == null) {
			return "[{\"responseCode\":\"INVALID_USER\"}]";
		}

		if (user != null) {
			mapiKey = user.getApiKey();
			// System.out.println(mapiKey);
			if (!mapiKey.equalsIgnoreCase(api)) {
				// System.out.println("In IF");
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}

		}
		String city1 = null;
		if (mapiKey != null) {
			if (city.equalsIgnoreCase("City")) {
				// System.out.println("NullisCalled");
				city1 = ocity;
			} else {
				// System.out.println("NullisnotCalled");
				city1 = city;
			}

			// List Catlist=adminService.getCategoryList(new Long(catId));
			// Object catObj=(Object[])Catlist.get(0);
			int ik = adminService.addLeadData(userName, firstName, lastName, email, companytName, catId, mobileNo,
					webSite, company, country, state, city1, remark);
			// System.out.println("RowCount:" + ik);
			if (ik != 0) {
				// System.out.println("InSMS");
				AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
				List list = null;
				String url1 = null;
				String message = "New Lead Assign To You.\nName : " + firstName + "\nEmail : " + email
						+ "\nContact No : " + mobileNo + "\nCategory : " + catName + "";
				final Tallycaller tcaler1 = adminService.GetTellyCallerById(Integer.parseInt(tallycallerid));

				list = adminService.GetAPI(tcaler1.getCompanyId());
				// System.out.println(list.size());
				for (int i = 0; i < list.size(); i++) {

					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// uid=(int) result[0];
					boolean status = (boolean) result[4];
					// System.out.println("STATUS:" + status);
					if (status) {
						url1 = (String) result[1];

						url1 = url1.replace("<mobileno>", tcaler1.getMobno() + "").replace("<message>",
								URLEncoder.encode(message));
						// System.out.println("Mobile : "+mob+" \n"+"MSG :
						// "+message);

						// System.out.println("SMSURL1:" + url1);

					}

				}
				String name = firstName + " " + lastName;
				CRMSendSMS sendsms = new CRMSendSMS();
				sendsms.SendWelcomeSMS(adminService, name, tcaler1.getCompanyId(), Integer.parseInt(catId), mobileNo,
						tallycallerid);
				if (url1 != null) {
					CRMSendSingleMail mail = new CRMSendSingleMail();
					mail.sendMail("<b>Hello Sajan....!</b>", 54);
					// final Tallycaller
					// tcaler1=adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
					final Sentsmslog log = new Sentsmslog();
					log.setTallycallerId(0);
					log.setStaffId(0);
					log.setCompayId(0);

					log.setMobileNo(Long.valueOf(tcaler1.getMobno()));
					log.setMessage(message);
					log.setSendDate(new Date());

					// System.out.println(log.getMobileNo()+"\n"+log.getMessage()+"\n"+log.getSendDate()+"\n"+log.getTallycallerId()+"\n"+log.getCompayId()+"\n"+log.getStaffId());
					// System.out.println("CallingURL:" + url1);
					asyncHttpClient.prepareGet(url1).execute(new AsyncCompletionHandler<Response>() {

						@Override
						public Response onCompleted(Response responce) throws Exception {
							// TODO Auto-generated method stub

							// System.out.println("Comp Id :
							// "+tcaler.getCompanyId());

							User user = new User();
							user = adminService.getUserByUid(Long.valueOf(tcaler1.getCompanyId()));

							List list = adminService.GetAPI(user.getCompanyId());
							String ststus = "";
							// System.out.println("List Size : "+list.size());
							int uid = 0;
							// System.out.println(list.size());
							for (int i = 0; i < list.size(); i++) {
								JSONObject leadmap = new JSONObject();
								Object[] result = (Object[]) list.get(i);
								uid = (int) result[0];
								boolean status = (boolean) result[4];
								if (status == true) {
									ststus = result[2].toString();
									// System.out.println("URL ID : "+uid);
								}
							}

							// System.out.println("Status : "+ststus);
							String res = responce.getResponseBody().replaceAll(" ", "");
							if (res.contains(ststus)) {
								// System.out.println("NoErrorinMSG");
								log.setMSGStatus("SUCCESS");
							} else {
								System.out.println("ErrorinMSG");

								log.setMSGStatus("FAIL");
							}

							// if(res.length()>255)
							// {
							// log.setAPIResponse(res.toString().substring(0,
							// 254));
							// }
							// else
							// {
							// log.setAPIResponse(res.toString());
							// }

							try {
								log.setAPIResponse(res);
								// System.out.println(log.getAPIResponse() +
								// "\n" + log.getMSGStatus());

								adminService.saveLog(log);

							} catch (Exception e) {
								// TODO Auto-generated catch block
								System.out.println("SaveError:" + e);
							}
							// System.out.println(responce.getResponseBody());
							return responce;
						}

						@Override
						public void onThrowable(Throwable t) {
							// Something wrong happened.
							t.printStackTrace();
							// System.out.println("=======" + t.getMessage());
							// System.out.println(statusCode+"::"+new Date());
							log.setAPIResponse(t.getMessage().substring(0, 100).trim());
							log.setMSGStatus("FAIL");
							adminService.saveLog(log);
						}

					});
				} else {
					return "SMS Url Not Generated.. " + url1;
				}

			} else {
				// System.out.println("Not in SMS");
				return "Lead not Successfully Inserted RowCount:" + ik;

			}

		}

		// String redirectUrl = request.getScheme() + "://www.yahoo.com";
		return "Lead Successfully Inserted";

	}

	// http://localhost:8191/spring-hib/AddLeadByBonrixWebAPI?tallycallerId=3&&userName=aaa&&firstName=bonric&&lastName=system&&email=bonrix@gmail.com&&companytName=aaa&&catName=Category7&&mobileNo=9426045500&webSite=www.abc.com&company=abbc
	// pvt ltd&country=India&state=Gujarat&city=Abd&remark=dcsd&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "AddLeadByBonrixWebAPI", method = RequestMethod.POST)

	public String AddLeadByBonrixWebAPI(HttpServletRequest request, HttpServletResponse response) throws Exception {

		System.out.println("AddLeadByBonrixWebAPI is called.....");
		String tallycallerid = request.getParameter("tallycallerId");
		String userName = request.getParameter("userName");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String companytName = "Bonrix Softeare System";
		String catName = request.getParameter("catName");
		String mobileNo = request.getParameter("mobileNo");
		String webSite = request.getParameter("webSite");
		String company = request.getParameter("company");
		String country = request.getParameter("country");
		String state = request.getParameter("state");
		String city = request.getParameter("city");
		String ocity = request.getParameter("ocity");
		String remark = request.getParameter("remark");
		String api = request.getParameter("apikey");
		String redirect = request.getParameter("redirect");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		String city1 = null;
		if (apiKey != null) {
			if (city.equalsIgnoreCase("City")) {
				// System.out.println("Null is Called");
				city1 = ocity;
			} else {
				// System.out.println("Null is not Called");
				city1 = city;
			}

			int ik = adminService.addLeadData(userName, firstName, lastName, email, companytName, catName, mobileNo,
					webSite, company, country, state, city1, remark);

		}

		// String redirectUrl = request.getScheme() + "://www.yahoo.com";
		return "redirect:" + redirect;

	}

	// http://localhost:8191/spring-hib/GetLeadByLeadIdAPI?leadId=3&&tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetLeadByLeadIdAPI", produces = "application/javascript")

	public @ResponseBody String GetLeadByLeadIdAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("GetLeadByLeadIdAPI is called.....");
		String leadId = request.getParameter("leadId");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {

			List list = adminService.GetLeadByLeadIdAPI(Integer.parseInt(leadId));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);
					leadmap.put("leadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tag", result[18]);
					leadmap.put("leadComment", result[19]);
					jarray.add(leadmap);
					tcallerWbJsom.put("data", jarray);
					// System.out.println(tcallerWbJsom);
				}

			} else
				return "[{\"responceCode\":\"No Data Found\"}]";

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/AddAllLeadToAutoDialAPI?tallycallerId=3&status=1&apikey=XXXXXX
	@RequestMapping(value = "AddAllLeadToAutoDialAPI")
	public @ResponseBody String AddAllLeadToAutoDialAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("AddAllLeadToAutoDialAPI is called.....");
		String tallycallerId = request.getParameter("tallycallerId");
		String statu1 = request.getParameter("status");
		String api = request.getParameter("apikey");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		String status = "";
		if (statu1.equalsIgnoreCase("true"))
			status = "1";
		else
			status = "0";

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {

			adminService.AddAllLeadToAutoDial(Integer.parseInt(tallycallerId), status);

		}
		if (status.equalsIgnoreCase("1"))
			return "[{\"responseCode\":\"All Leads are Successfully Added to AutoDial\"}]";
		else
			return "[{\"responseCode\":\"All Leads are Successfully Remove From AutoDial\"}]";
	}

	// localhost:8191/spring-hib/UpdateLeadDataAPI?tallycallerId=3&id=3&fname=bonrix&lname=demo&email=demo@gmail.com&mobno=9632587410&cat=cat1&sataus=Inprocess&compName=bonrix
	// PVT LTD&webAdd=www.bonrix.net&cont=India&stat=Guj&cty=deesa&apikey=XXXXXX
	@RequestMapping(value = "UpdateLeadDataAPI", produces = "application/javascript")
	public @ResponseBody String UpdateLeadDataAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("UpdateLeadDataAPI is called.....");
		int id = Integer.parseInt(request.getParameter("id"));
		String tallycallerId = request.getParameter("tallycallerId");
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String email = request.getParameter("email");
		String mobno = request.getParameter("mobno");
		String cat = request.getParameter("cat");
		String sataus = request.getParameter("sataus");
		String compName = request.getParameter("compName");
		String webAdd = request.getParameter("webAdd");
		String cont = request.getParameter("cont");
		String stat = request.getParameter("stat");
		String cty = request.getParameter("cty");
		String api = request.getParameter("apikey");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		// System.out.println("API : " + api + " " + "Tcaller : " +
		// tallycallerId);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {

			/*adminService.UpdateLead(id, fname, lname, email, mobno, cat, sataus, compName, webAdd, cont, stat, cty,
					Integer.parseInt(tallycallerId));*/

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCode\":\"Leads is Successfully Updated\"}]);";
		} else {
			return "[{\"responseCode\":\"Leads id Successfully Updated\"}]";
		}
	}

	// http://localhost:8191/spring-hib/GetAutoDialLeadAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetCatAutoDialLeadByIdAPI", produces = "application/javascript")

	public @ResponseBody String GetCatAutoDialLeadByIdAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("GetCatAutoDialLeadByIdAPI is called.....");
		String limit = request.getParameter("limit");
		String tallycallerId = request.getParameter("tallycallerId");
		String pageNo = request.getParameter("pageNo");
		String api = request.getParameter("apikey");
		int cat = Integer.parseInt(request.getParameter("catId"));
		int lid = Integer.parseInt(request.getParameter("lid"));

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {
			String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,lead.mobileNo, "
					+ " lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city "
					+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
					+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
					+ "    WHERE  tallycaller.tcallerid=" + Integer.parseInt(tallycallerId)
					+ "  AND autoDial=1 AND lead.categoryId=" + cat + " AND lead.leadState='Open' AND lead.leaadId="
					+ lid + " ORDER BY lead.leaadId LIMIT 1";
			List list = cservice.createSqlQuery(query);

			// List list =
			// adminService.GetCatAutoDialLead(Integer.parseInt(tallycallerId),cat);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					jarray.add(leadmap);
					tcallerWbJsom.put("data", jarray);
				}

			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";
			}
		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/GetAutoDialLeadAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetCatAutoDialLeadAPI", produces = "application/javascript")
	public @ResponseBody String GetCatAutoDialLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("GetAutoDialLeadAPI is called.....");
		String limit = request.getParameter("limit");
		String tallycallerId = request.getParameter("tallycallerId");
		String pageNo = request.getParameter("pageNo");
		String api = request.getParameter("apikey");
		int cat = Integer.parseInt(request.getParameter("catId"));
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {
			List list = adminService.GetCatAutoDialLead(Integer.parseInt(tallycallerId), cat);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					jarray.add(leadmap);
					tcallerWbJsom.put("data", jarray);
				}

			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";
			}
		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetCatAutoDialLeadAPIAndroid", produces = "application/javascript")
	public @ResponseBody String GetCatAutoDialLeadAPIAndroid(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("GetCatAutoDialLeadAPIAndro is called.....");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		int cat = Integer.parseInt(request.getParameter("catId"));

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {
			List list = adminService.GetAndroidCatAutoDialLead(Integer.parseInt(tallycallerId), cat);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tag", result[18] == null ? "N/A" : result[18]);
					leadmap.put("dialState", result[19]);
					jarray.add(leadmap);
					tcallerWbJsom.put("data", jarray);
				}
				return jarray.toJSONString();
			} else {
				return "[{\"responceCode\":\"No Data Found\"}]";
			}
		} else {
			return "[{\"responceCode\":\"INVALID_KEY\"}]";
		}
	}

	// http://localhost:8191/spring-hib/GetAutoDialLeadAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetAutoDialLeadAPI", produces = "application/javascript")

	public @ResponseBody String GetAutoDialLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("GetAutoDialLeadAPI is called.....");
		String limit = request.getParameter("limit");
		String tallycallerId = request.getParameter("tallycallerId");
		String pageNo = request.getParameter("pageNo");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {

			List list = adminService.GetAutoDialLead(Integer.parseInt(tallycallerId));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);
					leadmap.put("leadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					jarray.add(leadmap);
					tcallerWbJsom.put("data", jarray);
					// System.out.println(tcallerWbJsom);
				}

			} else {
				// return "[{\"responceCode\":\"No Data Found\"}]";
				// return request.getParameter("callback") +
				// "([{\"responseCode\":\"NO Data Found\"}]);";
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";

			}

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/GetPaggingAutoDialLeadAPI?tallycallerId=3&&limit=3&pageNo=1&apikey=XXXXXX
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @RequestMapping(value = "GetPaggingAutoDialLeadAPI") public @ResponseBody
	 * String GetPaggingAutoDialLeadAPI(HttpServletRequest request,
	 * HttpServletResponse response) throws Exception {
	 * System.out.println("GetPaggingAutoDialLeadAPI is called....."); String limit
	 * = request.getParameter("limit"); String tallycallerId =
	 * request.getParameter("tallycallerId"); String pageNo =
	 * request.getParameter("pageNo"); String api = request.getParameter("apikey");
	 * org.json.simple.JSONArray jarray = new org.json.simple.JSONArray(); ApiKey
	 * apiKey = null; apiKey =
	 * userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
	 * 
	 * if (apiKey == null) { apiKey =
	 * userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false); if
	 * (apiKey == null) { return "[{\"responseCode\":\"INVALID_KEY\"}]"; } }
	 * 
	 * if (apiKey != null) { apiKey =
	 * userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
	 * 
	 * if (apiKey == null) { apiKey =
	 * userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false); if
	 * (apiKey == null) { return "[{\"responseCode\":\"INVALID_KEY\"}]"; } } }
	 * 
	 * if (apiKey != null) {
	 * 
	 * List list =
	 * adminService.GetPaggingAutoDialLead(Integer.parseInt(tallycallerId),
	 * Integer.parseInt(limit), Integer.parseInt(pageNo)); if (list.size() != 0) {
	 * for (int i = 0; i < list.size(); i++) { JSONObject leadmap = new
	 * JSONObject(); Object[] result = (Object[]) list.get(i); //
	 * System.out.println("My : "+result[0]+" "+result[1]+" // "+result[2]+"
	 * "+result[3]+" "+result[4]+" "+result[5]+" // "+result[6]+" "+result[7]+"
	 * "+result[8]+" "+result[9]+" // "+result[10]+" "+result[11]+"
	 * "+result[12]); leadmap.put("categoryName", result[1]);
	 * leadmap.put("createDate", result[2]); leadmap.put("email", result[3]);
	 * leadmap.put("firstName", result[4]); leadmap.put("lastName", result[5]);
	 * leadmap.put("leadProcessStatus", result[6]); leadmap.put("leadState",
	 * result[7]); leadmap.put("mobileNo", result[8]); leadmap.put("sheduleDate",
	 * result[9] == null ? "N/A" : result[9]); leadmap.put("csvData", result[10] ==
	 * null ? "N/A" : result[10]); leadmap.put("leadType", result[11]);
	 * jarray.add(leadmap); }
	 * 
	 * } else return "[{\"responceCode\":\"No Data Found\"}]";
	 * 
	 * } return jarray.toJSONString(); }
	 */

	// http://localhost:8191/spring-hib/AddLeadToAutoDialAPI?tallycallerId=3&&leadId=2,3,4&status=1&apikey=XXXXXX
	@RequestMapping(value = "addLeadsToAutoDialAPI", produces = "application/javascript")
	public @ResponseBody String addLeadsToAutoDialAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addLeadsToAutoDialAPI is called.....");
		String tallycallerId = request.getParameter("tallycallerId");
		String categoryId = request.getParameter("categoryId");
		String status = request.getParameter("status");
		String api = request.getParameter("apikey");

		ApiKey apiKey = null;
		apiKey = adminService.ValidateAPI(api);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return request.getParameter("callback") + "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		String s = null;
		int autoDialVal = Integer.parseInt(status);
		if (apiKey != null) {
			if (tallycallerId.equalsIgnoreCase("0")) {

				adminService.AddcategoryLeadToAutoDial(Integer.parseInt(categoryId), autoDialVal);

			} else {
				adminService.AddTelecallrLeadToAutoDial(Integer.parseInt(tallycallerId), status);

			}

		}

		if (request.getParameter("callback") != null)
			return request.getParameter("callback")
					+ "([{\"responseCode\":\"Leads Successfully Added to AutoDial\"}]);";
		else {
			return "[{\"responseCode\":\"Leads Successfully Remove From AutoDial\"}]";
		}
	}

	// http://localhost:8191/spring-hib/AddLeadToAutoDialAPI?tallycallerId=3&&leadId=2,3,4&status=1&apikey=XXXXXX
	@RequestMapping(value = "AddLeadToAutoDialAPI", produces = "application/javascript")
	public @ResponseBody String AddAutoDialLead(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("AddLeadToAutoDialAPI is called.....");
		String leadId = request.getParameter("leadId");
		String tallycallerId = request.getParameter("tallycallerId");
		String statu1 = request.getParameter("status");
		String status = "";
		String api = request.getParameter("apikey");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (statu1.equalsIgnoreCase("true"))
			status = "1";
		else
			status = "0";
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		int rowcont = 0;
		if (apiKey != null) {
			// rowcont=adminService.AddLeadToAutoDial(leadid);
			String[] lid = leadId.split(",");
			for (int i = 0; i < lid.length; i++) {
				// System.out.println(lid[i]);
				adminService.AddLeadToAutoDial(Integer.parseInt(lid[i]), status);
			}

		}

		if (request.getParameter("callback") != null) {
			if (status.equalsIgnoreCase("1"))
				return request.getParameter("callback")
						+ "([{\"responseCode\":\"Leads Successfully Added to AutoDial\"}]);";
			else
				return request.getParameter("callback")
						+ "([{\"responseCode\":\"Leads Successfully Remove From AutoDial\"}]);";
		} else {
			if (status.equalsIgnoreCase("1"))
				return "[{\"responseCode\":\"Leads Successfully Added to AutoDial\"}]";

			else
				return "[{\"responseCode\":\"Leads Successfully Remove From AutoDial\"}]";

		}

	}

	// http://localhost:8191/spring-hib/GetSuccessStatusAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetSuccessStatusAPI", produces = "application/javascript")
	public @ResponseBody String GetSuccessStatusAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("GetSuccessStatusAPI is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {
			List list = adminService.GetSuccessStatusAPI(Integer.parseInt(tallycallerId));
			for (int i = 0; i < list.size(); i++) {
				JSONObject leadmap = new JSONObject();
				// Object[] result =(Object[]) list.get(i);
				leadmap.put("State_Name", list.get(i));
				jarray.add(leadmap);
			}

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getSuccessStatByleadstat", produces = "application/javascript")
	public @ResponseBody String getSuccessStatByleadstat(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "leadStatId") String leadStatId) throws Exception {

		List<Object[]> getdetails = adminService.getLeadSuccessStatByLeadStat(leadStatId);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		for (Object[] result : getdetails) {
			JSONObject DataObj = new JSONObject();
			DataObj.put("id", result[0].toString());
			DataObj.put("statusName", result[1].toString());
			jarray.add(DataObj);
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "AddFailFollowUpAPI", produces = "application/javascript")
	public @ResponseBody String AddFailFollowUpAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("AddFailFollowUpAPI is Called..  " + request.getParameter("reqfollowupTime"));
		String callingTime = request.getParameter("callingTime");
		int tallycallerId = Integer.parseInt(request.getParameter("tallycallerId"));
		String newStatus = request.getParameter("newStatus");
		String callStatus = request.getParameter("callStatus");
		String sheduleTime = request.getParameter("sheduleTime");
		String followupType = request.getParameter("followupType");
		String lid = request.getParameter("leadId");
		String api = request.getParameter("apikey");
		String fid = request.getParameter("fid");
		String reqfollowupTime = request.getParameter("reqfollowupTime");

		System.out.println("FID :: " + fid);
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tallycallerId, api, true);

		if (apiKey == null) {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}

		int second = 0;
		if (reqfollowupTime == null)
			second = 0;
		else
			second = Integer.parseInt(reqfollowupTime);

		// if (fid != null || !fid.equalsIgnoreCase("undefined")) {
		if (!fid.equalsIgnoreCase("undefined") && fid != null && fid != "null") {
			Date sdate = df.parse(sheduleTime);

			String uquery = "UPDATE followupshistory SET remark='N/A',sheduleTime='" + df4.format(sdate) + "'"
					+ ",newStatus='N/A',followsUpType='" + followupType + "',successStatus='None',failstatus='"
					+ newStatus + "',followAddSecond=" + Math.abs(second - 60)
					+ ",followupDevice='WEB' WHERE followupsId=" + fid;

			int i = adminService.updateSheduleDate(Integer.parseInt(lid), sheduleTime);
			cservice.createupdateSqlQuery(uquery);
			cservice.createupdateSqlQuery(
					"UPDATE lead SET lead.dialState=1 WHERE lead.leaadId=" + Integer.parseInt(lid) + "");
			if (request.getParameter("callback") != null)
				return request.getParameter("callback") + "([{\"responseCode\":\"Followups Successfully Added\"}]);";
			else
				return "[{\"responseCode\":\"Followups Successfully Added\"}]";

		} else {
			// System.out.println("reqfollowupTime : " + reqfollowupTime);

			List list = adminService.getTeleCallerByUid(tallycallerId);
			Object[] result = (Object[]) list.get(0);
			Date cdate = df.parse(callingTime);
			Date sdate = df.parse(sheduleTime);
			Followupshistory history = new Followupshistory();
			history.setTallycallerId(tallycallerId);
			history.setAudoiFilePath("N/A");
			history.setCallDuration(00);
			history.setCallingTime(cdate);
			history.setCallStatus("Fail");
			history.setNewStatus("N/A");
			history.setFailstatus(newStatus);
			history.setRemark("N/A");
			history.setSheduleTime(sdate);
			history.setLeadId(Integer.parseInt(lid));
			history.setCompanyId(Integer.parseInt("" + result[7]));
			history.setFollowsUpType(followupType);
			history.setFollowAddSecond(Math.abs(second - 60));
			adminService.AddFailFollowUpAPI(history);
			cservice.createupdateSqlQuery(
					"UPDATE lead SET lead.dialState=1 WHERE lead.leaadId=" + Integer.parseInt(lid) + "");
			int i = adminService.updateSheduleDate(Integer.parseInt(lid), sheduleTime);
		}

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Followups Successfully Added\"}]);";
		else
			return "[{\"responseCode\":\"Followups Successfully Added\"}]";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "AddFailFollowUpDesktopAPI", produces = "application/javascript")
	public @ResponseBody String AddFailFollowUpDesktopAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("AddFailFollowUpDesktopAPI is Called..  " + request.getParameter("reqfollowupTime"));
		String callingTime = request.getParameter("callingTime");
		int tallycallerId = Integer.parseInt(request.getParameter("tallycallerId"));
		String newStatus = request.getParameter("newStatus");
		String callStatus = request.getParameter("callStatus");
		String sheduleTime = request.getParameter("sheduleTime");
		String followupType = request.getParameter("followupType");
		String lid = request.getParameter("leadId");
		String api = request.getParameter("apikey");
		String reqfollowupTime = request.getParameter("reqfollowupTime");

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tallycallerId, api, true);

		if (apiKey == null) {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}

		int second = 0;
		if (reqfollowupTime == null)
			second = 0;
		else
			second = Integer.parseInt(reqfollowupTime);

		// System.out.println("reqfollowupTime : " + reqfollowupTime);

		List list = adminService.getTeleCallerByUid(tallycallerId);
		Object[] result = (Object[]) list.get(0);
		Date cdate = df.parse(callingTime);
		Date sdate = df.parse(sheduleTime);
		Followupshistory history = new Followupshistory();
		history.setTallycallerId(tallycallerId);
		history.setAudoiFilePath("N/A");
		history.setCallDuration(00);
		history.setCallingTime(cdate);
		history.setCallStatus("Fail");
		history.setNewStatus("N/A");
		history.setFailstatus(newStatus);
		history.setRemark("N/A");
		history.setSheduleTime(sdate);
		history.setLeadId(Integer.parseInt(lid));
		history.setCompanyId(Integer.parseInt("" + result[7]));
		history.setFollowsUpType(followupType);
		history.setFollowAddSecond(Math.abs(second - 60));
		adminService.AddFailFollowUpAPI(history);

		int i = adminService.updateSheduleDate(Integer.parseInt(lid), sheduleTime);
		// System.out.println("UPdate Shedule LEad: " + i);

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Followups Successfully Added\"}]);";
		else
			return "[{\"responseCode\":\"Followups Successfully Added\"}]";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "saveCloseFailFollowUpDesktopAPI", produces = "application/javascript")
	public @ResponseBody String saveCloseFailFollowUpDesktopAPI(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		System.out.println("saveCloseFailFollowUpDesktopAPI is Called..  " + request.getParameter("reqfollowupTime"));
		String callingTime = "0";
		int tallycallerId = Integer.parseInt(request.getParameter("tallycallerId"));
		String newStatus = request.getParameter("Failstatus");
		String sheduleTime = request.getParameter("sheduleTime");
		String lid = request.getParameter("leadId");
		String api = request.getParameter("apikey");
		String reqfollowupTime = "60";

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tallycallerId, api, true);

		if (apiKey == null) {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}

		List list = adminService.getTeleCallerByUid(tallycallerId);
		Object[] result = (Object[]) list.get(0);
		Date cdate = new Date();
		Date sdate = df.parse(sheduleTime);
		Followupshistory history = new Followupshistory();
		history.setTallycallerId(tallycallerId);
		history.setAudoiFilePath("N/A");
		history.setCallDuration(00);
		history.setCallingTime(cdate);
		history.setCallStatus("Fail");
		history.setNewStatus("N/A");
		history.setFailstatus(newStatus);
		history.setRemark("N/A");
		history.setSheduleTime(sdate);
		history.setLeadId(Integer.parseInt(lid));
		history.setCompanyId(Integer.parseInt("" + result[7]));
		history.setFollowsUpType("OutBound");
		history.setFollowupDevice("WEB");
		history.setSuccessStatus("None");
		history.setFollowAddSecond(0);
		adminService.AddFailFollowUpAPI(history);

		int i = adminService.updateSheduleDate(Integer.parseInt(lid), sheduleTime);
		// System.out.println("UPdate Shedule LEad: " + i);

		/*
		 * if (api != null) { adminService.CloseLead(Integer.parseInt(lid)); }
		 */
		adminService.CloseLead(Integer.parseInt(lid));

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Followups Successfully Added\"}]);";
		else
			return "[{\"responseCode\":\"Followups Successfully Added\"}]";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "saveCloseFailFollowUpAndroAPI", produces = "application/javascript")
	public @ResponseBody String saveCloseFailFollowUpAndroAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String callingTime = "0";
		int tallycallerId = Integer.parseInt(request.getParameter("tallycallerId"));
		String newStatus = request.getParameter("Failstatus");
		String sheduleTime = request.getParameter("sheduleTime");
		String lid = request.getParameter("leadId");
		String api = request.getParameter("apikey");
		String reqfollowupTime = "60";

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tallycallerId, api, true);

		if (apiKey == null) {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}
		List list = adminService.getTeleCallerByUid(tallycallerId);
		Object[] result = (Object[]) list.get(0);
		Date cdate = new Date();
		Date sdate = df.parse(sheduleTime);
		Followupshistory history = new Followupshistory();
		history.setTallycallerId(tallycallerId);
		history.setAudoiFilePath("N/A");
		history.setCallDuration(00);
		history.setCallingTime(cdate);
		history.setCallStatus("Fail");
		history.setNewStatus("N/A");
		history.setFailstatus(newStatus);
		history.setRemark("N/A");
		history.setSheduleTime(sdate);
		history.setLeadId(Integer.parseInt(lid));
		history.setCompanyId(Integer.parseInt("" + result[7]));
		history.setFollowsUpType("OutBound");
		history.setFollowupDevice("ANDRO_APP");
		history.setSuccessStatus("None");
		history.setFollowAddSecond(0);
		adminService.AddFailFollowUpAPI(history);

		int i = adminService.updateSheduleDate(Integer.parseInt(lid), sheduleTime);
		adminService.CloseLead(Integer.parseInt(lid));

		return "[{\"responseCode\":\"Followups Successfully Added\"}]";
	}

	// http://localhost:8191/spring-hib/GetFailStatusAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetFailStatusAPI", produces = "application/javascript")
	public @ResponseBody String GetFailStatusAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("GetFailStatusAPI is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			List list = adminService.GetCloseStatusAPI(Integer.parseInt(tallycallerId));
			for (int i = 0; i < list.size(); i++) {
				JSONObject leadmap = new JSONObject();
				// Object[] result =(Object[]) list.get(i);
				leadmap.put("State_Name", list.get(i));
				jarray.add(leadmap);
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/CloseLeadAPI?tallycallerId=3&&leadId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "CloseLeadAPI", produces = "application/javascript")
	public @ResponseBody String CloseLead(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("CloseLeadAPI is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String leadId = request.getParameter("leadId");
		String api = request.getParameter("apikey");

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			adminService.CloseLead(Integer.parseInt(leadId));
		}

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Lead Successfully Closed\"}]);";
		else
			return "[{\"responseCode\":\"Lead Successfully Closed\"}]";
	}

	// http://localhost:8191/spring-hib/GetLeadProcessState?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetLeadProcessState", produces = "application/javascript")
	public @ResponseBody String GetLeadProcessState(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("GetLeadProcessState is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		Tallycaller tcaller = new Tallycaller();
		tcaller = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
		// System.out.println(tcaller.getCompanyId());
		if (api != null) {
			List list = adminService.getLeadProcessState(tcaller.getCompanyId());
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);
					leadmap.put("Id", result[0]);
					leadmap.put("State_Name", result[1]);

					jarray.add(leadmap);
				}

			} else
				return "[{\"responceCode\":\"No Data Found\"}]";

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/sendSMSTemplateAPI?tallycallerId=3&&mobNo=9173186596&&tempId=1,4&&apikey=XXXXXX

//	@Async
	@RequestMapping(value = "sendSMSTemplateAPI")
	public @ResponseBody String sendSMSTemplateAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("sendSMSAPI is Called..");
		String mobNo = request.getParameter("mobNo");
		final String tallycallerId = request.getParameter("tallycallerId");
		String id = request.getParameter("tempId");
		String api = request.getParameter("apikey");

		// String msg =URLEncoder.encode(request.getParameter("message"),
		// "UTF-8");
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		// List l=adminService.getTemplateByTemplateId(id);
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			/*
			 * Tallycaller tcaler=new Tallycaller();
			 * tcaler=adminService.GetTellyCallerById(Integer.parseInt( tallycallerId));
			 */

			List list = null;
			// System.out.println("Id : " + id);
			List list1 = adminService.getTemplateByTemplateId(id);
			if (list1.size() != 0) {
				String url1 = null;

				Tallycaller tcaler = new Tallycaller();
				tcaler = (Tallycaller) adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
				int uid = 0;
				// System.out.println("Company Id here....!!!!");
				list = adminService.GetAPI(tcaler.getCompanyId());
				// System.out.println(list.size());
				for (int i = 0; i < list.size(); i++) {

					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					uid = (int) result[0];
					boolean status = (boolean) result[4];
					// System.out.println("STATUS : "+status);
					if (status) {
						url1 = (String) result[1];
						url1 = url1.replace("<mobileno>", mobNo);
					}
				}
				String temp = url1;
				for (int i = 0; i < list1.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list1.get(i);
					url1 = temp;
					System.out.println("Temp URL : " + temp);
					url1 = url1.replace("<message>", result[2].toString());
					final Sentsmslog log = new Sentsmslog();

					log.setMobileNo(Long.parseLong(mobNo));
					log.setMessage(result[2].toString());
					log.setSendDate(new Date());
					log.setTallycallerId(Integer.parseInt(tallycallerId));
					log.setCompayId(tcaler.getCompanyId());
					log.setStaffId(0);

					System.out.println("My URL : " + url1);
					asyncHttpClient.prepareGet(url1).execute(new AsyncCompletionHandler<Response>() {

						final Tallycaller tcaler1 = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));

						@Override
						public Response onCompleted(Response responce) throws Exception {
							// TODO Auto-generated method stub
							List list = adminService.GetAPI(tcaler1.getCompanyId());
							String ststus = "";
							// System.out.println("List Size : " + list.size());
							int uid = 0;
							// System.out.println(list.size());
							for (int i = 0; i < list.size(); i++) {
								JSONObject leadmap = new JSONObject();
								Object[] result = (Object[]) list.get(i);
								uid = (int) result[0];
								boolean status = (boolean) result[4];
								if (status == true) {
									ststus = result[2].toString();
									// System.out.println("URL ID : " + uid);
								}
							}

							String res = responce.getResponseBody();
							if (res.contains(ststus) && !res.contains("invalidnumber") && !res.contains("INVALID_USER")
									&& !res.contains("INVALID_KEY")) {
								// System.out.println("No Error in MSG");
								log.setMSGStatus("SUCCESS");
							} else {
								// System.out.println("Error in MSG");
								log.setMSGStatus("FAIL");
							}

							if (res.length() > 255) {
								// System.out.println("SUBSTR : " +
								// res.toString().substring(0, 250));
								log.setAPIResponse(res.toString().substring(0, 250));
							} else {
								log.setAPIResponse(res.toString());
							}
							adminService.saveLog(log);
							// System.out.println(responce.getResponseBody());
							return responce;
						}

						@Override
						public void onThrowable(Throwable t) {
							// Something wrong happened.
							t.printStackTrace();
							// System.out.println("=======" + t.getMessage());
							// System.out.println(statusCode+"::"+new Date());
							log.setAPIResponse(t.getMessage().substring(0, 100));
							log.setMSGStatus("FAIL");
							adminService.saveLog(log);
						}

					});

					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);

					/*
					 * leadmap.put("id",result[0]); leadmap.put("Temp_Name",result[1]);
					 * leadmap.put("Temp_Message",result[2]); jarray.add(leadmap);
					 */
				}
				return "[{\"responseCode\":\"Message Successfully Send\"}]";
			} else {
				if (request.getParameter("callback") != null) {
					return request.getParameter("callback") + "({\"responseCode\":\"No Data Found\"});";
				} else {
					return "[{\"responseCode\":\"No Data Found\"}]";
				}
			}

		}

		// return "[{\"responseCode\":\"Message Request Send Successfully\"}]";
		// return "Size : "+l.size();

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Message Request Send Successfully\"}]);";
		else
			return "[{\"responseCode\":\"Message Request Send Successfully\"}]";

	}

	// http://localhost:8191/spring-hib/sendSMSAPI?tallycallerId=3&&mobNo=9173186596&&message=Welcome
	// Bonrix&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "sendSMSAPI")
	public @ResponseBody String sendSMSAPI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

		System.out.println("sendSMSAPI is Called.. SAJAN");
		long mob = Long.parseLong(request.getParameter("mobNo"));
		String message = request.getParameter("message");
		String tallycallerId = request.getParameter("tallycallerId");
		// System.out.println("mob : " + mob);
		String msg = request.getParameter("msg");
		String api = request.getParameter("apikey");

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {
			List list = null;
			String url1 = null;

			Tallycaller tcaler = new Tallycaller();
			tcaler = (Tallycaller) adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
			int uid = 0;
			// System.out.println("Company Id here....!!!!");
			list = adminService.GetAPI(tcaler.getCompanyId());
			// System.out.println(list.size());
			for (int i = 0; i < list.size(); i++) {

				JSONObject leadmap = new JSONObject();
				Object[] result = (Object[]) list.get(i);
				uid = (int) result[0];
				boolean status = (boolean) result[4];
				// System.out.println("STATUS : " + status);
				if (status) {
					url1 = (String) result[1];
					url1 = url1.replace("<mobileno>", mob + "").replace("<message>", URLEncoder.encode(message));
				}
			}

			if (url1 != null) {

				final Tallycaller tcaler1 = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));

				final Sentsmslog log = new Sentsmslog();
				log.setMobileNo(mob);
				log.setMessage(msg);
				log.setSendDate(new Date());
				log.setTallycallerId(Integer.parseInt(tallycallerId));
				log.setStaffId(0);
				log.setCompayId(0);
				// System.out.println(log.getMobileNo()+"\n"+log.getMessage()+"\n"+log.getSendDate()+"\n"+log.getTallycallerId()+"\n"+log.getCompayId()+"\n"+log.getStaffId());
				System.out.println("Calling URL:" + url1);
				asyncHttpClient.prepareGet(url1).execute(new AsyncCompletionHandler<Response>() {

					@Override
					public Response onCompleted(Response responce) throws Exception {
						// TODO Auto-generated method stub

						// System.out.println("Comp Id :
						// "+tcaler.getCompanyId());
						List list = adminService.GetAPI(tcaler1.getCompanyId());
						String ststus = "";
						// System.out.println("List Size : " + list.size());
						int uid = 0;
						// System.out.println(list.size());
						for (int i = 0; i < list.size(); i++) {
							JSONObject leadmap = new JSONObject();
							Object[] result = (Object[]) list.get(i);
							uid = (int) result[0];
							boolean status = (boolean) result[4];
							if (status == true) {
								ststus = result[2].toString();
								// System.out.println("URL ID : " + uid);
							}
						}

						/*
						 * Smssettings settings=new Smssettings();
						 * settings=adminService.getSMSSettings(uid)
						 */;
						String res = responce.getResponseBody();
						// .api.String res = responce.getResponseBody().replaceAll(" ", "");
						if (res.contains(ststus)) {
							// System.out.println("No Error in MSG");
							log.setMSGStatus("SUCCESS");
						} else {
							// System.out.println("Error in MSG");

							log.setMSGStatus("FAIL");
						}

						// if(res.length()>255)
						// {
						// log.setAPIResponse(res.toString().substring(0, 254));
						// }
						// else
						// {
						// log.setAPIResponse(res.toString());
						// }

						try {
							log.setAPIResponse(res);
							// System.out.println(log.getAPIResponse()+"\n"+log.getMSGStatus());

							adminService.saveLog(log);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							// System.out.println("Save Error : " + e);
						}
						// System.out.println(responce.getResponseBody());
						return responce;
					}

					@Override
					public void onThrowable(Throwable t) {
						// Something wrong happened.
						t.printStackTrace();
						// System.out.println("=======" + t.getMessage());
						// System.out.println(statusCode+"::"+new Date());
						log.setAPIResponse(t.getMessage().substring(0, 100).trim());
						log.setMSGStatus("FAIL");
						adminService.saveLog(log);
					}

				});
			}

		}

		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Message Request Send Successfully\"}]);";
		else
			return "[{\"responseCode\":\"Message Request Send Successfully\"}]";
	}

	// http://localhost:8191/spring-hib/getTemplateAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getTemplateAPI", produces = "application/javascript")
	public @ResponseBody String getTemplateAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getTemplateAPI is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			Tallycaller tcaler = new Tallycaller();
			tcaler = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
			List list = adminService.getTemplateAPI(tcaler);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : "+result[0]+" "+result[1]+"
					// "+result[2]+" "+result[3]+" "+result[4]+" "+result[5]+"
					// "+result[6]+" "+result[7]+" "+result[8]+" "+result[9]+"
					// "+result[10]+" "+result[11]+" "+result[12]);
					leadmap.put("id", result[0]);
					leadmap.put("tempName", result[1]);
					leadmap.put("message", result[2]);
					leadmap.put("status", result[3]);

					jarray.add(leadmap);
				}

			} else {
				if (request.getParameter("callback") != null) {
					return request.getParameter("callback") + "([{\"responceCode\":\"No Data Found\"}]);";
				} else {
					return "[{\"responceCode\":\"No Data Found\"}]";
				}
			}

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/?tallycallerId=3&&uname=aaa&&cname=aaa&&email=aaa@aaa.com&&cno=4569871256&apikey=XXXXXX
	@SuppressWarnings("unused")
	@RequestMapping(value = "/addContactInfoAPI")
	public @ResponseBody String addContactInfoAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "uname") String uname, @RequestParam(value = "cname") String cname,
			@RequestParam(value = "email") String email, @RequestParam(value = "cno") String cno,
			@RequestParam(value = "apikey") String api, @RequestParam(value = "tallycallerId") String tallycallerId)
			throws ParseException {
		System.out.println("addContactInfo is called...");

		Tallycaller tcaller = new Tallycaller();
		tcaller = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));

		List list = null;
		list = adminService.getContactInfoByEmail(email);
		// System.out.println(list.size());
		if (list.size() != 0)
			return "[{\"responseCode\":\"Email Id Allready exists\"}]";

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null && list.size() == 0) {
			adminService.addContactInfo(cname, email, cno, tcaller.getCompanyId());
			return "[{\"responseCode\":\"Data Successfully Added\"}]";
		} else
			return "[{\"responseCode\":\"Error in Data\"}]";

	}

	// http://localhost:8191/spring-hib/remainLeadAPI?tallycallerId=3&&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "remainLeadAPI")
	public @ResponseBody String remainLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("remainLead is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			List list = adminService.remainLead(Integer.parseInt(tallycallerId));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("name", result[0]);
					leadmap.put("mobno", result[1]);
					leadmap.put("sheduleDate", result[2]);

					jarray.add(leadmap);

				}

			} else
				return "[{\"responceCode\":\"No Data Found\"}]";

		}

		return jarray.toJSONString();
	}

	// http://localhost:8191/spring-hib/addLeadDataAPI?tallycallerId=3&&userName=aaa&&firstName=bonric&&lastName=system&&email=bonrix@gmail.com&&companytName=aaa&&catName=Category7&&mobileNo=9426045500&webSite=www.abc.com&company=abbc
	// pvt ltd&country=India&state=Gujarat&city=Abd&apikey=XXXXXX
	@RequestMapping(value = "addLeadDataAPI")
	public @ResponseBody String addLeadDataAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addLeadData is Called..");
		String tallycallerid = request.getParameter("tallycallerId");
		String userName = request.getParameter("userName");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String companytName = request.getParameter("companytName");
		String catName = request.getParameter("catName");
		String mobileNo = request.getParameter("mobileNo");
		String webSite = request.getParameter("webSite");
		String company = request.getParameter("company");
		String country = request.getParameter("country");
		String state = request.getParameter("state");
		String city = request.getParameter("city");
		String api = request.getParameter("apikey");
		String remark = request.getParameter("remark");
		ApiKey apiKey = null;

		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null)
			adminService.addLeadData(userName, firstName, lastName, email, companytName, catName, mobileNo, webSite,
					company, country, state, city, remark);

		return "[{\"responseCode\":\"Data Successfully Added\"}]";

	}

	/* Used or Outgoing Calls */
	// http://localhost:8191/spring-hib/getLeadByContactNoAPI?ContactNo=9426045500
	@RequestMapping(value = "getLeadByContactNoAPI")
	public @ResponseBody String getLeadByContactNoAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getLeadByContactNo is Called..");

		String ContactNo = request.getParameter("ContactNo");

		List list = adminService.getLeadByContactNo(ContactNo);
		DatatableJsonObject djo = new DatatableJsonObject();

		djo.setRecordsFiltered(list.size());
		djo.setRecordsTotal(list.size());
		djo.setData(list);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsons = gson.toJson(djo);
		return jsons;

	}

	// http://localhost:8191/spring-hib/getPaggingLeadByCategoryAPI?tallycallerId=3&&catname=Category7&&limt=3&&pageNo=1&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getPaggingLeadByCategoryAPI")
	public @ResponseBody String getPaggingLeadByCategoryAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getPaggingLead is Called..");
		String tallycallerid = request.getParameter("tallycallerId");
		String catname = request.getParameter("catname");
		String limt = request.getParameter("limt");
		String pageNo = request.getParameter("pageNo");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;

		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerid), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			List list = adminService.getPaggingLeadByCategory(tallycallerid, catname, Integer.parseInt(limt),
					Integer.parseInt(pageNo));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					jarray.add(leadmap);
				}

			} else
				return "[{\"responceCode\":\"No Data Found\"}]";

		}
		return jarray.toJSONString();
	}

	// http://localhost:8191/spring-hib/getPaggingLeadAPI?tallycaller=3&&limit=3&&pageNo=1&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getPaggingLeadAPI")
	public @ResponseBody String getPaggingLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getPaggingLead is Called..");
		String tallycaller = request.getParameter("tallycaller");
		String limit = request.getParameter("limit");
		String pageNo = request.getParameter("pageNo");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;

		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycaller), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycaller), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		if (api != null) {
			List list = adminService.getPaggingLead(tallycaller, Integer.parseInt(limit), Integer.parseInt(pageNo));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					jarray.add(leadmap);
				}

			} else
				return "[{\"responceCode\":\"No Data Found\"}]";
		}
		return jarray.toJSONString();
	}

	// http://localhost:8191/spring-hib/addAndCloseLeadFollowupsHistoryAPI?tallycalleName=Bonrix&&callingTime=20150907124644&&remark=none&&sheduleTime=20150907124644&&newStatus=gstste&&audoiFilePath=messagebox.mp3&&callDuration=5&&callStatus=success&&leadId=320&apikey=XXXXXX
	@RequestMapping(value = "addAndCloseLeadFollowupsHistoryAPI", produces = "application/javascript")
	public @ResponseBody String addAndCloseLeadFollowupsHistoryAPI(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		System.out.println("addAndCloseLeadFollowupsHistoryAPI is Called..");
		String tallycalleName = request.getParameter("tallycalleName");
		String callingTime = request.getParameter("callingTime");
		String remark = request.getParameter("remark");
		String sheduleTime = request.getParameter("sheduleTime");
		String newStatus = request.getParameter("newStatus");
		String successStatus = request.getParameter("successStatus");
		String followupDevice = "ANDRO_APP";
		String audoiFilePath = request.getParameter("audoiFilePath");
		String cd = request.getParameter("callDuration");
		String callStatus = request.getParameter("callStatus");
		String lid = request.getParameter("leadId");
		String followupType = request.getParameter("followupType");
		String reqfollowupTime = request.getParameter("reqfollowupTime");
		String leadState = request.getParameter("leadState");
		String responce = "N/A";
		// System.out.println(tallycalleName + "-" + callingTime + "-" + remark
		// + "-" + sheduleTime + "-" + newStatus + "-"
		// + audoiFilePath + "-" + cd + "-" + callStatus + "-" + lid + " ");
		// "+lid);
		
		if(successStatus == null || successStatus.isEmpty()) {
			successStatus="None";
		}
		
		System.out.println("successStatus_addAndCloseLeadFollowupsHistoryAPI:::"+successStatus);
		int leadId = Integer.parseInt(lid);
		int callDuration = Integer.parseInt(cd);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		String api = request.getParameter("apikey");
		Lead lead = new Lead();

		// System.out.println(api + " === " + lead.getTallyCalletId());
		lead = adminService.GetLeadObjectByLeadId(leadId);
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (api != null) {
			int second = 0;

			if (reqfollowupTime == null)
				second = 0;
			else
				second = Integer.parseInt(reqfollowupTime);

			// System.out.println("reqfollowupTime : " + reqfollowupTime);

			Tallycaller tcaller = adminService.TallyCallerByName(tallycalleName);
			adminService.addFollowupsHistory(tallycalleName, callingTime, remark, sheduleTime, newStatus, audoiFilePath,
					callDuration, callStatus, leadId, followupType, tcaller.getCompanyId(), second, successStatus,
					followupDevice);

			int i = adminService.updateSheduleDate(leadId, sheduleTime);
			int j = adminService.updateLeadProcessState(leadId, leadState);
			adminService.CloseLead(leadId);

		}
		// return "[{\"responseCode\":\"Data Successfully Added\"}]";

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCode\":\"Data Successfully Added\"}])";
		} else {
			return "[{\"responseCode\":\"Data Successfully Added\"}]";
		}
	}

	// http://localhost:8191/spring-hib/addFollowupsHistoryAPI?tallycalleName=Bonrix&&callingTime=20150907124644&&remark=none&&sheduleTime=20150907124644&&newStatus=gstste&&audoiFilePath=messagebox.mp3&&callDuration=5&&callStatus=success&&leadId=320&apikey=XXXXXX
	@RequestMapping(value = "saveSuccessRemarkForDesktopAPI", produces = "application/javascript")
	public @ResponseBody String saveSuccessRemarkForDesktopAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("saveSuccessRemarkForDesktopAPI is Called..");
		String tallycalleName = request.getParameter("tallycalleName");
		String callingTime = request.getParameter("callingTime");
		String remark = request.getParameter("remark");
		String sheduleTime = request.getParameter("sheduleTime");
		String newStatus = request.getParameter("newStatus");
		String successStatus = request.getParameter("successStatus");
		String followupDevice = "WEB";
		String audoiFilePath = request.getParameter("audoiFilePath");
		String cd = request.getParameter("callDuration");
		String callStatus = request.getParameter("callStatus");
		String lid = request.getParameter("leadId");
		String followupType = request.getParameter("followupType");
		String reqfollowupTime = request.getParameter("reqfollowupTime");
		String leadStateId = request.getParameter("leadState");
		// String responcedata = request.getParameter("responcedata");

		// System.out.println(tallycalleName + "-" + callingTime + "-" + remark
		// + "-" + sheduleTime + "-" + newStatus + "-"
		// + audoiFilePath + "-" + cd + "-" + callStatus + "-" + lid + " ");
		// "+lid);
		String leadState = "NA";
		List<Object[]> leadProState = adminService.getLeadProStateById(Integer.parseInt(leadStateId));
		for (Object[] result : leadProState) {
			leadState = result[2].toString();
			newStatus= result[2].toString();
		}
	//	 System.out.println("leadState::::"+leadState+"=================="+"newStatus::::"+newStatus );

		int leadId = Integer.parseInt(lid);
		int callDuration = Integer.parseInt(cd);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		String api = request.getParameter("apikey");
		Lead lead = new Lead();

		// System.out.println(api + " === " + lead.getTallyCalletId());
		lead = adminService.GetLeadObjectByLeadId(leadId);
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);

			if (apiKey == null) {

				return "[{\"responseCode\":\"INVALID_KEY\"}]";

			}
		}

		int second = 0;

		if (reqfollowupTime == null)
			second = 0;
		else
			second = Integer.parseInt(reqfollowupTime);

		// System.out.println("reqfollowupTime : " + reqfollowupTime);

		Tallycaller tcaller = adminService.TallyCallerByName(tallycalleName);
		adminService.addFollowupsHistory(tallycalleName, callingTime, remark, sheduleTime, newStatus, audoiFilePath,
				callDuration, callStatus, leadId, followupType, tcaller.getCompanyId(), second, successStatus,
				followupDevice);

		int i = adminService.updateSheduleDate(leadId, sheduleTime);
		int j = adminService.updateLeadProcessState(leadId, leadState);

		// return "[{\"responseCode\":\"Data Successfully Added\"}]";

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCode\":\"Data Successfully Added\"}])";
		} else {
			return "[{\"responseCode\":\"Data Successfully Added\"}]";
		}
	}

	// http://localhost:8191/spring-hib/addFollowupsHistoryAPI?tallycalleName=Bonrix&&callingTime=20150907124644&&remark=none&&sheduleTime=20150907124644&&newStatus=gstste&&audoiFilePath=messagebox.mp3&&callDuration=5&&callStatus=success&&leadId=320&apikey=XXXXXX
	@RequestMapping(value = "saveIncommingFollowupsHistoryAPI", produces = "application/javascript")
	public @ResponseBody String saveIncommingFollowupsHistoryAPI(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		System.out.println("saveIncommingFollowupsHistoryAPI is Called..");
		int tcId = Integer.parseInt(request.getParameter("tcId"));
		String callingTime = request.getParameter("callingTime");
		String remark = request.getParameter("remark");
		String sheduleTime = request.getParameter("sheduleTime");
		String newStatus = request.getParameter("newStatus");
		String successStatus = request.getParameter("successStatus");
		String audoiFilePath = request.getParameter("audoiFilePath");
		int cd = Integer.parseInt(request.getParameter("callDuration"));
		String callStatus = request.getParameter("callStatus");
		String catId = request.getParameter("catId");
		String reqfollowupTime = request.getParameter("reqfollowupTime");
		String leadState = request.getParameter("newStatus");
		String mobileNo = request.getParameter("mobileNo");

		String api = request.getParameter("apikey");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tcId, api, true);

		if (apiKey == null) {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}
		Tallycaller tcaller = new Tallycaller();
		tcaller = adminService.GetTellyCallerById(tcId);
		if (tcaller == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}

		cservice.createupdateSqlQuery(
				"INSERT INTO followupshistory (tallycallerId,callingTime,remark,sheduleTime,newStatus,audoiFilePath,callDuration,callStatus,leadId,followsUpType,companyId,failstatus,successStatus,followAddSecond,followupDevice,responce) VALUES "
						+ "(" + tcId + ",'" + callingTime + "','" + remark + "','" + sheduleTime + "','" + newStatus
						+ "','" + audoiFilePath + "','" + cd + "','Success','" + Integer.parseInt(catId)
						+ "','InBound'," + "" + tcaller.getCompanyId() + ",'None','" + successStatus
						+ "','0','WEB','N/A')");

		int i = adminService.updateSheduleDate(Integer.parseInt(catId), sheduleTime);
		int j = adminService.updateLeadProcessState(Integer.parseInt(catId), leadState);

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCode\":\"Data Successfully Added\"}])";
		} else {
			return "[{\"responseCode\":\"Data Successfully Added\"}]";
		}
	}

	// http://localhost:8191/spring-hib/addFollowupsHistoryAPI?tallycalleName=Bonrix&&callingTime=20150907124644&&remark=none&&sheduleTime=20150907124644&&newStatus=gstste&&audoiFilePath=messagebox.mp3&&callDuration=5&&callStatus=success&&leadId=320&apikey=XXXXXX
	@RequestMapping(value = "addFollowupsHistoryAPI", produces = "application/javascript")
	public @ResponseBody String addFollowupsHistoryAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addFollowupsHistory is Called..");
		String tallycalleName = request.getParameter("tallycalleName");
		String callingTime = request.getParameter("callingTime");
		String remark0 = request.getParameter("remark");
		String sheduleTime = request.getParameter("sheduleTime");
		String newStatus = request.getParameter("newStatus");
		String successStatus = request.getParameter("successStatus");
		String followupDevice = "WEB";
		String audoiFilePath = request.getParameter("audoiFilePath");
		String cd = request.getParameter("callDuration");
		String callStatus = request.getParameter("callStatus");
		String lid = request.getParameter("leadId");
		String followupType = request.getParameter("followupType");
		String reqfollowupTime = request.getParameter("reqfollowupTime");
		String leadStateId = request.getParameter("leadState");
		// String responcedata = request.getParameter("responcedata");
		String fid = request.getParameter("fid");


		String leadState = "NA";
		if (leadStateId.equalsIgnoreCase("0")) {
			leadState = "None";
		} else {
			List<Object[]> leadProState = adminService.getLeadProStateById(Integer.parseInt(leadStateId));
			for (Object[] result : leadProState) {
				leadState = result[2].toString();
				newStatus= result[2].toString();
			}
		}
		
		if (newStatus == null || newStatus.isEmpty()) {
			newStatus = "N/A";
		}

		int leadId = Integer.parseInt(lid);
		int callDuration = Integer.parseInt(cd);

		String remark = URLDecoder.decode(remark0, "UTF-8");
		System.out.println("REMARK :: " + remark + "-------" + "newStatus::" + newStatus);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		String api = request.getParameter("apikey");
		Lead lead = new Lead();

		lead = adminService.GetLeadObjectByLeadId(leadId);
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);

			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		String leadMobile = null;
		String companyId = null;
		String tallycalleId = null;
		List<Object[]> leadList = userService.leadByLeadId(leadId);
		for (Object[] result : leadList) {
			leadMobile = result[9].toString();
			companyId = result[2].toString();
			tallycalleId = result[11].toString();
		}
		System.out.println("REMARK :: " + remark + " " + "leadMobile:::" + leadMobile);

		String SMSStat = null;
		List<Object[]> smsStatList = userService.CheckSMSStat(companyId);

		if (smsStatList.size() == 1) {
			String SMSTemp = "Hello" + " " + leadMobile + " " + "Your Lead Stat Changed to" + " " + leadState;
			List<Object[]> smsTempList = userService.getSMSTempByLeadStat(leadState);
			for (Object[] resultTemp : smsTempList) {
				SMSTemp = resultTemp[2].toString();
			}
			System.out.println("SMSTemp :: " + SMSTemp);

			if (leadState.equalsIgnoreCase("Day_1")) {
				SMSTemp = SMSTemp.replaceAll("\\<UserName\\>", tallycalleName.trim());
				SMSTemp = SMSTemp.replaceAll("\\<Mobile\\>", leadMobile.trim());
			} else {
				SMSTemp.toString();
			}

			String msg = "Hello" + " " + leadMobile + " " + "Your Lead Stat Changed to" + " " + leadState;
			System.out.println("msg is::" + msg);
			msg = URLEncoder.encode(msg, "UTF-8");

			SMSTemp = URLEncoder.encode(SMSTemp, "UTF-8");
			System.out.println("SMSTemp" + SMSTemp);

			int value = 1;
			String username = "https://api.msg91.com/api/sendhttp.php?mobiles=<mobile_number>&authkey=117971AVUIS91ZNwlk5775355d&route=4&sender=TESTIN&message=<message> ";

			username = username.replaceAll("\\<mobile_number\\>", leadMobile.trim());
			username = username.replaceAll("\\<message\\>", SMSTemp.trim());

			System.out.println("URL:::" + username);

			int n = 0;
			String smsURL = "";
			String token = "";
			try {
				URL sms = new URL(username);
				HttpURLConnection httpConn = (HttpURLConnection) sms.openConnection();
				BufferedReader httpReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
				httpResponse = "";
				String line = "";
				while ((line = httpReader.readLine()) != null) {
					httpResponse = line;
					System.out.println("Conformation From SMS Provider: :" + httpResponse);
					if (httpResponse.equalsIgnoreCase("Message dropped but Not Sent".trim())) {
						value = 0;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				value = 0;
			}
			Sentsmslog sslog = new Sentsmslog();
			sslog.setAPIResponse(httpResponse);
			sslog.setMessage(SMSTemp);
			sslog.setTallycallerId(Integer.parseInt(tallycalleId));
			sslog.setCompayId(Integer.parseInt(companyId));
			sslog.setSendDate(new Date());
			sslog.setStaffId(0);
			sslog.setMobileNo(Long.parseLong(leadMobile));
			if (value == 1) {
				sslog.setMSGStatus("SUCCESS");
			} else if (value == 0) {
				sslog.setMSGStatus("FAIL");
			} else {
				sslog.setMSGStatus("NA");
			}
			adminService.saveLog(sslog);
		} else {
			System.out.println("Message Not Sent");
		}

		int second = 0;

		if (reqfollowupTime == null)
			second = 0;
		else
			second = Integer.parseInt(reqfollowupTime);

		// System.out.println("reqfollowupTime : " + reqfollowupTime);

		if (!fid.equalsIgnoreCase("undefined") && fid != null) {
			System.out.println("in  SAJAN");
			String uquery = "UPDATE followupshistory SET failstatus='None' , remark='" + remark + "',sheduleTime='"
					+ sheduleTime + "'" + ",newStatus='" + newStatus + "',followsUpType='" + followupType
					+ "',successStatus='" + successStatus + "',followAddSecond=" + second + ",followupDevice='"
					+ followupDevice + "',callStatus='" + callStatus + "' WHERE followupsId=" + fid;

			System.out.println(uquery);
			int i = cservice.createupdateSqlQuery(uquery);
			if (i != 0)
				cservice.createupdateSqlQuery("UPDATE lead SET lead.dialState=1 WHERE lead.leaadId=" + leadId + "");
			else
				System.out.println("Dail Status Not Updated.");

		} else {
			System.out.println("in else Sudhir");
			Tallycaller tcaller = adminService.TallyCallerByName(tallycalleName);
			adminService.addFollowupsHistory(tallycalleName, callingTime, remark, sheduleTime, newStatus, audoiFilePath,
					callDuration, callStatus, leadId, followupType, tcaller.getCompanyId(), second, successStatus,
					followupDevice);
		}
		int i = adminService.updateSheduleDate(leadId, sheduleTime);
		int j = adminService.updateLeadProcessState(leadId, leadState);
		// cservice.createupdateSqlQuery("");
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCode\":\"Data Successfully Added\"}])";
		} else {
			return "[{\"responseCode\":\"Data Successfully Added\"}]";
		}
	}

	// @Async
	@RequestMapping(value = "addFollowupsHistoryAndroAPI", produces = "application/javascript")
	public @ResponseBody String addFollowupsHistoryAndroAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addFollowupsHistoryAndroAPI is Called..");
		String tallycalleName = request.getParameter("tallycalleName");
		String callingTime = request.getParameter("callingTime");
		String remark0 = request.getParameter("remark");
		String sheduleTime = request.getParameter("sheduleTime");
		String newStatus = request.getParameter("newStatus");
		String successStatus = request.getParameter("successStatus");
		String followupDevice = "ANDRO_APP";
		String audoiFilePath = request.getParameter("audoiFilePath");
		String cd = request.getParameter("callDuration");
		String callStatus = request.getParameter("callStatus");
		String lid = request.getParameter("leadId");
		String followupType = request.getParameter("followupType");
		String reqfollowupTime = request.getParameter("reqfollowupTime");
		String leadState = request.getParameter("leadState");
		// String responcedata = request.getParameter("responcedata");
		String fid = request.getParameter("fid");

		if (newStatus == null) {
			newStatus = "N/A";
		}
		System.out.println("lid :: " + lid + "--" + "tallycalleName::" + tallycalleName);
		int leadId = Integer.parseInt(lid);
		int callDuration = Integer.parseInt(cd);
		String remark = URLDecoder.decode(remark0, "UTF-8");
		System.out.println("REMARK :: " + remark + "-------" + "newStatus::" + newStatus);
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		String api = request.getParameter("apikey");
		Lead lead = new Lead();

		lead = adminService.GetLeadObjectByLeadId(leadId);
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		
		if(successStatus == null || successStatus.isEmpty()) {
			successStatus="None";
		}
		
		System.out.println("successStatus_addFollowupsHistoryAndroAPI:::"+successStatus);
		
		String leadMobile = null;
		String companyId = null;
		String tallycalleId = null;
		List<Object[]> leadList = userService.leadByLeadId(leadId);
		for (Object[] result : leadList) {
			leadMobile = result[9].toString();
			companyId = result[2].toString();
			tallycalleId = result[11].toString();
		}
		System.out.println("REMARK :: " + remark + " " + "leadMobile:::" + leadMobile);

		String SMSStat = null;
		List<Object[]> smsStatList = userService.CheckSMSStat(companyId);
		if (smsStatList.size() == 1) {
			String SMSTemp = "Hello" + " " + leadMobile + " " + "Your Lead Stat Changed to" + " " + leadState;
			List<Object[]> smsTempList = userService.getSMSTempByLeadStat(leadState);
			for (Object[] resultTemp : smsTempList) {
				SMSTemp = resultTemp[2].toString();
			}
			System.out.println("SMSTemp :: " + SMSTemp);
			if (leadState.equalsIgnoreCase("Day_1")) {
				SMSTemp = SMSTemp.replaceAll("\\<UserName\\>", tallycalleName.trim());
				SMSTemp = SMSTemp.replaceAll("\\<Mobile\\>", leadMobile.trim());
			} else {
				SMSTemp.toString();
			}

			String msg = "Hello" + " " + leadMobile + " " + "Your Lead Stat Changed to" + " " + leadState;
			System.out.println("msg is::" + msg);
			// msg = URLEncoder.encode(msg, "UTF-8");
			SMSTemp = URLEncoder.encode(SMSTemp, "UTF-8");
			System.out.println("SMSTemp" + SMSTemp);
			int value = 1;
			String username = "https://api.msg91.com/api/sendhttp.php?mobiles=<mobile_number>&authkey=117971AVUIS91ZNwlk5775355d&route=4&sender=TESTIN&message=<message> ";
			username = username.replaceAll("\\<mobile_number\\>", leadMobile.trim());
			username = username.replaceAll("\\<message\\>", SMSTemp.trim());
			System.out.println("URL:::" + username);
			int n = 0;
			String smsURL = "";
			String token = "";
			try {
				URL sms = new URL(username);
				HttpURLConnection httpConn = (HttpURLConnection) sms.openConnection();
				BufferedReader httpReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
				httpResponse = "";
				String line = "";
				while ((line = httpReader.readLine()) != null) {
					httpResponse = line;
					System.out.println("Conformation From SMS Provider: :" + httpResponse);
					if (httpResponse.equalsIgnoreCase("Message dropped but Not Sent".trim())) {
						value = 0;
					}
				}
			} catch (Exception e) {
				System.out.println("Exception Occured");
				e.printStackTrace();
				value = 0;
			}
			Sentsmslog sslog = new Sentsmslog();
			sslog.setAPIResponse(httpResponse);
			sslog.setMessage(SMSTemp);
			sslog.setTallycallerId(Integer.parseInt(tallycalleId));
			sslog.setCompayId(Integer.parseInt(companyId));
			sslog.setSendDate(new Date());
			sslog.setStaffId(0);
			sslog.setMobileNo(Long.parseLong(leadMobile));
			if (value == 1) {
				sslog.setMSGStatus("SUCCESS");
			} else if (value == 0) {
				sslog.setMSGStatus("FAIL");
			} else {
				sslog.setMSGStatus("NA");
			}
			adminService.saveLog(sslog);
		} else {
			System.out.println("Message Not Sent");
		}

		int second = 0;
		if (reqfollowupTime == null)
			second = 0;
		else
			second = Integer.parseInt(reqfollowupTime);

		// System.out.println("reqfollowupTime : " + reqfollowupTime);
		if (!fid.equalsIgnoreCase("undefined") && fid != null) {
			System.out.println("in  if sudhir");
			String uquery = "UPDATE followupshistory SET failstatus='None' , remark='" + remark + "',sheduleTime='"
					+ sheduleTime + "'" + ",newStatus='" + newStatus + "',followsUpType='" + followupType
					+ "',successStatus='" + successStatus + "',followAddSecond=" + second + ",followupDevice='"
					+ followupDevice + "',callStatus='" + callStatus + "' WHERE followupsId=" + fid;

			System.out.println(uquery);
			int i = cservice.createupdateSqlQuery(uquery);
			if (i != 0)
				cservice.createupdateSqlQuery("UPDATE lead SET lead.dialState=1 WHERE lead.leaadId=" + leadId + "");
			else
				System.out.println("Dail Status Not Updated.");

		} else {
			System.out.println("in else Sudhir");
			Tallycaller tcaller = adminService.TallyCallerByName(tallycalleName);
			adminService.addFollowupsHistory(tallycalleName, callingTime, remark, sheduleTime, newStatus, audoiFilePath,
					callDuration, callStatus, leadId, followupType, tcaller.getCompanyId(), second, successStatus,
					followupDevice);
		}
		int i = adminService.updateSheduleDate(leadId, sheduleTime);
		int j = adminService.updateLeadProcessState(leadId, leadState);
		// cservice.createupdateSqlQuery("");
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCode\":\"Data Successfully Added\"}])";
		} else {
			return "[{\"responseCode\":\"Data Successfully Added\"}]";
		}
	}

	// http://localhost:8191/spring-hib/getFollowupsHistoryAPI?leadid=320&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getFollowupsHistoryAPI", produces = "application/javascript")
	public @ResponseBody String getFollowupsHistoryAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String leadid = request.getParameter("leadid");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		String api = request.getParameter("apikey");
		Lead lead = new Lead();
		lead = adminService.GetLeadObjectByLeadId(Integer.parseInt(leadid));
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (api != null) {
			List list = adminService.getFollowupsHistory(Integer.parseInt(leadid));
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {

					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("followupsId", result[0]);
					leadmap.put("callingTime", result[1]);
					leadmap.put("remark", result[2]);
					leadmap.put("sheduleTime", result[3]);
					leadmap.put("failstatus", result[7]);
					int totalSecs = (int) result[5];
					int hours = totalSecs / 3600;
					int minutes = (totalSecs % 3600) / 60;
					int seconds = totalSecs % 60;

					String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
					leadmap.put("newStatus", result[4]);
					leadmap.put("callDuration", timeString);
					leadmap.put("callStatus", result[6]);
					leadmap.put("audoiFilePath", result[8]);
					leadmap.put("followsUpType", result[9]);
					leadmap.put("telecallerName", result[10]);
					leadmap.put("leadSate", result[11]);
					jarray.add(leadmap);

				}

			} else {
				if (request.getParameter("callback") != null) {
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				} else {
					return "[{\"responceCode\":\"No Data Found\"}]";
				}
			}

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/getLeadDataAPI?leadid=3&tecclyCallerId=25&apikey=XXXXXX
	@RequestMapping(value = "getLeadDataAPI", produces = "application/javascript")
	public @ResponseBody String getLeadDataAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String leadid = request.getParameter("leadid");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Lead lead = new Lead();
		lead = adminService.GetLeadObjectByLeadId(Integer.parseInt(leadid));
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (api != null) {
			List list = adminService.getLeadData(Integer.parseInt(leadid));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("Country", result[12]);
					leadmap.put("state", result[13]);
					leadmap.put("city", result[14]);
					jarray.add(leadmap);

				}
				// String json = new Gson().toJson(leadmap);
				try {

				} finally {

				}

			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/updateLeadProcessStateAPI?leadid=3&&LeadState=InProcess&apikey=XXXXXX
	@RequestMapping(value = "updateLeadProcessStateAPI", produces = "application/javascript")
	public @ResponseBody String updateLeadProcessStateAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String leadid = request.getParameter("leadid");
		String LeadState = request.getParameter("LeadState");
		String api = request.getParameter("apikey");
		Lead lead = new Lead();
		lead = adminService.GetLeadObjectByLeadId(Integer.parseInt(leadid));
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		int rowcont = 0;
		if (apiKey != null) {
			rowcont = adminService.updateLeadProcessState(Integer.parseInt(leadid), LeadState);

		}
		return "[{\"responseCount\":\"" + rowcont + "\"}]";

	}

	// http://localhost:8191/spring-hib/updateSheduleDateAPI?leadid=3&&shDate=2015-09-07
	// 12:46:44&apikey=XXXXXX
	@RequestMapping(value = "updateSheduleDateAPI", produces = "application/javascript")
	public @ResponseBody String updateSheduleDateAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int leadid = Integer.parseInt(request.getParameter("leadid"));
		String shDate = request.getParameter("shDate");
		String api = request.getParameter("apikey");
		Lead lead = new Lead();
		lead = adminService.GetLeadObjectByLeadId(leadid);
		ApiKey apiKey = null;
		if (lead == null)
			return "[{\"responseCode\":\"INVALID_LEAD\"}]";

		if (lead != null) {
			apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(lead.getTallyCalletId(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		int rowcont = 0;
		if (apiKey != null) {
			rowcont = adminService.updateSheduleDate(leadid, shDate);

		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responseCount\":\"" + rowcont + "\"}]);";
		} else {
			return "[{\"responseCount\":\"" + rowcont + "\"}]";
		}
	}

	// http://localhost:8191/spring-hib/getLeadByCompanyIdNDmobileNoAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getLeadByCompanyIdNDmobileNoAPI", produces = "application/javascript")
	public @ResponseBody String getLeadByCompanyIdNDmobileNoAPI(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String mobNo = request.getParameter("mobNo");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		System.out.println("getLeadByCompanyIdNDmobileNo is called...");

		Tallycaller user = new Tallycaller();
		user = adminService.validateDesktopAPI(api);
		// System.out.println(user);
		// System.out.println("getLeadByCompanyIdNDmobileNo is called..." +
		// user.getCompanyId() + " " + mobNo);

		Object[] result = null;
		if (user != null) {
			List list = adminService.getLeadByCompanyIdNDmobileNo(mobNo, (int) (long) user.getCompanyId());
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					result = (Object[]) list.get(i);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					jarray.add(leadmap);
				}
			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	// http://localhost:8191/spring-hib/getTallyCallerLeadAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getTallyCallerLeadAPI", produces = "application/javascript")
	public @ResponseBody String getTallyCallerLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);
			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		if (apiKey != null) {
			List list = adminService.getTallyCallerLead(tallycallerName);
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					result = (Object[]) list.get(i);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tag", result[18] == null ? "N/A" : result[18]);
					leadmap.put("dialState", result[19]);
					jarray.add(leadmap);
				}
			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/getCategoryLeadAPI?tallycallerName=Bonrix&catname=Category7&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getCategoryLeadAPI")
	public @ResponseBody String getCategoryLeadAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String catname = request.getParameter("catname");
		String api = request.getParameter("apikey");
		String username = request.getParameter("username");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);
			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		if (apiKey != null) {
			List list = adminService.getCategoryLead(tallycallerName, catname);
			for (int i = 0; i < list.size(); i++) {
				Object[] result = (Object[]) list.get(i);
				JSONObject leadmap = new JSONObject();
				leadmap.put("leaadId", result[0]);
				leadmap.put("categoryName", result[1]);
				leadmap.put("createDate", result[2]);
				leadmap.put("email", result[3]);
				leadmap.put("firstName", result[4]);
				leadmap.put("lastName", result[5]);
				leadmap.put("leadProcessStatus", result[6]);
				leadmap.put("leadState", result[7]);
				leadmap.put("mobileNo", result[8]);
				leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
				leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
				leadmap.put("leadType", result[11]);
				leadmap.put("autoDial", result[12]);
				leadmap.put("companyName", result[13]);
				leadmap.put("website", result[14]);
				leadmap.put("Country", result[15]);
				leadmap.put("state", result[16]);
				leadmap.put("city", result[17]);
				leadmap.put("tag", result[18] == null ? "N/A" : result[18]);
				leadmap.put("dialState", result[19]);
				jarray.add(leadmap);
			}
		}
		return jarray.toJSONString();
	}

	// http://localhost:8191/spring-hib/getCategoryLeadCountAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getCategoryAPI", produces = "application/javascript")
	public @ResponseBody String getCategoryAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		// System.out.println("tallycallerName : " + tallycallerName);
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		// JSONObject leadmap=new JSONObject();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			List list = adminService.getCategoryList(new Long(tclaer.getCompanyId()));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : " + result[0] + " " +
					// result[1]);
					leadmap.put("category", result[1]);
					leadmap.put("categoryId", result[0]);
					jarray.add(leadmap);
				}
			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return jarray.toJSONString();
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/getCategoryDesktopAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getCategoryDesktopAPI", produces = "application/javascript")
	public @ResponseBody String getCategoryDesktopAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		// System.out.println("tallycallerName : " + tallycallerName);
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		// JSONObject leadmap=new JSONObject();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		Tallycaller apiKey = new Tallycaller();
		apiKey = adminService.validateTelecallerDesktopAPI(api);
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		if (apiKey != null) {
			List list = adminService.getCategoryList(new Long(apiKey.getCompanyId()));
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					Object[] result = (Object[]) list.get(i);
					// System.out.println("My : " + result[0] + " " +
					// result[1]);
					leadmap.put("category", result[1]);
					leadmap.put("categoryId", result[0]);

					jarray.add(leadmap);
				}
			} else {
				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return jarray.toJSONString();
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getAutoDialCategoryLeadCountAPI", produces = "application/javascript")
	public @ResponseBody String getAutoDialCategoryLeadCountAPI(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			List list = adminService.getCategoryAutoDialLeadCount(tallycallerName);
			for (int i = 0; i < list.size(); i++) {
				JSONObject leadmap = new JSONObject();
				Object[] result = (Object[]) list.get(i);
				leadmap.put("category", result[0]);
				leadmap.put("leadcount", result[1]);
				leadmap.put("categoryId", result[2]);

				jarray.add(leadmap);
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	// http://localhost:8191/spring-hib/getCategoryLeadCountAPI?tallycallerName=Bonrix&apikey=XXXXXX
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getCategoryLeadCountAPI", produces = "application/javascript")
	public @ResponseBody String getCategoryLeadCountAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			List list = adminService.getCategoryLeadCount(tallycallerName);
			for (int i = 0; i < list.size(); i++) {
				JSONObject leadmap = new JSONObject();
				Object[] result = (Object[]) list.get(i);
				leadmap.put("category", result[0]);
				leadmap.put("leadcount", result[1]);
				leadmap.put("categoryId", result[2]);
				jarray.add(leadmap);
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @RequestMapping(value = "getCategoryAutoDialLeadCountAPI", produces =
	 * "application/javascript") public @ResponseBody String
	 * getCategoryAutoDialLeadCountAPI(HttpServletRequest request,
	 * HttpServletResponse response) throws Exception {
	 * 
	 * String tallycallerName = request.getParameter("tallycallerName"); String api
	 * = request.getParameter("apikey"); org.json.simple.JSONArray jarray = new
	 * org.json.simple.JSONArray(); Tallycaller tclaer = new Tallycaller(); tclaer =
	 * adminService.GetTellyCallerByName(tallycallerName);
	 * 
	 * if (tclaer == null) { return "[{\"responseCode\":\"INVALID_TELECALLER\"}]"; }
	 * ApiKey apiKey = null;
	 * 
	 * if (tclaer != null) { apiKey =
	 * userService.gettAPiByUid(tclaer.getTcallerid(), api, true);
	 * 
	 * if (apiKey == null) { apiKey =
	 * userService.gettAPiByUid(tclaer.getTcallerid(), api, false); if (apiKey ==
	 * null) { return "[{\"responseCode\":\"INVALID_KEY\"}]"; } } } if (apiKey !=
	 * null) { List list =
	 * adminService.getCategoryAutoDialLeadCount(tallycallerName); for (int i = 0; i
	 * < list.size(); i++) { JSONObject leadmap = new JSONObject(); Object[] result
	 * = (Object[]) list.get(i); leadmap.put("category", result[0]);
	 * leadmap.put("leadcount", result[1]); leadmap.put("categoryId", result[2]);
	 * jarray.add(leadmap); } } if (request.getParameter("callback") != null) {
	 * return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
	 * } else { return jarray.toJSONString(); } }
	 */

	// http://localhost:8191/spring-hib/checkLogintelecallerAPI?type=ANDRO&username=admin&password=xxxxxxxx
	@RequestMapping(value = "checkLogintelecallerAPI", produces = "application/javascript")
	public @ResponseBody String checkLogintelecallerAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String username = request.getParameter("username");
		String pass = request.getParameter("password");
		System.out.println("checkLogintelecaller API is Called....");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
		response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Content-Type", "application/json");
		Tallycaller tcaller = new Tallycaller();
		tcaller = adminService.TallyCallerByName(username);

		if (tcaller == null)
			return "[{\"responseCode\":\"Invali Telecaller\"}]";

		if (tcaller.getActive() == 0)
			return "[{\"responseCode\":\"Your Account is Block by Administrator\"}]";

		if (tcaller.getPasswrd().equalsIgnoreCase(pass) && tcaller.getActive() == 1) {
			UserService uservice = ApplicationContextHolder.getContext().getBean(UserService.class);
			String uuid = UUID.randomUUID().toString();
			ApiKey apiKey = new ApiKey();
			apiKey.setCreateDate(new Date());
			apiKey.setUid(tcaller.getTcallerid());
			apiKey.setIp("*");
			apiKey.setKeyValue(uuid);
			apiKey.setIshidden(true);
			apiKey.setAccessDate(new Date());
			userService.saveApiKey(apiKey);

			org.json.simple.JSONArray mainjarray = new org.json.simple.JSONArray();
			JSONObject mainjobj = new JSONObject();
			org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
			if (request.getParameter("callback") != null) {
				/*
				 * mainjobj.put("responseCode", "SUCCESS"); mainjobj.put("userId",
				 * tcaller.getTcallerid()); mainjobj.put("ApiKey", apiKey.getKeyValue());
				 * mainjobj.put("CompId", tcaller.getCompanyId()); mainjarray.add(mainjobj);
				 */

				System.out
						.println(request.getParameter("callback") + "(" + "[{\"responseCode\":\"SUCCESS\",\"userId\":\""
								+ tcaller.getTcallerid() + "\",\"ApiKey\":\"" + apiKey.getKeyValue() + "\"}]" + ");");
				return request.getParameter("callback") + "(" + "[{\"responseCode\":\"SUCCESS\",\"userId\":\""
						+ tcaller.getTcallerid() + "\",\"ApiKey\":\"" + apiKey.getKeyValue() + "\",\"CompId\":\""
						+ tcaller.getCompanyId() + "\"}]" + ");";
				// return mainjarray.toJSONString();
			} else {
				List list = adminService.getLeadProcessState(tcaller.getCompanyId());
				if (list.size() != 0) {
					for (int i = 0; i < list.size(); i++) {
						JSONObject leadmap = new JSONObject();
						org.json.simple.JSONArray succArray = new org.json.simple.JSONArray();
						Object[] result = (Object[]) list.get(i);
						leadmap.put("Id", result[0]);
						leadmap.put("LeadProcessStat", result[1]);
						List<Object[]> getdetails = adminService.getLeadSuccessStatByLeadStat(result[0].toString());
						for (Object[] succResult : getdetails) {
							JSONObject DataObj = new JSONObject();
							DataObj.put("id", succResult[0]);
							DataObj.put("SuccessStat", succResult[1].toString());
							succArray.add(DataObj);
						}
						leadmap.put("successSatat", succArray);
						jarray.add(leadmap);
					}
					mainjobj.put("responseCode", "SUCCESS");
					mainjobj.put("userId", tcaller.getTcallerid());
					mainjobj.put("ApiKey", apiKey.getKeyValue());
					mainjobj.put("CompId", tcaller.getCompanyId());
					mainjobj.put("Data", jarray);
					mainjarray.add(mainjobj);
					return mainjarray.toJSONString();
				} else {
					mainjobj.put("responseCode", "SUCCESS");
					mainjobj.put("userId", tcaller.getTcallerid());
					mainjobj.put("ApiKey", apiKey.getKeyValue());
					mainjobj.put("CompId", tcaller.getCompanyId());
					mainjobj.put("Data", jarray);
					mainjarray.add(mainjobj);
					return mainjarray.toJSONString();
					// return "[{\"responceCode\":\"No Data Found\"}]";
				}
				/*
				 * return "[{\"responseCode\":\"SUCCESS\",\"userId\":\"" +
				 * tcaller.getTcallerid() + "\",\"ApiKey\":\"" + apiKey.getKeyValue() +
				 * "\",\"responseCode\":\"SUCCESS\",\"companyId\":\"" + tcaller.getCompanyId() +
				 * "\"}]";
				 */
			}
		} else
			return "[{\"responseCode\":\"Invalid Telecaller\"}]";

	}

	@RequestMapping(value = "checkLogin")
	public @ResponseBody String checkLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String username = request.getParameter("username");
		String pass = request.getParameter("password");
		String password = new SecurityConfig().passwordEncoder().encode(pass);
		User user = new User();
		user = adminService.getUserByUserName(username);
		if (user.getPassword().equalsIgnoreCase(password))
			return "Welcome " + username;
		else
			return "Invalid UserName Or Password.";
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getAndroidCallLogByTelecaller", produces = "application/javascript")
	public @ResponseBody String getAndroidCallLogByTelecaller(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "mobileNo") String mobileNo, @RequestParam(value = "name") String name,
			@RequestParam(value = "tallycallerName") String tallycallerName, @RequestParam(value = "apikey") String api,
			@RequestParam(value = "callDue") String callDue, @RequestParam(value = "callSta") String callSta)
			throws Exception {

		System.out.println("*******getAndroidCallLogByTelecaller********");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
		response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Content-Type", "application/json");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		String listData = "";
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		if (apiKey != null) {
			int i = 0;
			String query = "";
			String queryCount = "";
			String stdate = request.getParameter("stdate");
			String endate = request.getParameter("endate");
			// System.out.println("***************");

			if (stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA") && name.equalsIgnoreCase("NA")
					&& mobileNo.equalsIgnoreCase("NA") && callDue.equalsIgnoreCase("NA")
					&& callSta.equalsIgnoreCase("NA")) {
				queryCount = "SELECT  COUNT(*) "
						+ " FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.telecallerId="
						+ tclaer.getTcallerid() + " ORDER BY mobilecalllog.id DESC";

				query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration,mobilecalllog.callStatus,mobilecalllog.entryDate,1"
						+ " FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.telecallerId="
						+ tclaer.getTcallerid() + " ORDER BY mobilecalllog.id DESC";
			} else if (!name.equalsIgnoreCase("NA") && !mobileNo.equalsIgnoreCase("NA")
					&& !callDue.equalsIgnoreCase("NA") && !callSta.equalsIgnoreCase("NA")
					&& stdate.equalsIgnoreCase("NA") && endate.equalsIgnoreCase("NA")) {
				queryCount = "SELECT  COUNT(*) "
						+ " FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.telecallerId="
						+ tclaer.getTcallerid() + " AND" + " (mobilecalllog.nane LIKE '%" + name
						+ "%' OR mobilecalllog.callDuration LIKE '%" + callDue
						+ "%' OR mobilecalllog.callStatus LIKE '%" + callSta + "%' OR mobilecalllog.mobileNo='%"
						+ mobileNo + "%')" + "  ORDER BY mobilecalllog.id DESC";

				query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration,mobilecalllog.callStatus,mobilecalllog.entryDate,1"
						+ " FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.telecallerId="
						+ tclaer.getTcallerid() + " AND" + " (mobilecalllog.nane LIKE '%" + name
						+ "%' OR mobilecalllog.callDuration LIKE '%" + callDue
						+ "%' OR mobilecalllog.callStatus LIKE '%" + callSta + "%' OR mobilecalllog.mobileNo='%"
						+ mobileNo + "%')" + "  ORDER BY mobilecalllog.id DESC";
			} else if (!stdate.equalsIgnoreCase("NA") && !endate.equalsIgnoreCase("NA") && !name.equalsIgnoreCase("NA")
					&& !mobileNo.equalsIgnoreCase("NA") && !callDue.equalsIgnoreCase("NA")
					&& !callSta.equalsIgnoreCase("NA")) {
				queryCount = "SELECT  COUNT(*) "
						+ " FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.telecallerId="
						+ tclaer.getTcallerid() + " AND" + "  (mobilecalllog.entryDate BETWEEN '" + stdate
						+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) " + "  ORDER BY mobilecalllog.id DESC";

				query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration,"
						+ " mobilecalllog.callStatus,mobilecalllog.entryDate,1 FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId "
						+ " WHERE mobilecalllog.telecallerId=" + tclaer.getTcallerid()
						+ " AND (mobilecalllog.nane LIKE '%" + name + "%' OR mobilecalllog.callDuration LIKE '%"
						+ callDue + "%' OR mobilecalllog.callStatus LIKE '%" + callSta + "%'"
						+ " OR mobilecalllog.mobileNo='%" + mobileNo + "%') AND (mobilecalllog.entryDate BETWEEN '"
						+ stdate + "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) ORDER BY mobilecalllog.id DESC";
			} else {
				queryCount = "SELECT  COUNT(*) "
						+ " FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.telecallerId="
						+ tclaer.getTcallerid() + " AND" + "  (mobilecalllog.entryDate BETWEEN '" + stdate
						+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY)) " + "  ORDER BY mobilecalllog.id DESC";

				query = "SELECT CONCAT(tallycaller.firstname,' ',tallycaller.lastname),mobilecalllog.nane,mobilecalllog.mobileNo,mobilecalllog.callDuration,mobilecalllog.callStatus,mobilecalllog.entryDate,1"
						+ " FROM mobilecalllog INNER JOIN tallycaller ON tallycaller.tcallerid=mobilecalllog.telecallerId WHERE mobilecalllog.telecallerId="
						+ tclaer.getTcallerid() + " AND" + " (mobilecalllog.entryDate BETWEEN '" + stdate
						+ "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY))" + "  ORDER BY mobilecalllog.id DESC";
			}

			// System.out.println("" + queryCount);
			// System.out.println("" + query);
			int page = 1;
			int listSize = 10;
			int start = 0;
			int len = 0;
			String mob = "";
			Date fdate = null;
			Date todate = null;
			try {
				if (request.getParameter("start") != null) {
					start = Integer.parseInt(request.getParameter("start"));
				} else {// System.out.println("STSRT : "+start);
				}
				if (request.getParameter("length") != null) {
					len = Integer.parseInt(request.getParameter("length"));
					listSize = len;
				} else {
					// System.out.println("LENGTH : " + len);
				}
				page = start / len + 1;
			} catch (Exception ex) {

			}
			listData = adminService.ConvertDataToWebJSON(queryCount, query, page, listSize);
		}
		return listData;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "geLeadByMobileNumberAPI", produces = "application/javascript")
	public @ResponseBody String geLeadByMobileNumberAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("geLeadByMobileNumberAPI is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String mobileNo = request.getParameter("mobileNo");
		String api = request.getParameter("apikey");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		String jsons = "";
		if (api != null) {
			List list = adminService.geLeadByMobileNumberAPI(tclaer.getTcallerid(), mobileNo);
			DatatableJsonObject datatableJsonObject = new DatatableJsonObject();

			datatableJsonObject.setRecordsFiltered(list.size());
			datatableJsonObject.setRecordsTotal(list.size());
			datatableJsonObject.setData(list);

			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			jsons = gson.toJson(datatableJsonObject);
			// System.out.println(jsons);
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jsons + ");";
		} else {
			// System.out.println("IN LIST");
			return jsons;
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "updateCSVdata", produces = "application/javascript")
	public @ResponseBody String updateCSVdata(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("updateCSVdata is Called..");
		String tallycallerName = request.getParameter("tallycallerName");
		String csv = request.getParameter("csv");
		int leadId = Integer.parseInt(request.getParameter("leadId"));
		String api = request.getParameter("apikey");

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (api != null) {
			adminService.updateCSVdata(leadId, csv);
			// adminService.addNotification(html_body,tclaer.getTcallerid())
		}
		if (request.getParameter("callback") != null)
			return request.getParameter("callback") + "([{\"responseCode\":\"Tag Successfully Added To Lead.\"}]);";
		else
			return "[{\"responceCode\":\"Tag Successfully Added To Lead.\"}]";
	}

	// http://localhost:8191/spring-hib/getEntryFromTelecallerList?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@RequestMapping(value = "demoAdd", produces = "application/javascript")
	public @ResponseBody String demoAdd(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "password") String password) throws Exception {
		// System.out.println("demoAdd is Called.. :: "+password);
		JSONObject obj = new JSONObject();
		JSONObject callLogEntry = new JSONObject();

		JSONObject user = new JSONObject();
		user.put("CountryCode", 123);
		JSONObject contryCode = new JSONObject();
		contryCode.put("ContryCode", user);

		JSONObject user2 = new JSONObject();
		user2.put("PhoneNumber", 456);
		JSONObject phoneNumber = new JSONObject();
		phoneNumber.put("Phone Number", user2);

		JSONObject user3 = new JSONObject();
		user3.put("name", 456);
		JSONObject name = new JSONObject();
		name.put("Name", user3);

		JSONArray notebookUsers = new JSONArray();
		notebookUsers.add(contryCode);
		notebookUsers.add(phoneNumber);
		notebookUsers.add(name);

		callLogEntry.put("CallLogEntry", notebookUsers);
		obj.put("CallLogEntry", notebookUsers);
		JSONObject obj1 = new JSONObject();
		obj1.put("name", "Sample");
		obj1.put("age", new Integer(100));
		obj1.put("Add", "Sample");
		obj1.put("Other", new Integer(100));

		JSONObject obj2 = new JSONObject();
		obj2.put("name", "Sample");
		obj2.put("age", new Integer(100));
		obj2.put("Add", "Sample");
		obj2.put("Other", new Integer(100));
		JSONObject callLog = new JSONObject();

		callLog.put("One", obj);
		callLog.put("Two", obj1);
		callLog.put("Three", obj2);

		JSONObject callLogs = new JSONObject();
		callLogs.put("CallLog", callLog);
		System.out.println(callLogs.toString());
		return callLogs.toString();
	}

	// http://localhost:8191/spring-hib/getEntryFromTelecallerList?tallycallerId=3&&apikey=aef38a9a-9812-42a6-9e0d-48b0439f5a8c
	@RequestMapping(value = "demoput", produces = "application/javascript")
	public @ResponseBody String demoput(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "password") String password) throws Exception {
		System.out.println("demoAdd is Called.. :: " + password);
		JSONObject obj = new JSONObject();
		JSONObject callLogEntry = new JSONObject();

		/* Airtel Object */
		JSONObject Airtel = new JSONObject();
		Airtel.put("Operator", "Airtel");

		JSONObject Airtel1 = new JSONObject();
		Airtel1.put("Operator", "Airtel");

		JSONObject Airtel2 = new JSONObject();
		Airtel2.put("Operator", "Airtel");

		JSONArray AirtelObj = new JSONArray();
		AirtelObj.add(Airtel);
		AirtelObj.add(Airtel1);
		AirtelObj.add(Airtel2);

		JSONObject myAirtel = new JSONObject();
		myAirtel.put("Operator", "Airtel");
		myAirtel.put("Info", AirtelObj);

		/* Idea Object */
		JSONObject Idea = new JSONObject();
		Idea.put("Operator", "Idea");

		JSONObject Idea1 = new JSONObject();
		Idea1.put("Operator", "Idea");

		JSONObject Idea2 = new JSONObject();
		Idea2.put("Operator", "Idea");

		JSONArray ideaObject = new JSONArray();
		ideaObject.add(Idea);
		ideaObject.add(Idea1);
		ideaObject.add(Idea2);
		JSONObject myIdea = new JSONObject();
		myIdea.put("Operator", "Idea");
		myIdea.put("Info", ideaObject);

		/* Vodafone Object */
		JSONObject Vodafone = new JSONObject();
		Vodafone.put("Operator", "Vodafone");

		JSONObject Vodafone1 = new JSONObject();
		Vodafone1.put("Operator", "Vodafone");

		JSONObject Vodafone2 = new JSONObject();
		Vodafone2.put("Operator", "Vodafone");

		JSONArray vodafoneObject = new JSONArray();
		vodafoneObject.add(Vodafone);
		vodafoneObject.add(Vodafone1);
		vodafoneObject.add(Vodafone2);

		JSONObject myvodafone = new JSONObject();
		myvodafone.put("Operator", "Vodafone");
		myvodafone.put("Info", vodafoneObject);

		JSONArray Object = new JSONArray();
		Object.add(myAirtel);
		Object.add(myIdea);
		Object.add(myvodafone);

		System.out.println(Object.toString());
		return Object.toString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getAudioFileAPI")
	public @ResponseBody String getAudioFileAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("getAudioFileAPI is Called..");
		LOG.info("Hello API ................");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		FileNameJSON jsonAry = new FileNameJSON();
		int srNo = 0;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			MediUser currentUser = (MediUser) auth.getPrincipal();
			int id = (int) currentUser.getUserid();
			String SEP = System.getProperty("file.separator");
			String filePath = request.getServletContext().getRealPath("//");
			filePath = filePath + SEP + "Sound" + SEP;
			filePath = filePath.concat("" + id) + SEP;
			// System.out.println(filePath);
			File file = new File(filePath);
			String[] fileList = file.list();
			for (String name : fileList) {
				srNo = srNo + 1;
				String[] telName = name.split("_");
				FileNameJSON json = new FileNameJSON();
				json.setSrNo(srNo);
				json.setTelecaller_Name(telName[0]);
				json.setAudio_File_Name(name);
				jarray.add(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(jarray);
	}

	@RequestMapping(value = "addOrRemoveLeadsFromAutoDial", produces = "application/javascript")
	public @ResponseBody String addOrRemoveLeadsFromAutoDial(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("addOrRemoveLeadsFromAutoDial is called.....");
		String tallycallerId = request.getParameter("tallycallerId");
		int status = Integer.parseInt(request.getParameter("status"));
		String api = request.getParameter("apikey");
		// Integer.parseInt()
		ApiKey apiKey = null;
		apiKey = adminService.ValidateAPI(api);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		int ct = adminService.addOrRemoveLeadsFromAutoDial(tallycallerId, status);

		if (request.getParameter("callback") != null)
			return request.getParameter("callback")
					+ "([{\"responseCode\":\"Leads Successfully Added to AutoDial\"}]);";
		else {
			if (status == 0)
				return "[{\"responseCode\":\"Leads Successfully Remove From AutoDial\"}]";
			else
				return "[{\"responseCode\":\"Leads Successfully Added To AutoDial\"}]";
		}
	}

	/*
	 * @RequestMapping(value = "GetAndroifFailCallLeads", produces =
	 * "application/javascript") public @ResponseBody String
	 * GetAndroifFailCallLeads(HttpServletRequest request, HttpServletResponse
	 * response,
	 * 
	 * @RequestParam(value = "tallycalleId") long tallycalleId, @RequestParam(value
	 * = "stdate") String stdate,
	 * 
	 * @RequestParam(value = "apikey") String api) throws Exception { List lead =
	 * null; try { ApiKey apiKey = userService.gettAPiByUid(tallycalleId, api,
	 * true);
	 * 
	 * if (apiKey == null) return "[{\"responseCode\":\"INVALID_KEY\"}]";
	 * 
	 * System.out.println("In GetAndroifFailCallLeads.........."); String query =
	 * ""; if (stdate.equalsIgnoreCase("NA")) { query =
	 * " SELECT lead.leaadId,lead.firstName, lead.lastName,lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,\n"
	 * +
	 * " followupshistory.callingTime,lead.leadState,lead.autoDial,lead.createDate,followupshistory.newStatus,followupshistory.sheduleTime\n"
	 * +
	 * " FROM lead INNER JOIN categorymanager ON ( lead.categoryId = categorymanager.categotyId)INNER JOIN tallycaller ON ( lead.tallyCalletId = tallycaller.tcallerid)\n"
	 * +
	 * " INNER JOIN followupshistory ON (lead.leaadId = followupshistory.leadId)WHERE tallycaller.tcallerid = "
	 * + tallycalleId + " AND lead.companyId = tallycaller.`companyId`\n" +
	 * " AND followupshistory.followupsId = (SELECT  MAX(followupshistory.followupsId)FROM followupshistory WHERE followupshistory.leadId = lead.leaadId)\n"
	 * +
	 * " AND leadState = 'Open' AND ( followupshistory.failstatus IS NOT NULL AND followupshistory.failstatus <> 'None')  ORDER BY DATE(followupshistory.`callingTime`) DESC "
	 * ; } else { query =
	 * " SELECT lead.leaadId,lead.firstName, lead.lastName,lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,\n"
	 * +
	 * " followupshistory.callingTime,lead.leadState,lead.autoDial,lead.createDate,followupshistory.newStatus,followupshistory.sheduleTime\n"
	 * +
	 * " FROM lead INNER JOIN categorymanager ON ( lead.categoryId = categorymanager.categotyId)INNER JOIN tallycaller ON ( lead.tallyCalletId = tallycaller.tcallerid)\n"
	 * +
	 * " INNER JOIN followupshistory ON (lead.leaadId = followupshistory.leadId)WHERE tallycaller.tcallerid = "
	 * + tallycalleId + " AND lead.companyId = tallycaller.`companyId`\n" +
	 * " AND followupshistory.followupsId = (SELECT  MAX(followupshistory.followupsId)FROM followupshistory WHERE followupshistory.leadId = lead.leaadId)\n"
	 * +
	 * " AND leadState = 'Open' AND ( followupshistory.failstatus IS NOT NULL AND followupshistory.failstatus <> 'None') AND DATE(followupshistory.`callingTime`) = '"
	 * + stdate +"'\n" + " ORDER BY DATE(followupshistory.`callingTime`) DESC "; }
	 * query =
	 * " SELECT lead.leaadId,lead.firstName, lead.lastName,lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,\n"
	 * +
	 * " followupshistory.callingTime,lead.leadState,lead.autoDial,lead.createDate,followupshistory.newStatus,followupshistory.sheduleTime,"
	 * +
	 * " lead.leadProcessStatus,lead.csvData,lead.city,lead.Country,lead.state,lead.companyName,lead.website,lead.tagName,lead.sheduleDate,lead.leadType\n"
	 * +
	 * " FROM lead INNER JOIN categorymanager ON ( lead.categoryId = categorymanager.categotyId)INNER JOIN tallycaller ON ( lead.tallyCalletId = tallycaller.tcallerid)\n"
	 * +
	 * " INNER JOIN followupshistory ON (lead.leaadId = followupshistory.leadId)WHERE tallycaller.tcallerid = "
	 * + tallycalleId + " AND lead.companyId = tallycaller.`companyId`\n" +
	 * " AND followupshistory.followupsId = (SELECT  MAX(followupshistory.followupsId)FROM followupshistory WHERE followupshistory.leadId = lead.leaadId)\n"
	 * +
	 * " AND leadState = 'Open' AND ( followupshistory.failstatus IS NOT NULL) AND DATE(followupshistory.`callingTime`) = '"
	 * + stdate +"'\n" + " ORDER BY DATE(followupshistory.`callingTime`) DESC "; //
	 * System.out.println(query); lead = adminService.excuteListQuery(query);
	 * 
	 * } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * // System.out.println("----"+ new Gson().toJson(lead).toString()); String
	 * jsonArrStr = new Gson().toJson(lead).toString(); JSONParser parser = new
	 * JSONParser(); JSONArray jsonArr = (JSONArray)parser.parse(jsonArrStr);
	 * JSONArray sortedJsonArray = new JSONArray();
	 * 
	 * Object newJson=""; List<JSONObject> jsonValues = new ArrayList<JSONObject>();
	 * for (int i = 0; i < jsonArr.size(); i++) { jsonValues.add((JSONObject)
	 * jsonArr.get(i)); newJson = jsonArr.get(i); System.out.println("newJson--"+
	 * newJson); jsonValues.add((JSONObject) newJson); }
	 * System.out.println("jsonValues--"+ jsonValues.toString());
	 * 
	 * Collections.sort(jsonValues, new Comparator<JSONObject>() { private static
	 * final String KEY_NAME = "callingTime";
	 * 
	 * @Override public int compare(JSONObject a, JSONObject b) { String valA = new
	 * String(); String valB = new String(); try { valA = (String) a.get(KEY_NAME);
	 * valB = (String) b.get(KEY_NAME); }catch (JSONException e) { } return
	 * valA.compareTo(valB); } });
	 * 
	 * for (int i = 0; i < jsonArr.size(); i++) { //
	 * sortedJsonArray.put(jsonValues.get(i));
	 * sortedJsonArray.add(jsonValues.get(i)); System.out.println("Ordered_Array--"+
	 * jsonValues.get(i)); } //return new Gson().toJson(lead).toString(); return
	 * sortedJsonArray.toJSONString(); }
	 */

	@RequestMapping(value = "GetAndroidTodayGenLead", produces = "application/javascript")
	public @ResponseBody String GetAndroidTodayGenLead(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("--------GetAndroidTodayGenLead--------");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(request.getParameter("tallycallerId")),
				request.getParameter("apikey"), true);

		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		String query = "";
		List lead = null;
		if (apiKey != null) {
			query = " SELECT lead.leaadId, lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,lead.leadProcessStatus,"
					+ " lead.staffId, lead.companyId,lead.sheduleDate,lead.createDate,lead.leadState,lead.tallyCalletId,lead.leadType,lead.autoDial,"
					+ " lead.companyName,lead.website, lead.Country,lead.state,lead.city,lead.notification_flag,lead.assignDate,lead.csvData,lead.tagName,"
					+ " lead.dialState  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
					+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
					+ " WHERE  lead.tallyCalletId=" + Integer.parseInt(request.getParameter("tallycallerId"))
					+ " AND leadState='Open' AND DATE(lead.createDate)=CURDATE() ORDER BY lead.leaadId DESC";
			System.out.println(query);
			lead = adminService.excuteListQuery(query);
		}
		return new Gson().toJson(lead).toString();
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "GetAndroidUnattenedLeadsAPI", produces = "application/javascript")
	public @ResponseBody String GetAndroidUnattenedLeadsAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycalleId") long tallycalleId, @RequestParam(value = "stdate") String stdate,
			@RequestParam(value = "apikey") String api) throws Exception {
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tallycalleId, api, true);
		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		String query = "";
		List lead = null;
		if (apiKey != null) {
			if (stdate.equalsIgnoreCase("NA")) {
				query = "SELECT * FROM lead WHERE lead.leaadId IN" + " (" + " SELECT lead.leaadId FROM lead "
						+ " INNER JOIN followupshistory ON lead.leaadId=followupshistory.leadId"
						+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory"
						+ " WHERE followupshistory.leadId=lead.leaadId " + " ) AND lead.tallyCalletId=" + tallycalleId
						+ "  " + " AND followupshistory.callStatus='Fail'" + " )";
			} else {
				query = "SELECT * FROM lead WHERE lead.leaadId IN" + " (" + " SELECT lead.leaadId FROM lead "
						+ " INNER JOIN followupshistory ON lead.leaadId=followupshistory.leadId"
						+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory"
						+ " WHERE followupshistory.leadId=lead.leaadId " + " ) AND lead.tallyCalletId=" + tallycalleId
						+ "  " + " AND followupshistory.callStatus='Fail' AND followupshistory.callingTime='" + stdate
						+ "'" + " )";
			}
			System.out.println(query);
			lead = adminService.excuteListQuery(query);
		}
		return new Gson().toJson(lead).toString();
	}

	@RequestMapping(value = "/AddLeadsToAutiDialByFlag", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String AddLeadsToAutiDialByFlag(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerName") String tallycallerName, @RequestParam(value = "apikey") String api,
			@RequestParam(value = "stdate") String stdate,
			@RequestParam(value = "mode", required = false, defaultValue = "None") String mode) {

		Tallycaller tcaller = adminService.GetTellyCallerByName(tallycallerName);
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tcaller.getTcallerid(), api, true);
		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		int ct = 0;
		if (apiKey != null) {
			if (!mode.equalsIgnoreCase("None")) {
				if (mode.equalsIgnoreCase("TODAY_LEAD")) {
					String query = "UPDATE lead SET autoDial=1 WHERE tallyCalletId=" + tcaller.getTcallerid()
							+ " AND  DATE(sheduleDate)='" + stdate + "' AND leadState='Open'";
					ct = adminService.UpdateDemoQuery(query);
					System.out.println("COUNT :: " + ct + " QUERY :: " + query);
				} else if (mode.equalsIgnoreCase("FAIL_LEAD")) {
					String query = "";
					if (!stdate.equalsIgnoreCase("NA")) {
						query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tcaller.getTcallerid()
								+ " AND lead.leadState='Open' AND DATE(lead.createDate)='" + stdate + "' "
								+ " AND lead.leaadId IN (SELECT followupshistory.leadId FROM followupshistory WHERE "
								+ "  followupshistory.callStatus='Fail' AND followupshistory.tallycallerId="
								+ tcaller.getTcallerid() + " GROUP BY followupshistory.leadId )";
					} else {
						query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=8 AND lead.leadState='Open' "
								+ " AND lead.leaadId IN (SELECT followupshistory.leadId FROM followupshistory WHERE "
								+ " followupshistory.callStatus='Fail' AND followupshistory.tallycallerId="
								+ tcaller.getTcallerid() + " GROUP BY followupshistory.leadId )";
					}
					ct = adminService.UpdateDemoQuery(query);
					System.out.println("COUNT :: " + ct + " QUERY :: " + query);
				} else if (mode.equalsIgnoreCase("TODAY_GEN_LEAD")) {
					String query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tcaller.getTcallerid()
							+ " AND DATE(lead.createDate)='" + stdate + "' AND leadState='Open'";
					ct = adminService.UpdateDemoQuery(query);
					System.out.println("COUNT :: " + ct + " QUERY :: " + query);
				} else if (mode.equalsIgnoreCase("UNATTEN_LEAD")) {
					String query = "";
					if (!stdate.equalsIgnoreCase("NA")) {
						query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tcaller.getTcallerid()
								+ " AND lead.leaadId NOT IN (SELECT followupshistory.leadId FROM followupshistory WHERE followupshistory.tallycallerId="
								+ tcaller.getTcallerid() + ")" + " AND leadState='Open' AND  DATE(lead.createDate)='"
								+ stdate + "'";
					} else {
						query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tcaller.getTcallerid()
								+ "  "
								+ " AND lead.leaadId NOT IN (SELECT followupshistory.leadId FROM followupshistory "
								+ " WHERE followupshistory.tallycallerId=8 AND leadState='Open' )";
					}
					ct = adminService.UpdateDemoQuery(query);
					System.out.println("COUNT :: " + ct + " QUERY :: " + query);
				} else if (mode.equalsIgnoreCase("ALL_TODAY_ASSIGN")) {
					String query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tcaller.getTcallerid()
							+ " AND DATE(lead.assignDate)='" + stdate + "' AND leadState='Open'";
					ct = adminService.UpdateDemoQuery(query);
					System.out.println("COUNT :: " + ct + " QUERY :: " + query);
				} else
					return "[{\"responseCode\":\"INVALID_MODE\"}]";
			}
		}
		return "[{\"responseCode\":\" All Leads are Successfully Added to AutoDial\"}]";
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "GetTodayAssignLeads", produces = "application/javascript")
	public @ResponseBody String GetTodayAssignLeads(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycalleId") long tallycalleId, @RequestParam(value = "stdate") String stdate,
			@RequestParam(value = "apikey") String api) throws Exception {
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tallycalleId, api, true);
		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		String query = "";
		List lead = null;
		if (apiKey != null) {
			if (stdate.equalsIgnoreCase("NA")) {
				query = "SELECT lead.leaadId, lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,lead.leadProcessStatus,"
						+ " lead.staffId, lead.companyId,lead.sheduleDate,lead.createDate,lead.leadState,lead.tallyCalletId,lead.leadType,lead.autoDial,"
						+ " lead.companyName,lead.website, lead.Country,lead.state,lead.city,lead.notification_flag,lead.assignDate,lead.csvData,lead.tagName,lead.dialState  FROM lead "
						+ " INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId 	"
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "  WHERE  lead.tallyCalletId=" + tallycalleId
						+ " AND leadState='Open' AND DATE(lead.assignDate)=CURDATE() ";
			} else {
				query = "SELECT lead.leaadId, lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,lead.leadProcessStatus,"
						+ " lead.staffId, lead.companyId,lead.sheduleDate,lead.createDate,lead.leadState,lead.tallyCalletId,lead.leadType,lead.autoDial,"
						+ " lead.companyName,lead.website, lead.Country,lead.state,lead.city,lead.notification_flag,lead.assignDate,lead.csvData,lead.tagName,lead.dialState  FROM lead "
						+ " INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId 	"
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "  WHERE  lead.tallyCalletId=" + tallycalleId
						+ " AND leadState='Open' AND DATE(lead.assignDate)='" + stdate + "'";
			}
			System.out.println(query);
			lead = adminService.excuteListQuery(query);
		}
		return new Gson().toJson(lead).toString();
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "GetTodayScheduleLeads", produces = "application/javascript")
	public @ResponseBody String GetTodayScheduleLeads(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String name = request.getParameter("name");
		String mobileNo = request.getParameter("mobileNo");
		String email = request.getParameter("email");
		String catNme = request.getParameter("catNme");
		String tallycallerName = request.getParameter("tallycallerName");
		String stdate = request.getParameter("stdate");
		String endate = request.getParameter("endate");
		String api = request.getParameter("apikey");
		String sStaus = request.getParameter("sStaus");
		String lState = request.getParameter("lstate");

		System.out.println("sStaus::" + sStaus + " " + "lstate::" + lState);

		String lstateNew = "None";
		if (lState.equalsIgnoreCase("0")) {
			lstateNew = "None";
		} else {
			List<Object[]> leadProState = adminService.getLeadProStateById(Integer.parseInt(lState));
			for (Object[] result : leadProState) {
				lstateNew = result[2].toString();
			}
		}

		System.out.println("NewsStaus::" + sStaus + " " + "lstateNew::" + lstateNew);

		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		String list = "";
		StringBuilder query = new StringBuilder();
		StringBuilder queryCount = new StringBuilder();
		if (apiKey != null) {
			if (name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")) {
				
				System.out.println("if...");
				query.append(
						"SELECT lead.leaadId,lead.firstName,lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),lead.tagName,DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),lead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState,lead.csvData "
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ "   INNER JOIN followupshistory ON lead.tallyCalletId=followupshistory.tallycallerId  WHERE    0=0 AND tallycaller.tcallerid="
								+ tclaer.getTcallerid() + " " + " AND leadState='Open' "
							//	+ "AND lead.leadProcessStatus='"+ lstateNew + "' AND followupshistory.successStatus='" + sStaus
								+ " AND lead.sheduleDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate+ "', INTERVAL 1 DAY) ");

				
				  if (!sStaus.equals("None")) {
				      query.append(" AND followupshistory.successStatus='" + sStaus + "'"); 
				  }
				  
				  if (!lstateNew.equals("None")) {
					  query.append(" AND lead.leadProcessStatus='" + lstateNew + "'"); 
				  }
				 
				  query.append("GROUP BY lead.leaadId ORDER BY lead.sheduleDate ASC");
				  
				queryCount.append("SELECT count(*) "
						+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "    WHERE    0=0 AND tallycaller.tcallerid=" + tclaer.getTcallerid() + " "
						+ " AND leadState='Open' AND lead.sheduleDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate
						+ "', INTERVAL 1 DAY) ORDER BY lead.sheduleDate ASC ");
			} else if (name.equalsIgnoreCase("NA") && mobileNo.equalsIgnoreCase("NA") && email.equalsIgnoreCase("NA")) {
				System.out.println("else...if...");
				query.append(
						"SELECT lead.leaadId,lead.firstName,lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),lead.tagName,DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),lead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState "
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ "  INNER JOIN followupshistory ON lead.tallyCalletId=followupshistory.tallycallerId  WHERE    0=0 AND tallycaller.tcallerid="
								+ tclaer.getTcallerid() + " " + " AND leadState='Open' "
							//	+ "AND lead.leadProcessStatus='"+ lstateNew + "' AND followupshistory.successStatus='" + sStaus+ "' "
							    + "AND lead.sheduleDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate + "', INTERVAL 1 DAY) GROUP BY lead.leaadId ORDER BY lead.sheduleDate ASC  ");

				
				  if (!sStaus.equals("None")) {
				    query.append(" AND followupshistory.successStatus='" + sStaus + "'"); 
				   }
				  
				  if (!lstateNew.equals("None")) { 
					  query.append(" AND lead.leadProcessStatus='" + lstateNew + "'"); 
					}
				 
				query.append("GROUP BY lead.leaadId ORDER BY lead.sheduleDate ASC");

				queryCount.append("SELECT count(*) "
						+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
						+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
						+ "    WHERE    0=0 AND tallycaller.tcallerid=" + tclaer.getTcallerid() + " "
						+ " AND leadState='Open' AND lead.sheduleDate BETWEEN '" + stdate + "' AND ADDDATE('" + endate
						+ "', INTERVAL 1 DAY) ORDER BY lead.sheduleDate ASC ");

			} else {
				System.out.println("else");
				int i = 0;
				queryCount.append("SELECT count(*) from lead " + "   WHERE lead.tallyCalletId=" + tclaer.getTcallerid()
						+ " " + "   AND lead.leadState='Open'");

				query.append(
						"SELECT lead.leaadId,lead.firstName,lead.mobileNo,lead.email,categorymanager.categortName,lead.leadProcessStatus,lead.autoDial,lead.leadType,DATE_FORMAT(lead.sheduleDate,'%d-%m-%Y %r'),lead.tagName,DATE_FORMAT(lead.createDate,'%d-%m-%Y %r'),DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %T'),lead.lastName,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState "
								+ " FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId "
								+ " INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid "
								+ " WHERE    0=0 AND tallycaller.tcallerid=" + tclaer.getTcallerid() + " "
								+ " AND leadState='Open' AND ( ");

				if ((!name.equalsIgnoreCase("NA")) || (!name.equalsIgnoreCase("undefined"))
						|| (!name.equalsIgnoreCase("null"))) {
					i++;
					query.append(" lead.firstName LIKE '%" + name + "%' OR lead.lastName LIKE '%" + name + "%'  ");
				}
				if (mobileNo != null && !mobileNo.equalsIgnoreCase("NA") && !mobileNo.equalsIgnoreCase("undefined")
						&& !mobileNo.equalsIgnoreCase("null")) {
					i++;
					query.append(" OR lead.mobileNo LIKE '%" + mobileNo + "%' ");
				}
				if (email != null && !email.equalsIgnoreCase("NA") && !email.equalsIgnoreCase("undefined")
						&& !email.equalsIgnoreCase("null")) {
					i++;
					query.append(" OR lead.email LIKE '%" + email + "%' ");
				}
				if (catNme != null && !catNme.equalsIgnoreCase("NA") && !catNme.equalsIgnoreCase("undefined")
						&& !catNme.equalsIgnoreCase("null")) {
					i++;
					query.append("  OR categorymanager.categortName LIKE '%" + catNme + "%' ");
				}
				if (!sStaus.equals("None")) {
					query.append(" AND followupshistory.successStatus='" + sStaus + "'");
				}

				if (!lstateNew.equals("None")) {
					query.append(" AND lead.leadProcessStatus='" + lstateNew + "'");
				}

				if (i == 0) {
					query.append("  0=0 ");
				}
				query.append("   ) AND lead.sheduleDate BETWEEN '" + tclaer.getTcallerid() + "' AND ADDDATE('" + endate
						+ "', INTERVAL 1 DAY) ORDER BY lead.sheduleDate ASC");
			}
		}
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		try {
			if (request.getParameter("start") != null)
				start = Integer.parseInt(request.getParameter("start"));

			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			}
			page = start / len + 1;
		} catch (Exception ex) {
			
		}
		System.out.println("query::" + query);
		list = adminService.ConvertDataToWebJSON("" + queryCount, "" + query, page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				System.out.println("IN LIST");
				return list;
			}
		} else {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "ResetDial", produces = "application/javascript")
	public @ResponseBody String ResetDial(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycalleId") long tallycalleId, @RequestParam(value = "apikey") String api)
			throws Exception {
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(tallycalleId, api, true);
		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		String query = "";
		List lead = null;
		if (apiKey != null) {
			cservice.createupdateSqlQuery("UPDATE lead SET lead.dialState=0 WHERE lead.tallyCalletId=" + tallycalleId);
		}
		return "Reset Successfully.";
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @RequestMapping(value = "TestCal") public @ResponseBody String
	 * TestCal(HttpServletRequest request, HttpServletResponse response) throws
	 * Exception {
	 * 
	 * Calendar now = Calendar.getInstance();
	 * 
	 * now =Calendar.getInstance(); now.add(Calendar.WEEK_OF_YEAR,-1);
	 * System.out.println("date before 1 week : " + (now.get(Calendar.MONTH) + 1)+
	 * "-"+ now.get(Calendar.DATE)+ "-" + now.get(Calendar.YEAR));
	 * 
	 * String test = (now.get(Calendar.MONTH) + 1)+ "-"+ now.get(Calendar.DATE)+
	 * "-"+ now.get(Calendar.YEAR);
	 * 
	 * int cmonth = Calendar.getInstance().get(Calendar.MONTH);
	 * 
	 * System.out.println("test::"+test+"   "+"cmonth::"+cmonth);
	 * 
	 * return test+" "+cmonth;
	 * 
	 * }
	 */

	@RequestMapping(value = "addLeadsToAutoDialManagerAPI", method = {
			RequestMethod.GET }, produces = "application/javascript")
	public @ResponseBody String addLeadsToAutoDialManagerAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("addLeadsToAutoDialManagerAPI is called.....");
		String leadIds = request.getParameter("leadIds");
		String[] arrOfleadIds = leadIds.split(",");
		int autoDialVal = 1;

		for (String newlead_id : arrOfleadIds) {
			// System.out.println("nwlead_id :::"+ newlead_id+"
			// "+"autoDialVal:::"+autoDialVal);
			adminService.addLeadToAutoDialManager(Integer.parseInt(newlead_id.trim()), autoDialVal);
		}
		return "1";
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getTelecallerTestReport", produces = "application/javascript")
	public @ResponseBody String getTelecallerTestReport(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String telecallerName = request.getParameter("telecallerName");
		String telecallerId = request.getParameter("telecallerId");
		String leadId = request.getParameter("leadId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String telecallerMobile = request.getParameter("telecallerMobile");
		String leadState = request.getParameter("leadState");
		String successState = request.getParameter("successState");

		Followupshistory fh = new Followupshistory();

		fh.setAudoiFilePath(successState);
		fh.setCallDuration(Integer.parseInt(leadId));
		fh.setCallingTime(new Date());
		fh.setCallStatus(leadState);
		fh.setCompanyId(0);
		fh.setFailstatus(leadState);
		fh.setFollowAddSecond(0);
		fh.setFollowsUpType(telecallerId);
		fh.setFollowupDevice(telecallerName);
		fh.setFollowupsId(1);
		fh.setLeadId(0);
		fh.setNewStatus(successState);
		fh.setRemark(telecallerName);
		fh.setSheduleTime(new Date());
		fh.setSuccessStatus(telecallerMobile);
		fh.setTallycallerId(0);

		adminService.deleteAPI(0);
		return "";
	}

	@RequestMapping(value = "/getCategoryByManager", method = { RequestMethod.GET })
	public @ResponseBody List getCategoryByManager(HttpServletRequest request, HttpServletResponse response)throws Exception {
		List list = adminService.getCategoryByManager(request.getParameter("uid"));
		return list;
	}

	@RequestMapping(value = "saveCatColumns", method = { RequestMethod.GET })
	public @ResponseBody String saveCatColumns(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String columnName = request.getParameter("columnName");
		int compId = Integer.parseInt(request.getParameter("managerId"));
		int catId = Integer.parseInt(request.getParameter("catId"));

		/*
		 * ExtraFields efs = (ExtraFields)
		 * cservice.getSingleObject(" from ExtraFields Where catId="
		 * +catId+" AND companyId="+compId+" AND fieldName='"+columnName+"' ");
		 * if(!(efs==null)) { return "Field Already Exist"; }
		 */

		int fieldStat = adminService.extraFieldCount(catId, compId, columnName);
		System.out.println("fieldStat :: " + fieldStat);
		if (fieldStat > 0) {
			return "1";
		} else {
			ExtraFields ef = new ExtraFields();
			ef.setFieldName(columnName);
			ef.setCompanyId(compId);
			ef.setCatId(catId);
			adminService.saveExtraFields(ef);
			List<Object[]> leadList = adminService.getLeadByCatComp(compId, catId);
			if (leadList.size() == 0) {
				return "Column Successfully Added.";
			} else {
				System.out.println("else :: ");
				String val = "";
				for (Object[] obj : leadList) {
					String leadJson = obj[1].toString();
					JSONObject jo = new JSONObject(leadJson);
					jo.append("" + columnName + "", val);
					String json = jo.toString().replaceAll("[\\[\\]()]", "");
					System.out.println("json :: "+json);
					adminService.updateCSVJSON(Integer.parseInt(obj[0].toString()), json);
				}
				return "11";
			}
		}

	}

	@RequestMapping(value = "/GetCatColumns", method = RequestMethod.GET)
	public @ResponseBody String GetCatColumns(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id = currentUser.getUserid();

		List list = adminService.GetCatColumns((int) id);
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(list);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(datatableJsonObject);
	}

	@RequestMapping(value = "/deleteCatCol", method = RequestMethod.GET)
	public @ResponseBody void deleteCatCol(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "colid") Long colid, @RequestParam(value = "catId") int catId,
			@RequestParam(value = "columnName") String columnName) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id = currentUser.getUserid();
		adminService.deleteCatCol(colid);
		List<Object[]> leadList = adminService.getLeadByCatComp((int) id, catId);
		for (Object[] obj : leadList) {
			String leadJson = obj[1].toString();
			JSONObject jo = new JSONObject(leadJson);
			jo.remove(columnName);
			String json = jo.toString();
			adminService.updateCSVJSON(Integer.parseInt(obj[0].toString()), json);
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getFaicallLogAndroidAPI", produces = "application/javascript")
	public @ResponseBody String getFaicallLogAndroidAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		String failstate = request.getParameter("failstate");
		// String tallycallerId = request.getParameter("tallycallerId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String order = request.getParameter("order");
		String pageNo = request.getParameter("page");
		String limit = request.getParameter("listSize");

		// org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		org.json.simple.JSONObject jobj = new org.json.simple.JSONObject();
		org.json.simple.JSONArray arryyyyyy = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		System.out.println("*************** :: " + startDate + "------------" + endDate);
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		int i = Integer.parseInt(limit) * Integer.parseInt(pageNo);
		System.out.println("i : " + i);
		int final_PageNo = Math.abs(Integer.parseInt(limit) - i);
		System.out.println("Fina OFFSET : " + final_PageNo);

		// Object[] result = null;
		// String list = "";
		List<Object[]> list = new ArrayList<>();
		if (apiKey != null) {
			if (failstate.equalsIgnoreCase("NA")) {
				StringBuilder queryCount = new StringBuilder();
				queryCount.append("SELECT count(*) " + " FROM lead INNER JOIN categorymanager "
						+ " ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory "
						+ " ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
						+ tclaer.getUsername() + "'  AND lead.companyId=" + tclaer.getCompanyId() + "  "
						+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
						+ " AND leadState='Open'  AND (followupshistory.failstatus IS NOT NULL  )  AND (followupshistory.callingTime BETWEEN '"
						+ startDate + "' AND '" + endDate + "') LIMIT " + limit + " OFFSET " + final_PageNo + " ");

				StringBuilder query = new StringBuilder();
				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,"
								+ "	 followupshistory.callingTime,lead.leadState, lead.autoDial,lead.createDate,followupshistory.newStatus,followupshistory.sheduleTime FROM lead INNER JOIN categorymanager "
								+ "	ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory "
								+ "	ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
								+ tclaer.getUsername() + "'  AND lead.companyId=tallycaller.`companyId`    "
								+ "	AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
								+ "	AND leadState='Open'  AND (followupshistory.failstatus IS NOT NULL ) AND (followupshistory.callingTime BETWEEN '"
								+ startDate + "' AND '" + endDate + "') ORDER BY followupshistory.callingTime " + order
								+ " LIMIT " + limit + " OFFSET " + final_PageNo + " ");

				System.out.println("query_main_if::" + query);
				String queryNew = new String(query);
				list = adminService.getAndroFailCallList(queryNew);

			} else if ((!failstate.equalsIgnoreCase("N/A") || !failstate.equalsIgnoreCase("NA"))) {
				System.out.println("In ELSE IF Sudhir...");
				StringBuilder queryCount = new StringBuilder();
				queryCount.append("SELECT count(*) FROM lead INNER JOIN categorymanager "
						+ " ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory"
						+ " ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
						+ tclaer.getUsername() + "'  AND lead.companyId=" + tclaer.getCompanyId() + "  "
						+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
						+ " AND leadState='Open'   ");
				StringBuilder query = new StringBuilder();

				query.append(
						"SELECT lead.leaadId,CONCAT(lead.firstName,lead.lastName),lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,"
								+ " lead.createDate,lead.leadState, lead.autoDial,followupshistory.callingTime,followupshistory.newStatus FROM lead INNER JOIN categorymanager "
								+ " ON (lead.categoryId=categorymanager.categotyId) INNER JOIN tallycaller ON (lead.tallyCalletId=tallycaller.tcallerid)  INNER JOIN followupshistory"
								+ " ON (lead.leaadId=followupshistory.leadId)  WHERE  tallycaller.username= '"
								+ tclaer.getUsername() + "'  AND lead.companyId=" + tclaer.getCompanyId() + "  "
								+ " AND followupshistory.followupsId=(SELECT MAX(followupshistory.followupsId) FROM followupshistory WHERE followupshistory.leadId=lead.leaadId) "
								+ " AND leadState='Open'  AND   ");
				query.append("  followupshistory.`failstatus`='" + failstate
						+ "' AND (followupshistory.callingTime BETWEEN '" + startDate + "' AND '" + endDate + "') ");
				query.append(" ORDER BY followupshistory.callingTime " + order + " LIMIT " + limit + " OFFSET "
						+ final_PageNo + " ");
				queryCount.append(" AND followupshistory.`failstatus`='" + failstate
						+ "' AND (followupshistory.callingTime BETWEEN '" + startDate + "' AND '" + endDate + "') ");

				System.out.println("query_main_else::" + query);
				String queryNew = new String(query);
				list = adminService.getAndroFailCallList(queryNew);
			}

			System.out.println("listSize::" + list.size());
			if (list.isEmpty()) {
				jobj.put("Status", "False");
				jobj.put("Message", "No FailCalls Found");
				jobj.put("Data", arryyyyyy);
				System.out.println("arryyyyyy::" + jobj.toJSONString());
				return jobj.toJSONString();
			} else {
				for (Object[] result : list) {
					org.json.simple.JSONObject formDetailsJson = new org.json.simple.JSONObject();
					formDetailsJson.put("leadId", result[0].toString().trim());
					formDetailsJson.put("leadName", result[1].toString().trim());
					formDetailsJson.put("mobileNo", result[2].toString().trim());
					formDetailsJson.put("email", result[3].toString().trim());
					formDetailsJson.put("categoryName", result[4].toString().trim());
					formDetailsJson.put("failStatus", result[5].toString().trim());
					formDetailsJson.put("callingTime", result[6].toString().trim());
					formDetailsJson.put("leadState", result[7].toString().trim());
					formDetailsJson.put("autoDial", result[8].toString().trim());
					formDetailsJson.put("createDate", result[9].toString().trim());
					formDetailsJson.put("newStatus", result[10].toString().trim());
					arryyyyyy.add(formDetailsJson);
				}
				jobj.put("Status", "True");
				jobj.put("Message", "FailCall List");
				jobj.put("Data", arryyyyyy);
				System.out.println("arryyyyyy::" + jobj.toJSONString());
				return jobj.toJSONString();
			}
		} else {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}
	}

	@RequestMapping(value = "AndroAddLeadToAutoDialAPI", produces = "application/javascript")
	public @ResponseBody String AndroAddLeadToAutoDialAPI(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("AndroAddLeadToAutoDialAPI is called.....");
		String catId = request.getParameter("catId");
		String tallycallerId = request.getParameter("tallycallerId");
		String statu1 = request.getParameter("status");

		String status = "";
		String api = request.getParameter("apikey");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (statu1.equalsIgnoreCase("true"))
			status = "1";
		else
			status = "0";
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		if (apiKey != null) {
			List<Object[]> rowcont = adminService.getLeadByCatTele(Integer.parseInt(tallycallerId),
					Integer.parseInt(catId));
			for (Object[] obj : rowcont) {
				adminService.AddLeadToAutoDial(Integer.parseInt(obj[0].toString()), status);
			}
		}

		if (request.getParameter("callback") != null) {
			if (status.equalsIgnoreCase("1"))
				return request.getParameter("callback")
						+ "([{\"responseCode\":\"Leads Successfully Added to AutoDial\"}]);";
			else
				return request.getParameter("callback")
						+ "([{\"responseCode\":\"Leads Successfully Remove From AutoDial\"}]);";
		} else {
			if (status.equalsIgnoreCase("1"))
				return "[{\"responseCode\":\"Leads Successfully Added to AutoDial\"}]";

			else
				return "[{\"responseCode\":\"Leads Successfully Remove From AutoDial\"}]";

		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "GetExtraParamByLeadId", produces = "application/javascript")
	public @ResponseBody String GetExtraParamByLeadId(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("GetExtraParamByLeadId is called.....");
		String leadId = request.getParameter("leadId");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		JSONObject expJson = null;
		if (apiKey != null) {
			List<Object[]> list = userService.getExtraParam(Integer.parseInt(leadId));
			if (list.size() != 0) {
				for (Object[] result : list) {
					JSONObject leadmap = new JSONObject();
					leadmap.put("extParams", result[1].toString());
					System.out.println("extParams........" + result[1].toString());
					expJson = new JSONObject(result[1].toString());
					System.out.println("expJson........" + expJson);
					jarray.add(expJson);
					tcallerWbJsom.put("data", jarray);
				}
			} else
				return "[{\"responceCode\":\"No Data Found\"}]";
		}
		if (request.getParameter("callback") != null) {
			System.out.println("jarray........" + jarray.toJSONString());
			return request.getParameter("callback") + "(" + expJson.toString() + ");";
		} else {
			System.out.println("jarray........" + expJson);
			return expJson.toString();
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "updateExtraParamByLeadId", produces = "application/javascript")
	public @ResponseBody String updateExtraParamByLeadId(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("updateExtraParamByLeadId is called.....");
		String leadId = request.getParameter("leadId");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		String paramJson = request.getParameter("paramJson");
		System.out.println("paramJson....." + paramJson);

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {
			int paramStat = userService.updateExtraParams(paramJson, Integer.parseInt(leadId));
			if (paramStat == 1) {
				return "[{\"responseCode\":\"PARAMS_UPDATED\"}]";
			} else {
				return "[{\"responceCode\":\"No Data Found\"}]";
			}
		} else {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "androidExtraParams", method = RequestMethod.POST)
	public @ResponseBody String androidExtraParams(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("androidExtraParams is called.....");
		String leadId = request.getParameter("leadId");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		String paramJson = request.getParameter("dataJSON");
		System.out.println("paramJson....." + paramJson);

		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		JSONObject tcallerWbJsom = new JSONObject();
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		if (apiKey != null) {
			int paramStat = userService.updateExtraParams(paramJson, Integer.parseInt(leadId));
			if (paramStat == 1) {
				return "[{\"responseCode\":\"PARAMS_UPDATED\"}]";
			} else {
				return "[{\"responceCode\":\"No Data Found\"}]";
			}
		} else {
			return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}
	}

	// http://localhost:8080/spring-hib/androGetStates?tallycallerId=1315&apikey=d6b45d08-5195-4262-92a4-abad9ae0885a
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "androGetStates", produces = "application/javascript")
	public @ResponseBody String androGetStates(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("androGetStates is Called..");
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		Tallycaller tcaller = new Tallycaller();
		tcaller = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
		// System.out.println(tcaller.getCompanyId());
		if (api != null) {
			List list = adminService.getLeadProcessState(tcaller.getCompanyId());
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();
					org.json.simple.JSONArray succArray = new org.json.simple.JSONArray();
					Object[] result = (Object[]) list.get(i);
					leadmap.put("Id", result[0]);
					leadmap.put("LeadProcessStat", result[1]);
					List<Object[]> getdetails = adminService.getLeadSuccessStatByLeadStat(result[0].toString());
					for (Object[] succResult : getdetails) {
						JSONObject DataObj = new JSONObject();
						DataObj.put("id", succResult[0]);
						DataObj.put("SuccessStat", succResult[1].toString());
						succArray.add(DataObj);
					}
					leadmap.put("successSatat", succArray);
					jarray.add(leadmap);
				}
			} else
				return "[{\"responceCode\":\"No Data Found\"}]";
		}
		System.out.println("jarray---" + jarray);
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}
	}

	/*@RequestMapping(value = "/fileTest", method = RequestMethod.POST)
	public @ResponseBody String fileTest(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("image") MultipartFile file) {

		String replaceStrinng = request.getParameter("replaceStrinng");

		String fileName = null;
		String filePath = null;
		String logopath = null;
		if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				int byteLen = bytes.length;
				String s1 = fileName.substring(fileName.indexOf(".") + 1);

				File convFile = new File(file.getOriginalFilename());
				convFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(convFile);
				fos.write(file.getBytes());
				fos.close();

				String clientSideFileName = convFile.getName();
				File uploadDirectory = convFile.getParentFile();
				String newFileName = replaceStrinng + "." + s1;
				File newFile = new File(uploadDirectory, newFileName);
				convFile.renameTo(newFile);

				filePath = request.getServletContext().getRealPath("//");
				System.out.println("Real_filePath---"+filePath);
				filePath = filePath + "UploadFile/logo";
				System.out.println("Real_filePath2---"+filePath);
				filePath = filePath + SEP;
				
				System.out.println(filePath);
				File directory = new File(filePath);
				if (!directory.exists()) {
					directory.mkdir();
				}
				BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(new File(filePath + newFileName)));
				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				String s = "You have successfully uploaded " + filePath;
				logopath = "UploadFile/logo/" + newFileName;
				System.out.println("logopath::" + logopath);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
		}
		System.out.println("logopath::" + logopath);
		return "1";
	}*/
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "downloadAudioFilesAPI")
	public @ResponseBody String downloadAudioFilesAPI(HttpServletRequest request, HttpServletResponse response)	throws Exception {
		
		String telecallerId = request.getParameter("telecallerId");
		String stDate = request.getParameter("stDate");
		String endDate = request.getParameter("endDate");
		System.out.println("telecallerId---"+telecallerId+"---stDate---"+stDate+"----endDate---"+endDate);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		int id = (int) currentUser.getUserid();
		String sourceDir="NA";
		Tallycaller tcaller = new Tallycaller();
		tcaller = adminService.GetTellyCallerById(Integer.parseInt(telecallerId));
		
		String DownloadPathNew = "";
		sourceDir = request.getServletContext().getRealPath("//")+ "Sound"+File.separator+currentUser.getUserid();
		if(Integer.parseInt(telecallerId)==0) {
			// sourceDir = request.getServletContext().getRealPath("//")+ "Sound"+File.separator+currentUser.getUsername();
			// DownloadPathNew = "http://crmdemo.bonrix.in/Sound/"+currentUser.getUsername()+".tar.gz";
			  DownloadPathNew = currentUser.getUsername()+".tar.gz";
		}else {
			// sourceDir = request.getServletContext().getRealPath("//")+ "Sound"+File.separator+tcaller.getUsername();
		//	 DownloadPathNew = "http://crmdemo.bonrix.in/Sound/"+tcaller.getUsername()+".tar.gz";
			 DownloadPathNew = tcaller.getUsername()+".tar.gz";
		}
		
		String DownloadPath = "";
		TarArchiveOutputStream tarOs = null;
		  try {
		   File source = new File(sourceDir);
		   // Using input name to create output name
		   DownloadPath = source.getAbsolutePath().concat(".tar.gz");
		   FileOutputStream fos = new FileOutputStream(source.getAbsolutePath().concat(".tar.gz"));
		      GZIPOutputStream gos = new GZIPOutputStream(new BufferedOutputStream(fos));
		      tarOs = new TarArchiveOutputStream(gos);
		      
		      String fPath;
		      List<Object[]> audioList = userService.getAudioFilesPath(Integer.parseInt(telecallerId), stDate, endDate,id);
		      for(Object[] pathList : audioList) {
		    	  if(pathList[1].toString().equalsIgnoreCase("NA")) {
		    		  System.out.println("No Record Found");
		    	  }else {
		    		  fPath= sourceDir +File.separator+ pathList[1].toString();
		    		  System.out.println("fPath::"+fPath);
		    		  
		    		  File tempFile = new File(fPath);
		    		  boolean exists = tempFile.exists();
		    		  if(exists==true) {
		    			  System.out.println("File exists");
		    			  addFilesToTarGZ(fPath, "", tarOs);
		    		  }else {
		    			  System.out.println("No File Found");
		    		  }
		    	  }
		      }
		     // addFilesToTarGZ(sourceDir, "", tarOs);     
		  } catch (IOException e) {
		      e.printStackTrace();
		  }finally{
		   try {
		       tarOs.close();
		      } catch (IOException e) {
		        e.printStackTrace();
		      }
		   }
		  System.out.println("DownloadPath--"+DownloadPath);
		return DownloadPath;
	}
	
	public void addFilesToTarGZ(String filePath, String parent, TarArchiveOutputStream tarArchive) throws IOException {
			
		 File file = new File(filePath);
	  // Create entry name relative to parent file path 
	     String entryName = parent + file.getName();
		  
	   // add tar ArchiveEntry
		  tarArchive.putArchiveEntry(new TarArchiveEntry(file, entryName));
		  if(file.isFile()){
		   FileInputStream fis = new FileInputStream(file);
		   BufferedInputStream bis = new BufferedInputStream(fis);
		   // Write file content to archive
		   IOUtils.copy(bis, tarArchive);
		   tarArchive.closeArchiveEntry();
		   bis.close();
		  }else if(file.isDirectory()){
		   // no need to copy any content since it is
		   // a directory, just close the outputstream
		   tarArchive.closeArchiveEntry();
		   // for files in the directories
		   for(File f : file.listFiles()){        
		    // recursively call the method for all the subdirectories
		    addFilesToTarGZ(f.getAbsolutePath(), entryName+File.separator, tarArchive);
		   }
		  }      
		  
	}
	
	
	@RequestMapping(value = "deleteTarFile")
	public @ResponseBody String deleteTarFile(HttpServletRequest request, HttpServletResponse response)	throws Exception {
		System.out.println("deleteTarFile is Called..");
		
		String url = request.getParameter("url");
		File fileToDelete = new File(url);
	    boolean success = fileToDelete.delete();
		return "1";
	}
	
	
	@RequestMapping(value = "teleDashboard", produces = "application/javascript")
	public @ResponseBody String teleDashboard(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		String tallycallerId = request.getParameter("tallycallerId");
		String api = request.getParameter("apikey");
		ApiKey apiKey = null;
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);

		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}
		
		if (apiKey != null) {
			apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		
		//System.out.println("Date::-"+ datef.format(new Date()));
		org.json.simple.JSONObject jobj = new org.json.simple.JSONObject();
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tcaller = new Tallycaller();
		tcaller = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
		if (api != null) {
			
			String query_Total ="SELECT COUNT(*) FROM lead WHERE lead.companyId="+tcaller.getCompanyId()+" AND lead.tallyCalletId="+tallycallerId+" ";
			String query_UnAt  ="SELECT COUNT(*)  FROM lead INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid WHERE  tallycaller.username= '"+tcaller.getUsername()+"'  \n" + 
					            "AND lead.companyId="+tcaller.getCompanyId()+" AND lead.leaadId NOT IN (SELECT followupshistory.leadId FROM followupshistory) AND leadState='Open' ";
			String query_OpenCount ="SELECT COUNT(*) FROM lead WHERE lead.companyId="+tcaller.getCompanyId()+" AND lead.tallyCalletId="+tallycallerId+" AND leadState='Open'";
			String query_Due ="SELECT COUNT(*) FROM lead WHERE lead.companyId="+tcaller.getCompanyId()+" AND lead.tallyCalletId="+tallycallerId+" AND assignDate LIKE '%"+datef.format(new Date())+"%'";
			//System.out.println("query_Total::"+query_Total);
		//	System.out.println("query_Due::"+query_Due);
			
			int totalLeadCount= adminService.getCountTele(query_Total);
			int unLeadCount= adminService.getCountTele(query_UnAt);
			int openLeadCount= adminService.getCountTele(query_OpenCount);
			int dueLeadCount= adminService.getCountTele(query_Due);
		//	System.out.println("dueLeadCount::"+dueLeadCount);
			
			int attendCount = totalLeadCount - unLeadCount;
			int closeLeadCount = totalLeadCount - openLeadCount;
			//System.out.println("totalLeadCount::"+totalLeadCount+"--"+"attendCount::"+attendCount+"--"+"unLeadCount::"+unLeadCount);
			
			jobj.put("totalLeadCount", totalLeadCount);
			jobj.put("attendCount", attendCount);
			jobj.put("unLeadCount", unLeadCount);
			jobj.put("openLeadCount", openLeadCount);
			jobj.put("closeLeadCount", closeLeadCount);
			jobj.put("dueLeadCount", dueLeadCount);
			jarray.add(jobj);
		}
		return jobj.toJSONString(); 
	}
}

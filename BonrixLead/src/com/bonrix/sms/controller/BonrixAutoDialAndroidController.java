package com.bonrix.sms.controller;
 
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bonrix.sms.model.Android_mode;
import com.bonrix.sms.model.Androidbuttonflag;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.User;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("unchecked")
@Controller
public class BonrixAutoDialAndroidController {

	@Autowired
	AdminService adminService;

	@Autowired
	UserService userService;
	
	@Autowired
	CommonService cservice;

	final SimpleDateFormat df4 = new SimpleDateFormat("yyyyMMddHHmmss");
	final DateFormat readDbFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@RequestMapping(value = "/AndroidAutoDialLogin", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String AndroidAutoDialLogin(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "username") String username, @RequestParam(value = "password") String password,
			@RequestParam(value = "mode", required = false, defaultValue = "None") String mode) {
		System.out.println("IN AndroidAutoDialLogin Mode");
		Tallycaller tclaer = adminService.GetTellyCallerByName(username);
		JSONArray jarray = new JSONArray();
		JSONObject jobj = new JSONObject();
		if (tclaer.getPasswrd().equals(password) && tclaer.getActive()==1 ) {
			String uuid = UUID.randomUUID().toString();
			ApiKey apiKey = new ApiKey();
			apiKey.setCreateDate(new Date());
			apiKey.setUid(tclaer.getTcallerid());
			apiKey.setIp("*");
			apiKey.setKeyValue(uuid);
			apiKey.setIshidden(true);
			apiKey.setAccessDate(new Date());
			userService.saveApiKey(apiKey);
			// adminService.AddLeadToAhtoDialByCompanyId(tclaer.getCompanyId());
			jobj.put("responseCode", "SUCCESS");
			jobj.put("userId", tclaer.getTcallerid());
			jobj.put("ApiKey", apiKey.getKeyValue());
			jobj.put("tallycalleName", tclaer.getUsername());
			jarray.add(jobj);

			if (!mode.equalsIgnoreCase("None")) {
				Tallycaller tcaller = adminService.TallyCallerByName(username);
				if (mode.equalsIgnoreCase("ALL_UN-ASSIGN"))
					adminService.AddUassignLeadToAutoDial(tclaer.getCompanyId());
				else if (mode.equalsIgnoreCase("ALL_SCHEDULE_ELAPSE"))
					adminService.addRemailSheduleLeadsToAutoDial(tcaller.getTcallerid());
				else if (mode.equalsIgnoreCase("ALL_ASSIGN"))
				{
					adminService.addAllMyLeadsToAutoDial(tcaller.getTcallerid());
				}
				else if (mode.equalsIgnoreCase("ALL_SCHEDULE_TODAY"))
				{
					System.out.println("IN ALL_SCHEDULE_TODAY :: ");
				int number=	adminService.addScheduleTodayLeadsToAutoDial(tcaller.getTcallerid());
					System.out.println("IN ALL_SCHEDULE_TODAY ::  "+number);
				}
				else if (mode.equalsIgnoreCase("ALL_TODAY"))
					adminService.addAllTodayLeadsLeadsToAutoDial(tcaller.getTcallerid());
				else if (mode.equalsIgnoreCase("ALL_TODAY_ASSIGN"))
					adminService.addAllTodayAssignLeadsLeadsToAutoDial(tcaller.getTcallerid());
				else if (mode.equalsIgnoreCase("ALL_TODAY_ASSIGN"))
					adminService.addAllLeadsToAutoDial(tcaller.getTcallerid());
				else if (mode.equalsIgnoreCase("AUTO_DIAL"))
					System.out.println("In AUTO_DIAL Mode");
				else if (mode.equalsIgnoreCase("ALL_SELECTIVE"))
				System.out.println("In ALL_SELECTIVE Mode");
				else
					return "[{\"responseCode\":\"INVALID_MODE\"}]";
			}
		}
		System.out.println(jarray.toString());
		return jarray.toString();
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "GetMyLead", produces = "application/javascript")
	public @ResponseBody String GetMyLead(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycalleName") String tallycalleName,
			@RequestParam(value = "mode", required = false, defaultValue = "None") String mode,
			@RequestParam(value = "apikey") String api) throws Exception {
		Tallycaller tcaller = adminService.TallyCallerByName(tallycalleName);
		ApiKey apiKey = userService.gettAPiByUid(tcaller.getTcallerid(), api, true);

		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		List<Lead> UnassignleadData = null;
		List<Lead> Shedulelead = null;
		List<Lead> Mylead = null;
		List<Lead> AllScheduleToday = null;
		List<Lead> AllToday = null;
		List<Lead> TodayAssignLeads = null;
		List<Lead> incredibleservicesLeads = null;
		System.out.println("In GetMyLead..........");

		if (!mode.equalsIgnoreCase("None")) {
			if (mode.equalsIgnoreCase("ALL_UN-ASSIGN"))
				UnassignleadData = adminService.getUnassignLeads(tcaller.getCompanyId());
			else if (mode.equalsIgnoreCase("ALL_SCHEDULE_ELAPSE"))
				Shedulelead = adminService.getElapseSheduleLeads(tcaller.getTcallerid());
			else if (mode.equalsIgnoreCase("ALL_ASSIGN"))
				Mylead = adminService.GetMyLead(tcaller.getTcallerid());
			else if (mode.equalsIgnoreCase("ALL_SCHEDULE_TODAY"))
				AllScheduleToday = adminService.GetScheduleTodayLead(tcaller.getTcallerid());
			else if (mode.equalsIgnoreCase("ALL_TODAY"))
				AllToday = adminService.GetTodayLead(tcaller.getTcallerid());
			else if (mode.equalsIgnoreCase("AUTO_DIAL"))
				incredibleservicesLeads = adminService.incredibleservicesLeads(tcaller.getCompanyId());
			else if (mode.equalsIgnoreCase("ALL_SELECTIVE")) {
				List<Lead> selectivelead = adminService.getAutoDialLeadObject(tcaller.getTcallerid());
				System.out.println("In Selective Mode :: "+selectivelead.size());
				return new Gson().toJson(selectivelead).toString();
			}else if (mode.equalsIgnoreCase("ALL_SELECTIVE")) {
				List<Lead> selectivelead = adminService.getAutoDialLeadObject(tcaller.getTcallerid());
				System.out.println("In Selective Mode :: "+selectivelead.size());
				return new Gson().toJson(selectivelead).toString();
			}
			else
				return "[{\"responseCode\":\"INVALID_MODE\"}]";
		} else if (mode.equalsIgnoreCase("None")) {
			//UnassignleadData = adminService.getUnassignLeads(tcaller.getCompanyId());
			//Shedulelead = adminService.getElapseSheduleLeads(tcaller.getTcallerid());
			Mylead = adminService.GetMyLead(tcaller.getTcallerid());
		}

		if (UnassignleadData != null && UnassignleadData.size() != 0) {
			adminService.assignLeadUpdate(new Long(UnassignleadData.get(0).getLeaadId()),
					new Long(tcaller.getTcallerid()));
			System.out.println(" Unassign :: " + new Gson().toJson(UnassignleadData).toString());
			return new Gson().toJson(UnassignleadData).toString();
		} else if (Shedulelead != null && Shedulelead.size() != 0) {
			System.out.println(" Shedulelead :: " + new Gson().toJson(Shedulelead).toString());
			return new Gson().toJson(Shedulelead).toString();
		} else if (Mylead != null && Mylead.size() != 0) {
			System.out.println(" Mylead :: " + new Gson().toJson(Mylead).toString());
			return new Gson().toJson(Mylead).toString();
		} else if (AllScheduleToday != null && AllScheduleToday.size() != 0) {
			System.out.println(" AllScheduleToday :: " + new Gson().toJson(AllScheduleToday).toString());
			return new Gson().toJson(AllScheduleToday).toString();
		} else if (AllToday != null && AllToday.size() != 0) {
			System.out.println(" AllToday :: " + new Gson().toJson(AllToday).toString());
			return new Gson().toJson(AllToday).toString();
		} else if (TodayAssignLeads != null && TodayAssignLeads.size() != 0) {
			System.out.println(" TodayAssignLeads :: " + new Gson().toJson(TodayAssignLeads).toString());
			return new Gson().toJson(TodayAssignLeads).toString();
		}else if (incredibleservicesLeads != null && incredibleservicesLeads.size() != 0) {
			System.out.println(" incredibleservicesLeads :: " + new Gson().toJson(incredibleservicesLeads).toString());
			adminService.AddLeadToAutoDial(incredibleservicesLeads.get(0).getLeaadId(),"0");
			cservice.createupdateSqlQuery("UPDATE lead SET lead.dialState=1 WHERE lead.leaadId="+incredibleservicesLeads.get(0).getLeaadId()+"");
			cservice.createupdateSqlQuery("UPDATE autodiallog SET autodiallog.end_date=NOW() WHERE autodiallog.id=(SELECT * FROM(SELECT MAX(autodiallog.id) FROM autodiallog WHERE autodiallog.tc_id="+tcaller.getTcallerid()+") t)");
			return new Gson().toJson(incredibleservicesLeads).toString();
			
		} else {
			System.out.println(" NO_LEAD_FOUND ");
			return "[{\"responseCode\":\"NO_LEAD_FOUND\"}]";
		}
	}

	@RequestMapping(value = "ReEterate", produces = "application/javascript")
	public @ResponseBody String ReEterate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycalleName") String tallycalleName, @RequestParam(value = "apikey") String api,
			@RequestParam(value = "flag") String flag) throws Exception {
		Tallycaller tcaller = adminService.TallyCallerByName(tallycalleName);
		ApiKey apiKey = userService.gettAPiByUid(tcaller.getTcallerid(), api, true);

		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		/*if (flag.equalsIgnoreCase("ELAPSE")) {
			int ct = adminService.addRemailSheduleLeadsToAutoDial(tcaller.getTcallerid());
			return "[{\"responseCode\":\"" + ct + " Leads Added To AutoDial List.\"}]";
		} else if (flag.equalsIgnoreCase("ALLMY")) {
			int ct = adminService.AddTelecallrLeadToAutoDial(tcaller.getTcallerid(), "1");
			return "[{\"responseCode\":\"" + ct + " Leads Added To AutoDial List.\"}]";
		} else if (flag.equalsIgnoreCase("UNASSIGN")) {
			int ct = adminService.AddUassignLeadToAutoDial(tcaller.getCompanyId());
			return "[{\"responseCode\":\"" + ct + " Leads Added To AutoDial List.\"}]";
		}

		else
			return "[{\"responseCode\":\"INVALID_FLAG\"}]";*/
		return "[{\"responseCode\":\"NO_DATA_FOUND\"}]";
	}

	@RequestMapping(value = "AndroidAutoDialSuccessRemark", produces = "application/javascript")
	public @ResponseBody String AndroidAutoDialSuccessRemark(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycalleName") String tallycalleName,
			@RequestParam(value = "callingTime") String callingTime, @RequestParam(value = "remark") String remark,
			@RequestParam(value = "sheduleTime") String sheduleTime,
			@RequestParam(value = "newStatus") String newStatus,
			@RequestParam(value = "successStatus") String successStatus,
			@RequestParam(value = "audoiFilePath") String audoiFilePath,
			@RequestParam(value = "callDuration") int callDuration, @RequestParam(value = "leadId") int leadId,
			@RequestParam(value = "followupType") String followupType,
			@RequestParam(value = "leadState") String leadState,
			@RequestParam(value = "Close", required = false, defaultValue = "false") String close,
			@RequestParam(value = "apikey") String api) throws Exception {
		System.out.println("AndroidAutoDialSuccessRemark is Called..");
		Lead lead = adminService.GetLeadObjectByLeadId(leadId);
		Tallycaller tcaller = adminService.TallyCallerByName(tallycalleName);
		ApiKey apiKey = null;
		if (lead != null) {
			apiKey = userService.gettAPiByUid(tcaller.getTcallerid(), api, true);
			if (apiKey == null)
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
		}
		
		if (apiKey != null) {
		adminService.addFollowupsHistory(tallycalleName, callingTime, remark, sheduleTime, newStatus, audoiFilePath,
				callDuration, "Success", leadId, followupType, tcaller.getCompanyId(), 0, successStatus,
				"BONRIX_AUTO_DIAL_ANDRO_APP");
		adminService.updateSheduleDate(leadId, sheduleTime);
		adminService.updateLeadProcessState(leadId, leadState);
		adminService.AddRemoveLeadToAutoDialByLeadId(leadId, 0);
		System.out.println("In Success Close : " + close);
		cservice.createupdateSqlQuery("UPDATE lead SET lead.dialState=1 WHERE lead.leaadId="+leadId+"");
		cservice.createupdateSqlQuery("UPDATE lead SET tallyCalletId="+tcaller.getTcallerid()+" WHERE lead.leaadId="+leadId+"");
	
		if (close.equalsIgnoreCase("true")) {
			adminService.CloseLead(leadId);
		}
		}
		return "[{\"responseCode\":\"Data Successfully Added\"}]";
	}

	@RequestMapping(value = "/AndroidAutoDialFailFollowUp", produces = "application/javascript")
	public @ResponseBody String AndroidAutoDialFailFollowUp(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "callingTime") String callingTime,
			@RequestParam(value = "tallycallerId") int tallycallerId,
			@RequestParam(value = "newStatus") String newStatus,
			@RequestParam(value = "sheduleTime") String sheduleTime,
			@RequestParam(value = "followupType") String followupType, @RequestParam(value = "leadId") int leadId,
			@RequestParam(value = "Close", required = false, defaultValue = "false") String close,
			@RequestParam(value = "apikey") String api) throws Exception {
		ApiKey apiKey = userService.gettAPiByUid(tallycallerId, api, true);
		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		List list = adminService.getTeleCallerByUid(tallycallerId);
		Object[] result = (Object[]) list.get(0);
		Followupshistory history = new Followupshistory();
		history.setTallycallerId(tallycallerId);
		history.setAudoiFilePath("N/A");
		history.setCallDuration(00);
		history.setCallingTime(readDbFormat.parse(callingTime));
		history.setCallStatus("Fail");
		history.setNewStatus("N/A");
		history.setFailstatus(newStatus);
		history.setRemark("N/A");
		history.setSheduleTime(df4.parse(sheduleTime));
		history.setLeadId(leadId);
		history.setCompanyId(Integer.parseInt("" + result[7]));
		history.setFollowsUpType(followupType);
		history.setFollowAddSecond(0);
		history.setFollowupDevice("BONRIX_AUTO_DIAL_ANDRO_APP");
		history.setSuccessStatus("None");
		adminService.AddFailFollowUpAPI(history);
		adminService.AddRemoveLeadToAutoDialByLeadId(leadId, 0);
		int i = adminService.updateSheduleDate(leadId, sheduleTime);
		cservice.createupdateSqlQuery("UPDATE lead SET lead.dialState=1 WHERE lead.leaadId="+leadId+"");
		cservice.createupdateSqlQuery("UPDATE lead SET tallyCalletId="+tallycallerId+" WHERE lead.leaadId="+leadId+"");

		System.out.println("AndroidAutoDialFailFollowUp UPDATE lead SET lead.dialState=1 and tallyCalletId="+tallycallerId+" WHERE lead.leaadId="+leadId+"");

		System.out.println("UPdate Shedule LEad: " + i);
		System.out.println("In Fail Close : " + close);
		if (close.equalsIgnoreCase("true")) {
			adminService.CloseLead(leadId);
		}
		return "[{\"responseCode\":\"Followups Successfully Added\"}]";
	}

	// http://localhost:8191/spring-hib/uploadAutoDialAudioFIleAPI
	@RequestMapping(value = "uploadAutoDialAudioFIleAPI", method = RequestMethod.POST)
	public @ResponseBody String uploadAudioFIleAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("file") MultipartFile file, @RequestParam("tellcallerid") int teleId) throws Exception {
		System.out.println("uploadAudioFIleAPI is Called..");
		System.out.println("FIle Path : " + file);
		String fileName = null;
		String filePath = null;
		Tallycaller caller = adminService.getTallycallerByIdMdel(teleId);

		User user = null;
		if (caller != null) {
			user = adminService.getComapnyByTeclId(caller.getCompanyId());
			System.out.println("USER_NAME :: " + user.getUid());
		}
		if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				System.out.println("FILE NAME :: " + file.getOriginalFilename());
				filePath = request.getServletContext().getRealPath("//");
				filePath = filePath + SEP + "Sound" + SEP;
				filePath = filePath.concat(Long.toString(user.getUid())) + SEP;
				System.out.println(filePath);
				File directory = new File(filePath);
				if (!directory.exists()) {
					directory.mkdir();
				}

				BufferedOutputStream buffStream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath + fileName)));

				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				String s = "You have successfully uploaded " + filePath;
				return "[{\"responseCode\":\"You have successfully uploaded => " + s + "\"}]";
				// return "You have successfully uploaded " + fileName;
			} catch (Exception e) {
				return "[{\"responseCode\":\"You failed to upload\"}]";
				// "You failed to upload " + fileName + ": " + e.getMessage();
			}
		} else {
			return "[{\"responseCode\":\"Unable to upload. File is empty.\"}]";
		}
	}

	@RequestMapping(value = "GetConstant", produces = "application/javascript")
	public @ResponseBody String GetConstant(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("In GetConstant..........");
		List<Android_mode> mode = adminService.GetConstant();
		return new Gson().toJson(mode).toString();
	}

	@RequestMapping(value = "GetDailyLeadCount", produces = "application/javascript")
	public @ResponseBody String GetDailyLeadCount(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerId") int tallycallerId, @RequestParam(value = "apikey") String api)
			throws Exception {

		ApiKey apiKey = userService.gettAPiByUid(tallycallerId, api, true);
		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		System.out.println("GetMonthlyLeadCount is Called..");
		List list = adminService.GetDailyLeadCount(tallycallerId);
		Gson gson = new GsonBuilder()
				   .setDateFormat("yyyy-MM-dd").create();
		return gson.toJson(list).toString();
	}

	@RequestMapping(value = "/AddLeadToAutoDialByDate", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String AddLeadToAutoDialByDate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerId") int tallycallerId, @RequestParam(value = "mode") String mode,
			@RequestParam(value = "seldate", required = false) String seldate,
			@RequestParam(value = "apikey") String api) {

		ApiKey apiKey = userService.gettAPiByUid(tallycallerId, api, true);
		if (apiKey == null)
			return "[{\"responseCode\":\"INVALID_KEY\"}]";

		int ct = 0;
		if (mode.equalsIgnoreCase("ALL_TODAY")) {
			String query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tallycallerId
					+ " AND DATE(lead.createDate)='" + seldate + "'";
			ct = adminService.UpdateDemoQuery(query);
			System.out.println("COUNT :: " + ct + " QUERY :: " + query);
		} /*
			 * else if (mode.equalsIgnoreCase("ALL_ASSIGN")) { String query =
			 * "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" +
			 * tallycallerId + " AND  DATE(lead.createDate)='" + seldate + "'";
			 * ct = adminService.UpdateDemoQuery(query); }
			 */ else if (mode.equalsIgnoreCase("ALL_TODAY_ASSIGN")) {
			String query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tallycallerId
					+ " AND DATE(lead.assignDate)='" + seldate + "' AND leadState='Open'";
			ct = adminService.UpdateDemoQuery(query);
			System.out.println("COUNT :: " + ct + " QUERY :: " + query);
		} else if (mode.equalsIgnoreCase("ALL_UN-ATTENDED")) {
			String query = "UPDATE lead SET lead.autoDial=1 WHERE lead.tallyCalletId=" + tallycallerId
					+ " AND lead.leaadId NOT IN (SELECT followupshistory.leadId FROM followupshistory WHERE followupshistory.tallycallerId="
					+ tallycallerId + ")" + " AND leadState='Open' AND  DATE(lead.createDate)='" + seldate + "'";
			ct = adminService.UpdateDemoQuery(query);
			System.out.println("COUNT :: " + ct + " QUERY :: " + query);
		} else if (mode.equalsIgnoreCase("ALL_SCHEDULE_TODAY")) {
			String query = "UPDATE lead SET autoDial=1 WHERE tallyCalletId=" + tallycallerId
					+ " AND  DATE(sheduleDate)='" + seldate + "' AND leadState='Open'";
			ct = adminService.UpdateDemoQuery(query);
			System.out.println("COUNT :: " + ct + " QUERY :: " + query);
		}else if (mode.equalsIgnoreCase("ALL_FAIL_CALLS")) {
			String query = "UPDATE lead SET autoDial=1 WHERE tallyCalletId=" + tallycallerId
					+ " AND  DATE(sheduleDate)=CURDATE() AND leadState='Open'";
			ct = adminService.UpdateDemoQuery(query);
			System.out.println("COUNT :: " + ct + " QUERY :: " + query);
		} else
			return "[{\"responseCode\":\"INVALID_MODE\"}]";

		return "[{\"responseCode\":\" " + ct + " Leads Added To Auto Dial List\"}]";
	}

	@RequestMapping(value = "GetAndroidButtonConstant", produces = "application/javascript")
	public @ResponseBody String GetAndroidButtonConstant(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("In GetConstant..........");
		List<Androidbuttonflag> mode = adminService.GetAndroidButtonConstant();
		return new Gson().toJson(mode).toString();
	}

	String GetMonthName() {
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		return "-" + (month + 1) + "-" + year;
	}
}

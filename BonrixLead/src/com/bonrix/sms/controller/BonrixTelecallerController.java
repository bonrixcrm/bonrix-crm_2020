package com.bonrix.sms.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.utils.EncryptDecrypt;

@Controller
public class BonrixTelecallerController {

	@Autowired
	AdminService adminService;

	@RequestMapping(value = "/addBonrixTellyCaller", method = RequestMethod.GET)
	public @ResponseBody boolean addTellyCaller(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "comastr") String ComaStr) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id = currentUser.getUserid();
		System.out.println("AddTellyCaller :  " + ComaStr);
		boolean ststus = false;
		String[] data = ComaStr.split(",");

		Tallycaller tc = new Tallycaller();
		tc.setUsername(data[0]);
		tc.setPasswrd(data[1]);
		tc.setFirstname(data[2]);
		tc.setLastname(data[3]);
		tc.setMobno(data[5]);
		tc.setEmail(data[4]);
		tc.setCompanyId((int) id);
		tc.setActive(1);
		tc.setAllocate_staff_id(0);
		tc.setRegDate(new Date());

		adminService.addTallyCaller(tc);

		EncryptDecrypt enc;
		try {
			enc = new EncryptDecrypt();
			int maxtele = enc.getmaxtelecaller();
			List list = adminService.getTellyCaller(id);

			if ((list.size() + 1) <= maxtele) {
				System.out.println("IN ADD TELE : " + maxtele + " " + list.size());
				String[] Adddata = ComaStr.split(",");

				Tallycaller addTelecaller = new Tallycaller();
				addTelecaller.setUsername(Adddata[0]);
				addTelecaller.setPasswrd(Adddata[1]);
				addTelecaller.setFirstname(Adddata[2]);
				addTelecaller.setLastname(Adddata[3]);
				addTelecaller.setMobno(Adddata[5]);
				addTelecaller.setEmail(Adddata[4]);
				addTelecaller.setCompanyId((int) id);
				addTelecaller.setActive(1);
				addTelecaller.setAllocate_staff_id(0);
				addTelecaller.setRegDate(new Date());

				adminService.addTallyCaller(addTelecaller);
				ststus = true;
			} else {
				ststus = false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ststus;
	}

}

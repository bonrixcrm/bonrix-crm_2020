package com.bonrix.sms.controller;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.Crmemailsetting;
import com.bonrix.sms.model.Crmemailtemplate;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.UserService;

@Controller
public class CRMEmailController {
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@RequestMapping(value = "/SendCRMMail", produces = "application/javascript")
	public @ResponseBody String SendCRMMail(final HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		final String templateId = request.getParameter("templateId");
		final String sendto = request.getParameter("sendto");
		final String sub = request.getParameter("sub");
		final String tallycallerId = request.getParameter("tcid");
		final String leadId = request.getParameter("leadId");
		String api = request.getParameter("apikey");
		String filesname = request.getParameter("filesname");
		ApiKey apiKey = null;

		String status = "";
		String SEP = System.getProperty("file.separator");
		String filePath = request.getServletContext().getRealPath("/");
		filePath = filePath + SEP + "Attachments" + SEP;
		Crmemailtemplate temp = new Crmemailtemplate();
		temp = adminService.getTemplateInfo(Integer.parseInt(templateId));
		Tallycaller tcaler = adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
		System.out.println(
				"API KEY : " + api + " :: " + filesname + " :: " + tallycallerId + " :: " + tcaler.getCompanyId());
		Crmemailsetting emailSetting = adminService.getCrmEmailSetting(tcaler.getCompanyId());
		apiKey = userService.gettAPiByUid(Integer.parseInt(tallycallerId), api, true);
		if (apiKey == null) {
			if (apiKey == null) {
				if (filesname.length() == 0)
					filesname = "N/A";

				adminService.saveEmailLog(filesname, Integer.parseInt(leadId), temp.getTemp_Text(), sub,
						Integer.parseInt(tallycallerId), "Failed", tcaler.getCompanyId(), "INVALID_KEY");

				if (request.getParameter("callback") != null) {
					return request.getParameter("callback") + "([{\"responceCode\":\"INVALID_KEY\"}]);";
				} else {
					return request.getParameter("callback") + "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}

		try {
			if (apiKey != null) {
				if (filesname.length() == 0)
					filesname = "N/A";

				adminService.SendCRMMail(emailSetting, sendto, sub, tallycallerId, leadId, filesname, temp, filePath,
						tcaler, adminService, "Success");
			}

		} catch (Exception e) {
			status = "Failed";
			System.out.println(e.getMessage());
			return "[{\"responceCode\":\"" + e.getMessage() + "\"}]";
		}

		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "([{\"responceCode\":\"Email Send Successfully.\"}]);";
		} else {
			return "[{\"responceCode\":\"Email Send Successfully.\"}]";
		}
	}

}

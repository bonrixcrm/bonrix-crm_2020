package com.bonrix.sms.controller;

import java.util.HashMap;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import com.bonrix.sms.model.Crmemailsetting;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;

public class CRMSendSingleMail {

	@Autowired
	AdminService adminService;

	public String sendMail(final String emailData, final int cat) {
		System.out.println("SendCRMMail is Called..");
		final HashMap<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(42, "javiyamilan.pm@gmail.com");
		hm.put(43, "javiyamilan.pm@gmail.com");
		hm.put(44, "gpsbonrix@gmail.com");
		hm.put(45, "bonrixgpsemployeetracking@gmail.com");
		hm.put(46, "sales.bonrix@gmail.com");
		hm.put(47, "sales.bonrix@gmail.com");
		hm.put(48, "bonrix.liverates@gmail.com");
		hm.put(49, "Rahul");
		hm.put(51, "bonrixsystem32264@gmail.com");
		hm.put(52, "Rahul");
		hm.put(53, "Rahul");
		hm.put(54, "crm.bonrix@gmail.com");
		hm.put(57, "Rahul");

		Tallycaller tcaler = adminService.GetTellyCallerById(212);
		Crmemailsetting emailSetting = null;
		System.out.println("Company Id : " + tcaler.getCompanyId());
		emailSetting = adminService.getCrmEmailSetting(tcaler.getCompanyId());
		final String sendFrom = emailSetting.getEmailId();

		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(emailSetting.getHostName());
		sender.setUsername(emailSetting.getEmailId());
		sender.setPassword(emailSetting.getPasswd());
		sender.setJavaMailProperties(javaMailProperties());

		sender.send(new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				// TODO Auto-generated method stub

				/*
				 * Crmemailtemplate temp=new Crmemailtemplate();
				 * temp=adminService.getTemplateInfo(Integer.parseInt(templateId
				 * ));
				 */
				// System.out.println(temp.getTemp_Text());
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				message.setFrom(sendFrom);
				message.setTo(hm.get(cat));
				message.setSubject("New Bonrix Inquery.");
				message.setText(emailData, true);
			}
		});
		return null;
	}

	private Properties javaMailProperties() {
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.debug", "true");
		properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
		properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.setProperty("templateMode", "HTML5l");
		/* mimeMessage.setContent(htmlMsg, "text/html"); */
		properties.setProperty("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");

		return properties;
	}

}

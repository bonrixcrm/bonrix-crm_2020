package com.bonrix.sms.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;

import com.bonrix.sms.config.core.ApplicationContextHolder;
import com.bonrix.sms.model.WebSocketObj;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.utils.AndroidResponceHashMap;
import com.bonrix.sms.utils.CRMHashMap;

@ServerEndpoint("/CrmMobileConnectWebSocket")
public class CRMWebSocket {

	private Session session;
	private CRMHashMap map = new CRMHashMap();
	static AndroidResponceHashMap androidMap = new AndroidResponceHashMap();

	@OnError
	public void onError(Session session, Throwable t) {
		t.printStackTrace();
	}

	@OnMessage
	public void onMessage(String data, Session session) {
		try {
			// System.out.println("Message From Client : "+data);
			JSONObject json = new JSONObject(data);

			String type = json.getString("Type");
			System.out.println("MSG Type : " + type);
			System.out.println("DATA : " + json);
			if (type.equals("PING-PONG")) {
				/// System.out.println("PING-PONG SEND.");
				int TcId = Integer.parseInt(json.getString("TelecallerId"));

				for (Map.Entry<String, WebSocketObj> entry1 : CRMHashMap.getInstance().connectedClient.entrySet()) {
					WebSocketObj wskt = entry1.getValue();
					// System.out.println("WebSocket Telecaller Id :
					// "+wskt.getTcalletId()+" ||| "+wskt.getSkt());

					// System.out.println("WebSocket Telecaller Id :
					// "+wskt.getTcalletId());
					if (wskt.getTcalletId().equals(json.getString("TelecallerId").toString())) {

						// Session session1=wskt.getSkt();
						Session session1 = null;
						if (session1 != null) {
							System.out.println(
									"WebSocket Telecaller Id21 : " + wskt.getTcalletId() + " ||| " + wskt.getSkt());
							session1.getBasicRemote().sendText(
									"[{\"Type\":\"PING-PONG\",\"Status\":\"PING-PONG  Received  Success\",\"Msg\":\"PONG\"}]");

						}
					}
					/*
					 * else { sendMessage("0","PING-PONG"); }
					 */
				}

			} else {
				if (type.equals("Authorization")) {
					System.out.println("In Authorization");
					System.out.println(map.getClientSize());
					if (map.getClientSize() != 0) {

						for (Map.Entry<String, WebSocketObj> entry1 : CRMHashMap.getInstance().connectedClient
								.entrySet()) {

							System.out.println(entry1.getKey() + "=||=" + entry1.getValue());
							WebSocketObj wskt = entry1.getValue();

							if (wskt.getClientCode().equals(json.getString("ClientCode").toString())) {
								WebSocketObj oldWobj = entry1.getValue();
								CRMHashMap.getInstance().RemoveClient(wskt.getTcalletId());
								WebSocketObj newWobj = new WebSocketObj();
								newWobj.setTcalletId(oldWobj.getTcalletId());
								newWobj.setClientCode(oldWobj.getClientCode());
								// newWobj.setSkt(session);
								newWobj.setStatus(true);
								map.AddClient(oldWobj.getTcalletId(), newWobj);
								sendMessage("" + oldWobj.getTcalletId(), "Authorization");
								break;
							} else {
								sendMessage("0", "Authorization");
							}
						}
					} else {
						sendMessage("-1", "Authorization");
					}
				}

				else if (type.equals("AutoDialRequest_Accepted")) {
					for (Map.Entry<String, String> androidMap : AndroidResponceHashMap
							.getInstance().connectedAndroidClient.entrySet()) {
						String key = androidMap.getKey();
						JSONObject jsonObj = new JSONObject(json.getJSONObject("TelecallerReqInfo").toString());

						String status = jsonObj.getString("Callstatus");
						String jsonKey = json.getString("TelecallerId");
						if (key.equals(jsonKey)) {
							AndroidResponceHashMap.getInstance().RemoveClient(key);

							AndroidResponceHashMap.getInstance().AddClient(key,
									json.getJSONObject("TelecallerReqInfo").toString());
						}

					}
				}

				else if (type.equals("AutoDialDisconnectRequest_Accepted")) {
					// System.out.println("IN AutoDialDisconnectRequest_Accepted
					// SUCCCESS");
					for (Map.Entry<String, String> androidMap : AndroidResponceHashMap
							.getInstance().connectedAndroidClient.entrySet()) {

						String key = androidMap.getKey();
						JSONObject jsonObj = new JSONObject(json.getJSONObject("TelecallerReqInfo").toString());
						String status = jsonObj.getString("Callstatus");
						String jsonKey = json.getString("TelecallerId");

						if (key.equals(jsonKey)) {
							AndroidResponceHashMap.getInstance().RemoveClient(key);

							AndroidResponceHashMap.getInstance().AddClient(key,
									json.getJSONObject("TelecallerReqInfo").toString());
						}

					}
				}

				else if (type.equals("Logout_Request")) {
					System.out.println("IN Logout_Request ");
					System.out.println("Telecaaller Id :  " + json.getString("TelecallerId").toString());

					for (Map.Entry<String, WebSocketObj> entry1 : CRMHashMap.getInstance().connectedClient.entrySet()) {

						System.out.println("IN Logout_Request Loop ");
						String key = entry1.getKey();
						WebSocketObj wskt = entry1.getValue();
						System.out.println(
								"WebSocket : " + wskt.getTcalletId() + " " + json.getString("TelecallerId").toString());
						if (wskt.getTcalletId().equalsIgnoreCase(json.getString("TelecallerId").toString())) {
							System.out.println("In IF Condition");
							CRMHashMap.getInstance().RemoveClient(key);
							AndroidResponceHashMap.getInstance().RemoveClient(key);
							Session currentSession = null;
							currentSession.close();
						}
					}
				}

				/*
				 * if(type.equals("Android_Logout_Request")) {
				 * System.out.println("IN Android_Logout_Request ");
				 * System.out.println("Telecaaller Id :  "+json.getString(
				 * "TelecallerId").toString());
				 * disconnectFromAndroidApp(json.getString("TelecallerId").
				 * toString()); }
				 */

			}
		} catch (Exception e) {
			System.out.println("Error In Message : " + e.getMessage());
		}
	}

	@OnOpen
	public void onOpen(Session session) {
		this.session = session;
		System.out.println("Client connected");
	}

	@OnClose
	public void onClose(Session session) {

		System.out.println("Connection closed");
	}

	public void disconnectFromAndroidApp(String TelecallerId) {
		for (Map.Entry<String, WebSocketObj> entry1 : CRMHashMap.getInstance().connectedClient.entrySet()) {

			System.out.println("IN Android Method Logout_Request Loop ");
			String key = entry1.getKey();
			WebSocketObj wskt = entry1.getValue();
			// System.out.println("WebSocket : "+ wskt.getTcalletId()+"
			// "+TelecallerId);
			System.out.println("IN Android Method Logout_Request Loop " + wskt.getTcalletId() + " " + TelecallerId);
			if (wskt.getTcalletId().toString().equalsIgnoreCase(TelecallerId)) {
				Session skt = null;
				try {
					System.out.println("IN IF CONDITION");
					skt.getBasicRemote().sendText(
							"{\"Type\":\"Logout_Request\",\"Status\":\"Logout Request is Sended.\",\"TelecallerId\":\""
									+ TelecallerId + "\"}");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void sendMessage(String msg, String msgType) {

		try {
			System.out.println("In sendMessage  " + msgType + " : " + msg);

			AdminService adminService = ApplicationContextHolder.getContext().getBean(AdminService.class);
			Object[] result = null;
			try {
				if (msgType.equals("Authorization")) {
					if (msg.equals("0")) {
						session.getBasicRemote().sendText(
								"[{\"Type\":\"Authorization\",\"Status\":\"Authorization  Fail\",\"TelecallerId\":\"N/A\",\"TelecallerName\":\"N/A\"}]");
						return;
						// session.close();
					} else if (msg.equals("-1")) {
						session.getBasicRemote().sendText(
								"[{\"Type\":\"Authorization\",\"Status\":\"Invalid Authorization Code \",\"TelecallerId\":\"N/A\",\"TelecallerName\":\"N/A\"}]");
						return;
						// session.close();
					} else {
						List list = adminService.getTeleCallerByUid(new Long(msg));
						result = (Object[]) list.get(0);
						session.getBasicRemote().sendText(
								"[{\"Type\":\"Authorization\",\"Status\":\"Authorization  Success\",\"TelecallerId\":\""
										+ result[0] + "\",\"TelecallerName\":\"" + result[1] + "\"}]");
						return;
						// session.close();
					}
				}

				else if (msgType.equals("MOBILE_NO_SEND")) {
					session.getBasicRemote().sendText(
							"[{\"Type\":\"MOBILE_NO_SEND\",\"Status\":\"Message Received  Success\",\"TelecallerId\":\""
									+ msg + "\",\"TelecallerName\":\"" + msg + "\"}]");

				}

				else if (msgType.equals("PING-PONG")) {
					session.getBasicRemote().sendText(
							"[{\"Type\":\"PING-PONG\",\"Status\":\"PING-PONG  Received  Fail\",\"Msg\":\"Invalid Telecaller\"}]");
					return;
				} else {
					session.getBasicRemote().sendText("Invalid Message Type..");
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Error In Send  Message : " + e.getMessage());
			}
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block

			System.out.println("Error In Send  Message : " + e.getMessage());
			try {
				session.getBasicRemote().sendText("Error In Send  Message : " + e.getMessage());

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}
}

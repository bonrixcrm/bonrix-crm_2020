package com.bonrix.sms.controller;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExcelToCSVConvertController {

	   /*<strong>
     * Convert Excel spreadsheet to CSV. 
   * Works for <strong>Excel 97-2003 Workbook (<em>.xls)</strong> and <strong>Excel Workbook (</em>.xlsx)</strong>.
  * Does not work for the <strong>XML Spreadsheet 2003 (*.xml)</strong> format produced by BIRT.
     * @param fileName
  * @throws InvalidFormatException
   * @throws IOException
  */
	
	@RequestMapping(value = "/ExcelToCsv", method = RequestMethod.GET)
	public @ResponseBody String ExcelToCsv(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "fileName") String fileName)
			throws ParseException, IOException, InvalidFormatException {
		
		String SEP = System.getProperty("file.separator");
		String filePath = request.getServletContext().getRealPath("/");
		//filePath = filePath + "/UploadFile/";
		filePath = filePath +SEP +"UploadFile"+SEP+fileName;
		 
		System.out.println("ExcelToCsv is Called....	"+filePath);

        BufferedWriter output = new BufferedWriter(new FileWriter(filePath.substring(0, filePath.lastIndexOf(".")) + ".csv"));
 
        InputStream is = new FileInputStream(filePath);
 
       Workbook wb = WorkbookFactory.create(is);
 
     Sheet sheet = wb.getSheetAt(0);
 
       // hopefully the first row is a header and has a full compliment of
        // cells, else you'll have to pass in a max (yuck)
        int maxColumns = sheet.getRow(0).getLastCellNum();
 
        for (Row row : sheet) {
 
           // row.getFirstCellNum() and row.getLastCellNum() don't return the
            // first and last index when the first or last column is blank
         int minCol = 0; // row.getFirstCellNum()
           int maxCol = maxColumns; // row.getLastCellNum()
           
           for (int i = minCol; i < maxCol; i++) {
 
                Cell cell = row.getCell(i);
                String buf = "";
             if (i > 0) {
                    buf = ",";
               }
 
             if (cell == null) {
                    output.write(buf);
                 //System.out.print(buf);
               } else {
 
                  String v = null;
 
                  switch (cell.getCellType()) {
                  
                   case Cell.CELL_TYPE_STRING:
                        v = cell.getRichStringCellValue().getString();
                     break;
                 
                   case Cell.CELL_TYPE_NUMERIC:
                       if (DateUtil.isCellDateFormatted(cell)) {
                    	   
                    	   //SimpleDateFormat datetemp = new SimpleDateFormat("yyyy-MM-dd");
                    	   //v = datetemp.format(cell.getDateCellValue());
                    	   
                          v = cell.getDateCellValue().toString().trim();
                        } else {
                           //v = String.valueOf(cell.getNumericCellValue());
                        	v = new HSSFDataFormatter().formatCellValue( cell ).trim();
                        }
                      break;
                 
                   case Cell.CELL_TYPE_BOOLEAN:
                       v = String.valueOf(cell.getBooleanCellValue());
                        break;
                  case Cell.CELL_TYPE_FORMULA:
                	  
                       v = cell.getCellFormula();
                       
                    switch(cell.getCachedFormulaResultType()) {
                  case Cell.CELL_TYPE_NUMERIC:
                    	   v =   String.valueOf(cell.getNumericCellValue());
                    	   //v = new HSSFDataFormatter().formatCellValue( cell );
                           break;
                       case Cell.CELL_TYPE_STRING:
                    	   v =   String.valueOf(cell.getRichStringCellValue());
                           break;
                   }
                	 //v = String.valueOf(cell.getDateCellValue());
                     break;
                 default:
                   }
 
                 if (v != null) {
                       buf = buf + toCSV(v);
                  }
                  output.write(buf);
                 //System.out.print(buf);
               }
          }
 
         output.write("\n");
          //System.out.println();
        }
      is.close();
        output.close();
		return fileName;
		
	}
   
 
   /*</strong>
     * Escape the given value for output to a CSV file. 
    * Assumes the value does not have a double quote wrapper.
  * @return
  */
   public static String toCSV(String value) {
     
       String v = null;
       boolean doWrap = true;
        
       if (value != null) {
           
           v = value;
         
           if (v.contains("\"")) {
             v = v.replace("\"", "\"\""); // escape embedded double quotes
               doWrap = true;
         }
          
           if (v.contains(",") || v.contains("\n")) {
             doWrap = true;
         }
           
           if (v.contains(" ")) {
               v = v.replace(" ", ""); // escape embedded double quotes
                 doWrap = true;
           }
          
           if (doWrap) {
              v = "\"" + v + "\""; // wrap with double quotes to hide the comma
            }
      }
      
       return v;
      
   }
    
}

package com.bonrix.sms.controller;

/*
 *  This is the code for read the unread mails from your mail account.
 *  Requirements:
 *      JDK 1.5 and above
 *      Jar:mail.jar
 *
 */
import java.io.*;
import java.rmi.UnknownHostException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.*;
import javax.mail.Flags.Flag;
import javax.mail.search.FlagTerm;

import org.springframework.beans.factory.annotation.Autowired;

import com.bonrix.sms.config.core.ApplicationContextHolder;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.Regexpatten;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.UserService;

public class ExtractContactInfo {

	AdminService adminService = ApplicationContextHolder.getContext().getBean(AdminService.class);

	Folder inbox;
	String subject = "";
	String from = "";
	String emailbdy = "";
	String content = "";

	// Constructor of the calss.
	public ExtractContactInfo(String username, String password, int staffId) {

		/* Set the mail properties */	
		System.out.println(username+" "+password);

		
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		try {
			/* Create the session and get the store for read the mail. */
			Store store;

			Session session = Session.getDefaultInstance(props, null);
			store = session.getStore("imaps");
			store.connect("imap.gmail.com", username, password);
			/* Mention the folder name which you want to read. */
			inbox = store.getFolder("Inbox");
			System.out.println("No of Unread Messages : " + inbox.getUnreadMessageCount());

			/* Open the inbox using store. */
			inbox.open(Folder.READ_WRITE);

			/* Get the messages which is unread in the Inbox */
			//Message messages[] = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false));
			Message messages[] = inbox.search(new FlagTerm(new Flags(Flag.SEEN), true));

			/* Use a suitable FetchProfile */
			FetchProfile fp = new FetchProfile();
			fp.add(FetchProfile.Item.ENVELOPE);
			fp.add(FetchProfile.Item.CONTENT_INFO);
			inbox.fetch(messages, fp);

			try {
				printAllMessages(messages);
				inbox.close(true);
				store.close();
			} catch (Exception ex) {
				System.out.println("Exception arise at the time of read mail");
				ex.printStackTrace();
			}
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (MessagingException e) {
			e.printStackTrace();
			try {
				Thread.sleep(50000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// System.exit(2);
		}
	}

	public void printAllMessages(Message[] msgs) throws Exception {
		for (int i = 0; i < msgs.length; i++) {
			System.out.println("---------------------------------");
			// System.out.println("MESSAGE #" + (i + 1) + ":");
			printEnvelope(msgs[i]);
		}
	}

	/* Print the envelope(FromAddress,ReceivedDate,Subject) */
	public void printEnvelope(Message message) throws Exception {
		Address[] a;
		// FROM
		if ((a = message.getFrom()) != null) {
			for (int j = 0; j < a.length; j++) {
				from += a[j].toString();
			}
		}
		// TO
		if ((a = message.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++) {
				// System.out.println("TO: " + a[j].toString());
			}
			java.lang.String s = "";
		}
		subject = message.getSubject();
		Date receivedDate = message.getReceivedDate();
		content = message.getContent().toString();
		 System.out.println("Subject : " + subject);
		 System.out.println("Received Date : " + receivedDate.toString());
		 System.out.println("Content : " + content);

		getContent(message);
		System.out.println("Message : "+message);
		// colsemsg(message);
		/*
		 * if(j>=a.length) inbox.setFlags(new Message[] {message}, new
		 * Flags(Flags.Flag.SEEN), true);
		 */

	}

	public void getContent(Message msg) {
		try {
			String contentType = msg.getContentType();
			// System.out.println("Content Type : " + contentType);
			Multipart mp = (Multipart) msg.getContent();
			int count = mp.getCount();

			for (int i = 0; i < count; i++) {
				emailbdy += dumpPart(mp.getBodyPart(i));
			}
			dispalyMsg(emailbdy);
		

		} catch (Exception ex) {
			System.out.println("Exception arise at get Content");
			ex.printStackTrace();
		}
	}

	String mesg = "";

	public String dumpPart(Part p) throws Exception {
		// Dump input stream ..
		InputStream is = p.getInputStream();
		// If "is" is not already buffered, wrap a BufferedInputStream
		// around it.
		if (!(is instanceof BufferedInputStream)) {
			is = new BufferedInputStream(is);
		}
		int c;

		// System.out.println("Message : ");
		while ((c = is.read()) != -1) {
			mesg += (char) c;
		}

		return mesg;
	}

	public void colsemsg(Message messages) {
		/*
		 * try { inbox.setFlags(new Message[] {messages}, new
		 * Flags(Flags.Flag.SEEN), true);
		 * System.out.println(messages.getSubject()+" "+"is Closed"); } catch
		 * (MessagingException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
	}

	public void dispalyMsg(String msg) {
		
		// System.out.println("From "+from);
		// System.out.println("Subject "+subject);

			int start_index = from.indexOf("<");
		int end_index = from.indexOf(">");
		// System.out.println(start_index+" "+end_index);
		// System.out.println(from.substring(start_index+1, end_index));

		Regexpatten patten = new Regexpatten();
		String email = from.substring(start_index + 1, end_index);
		patten = adminService.getregexpatten(email, subject);

		// System.out.println(patten.getFromEmail()+"
		// "+patten.getEmailSubject());
		System.out.println("Message : "+msg);
		System.out.println("Name Patten : "+patten.getNamePatten());
		System.out.println("CNO Patten : "+patten.getCnoPatten());
		System.out.println("Email Patten : "+patten.getEmailPatten());
		int count = 0;
		/* Matcher m = Pattern.compile("((\\bEmail:\\b))([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+)").matcher(msg);
		    while (m.find()) {
		        System.out.println("Email : "+m.group(0)+" "+m.group(1));*
		    }*/
		Pattern p = Pattern.compile("(\\bName\\b)(.?+)");
	      Matcher m = p.matcher(msg);   // get a matcher object
	      
	      if(m.find()) {
	          
	    	  System.out.println("Value "+m.group(0)+" "+m.group(1));
	       }
	      else
	      {
	    	  System.out.println("Value Not Found ");
	      }
	      
	      while(m.find()) {
	          count++;
	          
	          System.out.println("Match number "+count);
	          System.out.println("start(): "+m.start());
	          System.out.println("end(): "+m.end());
	       }
		
		/*Matcher m_name1 = Pattern.compile("(Name: )([A-Z][a-zA-Z]*)").matcher(msg);
		System.out.println("Matches : "+m_name1.toString());
		if (m_name1.find())
			System.out.println("Name1 : " + m_name1.group(2));*/


	/*	msg.replace("*","");
		Matcher m_name = Pattern.compile("(" + patten.getNamePatten() + ")([[a-zA-Z0-9.,_-]+[' ']+[a-zA-Z0-9.,_-]+]*)")
				.matcher(msg);
		if (m_name.find())
			System.out.println("Name : " + m_name.group(2));

		Matcher m_cno = Pattern.compile("(" + patten.getCnoPatten() + ")(\\d+)").matcher(msg);
		if (m_cno.find())
			System.out.println("Contact No : " + m_cno.group(2));

		Matcher m_email = Pattern
				.compile("(" + patten.getEmailPatten() + ")([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+)")
				.matcher(msg);
		if (m_email.find())
			System.out.println("Email : " + m_email.group(2));*/

		/*String fullName = m_name.group(2).toString();

		String[] split = fullName.split(" ");

		String OriginalEemail = emailbdy.substring(0, emailbdy.indexOf("<div"));

		String finalString = OriginalEemail.substring(0, OriginalEemail.lastIndexOf("txtname"));
		String emailDetail = "From :" + m_email.group(2) + "\\nSubject :" + subject + "\\nMail Body :" + finalString;
		System.out.println(finalString);*/
		/*Lead lead = new Lead();
		lead.setcategoryId("" + patten.getCategoryId());
		lead.setstaffId(patten.getStaffId());
		lead.setCreateDate(new Date());
		lead.setemail(m_email.group(2));
		lead.setFirstName(split[0]);
		lead.setLastName(split[1]);
		lead.setLeadProcessStatus("None");
		lead.setMobileNo(m_cno.group(2));
		lead.setLeadState("Open");
		lead.setSheduleDate(null);
		lead.setTallyCalletId(0);
		lead.setCsvData(null);
		lead.setTallyCalletId(patten.getTallycallerId());
		lead.setCsvData(emailDetail);
		lead.setLeadType("EMAIL");
		adminService.AddLead(lead);*/
		
		

	}

}
package com.bonrix.sms.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.Task;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


@Controller
@MultipartConfig
public class FailCallController {
	
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;
	
	
	
	@RequestMapping(value = "GetAndroifFailCallLeads", produces = "application/javascript")
	public @ResponseBody String GetAndroifFailCallLeads(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycalleId") long tallycalleId, @RequestParam(value = "stdate") String stdate,
			@RequestParam(value = "apikey") String api) throws Exception {
		List lead = null;
		try {
			ApiKey apiKey = userService.gettAPiByUid(tallycalleId, api, true);
			if (apiKey == null)
				return "[{\"responseCode\":\"INVALID_KEY\"}]";

			System.out.println("In GetAndroifFailCallLeads..........");
			String query = "";
			/*query = " SELECT lead.leaadId,lead.firstName, lead.lastName,lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,\n" + 
					" followupshistory.callingTime,lead.leadState,lead.autoDial,lead.createDate,followupshistory.newStatus,followupshistory.sheduleTime,"+
					" lead.leadProcessStatus,lead.csvData,lead.city,lead.Country,lead.state,lead.companyName,lead.website,lead.tagName,lead.sheduleDate,lead.leadType\n" + 
					" FROM lead INNER JOIN categorymanager ON ( lead.categoryId = categorymanager.categotyId)INNER JOIN tallycaller ON ( lead.tallyCalletId = tallycaller.tcallerid)\n" + 
					" INNER JOIN followupshistory ON (lead.leaadId = followupshistory.leadId)WHERE tallycaller.tcallerid = " + tallycalleId + " AND lead.companyId = tallycaller.`companyId`\n" + 
					" AND followupshistory.followupsId = (SELECT  MAX(followupshistory.followupsId)FROM followupshistory WHERE followupshistory.leadId = lead.leaadId)\n" + 
					" AND leadState = 'Open' AND ( followupshistory.failstatus IS NOT NULL) AND DATE(followupshistory.`callingTime`) = '" + stdate +"'\n" + 
					" ORDER BY followupshistory.callingTime DESC ";*/
			
			query = " SELECT lead.leaadId,lead.firstName, lead.lastName,lead.mobileNo,lead.email,categorymanager.categortName,followupshistory.failstatus,\n" + 
					" followupshistory.callingTime,lead.leadState,lead.autoDial,lead.createDate,followupshistory.newStatus,"+
					"(SELECT CASE WHEN sheduleTime ='1980-06-30 17:36:00' THEN 'N/A' ELSE followupshistory.sheduleTime END) AS sheduleTime,"+
					" lead.leadProcessStatus,lead.csvData,lead.city,lead.Country,lead.state,lead.companyName,lead.website,lead.tagName,"+
					"(SELECT CASE WHEN sheduleDate ='1980-06-30 17:36:00' THEN 'N/A' ELSE lead.sheduleDate END) AS sheduleDate,lead.leadType\n" + 
					" FROM lead INNER JOIN categorymanager ON ( lead.categoryId = categorymanager.categotyId)INNER JOIN tallycaller ON ( lead.tallyCalletId = tallycaller.tcallerid)\n" + 
					" INNER JOIN followupshistory ON (lead.leaadId = followupshistory.leadId)WHERE tallycaller.tcallerid = " + tallycalleId + " AND lead.companyId = tallycaller.`companyId`\n" + 
					" AND followupshistory.followupsId = (SELECT  MAX(followupshistory.followupsId)FROM followupshistory WHERE followupshistory.leadId = lead.leaadId)\n" + 
					" AND leadState = 'Open' AND ( followupshistory.failstatus IS NOT NULL AND followupshistory.failstatus <> 'None') AND DATE(followupshistory.`callingTime`) = '" + stdate +"'\n" + 
					" ORDER BY followupshistory.callingTime DESC ";
			
			System.out.println(query);
			lead = adminService.excuteListQuery(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		 String jsonArrStr = new Gson().toJson(lead).toString();
	//   System.out.println("jsonArrStr---"+ jsonArrStr.toString());
		 JSONParser parser = new JSONParser();
		 JSONArray jsonArr = (JSONArray)parser.parse(jsonArrStr);
		 JSONArray sortedJsonArray = new JSONArray();
	//	 System.out.println("Size---"+ jsonArr.size());
		 List<JSONObject> jsonValues = new ArrayList<JSONObject>();
		    for (int i = 0; i < jsonArr.size(); i++) {
		        jsonValues.add((JSONObject) jsonArr.get(i));
		    }
		    Collections.sort(jsonValues, new Comparator<JSONObject>() {
		    	private static final String KEY_NAME = "callingTime";
		    	@Override
		        public int compare(JSONObject a, JSONObject b) {
		            String valA = new String();
		            String valB = new String();
		            try {
		                valA = (String) a.get(KEY_NAME);
		                valB = (String) b.get(KEY_NAME);
		            }catch (JSONException e) {
		            }
		            return valA.compareTo(valB);
		        }
		    });
		    for (int i = 0; i < jsonArr.size(); i++) {
		        sortedJsonArray.add(jsonValues.get(i));
		    //	System.out.println("Ordered_Array--"+ jsonValues.get(i));
		    }
		  return sortedJsonArray.toJSONString();
	}
	
	
	/*@RequestMapping(value = "getScheduleWiseLead", produces = "application/javascript")
	public @ResponseBody String getScheduleWiseLead(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "tallycallerName") String tallycallerName, @RequestParam(value = "apikey") String api)throws Exception {

		System.out.println("getScheduleWiseLead is Called..");
		ApiKey apiKey = null;

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);
		String json = null;
		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		System.out.println("sssshhhh---");
		JSONArray sortedJsonArray = new JSONArray();
		if (apiKey != null) {
			System.out.println("in ifffff---");
			Type type = new TypeToken<List<Task>>(){}.getType();
			json = new GsonBuilder().serializeNulls().create().toJson(adminService.getScheduleWiseLead(tclaer.getTcallerid()), type);
		//	System.out.println(json);
			
			 JSONParser parser = new JSONParser();
			 JSONArray jsonArr = (JSONArray)parser.parse(json);
			 
			 List<JSONObject> jsonValues = new ArrayList<JSONObject>();
			    for (int i = 0; i < jsonArr.size(); i++) {
			        jsonValues.add((JSONObject) jsonArr.get(i));
			    }
			    Collections.sort(jsonValues, new Comparator<JSONObject>() {
			    	private static final String KEY_NAME = "sheduleDate";
			    	@Override
			        public int compare(JSONObject a, JSONObject b) {
			            String valA = new String();
			            String valB = new String();
			            try {
			                valA = (String) a.get(KEY_NAME);
			                valB = (String) b.get(KEY_NAME);
			            }catch (JSONException e) {
			            }
			            return valA.compareTo(valB);
			        }
			    });
			    for (int i = 0; i < jsonArr.size(); i++) {
			        sortedJsonArray.add(jsonValues.get(i));
			    }
		}
		  
		System.out.println("sortedJsonArray--"+ sortedJsonArray.toJSONString());
		//return sortedJsonArray.toJSONString();
		return json;
	}*/

}

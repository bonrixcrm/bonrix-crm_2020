package com.bonrix.sms.controller;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.formula.functions.Today;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.service.AdminService;
//import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

@Controller
@RequestMapping("/download.do")
public class FileDownloadController {
	/**
     * Size of a byte buffer to read/write file
     */
    private static final int BUFFER_SIZE = 4096;
             
    /**
     * Path of the file to be downloaded, relative to application's directory
     */
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss") ;
	String date = dateFormat.format(new Date());
	String ft=Calendar.getInstance().getTime().toString();

     String file_Path ="";
   
    /**
     * Method for handling file download request from client
     */
    
	@Autowired
	AdminService adminService;
	
	
	
    @RequestMapping(method = RequestMethod.GET)
    public void doDownload(HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "lId") String ledId,
			@RequestParam(value = "format") String format,
			@RequestParam(value = "flag") String flag) throws IOException {
 System.out.println("format : => "+format);
 System.out.println("ledId : "+ledId);
	System.out.println("format : "+format);
	System.out.println("flag : "+flag);
 
 //if(format.equals("csv")&& flag.equals("All"))
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	MediUser currentUser = (MediUser) auth.getPrincipal();
	long id = currentUser.getUserid();
	int Company_id = (int) (long) id;
//	String fileName="LeadData.csv";
	if(format.equals("csv")&& flag.equals("All"))
	{
		 Date date = Calendar.getInstance().getTime();
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.hhmmss");
		    String dat=date.toString();
		    String s1=dat.replace(":", "_");
		   String  s= s1.replace(" ", "_");
		    System.out.println(s+" "+s1);
		String COMMA_DELIMITER = ",";
		String NEW_LINE_SEPARATOR="\n";
		final String FILE_HEADER = "Id, Category,Company,Created Date,Email,First Name,Last Name,Process State,Lead State,Mobile,CompanyName,Website,Country,State,City";
		FileWriter fileWriter = null;
		String filePath = request.getServletContext().getRealPath("/");
		
		System.out.println(filePath+s+"+your_csv_file.csv");
		fileWriter = new FileWriter(filePath+"\\Sound\\"+s+"_AllData.csv");
		file_Path="\\Sound\\"+s+"_AllData.csv";
		//filePath+=s+"_AllData.csv";
		System.out.println(filePath+"your_csv_file.csv");
		System.out.println("ledId : "+ledId);
		System.out.println("format : "+format);
		System.out.println("flag : "+flag);
		//String s=new Date().getDate();
		// Write the CSV file header
		fileWriter.append(FILE_HEADER.toString());
		// Add a new line separator after the header
		
		System.out.println("SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,"
				+" lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country,"
				+" lead.companyName,lead.website,lead.csvData,tallycaller.username  FROM lead  INNER JOIN  categorymanager "
				+" ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId "
				+" INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE   lead.companyId="+Company_id+"");
		
		
	      List listq=adminService.GetLeadParameterForSearch("SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,"
					+" lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country,"
					+" lead.companyName,lead.website,lead.csvData,tallycaller.username  FROM lead  INNER JOIN  categorymanager "
					+" ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId "
					+" INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE   lead.companyId="+Company_id+"");
	
	      //List listq=null;
			if (listq.size() != 0) {
				 Object[] result = null;
				for (int i = 0; i < listq.size(); i++) {
					JSONObject leadmap = new JSONObject();
					fileWriter.append(NEW_LINE_SEPARATOR);
					result = (Object[]) listq.get(i);
					/*System.out.println("LID : "+result[0].toString());
					System.out.println("FNAME : "+result[1].toString());
					System.out.println("LNAME : "+result[2].toString());
					System.out.println("CATNAME : "+result[3].toString());
					System.out.println("EMAIL : "+result[4].toString());
					System.out.println("MObNO : "+result[5].toString());
					System.out.println("LEAD STATE : "+result[6].toString());
					System.out.println("LP STATE : "+result[7].toString());
					System.out.println("CRDATE : "+result[8].toString());
					System.out.println("SHDATE : "+result[9].toString());
					System.out.println("CMPNAME : "+result[10].toString());
					System.out.println("CTY : "+result[11].toString());
					System.out.println("STATE : "+result[12].toString());
					System.out.println("COUNTYR : "+result[13].toString());
					System.out.println("WEB : "+result[14].toString());
					System.out.println("CSV : "+result[15].toString());*/
				//	System.out.println("USERNAME : "+result[16].toString());
					fileWriter.append(result[0].toString()== null ?  "\""+"N/A"+"\"" :"\""+result[0].toString()+"\"");
					fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[3].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[3].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[10].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[10].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[8].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[8].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[4].toString()== null ?  "\""+"N/A"+"\"" : "\""+ result[4].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[1].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[1].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[2].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[2].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[7].toString()== null ? "\""+"N/A"+"\"" : (char)34+result[7].toString()+ (char)34);
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[6].toString()== null ? "\""+"N/A"+"\"" : "\""+result[6].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[5].toString()== null ? "\""+"N/A"+"\"" : "\""+result[5].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
	                fileWriter.append(result[17].toString()== null ? "\""+"N/A"+"\"" : "\""+result[17].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		          /*  fileWriter.append("'"+result[16].toString()== null ? "N/A" : result[16].toString()+"'");
		            fileWriter.append(COMMA_DELIMITER);*/
		            fileWriter.append("'"+result[14].toString()== null ? "\""+"N/A"+"\"" : "\""+result[14].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		           /* fileWriter.append("'"+result[15].toString()== null ? "N/A" : result[15].toString()+"'");
		            fileWriter.append(COMMA_DELIMITER);*/
		            fileWriter.append(result[13].toString()== null ? "\""+"N/A"+"\"" : "\""+result[13].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[12].toString()== null ? "\""+"N/A"+"\"" : "\""+ result[12].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[11].toString()== null ? "\""+"N/A"+"\"" : "\""+result[11].toString()+"\"");
		            fileWriter.append(NEW_LINE_SEPARATOR);

				}

			}
		fileWriter.flush();
		fileWriter.close();
		download(request,response);
	}
	
	 if (format.equals("csv") && flag.equals("Selected"))
	{
		System.out.println("Seleceted Called...");
		
		String COMMA_DELIMITER = ",";
		String NEW_LINE_SEPARATOR="\n";
		final String FILE_HEADER = "Id, Category,Company,Created Date,Email,First Name,Last Name,Process State,Lead State,Mobile,Telecaller,Other Data,CompanyName,Website,Country,State,City";
		FileWriter fileWriter = null;
		String filePath = request.getServletContext().getRealPath("/");
		filePath = filePath + "\\Sound\\";
		
		 Date date = Calendar.getInstance().getTime();
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.hhmmss");
		    String dat=date.toString();
		    String s1=dat.replace(":", "_");
		   String  s= s1.replace(" ", "_");
		    System.out.println(s+" "+s1);
				
			fileWriter = new FileWriter(filePath+s+"_SelectedData.csv");
			file_Path="\\Sound\\"+s+"_SelectedData.csv";
		System.out.println(filePath+"your_csv_file.csv");
		// Write the CSV file header
		fileWriter.append(FILE_HEADER.toString());
		// Add a new line separator after the header
		/*for (String retval: ledId.split(","))
		{*/
		String IN="";
		 for (String retval: ledId.split(","))
			{
			 	IN+=retval+",";
			}
		 IN = IN.substring(0,IN.length() - 1);
		 System.out.println("IN : "+IN);
			 List listq=adminService.GetLeadParameterForSearch("SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,"
						+" lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country,"
						+" lead.companyName,lead.website,lead.csvData,tallycaller.username  FROM lead  INNER JOIN  categorymanager "
						+" ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId "
						+" INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE lead.leaadId In ("+IN+") AND   lead.companyId="+Company_id+"");
			System.out.println("SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,"
					+" lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country,"
					+" lead.companyName,lead.website,lead.csvData,tallycaller.username  FROM lead  INNER JOIN  categorymanager "
					+" ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId "
					+" INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE lead.leaadId In ("+IN+") AND   lead.companyId="+Company_id+"");
			if (listq.size() != 0) {
				 Object[] result = null;
				for (int i = 0; i < listq.size(); i++) {
					JSONObject leadmap = new JSONObject();
					fileWriter.append(NEW_LINE_SEPARATOR);
					result = (Object[]) listq.get(i);
					fileWriter.append(result[0].toString()== null ?  "\""+"N/A"+"\"" :"\""+result[0].toString()+"\"");
					fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[3].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[3].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[10].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[10].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[8].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[8].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[4].toString()== null ?  "\""+"N/A"+"\"" : "\""+ result[4].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[1].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[1].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[2].toString()== null ?  "\""+"N/A"+"\"" : "\""+result[2].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[7].toString()== null ? "\""+"N/A"+"\"" : (char)34+result[7].toString()+ (char)34);
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[6].toString()== null ? "\""+"N/A"+"\"" : "\""+result[6].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[5].toString()== null ? "\""+"N/A"+"\"" : "\""+result[5].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
	                fileWriter.append(result[17].toString()== null ? "\""+"N/A"+"\"" : "\""+result[17].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		          /*  fileWriter.append("'"+result[16].toString()== null ? "N/A" : result[16].toString()+"'");
		            fileWriter.append(COMMA_DELIMITER);*/
		            fileWriter.append("'"+result[14].toString()== null ? "\""+"N/A"+"\"" : "\""+result[14].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		           /* fileWriter.append("'"+result[15].toString()== null ? "N/A" : result[15].toString()+"'");
		            fileWriter.append(COMMA_DELIMITER);*/
		            fileWriter.append(result[13].toString()== null ? "\""+"N/A"+"\"" : "\""+result[13].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[12].toString()== null ? "\""+"N/A"+"\"" : "\""+ result[12].toString()+"\"");
		            fileWriter.append(COMMA_DELIMITER);
		            fileWriter.append(result[11].toString()== null ? "\""+"N/A"+"\"" : "\""+result[11].toString()+"\"");
		            fileWriter.append(NEW_LINE_SEPARATOR);				}

			}
	//	}
		fileWriter.flush();
		fileWriter.close();
		download(request,response);
	}
	
	//	}
	 

}
    
	public void download(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
	// get absolute path of the application
     ServletContext context = request.getServletContext();
     String appPath = context.getRealPath("");
     System.out.println("appPath = " + appPath);

     // construct the complete absolute path of the file
     String fullPath = appPath + file_Path;      
     File downloadFile = new File(fullPath);
     FileInputStream inputStream = new FileInputStream(downloadFile);
      
     // get MIME type of the file
     String mimeType = context.getMimeType(fullPath);
     if (mimeType == null) {
         // set to binary type if MIME mapping not found
         mimeType = "application/octet-stream";
     }
     System.out.println("MIME type: " + mimeType);

     // set content attributes for the response
     response.setContentType(mimeType);
     response.setContentLength((int) downloadFile.length());

     // set headers for the response
     String headerKey = "Content-Disposition";
     String headerValue = String.format("attachment; filename=\"%s\"",
             downloadFile.getName());
     response.setHeader(headerKey, headerValue);

     // get output stream of the response
     OutputStream outStream = response.getOutputStream();

     byte[] buffer = new byte[BUFFER_SIZE];
     int bytesRead = -1;

     // write bytes read from the input stream into the output stream
     while ((bytesRead = inputStream.read(buffer)) != -1) {
         outStream.write(buffer, 0, bytesRead);
     }

     inputStream.close();
     outStream.close();
	}
}


package com.bonrix.sms.controller;

import java.text.DateFormat;
import java.util.List;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.dto.LeadDTO;
import com.bonrix.sms.service.AdminService; 

@Controller
public class FinanceCRMEmailController {

	@Autowired
	AdminService adminService;
	
	@RequestMapping(value = "sendScheduleWiseLeadToFinanceMarket", produces = "application/javascript")
	public @ResponseBody String sendScheduleWiseLeadToFinanceMarket()
			throws Exception {
		System.out.println("In sendScheduleWiseLeadToFinanceMarket");
		List<LeadDTO> leads=adminService.getFininceScheduleWiseLead(263);
		
		try {
			JavaMailSenderImpl sender = new JavaMailSenderImpl();
			sender.setHost("smtp.gmail.com");
			sender.setUsername("demobonrix123@gmail.com");
			sender.setPassword("bonrix@123");
			sender.setJavaMailProperties(javaMailProperties());
			sender.send(new MimeMessagePreparator() {

				@Override
				public void prepare(MimeMessage mimeMessage) {
					try {
						 String html="";
						for (LeadDTO lead : leads) {
							System.out.println(lead);
							html+="<tr>" + 
							"<td style='border: 1px solid #ddd; text-align: left;'>"+lead.getFirstName()+" "+lead.getLastName()+"</td>" + 
							"<td style='border: 1px solid #ddd; text-align: left;'>"+lead.getMobileNo()+"</td>" + 
							"<td style='border: 1px solid #ddd; text-align: left;'>"+DateFormat.getInstance().format(lead.getCreateDate())+"</td>" + 
							"<td style='border: 1px solid #ddd; text-align: left;'>"+DateFormat.getInstance().format(lead.getSheduleDate())+"</td>" + 
							"<td style='border: 1px solid #ddd; text-align: left;'>"+lead.getLeadProcessStatus()+"</td>" + 
							"<td style='border: 1px solid #ddd; text-align: left;'>"+lead.getCategoryName()+"</td>" +
							"<td style='border: 1px solid #ddd; text-align: left;'>"+lead.getUsername()+"</td>" +
							"</tr>";
						}
						MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
						message.setFrom("demobonrix123@gmail.com");
						message.setTo("kamal.thakkar80@gmail.com");
						message.setSubject("Today Lead Reminder");
						message.setCc("sajanparmar7@gmail.com");
						message.setText("<h4 style='color: #5e9ca0;'><span style='color: #000000;'>Dear Kamal Thakkar,</span></h4>" + 
								"<h3>Today's Follow-up Leads List</h3>" + 
								"<p>You need to follow up with following contacts / prospects Today.</p>" + 
								"<table style='border-collapse: collapse; width: 100%; border: 1px solid #ddd; text-align: left;'>" + 
								"<tbody>" + 
								"<tr>" + 
								"<td style='border: 1px solid #ddd; text-align: left; background-color: #4caf50;'><span style='color: #ffffff;'>Name</span></td>" + 
								"<td style='border: 1px solid #ddd; text-align: left; background-color: #4caf50;'><span style='color: #ffffff;'>Mobile</span></td>" + 
								"<td style='border: 1px solid #ddd; text-align: left; background-color: #4caf50;'><span style='color: #ffffff;'>Entry date</span></td>" + 
								"<td style='border: 1px solid #ddd; text-align: left; background-color: #4caf50;'><span style='color: #ffffff;'>Follow-Up Date</span></td>" + 
								"<td style='border: 1px solid #ddd; text-align: left; background-color: #4caf50;'><span style='color: #ffffff;'>Status</span></td>" + 
								"<td style='border: 1px solid #ddd; text-align: left; background-color: #4caf50;'><span style='color: #ffffff;'>Category</span></td>" + 
								"<td style='border: 1px solid #ddd; text-align: left; background-color: #4caf50;'><span style='color: #ffffff;'>Telecaller</span></td>" + 
								"</tr>" + 
								"<tr>" + 
								  html+
								"</tr>" + 
								"</tbody>" + 
								"</table>", true);
					} catch (Exception e) {
					}
				}
			});
			System.out.println("Mail Successfully Sent...");
			adminService.saveEmailLog("N/A", 0, "Hello Sajan.This is Email Reminder.", "Today Lead Reminder",
					0, "Success", 263, "N/A");
		} catch (MailException e) {
			System.out.println("IN ERROR MailException");
			adminService.saveEmailLog("N/A", 0, "Hello Sajan.This is Email Reminder.", "Today Lead Reminder",
					0, "Failed", 263, e.getMessage().toString());
			System.out.println("IN ERROR ::: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("IN ERROR Exception");
			System.out.println("IN ERROR ::: " + e.getMessage());
			e.printStackTrace();
		}
		return "";

	}

	private Properties javaMailProperties() {
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		/* properties.setProperty("mail.debug", "true"); */
		properties.setProperty("mail.smtp.ssl.trust", "mail.shyamshoppe.com");
		properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.setProperty("templateMode", "HTML5l");
		/* mimeMessage.setContent(htmlMsg, "text/html"); */
		properties.setProperty("mail.smtp.auth", "true");   
		properties.put("mail.smtp.port", "465");
		return properties;
	}

}

package com.bonrix.sms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.Lead;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;

@Controller
@MultipartConfig
public class HashmukhController {
	
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	SMSService smsService;
	
	@Autowired
	CommonService cservice;

	final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMddHHmm");
	final SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	
//	http://localhost:8080/spring-hib/AddSecureTradeLeadData?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/AddSecureTradeLeadData", method = RequestMethod.GET)
	public @ResponseBody String AddSecureTradeLeadData(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName,
			@RequestParam(value = "emailId") String emailId, @RequestParam(value = "contactNo") String contactNo,
			@RequestParam(value = "categoryId") String categoryId,@RequestParam(value = "sheduleDate") String sheduleDate,
			@RequestParam(value = "companyName") String companyName, @RequestParam(value = "WebURL") String WebURL,
	        @RequestParam(value = "country") String country, @RequestParam(value = "state") String state,
	        @RequestParam(value = "city") String city)throws ParseException, InterruptedException {
		
		
		if (contactNo.matches("\\d{10}")) {
			 System.out.println("Mobile Num Valid");
		 }else {
			 return "Invalid Mobile Number";
		 }
		
		/*Lead efs = (Lead) cservice.getSingleObject("FROM Lead Where categoryId='"+categoryId+"' AND mobileNo='"+contactNo+"' ");
		 if(!(efs==null)) {
			 return "Lead Already Exist In This Category"; 
	    }*/
		
		List<Object[]> leadCount =  adminService.getHashmukhLeadCount(categoryId, contactNo);
		if(leadCount.size()>0) {
			 return "Lead Already Exist In This Category"; 
	    }
		
			String teleId="0";
			if(Integer.parseInt(categoryId)==1457) {
				teleId="1394";
			}else if(Integer.parseInt(categoryId)==1458){
				teleId="1395";
			}else {
				teleId="0";
			}

				   Lead ld =new Lead();
				   
				   ld.setcategoryId(categoryId);
				   ld.setCompanyId(331);
				   ld.setCreateDate(new Date());
				   ld.setemail(emailId);
				   ld.setFirstName(firstName);
				   ld.setLastName(lastName);
				   ld.setLeadProcessStatus("None");
				   ld.setLeadState("open");
				   ld.setMobileNo(contactNo);
				   ld.setSheduleDate(scheduledf.parse(sheduleDate));
				   ld.setTallyCalletId(Integer.parseInt(teleId));
				   ld.setCsvData("{}");
				   ld.setstaffId(0);
				   ld.setLeadType("IVR");
				   ld.setAutoDial(false);
				   ld.setCountry(country);
				   ld.setCity(city);
				   ld.setCompanyName(companyName);
				   ld.setState(state);
				   ld.setWebsite(WebURL);
				   ld.setLeadcomment("NA");
				   ld.setTagName("NA");
				   ld.setNotification_flag("TRUE");
				   ld.setDialState(false);
				   ld.setAssignDate(new Date());
				   
				   adminService.AddLead(ld);
				   return "Lead Successfully Added.";
	}

}

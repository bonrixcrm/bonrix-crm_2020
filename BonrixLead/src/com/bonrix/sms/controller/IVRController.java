package com.bonrix.sms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;

@Controller
@MultipartConfig
public class IVRController {
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	SMSService smsService;
	
	@Autowired
	CommonService cs;

	final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMddHHmm");
	final SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	// http://localhost:8191/spring-hib/TelecallerStartWorkRequest?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
	@RequestMapping(value = "/AddIVRFollowupData", method = RequestMethod.GET)
	public @ResponseBody String AddIVRFollowupData(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "IVRNo") String IVRNo, @RequestParam(value = "CustomerNo") String CustomerNo,
			@RequestParam(value = "stsrtTime") String stsrtTime, @RequestParam(value = "endTime") String endTime,
			@RequestParam(value = "duration") String duration,
			@RequestParam(value = "ExecutiveMobNo") String ExecutiveMobNo,
			@RequestParam(value = "audioFIleName") String audioFIleName, @RequestParam(value = "status") String status)
			throws ParseException, InterruptedException {
		System.out.println("AddIVRFollowupData Extra is caled...");

		System.out.println("IVRNo : " + IVRNo);
		System.out.println("CustomerNo : " + CustomerNo);
		System.out.println("stsrtTime : " + stsrtTime);
		System.out.println("endTime : " + endTime);
		System.out.println("duration : " + duration);
		System.out.println("ststus : " + status);
		System.out.println("ExecutiveMobNo : " + ExecutiveMobNo);
		System.out.println("audioFIleName : " + audioFIleName);
		System.out.println();
		String IVRcategory = "";
		if (IVRNo.equalsIgnoreCase("8824005042")) {
			IVRcategory = "49";
		} else {
			IVRcategory = "50";
		}

		List list = adminService.getLeadByContactNo(CustomerNo);
		Date cdate = df.parse(stsrtTime);
		Date sdate = df.parse("2016-12-15 00:00:00");
		System.out.println("Size : " + list.size());
		int mobAccr = 0;
		int leadId = 0;

		String mob = CustomerNo;
		String subMob = "";
		if (mob.length() == 11) {
			String mobNo = mob.replaceAll("[^0-9]", "");
			String SubmobNo = null;
			if (mobNo.length() == 11 && Pattern.matches("[a-zA-Z]+", mobNo) == false)
				SubmobNo = mobNo.substring(1, mobNo.length());

			System.out.println("Ext mob : " + mobNo);
			System.out.println("Sub mob : " + SubmobNo);
			// subMob=mobNo.substring((mobNo.length())-10);
			subMob = SubmobNo;
			System.out.println("Indo mob : " + subMob);
		} else if (mob.length() == 12) {
			String mobNo = mob.replaceAll("[^0-9]", "");
			String SubmobNo = null;
			if (mobNo.length() == 12 && Pattern.matches("[a-zA-Z]+", mobNo) == false)
				SubmobNo = mobNo.substring(2, mobNo.length());

			System.out.println("Ext mob : " + mobNo);
			System.out.println("Sub mob : " + SubmobNo);
			subMob = SubmobNo;
		} else if (mob.length() == 10) {
			subMob = CustomerNo;
		} else {
			return "Discard Row Due to Invalid Mobile Number";
		}

		Tallycaller telecaller = new Tallycaller();
		telecaller = adminService.GetTelecallerByMobileNo(ExecutiveMobNo);
		System.out.println("telecaller : " + telecaller);

		for (int i = 0; i < list.size(); i++) {
			JSONObject leadmap = new JSONObject();
			Object[] result = (Object[]) list.get(i);
			System.out.println(result[9] + " " + result[1]);
			String mobileNo = result[9].toString();
			String category = result[1].toString();

			if (mobileNo.equalsIgnoreCase(subMob) && category.equalsIgnoreCase("49")) {
				mobAccr++;
				leadId = Integer.parseInt(result[0].toString());
			}

		}

		if (mobAccr == 0) {
			System.out.println("Number Not Found");

			if (telecaller != null) {

				Lead lead = new Lead();
				lead.setcategoryId(IVRcategory);
				lead.setCompanyId((int) (long) telecaller.getCompanyId());
				lead.setCreateDate(new Date());
				lead.setemail("N/A");
				lead.setFirstName("N/A");
				lead.setLastName("N/A");
				lead.setLeadProcessStatus("None");
				lead.setMobileNo(subMob);
				lead.setLeadState("Open");
				lead.setSheduleDate(df.parse("1980-06-30 17:36:00"));
				lead.setTallyCalletId(telecaller.getTcallerid());
				lead.setCsvData("IVRNO,\\n" + IVRNo + ",");
				lead.setLeadType("IVRWEB");
				lead.setCountry("N/A");
				lead.setState("N/A");
				lead.setCity("N/A");
				lead.setCompanyName("N/A");
				lead.setWebsite("N/A");
				adminService.AddLead(lead);
				System.out.println("TCID " + lead.getLeaadId());
				if (lead.getLeaadId() != 0) {
					if (status.equalsIgnoreCase("ANSWERED")) {
						adminService.addFollowupsHistory(telecaller.getUsername(), stsrtTime, "N/A",
								"1980-06-30 17:36:00", "N/A", audioFIleName, Integer.parseInt(duration), "Success",
								lead.getLeaadId(), "InBound", telecaller.getCompanyId(), 0, "N/A", "WEB");
						adminService.assignLeadUpdate((long) lead.getLeaadId(), (long) telecaller.getTcallerid());
						return "Lead and Success Followup Successfully Added.";
					} else {
						Followupshistory history = new Followupshistory();
						history.setTallycallerId(telecaller.getTcallerid());
						history.setAudoiFilePath("N/A");
						history.setCallDuration(00);
						history.setCallingTime(cdate);
						history.setCallStatus("Fail");
						history.setNewStatus("N/A");
						history.setFailstatus("N/A");
						history.setRemark("N/A");
						history.setSheduleTime(sdate);
						history.setLeadId(lead.getLeaadId());
						history.setCompanyId(telecaller.getCompanyId());
						history.setFollowsUpType("InBound");
						history.setFollowAddSecond(0);
						adminService.AddFailFollowUpAPI(history);
						return "Lead and Fail Followup Successfully Added.";
					}
				}
				return "Number Not Found : " + lead.getLeaadId();
			} else {
				return "Executive Mobile Number is Not Matched With CRM Telecaller Number";
			}
		} else {
			if (telecaller != null) {
				System.out.println("Number  Found");
				if (status.equalsIgnoreCase("ANSWERED")) {
					adminService.addFollowupsHistory(telecaller.getUsername(), stsrtTime, "N/A", "1993-05-29 00:00:00",
							"N/A", audioFIleName, Integer.parseInt(duration), "Success", leadId, "InBound",
							telecaller.getCompanyId(), 0, "N/A", "WEB");
					adminService.assignLeadUpdate((long) leadId, (long) telecaller.getTcallerid());

					return "Success Followup Successfully Added.";
				} else {
					Followupshistory history = new Followupshistory();
					history.setTallycallerId(telecaller.getTcallerid());
					history.setAudoiFilePath("N/A");
					history.setCallDuration(00);
					history.setCallingTime(cdate);
					history.setCallStatus("Fail");
					history.setNewStatus("N/A");
					history.setFailstatus("N/A");
					history.setRemark("N/A");
					history.setSheduleTime(sdate);
					history.setLeadId(leadId);
					history.setCompanyId(telecaller.getCompanyId());
					history.setFollowsUpType("InBound");
					history.setFollowAddSecond(0);
					adminService.AddFailFollowUpAPI(history);
					return "Fail Followup Successfully Added.";
				}

				// return "Number Found";
			} else {
				return "Executive Mobile Number is Not Matched With CRM Telecaller Number";
			}
		}

		// return "[{\"responceCode\":\"Start WOrk\"}]";

	}
	
	/*@RequestMapping(value = "/CloudIVRFollowupData", method = RequestMethod.GET)
	public @ResponseBody String CloudIVRFollowupData(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "CalllogId") String CalllogId, @RequestParam(value = "CallingNumber") String CallingNumber,
			@RequestParam(value = "AgentNumber") String AgentNumber, @RequestParam(value = "IvrDuration") String IvrDuration,
			@RequestParam(value = "CallDuration") String CallDuration,@RequestParam(value = "CallStatus") String CallStatus,
			@RequestParam(value = "State") String State, @RequestParam(value = "IbizfoneNumber") String IbizfoneNumber,
	        @RequestParam(value = "CallRecordingURL") String CallRecordingURL, @RequestParam(value = "Department") String Department)throws ParseException, InterruptedException {
		
		   String teleId="0";
		   List<Object[]> teleByMob = new ArrayList<>();
		   teleByMob=adminService.getTallyCallerByMobile(AgentNumber);
			  for(Object[] teleListresult : teleByMob) {
					 teleId=teleListresult[0].toString().trim();
			  }

			  if(teleId != null) {
				  Lead ld =new Lead();
				   
				   ld.setcategoryId("0");
				   ld.setCompanyId(314);
				   ld.setCreateDate(new Date());
				   ld.setemail("");
				   ld.setFirstName("");
				   ld.setLastName("");
				   ld.setLeadProcessStatus("None");
				   ld.setLeadState("open");
				   ld.setMobileNo(CallingNumber);
				   ld.setSheduleDate(scheduledf.parse("1980-06-30 17:36:00"));
				   ld.setTallyCalletId(Integer.parseInt(teleId));
				   ld.setCsvData("{}");
				   ld.setstaffId(0);
				   ld.setLeadType("IVR");
				   ld.setAutoDial(false);
				   ld.setCountry("NA");
				   ld.setCity("NA");
				   ld.setCompanyName("NA");
				   ld.setState("NA");
				   ld.setWebsite("NA");
				   ld.setLeadcomment("NA");
				   ld.setTagName("NA");
				   ld.setNotification_flag("TRUE");
				   ld.setDialState(false);
				   ld.setAssignDate(new Date());
				   
				   adminService.AddLead(ld);
				   
				   System.out.println("TCID " + ld.getLeaadId());
				   int leadId = ld.getLeaadId();
				   String callStat="";
				   String succStat="";
				   String failStat="";
				   
				   if(Integer.parseInt(CallDuration) == 0) {
					   callStat="Fail";
				   }else {
					   callStat="Success";
				   }
				   
				   if(CallStatus.equalsIgnoreCase("answer")) {
					   succStat="Answer";
					   failStat="None";
				   }else if(CallStatus.equalsIgnoreCase("missed") && CallStatus.equalsIgnoreCase("dropped")) {
					   succStat="None";
					   failStat=CallStatus;
				   }else {
					   succStat="None";
					   failStat="None";
				   }
				   
					Followupshistory history = new Followupshistory();
					
					history.setAudoiFilePath("NA");
					history.setCallDuration(Integer.parseInt(CallDuration));
					history.setCallingTime(new Date());
					history.setCallStatus(callStat);
					history.setCompanyId(0);
					history.setFailstatus(failStat);
					history.setFollowAddSecond(0);
					history.setFollowsUpType("");
					history.setFollowupDevice("IVR");
					history.setLeadId(leadId);
					history.setNewStatus("NA");
					history.setRemark("NA");
					history.setSheduleTime(scheduledf.parse("1980-06-30 17:36:00"));
					history.setSuccessStatus(succStat);
					history.setTallycallerId(Integer.parseInt(teleId));
				   
					adminService.AddFailFollowUpAPI(history);
					return "Lead Successfully Added.";
			  }else {
				  Lead ld =new Lead();
				   
				   ld.setcategoryId("0");
				   ld.setCompanyId(0);
				   ld.setCreateDate(new Date());
				   ld.setemail("");
				   ld.setFirstName("");
				   ld.setLastName("");
				   ld.setLeadProcessStatus("None");
				   ld.setLeadState("open");
				   ld.setMobileNo(CallingNumber);
				   ld.setSheduleDate(scheduledf.parse("1980-06-30 17:36:00"));
				   ld.setTallyCalletId(Integer.parseInt(teleId));
				   ld.setCsvData("NA");
				   ld.setstaffId(0);
				   ld.setLeadType("IVR");
				   ld.setAutoDial(false);
				   ld.setCountry("NA");
				   ld.setCity("NA");
				   ld.setCompanyName("NA");
				   ld.setState("NA");
				   ld.setWebsite("NA");
				   ld.setLeadcomment("NA");
				   ld.setTagName("NA");
				   ld.setNotification_flag("TRUE");
				   ld.setDialState(false);
				   ld.setAssignDate(new Date());
				   
				   adminService.AddLead(ld);
				   
				   System.out.println("TCID " + ld.getLeaadId());
				   int leadId = ld.getLeaadId();
				   String callStat="";
				   String succStat="";
				   String failStat="";
				   
				   if(Integer.parseInt(CallDuration) == 0) {
					   callStat="Fail";
				   }else {
					   callStat="Success";
				   }
				   if(CallStatus.equalsIgnoreCase("answer")) {
					   succStat="Answer";
					   failStat="None";
				   }else if(CallStatus.equalsIgnoreCase("missed") && CallStatus.equalsIgnoreCase("dropped")) {
					   succStat="None";
					   failStat=CallStatus;
				   }else {
					   succStat="None";
					   failStat="None";
				   }
				   
					Followupshistory history = new Followupshistory();
					
					history.setAudoiFilePath("NA");
					history.setCallDuration(Integer.parseInt(CallDuration));
					history.setCallingTime(new Date());
					history.setCallStatus(callStat);
					history.setCompanyId(0);
					history.setFailstatus(failStat);
					history.setFollowAddSecond(0);
					history.setFollowsUpType("");
					history.setFollowupDevice("IVR");
					history.setLeadId(leadId);
					history.setNewStatus("NA");
					history.setRemark("NA");
					history.setSheduleTime(scheduledf.parse("1980-06-30 17:36:00"));
					history.setSuccessStatus(succStat);
					history.setTallycallerId(0);
				   
				    return "Lead Successfully Added.";
				//  return "Agent Mobile Number is Not Matched With CRM Telecaller Number";
			  }
	}*/

	
//	http://localhost:8080/spring-hib/AddSecureTradeLeadData?telecallId=3&&apikey=8b397b5d-5c9b-40be-9de1-575c595f6fa3
/*	@RequestMapping(value = "/AddSecureTradeLeadData", method = RequestMethod.GET)
	public @ResponseBody String AddSecureTradeLeadData(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName,
			@RequestParam(value = "emailId") String emailId, @RequestParam(value = "contactNo") String contactNo,
			@RequestParam(value = "categoryId") String categoryId,@RequestParam(value = "sheduleDate") String sheduleDate,
			@RequestParam(value = "companyName") String companyName, @RequestParam(value = "WebURL") String WebURL,
	        @RequestParam(value = "country") String country, @RequestParam(value = "state") String state,
	        @RequestParam(value = "city") String city)throws ParseException, InterruptedException {
		
		
			String teleId="0";
			if(Integer.parseInt(categoryId)==1457) {
				teleId="1394";
			}else if(Integer.parseInt(categoryId)==1458){
				teleId="1395";
			}else {
				teleId="0";
			}

				   Lead ld =new Lead();
				   
				   ld.setcategoryId(categoryId);
				   ld.setCompanyId(331);
				   ld.setCreateDate(new Date());
				   ld.setemail(emailId);
				   ld.setFirstName(firstName);
				   ld.setLastName(lastName);
				   ld.setLeadProcessStatus("None");
				   ld.setLeadState("open");
				   ld.setMobileNo(contactNo);
				   ld.setSheduleDate(scheduledf.parse(sheduleDate));
				   ld.setTallyCalletId(Integer.parseInt(teleId));
				   ld.setCsvData("{}");
				   ld.setstaffId(0);
				   ld.setLeadType("IVR");
				   ld.setAutoDial(false);
				   ld.setCountry(country);
				   ld.setCity(city);
				   ld.setCompanyName(companyName);
				   ld.setState(state);
				   ld.setWebsite(WebURL);
				   ld.setLeadcomment("NA");
				   ld.setTagName("NA");
				   ld.setNotification_flag("TRUE");
				   ld.setDialState(false);
				   ld.setAssignDate(new Date());
				   
				   adminService.AddLead(ld);
				   return "Lead Successfully Added.";
	}*/
	
}

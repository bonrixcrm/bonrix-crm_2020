package com.bonrix.sms.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.LeadClone;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.service.AdminService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class IndiaMartAPIController  {

	@Autowired
	AdminService adminService;

	@RequestMapping(value = "/glypticartsIndaiMartLeads", method = RequestMethod.GET)
	public @ResponseBody String glypticartsIndaiMartLeads(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startDate") String startDate, @RequestParam(value = "endDate") String endDate)
			throws ParseException,CloneNotSupportedException {
		System.out.println("glypticartsIndaiMartLeads is Called");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long CompId = currentUser.getUserid();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		List<Lead> leads = IndiaMartJsonReader.GetGlypticartsIndaiMartLeads(startDate, endDate);
		if(leads.size()!=0)
		{
			
		for(Lead lead:leads)
		{
			
			List duplicateLead = adminService.getLeadForCSV((int) (long)CompId , "3011", lead.getMobileNo().toString());
			System.out.println("duplicateLead :: "+duplicateLead.size());
			if(duplicateLead.size()==0 && duplicateLead!=null && lead.getMobileNo().length()==10)
			adminService.AddLead(lead);
			
		}
		return gson.toJson(leads);
		}else
		{
			return "Error from India Mart.";
		}
	}
	
	
	@RequestMapping(value = "/satcopIndaiMartLeads", method = RequestMethod.GET)
	public @ResponseBody String satcopIndaiMartLeads(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startDate") String startDate, @RequestParam(value = "endDate") String endDate)
			throws ParseException,CloneNotSupportedException {   
		System.out.println("satcopIndaiMartLeads is Called");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long CompId = currentUser.getUserid();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		List<Lead> leads = IndiaMartJsonReader.GetSatcopIndaiMartLeads(startDate, endDate);
		if(leads.size()!=0){
			
		for(Lead lead:leads){
			List duplicateLead = adminService.getLeadForCSV((int) (long)CompId , "3185", lead.getMobileNo().toString());
			System.out.println("duplicateLead :: "+duplicateLead.size());
			if(duplicateLead.size()==0 && duplicateLead!=null && lead.getMobileNo().length()==10)
			adminService.AddLead(lead);
		}
		return gson.toJson(leads);
		}else
		{
			return "Error from India Mart.";
		}
	}
}

package com.bonrix.sms.controller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.bonrix.sms.model.Lead;

public class IndiaMartJsonReader {

  private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  public static  org.json.simple.JSONObject readJsonFromUrl(String url)  {
    InputStream is = null;
    org.json.simple.JSONObject dataArray=null;
    JSONParser jsonParser = new JSONParser();
	try {
		is = new URL(url).openStream();
	
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      System.out.println(jsonText);
      try {
		 dataArray=( org.json.simple.JSONObject) jsonParser.parse(jsonText);
	} catch (ParseException e) {
				e.printStackTrace();
				return dataArray;
	}
       return dataArray;
    } catch (IOException e) {
		e.printStackTrace();
		return dataArray;
	} finally {
      try {
		is.close();
	} catch (IOException e) {
		e.printStackTrace();
		return dataArray;
	}
    }
  }

  public static List<Lead> GetGlypticartsIndaiMartLeads(String startDate,String endDate) throws java.text.ParseException
  {

	  org.json.simple.JSONObject indiaMartJsonObject = readJsonFromUrl("https://mapi.indiamart.com/wservce/crm/crmListing/v2/?glusr_crm_key=mR20FLxu5nnHT/eu5HSC7lyNolXAlTI=&start_time="+startDate+"&end_time="+endDate+"");
	  JSONArray finalDataArray=(JSONArray) indiaMartJsonObject.get("RESPONSE");
	  List<Lead> leads = new ArrayList<Lead>();
	  System.out.println(finalDataArray);
	  
    if(finalDataArray==null)
    	System.out.println("Invalid Responce From IndiaMart :: "+finalDataArray);
    else if(finalDataArray.size()==1)
    {
    	for (int i = 0; i < finalDataArray.size(); i++) {
            org.json.simple.JSONObject jsonobject = (org.json.simple.JSONObject) finalDataArray.get(i);
            String Error_Message = jsonobject.get("Error_Message").toString();
            System.out.println(Error_Message);
    	}
    }
    else 
    {  
    	for (int i = 0; i < finalDataArray.size(); i++) {
    		
            org.json.simple.JSONObject jsonobject = (org.json.simple.JSONObject) finalDataArray.get(i);
            String SENDERNAME = ""+jsonobject.get("SENDER_NAME");
            String GLUSR_USR_COMPANYNAME = ""+jsonobject.get("GLUSR_USR_COMPANYNAME");
            String MOB =""+ jsonobject.get("SENDER_MOBILE");
            String ENQ_CITY =""+ jsonobject.get("SENDER_CITY");
            String ENQ_STATE =""+ jsonobject.get("SENDER_STATE");
            String SENDEREMAIL = ""+jsonobject.get("SENDER_EMAIL");
            String MOBILE_ALT =""+ jsonobject.get("SENDER_MOBILE_ALT");
            String LOG_TIME = ""+jsonobject.get("QUERY_TIME");
            String REQUIRMENT = ""+jsonobject.get("SUBJECT");
           
            SimpleDateFormat formatter6=new SimpleDateFormat("yyyyMMddHHmmss");  
            SimpleDateFormat formatter7=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
            final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            
            
            Lead dataLead=new Lead();
            dataLead.setAltmobileNo(MOBILE_ALT.length()>10?MOBILE_ALT.substring(4):MOBILE_ALT);
            dataLead.setAssignDate(new Date());
            dataLead.setAutoDial(true);
            dataLead.setcategoryId("3185");
            dataLead.setCity(ENQ_CITY.equalsIgnoreCase("null")?"NA":ENQ_CITY);
            dataLead.setCompanyId(400);
            dataLead.setCompanyName(GLUSR_USR_COMPANYNAME.equalsIgnoreCase("null")?"NA":GLUSR_USR_COMPANYNAME);
            dataLead.setCountry("NA");
            dataLead.setCreateDate(scheduledf.parse(LOG_TIME));
            dataLead.setCsvData(REQUIRMENT);
            dataLead.setDialState(false);
            dataLead.setEmail(SENDEREMAIL.equalsIgnoreCase("null")?"NA":SENDEREMAIL);
            dataLead.setFirstName(SENDERNAME.equalsIgnoreCase("null")?"NA":SENDERNAME);
            dataLead.setLastName("NA");
            dataLead.setLeadcomment("NA");
            dataLead.setLeadProcessStatus("None");
            dataLead.setLeadState("Open");
            dataLead.setMobileNo(MOB.length()>10?MOB.substring(4):MOB);
            dataLead.setNotification_flag("FALSE");
            dataLead.setSheduleDate(formatter6.parse("19800630173600"));
            dataLead.setStaffId(0);
            dataLead.setState(ENQ_STATE.equalsIgnoreCase("null")?"NA":ENQ_STATE);
            dataLead.setTagName("NA");
            dataLead.setTallyCalletId(0);
            dataLead.setWebsite("NA");   
            dataLead.setLeadType("IndiaMart");
            System.out.println(formatter7.format(dataLead.getCreateDate()));
            leads.add(dataLead);
    	} 
    }
  return leads;
  }
  
  
  public static List<Lead> GetSatcopIndaiMartLeads(String startDate,String endDate) throws java.text.ParseException
  {

	  org.json.simple.JSONObject indiaMartJsonObject = readJsonFromUrl("https://mapi.indiamart.com/wservce/crm/crmListing/v2/?glusr_crm_key=mR20FLxu5nnHT/eu5HSC7lyNolXAlTI=&start_time="+startDate+"&end_time="+endDate+"");
	  JSONArray finalDataArray=(JSONArray) indiaMartJsonObject.get("RESPONSE");
	  List<Lead> leads = new ArrayList<Lead>();
	  System.out.println(finalDataArray);
	  
    if(finalDataArray==null)
    	System.out.println("Invalid Responce From IndiaMart :: "+finalDataArray);
    else if(finalDataArray.size()==1)
    {
    	for (int i = 0; i < finalDataArray.size(); i++) {
            org.json.simple.JSONObject jsonobject = (org.json.simple.JSONObject) finalDataArray.get(i);
            String Error_Message = jsonobject.get("Error_Message").toString();
            System.out.println(Error_Message);
    	}
    }
    else 
    {  
    	for (int i = 0; i < finalDataArray.size(); i++) {
    		
            org.json.simple.JSONObject jsonobject = (org.json.simple.JSONObject) finalDataArray.get(i);
            String SENDERNAME = ""+jsonobject.get("SENDER_NAME");
            String GLUSR_USR_COMPANYNAME = ""+jsonobject.get("GLUSR_USR_COMPANYNAME");
            String MOB =""+ jsonobject.get("SENDER_MOBILE");
            String ENQ_CITY =""+ jsonobject.get("SENDER_CITY");
            String ENQ_STATE =""+ jsonobject.get("SENDER_STATE");
            String SENDEREMAIL = ""+jsonobject.get("SENDER_EMAIL");
            String MOBILE_ALT =""+ jsonobject.get("SENDER_MOBILE_ALT");
            String LOG_TIME = ""+jsonobject.get("QUERY_TIME");
            String REQUIRMENT = ""+jsonobject.get("SUBJECT");
           
            SimpleDateFormat formatter6=new SimpleDateFormat("yyyyMMddHHmmss");  
            SimpleDateFormat formatter7=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
            final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            
            
            Lead dataLead=new Lead();
            dataLead.setAltmobileNo(MOBILE_ALT.length()>10?MOBILE_ALT.substring(4):MOBILE_ALT);
            dataLead.setAssignDate(new Date());
            dataLead.setAutoDial(true);
            dataLead.setcategoryId("3185");
            dataLead.setCity(ENQ_CITY.equalsIgnoreCase("null")?"NA":ENQ_CITY);
            dataLead.setCompanyId(400);
            dataLead.setCompanyName(GLUSR_USR_COMPANYNAME.equalsIgnoreCase("null")?"NA":GLUSR_USR_COMPANYNAME);
            dataLead.setCountry("NA");
            dataLead.setCreateDate(scheduledf.parse(LOG_TIME));
            dataLead.setCsvData(REQUIRMENT);
            dataLead.setDialState(false);
            dataLead.setEmail(SENDEREMAIL.equalsIgnoreCase("null")?"NA":SENDEREMAIL);
            dataLead.setFirstName(SENDERNAME.equalsIgnoreCase("null")?"NA":SENDERNAME);
            dataLead.setLastName("NA");
            dataLead.setLeadcomment("NA");
            dataLead.setLeadProcessStatus("None");
            dataLead.setLeadState("Open");
            dataLead.setMobileNo(MOB.length()>10?MOB.substring(4):MOB);
            dataLead.setNotification_flag("FALSE");
            dataLead.setSheduleDate(formatter6.parse("19800630173600"));
            dataLead.setStaffId(0);
            dataLead.setState(ENQ_STATE.equalsIgnoreCase("null")?"NA":ENQ_STATE);
            dataLead.setTagName("NA");
            dataLead.setTallyCalletId(0);
            dataLead.setWebsite("NA");   
            dataLead.setLeadType("IndiaMart");
            System.out.println(formatter7.format(dataLead.getCreateDate()));
            leads.add(dataLead);
    	} 
    }
  return leads;
  }
  
  public static void main(String[] args) throws IOException, JSONException, java.text.ParseException {
	  List<Lead> leads = IndiaMartJsonReader.GetSatcopIndaiMartLeads("23-03-2023","24-03-2023");
  }
}
package com.bonrix.sms.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bonrix.sms.config.SecurityConfig;
import com.bonrix.sms.job.EmailSenderThread;
import com.bonrix.sms.model.DisplaySetting;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.User;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.bonrix.sms.utils.StringUtils;

/**
 * @author Bhavesh Patel
 *
 */
@Controller
public class MainController {

	@Autowired
	AdminService adminService;
	@Autowired
	UserService userService;

	@Autowired
	SMSService smsservice;
	/*
	 * @Autowired MyUserDetailsService myUserDetailsService = new
	 * MyUserDetailsService();
	 */

	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.POST)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security + Hibernate Example Default Page");
		model.addObject("message", "This is Welcome  page!");
		model.setViewName("hello");
		return model;

	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		System.out.println("in admin");

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security + Hibernate Example");
		model.addObject("message", "This page is for ROLE_ADMIN only!");
		model.setViewName("admin");
		return model;
	}

	@RequestMapping(value = "/regUser", method = RequestMethod.POST)
	public ModelAndView regUser() {
		System.out.println(".....regUser........");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("user", new User());
		modelAndView.setViewName("regUser");
		return modelAndView;

	}

	@RequestMapping(value = "registerUser", method = RequestMethod.GET)
	public @ResponseBody String registerUser(HttpServletRequest request, HttpServletResponse response) {
		// System.out.println(".....regUser........");

		List<String> earray = new ArrayList<String>();
		Long parentid = 1L;
		DisplaySetting ds = adminService.getDisplaySettingByURL(request.getServerName());

		User usr2 = adminService.getUserByUserName("admin");
		if (ds == null) {
			parentid = 1L;
		} else {

			User usr = adminService.getUserByUserName(ds.getUserName());

			if (usr != null) {
				parentid = usr.getUid();
				earray.addAll(Arrays.asList(usr.getEmail().split(",")));
			}
		}
		String name = request.getParameter("name");
		String username = request.getParameter("username");

		if (username == null || username.isEmpty()) {
			return "Username Can't blank";
		}

		if (!StringUtils.isAlphaNumeric(username)) {
			return "only Alphanumaric value only in Username";
		}
		String pass = request.getParameter("password");
		String email = request.getParameter("email");
		String companyName = request.getParameter("companyName");

		String password = new SecurityConfig().passwordEncoder().encode(pass);

		// System.out.println("passsssss:"+pass+":::::::::"+password+":::::::"+encodedPassword);

		String address = request.getParameter("address");
		String number = request.getParameter("number");

		if (StringUtils.convertMobileno(number) == null) {
			return "Invalid Mobile Number";
		}
		String city = request.getParameter("city");

		User user = new User();
		user.setParentId(parentid);
		user.setActive(true);
		user.setAddBy(1);
		user.setAddress(address);
		user.setCmpName(companyName);
		user.setCountry("India");
		user.setDefaultSenderId(1);
		user.setDlrReport("YES");
		user.setEmail(email);
		user.setEnabled(true);
		user.setExpireDate(new Date());
		user.setMaxUser(0);
		user.setMobileNumber(number);
		user.setName(name);
		user.setParentId(1);
		user.setPassword(password);
		user.setPriority(1);
		user.setRegDate(new Date());
		user.setUsername(username);
		user.setCity(city);

		try {
			userService.saveRegUser(user);
		} catch (Exception ex) {
			return "UserName Already Exist Choose Diffrent";
		}
		// System.out.println("password : "+name+"===" + password);

		EmailTemplate et = new EmailTemplate();
		et.setMessage("Dear " + usr2.getUsername() + " <br/> new  user sign up at <b>" + request.getServerName()
				+ "</b><br/>Name:" + user.getUsername() + "<br/>Mobile:" + user.getMobileNumber() + "<br/>Name:"
				+ user.getUsername() + "<br/>");
		et.setSubject("New User SignUp at" + request.getServerName());
		earray.addAll(Arrays.asList(usr2.getEmail().split(",")));
		earray.add("89bhavesh@gmail.com");

		for (String em : earray) {
			if (StringUtils.isEmailValid(em)) {
				new Thread(new EmailSenderThread(et, smsservice.getHostSetting("admin"), em)).start();
			}

		}
		return "You are successfully Registered with Us we will get back you soon";
	}

	public static String md5(String input) {

		String md5 = null;

		if (null == input)
			return null;

		try {

			// Create MessageDigest object for MD5
			MessageDigest digest = MessageDigest.getInstance("MD5");

			// Update input string in message digest
			digest.update(input.getBytes(), 0, input.length());

			// Converts message digest value in base 16 (hex)
			md5 = new BigInteger(1, digest.digest()).toString(16);

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return md5;
	}

	@RequestMapping(value = "/reseller**", method = RequestMethod.GET)
	public ModelAndView reseller() {
		ModelAndView model = new ModelAndView();
		System.out.println("---------------------------------");
		model.addObject("message", "This page is for ROLE_RESELLER only!");
		model.setViewName("reseller");
		return model;

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {

		System.out.println("in login" + new Date() + "::" + request.getServerName());

		DisplaySetting ds = adminService.getDisplaySettingByURL(request.getServerName());

		ModelAndView model = new ModelAndView();
		if (ds != null) {
			System.out.println("DS::" + ds.getUserName());
			model.addObject("mtitle", ds.getTitle());
			model.addObject("logoImage", ds.getLogoImage());

			model.addObject("sub_title", ds.getSubTitle());
			model.addObject("image2", ds.getImage2());
			model.addObject("address", ds.getAddress());
			model.addObject("aboutus", ds.getAboutus());
			model.addObject("extraInfo1", ds.getExtraInfo1());
			model.addObject("extraInfo2", ds.getExtraInfo2());
			model.addObject("aboutus", ds.getAboutus());
			model.addObject("aboutus", ds.getAboutus());
			model.addObject("aboutus", ds.getAboutus());

		}
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}

	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else if (exception instanceof UsernameNotFoundException) {
			error = exception.getMessage();
		} else {
			error = exception.getMessage();
		}

		return "<div class='alert alert-danger fade in'>"
				+ "<button class='close' data-dismiss='alert'></button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>"
				+ error + "</div>";

	}

	// for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();

			System.out.println(userDetail);

			model.addObject("username", userDetail.getUsername());
			model.addObject("password", userDetail.getPassword());

		}

		model.setViewName("views/403");
		return model;

	}

}
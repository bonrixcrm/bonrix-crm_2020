package com.bonrix.sms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.dao.AdminDao;
import com.bonrix.sms.model.CategoryBean;
import com.bonrix.sms.model.CategoryManager;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.google.gson.GsonBuilder;

/**
 * @author Sajan
 *
 */

@Controller
public class MisscallAPIController {
	
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	SMSService smsService;
	
	@Autowired
	AdminDao dao;
	
	@Autowired
	CommonService cservice;
	
	final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@RequestMapping(value = "/oddekcallcenter")
	public @ResponseBody String oddekcallcenter(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "mobileNo", required = true) String mobileNo,
			@RequestParam(value = "categoryName", required = true) String categoryName,
			@RequestParam(value = "entryDate", required = true) String entryDate,
			@RequestParam(value = "tallyCalle", required = true) String tallyCalle,
			@RequestParam(value = "autoDial", required = true) boolean autoDial) throws Exception {
		
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallyCalle);
		if(tclaer==null)
			return "[{\"responseCode\":\"Invalid Telecaller.\"}]";
		
		CategoryManager category=adminService.getCategoryByNaame(categoryName,tclaer.getCompanyId());
		if(category==null)
		return "[{\"responseCode\":\"Invalid Category.\"}]";
		
		int no=adminService.validateNoCategory(mobileNo,(int)(long)category.getCategotyId());
		if(no!=0)
			return "[{\"responseCode\":\"Lead is Already Available in Selected Category.\"}]";
		else
		{
		Lead lead = new Lead();
		lead.setcategoryId(category.getCategotyId().toString());
		lead.setCompanyId(tclaer.getCompanyId());
		lead.setCreateDate(scheduledf.parse(entryDate));
		lead.setemail("N/A");
		lead.setFirstName("N/A");
		lead.setLastName("N/A");
		lead.setAutoDial(autoDial);
		lead.setLeadProcessStatus("None");
		lead.setMobileNo(mobileNo);
		lead.setLeadState("Open");
		lead.setSheduleDate(scheduledf.parse("1980-06-30 17:36:00"));
		lead.setTallyCalletId(tclaer.getTcallerid());
		lead.setCsvData("N/A");
		lead.setLeadType("MISSCALL");
		lead.setCountry("N/A");
		lead.setState("N/A");
		lead.setCity("N/A");
		lead.setCompanyName("N/A");
		lead.setWebsite("N/A");
		adminService.AddLead(lead);
		}
		return "[{\"responseCode\":\"Lead Successfully Added.\"}]";
	}
	
	@RequestMapping(value = "/bonrixmisscall")
	public @ResponseBody String bonrixmisscall(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "mobileNo", required = true) String mobileNo,
			@RequestParam(value = "categoryName", required = true) String categoryName,
			@RequestParam(value = "entryDate", required = true) String entryDate,
			@RequestParam(value = "tallyCalle", required = true) String tallyCalle,
			@RequestParam(value = "autoDial", required = true) boolean autoDial) throws Exception {
		
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallyCalle);
		if(tclaer==null)
			return "[{\"responseCode\":\"Invalid Telecaller.\"}]";
		
		CategoryManager category=adminService.getCategoryByNaame(categoryName,tclaer.getCompanyId());
		if(category==null)
		return "[{\"responseCode\":\"Invalid Category.\"}]";
		
		int no=adminService.validateNoCategory(mobileNo,(int)(long)category.getCategotyId());
		if(no!=0)
			return "[{\"responseCode\":\"Lead is Already Available in Selected Category.\"}]";
		else
		{
		Lead lead = new Lead();
		lead.setcategoryId(category.getCategotyId().toString());
		lead.setCompanyId(tclaer.getCompanyId());
		lead.setCreateDate(scheduledf.parse(entryDate));
		lead.setemail("N/A");
		lead.setFirstName("N/A");
		lead.setLastName("N/A");
		lead.setAutoDial(autoDial);
		lead.setLeadProcessStatus("None");
		lead.setMobileNo(mobileNo);
		lead.setLeadState("Open");
		lead.setSheduleDate(scheduledf.parse("1980-06-30 17:36:00"));
		lead.setTallyCalletId(tclaer.getTcallerid());
		lead.setCsvData("N/A");
		lead.setLeadType("MISSCALL");
		lead.setCountry("N/A");
		lead.setState("N/A");
		lead.setCity("N/A");
		lead.setCompanyName("N/A");
		lead.setWebsite("N/A");
		adminService.AddLead(lead);
		}
		return "[{\"responseCode\":\"Lead Successfully Added.\"}]";
	}
	
	@RequestMapping(value = "/GetMissCallLead", method = RequestMethod.GET)
	public @ResponseBody String GetMissCallLead(HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List list = adminService.GetMissCallLead((int) (long)currentUser.getUserid());
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(list);
		return new GsonBuilder().setPrettyPrinting().create().toJson(datatableJsonObject);

	}
	
	
	

}

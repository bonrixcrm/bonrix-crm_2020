package com.bonrix.sms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bonrix.sms.job.PushFCMNotifiction;
import com.bonrix.sms.service.AdminService;

@Controller
public class NotitficationController {

	@Autowired
	AdminService adminService;
	
	
	// CRM upcoming_notification
	
	@Scheduled(fixedRate = 900000)
	//@RequestMapping(value = "upcoming_notification", method = RequestMethod.GET)
	//public String CRM_upcoming_notification(HttpServletRequest request, HttpServletResponse response) {
	public void CRM_upcoming_notification() {

		System.out.println("in upcoming_notification");		
		List<Object[]> results = adminService.gettelecallerID();
			if(results == null) {
				System.out.println("No Records Found");	
				return ;
			}else {
				
				String TeleCallerId = "";
				int leadID = 0;
				String fname="";
				String lname="";
				String mobile="";
				
				for (Object[] result : results) {
					TeleCallerId = result[11].toString();
					leadID=(int) result[0];
					fname=result[5].toString();
					lname=result[6].toString();
					mobile=result[9].toString();
				}
				String fullname= fname+lname;
				System.out.println("TeleCallerId::" +TeleCallerId +" "+"leadID::"+leadID+"FullName::"+fname+" "+"mobile::"+mobile);

				List<String> tockenlist = null;
				PushFCMNotifiction pn_upcoming = new PushFCMNotifiction();
				tockenlist = adminService.gettocken(TeleCallerId);
				System.out.println("v1:" + tockenlist.toString());

				String tocken[] = tockenlist.stream().toArray(String[]::new);
				System.out.println("tocken::" + tocken.length);
				String msg_upcoming = null;
				try {
					msg_upcoming = pn_upcoming.sendPushNotification(tocken,leadID,fullname,mobile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("msg_upcoming::" + msg_upcoming);
			}
		
	}
	
	
	// CRM elapse_notification
	
	@Scheduled(fixedRate = 900000)
	//@RequestMapping(value = "elapse_notification", method = RequestMethod.GET)
	//public String CRM_elapse_notification(HttpServletRequest request, HttpServletResponse response) {
	public void CRM_elapse_notification() {

		System.out.println("in elapse_notification");		
		List<Object[]> results_elapse = adminService.getEllapsetelecallerID();
		
		if(results_elapse == null) {
			System.out.println("No Records Found");
			return ;
		}else {
			
			String TeleCallerId_elapse = "";
			int leadID_elapse = 0;
			String fname_elapse="";
			String lname_elapse="";
			String mobile_elapse="";
			
			for (Object[] result_elapse : results_elapse) {
				TeleCallerId_elapse = result_elapse[11].toString();
				leadID_elapse=(int) result_elapse[0];
				fname_elapse=result_elapse[5].toString();
				lname_elapse=result_elapse[6].toString();
				mobile_elapse=result_elapse[9].toString();
			}
			
			String fullname_elapse= fname_elapse+lname_elapse;
			
			System.out.println("TeleCallerId::" +TeleCallerId_elapse +" "+"leadID::"+leadID_elapse+"fullname_elapse::"+fullname_elapse+" "+"mobile_elapse::"+mobile_elapse);
			List<String> tockenlist_elapse = null;
			
			PushFCMNotifiction pn_elapse = new PushFCMNotifiction();
			tockenlist_elapse = adminService.gettocken(TeleCallerId_elapse);
			System.out.println("v1:" + tockenlist_elapse.toString());
			String tocken_elapse[] = tockenlist_elapse.stream().toArray(String[]::new);
			System.out.println("tocken::" + tocken_elapse.length);

			String msg_elapse = null;
			try {
				msg_elapse = pn_elapse.sendElapsePushNotification(tocken_elapse,leadID_elapse,fullname_elapse,mobile_elapse);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("msg_elapse::" + msg_elapse);
		}
		
		//return "";
	}

		
	
}

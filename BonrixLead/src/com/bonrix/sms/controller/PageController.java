package com.bonrix.sms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PageController {
	
	
	@RequestMapping(value = "/loginPage", method = RequestMethod.GET)
	public String redirect( Model model) {
		model.addAttribute("name", "sajan");
	        return "redirect:user/page1.jsp";
	   
	}
	
}
package com.bonrix.sms.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.queue.ConnectedProviderList;
import com.bonrix.sms.queue.ConnectedProviderNofityList;
import com.bonrix.sms.queue.ConnectedUserList;
import com.bonrix.sms.queue.ConnectedUserNotifyList;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
@MultipartConfig
public class RechargeGridController {
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	SMSService smsService;

	final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMddHHmm");
	final SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	Date CURR_DATE = new Date();

	@RequestMapping(value = "getConnectedClientList", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getConnectedClientList(HttpServletRequest request, HttpServletResponse response) {

		Long uids = 0L;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		if (request.getParameter("uids") != null && currentUser.getUserid() == 1) {

			uids = Long.parseLong(request.getParameter("uids"));
		} else {
			uids = currentUser.getUserid();
		}
		List list = adminService.getUsersByUid(uids);
		List<Map> lst = new ArrayList<Map>();

		for (int i = 0; i < list.size(); i++) {
			Object[] obj2 = (Object[]) list.get(i);
			Map<String, String> usrmap = new HashMap<String, String>();
			usrmap.put("uid", obj2[0].toString());
			usrmap.put("username", obj2[2].toString());
			usrmap.put("user", obj2[1].toString());
			usrmap.put("mobile", obj2[4].toString());
			usrmap.put("isactive", obj2[5].toString());
			usrmap.put("usercon", ConnectedUserList.getInstance().connectedClient.containsKey(obj2[2].toString())
					? "<img src=img/Circle_Green.png>" : "<img src=img/Circle_Red.png>");
			usrmap.put("usernotifycon",
					ConnectedUserNotifyList.getInstance().connectedClient.containsKey(obj2[2].toString())
							? "<img src=img/Circle_Green.png>" : "<img src=img/Circle_Red.png>");
			usrmap.put("providercon",
					ConnectedProviderList.getInstance().connectedClient.containsKey(obj2[2].toString())
							? "<img src=img/Circle_Green.png>" : "<img src=img/Circle_Red.png>");
			usrmap.put("providernotifycon",
					ConnectedProviderNofityList.getInstance().connectedClient.containsKey(obj2[2].toString())
							? "<img src=img/Circle_Green.png>" : "<img src=img/Circle_Red.png>");

			// System.out.println(usrmap);
			lst.add(usrmap);
		}
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();

		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(lst);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsons = gson.toJson(datatableJsonObject);
		return jsons;
	}

}

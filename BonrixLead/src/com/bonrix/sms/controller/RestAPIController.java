package com.bonrix.sms.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.UserService;
import com.google.gson.Gson;

@RestController
public class RestAPIController {
	public static final Logger logger = LoggerFactory.getLogger(RestAPIController.class);
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;
	
	final SimpleDateFormat df4 = new SimpleDateFormat("yyyyMMddHHmmss");
	final DateFormat readMyFormat = new SimpleDateFormat("yyyy-MM-dd");

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bonrixcrm/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getEmployeeById(/* @PathVariable("id") int id */) {
		// {"Status":"True","Message":"","Data":[{"Type":"Dealer","UserName":"dealer","Balance":208920.59}]}
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		/* Main JSON Object */
		JSONObject leadmap = new JSONObject();
		leadmap.put("Status", "1");
		leadmap.put("Message", "2");
		/* Inner Array */
		JSONObject data = new JSONObject();
		data.put("Type", "Dealer");
		data.put("UserName", "dealer");
		data.put("Balance", "208920.59");
		jarray.add(data);
		leadmap.put("Data", jarray);
		Lead lead = new Lead();
		return leadmap.toString();
	}

	@RequestMapping(value = "/bonrixcrm", method = RequestMethod.POST)
	public String addLead(@RequestBody Lead lead) throws Exception {
		adminService.AddLead(lead);
		return "";
	}
	
	@RequestMapping(value = "/bonrixcrm21")
	public String addLead21() throws Exception {
		System.out.println("Hello...");
		//adminService.AddLead(lead);
		return "";
	}

	/*
	 * @SuppressWarnings({ "unchecked", "rawtypes" })
	 * 
	 * @RequestMapping(value = "/user/{id}", method = RequestMethod.GET) public
	 * ResponseEntity<?> getUser(@PathVariable("id") int id) {
	 * logger.info("Fetching User with id {}", id);
	 * System.out.println("Fetching User with id {}"+id); Followupshistory user
	 * = adminService.findById(id); if (user == null) {
	 * 
	 * logger.error("User with id {} not found.", id); return new
	 * ResponseEntity(new CustomErrorType("User with id " + id + " not found"),
	 * HttpStatus.NOT_FOUND); } return new
	 * ResponseEntity<Followupshistory>(user, HttpStatus.OK); }
	 */

	@RequestMapping(value = "/SuccessCallLog/{id}/{start_date}/{end_date}/{call_satus}", method = RequestMethod.GET)
	public String getUser(@PathVariable("id") int id, @PathVariable("start_date") int start_date,
			@PathVariable("end_date") int end_date, @PathVariable("call_satus") int call_satus) {
		logger.info("Fetching User with id {}", id);
		Gson gson = new Gson();
		JSONArray ary = new JSONArray();
		JSONObject leadmap = new JSONObject();
		Followupshistory user = adminService.GetCallLog(id,start_date,end_date,call_satus);
		if (user == null) {
			logger.error("User with id {} not found.", id);
			leadmap.put("Status", "False");
			leadmap.put("Message", "No Data Found");
		} else {
			ary.put(new JSONObject(gson.toJson(user)));
			leadmap.put("Status", "True");
			leadmap.put("Message", "Data Found");
		}
		leadmap.put("Data", ary);
		return leadmap.toString();
	}
}

package com.bonrix.sms.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.bonrix.sms.model.HistoryCSV;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.SmsHistory;
import com.bonrix.sms.model.SpamWords;
import com.bonrix.sms.model.WhiteListNumber;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.SmsHistoryFilesBuilder;
import com.bonrix.sms.utils.StringUtils;
import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.MongoClient;

@Controller
public class SMSController {

	final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	MediUser currentUser = null;

	@Autowired
	SMSService smsService;

	@RequestMapping(value = "getSentSMSbyUid", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getHitsummarybyUid(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "uid", required = true) Long uid,
			@RequestParam(value = "start", required = true) String start,
			@RequestParam(value = "end", required = true) String end) {

		// String message = request.getParameter("message");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		currentUser = (MediUser) auth.getPrincipal();

		if (currentUser.getUserid() == 1) {
			if (uid == 0) {
				return smsService.getsmscountbyuid(0L, start, end);
			} else {
				return smsService.getsmscountbyuid(uid, start, end);
			}
		} else {
			if (uid == 0) {
				return "420";
			}
			return smsService.getsmscountbyuid(uid, start, end);
		}

	}

	@RequestMapping(value = "UpdateDlrSummary", method = RequestMethod.GET)
	public @ResponseBody String UpdateDlrSummary(@RequestParam(value = "from", required = true) String from,
			@RequestParam(value = "to", required = true) String to, HttpServletRequest request,
			HttpServletResponse response) {
		smsService.updateSmsSummary(1L, from, to);
		return "1";
	}

	@RequestMapping(value = "saveshistory", method = RequestMethod.GET)
	public @ResponseBody String saveshistory(HttpServletRequest request, HttpServletResponse response)
			throws UnknownHostException {
		// String message = request.getParameter("message");
		// Authentication auth = SecurityContextHolder.getContext()
		// .getAuthentication();
		// currentUser = (MediUser) auth.getPrincipal();
		// String outp=smsService.getsmscountbyuid(currentUser.getUserid(),
		// null, null);
		// System.out.println("=:::::==:"+outp);

		DB db = new MongoClient("176.9.137.236", 27017).getDB("sms");

		Jongo jongo = new Jongo(db);

		MongoCollection friends = jongo.getCollection("smshistory");

		List<SmsHistory> sh = smsService.getSmsHistorybyUid(1, 0, 500);

		System.out.println(sh.size());
		for (int i = 0; i < sh.size(); i++) {

			friends.save(sh.get(i));
			System.out.println(i);
		}

		return "asdgsadg";
	}

	@RequestMapping(value = "/saveSpamWords")
	public @ResponseBody int saveSpamWords(@RequestParam("spamtext") String spamtext, HttpServletResponse response) {
		SpamWords sw = new SpamWords();
		sw.setAddedby(1);
		sw.setAddedon(new Date());
		sw.setSpamtext(spamtext);
		sw.setServiceid(0);
		sw.setIsactive(true);
		return smsService.saveSpamWord(sw);
	}

	@RequestMapping(value = "/getSpamWords")
	public @ResponseBody String getSpamWords(HttpServletResponse response) {

		List<SpamWords> sw = smsService.getSpamWords();

		return "{ \"data\":" + new Gson().toJson(sw) + "}";

	}

	@RequestMapping(value = "/saveBlackWhiteNumber")
	public @ResponseBody int saveBlackWhiteNumber(@RequestParam("bwnumber") String bwnumber,
			@RequestParam("type") int type, HttpServletResponse response) {
		WhiteListNumber sw = new WhiteListNumber();
		sw.setAddedby(1);
		sw.setWhitenumber(bwnumber);

		return smsService.saveWhiteList(sw);
	}

	@RequestMapping(value = "/deleteBlackWhiteList")
	public @ResponseBody int deleteBlackWhiteList(@RequestParam("bwid") int bwid, HttpServletResponse response) {
		return smsService.deleteWhiteList(bwid);
	}

	@RequestMapping(value = "/getBlackWhiteList")
	public @ResponseBody String getBlackWhiteList(@RequestParam("type") int type, HttpServletResponse response) {

		List<WhiteListNumber> sw = smsService.getWhiteListObj();

		return "{ \"data\":" + new Gson().toJson(sw) + "}";

	}

	@RequestMapping(value = "/downloadSMS-CSV")
	public void downloadSMSCSV(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate,
			@RequestParam("status") String status, @RequestParam("service") String service,
			HttpServletResponse response) throws IOException, ParseException {

		/*
		 * List<SmsHistory> smsHistoryList = new ArrayList<SmsHistory>();
		 * SmsHistoryService smsHistoryService = new SmsHistoryService();
		 * smsHistoryList =
		 * smsHistoryService.getData(startDate,endDate,status,service);
		 */

		// int page, int listSize,long uid,Date sdate,Date edate,String mobile
		// if(request.getParameter("fdate") !=null &&
		// request.getParameter("fdate")!=""){
		Date fdate = df.parse(startDate + " 00:00:00");
		// }

		Date todate = df.parse(endDate + " 23:59:59");

		List smsHisList = smsService.getSmsHistoryByAdmin4Report(0, 0, currentUser.getUserid(), fdate, todate, service);

		List<HistoryCSV> hc = new ArrayList<HistoryCSV>();

		for (int i = 0; i < smsHisList.size(); i++) {
			Object[] smsHistory = (Object[]) smsHisList.get(i);
			hc.add(new HistoryCSV(i, smsHistory[3].toString(), smsHistory[2].toString(), smsHistory[5].toString(),
					smsHistory[7].toString(), smsHistory[8].toString(), smsHistory[9].toString()));
		}

		response.setContentType("text/csv");
		// response.setContentType("application/force-download");
		String headerKey = "Content-Disposition";
		String fileName = new String();
		fileName = "SMS_History.csv";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

		String[] header = { "srno", "MobileNo", "Message", "SentOn", "Status", "Service", "SenderName" };

		csvWriter.writeHeader(header);
		for (HistoryCSV smsHistory1 : hc) {
			csvWriter.write(smsHistory1, header);
		}
		csvWriter.close();
	}

	@RequestMapping(value = "/download-SMS-PDF")
	@ResponseBody
	public ModelAndView downloadSMS(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, @RequestParam("status") String status,
			@RequestParam("service") int service, @RequestParam("require") String require, HttpServletResponse response)
			throws IOException, ParseException {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		currentUser = (MediUser) auth.getPrincipal();

		// require="pdadgadf";
		// int page, int listSize,long uid,Date sdate,Date edate,String mobile
		// if(request.getParameter("fdate") !=null &&
		// request.getParameter("fdate")!=""){

		System.out.println("in to the download pdf");
		Date fdate = df.parse(startDate + " 00:00:00");
		// }

		Date todate = df.parse(endDate + " 23:59:59");

		System.out.println(fdate + "======" + todate + "====");
		List smsHisList = smsService.getSmsHistoryByAdmin4Report(service, 0, currentUser.getUserid(), fdate, todate,
				"");

		System.out.println(fdate + "======" + todate + "====" + smsHisList.size());
		/*
		 * List<com.bonrix.sms.model.SmsHistory> smsHistoryList = new
		 * ArrayList<SmsHistory>(); smsHistoryService smsHistoryService = new
		 * smsHistoryService(); smsHistoryList =
		 * smsHistoryService.getData(startDate,endDate,status,service);
		 */
		/*
		 * List listq=adminService.
		 * GetLeadParameterForSearch("SELECT lead.leaadId,lead.firstName,lead.lastName,categorymanager.categortName,lead.email,lead.mobileNo,"
		 * +" lead.leadState,lead.leadProcessStatus,DATE_FORMAT(lead.createDate,'%b %d %Y %h:%i %p'),DATE_FORMAT(lead.sheduleDate,'%b %d %Y %h:%i %p'),users.cmpname,lead.city,lead.state,lead.Country,"
		 * +" lead.companyName,lead.website,lead.csvData,tallycaller.username  FROM lead  INNER JOIN  categorymanager "
		 * +" ON lead.categoryId=categorymanager.categotyId INNER JOIN  users ON users.uid=lead.companyId "
		 * +" INNER JOIN tallycaller ON tallycaller.tcallerid=lead.tallyCalletId WHERE   lead.companyId="
		 * +Company_id+"");
		 */

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("smsHistoryList", smsHisList);
		model.put("startDate", new String(startDate));
		model.put("endDate", new String(endDate));

		if (require.equals("pdf")) {
			response.setContentType("application/force-download");
			String pdfNM = new String();
			pdfNM = "LEAD_History.pdf";
			// response.setHeader("Content-Disposition",
			// "attachment;filename=\""+ pdfNM + "\"");

			response.setContentType("application/force-download");
			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + pdfNM + "\"");
			return new ModelAndView("pdfSMSDownload", model);

		} else
			return new ModelAndView("excelSMSDownload", model);
	}

	@RequestMapping(value = "/downloadSMS-ZIP")
	public void downloadZIP(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate,
			@RequestParam("status") String status, @RequestParam("service") String service, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ParseException {

		String relativeWebPath = "/WEB-INF";
		String filePath = request.getServletContext().getRealPath(relativeWebPath);

		String path = new String(filePath + "\\SMS_HistoryDownload");

		boolean result = false;

		File directory = new File(path);
		if (!(directory.exists()))
			result = directory.mkdir();

		String fileNM = new String("SMS_History");

		Date fdate = df.parse(startDate + " 00:00:00");
		// }

		Date todate = df.parse(endDate + " 23:59:59");

		List smsHisList = smsService.getSmsHistoryByAdmin4Report(0, 0, currentUser.getUserid(), fdate, todate, service);

		try {
			new SmsHistoryFilesBuilder().fileMaker(smsHisList, path, fileNM, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		File directory1 = new File(path);
		String[] files = directory1.list();
		if (files != null && files.length > 0) {

			ServletOutputStream sos = response.getOutputStream();
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + fileNM + ".zip\"");
			byte[] zip = generateZip(directory1, files);

			String[] entries = directory.list();
			for (String s : entries) {
				File currentFile = new File(directory.getPath(), s);
				currentFile.delete();
			}
			directory.delete();

			sos.write(zip);
			sos.flush();
		}
	}

	/**
	 * Use in zip file
	 *****************************************************************************************/
	private byte[] generateZip(File directory, String[] files) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(baos);
		byte bytes[] = new byte[2048];

		for (String fileName : files) {
			FileInputStream fis = new FileInputStream(directory.getPath() + "\\" + fileName);
			BufferedInputStream bis = new BufferedInputStream(fis);

			zos.putNextEntry(new ZipEntry(fileName));

			int bytesRead;
			while ((bytesRead = bis.read(bytes)) != -1) {
				zos.write(bytes, 0, bytesRead);
			}
			zos.closeEntry();
			bis.close();
			fis.close();
		}
		zos.flush();
		baos.flush();
		zos.close();
		baos.close();

		return baos.toByteArray();
	}

	@RequestMapping(value = "getSentMessagebyUid", method = RequestMethod.GET)
	public @ResponseBody String getSentMessageByUid(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "stype", required = true) int service) throws ParseException {
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		String mob = "";
		Date fdate = null;
		Date todate = null;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}

			if (request.getParameter("m") != null) {
				mob = request.getParameter("m");
			}

			if (!StringUtils.isEmpty(request.getParameter("fdate"))) {
				fdate = df.parse(request.getParameter("fdate") + " 00:00:00");
			}
			if (!StringUtils.isEmpty(request.getParameter("todate"))) {
				todate = df.parse(request.getParameter("todate") + " 23:59:59");
			}

			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			}
			page = start / len + 1;
			currentUser = (MediUser) auth.getPrincipal();
		} catch (Exception ex) {
			ex.printStackTrace();
			return "ERROR-505";
		}

		return smsService.getSmsHistoryByUid(page, listSize, currentUser.getUserid(), fdate, todate, mob, service);
	}

	@RequestMapping(value = "getScheduleMessagebyUid", method = RequestMethod.GET)
	public @ResponseBody String getScheduleMessagebyUid(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "stype", required = true) int service) throws ParseException {
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		String mob = "";
		Date fdate = null;
		Date todate = null;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}

			if (request.getParameter("m") != null) {
				mob = request.getParameter("m");
			}

			if (!StringUtils.isEmpty(request.getParameter("fdate"))) {
				fdate = df.parse(request.getParameter("fdate") + " 00:00:00");
			}
			if (!StringUtils.isEmpty(request.getParameter("todate"))) {
				todate = df.parse(request.getParameter("todate") + " 23:59:59");
			}

			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			}
			page = start / len + 1;
			currentUser = (MediUser) auth.getPrincipal();
		} catch (Exception ex) {
			ex.printStackTrace();
			return "ERROR-505";
		}

		return smsService.getSeduleMessageByUid(page, listSize, currentUser.getUserid(), fdate, todate, mob, service);
	}

	@RequestMapping(value = "DeleteQueueById", method = RequestMethod.GET)
	public @ResponseBody String DeleteQueueById(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "msgid", required = true) String msgid) {
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		} catch (Exception ex) {
			return "ERROR-505";
		}
		return smsService.deleteMultismsQ(msgid) + "";
	}

	@RequestMapping(value = "getSentMessagebyAdmin", method = RequestMethod.GET)
	public @ResponseBody String getSentMessageByAdmin(HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		int uid = 0;

		String mob = "";
		Date fdate = null;
		Date todate = null;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}

			if (request.getParameter("m") != null) {
				mob = request.getParameter("m");
			}
			if (!StringUtils.isEmpty(request.getParameter("fdate"))) {
				fdate = df.parse(request.getParameter("fdate") + " 00:00:00");
			}
			if (!StringUtils.isEmpty(request.getParameter("todate"))) {
				todate = df.parse(request.getParameter("todate") + " 23:59:59");
			}

			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			}
			if (request.getParameter("uid") != null) {
				uid = Integer.parseInt(request.getParameter("uid"));
			}
			page = start / len + 1;
			currentUser = (MediUser) auth.getPrincipal();
		} catch (Exception ex) {
			return "ERROR-505";
		}
		System.out.println("In Sent message ------ > PAGE::" + page + "::LIST SIZE:" + listSize + "::::" + mob + "--"
				+ currentUser.getUserid());

		return smsService.getSmsHistoryByAdmin(page, listSize, uid, fdate, todate, mob);
	}

	@RequestMapping(value = "getBlockedNumbers", method = RequestMethod.GET)
	public @ResponseBody String getBlockedNumbers(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "start", required = true) int start,
			@RequestParam(value = "m", required = false) String mob,
			@RequestParam(value = "length", required = false) int listSize) throws ParseException {
		int page = 1;

		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			page = start / listSize + 1;
			currentUser = (MediUser) auth.getPrincipal();
		} catch (Exception ex) {
			ex.printStackTrace();
			return "ERROR-505";
		}
		return smsService.getBlockedNumbers(page, listSize, mob);
	}

}
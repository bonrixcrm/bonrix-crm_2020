package com.bonrix.sms.controller;

import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import com.bonrix.sms.model.Crmemailsetting;
import com.bonrix.sms.model.Crmemailtemplate;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;

public class SendCRMMail {

	public String send(final Crmemailsetting emailSetting, final String sendto, final String sub,
			final String tallycallerId, final String leadId, final String filesname, final Crmemailtemplate temp,
			final String filePath, final Tallycaller tcaler, final AdminService adminService, final String status) {
		System.out.println("FILES::" + filesname + " :: " + status);
		System.out.println("EMAIL :: "+emailSetting.getHostName());
		final String files_name = filesname;
		try {
			JavaMailSenderImpl sender = new JavaMailSenderImpl();
			sender.setHost(emailSetting.getHostName());
			sender.setUsername(emailSetting.getEmailId());
			sender.setPassword(emailSetting.getPasswd());
			sender.setJavaMailProperties(javaMailProperties());
			sender.send(new MimeMessagePreparator() {

				@Override
				public void prepare(MimeMessage mimeMessage) {
					try {
						MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
						message.setFrom(emailSetting.getEmailId());
						message.setTo(sendto);
						message.setSubject(sub);
						message.setText(temp.getTemp_Text(), true);// C:\\Users\b\onrix\\Desktop
						String[] file = filesname.split(",");

						int i = 0;
						System.out.println("SIXE: " + file.length);
						if (!filesname.equalsIgnoreCase("N/A")) {
							for (i = 0; i < file.length; i++) {
								System.out.println(filePath + file[i]);
								FileSystemResource Atfile = new FileSystemResource(filePath + file[i]);
								message.addAttachment(Atfile.getFilename(), Atfile);
							}
						}

					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});

			adminService.saveEmailLog(filesname, Integer.parseInt(leadId), temp.getTemp_Text(), sub,
					Integer.parseInt(tallycallerId), "Success", tcaler.getCompanyId(), "N/A");
		} catch (MailException e) {
			System.out.println("IN ERROR MailException");
			adminService.saveEmailLog(filesname, Integer.parseInt(leadId), temp.getTemp_Text(), sub,
					Integer.parseInt(tallycallerId), "Failed", tcaler.getCompanyId(), e.getMessage().toString());
			System.out.println("IN ERROR ::: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("IN ERROR Exception");
			adminService.saveEmailLog(filesname, Integer.parseInt(leadId), temp.getTemp_Text(), sub,
					Integer.parseInt(tallycallerId), "Failed", tcaler.getCompanyId(), e.getMessage().toString());
			System.out.println("IN ERROR ::: " + e.getMessage());
			e.printStackTrace();
		}
		return filesname;

	}

	private Properties javaMailProperties() {
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		/* properties.setProperty("mail.debug", "true"); */
		properties.setProperty("mail.smtp.ssl.trust", "mail.shyamshoppe.com");
		properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.setProperty("templateMode", "HTML5l");
		/* mimeMessage.setContent(htmlMsg, "text/html"); */
		properties.setProperty("mail.smtp.auth", "true");   
		properties.put("mail.smtp.port", "465");
		return properties;
	}
}

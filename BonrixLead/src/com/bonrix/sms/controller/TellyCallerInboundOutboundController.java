package com.bonrix.sms.controller;

import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.UserService;

@Controller
@MultipartConfig
public class TellyCallerInboundOutboundController {
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	CommonService cservice;
	
	@Autowired
	UserService userService;
	
	
	
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "getAndroidTallyCallerLeadAPI", produces = "application/javascript")
	public @ResponseBody String getAndroidTallyCallerLeadAPI(HttpServletRequest request, HttpServletResponse response)throws Exception {
		String tallycallerName = request.getParameter("tallycallerName");
		String api = request.getParameter("apikey");
		String limit = request.getParameter("limit");
		String pageNo = request.getParameter("pageNo");
		
		JSONObject mainjobj = new JSONObject();
		
		mainjobj.put("limit", limit.toString());
		mainjobj.put("page", pageNo.toString());
		
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		if (apiKey != null) {
			List list = adminService.getAndroidTallyCallerLead(tallycallerName,Integer.parseInt(limit),Integer.parseInt(pageNo));
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();

					result = (Object[]) list.get(i);
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tag", result[18] == null ? "N/A" : result[18]);
					leadmap.put("dialState", result[19]);
					jarray.add(leadmap);
				}
				
				String totalRecord= adminService.getAndroTellyCount(tallycallerName);
				mainjobj.put("total_rec", totalRecord.toString());
				mainjobj.put("Data", jarray);
			} else {
				if (request.getParameter("callback") != null) {
					return request.getParameter("callback") +  mainjobj.toString();
				}else {
					return "[{\"responceCode\":\"No Data Found\"}]";
				}	
			}

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + mainjobj.toString();
		} else {
			return mainjobj.toString();
		}

	}
	
	
	
	
	// http://localhost:8191/spring-hib/getCategoryLeadAPI?tallycallerName=Bonrix&catname=Category7&apikey=XXXXXX
		@SuppressWarnings("unchecked")
		@RequestMapping(value = "getAndroidCategoryLeadAPI")
		public @ResponseBody String getAndroidCategoryLeadAPI(HttpServletRequest request, HttpServletResponse response)throws Exception {
			String tallycallerName = request.getParameter("tallycallerName");
			String catname = request.getParameter("catname");
			String api = request.getParameter("apikey");
			String username = request.getParameter("username");
			String limit = request.getParameter("limit");
			String pageNo = request.getParameter("pageNo");

			Tallycaller tclaer = new Tallycaller();
			tclaer = adminService.GetTellyCallerByName(tallycallerName);

			if (tclaer == null) {
				return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
			}
			ApiKey apiKey = null;

			if (tclaer != null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

				if (apiKey == null) {
					apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
					if (apiKey == null) {
						return "[{\"responseCode\":\"INVALID_KEY\"}]";
					}
				}
			}
			org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

			if (apiKey != null) {
				List list = adminService.getAndroidCategoryLead(tallycallerName, catname,Integer.parseInt(limit),Integer.parseInt(pageNo));
				for (int i = 0; i < list.size(); i++) {
					Object[] result = (Object[]) list.get(i);

					JSONObject leadmap = new JSONObject();
					leadmap.put("leaadId", result[0]);
					leadmap.put("categoryName", result[1]);
					leadmap.put("createDate", result[2]);
					leadmap.put("email", result[3]);
					leadmap.put("firstName", result[4]);
					leadmap.put("lastName", result[5]);
					leadmap.put("leadProcessStatus", result[6]);
					leadmap.put("leadState", result[7]);
					leadmap.put("mobileNo", result[8]);
					leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
					leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
					leadmap.put("leadType", result[11]);
					leadmap.put("autoDial", result[12]);
					leadmap.put("companyName", result[13]);
					leadmap.put("website", result[14]);
					leadmap.put("Country", result[15]);
					leadmap.put("state", result[16]);
					leadmap.put("city", result[17]);
					leadmap.put("tag", result[18] == null ? "N/A" : result[18]);
					leadmap.put("dialState", result[19]);
					jarray.add(leadmap);
				}

			}
			return jarray.toJSONString();
		}
		
		
		@SuppressWarnings({ "unchecked", "unused" })
		@RequestMapping(value = "getAndroidLeadBymobileNoAPI", produces = "application/javascript")
		public @ResponseBody String getAndroidLeadBymobileNoAPI(HttpServletRequest request, HttpServletResponse response)throws Exception {

			String mobNo = request.getParameter("mobNo");
			String api = request.getParameter("apikey");
			String tallycallerName = request.getParameter("tallycallerName");
			String limit = request.getParameter("limit");
			String pageNo = request.getParameter("pageNo");
			
			JSONObject mainjobj = new JSONObject();
			
			mainjobj.put("limit", limit.toString());
			mainjobj.put("page", pageNo.toString());
			
			org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
			

			System.out.println("getLeadByCompanyIdNDmobileNo is called...");

			ApiKey apiKey = null;
			Tallycaller tclaer = new Tallycaller();
			tclaer = adminService.GetTellyCallerByName(tallycallerName);

			if (tclaer == null) {
				return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
			}
			// System.out.println(tallycallerName + " " + tclaer.getUsername());
			if (tclaer != null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

				if (apiKey == null) {
					apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
					if (apiKey == null) {
						return "[{\"responseCode\":\"INVALID_KEY\"}]";
					}
				}
			}

			Object[] result = null;
			if (apiKey != null) {
				List list = adminService.getAndroidLeadByCompanyIdNDmobileNo(mobNo, (int) (long) tclaer.getCompanyId(),Integer.parseInt(limit),Integer.parseInt(pageNo));
				if (list.size() != 0) {

					for (int i = 0; i < list.size(); i++) {
						JSONObject leadmap = new JSONObject();

						result = (Object[]) list.get(i);
						/*
						 * System.out.println("My : "+result[0]+" "+result[1]+" +"result[2]"+
						 * "+result[3]+" "+result[4]"+ "+result[5] +"result[6]"+ "+result[7]"+
						 * "+result[8]"+"result[9] +"result[10]"+ "+result[11]"+ "+result[12]);
						 */
						leadmap.put("leaadId", result[0]);
						leadmap.put("categoryName", result[1]);
						leadmap.put("createDate", result[2]);
						leadmap.put("email", result[3]);
						leadmap.put("firstName", result[4]);
						leadmap.put("lastName", result[5]);
						leadmap.put("leadProcessStatus", result[6]);
						leadmap.put("leadState", result[7]);
						leadmap.put("mobileNo", result[8]);
						leadmap.put("sheduleDate", result[9] == null ? "N/A" : result[9]);
						leadmap.put("csvData", result[10] == null ? "N/A" : result[10]);
						leadmap.put("leadType", result[11]);
						leadmap.put("autoDial", result[12]);
						leadmap.put("companyName", result[13]);
						leadmap.put("website", result[14]);
						leadmap.put("Country", result[15]);
						leadmap.put("state", result[16]);
						leadmap.put("city", result[17]);
						jarray.add(leadmap);
					}
					
					String totalRecord= adminService.getAndroidCountByCompanyIdNDmobileNo(mobNo,tclaer.getCompanyId());
					mainjobj.put("total_rec", totalRecord.toString());
					mainjobj.put("Data", jarray);
				} else {
					if (request.getParameter("callback") != null) {
						return request.getParameter("callback") + mainjobj.toString();
					}else {
						return "[{\"responceCode\":\"No Data Found\"}]";
					}
				}

			}
			if (request.getParameter("callback") != null) {
				return request.getParameter("callback") + mainjobj.toString();
			} else {
				return mainjobj.toString();
			}

		}

}

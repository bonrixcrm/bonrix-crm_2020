package com.bonrix.sms.controller;

import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bonrix.sms.job.FilterNSaveJob;
import com.bonrix.sms.model.CampaignLog;
import com.bonrix.sms.model.GroupName;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.ServiceType;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.User;
import com.bonrix.sms.queue.QueueUltraHigh;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.bonrix.sms.utils.StringUtils;
import com.google.gson.Gson;
//import org.apache.commons.fileupload.FileItem;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

@Controller
@MultipartConfig
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	SMSService smsService;

	@RequestMapping(value = "RestoreSMS", produces = "text/plain;charset=UTF-8")
	public @ResponseBody String RestoreSMS(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "cid", required = true) int cid,
			@RequestParam(value = "addmin", required = true) int addmin) {
		int sizes = 0;
		if (cid == 1) {
			sizes = QueueUltraHigh.getInstance().ultraHigh1.size();
		} else if (cid == 2) {
			sizes = QueueUltraHigh.getInstance().ultraHigh2.size();
		} else if (cid == 3) {
			sizes = QueueUltraHigh.getInstance().ultraHigh3.size();
		} else if (cid == 4) {
			sizes = QueueUltraHigh.getInstance().ultraHigh4.size();
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List<SmsQueue> lstsmsqueue = new ArrayList<SmsQueue>();
		if (currentUser.getUserid() == 1) {

			Calendar cal = Calendar.getInstance();
			for (int k = 0; k < sizes; k++) {
				SmsQueue sq = QueueUltraHigh.getInstance().getSmsObject(cid);
				if (sq != null) {
					cal.setTime(sq.getCreatDate());
					cal.add(12, addmin);
					sq.setCreatDate(cal.getTime());
					lstsmsqueue.add(sq);
				} else {
					break;
				}

			}

			smsService.saveSmsQueue(lstsmsqueue);

		}

		return "Channel #" + cid + "'s total " + lstsmsqueue.size() + "SMS Restored And schedule after " + addmin
				+ " Minuts";
	}

	@RequestMapping(value = "getUsersParent", method = RequestMethod.GET)
	public ModelAndView UserDetail(HttpServletRequest request,
			@RequestParam(value = "uid", required = true) String uid) {
		User us = adminService.getUserByUserName(uid);

		Long parentid = us.getUid();
		List<String[]> lst = new ArrayList<String[]>();

		while (parentid != 1) {
			User usr = adminService.getUserByUid(parentid);
			lst.add(new String[] { usr.getUid() + "", usr.getUsername(), usr.getMobileNumber(), usr.getEmail() });
			parentid = usr.getParentId();
		}

		ModelAndView model = new ModelAndView("parentList");
		model.addObject("lists", lst);

		return model;

	}

	@RequestMapping(value = "sendMessage", produces = "text/plain;charset=UTF-8")
	public @ResponseBody String sendMessage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyyHH:mm");
		Date CURR_DATE = new Date();
		String senderId = "INFOIN";
		String message = request.getParameter("message");
		if (message == null || message.isEmpty()) {
			return -1 + ""; // "Message Can't Blank";
		}
		message = URLDecoder.decode(message.trim(), "UTF-8");

		if (!StringUtils.isEmpty(request.getParameter("senderId"))) {
			senderId = request.getParameter("senderId");
		}
		if (request.getParameter("schedule") != null || request.getParameter("schedule").length() != 0) {
			try {
				CURR_DATE = sdf.parse(request.getParameter("schedule"));
			} catch (ParseException pe) {

			}
		}
		Long serviceid = Long.parseLong(request.getParameter("serviceid"));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List<SmsQueue> lstsmsqueue = new ArrayList<SmsQueue>();
		ServiceType serviceType = userService.getServiceTypeById(serviceid);
		String mobileNumbers = request.getParameter("numbers");
		String[] numbers = mobileNumbers.split(",");
		if (!StringUtils.isNumeric(mobileNumbers.replaceAll(",", ""))) {
			return -2 + ""; // "Message Not Sent !Enter Valid Number !";
		}
		if (serviceType.getTemplatecheck() && !isvalidtemplate(message, currentUser.getUserid())) {
			return -3 + ""; // "Message Not Match With Template";
		}
		boolean isUnicode = StringUtils.isUnicode(message);
		int crdeduct = StringUtils.SMSCounter(message.length(), isUnicode);
		int crdup = smsService.creditUpdate(crdeduct * numbers.length, currentUser.getUserid(), serviceType.getStid());
		if (crdup == 0) {
			return -4 + ""; // insufficiant credit";
		}
		for (int i = 0; i < numbers.length; i++) {
			SmsQueue smsQueue = new SmsQueue();
			smsQueue.setCreatDate(CURR_DATE);
			smsQueue.setMessage(message);
			smsQueue.setPriority(serviceType.getPriority());
			smsQueue.setRetryCount(1);
			smsQueue.setStatus(1);
			smsQueue.setCampid(0);
			smsQueue.setIsunicode(isUnicode);
			smsQueue.setDndChecked(!serviceType.getDndcheck());
			smsQueue.setUid(currentUser.getUserid());
			smsQueue.setStid(serviceType.getStid());
			smsQueue.setSenderid(senderId);
			smsQueue.setIscutted(false);
			smsQueue.setMsgcredit(crdeduct);
			String newno = StringUtils.convertMobileno(numbers[i]);
			if (newno != null) {
				smsQueue.setNumber(newno);
				lstsmsqueue.add(smsQueue);
			}

		}
		/*
		 * if(currentUser.getMinnumber()!=0 &&
		 * currentUser.getMinnumber()<lstsmsqueue.size()){ int totalcut=(int)
		 * (lstsmsqueue.size()*currentUser.getCutting()*0.01); for(int
		 * i=0;i<totalcut;i++){ Collections.shuffle(lstsmsqueue); // Shuffle the
		 * list. SmsQueue sms=lstsmsqueue.remove(0); sms.setIscutted(true);
		 * lstsmsqueue.add(sms); } } smsService.saveSmsQueue(lstsmsqueue);
		 */

		new FilterNSaveJob().filterNSave(lstsmsqueue, smsService, currentUser);
		return 1 + ""; // Message Send SuccessFully";
	}

	// for send message at user createtaion time

	/* @Secured("ROLE_ADMIN") */
	/* @PreAuthorize("hasRole('ROLE_ADMIN')") */

	/*
	 * @RequestMapping(value = "sendMessageApi", produces = "application/json")
	 * public @ResponseBody String sendMessageApi(
	 * 
	 * @RequestParam(value = "username", required = true) String userName,
	 * 
	 * @RequestParam(value = "apikey", required = true) String key,
	 * 
	 * @RequestParam(value = "message", required = true) String message,
	 * 
	 * @RequestParam(value = "numbers", required = true) String mobileNumbers
	 * ,HttpServletRequest request, HttpServletResponse response) throws
	 * Exception { //System.out.println("sendMessageApi"); SmsCredit sc=null;
	 * 
	 * String senderName = request.getParameter("senderName"); String smsType =
	 * request.getParameter("smsType"); String[] numbers =
	 * mobileNumbers.split(","); String serverIP = request.getRemoteAddr();
	 * JsonObject jo=new JsonObject();
	 * 
	 * if (!userName.equalsIgnoreCase("") || !key.equalsIgnoreCase("") ||
	 * !senderName.equalsIgnoreCase("") || !message.equalsIgnoreCase("")) { User
	 * userObj = adminService .getUserByUserName(userName);
	 * 
	 * if(!userObj.isActive()){ jo.addProperty("error",
	 * "User Account Deactivated"); return jo.toString();
	 * 
	 * };
	 * 
	 * if (userObj.getUsername() != null ||
	 * !userObj.getUsername().equalsIgnoreCase("")) {
	 * 
	 * // System.out.print("UID:"+userObj.getUid()+"::KEY:"+key); ApiKey apiKey
	 * = userService.gettAPiByUid(userObj.getUid(), key,false);
	 * 
	 * if (apiKey != null) {
	 * 
	 * if (!apiKey.getIp().contains("*") &&
	 * !apiKey.getIp().equalsIgnoreCase(serverIP)) { jo.addProperty("error",
	 * "invalid IP address"); return jo.toString(); }
	 * 
	 * ServiceType serviceTypes = userService .getServiceTypeByName(smsType);
	 * 
	 * if(serviceTypes==null){ jo.addProperty("error", "Service not available");
	 * return jo.toString(); }
	 * 
	 * try{ sc = adminService.getSmsCredit( userObj.getUid(),
	 * serviceTypes.getStid()); }catch(NullPointerException ex){
	 * jo.addProperty("error",
	 * "No Credit in Given Service ? Please Contact customer Care"); return
	 * jo.toString(); } boolean isUnicode = StringUtils.isUnicode(message); int
	 * crdeduct=StringUtils.SMSCounter(message.length(),isUnicode);
	 * 
	 * Long bal = sc.getSmsCredit();
	 * 
	 * ServiceType serviceType = userService .getServiceTypeById(serviceTypes
	 * .getStid()); if (serviceType.getTemplatecheck() &&
	 * !isvalidtemplate(message,userObj.getUid())) { jo.addProperty("error",
	 * "Message Not Match With Template"); return jo.toString(); }
	 * 
	 * 
	 * int crdup =
	 * smsService.creditUpdate(crdeduct*numbers.length,userObj.getUid(),
	 * serviceType.getStid()); if (crdup == 0) { System.out.print(
	 * "Insuffician credit of User id:" + userObj.getUid() +
	 * "::Message cannot be sent" + new Date()); jo.addProperty("error",
	 * "insufficiant credit"); return jo.toString();
	 * 
	 * }
	 * 
	 * 
	 * if (bal > (numbers.length*crdeduct)) {
	 * 
	 * for (int i = 0; i < numbers.length; i++) { SmsQueue smsQueue = new
	 * SmsQueue(); smsQueue.setCreatDate(new Date());
	 * smsQueue.setMessage(message);
	 * smsQueue.setPriority(serviceType.getPriority());
	 * smsQueue.setRetryCount(1); smsQueue.setStatus(1);
	 * smsQueue.setSenderid(senderName); smsQueue.setUid(userObj.getUid());
	 * smsQueue.setStid(serviceType.getStid());
	 * smsQueue.setDndChecked(!serviceType.getDndcheck());
	 * smsQueue.setNumber(numbers[i]); smsQueue.setMsgcredit(crdeduct);
	 * userService.saveSms(smsQueue); int crdup =
	 * smsService.creditUpdate(crdeduct, userObj.getUid(),
	 * serviceTypes.getStid());
	 * 
	 * } } else { jo.addProperty("error", "Not Enough SMS Credit."); return
	 * jo.toString();
	 * 
	 * } } else { return "Check Ip Address."; }
	 * 
	 * } else { jo.addProperty("error","Wrong Api Key."); return jo.toString();
	 * }
	 * 
	 * }
	 * 
	 * } else { jo.addProperty("error","Not Enough Perameter"); return
	 * jo.toString(); }
	 * 
	 * jo.addProperty("success","Message SuccessFully Submitted"); return
	 * jo.toString(); }
	 */

	@RequestMapping(value = "sendMessageByGid")
	public @ResponseBody String sendMessageByGid(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyyhh:mma");
		String message = request.getParameter("message").trim();
		// Long senderId = Long.parseLong(request.getParameter("senderId"));

		if (message == null || message.isEmpty()) {
			return "Message Can't Empty";
		}

		message = URLDecoder.decode(message, "UTF-8");
		String senderId = request.getParameter("senderId");

		Date CURR_DATE = new Date();

		if (request.getParameter("isschedule").equalsIgnoreCase("true")) {
			try {
				CURR_DATE = sdf.parse(request.getParameter("schedule"));
			} catch (ParseException pe) {
				return "Invalid Date or Time Formate";
			}
		}

		Long serviceid = Long.parseLong(request.getParameter("serviceid"));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List<SmsQueue> lstsmsqueue = new ArrayList<SmsQueue>();
		ServiceType serviceType = userService.getServiceTypeById(serviceid);

		String[] gid = request.getParameter("gid").split(",");
		List<String> mobileNumberList = new ArrayList<String>();
		StringBuilder gnamelog = new StringBuilder();
		for (String gids : gid) {
			GroupName gname = adminService.getGroupNameByGId(Long.parseLong(gids));
			if (gname == null || gname.getUid() != currentUser.getUserid()) {
				return "you are at invalid Destination ! Please go back!";
			}
			gnamelog.append(gname).append(",");

			mobileNumberList.addAll(userService.getContactByGid(gname.getGid()));
		}
		if (serviceType.getTemplatecheck() && !isvalidtemplate(message, currentUser.getUserid())) {
			return "Message Not Match With Template";

		}

		boolean isUnicode = StringUtils.isUnicode(message);
		int crdeduct = StringUtils.SMSCounter(message.length(), isUnicode);

		int crdup = smsService.creditUpdate(crdeduct * mobileNumberList.size(), currentUser.getUserid(),
				serviceType.getStid());

		if (crdup == 0) {
			System.out.print("Insuffician credit of User id:" + currentUser.getUserid() + "::Message cannot be sent"
					+ new Date());
			return "insufficiant credit";
		}

		String uuid = UUID.randomUUID().toString();
		CampaignLog cl = new CampaignLog(currentUser.getUserid(), gnamelog.toString(), uuid, CURR_DATE, CURR_DATE,
				mobileNumberList.size(), 0, "GROUPWEB", "", crdeduct);
		cl = smsService.saveCampaign(cl);

		for (int i = 0; i < mobileNumberList.size(); i++) {

			SmsQueue sq = new SmsQueue(currentUser.getUserid(), serviceType.getPriority(), message,
					mobileNumberList.get(i), serviceType.getStid(), !serviceType.getDndcheck(), false, isUnicode,
					senderId, CURR_DATE, 0, (int) cl.getCampid(), crdeduct);
			lstsmsqueue.add(sq);
		}
		new FilterNSaveJob().filterNSave(lstsmsqueue, smsService, currentUser);
		return "Message SuccessFully Submitted!";
	}

	@RequestMapping(value = "deleteApi")
	public String deleteApi(HttpServletRequest request, HttpServletResponse response) {
		String aid = request.getParameter("aid");
		userService.deteleApiKey(aid);
		return "redirect:/#/Api";

	}

	public boolean isvalidtemplate(String sms, Long userid) throws Exception {

		List<Tamplate> spamText = null;

		spamText = userService.getTemplateByuser(userid);
		if (spamText != null && spamText.size() > 0) {

			for (Tamplate spmText : spamText) {
				Pattern pattern = Pattern.compile(spmText.getTname(), Pattern.CASE_INSENSITIVE);

				Matcher matcher = pattern.matcher(sms);
				if (matcher.find())
					return true;
			}
		}
		return false;
	}

	@RequestMapping(value = "SaveUrlSetting", method = RequestMethod.POST)
	public @ResponseBody String SaveUrlSetting(HttpServletRequest request, HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		if (currentUser.getUserid() != 1) {
			return "Unauthorized";
		}

		String apiText = request.getParameter("apitext");
		String priority = request.getParameter("priority");
		String successMeassage = request.getParameter("successMeassage");
		int uids = Integer.parseInt(request.getParameter("uid"));
		int sid = Integer.parseInt(request.getParameter("sid"));
		int isfailover = Integer.parseInt(request.getParameter("isfailover"));

		SmsApi smsApi = new SmsApi();
		smsApi.setApiText(apiText);
		smsApi.setIsFailOver(isfailover);

		smsApi.setPriorty(Integer.parseInt(priority));
		smsApi.setSuccessMsg(successMeassage);
		smsApi.setuId(uids);
		smsApi.setServiceid(sid);

		userService.saveHttpGateway(smsApi);

		/*
		 * if (!altapiText.equalsIgnoreCase("") ||
		 * !altpriority.equalsIgnoreCase("") ||
		 * !altsuccessMeassage.equalsIgnoreCase("")) { SmsApi smsApi2 = new
		 * SmsApi(); smsApi2.setApiText(altapiText);
		 * smsApi2.setPriorty(Integer.parseInt(altpriority));
		 * smsApi2.setSuccessMsg(altsuccessMeassage); smsApi2.setuId(uids);
		 * userService.saveSmsApi(smsApi2); }
		 */

		return "URL SAVED";

	}

	@RequestMapping(value = "findHttpGateway", method = RequestMethod.GET)
	public @ResponseBody String findHttpGateway(@RequestParam(value = "t", required = true) int type,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		if (currentUser.getUserid() != 1) {
			return "Unauthorized";
		}

		return userService.findHttpGateway(type);
	}

	@RequestMapping(value = "deleteHttpGatewaybyid", method = RequestMethod.GET)
	public @ResponseBody String deleteHttpGatewaybyid(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		if (currentUser.getUserid() != 1) {
			return "Unauthorized";
		}
		Long hid = Long.parseLong(request.getParameter("hid"));
		userService.deleteHttpGateway(hid);
		return 1 + "";
	}

	@RequestMapping(value = "getCountSmsQueue", method = RequestMethod.GET)
	public @ResponseBody String getCountSmsQueue(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		String cnt = userService.getQueueCount(currentUser.getUserid());
		System.out.println("Cnt ------ > " + cnt);
		return cnt;
	}

	@RequestMapping(value = "getCountSentSms", method = RequestMethod.GET)
	public @ResponseBody String getCountSentSms(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		System.out.println(currentUser.getUserid());
		String cnt = userService.getSentSms(currentUser.getUserid());
		System.out.println("Cnt ------ > " + cnt);
		return cnt;
	}

	@RequestMapping(value = "getCountTodaySentSms", method = RequestMethod.GET)
	public @ResponseBody String getCountTodaySentSms(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		String cnt = userService.getSentSmsToday(currentUser.getUserid(), sdf.format(date));
		System.out.println("Cnt ------ > " + cnt);
		return cnt;
	}

	@RequestMapping(value = "getSentMessage", method = RequestMethod.POST)
	public @ResponseBody String getSentMessage(HttpServletRequest request, HttpServletResponse response) {

		int page = 1;
		int listSize = 10;
		int ttlpage = 10;

		List jsonList = userService.getSmsHistory(page, listSize);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		MediUser currentUser = (MediUser) auth.getPrincipal();
		System.out.println("In Sent message ------ > " + request.getParameter("t_currentPage"));

		return new Gson().toJson(jsonList);
	}

	@RequestMapping(value = "user/activedeactive", method = RequestMethod.GET)
	public @ResponseBody int activeDeactive(HttpServletRequest request, HttpServletResponse response) {

		Long id = Long.parseLong(request.getParameter("uid"));

		int act = Integer.parseInt(request.getParameter("isactive"));

		// System.out.println("delete id" + id);
		/*
		 * if(act==0){ smsService.moveToFailedSmsQueue(id); }
		 */
		return adminService.updateStatusUser(id, act);

	}

	@RequestMapping(value = "user/defaultSender/{sid}", method = RequestMethod.GET)
	public @ResponseBody int defaultSender(@PathVariable("sid") int sid) {
		// Long id = Long.parseLong(request.getParameter("uid"));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		return adminService.updateDefaultSender(currentUser.getUserid(), sid);

	}

}

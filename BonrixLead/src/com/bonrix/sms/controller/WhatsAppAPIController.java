package com.bonrix.sms.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bonrix.sms.dto.WhatsAppMessage;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.WhatsAppTemplate;
import com.bonrix.sms.model.WhatsappMedia;
import com.bonrix.sms.model.WhatsappOutbox;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.UserService;
import com.bonrix.sms.service.WhatsAppMediaService;
import com.bonrix.sms.service.WhatsAppService;
import com.bonrix.sms.service.WhatsappOutboxService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
  
@Controller
public class WhatsAppAPIController {

	@Autowired
	WhatsAppService WService;

	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;
	
	@Autowired
	WhatsAppMediaService WMService;
	
	@Autowired
	WhatsappOutboxService outboxServie;
	
	@Autowired
	WhatsappOutboxService outboxService;
	
	final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 
	@RequestMapping(value = "GetTelecallerdWhatsAppTemplate", produces = "application/javascript")
	public @ResponseBody String GetTelecallerdWhatsAppTemplate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallerusername") String telecallerusername,
			@RequestParam(value = "apikey") String api) throws Exception {
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(telecallerusername);

		ApiKey apiKey = null;

		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);
	if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		}

		if (apiKey != null) {
			List list = WService.getWhatsAppTemplateByuser(tclaer.getCompanyId());
			if (list.size() != 0) {
				for (int i = 0; i < list.size(); i++) {
					
					Object[] result = (Object[]) list.get(i);
					if (result[3].toString().equalsIgnoreCase("APPROVED")) {
						JSONObject leadmap = new JSONObject();
						leadmap.put("id", result[0]);
						leadmap.put("tempName", result[1]);
						leadmap.put("subetemplateId", 0);
						jarray.add(leadmap);
						List<WhatsappMedia> media=	WMService.getWhatsAppMedia(Long.parseLong(result[0].toString()));
						for(WhatsappMedia temp:media)
						{
							JSONObject leadmap1 = new JSONObject();
							leadmap1.put("id", result[0]);
							leadmap1.put("tempName",result[1]+"**"+temp.getName());
							leadmap1.put("subetemplateId", temp.getId());
							jarray.add(leadmap1);
						}
						System.out.println(media.size());
					
					}
				}
			} else {
				if (request.getParameter("callback") != null) {
					return request.getParameter("callback") + "([{\"responceCode\":\"No Data Found\"}]);";
				} else {
					return "[{\"responceCode\":\"No Data Found\"}]";
				}
			}
		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}

	@RequestMapping(value = "SendWhatsAppMessage", produces = "application/javascript")
	public @ResponseBody String SendWhatsAppMessage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallerusername") String telecallerusername,
			@RequestParam(value = "templateName") long templateId,
			@RequestParam(value = "recipientphonenumber") String recipientphonenumber,
			@RequestParam(value = "subetemplateId") long subetemplateId,
			@RequestParam(value = "apikey") String api,
			@RequestParam(value = "ledId") long leadId) throws Exception {

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(telecallerusername);

		ApiKey apiKey = null;

		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		} 
		WhatsAppTemplate template=WService.GetWhatsAppTemplateByName(templateId);
		if (apiKey != null) {
			String jsonData=null;
			List list = WService.GetWhatsAppSettingsByUserId(tclaer.getCompanyId());
			Object[] settings = (Object[]) list.get(0);
			String apiUrl = "https://graph.facebook.com/v17.0/"+settings[3].toString()+"/messages"; // Replace with
			String media_url="";
			long mediaId = 0;
			if(subetemplateId!=0) {
				WhatsappMedia media=	WMService.getWhatsAppMediaTemplate(subetemplateId);
				media_url=media.getPath();
				mediaId = media.getId();
				}
			else
				media_url=template.getMedia_url();
			if(template.getMedia_template_type().equalsIgnoreCase("IMAGE"))
				jsonData="{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"+recipientphonenumber+"\",\"type\":\"template\",\"template\":{\"name\":\""+template.getTemplate_name()+"\",\"language\":{\"code\":\"en\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"image\",\"image\":{\"link\":\""+media_url+"\"}}]}]}}";
			else if(template.getMedia_template_type().equalsIgnoreCase("DOCUMENT"))
				jsonData="{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"+recipientphonenumber+"\",\"type\":\"template\",\"template\":{\"name\":\""+template.getTemplate_name()+"\",\"language\":{\"code\":\"en\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"document\",\"document\":{\"link\":\""+media_url+"\",\"filename\":\"Document\"}}]}]}}";
			else
			 jsonData="{\"messaging_product\":\"whatsapp\",\"to\":\""+"91"+recipientphonenumber+"\",\"type\":\"template\",\"template\":{\"name\":\""+template.getTemplate_name()+"\",\"language\":{\"code\":\"en_US\"}}}";																				// your
		
			
			 System.out.println(apiUrl);
		System.out.println(jsonData);
			String bearerToken = settings[2].toString(); // Replace with your actual Bearer token
		        try {
		            URL url = new URL(apiUrl);
		            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		            connection.setRequestMethod("POST");

		            connection.setRequestProperty("Authorization", "Bearer " + bearerToken);

		            connection.setRequestProperty("Content-Type", "application/json");

		            connection.setDoOutput(true);

		            try (OutputStream os = connection.getOutputStream()) {
		                byte[] input = jsonData.getBytes("utf-8");
		                os.write(input, 0, input.length); 
		            }

		            int responseCode = connection.getResponseCode();

		            if (responseCode == HttpURLConnection.HTTP_OK) {

		            	BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		                String line;
		                StringBuilder response1 = new StringBuilder();

		                while ((line = reader.readLine()) != null) {
		                	response1.append(line);
		                }  
		                System.err.println("response : " + response1);
		                reader.close();
		                
		                try {
							String responcejson = response1.toString();

							ObjectMapper objectMapper = new ObjectMapper();
							WhatsAppMessage whatsAppMessage = objectMapper.readValue(responcejson, WhatsAppMessage.class);
							WhatsappOutbox Whatsapplog = new WhatsappOutbox();
							Whatsapplog.setCompanyId(Long.parseLong(""+tclaer.getCompanyId()));
							Whatsapplog.setLeadId(leadId);
							Whatsapplog.setMediaId(mediaId);
							Whatsapplog.setMsgId(whatsAppMessage.getMessages().get(0).getId());
							Whatsapplog.setRequest(jsonData);
							Whatsapplog.setResponse(responcejson);
							Whatsapplog.setSendTime(new Date());
							Whatsapplog.setStatus(whatsAppMessage.getMessages().get(0).getMessageStatus());
							Whatsapplog.setTelecallerId(Long.parseLong(""+tclaer.getTcallerid()));
							Whatsapplog.setTemplateId(template.getTemplate_id());
							Whatsapplog.setToNumber(whatsAppMessage.getContacts().get(0).getInput());
							outboxService.saveWhatsappOutbox(Whatsapplog);

						} catch (Exception e) {
							e.printStackTrace();
						}
		                
		            } else {
		                System.err.println("HTTP Request Failed with response code: " + responseCode);
		            }
		            connection.disconnect();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		}
		return "[{\"responseCode\":\"Message Successfully Send.\"}]";

	}
	
	
	@RequestMapping(value = "SendWhatsAppMessageSatus", produces = "application/javascript")
	public ResponseEntity<String>  SendWhatsAppMessage(@RequestBody String payload) throws Exception {

		try {
            // Parse the JSON payload
            JSONObject json = new JSONObject(payload);

            // Get the object type
            String objectType = json.getString("object");
            System.out.println("Object Type: " + objectType);

            // Get the entry array
            JSONArray entries = json.getJSONArray("entry");
            for (int i = 0; i < entries.length(); i++) {
                JSONObject entry = entries.getJSONObject(i);
                String entryId = entry.getString("id");
                System.out.println("Entry ID: " + entryId);

                JSONArray changes = entry.getJSONArray("changes");
                for (int j = 0; j < changes.length(); j++) {
                    JSONObject change = changes.getJSONObject(j);
                    String field = change.getString("field");
                    System.out.println("Field: " + field);

                    JSONObject value = change.getJSONObject("value");
                    String messagingProduct = value.getString("messaging_product");
                    System.out.println("Messaging Product: " + messagingProduct);

                    // Get statuses array
                    JSONArray statuses = value.getJSONArray("statuses");
                    for (int k = 0; k < statuses.length(); k++) {
                        JSONObject status = statuses.getJSONObject(k);
                        String statusId = status.getString("id");
                        String statusText = status.getString("status");
                        String recipientId = status.getString("recipient_id");

                        
                       WhatsappOutbox sentMessage= outboxServie.getMessage(statusId);
                     sentMessage.setStatus(statusText);
                       outboxServie.updateStatus(sentMessage);
                       
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Invalid JSON format", HttpStatus.BAD_REQUEST);
        }
		return new ResponseEntity<>("Webhook received successfully", HttpStatus.OK);  

	}
	
	@RequestMapping(value = "/getTelecallerWhatsAppLogs", method = RequestMethod.GET, produces = "application/javascript")
	public @ResponseBody String getTelecallerWhatsAppLogs(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String tallycallerName = request.getParameter("telecallerusername");
		String api = request.getParameter("apikey");
		String entrydate=request.getParameter("entrydate");
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(tallycallerName);

		if (tclaer == null) {
			return "[{\"responseCode\":\"INVALID_TELECALLER\"}]";
		}
		ApiKey apiKey = null;

		if (tclaer != null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);

			if (apiKey == null) {
				apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
				if (apiKey == null) {
					return "[{\"responseCode\":\"INVALID_KEY\"}]";
				}
			}
		}
		Object[] result = null;
		if (apiKey != null) {
			String query = " select template_id,sendtime,request,response,status,to_number from whatsappoutbox where telecaller_id="+tclaer.getTcallerid()+" and sendtime BETWEEN '"+entrydate+" 00:00:00' AND '"+entrydate+" 23:59:59'";
			System.out.println("query_:"+query);
			List list = adminService.getTelecallerStateLeadCount(query);
			if (list.size() != 0) {

				for (int i = 0; i < list.size(); i++) {
					JSONObject leadmap = new JSONObject();

					result = (Object[]) list.get(i);
					WhatsAppTemplate template = WService.GetWhatsAppTemplate(Long.parseLong( result[0].toString()));
					leadmap.put("templateName", template.getTemplate_name());
					leadmap.put("Mobile", result[5]);
					leadmap.put("SendTime", result[1]);
					leadmap.put("Status", result[4]);
					
					jarray.add(leadmap);

				}

			} else {

				if (request.getParameter("callback") != null)
					return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
				else
					return "[{\"responceCode\":\"No Data Found\"}]";

			}

		}
		if (request.getParameter("callback") != null) {
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		} else {
			return jarray.toJSONString();
		}

	}
	
	@RequestMapping(value = "SendTelecallerBulkWhatsAppMessage", produces = "application/javascript")
	public @ResponseBody String SendTelecallerBulkWhatsAppMessage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "telecallerusername") String telecallerusername,
			@RequestParam(value = "templateName") long templateId,
			@RequestParam(value = "subetemplateId") long subetemplateId,
			@RequestParam(value = "apikey") String api,
			@RequestParam(value = "whatsAppleadId") String whatsAppleadId) throws Exception {

		Tallycaller tclaer = new Tallycaller();
		tclaer = adminService.GetTellyCallerByName(telecallerusername);

		ApiKey apiKey = null;

		apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, true);
		if (apiKey == null) {
			apiKey = userService.gettAPiByUid(tclaer.getTcallerid(), api, false);
			if (apiKey == null) {
				return "[{\"responseCode\":\"INVALID_KEY\"}]";
			}
		} 
		String[] leadIds=whatsAppleadId.split(",");
		for(String leadId:leadIds)
		{
			if (apiKey != null) {
				String jsonData=null;
				WhatsAppTemplate template=WService.GetWhatsAppTemplateByName(templateId);
				Lead lead=adminService.GetLeadObjectByLeadId(Integer.parseInt(leadId));
				String recipientphonenumber=lead.getMobileNo();
				List list = WService.GetWhatsAppSettingsByUserId(tclaer.getCompanyId());
				Object[] settings = (Object[]) list.get(0);
				String apiUrl = "https://graph.facebook.com/v17.0/"+settings[3].toString()+"/messages"; // Replace with
				String media_url="";
				long mediaId = 0;
				if(subetemplateId!=0) {  
					WhatsappMedia media=	WMService.getWhatsAppMediaTemplate(subetemplateId);
					media_url=media.getPath();
					mediaId = media.getId();
					}
				else
					media_url=template.getMedia_url();
				if(template.getMedia_template_type().equalsIgnoreCase("IMAGE"))
					jsonData="{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"+recipientphonenumber+"\",\"type\":\"template\",\"template\":{\"name\":\""+template.getTemplate_name()+"\",\"language\":{\"code\":\"en\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"image\",\"image\":{\"link\":\""+media_url+"\"}}]}]}}";
				else if(template.getMedia_template_type().equalsIgnoreCase("DOCUMENT"))
					jsonData="{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"+recipientphonenumber+"\",\"type\":\"template\",\"template\":{\"name\":\""+template.getTemplate_name()+"\",\"language\":{\"code\":\"en\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"document\",\"document\":{\"link\":\""+media_url+"\",\"filename\":\"Document\"}}]}]}}";
				else
				 jsonData="{\"messaging_product\":\"whatsapp\",\"to\":\""+"91"+recipientphonenumber+"\",\"type\":\"template\",\"template\":{\"name\":\""+template.getTemplate_name()+"\",\"language\":{\"code\":\"en_US\"}}}";																				// your
			
				
				 System.out.println(apiUrl);
			System.out.println(jsonData);
				String bearerToken = settings[2].toString(); // Replace with your actual Bearer token
			        try {
			            URL url = new URL(apiUrl);
			            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			            connection.setRequestMethod("POST");

			            connection.setRequestProperty("Authorization", "Bearer " + bearerToken);

			            connection.setRequestProperty("Content-Type", "application/json");

			            connection.setDoOutput(true);

			            try (OutputStream os = connection.getOutputStream()) {
			                byte[] input = jsonData.getBytes("utf-8");
			                os.write(input, 0, input.length); 
			            }

			            int responseCode = connection.getResponseCode();

			            if (responseCode == HttpURLConnection.HTTP_OK) {

			            	BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			                String line;
			                StringBuilder response1 = new StringBuilder();

			                while ((line = reader.readLine()) != null) {
			                	response1.append(line);
			                }  
			                System.err.println("response : " + response1);
			                reader.close();
			                
			                try {
								String responcejson = response1.toString();

								ObjectMapper objectMapper = new ObjectMapper();
								WhatsAppMessage whatsAppMessage = objectMapper.readValue(responcejson, WhatsAppMessage.class);
								WhatsappOutbox Whatsapplog = new WhatsappOutbox();
								Whatsapplog.setCompanyId(Long.parseLong(""+tclaer.getCompanyId()));
								Whatsapplog.setLeadId(Long.parseLong(""+lead.getLeaadId()));
								Whatsapplog.setMediaId(mediaId);
								Whatsapplog.setMsgId(whatsAppMessage.getMessages().get(0).getId());
								Whatsapplog.setRequest(jsonData);
								Whatsapplog.setResponse(responcejson);
								Whatsapplog.setSendTime(new Date());
								Whatsapplog.setStatus(whatsAppMessage.getMessages().get(0).getMessageStatus());
								Whatsapplog.setTelecallerId(Long.parseLong(""+tclaer.getTcallerid()));
								Whatsapplog.setTemplateId(template.getTemplate_id());
								Whatsapplog.setToNumber(whatsAppMessage.getContacts().get(0).getInput());
								outboxService.saveWhatsappOutbox(Whatsapplog);

							} catch (Exception e) {
								e.printStackTrace();
							}
			                
			            } else {
			                System.err.println("HTTP Request Failed with response code: " + responseCode);
			            }
			            connection.disconnect();
			        } catch (IOException e) {
			            e.printStackTrace();
			        }
			}
		}
		
		
		return "[{\"responseCode\":\"Message Successfully Send.\"}]";

	}
	
	
}

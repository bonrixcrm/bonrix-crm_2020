package com.bonrix.sms.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bonrix.sms.dto.WhatsAppMessage;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.WhatsAppSettings;
import com.bonrix.sms.model.WhatsAppTemplate;
import com.bonrix.sms.model.WhatsappMedia;
import com.bonrix.sms.model.WhatsappOutbox;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.WhatsAppMediaService;
import com.bonrix.sms.service.WhatsAppService;
import com.bonrix.sms.service.WhatsappOutboxService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
@MultipartConfig
public class WhatsAppController {

	@Autowired
	WhatsAppService WService;

	@Autowired
	WhatsAppMediaService WMService;

	@Autowired
	WhatsappOutboxService outboxService;

	@Autowired
	AdminService adminService;

	Logger log = Logger.getLogger(WhatsAppController.class);
	final SimpleDateFormat scheduledf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@RequestMapping(value = "/saveWhatsAppSettings", method = RequestMethod.GET)
	public @ResponseBody String saveWhatsAppSettings(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		WhatsAppSettings setting = new WhatsAppSettings();

		setting.setMedia_url(request.getParameter("media_url"));
		setting.setPhone_number_id(request.getParameter("phone_number_id"));
		setting.setToken(request.getParameter("token"));
		setting.setWhatsapp_business_account_id(request.getParameter("whatsapp_business_account_id"));
		setting.setWhatsapp_number(request.getParameter("whatsapp_number"));
		setting.setUser_id(currentUser.getUserid());
		WService.saveWhatsAppSettings(setting);
		return "Settingd Successfully Saved.";
	}

	@RequestMapping(value = "/GetWhatsAppSettingsByUserId", method = RequestMethod.GET)
	public @ResponseBody String GetWhatsAppSettingsByUserId(HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List list = WService.GetWhatsAppSettingsByUserId((int) (long) currentUser.getUserid());
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(list);
		return new GsonBuilder().setPrettyPrinting().create().toJson(datatableJsonObject);

	}

	@RequestMapping(value = "/SysWhatsAppTemplate", method = RequestMethod.GET)
	public @ResponseBody String SysWhatsAppTemplate(HttpServletRequest request, HttpServletResponse response1)
			throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		List list = WService.GetWhatsAppSettingsByUserId((int) (long) currentUser.getUserid());
		Object[] settings = (Object[]) list.get(0);
		String apiUrl = "https://graph.facebook.com/v17.0/" + settings[4] + "/message_templates"; // Replace with your
																									// API endpoint URL
		String bearerToken = settings[2].toString(); // Replace with your actual Bearer token

		try {
			URL url = new URL(apiUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("GET");

			connection.setRequestProperty("Authorization", "Bearer " + bearerToken);

			int responseCode = connection.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				StringBuilder response = new StringBuilder();

				while ((line = reader.readLine()) != null) {
					response.append(line);
				}
				reader.close();

				JSONObject dataJson = new JSONObject(response.toString());
				JSONArray dataArray = dataJson.getJSONArray("data");

				for (int i = 0; i < dataArray.length(); i++) {
					JSONObject dataObject = (JSONObject) dataArray.get(i);
					JSONObject bodyObject = null;
					JSONObject headerObject = null;
					JSONObject formatObject = null;
					JSONArray componentsArray = dataObject.getJSONArray("components");
					if (componentsArray.length() > 1) {
						headerObject = (JSONObject) componentsArray.get(0);
						bodyObject = (JSONObject) componentsArray.get(1);
						formatObject = (JSONObject) componentsArray.get(0);
					} else {
						headerObject = (JSONObject) componentsArray.get(0);
						bodyObject = (JSONObject) componentsArray.get(0);
						formatObject = (JSONObject) componentsArray.get(0);
					}
					WhatsAppTemplate savedTemplate = WService
							.GetWhatsAppTemplate(Long.parseLong(dataObject.get("id").toString()));

					if (savedTemplate == null) {
						// WService.deleteTemplate(Long.parseLong(dataObject.get("id").toString()));

						WhatsAppTemplate template = new WhatsAppTemplate();
						template.setTemplate_id(Long.parseLong(dataObject.get("id").toString()));
						template.setUserid((int) (long) currentUser.getUserid());
						if (bodyObject.get("type").toString().equalsIgnoreCase("BODY"))
							template.setTemplate_body(bodyObject.get("text").toString().getBytes("UTF-8"));
						else
							template.setTemplate_body(headerObject.get("text").toString().getBytes("UTF-8"));
						template.setTemplate_name(dataObject.get("name").toString());
						template.setTemplate_status(dataObject.get("status").toString());
						template.setHeader("NA".getBytes("UTF-8"));
						template.setMedia_url("NA");
						if (formatObject.has("format")) {
							if (formatObject.get("format").toString().equalsIgnoreCase("VIDEO")
									|| formatObject.get("format").toString().equalsIgnoreCase("DOCUMENT")
									|| formatObject.get("format").toString().equalsIgnoreCase("IMAGE")
									|| formatObject.get("format").toString().equalsIgnoreCase("TEXT"))
								template.setMedia_template_type(headerObject.get("format").toString());
						} else
							template.setMedia_template_type("TEXT");

						template.setLanguage(dataObject.get("language").toString());
						WService.saveWhatsAppTemplate(template);
					}
				}

			} else {
				System.err.println("HTTP Request Failed with response code: " + responseCode);
			}
			connection.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	@RequestMapping(value = "/UpdateWhatsAppTemplate", method = RequestMethod.GET)
	public @ResponseBody String UpdateWhatsAppTemplate(HttpServletRequest request, HttpServletResponse response1)
			throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		List list = WService.GetWhatsAppSettingsByUserId((int) (long) currentUser.getUserid());
		Object[] settings = (Object[]) list.get(0);
		String apiUrl = "https://graph.facebook.com/v17.0/" + settings[4] + "/message_templates/?name="
				+ request.getParameter("templateName"); // Replace with your
		String bearerToken = settings[2].toString(); // Replace with your actual Bearer token

		try {
			URL url = new URL(apiUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("GET");

			connection.setRequestProperty("Authorization", "Bearer " + bearerToken);

			int responseCode = connection.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				StringBuilder response = new StringBuilder();

				while ((line = reader.readLine()) != null) {
					response.append(line);
				}
				reader.close();

				JSONObject dataJson = new JSONObject(response.toString());
				JSONArray dataArray = dataJson.getJSONArray("data");

				for (int i = 0; i < dataArray.length(); i++) {
					JSONObject dataObject = (JSONObject) dataArray.get(i);
					JSONObject bodyObject = null;
					JSONObject headerObject = null;
					JSONObject formatObject = null;
					JSONArray componentsArray = dataObject.getJSONArray("components");
					if (componentsArray.length() > 1) {
						headerObject = (JSONObject) componentsArray.get(0);
						bodyObject = (JSONObject) componentsArray.get(1);
						formatObject = (JSONObject) componentsArray.get(0);
					} else {
						headerObject = (JSONObject) componentsArray.get(0);
						bodyObject = (JSONObject) componentsArray.get(0);
						formatObject = (JSONObject) componentsArray.get(0);
					}
					WhatsAppTemplate savedTemplate = WService
							.GetWhatsAppTemplate(Long.parseLong(dataObject.get("id").toString()));

					if (savedTemplate != null) {
						if (bodyObject.get("type").toString().equalsIgnoreCase("BODY"))
							savedTemplate.setTemplate_body(bodyObject.get("text").toString().getBytes("UTF-8"));
						else
							savedTemplate.setTemplate_body(headerObject.get("text").toString().getBytes("UTF-8"));
						savedTemplate.setTemplate_name(dataObject.get("name").toString());
						savedTemplate.setTemplate_status(dataObject.get("status").toString());
						if (formatObject.has("format")) {
							if (formatObject.get("format").toString().equalsIgnoreCase("VIDEO")
									|| formatObject.get("format").toString().equalsIgnoreCase("DOCUMENT")
									|| formatObject.get("format").toString().equalsIgnoreCase("IMAGE")
									|| formatObject.get("format").toString().equalsIgnoreCase("TEXT"))
								savedTemplate.setMedia_template_type(headerObject.get("format").toString());
						} else
							savedTemplate.setMedia_template_type("TEXT");

						savedTemplate.setLanguage(dataObject.get("language").toString());
						WService.saveWhatsAppTemplate(savedTemplate);
					}
				}

			} else {
				System.err.println("HTTP Request Failed with response code: " + responseCode);
			}
			connection.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	@RequestMapping(value = "/GetWhatsAppTemplateByUserId", method = RequestMethod.GET)
	public @ResponseBody String GetWhatsAppTemplateByUserId(HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List list = WService.getWhatsAppTemplateByuser((int) (long) currentUser.getUserid());
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(list);
		return new GsonBuilder().setPrettyPrinting().create().toJson(datatableJsonObject);

	}

	@RequestMapping(value = "/deleteWhatsAppTemplateByUserId", method = RequestMethod.GET)
	public @ResponseBody String deleteWhatsAppTemplateByUserId(HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		WService.deleteTemplate(Long.parseLong(request.getParameter("templateId").toString()));
		return "Tempalte Delete Successfully.";

	}

	@RequestMapping(value = "/GetWhatsAppTemplateByUserIdLead", method = RequestMethod.GET)
	public @ResponseBody String GetWhatsAppTemplateByUserIdLead(HttpServletRequest request,
			HttpServletResponse response) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List list = WService.getWhatsAppTemplateByuser((int) (long) currentUser.getUserid());
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		if (list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {

				Object[] result = (Object[]) list.get(i);
				if (result[3].toString().equalsIgnoreCase("APPROVED")) {
					JSONObject leadmap = new JSONObject();
					leadmap.put("id", result[0]);
					leadmap.put("tempName", result[1]);
					leadmap.put("subetemplateId", 0);
					jarray.add(leadmap);
					List<WhatsappMedia> media = WMService.getWhatsAppMedia(Long.parseLong(result[0].toString()));
					for (WhatsappMedia temp : media) {
						JSONObject leadmap1 = new JSONObject();
						leadmap1.put("id", result[0]);
						leadmap1.put("tempName", result[1] + "**" + temp.getName());
						leadmap1.put("subetemplateId", temp.getId());
						jarray.add(leadmap1);
					}
					System.out.println(media.size());

				}
			}
		}
		return jarray.toString();
	}

	@RequestMapping(value = "/uploadWhatsAppMedia", method = RequestMethod.POST)
	public @ResponseBody String uploadWhatsAppMedia(@RequestParam("mediaFile") MultipartFile mediaFile,
			@RequestParam("templateId") long templateId, HttpServletRequest request) {
		String fileName = null;
		String filePath = null;
		if (!mediaFile.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				filePath = request.getServletContext().getRealPath("//");
				filePath = filePath + SEP + "WhatsAppMedia" + SEP;
				fileName = templateId + mediaFile.getOriginalFilename();

				File directory = new File(filePath);
				if (!directory.exists()) {
					directory.mkdir();
				}
				byte[] bytes = mediaFile.getBytes();
				BufferedOutputStream buffStream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath + fileName)));
				buffStream.write(bytes);
				buffStream.close();
				WService.updateTemplateMedia(templateId, "http://crmdemo.bonrix.in/WhatsAppMedia/" + fileName);
				return "You have successfully uploaded " + fileName;
			} catch (Exception e) {
				return "You failed to upload " + fileName + ": " + e.getMessage();
			}
		} else {
			return "Unable to upload. File is empty.";
		}
	}

	@RequestMapping(value = "SendManagerWhatsAppMessage", produces = "application/javascript")
	public @ResponseBody String SendWhatsAppMessage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "templateName") long templateName,
			@RequestParam(value = "recipientphonenumber") String recipientphonenumber,
			@RequestParam(value = "subetemplateId") long subetemplateId,
			@RequestParam(value = "whatsAppleadId") long whatsAppleadId) throws Exception {
		 TimeZone.setDefault(TimeZone.getTimeZone("Asia/Kolkata")); 
		WhatsAppTemplate template = WService.GetWhatsAppTemplateByName(templateName);
		String jsonData = null;
		long mediaId = 0;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		String media_url = "";
		if (subetemplateId != 0) {
			WhatsappMedia media = WMService.getWhatsAppMediaTemplate(subetemplateId);
			media_url = media.getPath();  
			mediaId = media.getId();
		} else
			media_url = template.getMedia_url();
		List list = WService.GetWhatsAppSettingsByUserId((int) (long) currentUser.getUserid());
		Object[] settings = (Object[]) list.get(0);
		String apiUrl = "https://graph.facebook.com/v17.0/" + settings[3].toString() + "/messages"; // Replace with
		if (template.getMedia_template_type().equalsIgnoreCase("IMAGE"))
			jsonData = "{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"
					+ recipientphonenumber + "\",\"type\":\"template\",\"template\":{\"name\":\""
					+ template.getTemplate_name() + "\",\"language\":{\"code\":\"" + template.getLanguage()
					+ "\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"image\",\"image\":{\"link\":\""
					+ media_url + "\"}}]}]}}";
		else if (template.getMedia_template_type().equalsIgnoreCase("DOCUMENT"))
			jsonData = "{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"
					+ recipientphonenumber + "\",\"type\":\"template\",\"template\":{\"name\":\""
					+ template.getTemplate_name() + "\",\"language\":{\"code\":\"" + template.getLanguage()
					+ "\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"document\",\"document\":{\"link\":\""
					+ media_url + "\",\"filename\":\"Document\"}}]}]}}";
		else if (template.getMedia_template_type().equalsIgnoreCase("video"))
			jsonData = "{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"
					+ recipientphonenumber + "\",\"type\":\"template\",\"template\":{\"name\":\""
					+ template.getTemplate_name() + "\",\"language\":{\"code\":\"" + template.getLanguage()
					+ "\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"video\",\"video\":{\"link\":\""
					+ media_url + "\"}}]}]}}";
		else
			jsonData = "{\"messaging_product\":\"whatsapp\",\"to\":\"" + "91" + recipientphonenumber
					+ "\",\"type\":\"template\",\"template\":{\"name\":\"" + template.getTemplate_name()
					+ "\",\"language\":{\"code\":\"" + template.getLanguage() + "\"}}}"; // your

		String bearerToken = settings[2].toString(); // Replace with your actual Bearer token
		try {
			URL url = new URL(apiUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("POST");

			connection.setRequestProperty("Authorization", "Bearer " + bearerToken);

			connection.setRequestProperty("Content-Type", "application/json");

			connection.setDoOutput(true);

			try (OutputStream os = connection.getOutputStream()) {
				byte[] input = jsonData.getBytes("utf-8");
				os.write(input, 0, input.length);
			}

			int responseCode = connection.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {

				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				StringBuilder response1 = new StringBuilder();

				while ((line = reader.readLine()) != null) {
					response1.append(line);
				}
				System.err.println("HTTP response : " + response1);
				reader.close();
				try {
					String responcejson = response1.toString();

					ObjectMapper objectMapper = new ObjectMapper();
					WhatsAppMessage whatsAppMessage = objectMapper.readValue(responcejson, WhatsAppMessage.class);
					WhatsappOutbox Whatsapplog = new WhatsappOutbox();
					Whatsapplog.setCompanyId(currentUser.getUserid());
					Whatsapplog.setLeadId(whatsAppleadId);
					Whatsapplog.setMediaId(mediaId);
					Whatsapplog.setMsgId(whatsAppMessage.getMessages().get(0).getId());
					Whatsapplog.setRequest(jsonData);
					Whatsapplog.setResponse(responcejson);
					Whatsapplog.setSendTime(new Date());
					Whatsapplog.setStatus(whatsAppMessage.getMessages().get(0).getMessageStatus());
					Whatsapplog.setTelecallerId(0L);
					Whatsapplog.setTemplateId(template.getTemplate_id());
					Whatsapplog.setToNumber(whatsAppMessage.getContacts().get(0).getInput());
					outboxService.saveWhatsappOutbox(Whatsapplog);

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				System.err.println("HTTP HTTP Request Failed with response code: " + responseCode);
			}
			connection.disconnect();
		} catch (IOException e) {
			System.err.println("HTTP " + e.getMessage());
			e.printStackTrace();
		}
		return "[{\"responseCode\":\"Message Successfully Send.\"}]";
	}

	@RequestMapping(value = "/deleteWhatsAppSettings", method = RequestMethod.GET)
	public @ResponseBody String deleteWhatsAppSettings(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		WService.deleteWhatsAppSettings(Long.parseLong(request.getParameter("id")));
		return "Settingd Successfully deleted!";
	}

	// @PostMapping("/upload")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String uploadWhatsAppBulkMedia(@RequestParam("files[]") List<MultipartFile> files,
			@RequestParam("descriptions[]") List<String> descriptions, HttpServletRequest request,
			@RequestParam("templateId") long templateId) throws IOException {
		String fileName = null;
		String filePath = null;
		if (files.size() != descriptions.size()) {
			return "Files and descriptions count do not match";
		}

		for (int i = 0; i < files.size(); i++) {
			MultipartFile mediaFile = files.get(i);
			String description = descriptions.get(i);

			String SEP = System.getProperty("file.separator");
			filePath = request.getServletContext().getRealPath("//");
			filePath = filePath + SEP + "WhatsAppMedia" + SEP;
			fileName = templateId + "_" + mediaFile.getOriginalFilename();
			System.out.println(filePath);
			File directory = new File(filePath);
			if (!directory.exists()) {
				directory.mkdir();
			}
			byte[] bytes = mediaFile.getBytes();
			BufferedOutputStream buffStream = new BufferedOutputStream(
					new FileOutputStream(new File(filePath + fileName)));
			buffStream.write(bytes);
			buffStream.close();
			WhatsappMedia media = new WhatsappMedia();
			media.setName(description);
			media.setPath("http://crmdemo.bonrix.in/WhatsAppMedia/" + fileName);
			media.setWhatsAppTemplate(WService.GetWhatsAppTemplate(templateId));
			WMService.saveWhatsAppMedia(media);
		}

		return "Upload successful!";
	}

	@RequestMapping(value = "/getWhatsAppLogs", method = RequestMethod.GET)
	public @ResponseBody String getWhatsAppLogs(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List<Object[]> ary = (List) new ArrayList<>();
		List<WhatsappOutbox> WhatsappOutbox = outboxService.getWhatsappOutbox(currentUser.getUserid(),
				scheduledf.parse(request.getParameter("date") + " 00:00:00"));
		for (WhatsappOutbox outbox : WhatsappOutbox) {
			WhatsAppTemplate template = WService.GetWhatsAppTemplate(outbox.getTemplateId());
			String username = "Na";
			if (outbox.getTelecallerId() != 0)
				username = adminService.getTallycallerByIdMdel(Integer.parseInt(outbox.getTelecallerId().toString()))
						.getUsername();
			Object[] str = { outbox.getToNumber(), template.getTemplate_name(), outbox.getStatus(),
					outbox.getSendTime(), username };
			ary.add(str);
		}
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(ary.size());
		datatableJsonObject.setRecordsTotal(ary.size());
		datatableJsonObject.setData(ary);
		Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
		String jsons = gson.toJson(datatableJsonObject);
		return jsons;
	}
	
	//@Async
	@RequestMapping(value = "SendManagerBulkWhatsAppMessage", produces = "application/javascript")
	public @ResponseBody String SendManagerBulkWhatsAppMessage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "templateName") long templateName,
			@RequestParam(value = "subetemplateId") long subetemplateId,
			@RequestParam(value = "whatsAppleadId") String whatsAppleadId) throws Exception {
		String[] leadIds=whatsAppleadId.split(",");
		for(String leadId:leadIds)
		{
			Lead lead=adminService.GetLeadObjectByLeadId(Integer.parseInt(leadId));
			String recipientphonenumber=lead.getMobileNo();
			 TimeZone.setDefault(TimeZone.getTimeZone("Asia/Kolkata")); 
				WhatsAppTemplate template = WService.GetWhatsAppTemplateByName(templateName);
				String jsonData = null;
				long mediaId = 0;
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				MediUser currentUser = (MediUser) auth.getPrincipal();
				String media_url = "";
				if (subetemplateId != 0) {
					WhatsappMedia media = WMService.getWhatsAppMediaTemplate(subetemplateId);
					media_url = media.getPath();  
					mediaId = media.getId();
				} else
					media_url = template.getMedia_url();
				List list = WService.GetWhatsAppSettingsByUserId((int) (long) currentUser.getUserid());
				Object[] settings = (Object[]) list.get(0);
				String apiUrl = "https://graph.facebook.com/v17.0/" + settings[3].toString() + "/messages"; // Replace with
				if (template.getMedia_template_type().equalsIgnoreCase("IMAGE"))
					jsonData = "{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"
							+ recipientphonenumber + "\",\"type\":\"template\",\"template\":{\"name\":\""
							+ template.getTemplate_name() + "\",\"language\":{\"code\":\"" + template.getLanguage()
							+ "\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"image\",\"image\":{\"link\":\""
							+ media_url + "\"}}]}]}}";
				else if (template.getMedia_template_type().equalsIgnoreCase("DOCUMENT"))
					jsonData = "{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"
							+ recipientphonenumber + "\",\"type\":\"template\",\"template\":{\"name\":\""
							+ template.getTemplate_name() + "\",\"language\":{\"code\":\"" + template.getLanguage()
							+ "\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"document\",\"document\":{\"link\":\""
							+ media_url + "\",\"filename\":\"Document\"}}]}]}}";
				else if (template.getMedia_template_type().equalsIgnoreCase("video"))
					jsonData = "{\"messaging_product\":\"whatsapp\",\"recipient_type\":\"individual\",\"to\":\"91"
							+ recipientphonenumber + "\",\"type\":\"template\",\"template\":{\"name\":\""
							+ template.getTemplate_name() + "\",\"language\":{\"code\":\"" + template.getLanguage()
							+ "\"},\"components\":[{\"type\":\"HEADER\",\"parameters\":[{\"type\":\"video\",\"video\":{\"link\":\""
							+ media_url + "\"}}]}]}}";
				else
					jsonData = "{\"messaging_product\":\"whatsapp\",\"to\":\"" + "91" + recipientphonenumber
							+ "\",\"type\":\"template\",\"template\":{\"name\":\"" + template.getTemplate_name()
							+ "\",\"language\":{\"code\":\"" + template.getLanguage() + "\"}}}"; // your

				String bearerToken = settings[2].toString(); // Replace with your actual Bearer token
				try {
					URL url = new URL(apiUrl);
					HttpURLConnection connection = (HttpURLConnection) url.openConnection();

					connection.setRequestMethod("POST");

					connection.setRequestProperty("Authorization", "Bearer " + bearerToken);

					connection.setRequestProperty("Content-Type", "application/json");

					connection.setDoOutput(true);

					try (OutputStream os = connection.getOutputStream()) {
						byte[] input = jsonData.getBytes("utf-8");
						os.write(input, 0, input.length);
					}

					int responseCode = connection.getResponseCode();

					if (responseCode == HttpURLConnection.HTTP_OK) {

						BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
						String line;
						StringBuilder response1 = new StringBuilder();

						while ((line = reader.readLine()) != null) {
							response1.append(line);
						}
						System.err.println("HTTP response : " + response1);
						reader.close();
						try {
							String responcejson = response1.toString();

							ObjectMapper objectMapper = new ObjectMapper();
							WhatsAppMessage whatsAppMessage = objectMapper.readValue(responcejson, WhatsAppMessage.class);
							WhatsappOutbox Whatsapplog = new WhatsappOutbox();
							Whatsapplog.setCompanyId(currentUser.getUserid());
							Whatsapplog.setLeadId(Long.parseLong(""+lead.getLeaadId()));
							Whatsapplog.setMediaId(mediaId);
							Whatsapplog.setMsgId(whatsAppMessage.getMessages().get(0).getId());
							Whatsapplog.setRequest(jsonData);
							Whatsapplog.setResponse(responcejson);
							Whatsapplog.setSendTime(new Date());
							Whatsapplog.setStatus(whatsAppMessage.getMessages().get(0).getMessageStatus());
							Whatsapplog.setTelecallerId(0L);
							Whatsapplog.setTemplateId(template.getTemplate_id());
							Whatsapplog.setToNumber(whatsAppMessage.getContacts().get(0).getInput());
							outboxService.saveWhatsappOutbox(Whatsapplog);

						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						System.err.println("HTTP HTTP Request Failed with response code: " + responseCode);
					}
					connection.disconnect();
				} catch (IOException e) {
					System.err.println("HTTP " + e.getMessage());
					e.printStackTrace();
				}
		}
		
		return "[{\"responseCode\":\"Message Successfully Send.\"}]";
	}
	
	

}

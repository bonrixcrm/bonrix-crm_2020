package com.bonrix.sms.dao;

import com.bonrix.sms.controller.SendCRMMail;
import com.bonrix.sms.dao.AdminDao;
import com.bonrix.sms.dao.AdminDaoImpl;
import com.bonrix.sms.dto.GetScheduleWiseLead;
import com.bonrix.sms.dto.LeadDTO;
import com.bonrix.sms.model.AegeCustDetail;
import com.bonrix.sms.model.Android_mode;
import com.bonrix.sms.model.Androidbuttonflag;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.AutoDialLog;
import com.bonrix.sms.model.CategoryManager;
import com.bonrix.sms.model.Contactinfo;
import com.bonrix.sms.model.Contacts;
import com.bonrix.sms.model.Crmemailsetting;
import com.bonrix.sms.model.Crmemailtemplate;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.DateWiseLeadCount;
import com.bonrix.sms.model.DisplaySetting;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.ExtraFields;
import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.GroupName;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.ImapSetting;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.LeadAliasToBean;
import com.bonrix.sms.model.Leadprocesstate;
import com.bonrix.sms.model.MessageAudit;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.MobileCallLog;
import com.bonrix.sms.model.Regexpatten;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.Sentsmslog;
import com.bonrix.sms.model.SmsCredit;
import com.bonrix.sms.model.Smssettings;
import com.bonrix.sms.model.Smstemplate;
import com.bonrix.sms.model.SystemNotification;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.Telecaller_break;
import com.bonrix.sms.model.Telecaller_work;
import com.bonrix.sms.model.Ticketchildmaster;
import com.bonrix.sms.model.Ticketmaster;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("AdminDao")
public class AdminDaoImpl implements AdminDao {
  @Autowired
  SessionFactory sessionFactory;
  
  final SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  
  final SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
  
  public void updateAssignCredit(SmsCredit smsCredit) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(smsCredit);
  }
  
  public SmsCredit getSmsCredit(long uid, long stid) {
    SmsCredit smsCredit = new SmsCredit();
    smsCredit = (SmsCredit)this.sessionFactory.getCurrentSession()
      .createQuery("from SmsCredit where uId=" + uid + " and stId =" + stid).uniqueResult();
    return smsCredit;
  }
  
  public void saveMessageAudit(MessageAudit messageAudit) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(messageAudit);
  }
  
  public User getUserByUid(Long uid) {
    User user = (User)this.sessionFactory.getCurrentSession().get(User.class, uid);
    return user;
  }
  
  public void deleteUser(Long id) {
    User user = new User();
    user = (User)this.sessionFactory.getCurrentSession().get(User.class, id);
    this.sessionFactory.getCurrentSession().delete(user);
  }
  
  public void deleteCredit(Long id) {
    SmsCredit smsCredit = new SmsCredit();
    smsCredit = (SmsCredit)this.sessionFactory.getCurrentSession().get(SmsCredit.class, id);
    this.sessionFactory.getCurrentSession().delete(smsCredit);
  }
  
  public void saveSenderName(SenderName senderName2) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(senderName2);
  }
  
  public SenderName getSenderNameById(Long sid) {
    SenderName senderName = (SenderName)this.sessionFactory.getCurrentSession().get(SenderName.class, sid);
    return senderName;
  }
  
  public void deleteSenderName(Long sid) {
    SenderName senderName = (SenderName)this.sessionFactory.getCurrentSession().get(SenderName.class, sid);
    this.sessionFactory.getCurrentSession().delete(senderName);
  }
  
  public void saveOrUpdateTamplate(Tamplate tamplate) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(tamplate);
  }
  
  public Tamplate getTamplateById(Long tid) {
    Tamplate tamplate = new Tamplate();
    tamplate = (Tamplate)this.sessionFactory.getCurrentSession().get(Tamplate.class, tid);
    return tamplate;
  }
  
  public void deleteTamplate(Long tid) {
    Tamplate tamplate = (Tamplate)this.sessionFactory.getCurrentSession().get(Tamplate.class, tid);
    this.sessionFactory.getCurrentSession().delete(tamplate);
  }
  
  public User getUserByUserName(String userName) {
    System.out.println("asdasd   " + userName);
    User user = new User();
    try {
      Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class)
        .add((Criterion)Restrictions.eq("username", userName));
      user = (User)criteria.uniqueResult();
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return user;
  }
  
  public List getCreditByUid(long userid) {
    String q = "SELECT u.uid,u.username,s.scid,s.smscredit,DATE_FORMAT(s.validity,'%d-%m-%Y') AS valdate,u.mobilenumber,u.email,st.type FROM smscredit s RIGHT JOIN users u ON s.uid=u.uid RIGHT JOIN servicetype st ON st.stid=s.stid WHERE u.parentid=" + 
      userid + " ORDER BY s.smscredit";
    return this.sessionFactory.getCurrentSession().createSQLQuery(q).list();
  }
  
  public List getUsersByUid(long userid) {
    String query = "SELECT us1.uid,us1.name,us1.username,us1.email,us1.mobilenumber,us1.isactive,DATE_FORMAT(us1.regdate,'%d/%m/%Y') AS regdate2,COUNT(us2.uid),1 FROM users us1 LEFT OUTER JOIN users us2 ON us1.uid=us2.parentid WHERE us1.parentid=" + 
      userid + "  GROUP BY us1.uid";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getTeleCallerByUid(long userid) {
    String query = "SELECT tcallerid,username,firstname,email,mobno,active,regDate,companyId,lastname,apikey from tallycaller where tcallerid=" + 
      userid;
    System.out.println("My : " + query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getSenderNamesByUId(long l) {
    String query;
    if (l == 1L) {
      query = "SELECT s.sid,s.sendername,u.username,DATE_FORMAT(s.submitdate,'%d/%m/%Y'),DATE_FORMAT(s.approvedate,'%d/%m/%Y'),s.isactive,s.service,1 FROM sendername s ,users u WHERE u.uid=s.uid ORDER BY isactive asc";
    } else {
      query = "SELECT s.sid,s.sendername,u.username,DATE_FORMAT(s.submitdate,'%d/%m/%Y'),DATE_FORMAT(s.approvedate,'%d/%m/%Y'),s.isactive,s.service,1 FROM sendername s ,users u WHERE u.uid=s.uid and u.uid=" + 
        l + " ORDER BY isactive asc";
    } 
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getTamplatesByUid(long l, boolean istrans) {
    String query;
    if (l == 0L) {
      query = "SELECT t.tid,t.tname,t.tmessage,u.username,t.isactive,1 FROM tamplate t,users u WHERE istrans=" + 
        istrans + " AND t.uid=u.uid";
    } else {
      query = "SELECT t.tid,t.tname,t.tmessage,u.username,t.isactive,1 FROM tamplate t,users u WHERE istrans=" + 
        istrans + " AND  t.uid=u.uid AND t.uid=" + l;
    } 
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void saveGroup(GroupName grouname) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(grouname);
  }
  
  public int saveContects(List<Contacts> contacts) {
    Session session = this.sessionFactory.openSession();
    Transaction tx = null;
    int failed = 0;
    int succ = 0;
    for (int i = 0; i < contacts.size(); i++) {
      try {
        tx = session.beginTransaction();
        session.save(contacts.get(i));
        tx.commit();
        succ++;
      } catch (Exception ex) {
        failed++;
        tx.rollback();
        session.close();
        session = this.sessionFactory.openSession();
      } 
    } 
    session.close();
    return failed;
  }
  
  public List getGroupNamesByUId(long userid) {
    String query = "SELECT g.gid,g.groupname,g.TYPE,DATE_FORMAT(g.DATE,'%d/%m/%Y %H:%i'),COUNT(c.gid)AS cnt,1 FROM groupname g LEFT OUTER JOIN contacts c ON g.gid=c.gid WHERE uid=" + 
      userid + " GROUP BY groupname";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public GroupName getGroupNameByGId(Long id) {
    GroupName groupName = (GroupName)this.sessionFactory.getCurrentSession().get(GroupName.class, id);
    return groupName;
  }
  
  public void updateGroup(GroupName grouname) {
    this.sessionFactory.getCurrentSession().update(grouname);
  }
  
  public void deleteUserByGroupId(Long id) {
    this.sessionFactory.getCurrentSession().createSQLQuery("delete from contacts where gid=" + id).executeUpdate();
  }
  
  public void deleteGroupByGroupId(Long id) {
    GroupName groupName = (GroupName)this.sessionFactory.getCurrentSession().get(GroupName.class, id);
    this.sessionFactory.getCurrentSession().delete(groupName);
  }
  
  public void updateSenderName(SenderName senderName) {
    this.sessionFactory.getCurrentSession().update(senderName);
  }
  
  public String getContectsByGid(int page, int listSize, long userid, String name, String email, String number, String gid) {
    String query = "SELECT * FROM contacts c,groupname g WHERE c.gid=g.gid ";
    if (!name.equalsIgnoreCase(""))
      query = String.valueOf(query) + " AND c.name='" + name + "'"; 
    if (!number.equalsIgnoreCase(""))
      query = String.valueOf(query) + " AND c.number='" + number + "'"; 
    if (!email.equalsIgnoreCase(""))
      query = String.valueOf(query) + " AND c.email='" + email + "'"; 
    query = String.valueOf(query) + " AND uid=" + userid + " AND c.gid=" + gid;
    List list = null;
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(query.replace("*", "count(*) as count"));
    BigInteger count = (BigInteger)sQLQuery.uniqueResult();
    Query quer = this.sessionFactory.getCurrentSession()
      .createSQLQuery(
        query.replace("*", "c.cid,c.email,c.gid,c.name,c.number,g.type,g.date,g.uid,g.groupname,1"))
      .setMaxResults(listSize).setFirstResult((page - 1) * listSize);
    list = quer.list();
    DatatableJsonObject personJsonObject = new DatatableJsonObject();
    personJsonObject.setRecordsFiltered(count.intValue());
    personJsonObject.setRecordsTotal(count.intValue());
    personJsonObject.setData(list);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String json2 = gson.toJson(personJsonObject);
    return json2;
  }
  
  public void deleteContactbyid(long id) {
    Contacts user = new Contacts();
    user = (Contacts)this.sessionFactory.getCurrentSession().get(Contacts.class, Long.valueOf(id));
    this.sessionFactory.getCurrentSession().delete(user);
  }
  
  public String getSmsAuditByAdmin(int page, int listSize, long uid, Date sdate, Date edate, String serviceid) {
    String q = "";
    String q2 = "";
    String q3 = "";
    List lst = null;
    if (sdate != null)
      q = String.valueOf(q) + " ma.transtime between '" + this.df.format(sdate) + "' AND '" + this.df.format(edate) + "' "; 
    if (!serviceid.isEmpty()) {
      if (!q.isEmpty())
        q = String.valueOf(q) + " AND "; 
      q = String.valueOf(q) + " ma.serviceid=" + Integer.parseInt(serviceid) + "  ";
    } 
    if (uid != 0L) {
      q2 = "AND ma.uid=" + uid;
      if (q.isEmpty()) {
        q3 = "select count(*) from messageaudit ma where ma.uid=" + uid;
      } else {
        q = " AND " + q;
        q3 = "select count(*) from messageaudit ma where ma.uid=" + uid + q;
      } 
    } else if (q.isEmpty()) {
      q3 = "select count(*) from messageaudit ma";
    } else {
      q3 = "select count(*) from messageaudit ma where " + q;
      q = "AND " + q;
    } 
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(q3);
    BigInteger count = (BigInteger)sQLQuery.uniqueResult();
    String qury = "SELECT 1,ma.auid,usr.username,usr.mobilenumber,ma.lastcridit,ma.newcridit,DATE_FORMAT(ma.lastvalidity,'%d/%m/%Y %H:%i'),DATE_FORMAT(ma.newvalidity,'%d/%m/%Y %H:%i'),DATE_FORMAT(ma.transtime,'%d/%m/%Y %H:%i'),ma.serviceid,ma.comments,ma.transtype FROM messageaudit ma,users usr WHERE usr.uid=ma.uid " + 
      q + q2 + " ORDER BY ma.transtime DESC";
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(qury).setMaxResults(listSize)
      .setFirstResult((page - 1) * listSize).list();
    DatatableJsonObject jo = new DatatableJsonObject();
    jo.setRecordsFiltered(count.intValue());
    jo.setRecordsTotal(count.intValue());
    jo.setData(lst);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    String json2 = gson.toJson(jo);
    return json2;
  }
  
  public List getDisplaySetting() {
    String query = "SELECT id,user,title,url,logoImage,homeUrl,1 FROM displaysetting";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void saveDisplaySetting(DisplaySetting displaySetting) {
    this.sessionFactory.getCurrentSession().save(displaySetting);
  }
  
  public DisplaySetting getDisplaySettingById(long parseLong) {
    return (DisplaySetting)this.sessionFactory.getCurrentSession().get(DisplaySetting.class, Long.valueOf(parseLong));
  }
  
  public void updateDisplaySetting(DisplaySetting displaySetting) {
    this.sessionFactory.getCurrentSession().update(displaySetting);
  }
  
  public void deleteDisplaySettingById(long l) {
    DisplaySetting displaySetting = new DisplaySetting();
    displaySetting = (DisplaySetting)this.sessionFactory.getCurrentSession().get(DisplaySetting.class, Long.valueOf(l));
    this.sessionFactory.getCurrentSession().delete(displaySetting);
  }
  
  public void resetPassword(String username, String password) {
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession()
      .createSQLQuery("update users set password='" + password + "' where username='" + username + "'");
    sQLQuery.executeUpdate();
  }
  
  public long saveHostSetting(HostSetting hostSetting) {
    return ((Long)this.sessionFactory.getCurrentSession().save(hostSetting)).longValue();
  }
  
  public List getHostSettings() {
    String query = "SELECT id,USER,username,PASSWORD,displayName,from1,hostName,1 FROM hostsetting";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public HostSetting getHostSetting(String user) {
    Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(HostSetting.class)
      .add((Criterion)Restrictions.eq("user", user));
    HostSetting hostSetting = (HostSetting)criteria.uniqueResult();
    return hostSetting;
  }
  
  public long updateHostSetting(HostSetting hostSetting) {
    this.sessionFactory.getCurrentSession().update(hostSetting);
    return 1L;
  }
  
  public void deleteHostSettingById(long l) {
    HostSetting hostSetting = new HostSetting();
    hostSetting = (HostSetting)this.sessionFactory.getCurrentSession().get(HostSetting.class, Long.valueOf(l));
    this.sessionFactory.getCurrentSession().delete(hostSetting);
  }
  
  public MessageTemplate getMessageTemplate(long userid, String type) {
    Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MessageTemplate.class)
      .add((Criterion)Restrictions.eq("uid", Long.valueOf(userid))).add((Criterion)Restrictions.eq("templateName", type));
    MessageTemplate template = (MessageTemplate)criteria.uniqueResult();
    return template;
  }
  
  public EmailTemplate getEmailTemplate(long userid, String type) {
    Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(EmailTemplate.class)
      .add((Criterion)Restrictions.eq("templateName", type)).add((Criterion)Restrictions.eq("uid", Long.valueOf(userid)));
    EmailTemplate template = (EmailTemplate)criteria.uniqueResult();
    return template;
  }
  
  public void deleteEmailTemplate(Long id) {
    this.sessionFactory.getCurrentSession().createSQLQuery("delete from emailtemplate where id=" + id).executeUpdate();
  }
  
  public void deleteMessageTemplate(Long id) {
    this.sessionFactory.getCurrentSession().createSQLQuery("delete from messagetemplate where id=" + id).executeUpdate();
  }
  
  public List<SmsCredit> getSmsCreditByUid(Long id) {
    List<SmsCredit> smsCredit = new ArrayList<>();
    smsCredit = this.sessionFactory.getCurrentSession().createQuery("from SmsCredit where uId=" + id).list();
    return smsCredit;
  }
  
  public int updateStatusUser(Long id, int status) {
    String hql = "UPDATE users set isactive =" + status + " WHERE uid=" + id;
    int result = 0;
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
      result = sQLQuery.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
    return result;
  }
  
  public int updateDefaultSender(Long id, int status) {
    String hql = "UPDATE users set defaultsenderid =" + status + " WHERE uid=" + id;
    int result = 0;
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
      result = sQLQuery.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
    return result;
  }
  
  public SystemNotification saveSystemNotification(SystemNotification sysnf) {
    this.sessionFactory.getCurrentSession().save(sysnf);
    return sysnf;
  }
  
  public List<EmailTemplate> getEmailTemplatebyUid(long uid) {
    List<EmailTemplate> emailTemplates = new ArrayList<>();
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession()
        .createSQLQuery("select * from emailtemplate where uid=" + uid);
      emailTemplates = sQLQuery.list();
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return emailTemplates;
  }
  
  public List<MessageTemplate> getMessageTemplatebyUid(long uid) {
    List<MessageTemplate> messageTemplates = new ArrayList<>();
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession()
        .createSQLQuery("select * from messagetemplate where uid=" + uid);
      messageTemplates = sQLQuery.list();
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return messageTemplates;
  }
  
  public Long saveMessageTemplate(MessageTemplate messageTemplate) {
    return (Long)this.sessionFactory.getCurrentSession().save(messageTemplate);
  }
  
  public Long saveEmailTemplate(EmailTemplate emailTemplate) {
    return (Long)this.sessionFactory.getCurrentSession().save(emailTemplate);
  }
  
  public MessageTemplate getMessageTemplateById(long id) {
    MessageTemplate messageTemplate = (MessageTemplate)this.sessionFactory.getCurrentSession()
      .get(MessageTemplate.class, Long.valueOf(id));
    return messageTemplate;
  }
  
  public int updateMessageTemplate(MessageTemplate messageTemplate) {
    try {
      this.sessionFactory.getCurrentSession().update(messageTemplate);
    } catch (Exception e) {
      e.printStackTrace();
      return -1;
    } 
    return 0;
  }
  
  public EmailTemplate getMessageEmailById(long id) {
    EmailTemplate emailTemplate = (EmailTemplate)this.sessionFactory.getCurrentSession().get(EmailTemplate.class, Long.valueOf(id));
    return emailTemplate;
  }
  
  public int updateEmailTemplate(EmailTemplate emailTemplate) {
    try {
      this.sessionFactory.getCurrentSession().update(emailTemplate);
    } catch (Exception e) {
      e.printStackTrace();
      return -1;
    } 
    return 0;
  }
  
  public List<User> getChildByUid(long userid) {
    Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class)
      .add((Criterion)Restrictions.eq("parentId", Long.valueOf(userid)));
    return criteria.list();
  }
  
  public GroupName getGroupByNamenUid(String gname, Long uid) {
    GroupName gna = null;
    Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(GroupName.class)
      .add((Criterion)Restrictions.eq("groupName", gname)).add((Criterion)Restrictions.eq("uid", uid));
    try {
      gna = (GroupName)criteria.uniqueResult();
    } catch (Exception exception) {}
    return gna;
  }
  
  public DisplaySetting getDisplaySettingByURL(String url) {
    return (DisplaySetting)this.sessionFactory.getCurrentSession().createCriteria(DisplaySetting.class)
      .add((Criterion)Restrictions.eq("url", url)).uniqueResult();
  }
  
  public void addtallyCaller(Tallycaller tcaller) {
    this.sessionFactory.getCurrentSession().save(tcaller);
  }
  
  public List getLeadForAssign(long userid, int companyId) {
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName,'(',categorymanager.categortName,')') AS NAME,lead.tallyCalletId FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.companyId=" + 
      companyId + " AND lead.leadState='Open' AND ( lead.tallyCalletId=0 OR lead.tallyCalletId=" + userid + 
      ")";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getTeleCallerForAdmin(long userid) {
    String query = "SELECT tcallerid,username,allocate_staff_id from tallycaller WHERE allocate_staff_id=0 OR allocate_staff_id=" + 
      userid;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void UnassignTallyCallerStaff(Long id) {
    System.out.println("Long Id :" + id);
    String query = "update tallycaller set allocate_staff_id=0 where tcallerid=" + id;
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(query);
    sQLQuery.executeUpdate();
  }
  
  public void assignTallyCallerStaff(Long id, Long TallyCaller_id) {
    String query = "update tallycaller set allocate_staff_id=" + id + " where tcallerid=" + TallyCaller_id;
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(query);
    sQLQuery.executeUpdate();
  }
  
  public List getAssignTellyCaller(Long id) {
    System.out.println("Id GET : " + id);
    String query = "SELECT tcallerid,username,firstname,lastname,mobno,email FROM tallycaller WHERE allocate_staff_id=" + 
      id;
    List s = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    System.out.println("List SIze :" + s.size());
    System.out.println("Gson Value : " + s);
    return s;
  }
  
  public List getCategoryList(Long Id) {
    String query = "SELECT * FROM categorymanager WHERE companyId=" + Id + 
      " ORDER BY categorymanager.categotyId DESC";
    System.out.println(query);
    List s = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    return s;
  }
  
  public void DeleteCategory(Long catId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM categorymanager where categotyId=" + catId)
      .executeUpdate();
  }
  
  public void UpdateCategory(CategoryManager catmanager) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(catmanager);
  }
  
  public void AddCategory(CategoryManager catmanager) {
    this.sessionFactory.getCurrentSession().save(catmanager);
  }
  
  public List GetAssignLeadState(int compId) {
    System.out.println("Cmp Id= " + compId);
    String query = "SELECT stateId,stateName FROM leadprocesstate WHERE companyId=" + compId;
    List s = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    return s;
  }
  
  public void deleteState(int ststeId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM leadprocesstate WHERE stateId=" + ststeId)
      .executeUpdate();
  }
  
  public void UpdateLeadstate(Leadprocesstate lpsd) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(lpsd);
  }
  
  public void AddState(Leadprocesstate ldst) {
    this.sessionFactory.getCurrentSession().save(ldst);
  }
  
  public List GetCategory(int CompId) {
    String query = "SELECT categotyId,categortName FROM categorymanager WHERE companyId=" + CompId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void AddLead(Lead lead) {
    this.sessionFactory.getCurrentSession().save(lead);
  }
  
  public List GetLead(int CompId, int page, int listSize) {
    System.out.println("CID " + CompId);
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND  lead.companyId=" + 
      
      CompId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).setMaxResults(listSize).setFirstResult((page - 1) * listSize).list();
  }
  
  public void deleteLead(int leadId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM lead WHERE leaadId=" + leadId).executeUpdate();
  }
  
  public List GetCompanyLead(int leadId) {
    String query = "";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetLeadCategoryName(Long id) {
    String query = "SELECT categorymanager.categortName  FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.leaadId=" + 
      id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetLeadState(Long id) {
    String query = "SELECT stateName FROM leadprocesstate WHERE companyId=" + id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void UpdateLead(int id, String fname, String lname, String email, String mobno, String cat, String sataus, String compName, String webAdd, String cont, String stat, String cty, int tcaller, String updatealtmobileNo) {
    String query = "update lead set categoryId=" + cat + ",firstName='" + fname + "',lastName='" + lname + 
      "',mobileNo='" + mobno + "',email='" + email + "',leadProcessStatus='" + sataus + "',companyName='" + 
      compName + "',website='" + webAdd + "',Country='" + cont + "',state='" + stat + "',city='" + cty + 
      "',tallyCalletId=" + tcaller + ",altmobileNo='" + updatealtmobileNo + "' where leaadId=" + id;
    System.out.println(query);
    int query1 = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public List getTellyCaller(long id) {
    System.out.println("COMP ID : : " + id);
    String query = "SELECT tcallerid,username,concat(firstname,' ',lastname),email,mobno,active,companyId,allocate_staff_id,regDate,passwrd from tallycaller WHERE companyId=" + 
      id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void UpdateTallycaller(String comastr) {
    String[] data = comastr.split(",");
    System.out.println(data[0]);
    System.out.println(data[1]);
    System.out.println(data[2]);
    System.out.println(data[3]);
    System.out.println(data[4]);
    System.out.println(data[5]);
    String query = "UPDATE tallycaller set username='" + data[1] + "',firstname='" + data[2] + "',lastname='" + 
      data[3] + "',mobno='" + data[5] + "',email='" + data[4] + "' WHERE tcallerid=" + data[0];
    System.out.println(query);
    int i = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
    System.out.println("Update Row Count is " + i);
  }
  
  public void deleteTellyCaller(int tcId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM tallycaller where tcallerid=" + tcId)
      .executeUpdate();
  }
  
  public void addTellyCaller(Tallycaller tc) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(tc);
  }
  
  public void UnassignLeadUpdate(Long LeadId) {
    int query = this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE lead set tallyCalletId=0 WHERE leaadId=" + LeadId).executeUpdate();
    System.out.println("Update Lead Count is " + query);
  }
  
  public void assignLeadUpdate(Long LeadId, Long tcId) {
    int query = this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE lead set tallyCalletId=" + tcId + " WHERE leaadId=" + LeadId).executeUpdate();
    System.out.println("Update Lead Count is " + query);
  }
  
  public void AddCSVDataField(String field, String value) {}
  
  public Tallycaller TallyCallerByName(String uname) {
    Tallycaller tcaller = new Tallycaller();
    tcaller = (Tallycaller)this.sessionFactory.getCurrentSession()
      .createQuery("FROM Tallycaller WHERE username='" + uname + "'").uniqueResult();
    return tcaller;
  }
  
  public List getCategoryLeadCount(String uname) {
    String query = "SELECT categorymanager.categortName,COUNT(lead.categoryId) AS LeadCount,categoryId FROM lead  JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.leadState='Open'  AND lead.tallyCalletId=(SELECT tcallerid FROM tallycaller WHERE username='" + 
      uname + "') GROUP BY lead.categoryId ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getCategoryAutoDialLeadCount(String uname) {
    String query = "SELECT categorymanager.categortName,COUNT(lead.categoryId) AS LeadCount,categoryId FROM lead  JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.leadState='Open' AND lead.autoDial=1  AND lead.tallyCalletId=(SELECT tcallerid FROM tallycaller WHERE username='" + 
      uname + "') GROUP BY lead.categoryId ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getCategoryLead(String tallyCallername, String catname) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tagName,lead.dialState,lead.altmobileNo FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE categorymanager.categortName='" + 
      
      catname + "' AND tallycaller.username='" + tallyCallername + 
      "'   AND leadState='Open'";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getTallyCallerLead(String tallycallerName) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tagName,lead.dialState,lead.altmobileNo FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE  tallycaller.username='" + 
      
      tallycallerName + "'  AND leadState='Open'";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public int updateSheduleDate(int leadId, String sheDate) {
    String query = "UPDATE Lead SET sheduleDate='" + sheDate + "' WHERE leaadId=" + leadId;
    System.out.println("Q::" + query);
    this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE lead SET notification_flag='TRUE'  WHERE lead.leaadId=" + leadId)
      .executeUpdate();
    int rowCount = this.sessionFactory.getCurrentSession().createQuery(query).executeUpdate();
    this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE followupshistory SET sheduleTime='" + sheDate + "' WHERE leadId=" + leadId)
      .executeUpdate();
    return rowCount;
  }
  
  public int updateLeadProcessState(int leadId, String state) {
    String query = "UPDATE Lead SET leadProcessStatus='" + state + "' WHERE leaadId=" + leadId;
    System.out.println("Q:::" + query);
    int rowCount = this.sessionFactory.getCurrentSession().createQuery(query).executeUpdate();
    return rowCount;
  }
  
  public int getfollowupsCount(int leadId) {
    Number number = (Number)this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT COUNT(followupsId) FROM followupshistory WHERE leadId=" + leadId)
      .uniqueResult();
    System.out.println("Count : " + number.intValue());
    return number.intValue();
  }
  
  public List getFollowupsHistory(int lead_id) {
    String query = null;
    query = "SELECT followupshistory.followupsId,followupshistory.callingTime,followupshistory.remark,followupshistory.sheduleTime,followupshistory.newStatus,followupshistory.callDuration,followupshistory.callStatus,followupshistory.failstatus,followupshistory.audoiFilePath,followupshistory.followsUpType,CONCAT(tallycaller.firstname,' ',tallycaller.lastname) FROM followupshistory INNER JOIN tallycaller ON followupshistory.`tallycallerId`=tallycaller.tcallerid  WHERE followupshistory.leadId=" + 
      
      lead_id + " ORDER BY callingTime DESC";
    query = "SELECT followupshistory.followupsId,followupshistory.callingTime,followupshistory.remark,followupshistory.sheduleTime,followupshistory.newStatus,followupshistory.callDuration,followupshistory.callStatus,followupshistory.failstatus,followupshistory.audoiFilePath,followupshistory.followsUpType,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.`leadProcessStatus` FROM followupshistory INNER JOIN tallycaller ON followupshistory.`tallycallerId`=tallycaller.tcallerid  INNER JOIN lead ON followupshistory.`leadId`=lead.`leaadId`WHERE followupshistory.leadId=  " + 
      
      lead_id + "   ORDER BY callingTime DESC";
    System.out.println("MY :: " + query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getLeadData(int lead_id) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,  lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.city,lead.state,lead.Country,lead.companyName,lead.website,lead.tagName,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE  lead.leaadId=" + 
      
      lead_id + "  AND leadState='Open'";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void addFollowupsHistory(String tallycalleName, String callingTime, String remark, String sheduleTime, String newStatus, String audoiFilePath, int callDuration, String callStatus, int leadId, String followupType, int compId, int second, String successStatus, String followupDevice) {
    String hql = "";
    if (second == 0) {
      hql = "INSERT INTO followupshistory(tallycallerId,companyId,callingTime,remark,sheduleTime,newStatus,audoiFilePath,callDuration,callStatus,leadId,followsUpType,followAddSecond,successStatus,followupDevice)VALUES ((SELECT tcallerid FROM tallycaller WHERE username='" + 
        tallycalleName + "')," + 
        compId + ",'" + callingTime + "','" + remark + "','" + sheduleTime + "','" + newStatus + "','" + 
        audoiFilePath + "'," + callDuration + ",'" + callStatus + "'," + leadId + ",'" + followupType + 
        "',0,'" + successStatus + "','" + followupDevice + "')";
    } else {
      hql = "INSERT INTO followupshistory(tallycallerId,companyId,callingTime,remark,sheduleTime,newStatus,audoiFilePath,callDuration,callStatus,leadId,followsUpType,followAddSecond,successStatus,followupDevice)VALUES ((SELECT tcallerid FROM tallycaller WHERE username='" + 
        tallycalleName + "')," + 
        compId + ",'" + callingTime + "','" + remark + "','" + sheduleTime + "','" + newStatus + "','" + 
        audoiFilePath + "'," + callDuration + ",'" + callStatus + "'," + leadId + ",'" + followupType + 
        "'," + Math.abs(second - 60) + ",'" + successStatus + "','" + followupDevice + "')";
    } 
    System.out.println(hql);
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
    int i = sQLQuery.executeUpdate();
    if (i != 0) {
      this.sessionFactory.getCurrentSession()
        .createSQLQuery("UPDATE lead SET lead.dialState=1 WHERE lead.leaadId=" + leadId)
        .executeUpdate();
    } else {
      System.out.println("Dail Status Not Updated.");
    } 
    System.out.println("Affected Rows Count : " + i);
  }
  
  public List getPaggingLead(String tallycaller, int limit, int pageNo) {
    int tcid = Integer.parseInt(tallycaller);
    int i = limit * pageNo;
    System.out.println("i : " + i);
    int final_PageNo = Math.abs(limit - i);
    System.out.println("Fina OFFSET : " + final_PageNo);
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.altmobileNo FROM Lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE  tallycaller.tcallerid=" + 
      
      Integer.parseInt(tallycaller) + "  AND leadState='Open' LIMIT " + 
      limit + " OFFSET " + final_PageNo;
    List lead = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    return lead;
  }
  
  public List getPaggingLeadByCategory(String userName, String catName, int limt, int pageNo) {
    int i = limt * pageNo;
    System.out.println("i : " + i);
    int final_PageNo = Math.abs(limt - i);
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.altmobileNo FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE tallycaller.tcallerid=" + 
      
      Integer.parseInt(userName) + " AND categorymanager.categortName='" + 
      catName + "'  AND leadState='Open' LIMIT " + limt + " OFFSET " + final_PageNo;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getLeadByContactNo(String contactNo) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM lead   WHERE mobileNo='" + contactNo + "'").list();
  }
  
  public int addLeadData(String userName, String firstName, String lastName, String email, String companytNam, String catName, String mobileNo, String webSite, String company, String country, String state, String city, String remark) {
    Date date = new Date();
    Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String s = formatter.format(date);
    System.out.println(s);
    String query = "INSERT INTO lead (categoryId,companyId,createDate,email,firstName,lastName,leadProcessStatus,leadState,mobileNo,sheduleDate,tallyCalletId,csvData,companyName,website,Country,state,city,leadType) VALUES ( (SELECT categotyId FROM categorymanager WHERE categotyId='" + 
      Integer.parseInt(catName) + "') , (SELECT uid FROM users WHERE username='" + userName + "') ,'" + s + 
      "','" + email + "','" + firstName + "','" + lastName + "','None','Open','" + mobileNo + 
      "','1992-04-29 10:10:10',(SELECT defaultAssignId FROM categorymanager WHERE categotyId='" + 
      Integer.parseInt(catName) + "'),'" + remark + "','" + company + "','" + webSite + "','" + country + 
      "','" + state + "','" + city + "','Bonrix Web')";
    System.out.println(query);
    int i = 0;
    i = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
    System.out.println("Affected Rows Count : " + i);
    return i;
  }
  
  public List remainLead(int tcId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT CONCAT(firstname,' ',lastname),mobileNo,sheduleDate FROM lead WHERE leadState='Open' AND tallyCalletId=" + 
        tcId + " ORDER  BY sheduleDate DESC ")
      .list();
  }
  
  public List GetContact(long uid) {
    Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Contactinfo.class)
      .add((Criterion)Restrictions.eq("companyId", Long.valueOf(uid)));
    List<Contactinfo> cingo = criteria.list();
    return cingo;
  }
  
  public String updateContactInfo(int cid, String cname, String email, long cno) {
    Query query = this.sessionFactory.getCurrentSession().createQuery(
        "UPDATE Contactinfo SET contactPersonName=:cname,contactNo=:cno,emailId=:email WHERE contactId=:cid");
    query.setInteger("cid", cid);
    query.setParameter("cname", cname);
    query.setParameter("email", email);
    query.setParameter("cno", Long.valueOf(cno));
    int i = query.executeUpdate();
    String returnst = null;
    if (i != 0) {
      returnst = "Update Successfully";
    } else {
      returnst = "Error in Update Statment";
    } 
    return returnst;
  }
  
  public String deleteContactInfo(int cid) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("DELETE FROM Contactinfo WHERE contactId=:cid");
    query.setParameter("cid", Integer.valueOf(cid));
    int i = query.executeUpdate();
    String returnst = null;
    if (i != 0) {
      returnst = "Delete Successfully";
    } else {
      returnst = "Error in Delete Statment";
    } 
    return returnst;
  }
  
  public void addContactInfo(String cname, String email, String cno, int cmpId) {
    Contactinfo cinfo = new Contactinfo();
    cinfo.setContactPersonName(cname);
    cinfo.setEmailId(email);
    cinfo.setContactNo(cno);
    cinfo.setCompanyId(cmpId);
    cinfo.setTicketId(0);
    this.sessionFactory.getCurrentSession().save(cinfo);
  }
  
  public User getUidByUserName(String username) {
    User user = new User();
    user = (User)this.sessionFactory.getCurrentSession().createQuery("FROM User WHERE username='" + username + "'")
      .uniqueResult();
    return user;
  }
  
  public List GetLeadToAddCustomer(int compId) {
    String query = "  SELECT  leaadId,CONCAT(firstname,' ',lastname),email,mobileNo,createDate  FROM lead WHERE companyId=" + 
      compId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public int compareLeadContact(String contactNo, int uid) {
    List cinfo = this.sessionFactory.getCurrentSession()
      .createSQLQuery(
        "SELECT * FROM contactinfo WHERE contactNo='" + contactNo + "' AND companyId=" + uid)
      .list();
    System.out.println("I : " + cinfo.size());
    return cinfo.size();
  }
  
  public Ticketmaster addTicket(String email, String subject, String filepath, String desc) {
    Ticketmaster ticket = new Ticketmaster();
    ticket.setCompanyId(0);
    ticket.setUserEmailId(email);
    ticket.setSubject(subject);
    ticket.setPriority(null);
    ticket.setTicketStatus("Open");
    ticket.setSource(desc);
    ticket.setTicketType(null);
    ticket.setAgentId(0);
    ticket.setDepartment(0);
    ticket.setCreatedDate(new Date());
    ticket.setDueOn(null);
    ticket.setIdDeleted(0);
    ticket.setFileName(filepath);
    this.sessionFactory.getCurrentSession().save(ticket);
    return ticket;
  }
  
  public List getDepartment(int companyId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT departmentId,departmentName FROM departmentmaster WHERE compantId=(SELECT companyId FROM aegecustdetail WHERE Id=" + 
        companyId + ")")
      .list();
  }
  
  public List getAgentByCompany(int id) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT Id,NAME FROM aegecustdetail WHERE companyId=(SELECT companyId FROM aegecustdetail WHERE Id=" + 
        id + ") AND TYPE=0")
      .list();
  }
  
  public List getAgentByCompanyId(int companyId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM agentmaster WHERE companyId='" + companyId + "'").list();
  }
  
  public List getRequestInfo(String email) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM ticketmaster WHERE userEmailId='" + email + "'").list();
  }
  
  public List getRecentHistory() {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM recentactivitymaster ORDER BY createDateTime DESC LIMIT 10").list();
  }
  
  public List GetOpenTickect() {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM ticketmaster WHERE ticketStatus='Open' AND idDeleted=0").list();
  }
  
  public List GetUnassignTickect() {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM ticketmaster WHERE agentId=0 AND idDeleted=0").list();
  }
  
  public List GetOverdueticket() {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT * FROM ticketmaster WHERE ABS(DATEDIFF(createdDate,CURDATE()))>3 AND ticketStatus='Open' AND idDeleted=0 ")
      .list();
  }
  
  public List GetTickectByTicketId(int tkid) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM recentactivitymaster WHERE ticketId=" + tkid).list();
  }
  
  public List GetTicketDetail(int tcid) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM ticketmaster WHERE ticketId=" + tcid).list();
  }
  
  public List getContactInfoByEmail(String email) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM contactinfo WHERE emailId='" + email + "'").list();
  }
  
  public List GetTicketChatHistory(int tkid) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT ticketchildmaster.ticketChileId,ticketchildmaster.folloUps, ticketchildmaster.updateOn,aegecustdetail.Name FROM ticketchildmaster INNER JOIN aegecustdetail ON ticketchildmaster.SenderId=aegecustdetail.Id WHERE ticketchildmaster.ticketId=" + 
        tkid + " ORDER BY ticketchildmaster.updateOn ASC;")
      .list();
  }
  
  public void updateTicketProprities(String status, String priority, int tickId, int departmentId, int agentId) {
    String query = "UPDATE ticketmaster set ticketStatus='" + status + "',priority='" + priority + "',departmentId=" + 
      departmentId + ",agentId=" + agentId + " WHERE ticketId=" + tickId;
    System.out.println(query);
    int i = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
    System.out.println("updateTicketProprities Row Count is " + i);
  }
  
  public List GetContactInfo(int agentId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT Email,NAME FROM aegecustdetail WHERE TYPE!=0 AND companyId=(SELECT companyId FROM aegecustdetail WHERE Id=" + 
        agentId + " )")
      .list();
  }
  
  public List GetTickectByEmailId(String emailId, String todate, String fromdate, String status) {
    String sql = null;
    System.out.println(String.valueOf(emailId) + " : " + status);
    if (emailId.equalsIgnoreCase("--ALL--") && status.equalsIgnoreCase("ALL")) {
      System.out.println("First");
      sql = "SELECT ticketId,SUBJECT,createdDate,userEmailId FROM ticketmaster WHERE  (createdDate BETWEEN '" + 
        todate + "' AND '" + fromdate + "')  AND idDeleted=0";
    } else if (emailId.equalsIgnoreCase("--ALL--") && status != "ALL") {
      System.out.println("Second");
      sql = "SELECT ticketId,SUBJECT,createdDate,userEmailId FROM ticketmaster WHERE  (createdDate BETWEEN '" + 
        todate + "' AND '" + fromdate + "') AND ticketStatus='" + status + "' AND idDeleted=0";
    } else if (status.equalsIgnoreCase("ALL") && emailId != "--ALL--") {
      System.out.println("Thinrd");
      sql = "SELECT ticketId,SUBJECT,createdDate,userEmailId FROM ticketmaster WHERE userEmailId='" + emailId + 
        "' AND (createdDate BETWEEN '" + todate + "' AND '" + fromdate + "')  AND idDeleted=0";
    } else {
      System.out.println("Else");
      sql = "SELECT ticketId,SUBJECT,createdDate,userEmailId FROM ticketmaster WHERE userEmailId='" + emailId + 
        "' AND (createdDate BETWEEN '" + todate + "' AND '" + fromdate + "') AND ticketStatus='" + status + 
        "' AND idDeleted=0";
    } 
    System.out.println(sql);
    return this.sessionFactory.getCurrentSession().createSQLQuery(sql).list();
  }
  
  public List getTicketIdByEmailId(String emailId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT ticketId FROM ticketmaster WHERE userEmailId='" + emailId + "'").list();
  }
  
  public void addTicketchildMST(int tickId, int agentId, String msg) {
    Ticketchildmaster mst = new Ticketchildmaster();
    mst.setFolloUps(msg);
    mst.setTicketId(tickId);
    mst.setUpdatedBy(agentId);
    mst.setUpdateOn(new Date());
    mst.setVisible(true);
    mst.setSenderId(agentId);
    this.sessionFactory.getCurrentSession().save(mst);
  }
  
  public Ticketmaster getTicketObjectById(int id) {
    Ticketmaster tmst = new Ticketmaster();
    tmst = (Ticketmaster)this.sessionFactory.getCurrentSession()
      .createQuery("from Ticketmaster where ticketId=" + id).uniqueResult();
    return tmst;
  }
  
  public void sendTicketWithStatus(int tkid, String status) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Ticketmaster SET ticketStatus=:tstatus WHERE ticketId=:tid ");
    query.setString("tstatus", status);
    query.setInteger("tid", tkid);
    int count = query.executeUpdate();
  }
  
  public List GetAllTickect() {
    Ticketmaster tmst = new Ticketmaster();
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("select ticketId,SUBJECT,createdDate,userEmailId from ticketmaster").list();
  }
  
  public List GetContactInfoDetail(int agentId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT contactId,contactPersonName,contactNo,emailId FROM contactinfo WHERE companyId=(SELECT companyId FROM aegecustdetail WHERE Id=" + 
        agentId + ") ")
      .list();
  }
  
  public void updateContactInfoDetail(int id, String name, String email, String cno) {
    Query query = this.sessionFactory.getCurrentSession().createQuery(
        "UPDATE Contactinfo SET contactPersonName=:cname,contactNo=:cno,emailId=:eid  WHERE contactId=:cid ");
    query.setString("cname", name);
    query.setString("cno", cno);
    query.setString("eid", email);
    query.setInteger("cid", id);
    int count = query.executeUpdate();
  }
  
  public void deleteContactInfoDetail(int cid) {
    String hql = "DELETE FROM Contactinfo WHERE id = :dcid";
    Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
    query.setParameter("dcid", Integer.valueOf(cid));
    int result = query.executeUpdate();
  }
  
  public void AddContactInfoDetail(int agentid, String name, String email, String cno) {
    AegeCustDetail agdetail = new AegeCustDetail();
    Ticketmaster tmst = new Ticketmaster();
    agdetail = (AegeCustDetail)this.sessionFactory.getCurrentSession()
      .createQuery("from AegeCustDetail where Id=" + agentid).uniqueResult();
    Contactinfo cinfo = new Contactinfo();
    cinfo.setContactPersonName(name);
    cinfo.setEmailId(email);
    cinfo.setContactNo(cno);
    cinfo.setCompanyId(agdetail.getCompanyId());
    cinfo.setTicketId(0);
    this.sessionFactory.getCurrentSession().save(cinfo);
  }
  
  public void addFullTicket(Ticketmaster tmst) {
    this.sessionFactory.getCurrentSession().save(tmst);
  }
  
  public AegeCustDetail getCompanyIdByAgentId(int agentid) {
    AegeCustDetail agdetail = new AegeCustDetail();
    agdetail = (AegeCustDetail)this.sessionFactory.getCurrentSession()
      .createQuery("from AegeCustDetail where Id=" + agentid).uniqueResult();
    return agdetail;
  }
  
  public List GetDueToday() {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT * FROM ticketmaster WHERE DATE(createdDate) = DATE(CURDATE()) AND ticketStatus='Open' AND idDeleted=0 ")
      .list();
  }
  
  public List GetStaff(int id) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT uid,username,NAME,city,email,mobilenumber,regdate FROM users where companyId=" + id)
      .list();
  }
  
  public User AddStaff(String username, String email, String name, String mob, String pass, String city, int companyId) {
    User user = new User();
    user.setUsername(username);
    user.setEmail(email);
    user.setName(name);
    user.setMobileNumber(mob);
    user.setPassword(pass);
    user.setCity(city);
    user.setCompanyId(companyId);
    user.setRegDate(new Date());
    user.setAddBy(0L);
    user.setAddress(null);
    user.setCmpName(null);
    user.setCountry(null);
    user.setDefaultSenderId(0L);
    user.setDlrReport(null);
    user.setEnabled(true);
    user.setExpireDate(null);
    user.setActive(true);
    user.setMaxUser(0L);
    user.setParentId(0L);
    user.setLimit_staff(0);
    user.setLimit_tally_caller(0);
    user.setCutting(0);
    user.setMinNumber(0);
    this.sessionFactory.getCurrentSession().save(user);
    return user;
  }
  
  public void addRole(String uname) {
    String hql = "INSERT INTO user_roles (role,uid) VALUES('ROLE_STAFF',(SELECT uid FROM users WHERE username='" + 
      uname + "')) ";
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
    int i = sQLQuery.executeUpdate();
    System.out.println("Affected Rows Count : " + i);
  }
  
  public Tallycaller GetTellyCallerByName(String tcallerName) {
    Tallycaller tcaller = new Tallycaller();
    tcaller = (Tallycaller)this.sessionFactory.getCurrentSession()
      .createQuery("from Tallycaller where username='" + tcallerName + "'").uniqueResult();
    return tcaller;
  }
  
  public Lead GetLeadObjectByLeadId(int leadId) {
    Lead lead = new Lead();
    lead = (Lead)this.sessionFactory.getCurrentSession().createQuery("from Lead WHERE leaadId=" + leadId)
      .uniqueResult();
    return lead;
  }
  
  public Tallycaller GetTellyCallerById(int tcallerId) {
    Tallycaller tcaller = new Tallycaller();
    tcaller = (Tallycaller)this.sessionFactory.getCurrentSession()
      .createQuery("from Tallycaller where tcallerid=" + tcallerId).uniqueResult();
    return tcaller;
  }
  
  public void addRegeX(String from, String subject, int category, String namepatten, String emailpatten, String mobilepatten, int tallycallerId, int companyId) {
    Regexpatten regx = new Regexpatten();
    regx.setNamePatten(namepatten);
    regx.setEmailPatten(emailpatten);
    regx.setCnoPatten(mobilepatten);
    regx.setFromEmail(from);
    regx.setEmailSubject(subject);
    regx.setCategoryId(category);
    regx.setCompanyId(companyId);
    regx.setStaffId(0);
    regx.setTallycallerId(tallycallerId);
    this.sessionFactory.getCurrentSession().save(regx);
  }
  
  public List GetTellyCallerBycompanyId(int companyId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT tcallerid,username,firstname,lastname FROM tallycaller WHERE companyId=" + companyId + " ")
      .list();
  }
  
  public List GetImapSettingByStaffId(int StaffId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM imapsetting WHERE staffid=" + StaffId + " ").list();
  }
  
  public void UpdateImapSetting(String email, String password, int staffId) {
    String hql = "UPDATE imapsetting set email ='" + email + "',PASSWORD='" + password + "' WHERE companyId=" + 
      staffId;
    int result = 0;
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
      result = sQLQuery.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
  }
  
  public void addImapSetting(String email, String password, int companyId) {
    ImapSetting isetting = new ImapSetting();
    isetting.setEmail(email);
    isetting.setPassword(password);
    isetting.setStaffid(0);
    isetting.setCompanyId(companyId);
    this.sessionFactory.getCurrentSession().save(isetting);
  }
  
  public List getIMAPSetting() {
    return this.sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM imapsetting ").list();
  }
  
  public Regexpatten getregexpatten(String from, String subject) {
    Regexpatten patten = new Regexpatten();
    patten = (Regexpatten)this.sessionFactory.getCurrentSession()
      .createQuery("FROM Regexpatten WHERE fromEmail='" + from + "' AND emailSubject='" + subject + "' ")
      .uniqueResult();
    System.out.println("demo " + patten.getNamePatten());
    return patten;
  }
  
  public List GetTemplate(int Id, String rol) {
    return this.sessionFactory.getCurrentSession().createQuery(
        "SELECT s.temp_Id,s.temp_Name,s.temp_Text,s.STATUS FROM Smstemplate as s WHERE  " + rol + "=" + Id)
      .list();
  }
  
  public void updateTemplate(int tempid, String tempname, String tempdesc) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Smstemplate SET temp_Name=:tname,temp_Text=:tdesc WHERE temp_Id=:tid");
    query.setString("tname", tempname);
    query.setString("tdesc", tempdesc);
    query.setInteger("tid", tempid);
    query.executeUpdate();
  }
  
  public void deleteTemplate(int tempid) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("DELETE FROM Smstemplate WHERE temp_Id=:tid");
    query.setInteger("tid", tempid);
    query.executeUpdate();
  }
  
  public void deactiveTemplate(int tempid) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Smstemplate SET STATUS=:st WHERE temp_Id=:tid");
    query.setParameter("st", Boolean.valueOf(true));
    query.setInteger("tid", tempid);
    query.executeUpdate();
  }
  
  public void activeTemplate(int tempid) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Smstemplate SET STATUS=:st WHERE temp_Id=:tid");
    query.setParameter("st", Boolean.valueOf(false));
    query.setInteger("tid", tempid);
    query.executeUpdate();
  }
  
  public UserRole getUserRole(int uid) {
    UserRole role = new UserRole();
    role = (UserRole)this.sessionFactory.getCurrentSession().createQuery("from UserRole WHERE uid=" + uid)
      .uniqueResult();
    return role;
  }
  
  public void addTemplate(Smstemplate temp) {
    this.sessionFactory.getCurrentSession().save(temp);
  }
  
  public List getTemplateAPI(Tallycaller tcaler) {
    return this.sessionFactory.getCurrentSession().createQuery("SELECT s.temp_Id,s.temp_Name,s.temp_Text,s.STATUS FROM Smstemplate AS s WHERE STATUS=1 AND s.comp_Id=" + tcaler.getCompanyId())
      .list();
  }
  
  public List getTemplateByTemplateId(String id) {
    return this.sessionFactory.getCurrentSession().createQuery("SELECT ss.temp_Id,ss.temp_Name, ss.temp_Text FROM Smstemplate as ss WHERE ss.STATUS=1  AND ss.temp_Id IN(" + id + ") ").list();
  }
  
  public void saveLog(Sentsmslog log) {
    System.out.println(log.toString());
    this.sessionFactory.getCurrentSession().save(log);
  }
  
  public List GetAPI(int id) {
    return this.sessionFactory.getCurrentSession()
      .createQuery("SELECT s.url_Id,s.url,s.responce,s.provider,s.API_Status FROM Smssettings AS s WHERE comp_Id =" + id + " AND API_Status=1").list();
  }
  
  public List GetAllAPI(int id) {
    return this.sessionFactory.getCurrentSession()
      .createQuery(
        "SELECT s.url_Id,s.url,s.responce,s.provider,s.API_Status FROM Smssettings AS s WHERE comp_Id =" + 
        id)
      .list();
  }
  
  public List<Tallycaller> getTallycallerById(int Id) {
    Tallycaller tcaller = new Tallycaller();
    return (List<Tallycaller>)this.sessionFactory.getCurrentSession()
      .createQuery("FROM Tallycaller WHERE tcallerid=" + Id).uniqueResult();
  }
  
  public List getLeadProcessState(int compId) {
    return this.sessionFactory.getCurrentSession()
      .createQuery("SELECT s.stateId,s.stateName FROM Leadprocesstate as s WHERE companyId=" + compId)
      .list();
  }
  
  public void addAPI(Smssettings setings) {
    this.sessionFactory.getCurrentSession().save(setings);
  }
  
  public Smssettings getSMSSettings(int id) {
    return (Smssettings)this.sessionFactory.getCurrentSession().createQuery("FROM Smssettings WHERE comp_Id=" + id)
      .list();
  }
  
  public void updateAPI(int id, String key, String provider, String desc) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Smssettings SET responce=:key,provider=:provider,url=:desc WHERE url_Id=:id");
    query.setInteger("id", id);
    query.setString("key", key);
    query.setString("desc", desc);
    query.setString("provider", provider);
    query.executeUpdate();
  }
  
  public void deleteAPI(int id) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("DELETE FROM Smssettings WHERE url_Id=:id");
    query.setInteger("id", id);
    query.executeUpdate();
  }
  
  public void activeAPI(int APIid, int Id) {
    this.sessionFactory.getCurrentSession()
      .createSQLQuery(
        "UPDATE Smssettings SET API_Status = IF(url_Id=" + APIid + ", 1,0) WHERE comp_Id=" + Id)
      .executeUpdate();
  }
  
  public List GetContactInfoDetailSMS(int uid) {
    System.out.println(
        "SELECT contactId,contactPersonName,contactNo,emailId FROM contactinfo WHERE companyId=" + uid);
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT contactId,contactPersonName,contactNo,emailId FROM contactinfo WHERE companyId=" + uid + " ")
      .list();
  }
  
  public List getLeadDataSMS(int id, int catId, String lpstste, String lstate) {
    String query = "";
    System.out.println(String.valueOf(lpstste) + "   " + lstate + " " + catId);
    if (id != 0 && catId != 0 && !lpstste.equals("Process State") && !lstate.equals("Lead State")) {
      System.out.println("All is called.....");
      query = "SELECT  lead.leaadId, lead.firstName,lead.lastName,lead.mobileNo,lead.leadState,lead.leadProcessStatus, categorymanager.categortName ,lead.email FROM lead INNER JOIN categorymanager ON  categorymanager.categotyId=lead.categoryId WHERE staffId IN(SELECT uid FROM users WHERE companyId=" + 
        id + " ) AND categorymanager.categotyId=lead.categoryId AND lead.leadProcessStatus='" + lpstste + 
        "' AND lead.leadState='" + lstate + "' AND categorymanager.categotyId=" + catId;
    } else if (id != 0 && catId != 0 && !lpstste.equals("Process State")) {
      System.out.println("cat and Lead process stste is called.....");
      query = "SELECT  lead.leaadId, lead.firstName,lead.lastName,lead.mobileNo,lead.leadState,lead.leadProcessStatus, categorymanager.categortName ,lead.email FROM lead INNER JOIN categorymanager ON  categorymanager.categotyId=lead.categoryId WHERE staffId IN(SELECT uid FROM users WHERE companyId=" + 
        id + " ) AND categorymanager.categotyId=lead.categoryId AND lead.leadProcessStatus='" + lpstste + 
        "'  AND categorymanager.categotyId=" + catId;
    } else if (id != 0 && catId != 0 && !lstate.equals("Lead State")) {
      System.out.println("cat and Lead stste is called.....");
      query = "SELECT  lead.leaadId, lead.firstName,lead.lastName,lead.mobileNo,lead.leadState,lead.leadProcessStatus, categorymanager.categortName ,lead.email FROM lead INNER JOIN categorymanager ON  categorymanager.categotyId=lead.categoryId WHERE staffId IN(SELECT uid FROM users WHERE companyId=" + 
        id + " ) AND categorymanager.categotyId=lead.categoryId  AND lead.leadState='" + lstate + 
        "' AND categorymanager.categotyId=" + catId;
    } else if (id != 0 && !lpstste.equals("Process State") && !lstate.equals("Lead State")) {
      System.out.println("Prodess st and lead state  is called.....");
      query = "SELECT  lead.leaadId, lead.firstName,lead.lastName,lead.mobileNo,lead.leadState,lead.leadProcessStatus, categorymanager.categortName,lead.email  FROM lead INNER JOIN categorymanager ON  categorymanager.categotyId=lead.categoryId WHERE staffId IN(SELECT uid FROM users WHERE companyId=" + 
        id + " ) AND categorymanager.categotyId=lead.categoryId AND lead.leadProcessStatus='" + lpstste + 
        "' AND lead.leadState='" + lstate + "'";
    } else if (id != 0 && catId != 0 && lpstste.equals("Process State") && lstate.equals("Lead State")) {
      System.out.println("Cat is called.....");
      query = "SELECT  lead.leaadId, lead.firstName,lead.lastName,lead.mobileNo,lead.leadState,lead.leadProcessStatus, categorymanager.categortName ,lead.email FROM lead INNER JOIN categorymanager ON  categorymanager.categotyId=lead.categoryId WHERE staffId IN(SELECT uid FROM users WHERE companyId=" + 
        id + " ) AND categorymanager.categotyId=" + catId + " ";
    } else if (id != 0 && catId == 0 && !lpstste.equals("Process State") && lstate.equals("Lead State")) {
      System.out.println("Lead process stste is called.....");
      query = "SELECT  lead.leaadId, lead.firstName,lead.lastName,lead.mobileNo,lead.leadState,lead.leadProcessStatus, categorymanager.categortName ,lead.email FROM lead INNER JOIN categorymanager ON  categorymanager.categotyId=lead.categoryId WHERE staffId IN(SELECT uid FROM users WHERE companyId=" + 
        id + " ) AND  lead.leadProcessStatus='" + lpstste + "' ";
    } else if (id != 0 && catId == 0 && lpstste.equals("Process State") && !lstate.equals("Lead State")) {
      System.out.println("Lead stste is called.....");
      query = "SELECT  lead.leaadId, lead.firstName,lead.lastName,lead.mobileNo,lead.leadState,lead.leadProcessStatus, categorymanager.categortName ,lead.email FROM lead INNER JOIN categorymanager ON  categorymanager.categotyId=lead.categoryId WHERE staffId IN(SELECT uid FROM users WHERE companyId=" + 
        id + " )  AND lead.leadState='" + lstate + "' ";
    } 
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void CloseLead(int leadId) {
    System.out.println("Lead Id : " + leadId);
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Lead SET leadState='Close' WHERE leaadId=:id");
    query.setInteger("id", leadId);
    query.executeUpdate();
  }
  
  public List getSentSMSLog(int id, String startDate, String endDate, String role, int mobNo) {
    System.out.println("Company Id : " + id);
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT id,mobileNo,message,sendDate,MSGStatus FROM sentsmslog WHERE compayId=" + id + 
        " OR staffId IN(SELECT uid FROM users WHERE companyId=" + id + 
        " ) OR tallycallerId IN(SELECT tcallerid FROM tallycaller WHERE companyId=" + id + " )");
    return sQLQuery.list();
  }
  
  public List getTodaySentSMSLog(int id, String stDate, String endDate, String rol) {
    Query query = null;
    System.out.println("SELECT id,mobileNo,message,sendDate,MSGStatus FROM sentsmslog WHERE (sendDate BETWEEN '" + 
        stDate + "' AND '" + endDate + "') AND compayId=" + id + " ");
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT id,mobileNo,message,sendDate,MSGStatus FROM sentsmslog WHERE (sendDate BETWEEN '" + stDate + 
        "' AND '" + endDate + "') AND compayId=" + id + " ORDER BY id DESC");
    return sQLQuery.list();
  }
  
  public List GetCloseStatusAPI(int tcId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT callStatus.cstatusName FROM callStatus INNER JOIN users ON callStatus.compId=users.uid WHERE callStatus.compId=(SELECT tallycaller.companyId FROM tallycaller WHERE tallycaller.tcallerid=" + 
        tcId + " )")
      .list();
  }
  
  public void AddFailFollowUpAPI(Followupshistory history) {
    this.sessionFactory.getCurrentSession().save(history);
  }
  
  public List GetSuccessStatusAPI(int tcId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT successstatus.statusName FROM successstatus INNER JOIN users ON successstatus.compId=users.uid WHERE successstatus.compId=(SELECT tallycaller.companyId FROM tallycaller WHERE tallycaller.tcallerid=" + 
        tcId + " );")
      .list();
  }
  
  public void UpdateStaff(int sid, String username, String email, String name, String mob, String password, String city, int companyId) {
    System.out.println("NAME : " + name);
    Query query = this.sessionFactory.getCurrentSession().createQuery(
        "UPDATE User SET username=:usr,email=:eml,name=:nam,mobilenumber=:mno, city=:cty WHERE uid=:id");
    query.setInteger("id", sid);
    query.setString("usr", username);
    query.setString("eml", email);
    query.setString("nam", name);
    query.setString("mno", mob);
    query.setString("cty", city);
    query.executeUpdate();
  }
  
  public void DeleteStaff(int sid) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("DELETE FROM User WHERE uid=:id ");
    query.setInteger("id", sid);
    int count = query.executeUpdate();
  }
  
  public List getCRMEmailTemplate(int finalId, String rol) {
    System.out.println(
        "SELECT temp_Id,temp_Name,temp_Text,emailSubject FROM crmemailtemplate WHERE comp_Id=" + finalId + " ");
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT temp_Id,temp_Name,temp_Text,emailSubject FROM crmemailtemplate WHERE comp_Id=" + finalId)
      .list();
  }
  
  public void AddLeadToAutoDial(int lid, String status) {
    String q = "";
    if (status.equals("0")) {
      q = "UPDATE Lead SET autoDial=0 WHERE leaadId=:id";
    } else if (status.equals("1")) {
      q = "UPDATE Lead SET autoDial=1 WHERE leaadId=:id";
    } 
    System.out.println("SAJAN BONRIX :: " + q);
    Query query = this.sessionFactory.getCurrentSession().createQuery(q);
    query.setInteger("id", lid);
    query.executeUpdate();
  }
  
  public List GetPaggingAutoDialLead(int tallycaller, int limit, int pageNo) {
    int i = limit * pageNo;
    System.out.println("i : " + i);
    int final_PageNo = Math.abs(limit - i);
    System.out.println("Fina OFFSET : " + final_PageNo);
    String queery = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE tallycaller.tcallerid=" + 
      tallycaller + " AND autoDial=1 AND lead.leadState='Open' ORDER BY sheduleDate ASC LIMIT " + limit + 
      " OFFSET " + final_PageNo + " ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(queery).list();
  }
  
  public String GetAllAutoDialLead(int tcid, int page, int listSize) {
    String qry = "SELECT count(*) from lead    WHERE  lead.tallyCalletId=" + tcid + 
      "  AND autoDial=1 AND lead.leadState='Open'";
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(qry);
    BigInteger count = (BigInteger)sQLQuery.uniqueResult();
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,lead.leadState,categorymanager.categortName,lead.leadProcessStatus,lead.createDate,lead.email, lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE  tallycaller.tcallerid=" + 
      
      tcid + 
      "  AND autoDial=1 AND lead.leadState='Open' ORDER BY sheduleDate ASC";
    System.out.println(query);
    List lst = this.sessionFactory.getCurrentSession().createSQLQuery(query).setMaxResults(listSize)
      .setFirstResult((page - 1) * listSize).list();
    DatatableJsonObject pjo = new DatatableJsonObject();
    pjo.setRecordsFiltered(count.intValue());
    pjo.setRecordsTotal(count.intValue());
    pjo.setData(lst);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(pjo).toString();
  }
  
  public List GetAllAutoDialLead(int tcid) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tagName,lead.dialState,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE  tallycaller.tcallerid=" + 
      
      tcid + 
      "  AND autoDial=1 AND lead.leadState='Open' ORDER BY sheduleDate ASC";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void AddAllLeadToAutoDial(int tcid, String status) {
    String q = "";
    if (status.equals("0")) {
      q = "UPDATE Lead SET autoDial=0 WHERE tallyCalletId=:id";
    } else if (status.equals("1")) {
      q = "UPDATE Lead SET autoDial=1 WHERE tallyCalletId=:id";
    } 
    Query query = this.sessionFactory.getCurrentSession().createQuery(q);
    query.setInteger("id", tcid);
    query.executeUpdate();
  }
  
  public void updateEmailTemplateDetail(int tid, String tsubject, String tText, String tempName) {
    String query = "UPDATE crmemailtemplate SET crmemailtemplate.temp_Name='" + tempName + 
      "',crmemailtemplate.temp_Text='" + tText + "',crmemailtemplate.emailSubject='" + tsubject + "'" + 
      " WHERE crmemailtemplate.temp_Id=" + tid;
    System.out.println(query);
    this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public void addContactInfoByRol(String cname, String email, long cno, int companyId, String roll) {
    Contactinfo cinfo = new Contactinfo();
    cinfo.setContactPersonName(cname);
    cinfo.setEmailId(email);
    cinfo.setContactNo(Long.toString(cno));
    if (roll.equalsIgnoreCase("staffId")) {
      cinfo.setStaffId(companyId);
      cinfo.setCompanyId(0L);
    } else {
      cinfo.setCompanyId(companyId);
      cinfo.setStaffId(0);
    } 
    cinfo.setTicketId(0);
    this.sessionFactory.getCurrentSession().save(cinfo);
  }
  
  public List GetLeadByLeadIdAPI(int leadId) {
    String queery = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tagName,lead.leadcomment,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId    WHERE lead.leaadId=" + 
      leadId + "  ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(queery).list();
  }
  
  public List GetLeadParameter(int company_id) {
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName,lead.createDate,lead.sheduleDate,lead.leadProcessStatus,lead.csvData,CONCAT(lead.companyName,'(',lead.website,')') AS companyWeb,CONCAT(lead.Country,',',lead.state,',',lead.city ) AS location FROM lead INNER JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.leadState='Open' AND lead.companyId=" + 
      company_id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetTelecaller(int id) {
    String query = "SELECT tcallerid,CONCAT(firstname,' ',lastname) AS Tname FROM tallycaller WHERE companyId=" + id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetLeadByCategoryId(String query) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void AssignLeadToTelecaller(int leadId, int tcid) {
    System.out.println(
        "UPDATE lead SET tallyCalletId = " + tcid + ", assignDate= NOW() WHERE leaadId = " + leadId);
    this.sessionFactory.getCurrentSession().createSQLQuery(
        "UPDATE lead SET tallyCalletId = " + tcid + ", assignDate= NOW() WHERE leaadId = " + leadId)
      .executeUpdate();
  }
  
  public ApiKey ValidateAPI(String api) {
    ApiKey apival = new ApiKey();
    apival = (ApiKey)this.sessionFactory.getCurrentSession().createQuery("from ApiKey WHERE keyValue='" + api + "'")
      .uniqueResult();
    return apival;
  }
  
  public List GetContactDetaails(long uid, String mobile) {
    String query = "SELECT CONCAT(lead.firstName,' ',lead.lastName) AS FullName,categorymanager.categortName AS Category FROM lead  INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE mobileNo='" + 
      
      mobile + "' AND lead.companyId=" + uid;
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List ValidateLead(String mobile, String categoty, long tcId) {
    String queery = "SELECT leaadId FROM lead WHERE mobileNo='" + mobile + "' AND categoryId='" + categoty + 
      "' AND companyId=(SELECT companyId FROM tallycaller WHERE tcallerid=" + tcId + ")";
    return this.sessionFactory.getCurrentSession().createSQLQuery(queery).list();
  }
  
  public List getAllLeadOfContactNo(String mobileNo, String telecallerId) {
    String querry = "SELECT lead.leaadId, CONCAT(lead.firstName,' ',lead.lastName) AS FullName,lead.mobileNo,lead.leadState,lead.email,categorymanager.categortName AS Category,lead.sheduleDate,lead.autoDial,lead.leadProcessStatus,lead.firstName,lead.lastName,lead.companyName,lead.website,lead.Country,lead.city,lead.state,lead.categoryId FROM lead  INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE mobileNo='" + 
      
      mobileNo + 
      "' AND lead.companyId=(SELECT companyId FROM tallycaller WHERE tcallerid=" + 
      Integer.parseInt(telecallerId) + ")";
    System.out.println(querry);
    return this.sessionFactory.getCurrentSession().createSQLQuery(querry).list();
  }
  
  public List getLeadIdAndCategoryName(String mobno, int CompanyId) {
    String querry = "select lead.leaadId,categorymanager.categortName from lead inner join categorymanager on lead.categoryId=categorymanager.categotyId where lead.mobileNo='" + 
      mobno + "' AND lead.companyId=" + CompanyId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(querry).list();
  }
  
  public List GetLeadDetailByMobileNoAPI(String parseInt) {
    String mobNo = parseInt;
    String queery = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId    WHERE lead.mobileNo='" + 
      parseInt + "' ";
    System.out.println(queery);
    return this.sessionFactory.getCurrentSession().createSQLQuery(queery).list();
  }
  
  public List getCategoryAPI(int catId) {
    return null;
  }
  
  public List GetImapSettingByCompanyId(int companyId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM imapsetting WHERE companyId=" + companyId + " ").list();
  }
  
  public List getTallyCallerByCompanyId(int companyId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM tallycaller WHERE companyId=" + companyId + " ").list();
  }
  
  public List GetSuccessCallLog(int userId) {
    String query = "SELECT followupshistory.followupsId,DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i %p'),followupshistory.newStatus,followupshistory.callStatus,followupshistory.callDuration,followupshistory.audoiFilePath,followupshistory.followsUpType,lead.mobileNo,CONCAT(tallycaller.firstname,' ',tallycaller.lastname) FROM followupshistory  INNER JOIN lead ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId WHERE followupshistory.callStatus='Success' AND followupshistory.companyId=" + 
      
      userId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetInOutBoundSuccessCallLog(int userId, String callType, String stDate, int catId, int tcId) {
    String query = "SELECT followupshistory.followupsId,lead.mobileNo,DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i %p'), followupshistory.callDuration,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),followupshistory.callStatus, followupshistory.followsUpType,categorymanager.categortName,followupshistory.audoiFilePath FROM followupshistory  INNER JOIN lead  ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId  INNER JOIN categorymanager ON categorymanager.categotyId=lead.categoryId WHERE followupshistory.callStatus='Success' AND followupshistory.followsUpType='" + 
      
      callType + 
      "' AND followupshistory.companyId=" + userId + " " + " AND DATE(followupshistory.callingTime)='" + 
      stDate + "' AND lead.categoryId=" + catId + " AND tallycaller.tcallerid=" + tcId + "  ";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetDateRangeSuccessCallLog(int userId, String stdate, String endate) {
    String query = "SELECT followupshistory.followupsId,DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i %p'),followupshistory.newStatus,followupshistory.callStatus,followupshistory.callDuration,followupshistory.audoiFilePath,followupshistory.followsUpType,lead.mobileNo,CONCAT(tallycaller.firstname,' ',tallycaller.lastname) FROM followupshistory  INNER JOIN lead ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId WHERE followupshistory.callStatus='Success'  AND followupshistory.companyId=" + 
      
      userId + " AND (followupshistory.callingTime BETWEEN '" + stdate + "' AND ADDDATE('" + endate + 
      "', INTERVAL 1 DAY))";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetFailCallLog(int userId) {
    String query = "SELECT followupshistory.followupsId,DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i %p'),followupshistory.failstatus,followupshistory.callStatus,followupshistory.callDuration,followupshistory.audoiFilePath,followupshistory.followsUpType,lead.mobileNo,CONCAT(tallycaller.firstname,' ',tallycaller.lastname) FROM followupshistory  INNER JOIN lead ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId WHERE followupshistory.callStatus='Fail' AND followupshistory.companyId=" + 
      
      userId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetInOutBoundFailCallLog(int userId, String callType, String startDt, int catId, int tcId) {
    String query = "SELECT followupshistory.followupsId,lead.mobileNo,DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i %p'), followupshistory.callDuration,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),followupshistory.callStatus, followupshistory.followsUpType,categorymanager.categortName FROM followupshistory  INNER JOIN lead  ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId  INNER JOIN categorymanager ON categorymanager.categotyId=lead.categoryId WHERE followupshistory.callStatus='Fail' AND followupshistory.followsUpType='" + 
      
      callType + 
      "' AND followupshistory.companyId=" + userId + " AND tallycaller.tcallerid=" + tcId + " " + 
      " AND DATE(followupshistory.callingTime)='" + startDt + "' AND lead.categoryId=" + catId + "  ";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetDateRangeFailCallLog(int userId, String stdate, String endate) {
    String query = "SELECT followupshistory.followupsId,DATE_FORMAT(followupshistory.callingTime,'%b %d %Y %h:%i %p'),followupshistory.failstatus,followupshistory.callStatus,followupshistory.callDuration,followupshistory.audoiFilePath,followupshistory.followsUpType,lead.mobileNo,CONCAT(tallycaller.firstname,' ',tallycaller.lastname) FROM followupshistory  INNER JOIN lead ON followupshistory.leadId=lead.leaadId  INNER JOIN tallycaller ON tallycaller.tcallerid=followupshistory.tallycallerId WHERE followupshistory.callStatus='Fail'  AND followupshistory.companyId=" + 
      
      userId + " AND (followupshistory.callingTime BETWEEN '" + stdate + "' AND '" + endate + "')";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getAllTimeCallLog(int userId) {
    String query = "SELECT  CONCAT(tallycaller.firstname,' ',tallycaller.lastname),COUNT(CASE WHEN followupshistory.callStatus = 'Fail' THEN 1 ELSE NULL END) AS FailCount,  COUNT(CASE WHEN followupshistory.callStatus = 'Success' THEN 1 ELSE NULL END) AS SuccessCount, COUNT(followupshistory.tallycallerId) AS TotalCallCount, SUM(followupshistory.callDuration) AS TotalCallDuration,followupshistory.tallycallerId  FROM followupshistory INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid WHERE   followupshistory.companyId=" + 
      
      userId + " GROUP BY tallycaller.tcallerid";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getStatistic(String statisticType, int userId) {
    String query = "SELECT DISTINCT followupshistory.newStatus,  COUNT(followupshistory.newStatus) AS statecount FROM followupshistory  WHERE followupshistory.callStatus='" + 
      statisticType + "' AND followupshistory.tallycallerId=" + 
      userId + " GROUP BY followupshistory.newStatus";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getTodayTimeCallLog(int userId) {
    String query = "SELECT  CONCAT(tallycaller.firstname,' ',tallycaller.lastname),COUNT(CASE WHEN followupshistory.callStatus = 'Fail' THEN 1 ELSE NULL END) AS FailCount,  COUNT(CASE WHEN followupshistory.callStatus = 'Success' THEN 1 ELSE NULL END) AS SuccessCount, COUNT(followupshistory.tallycallerId) AS TotalCallCount, SUM(followupshistory.callDuration) AS TotalCallDuration,followupshistory.tallycallerId  FROM followupshistory INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid WHERE   followupshistory.companyId=" + 
      
      userId + 
      " AND DATE_FORMAT(callingTime,'%Y-%m-%d')=CURDATE() GROUP BY tallycaller.tcallerid";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getOpenColseLeadCountByTelecaller(int userId) {
    String query = "SELECT tallycaller.tcallerid,COUNT(IF(lead.leadState = 'Open',1,NULL))  AS opn, COUNT(IF(lead.leadState = 'Close',1,NULL))  AS cls , CONCAT(tallycaller.firstname,' ',tallycaller.lastname) AS tcname FROM lead INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid WHERE lead.companyId=" + 
      
      userId + 
      "  GROUP BY lead.tallyCalletId";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getOpenCloseLeadCountByCategory(int userId) {
    String query = "SELECT COUNT(IF(lead.leadState = 'Open',1,NULL))  AS opn, COUNT(IF(lead.leadState = 'Close',1,NULL))  AS cls , categorymanager.categortName FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  WHERE  lead.companyId=" + 
      
      userId + 
      "  GROUP BY categorymanager.categotyId";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void AddcategoryLeadToAutoDial(int catId, int autoDialVal) {
    String query = "UPDATE lead SET lead.autoDial=" + autoDialVal + " WHERE lead.categoryId='" + catId + "'";
    this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public int AddTelecallrLeadToAutoDial(int tcId, String status) {
    String query = "UPDATE lead SET lead.autoDial=" + Integer.parseInt(status) + " WHERE lead.tallyCalletId=" + tcId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public String DefaultLeadAssignToTelecaller(int catId, int tcId) {
    String query = "UPDATE categorymanager SET categorymanager.defaultAssignId=" + tcId + 
      " WHERE categorymanager.categotyId=" + catId;
    int i = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
    return "Done";
  }
  
  public List GeneraetDashboard(String leadProcessStatus, int userId) {
    String query = "SELECT  ( SELECT COUNT(*) FROM   lead WHERE lead.leadState='Open' AND  lead.companyId=" + userId + 
      ") AS totalLead, " + 
      " ( SELECT COUNT(*) FROM   lead WHERE lead.leadState='Open' AND DATE(lead.createDate)=CURDATE() AND lead.companyId=" + 
      userId + ") AS todayLead, " + 
      " ( SELECT COUNT(*) FROM   lead WHERE lead.leadState='Open' AND DATE(lead.sheduleDate)=CURDATE() AND lead.companyId=" + 
      userId + ") AS sheduletodayLead," + 
      " ( SELECT COUNT(*) FROM   lead WHERE lead.leadState='Close' AND lead.companyId=" + userId + 
      ") AS closeLead," + " ( SELECT COUNT(*) FROM   lead WHERE  lead.companyId=" + userId + 
      ") AS totalLead1, " + 
      " ( SELECT COUNT(*) FROM   followupshistory WHERE followupshistory.callStatus='Success' AND followupshistory.companyId=" + 
      userId + ") AS successCall";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetCategoryLeadCount(int userId) {
    String query = "SELECT categorymanager.categortName,COUNT(lead.categoryId) AS LeadCount,categoryId FROM lead JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE  lead.leadState='Open' AND   lead.`companyId`=" + 
      
      userId + " GROUP BY lead.categoryId ";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void AddEmailTemplateDetail(int userId, String tempName, String subject, String text) {
    String query = "INSERT INTO crmemailtemplate (temp_Name,temp_Text,emailSubject,STATUS,comp_Id,staff_Id) VALUES (?,?,?,?,?,0)";
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(query);
    sQLQuery.setString(0, tempName);
    sQLQuery.setString(1, text);
    sQLQuery.setString(2, subject);
    sQLQuery.setBoolean(3, true);
    sQLQuery.setInteger(4, userId);
    sQLQuery.executeUpdate();
  }
  
  public Crmemailtemplate getTemplateInfo(int tempId) {
    return (Crmemailtemplate)this.sessionFactory.getCurrentSession().get(Crmemailtemplate.class, Integer.valueOf(tempId));
  }
  
  public List getEmailTemplateAPI(int compId) {
    String query = "SELECT crmemailtemplate.temp_Id,crmemailtemplate.temp_Name,crmemailtemplate.temp_Text,crmemailtemplate.emailSubject FROM crmemailtemplate WHERE crmemailtemplate.comp_Id=" + 
      compId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public Crmemailsetting getCrmEmailSetting(int companyId) {
    Crmemailsetting emailSetting = new Crmemailsetting();
    emailSetting = (Crmemailsetting)this.sessionFactory.getCurrentSession()
      .createQuery("From Crmemailsetting WHERE companyId=" + companyId + " AND active_temp=1 ")
      .uniqueResult();
    return emailSetting;
  }
  
  public Smstemplate getSmsTemplateByCompIdNdCatId(int compId, int catId) {
    Smstemplate smstemp = new Smstemplate();
    smstemp = (Smstemplate)this.sessionFactory.getCurrentSession()
      .createQuery("From Smstemplate WHERE temp_Id=" + compId + " ").uniqueResult();
    return smstemp;
  }
  
  public List getCategoryById(int catId) {
    String query = "SELECT * FROM categorymanager WHERE categotyId=" + catId;
    System.out.println(query);
    List s = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    return s;
  }
  
  public List getMessageTemplateByCompanyId(int userId) {
    String query = "SELECT * FROM smstemplate WHERE comp_Id=" + userId;
    List s = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    return s;
  }
  
  public String AssignDefaultMsgTemp(int catId, int tempId) {
    String query = "UPDATE categorymanager SET categorymanager.defaulttempId=" + tempId + 
      " WHERE categorymanager.categotyId=" + catId;
    System.out.println(query);
    int i = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
    return "Done";
  }
  
  public List getEmailTemplateByCompanyId(int userId) {
    String query = "SELECT * FROM crmemailtemplate WHERE comp_Id=" + userId;
    List s = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    return s;
  }
  
  public String AssignDefaultEmailTemp(int catId, int tempId) {
    String query = "UPDATE categorymanager SET categorymanager.defaulttEmailpId=" + tempId + 
      " WHERE categorymanager.categotyId=" + catId;
    System.out.println(query);
    int i = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
    return "Done";
  }
  
  public List GetLeadByAdvanceSearch(String sb) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(sb).list();
  }
  
  public List GetSuccessStatus(long id) {
    String sql = "SELECT * FROM successstatus WHERE successstatus.compId=" + id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(sql).list();
  }
  
  public void UpdateSuccessState(int ststeId, String stateName, int leadStatId) {
    String query = "UPDATE successstatus SET statusName='" + stateName + "',leadStateId='" + leadStatId + 
      "' WHERE id=" + ststeId;
    this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public void deleteSuccessStae(Long statusId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM successstatus where id=" + statusId)
      .executeUpdate();
  }
  
  public void AddSuccessState(String stateName, int CompId, int leadStateId) {
    String query = "INSERT INTO successstatus(statusName,compId,leadStateId) VALUES ('" + stateName + "'," + CompId + 
      "," + leadStateId + ")";
    this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public void DeleteContact(long contactId) {
    Contactinfo user = new Contactinfo();
    user = (Contactinfo)this.sessionFactory.getCurrentSession().get(Contactinfo.class, Long.valueOf(contactId));
    this.sessionFactory.getCurrentSession().delete(user);
  }
  
  public List GetLeadParameterForSearch(String query) {
    System.out.println("query::" + query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getLeadForCSV(int compId, String catId, String mobileNo) {
    List list = this.sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM lead WHERE lead.mobileNo='" + 
        mobileNo + "' AND lead.categoryId=" + Integer.parseInt(catId) + " ").list();
    return list;
  }
  
  public List getTelecallerStateLeadCount(String query) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public User GetUserData(int company_id) {
    String query = "FROM User U WHERE U.uid=" + company_id;
    User user = (User)this.sessionFactory.getCurrentSession().createQuery(query).uniqueResult();
    return user;
  }
  
  public void updateMasterPassword(String query) {
    this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public List getDueTodayLead(int tcId, String stDate, String enDate) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState   FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE (lead.sheduleDate BETWEEN '" + 
      
      stDate + "' AND '" + enDate + "') AND lead.leadState='Open' AND lead.tallyCalletId=" + 
      tcId;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void AddTelecallerStartWork(int tcId) {
    System.out.println("AddTelecallerStartWork DAO ");
    Telecaller_work twork = new Telecaller_work();
    twork.setStart_Time(new Date());
    twork.setEnd_Time(null);
    twork.setTelecaller_Id(tcId);
    this.sessionFactory.getCurrentSession().save(twork);
  }
  
  public void AddTelecallerBreakTime(int tcId) {
    Telecaller_break tBwork = new Telecaller_break();
    tBwork.setStart_Break(new Date());
    tBwork.setEnd_Break(null);
    tBwork.setTelecaller_Id(tcId);
    this.sessionFactory.getCurrentSession().save(tBwork);
  }
  
  public void AddTelecallerEndWork(int tcId) {
    List<String> dataquery = this.sessionFactory.getCurrentSession()
      .createSQLQuery(
        "SELECT  telecaller_work.WorkId FROM telecaller_work WHERE telecaller_work.Telecaller_Id=" + 
        tcId + " ORDER BY telecaller_work.WorkId DESC  LIMIT 1 ")
      .list();
    if (dataquery.size() != 0) {
      System.out.println("Work Id : " + dataquery.get(0));
      Query query = this.sessionFactory.getCurrentSession()
        .createQuery("UPDATE Telecaller_work SET End_Time=? WHERE Telecaller_Id=? AND WorkId=?");
      query.setParameter(0, new Date());
      query.setParameter(1, Integer.valueOf(tcId));
      query.setParameter(2, dataquery.get(0));
      int i = query.executeUpdate();
    } 
  }
  
  public void AddTelecallerEndBreakTime(int tcId) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Telecaller_break SET End_Break=? WHERE Telecaller_Id=?");
    query.setParameter(0, new Date());
    query.setParameter(1, Integer.valueOf(tcId));
    int res = query.executeUpdate();
  }
  
  public List GetAutoDialLead(int tcid) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,lead.mobileNo,  lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid     WHERE  tallycaller.tcallerid=" + 
      
      tcid + 
      "  AND autoDial=1 AND lead.leadState='Open' ORDER BY lead.leaadId LIMIT 1";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getTelecallerFollowupReport(int companyId, String startDate, String endDate) {
    String query = "SELECT tallycaller.username,AVG(CASE WHEN followupshistory.callStatus='Success' THEN followupshistory.followAddSecond ELSE 0 END) AS successAVG ,AVG(CASE WHEN followupshistory.callStatus='Fail' THEN followupshistory.followAddSecond ELSE 0 END) AS failAVG, COUNT(CASE WHEN followupshistory.followAddSecond=0 THEN 1 END) AS OverAccessTime FROM tallycaller INNER JOIN followupshistory WHERE  tallycaller.tcallerid= followupshistory.tallycallerId   AND tallycaller.companyId=" + 
      
      companyId + " AND (followupshistory.`callingTime` BETWEEN '" + startDate + "' AND ADDDATE('" + endDate + 
      "', INTERVAL 1 DAY)) GROUP BY tallycaller.username";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public Tallycaller validateDesktopAPI(String apikey) {
    Tallycaller user = new Tallycaller();
    user = (Tallycaller)this.sessionFactory.getCurrentSession()
      .createQuery("from Tallycaller WHERE apiKey='" + apikey + "'").uniqueResult();
    return user;
  }
  
  public List getLeadByCompanyIdNDmobileNo(String mobNo, int companyId) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,  lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  WHERE lead.companyId=" + 
      
      companyId + " AND lead.mobileNo='" + mobNo + "'";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public Tallycaller GetTelecallerByMobileNo(String mobNo) {
    Tallycaller telecaller = new Tallycaller();
    telecaller = (Tallycaller)this.sessionFactory.getCurrentSession()
      .createQuery("from Tallycaller where mobno=" + mobNo).uniqueResult();
    return telecaller;
  }
  
  public String ConvertDataToWebJSON(String queryCOunt, String query, int page, int listSize) {
    SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(queryCOunt);
    BigInteger count = (BigInteger)sQLQuery.uniqueResult();
    List lst = this.sessionFactory.getCurrentSession().createSQLQuery(query).setMaxResults(listSize).setFirstResult((page - 1) * listSize).list();
    DatatableJsonObject pjo = new DatatableJsonObject();
    pjo.setRecordsFiltered(count.intValue());
    pjo.setRecordsTotal(lst.size());
    pjo.setData(lst);
    Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
    return gson.toJson(pjo).toString();
  }
  
  public List GetTelecallerWorkDetails(int companyId, String getDate) {
    String query = "SELECT 1,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),telecaller_work.Start_Time,telecaller_work.End_Time,  TIME_FORMAT(TIMEDIFF(telecaller_work.End_Time,telecaller_work.Start_Time),'%H:%i:%s') AS  timediffIN FROM telecaller_work  INNER JOIN tallycaller ON telecaller_work.Telecaller_Id=tallycaller.tcallerid WHERE telecaller_work.Telecaller_Id=" + 
      
      companyId + " AND DATE(telecaller_work.Start_Time)='" + getDate + 
      "' AND telecaller_work.Start_Time IS NOT NULL AND telecaller_work.End_Time IS NOT NULL";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public Tallycaller validateTelecallerDesktopAPI(String api) {
    Tallycaller telecaller = new Tallycaller();
    telecaller = (Tallycaller)this.sessionFactory.getCurrentSession()
      .createQuery("from Tallycaller WHERE apikey='" + api + "'").uniqueResult();
    return telecaller;
  }
  
  public List GetTelecallerDateWiseDetails(int compid, int tcId, String getDate) {
    String query = "SELECT  tallycaller.username AS telecaller,  COUNT(followupshistory.tallycallerId) AS totalCall,   SUM(IF (followupshistory.callStatus='Success',1,0)) AS successCount,   SUM(IF (followupshistory.callStatus='Fail',1,0)) AS failCount,     SUM(followupshistory.callDuration) AS totalcallDuration     FROM followupshistory INNER JOIN tallycaller ON  followupshistory.tallycallerId= tallycaller.tcallerid   WHERE followupshistory.companyId=" + 
      
      compid + " AND followupshistory.tallycallerId=" + tcId + 
      " AND DATE(followupshistory.callingTime)='" + getDate + "'";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void assignIVRFOllowup(long leadId, long tcId) {
    System.out.println("Update assignIVRFOllowup");
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Followupshistory SET tallycallerId=? WHERE leadId=?");
    query.setParameter(0, Integer.valueOf((int)tcId));
    query.setParameter(1, Integer.valueOf((int)leadId));
    int res = query.executeUpdate();
  }
  
  public List GetIVRWorkDetails(int tcId, String getDate) {
    String query = "SELECT 1,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.mobileNo,followupshistory.callingTime, followupshistory.callDuration,followupshistory.audoiFilePath FROM followupshistory INNER JOIN tallycaller ON followupshistory.tallycallerId=tallycaller.tcallerid INNER JOIN lead ON followupshistory.leadId=lead.leaadId WHERE followupshistory.tallycallerId=" + 
      
      tcId + 
      " AND lead.leadType='IVRWEB' AND followupshistory.callStatus='Success' AND DATE(followupshistory.callingTime)='" + 
      getDate + "'";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void updateLeadComment(String comment, int id) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("UPDATE Lead SET leadcomment=? WHERE leaadId=? ");
    query.setParameter(0, comment);
    query.setParameter(1, Integer.valueOf(id));
    int res = query.executeUpdate();
  }
  
  public List getTag(int leadId) {
    String query = "SELECT leadtag.tagId,leadtag.tagName FROM leadtag INNER JOIN tagroll ON leadtag.tagId=tagroll.tagId WHERE tagroll.leadId=" + 
      leadId + " ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getAllTag(int companyId) {
    String query = "SELECT leadtag.tagId,leadtag.tagName FROM leadtag WHERE leadtag.companyId=" + companyId + "  ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void addTag(int companyId, String tagName) {
    String query = "INSERT INTO leadtag  (tagName,companyId) VALUE ('" + tagName + "'," + companyId + ") ";
    this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public void addTagToLead(int leadId, String tagList) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("UPDATE Lead SET tagName=? WHERE leaadId=? ");
    query.setParameter(0, tagList);
    query.setParameter(1, Integer.valueOf(leadId));
    int res = query.executeUpdate();
  }
  
  public void updateTagToCompany(int tagId, String tagName) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("UPDATE Leadtag SET tagName=? WHERE tagId=? ");
    query.setParameter(0, tagName);
    query.setParameter(1, Integer.valueOf(tagId));
    int res = query.executeUpdate();
  }
  
  public void deleteTagToCompany(int tagId) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("DELETE FROM Leadtag WHERE tagId=:id ");
    query.setInteger("id", tagId);
    int count = query.executeUpdate();
  }
  
  public void updateNotificationFlag(int leadId) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Lead SET notification_flag='FALSE' WHERE leaadId=? ");
    query.setParameter(0, Integer.valueOf(leadId));
    int res = query.executeUpdate();
  }
  
  public void addNotification(int tcallerid, String html_body) {
    try {
      System.out.println("INSERT INTO notification  (DATETIME,notification,telecallerId) VALUES ('" + 
          this.df1.format(new Date()) + "','" + html_body + "'," + tcallerid + ")");
      int i = this.sessionFactory.getCurrentSession()
        .createSQLQuery("INSERT INTO notification  (DATETIME,notification,telecallerId) VALUES ('" + 
          this.df1.format(new Date()) + "','" + html_body + "'," + tcallerid + ")")
        .executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    } 
  }
  
  public List getNotification(int tcallerid) {
    String query = "SELECT notification.dateTime,notification.notification,notification.id FROM notification WHERE notification.telecallerId=" + 
      tcallerid + " AND notification.status='FALSE'";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void UpdateNotificationSatus(int notiId) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Notification SET status='TRUE' WHERE id=? ");
    query.setParameter(0, Integer.valueOf(notiId));
    int res = query.executeUpdate();
  }
  
  public void saveEmailLog(String filesname, int leadId, String temp_Text, String sub, int tallycallerId, String Status, int CompanyId, String mail_error) {
    try {
      System.out.println(
          "INSERT INTO emaillog  (attached_file,lead_id,mail_body,send_date,status,subject,telecallet_id,company_id,mail_error) VALUES ('" + 
          filesname + "'," + leadId + ",'" + temp_Text + "'," + new Date() + ",'" + Status + "','" + 
          sub + "'," + tallycallerId + "," + CompanyId + ",'" + mail_error + "')");
      int i = this.sessionFactory.getCurrentSession().createSQLQuery(
          "INSERT INTO emaillog  (attached_file,lead_id,mail_body,send_date,status,subject,telecallet_id,company_id,mail_error) VALUES ('" + 
          filesname + "'," + leadId + ",'" + temp_Text + "','" + this.df1.format(new Date()) + "','" + 
          Status + "','" + sub + "'," + tallycallerId + "," + CompanyId + ",'" + mail_error + "')")
        .executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    } 
  }
  
  public void saveMobileCallLog(MobileCallLog log) {
    this.sessionFactory.getCurrentSession().save(log);
  }
  
  public int getLeadCountByMobileNo(String mobileNo, int compantId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery("SELECT COUNT(*) FROM lead WHERE lead.mobileNo='" + 
        mobileNo + "' AND lead.companyId=" + compantId + " ").list().size();
  }
  
  public String SendCRMMail(Crmemailsetting emailSetting, String sendto, String sub, String tallycallerId, String leadId, String filesname, Crmemailtemplate temp, String filePath) {
    SendCRMMail mail = new SendCRMMail();
    return null;
  }
  
  public void deleteCRMEmailTemplate(long id) {
    this.sessionFactory.getCurrentSession().createSQLQuery("delete from crmemailtemplate where temp_Id=" + id)
      .executeUpdate();
  }
  
  public List getHostSettings(int id) {
    return null;
  }
  
  public List getCRMHostSettings(int id) {
    String query = "SELECT crmemailsetting.id,crmemailsetting.emailId,crmemailsetting.passwd,crmemailsetting.hostName FROM crmemailsetting WHERE crmemailsetting.companyId=" + 
      id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void updateCRMHostSetting(String hostName, String emailId, String password, int id) {
    this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE crmemailsetting SET crmemailsetting.hostName='" + hostName + 
        "',crmemailsetting.emailId='" + emailId + "',crmemailsetting.passwd='" + password + 
        "' WHERE crmemailsetting.id=" + id)
      .executeUpdate();
  }
  
  public void deleteCRMHostSetting(int id) {
    this.sessionFactory.getCurrentSession()
      .createSQLQuery("DELETE FROM  crmemailsetting WHERE crmemailsetting.id=" + id).executeUpdate();
  }
  
  public void addCRMHostSetting(Crmemailsetting setting) {
    this.sessionFactory.getCurrentSession().save(setting);
  }
  
  public int validateNoCategory(String str, int id) {
    Number number = (Number)this.sessionFactory.getCurrentSession()
      .createSQLQuery(
        "SELECT COUNT(*) FROM lead WHERE lead.mobileNo='" + str + "' AND lead.categoryId=" + id)
      .uniqueResult();
    System.out
      .println("SELECT COUNT(*) FROM lead WHERE lead.mobileNo='" + str + "' AND lead.categoryId=" + id);
    return number.intValue();
  }
  
  public List geLeadByMobileNumber(int userid, String mobileNo) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,categorymanager.categortName,lead.email FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.mobileNo='" + 
        mobileNo + "' AND lead.companyId=" + userid)
      .list();
  }
  
  public List geLeadByMobileNumberAPI(int tcallerid, String mobileNo) {
    System.out.println(
        "SELECT CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,categorymanager.categortName,lead.email FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.mobileNo='" + 
        mobileNo + "' AND lead.tallyCalletId=" + tcallerid);
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,categorymanager.categortName,lead.email FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE lead.mobileNo='" + 
        mobileNo + "' AND lead.tallyCalletId=" + tcallerid)
      .list();
  }
  
  public void updateCSVdata(int leadId, String csv) {
    Query query = this.sessionFactory.getCurrentSession().createQuery("UPDATE Lead SET csvData=? WHERE leaadId=? ");
    query.setParameter(0, csv);
    query.setParameter(1, Integer.valueOf(leadId));
    int res = query.executeUpdate();
  }
  
  public List GetMonthlyLeadCount(int userId) {
    String query = "SELECT MONTHNAME(lead.createDate), COUNT(lead.leaadId)  FROM  lead    WHERE lead.companyId=" + 
      userId + " GROUP BY MONTH(lead.createDate) ORDER BY lead.createDate";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public Tallycaller getTallycallerByIdMdel(int teleId) {
    Tallycaller caller = new Tallycaller();
    caller = (Tallycaller)this.sessionFactory.getCurrentSession()
      .createQuery("from Tallycaller where tcallerid=" + teleId).uniqueResult();
    return caller;
  }
  
  public User getComapnyByTeclId(int companyId) {
    User user = new User();
    user = (User)this.sessionFactory.getCurrentSession().createQuery("from User where uid=" + companyId)
      .uniqueResult();
    return user;
  }
  
  public List GetMonthlySuccessFailLogCount(int userId) {
    String query = "SELECT MONTHNAME(followupshistory.callingTime), SUM(IF (followupshistory.callStatus='Success',1,0)) AS successCount,  SUM(IF (followupshistory.callStatus='Fail',1,0)) AS failCount,MONTH(followupshistory.callingTime) AS monthNum  FROM followupshistory WHERE followupshistory.companyId=" + 
      
      userId + 
      " GROUP BY MONTHNAME(followupshistory.callingTime) ORDER BY MONTHNAME(followupshistory.callingTime)";
    System.out.println(query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetDailySuccessFailLogCount(int userId) {
    String query = "SELECT DAY(followupshistory.callingTime), SUM(IF (followupshistory.callStatus='Success',1,0)) AS successCount,  SUM(IF (followupshistory.callStatus='Fail',1,0)) AS failCount,DAY(followupshistory.callingTime) AS dayNum  FROM followupshistory WHERE followupshistory.companyId=" + 
      
      userId + 
      " AND MONTH(followupshistory.callingTime)=MONTH(CURRENT_DATE()) GROUP BY DAY(followupshistory.callingTime) ORDER BY DAY(followupshistory.callingTime)";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public Followupshistory getFollowupsHistoryByFID(int fId) {
    Followupshistory followupshistory = new Followupshistory();
    followupshistory = (Followupshistory)this.sessionFactory.getCurrentSession()
      .createQuery("from Followupshistory where followupsId=" + fId).uniqueResult();
    return followupshistory;
  }
  
  public int updateFollowUpByAudioFile(int fId, String fileName, int callDuration) {
    int i = this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE followupshistory SET followupshistory.audoiFilePath='" + fileName + 
        "' , followupshistory.callDuration=" + callDuration + 
        ",callStatus='Success' WHERE followupshistory.followupsId=" + fId)
      .executeUpdate();
    return i;
  }
  
  public List GetCountByLeadState(int userId) {
    String query = "SELECT leadprocesstate.stateName,COUNT(*) FROM lead INNER JOIN leadprocesstate ON lead.leadProcessStatus=leadprocesstate.stateName  WHERE lead.companyId=" + 
      userId + " GROUP BY lead.leadProcessStatus ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetCountBySuccessState(int userId) {
    String query = "SELECT successstatus.statusName,COUNT(*) FROM followupshistory INNER JOIN successstatus ON followupshistory.successStatus=successstatus.statusName WHERE followupshistory.companyId=" + 
      userId + " GROUP BY followupshistory.successStatus ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetCountByFailState(int userId) {
    String query = "SELECT followupshistory.failstatus, COUNT(*) FROM followupshistory  WHERE followupshistory.companyId=" + 
      userId + " GROUP BY followupshistory.failstatus ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetTelecallerCallCount(int userId) {
    String query = "SELECT tallycaller.username,COUNT(CASE WHEN followupshistory.callStatus='Success' THEN 1 END) AS successCall, COUNT(CASE WHEN followupshistory.callStatus='Fail' THEN 1 END) AS FaillCall FROM followupshistory INNER JOIN tallycaller ON  followupshistory.tallycallerId= tallycaller.tcallerid  WHERE followupshistory.companyId=" + 
      
      userId + " GROUP BY tallycaller.tcallerid";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public String updateCallStatus(int parseInt, String string) {
    int i = this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE followupshistory SET followupshistory.callStatus='" + string + 
        "' WHERE followupshistory.followupsId=" + parseInt)
      .executeUpdate();
    return "Upate Call Status :: " + i;
  }
  
  public Followupshistory findById(int id) {
    Followupshistory user = new Followupshistory();
    user = (Followupshistory)this.sessionFactory.getCurrentSession()
      .createQuery("from Followupshistory where followupsId=" + id).uniqueResult();
    return user;
  }
  
  public Followupshistory GetCallLog(int id, int start_date, int end_date, int call_satus) {
    return null;
  }
  
  public void AddLeadToAhtoDialByCompanyId(int companyId) {
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("UPDATE Lead SET autoDial=1 WHERE companyId=? AND tallyCalletId=0 ");
    query.setParameter(0, Integer.valueOf(companyId));
    int res = query.executeUpdate();
  }
  
  public List<Lead> getLeadsByCompanyId(int compId) {
    List<Lead> lead = new ArrayList<>();
    lead = this.sessionFactory.getCurrentSession()
      .createQuery("from Lead where companyId=" + compId + " AND  leadState='Open'").list();
    return lead;
  }
  
  public List<Lead> getUnassignLeads(int companyId) {
    List<Lead> lead = new ArrayList<>();
    Query q = this.sessionFactory.getCurrentSession().createQuery("from Lead where companyId=" + companyId + 
        " and tallyCalletId=0 AND leadState='Open' AND autoDial=1 ORDER BY createDate DESC");
    q.setMaxResults(1);
    lead = q.list();
    return lead;
  }
  
  public List<Lead> getElapseSheduleLeads(int companyId) {
    System.out.println("Hello Sajan....");
    List<Lead> lead = new ArrayList<>();
    System.out.println(
        "from Lead WHERE sheduleDate < NOW() AND sheduleDate <> '1980-06-30 17:36:00'  AND leadState='Open' AND tallyCalletId=" + 
        companyId + " AND autoDial=1");
    Query q = this.sessionFactory.getCurrentSession().createQuery(
        "from Lead WHERE sheduleDate < NOW() AND sheduleDate <> '1980-06-30 17:36:00'  AND leadState='Open' AND tallyCalletId=" + 
        companyId + " AND autoDial=1");
    q.setMaxResults(1);
    lead = q.list();
    return lead;
  }
  
  public int addRemailSheduleLeadsToAutoDial(int tcallerid) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "UPDATE lead SET lead.autoDial=1 WHERE  sheduleDate < NOW() AND sheduleDate <> '1980-06-30 17:36:00' AND lead.tallyCalletId=" + 
        tcallerid + " AND leadState='Open'")
      .executeUpdate();
  }
  
  public List<Lead> GetMyLead(int tcallerid) {
    List<Lead> lead = new ArrayList<>();
    Query q = this.sessionFactory.getCurrentSession().createQuery("from Lead where tallyCalletId=" + tcallerid + 
        " AND leadState='Open' AND autoDial=1 ORDER BY createDate DESC");
    q.setMaxResults(1);
    lead = q.list();
    return lead;
  }
  
  public void AddRemoveLeadToAutoDialByLeadId(int leadId, int i) {
    this.sessionFactory.getCurrentSession()
      .createSQLQuery("UPDATE lead SET lead.autoDial=" + i + " WHERE lead.leaadId=" + leadId + " ")
      .executeUpdate();
  }
  
  public int AddUassignLeadToAutoDial(int companyId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery("UPDATE lead SET lead.autoDial=1 WHERE lead.companyId=" + 
        companyId + " AND  lead.tallyCalletId=0 AND leadState='Open'").executeUpdate();
  }
  
  public List<Android_mode> GetConstant() {
    List<Android_mode> mode = new ArrayList<>();
    mode = this.sessionFactory.getCurrentSession().createQuery("from Android_mode").list();
    return mode;
  }
  
  public List<Lead> GetScheduleTodayLead(int tcallerid) {
    List<Lead> lead = new ArrayList<>();
    lead = this.sessionFactory.getCurrentSession()
      .createQuery("FROM  Lead WHERE  tallyCalletId=" + tcallerid + " AND  DATE(sheduleDate)=CURDATE()")
      .setMaxResults(1).list();
    return lead;
  }
  
  public List<Lead> GetTodayLead(int tcallerid) {
    List<Lead> lead = new ArrayList<>();
    lead = this.sessionFactory.getCurrentSession()
      .createQuery("FROM Lead WHERE DATE(createDate)=CURDATE()  AND leadState='Open' AND tallyCalletId=" + 
        tcallerid + " AND autoDial=1")
      .setMaxResults(1).list();
    return lead;
  }
  
  public void addAllMyLeadsToAutoDial(int tcallerid) {
    this.sessionFactory.getCurrentSession().createSQLQuery(
        "UPDATE lead SET lead.autoDial=1 WHERE  lead.tallyCalletId=" + tcallerid + " AND leadState='Open'")
      .executeUpdate();
  }
  
  public int addScheduleTodayLeadsToAutoDial(int tcallerid) {
    System.out.println();
    Number number = 
      
      Integer.valueOf(this.sessionFactory.getCurrentSession().createSQLQuery("UPDATE lead SET autoDial=1 WHERE tallyCalletId=" + tcallerid + " AND DATE(sheduleDate)=CURDATE() AND leadState='Open'").executeUpdate());
    return number.intValue();
  }
  
  public void addAllTodayLeadsLeadsToAutoDial(int tcallerid) {
    this.sessionFactory.getCurrentSession().createSQLQuery("UPDATE lead SET autoDial=1 WHERE tallyCalletId=" + tcallerid + 
        " AND DATE(createDate)=CURDATE() AND leadState='Open'").executeUpdate();
  }
  
  public void addAllTodayAssignLeadsLeadsToAutoDial(int tcallerid) {
    this.sessionFactory.getCurrentSession().createSQLQuery("UPDATE lead SET autoDial=1 WHERE tallyCalletId=" + tcallerid + 
        " AND DATE(assignDate)=CURDATE() AND leadState='Open'").executeUpdate();
  }
  
  public List<Lead> GetTodayAssignLead(int tcallerid) {
    List<Lead> lead = new ArrayList<>();
    lead = this.sessionFactory.getCurrentSession()
      .createQuery("FROM Lead WHERE DATE(assignDate)=CURDATE()  AND leadState='Open' AND tallyCalletId=" + 
        tcallerid + " AND autoDial=1")
      .setMaxResults(1).list();
    return lead;
  }
  
  public List GetDailyLeadCount(int tallycallerId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT DATE(lead.createDate) AS ctDate,COUNT(*) AS ctCount FROM lead WHERE lead.tallyCalletId=" + 
        tallycallerId + 
        " AND lead.leadState='Open' GROUP BY DATE(lead.createDate) ORDER BY lead.createDate DESC ")
      .setResultTransformer((ResultTransformer)new AliasToBeanResultTransformer(DateWiseLeadCount.class)).list();
  }
  
  public int UpdateDemoQuery(String query) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public List<Androidbuttonflag> GetAndroidButtonConstant() {
    List<Androidbuttonflag> mode = new ArrayList<>();
    mode = this.sessionFactory.getCurrentSession().createQuery("from Androidbuttonflag").list();
    return mode;
  }
  
  public List<Lead> getAutoDialLeadObject(int tcallerid) {
    List<Lead> lead = new ArrayList<>();
    lead = this.sessionFactory.getCurrentSession()
      .createQuery("FROM Lead WHERE leadState='Open' AND tallyCalletId=" + tcallerid + " AND autoDial=1")
      .setMaxResults(1).list();
    return lead;
  }
  
  public List excuteListQuery(String query) {
    LeadAliasToBean beab = new LeadAliasToBean();
    List lead = this.sessionFactory.getCurrentSession().createSQLQuery(query)
      .setResultTransformer((ResultTransformer)new AliasToBeanResultTransformer(LeadAliasToBean.class)).list();
    return lead;
  }
  
  public int addOrRemoveLeadsFromAutoDial(String tallycallerId, int status) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery(
        "UPDATE lead SET lead.autoDial=" + status + " WHERE lead.tallyCalletId=" + tallycallerId)
      .executeUpdate();
  }
  
  public void sstest() {
    List<Integer> ids = Arrays.asList(new Integer[] { Integer.valueOf(438), Integer.valueOf(3) });
    Query query = this.sessionFactory.getCurrentSession()
      .createQuery("FROM Lead item WHERE item.tallyCalletId IN (:ids)");
    query.setParameterList("ids", ids);
    List<Lead> items = query.list();
    System.out.println(items.size());
    items.forEach(lead -> System.out.println(lead.getMobileNo()));
  }
  
  public List<GetScheduleWiseLead> getScheduleWiseLead(int tcallerid) {
    List<GetScheduleWiseLead> lead = this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT  lead.lastName,lead.website,lead.leadProcessStatus,lead.city,lead.autoDial,lead.companyName,lead.leadState,lead.mobileNo,categorymanager.categortName as categoryName,lead.firstName, lead.sheduleDate,lead.csvData,lead.leadType,lead.leaadId,lead.Country,lead.state,lead.tagName as tag,lead.email,lead.createDate,lead.dialState,lead.altmobileNo FROM lead INNER JOIN  categorymanager ON lead.categoryId=categorymanager.categotyId  WHERE lead.tallyCalletId=" + 
        
        tcallerid + 
        " AND lead.sheduleDate <> '1980-06-30 17:36:00' AND lead.leadState='Open' ORDER BY lead.sheduleDate ASC")
      
      .setResultTransformer(Transformers.aliasToBean(GetScheduleWiseLead.class)).list();
    return lead;
  }
  
  public List getGetScheduleLeadScheduler(int cmpId, int interval) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT lead.leaadId,CONCAT(lead.firstName,' ',lead.lastName),lead.mobileNo,tallycaller.mobno ,lead.email,categorymanager.categortName,lead.leadProcessStatus, DATE_FORMAT(lead.sheduleDate, '%Y-%m-%d %T') FROM lead INNER JOIN categorymanager ON lead.categoryId = categorymanager.categotyId  INNER JOIN tallycaller  ON lead.tallyCalletId = tallycaller.tcallerid WHERE 0 = 0  AND lead.companyId=" + 
        
        cmpId + "  AND lead.leadState ='Open' " + 
        " AND DATE_FORMAT(lead.sheduleDate,'%Y-%m-%d %h:%i')=DATE_FORMAT(DATE_SUB(NOW(), INTERVAL " + 
        interval + " MINUTE) ,'%Y-%m-%d %h:%i')")
      .list();
  }
  
  public List<LeadDTO> getFininceScheduleWiseLead(int tcId) {
    List<LeadDTO> lead = this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT  lead.lastName,lead.website,lead.leadProcessStatus,lead.city,lead.autoDial,lead.companyName,lead.leadState,lead.mobileNo,categorymanager.categortName AS categoryName,lead.firstName,\t\t\t\t lead.sheduleDate,lead.csvData,lead.leadType,lead.leaadId,lead.Country,lead.state,lead.tagName AS tag,lead.email,lead.createDate,tallycaller.tcallerid,tallycaller.username\t\t\t\t FROM lead INNER JOIN  categorymanager ON lead.categoryId=categorymanager.categotyId\t\t\t\t INNER JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid \t\t\t\tWHERE lead.companyId=" + 
        
        tcId + 
        " AND lead.leadState='Open' AND DATE(lead.sheduleDate)=CURDATE()")
      .setResultTransformer(Transformers.aliasToBean(LeadDTO.class)).list();
    return lead;
  }
  
  public List<Lead> incredibleservicesLeads(int companyId) {
    List<Lead> lead = new ArrayList<>();
    lead = this.sessionFactory.getCurrentSession()
      .createQuery("FROM Lead WHERE leadState='Open' AND companyId=" + companyId + " AND autoDial=1")
      .setMaxResults(1).list();
    return lead;
  }
  
  public void addAllLeadsToAutoDial(int companyId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("UPDATE lead SET lead.autoDial=0 WHERE lead.tallyCalletId=" + 
        companyId + " AND DATE(lead.assignDate)=CURDATE()").executeUpdate();
  }
  
  public AutoDialLog GetAutoDial(int responseId) {
    return (AutoDialLog)this.sessionFactory.getCurrentSession().get(AutoDialLog.class, Integer.valueOf(responseId));
  }
  
  public CategoryManager getCategoryByNaame(String categoryName, int uid) {
    CategoryManager category = (CategoryManager)this.sessionFactory.getCurrentSession()
      .createQuery("from CategoryManager where categortName='" + categoryName + "' AND companyId=" + uid)
      .uniqueResult();
    return category;
  }
  
  public List GetMissCallLead(int company_id) {
    String query = "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName , lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country, lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead  LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId   LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  WHERE lead.leadState='Open' AND lead.leadType='MISSCALL' AND  lead.companyId=" + 
      
      company_id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List GetCatAutoDialLead(int parseInt, int cat) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,lead.mobileNo,  lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid     WHERE  tallycaller.tcallerid=" + 
      
      parseInt + "  AND autoDial=1 AND lead.categoryId=" + cat + 
      " AND lead.leadState='Open' ORDER BY lead.leaadId LIMIT 1";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List<String> getUsersBySchedule() {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT * FROM lead WHERE sheduleDate BETWEEN  CURRENT_TIMESTAMP  AND ADDTIME(CURRENT_TIMESTAMP, \"0:02:0.000000\") ")
      .list();
  }
  
  public List<String> gettocken(String userid) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT fcmid FROM tallycaller where tcallerid IN (" + userid + ")").list();
  }
  
  public List<Object[]> gettelecallerID() {
    String time = "0:15:0.000000";
    String query = "SELECT * FROM lead WHERE sheduleDate BETWEEN  CURRENT_TIMESTAMP  AND ADDTIME(CURRENT_TIMESTAMP, '" + 
      time + "') ";
    System.out.println("query::" + query);
    List<Object[]> lst = null;
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    System.out.println("Ellapse_Telecaller_lst:::" + lst.toString() + "Size::" + lst.size());
    if (lst.size() == 0)
      return null; 
    return lst;
  }
  
  public int updatefcmid(String fcmid, String username) {
    List lst = null;
    String qury12 = "";
    qury12 = "SELECT fcmid FROM tallycaller WHERE username='" + username + "' ";
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(qury12).list();
    System.out.println("Data_lst00:::" + lst.toString() + "Size::" + lst.size());
    int result = 0;
    if (lst.size() == 1) {
      String hql2 = "UPDATE tallycaller SET fcmid  ='" + fcmid + "' WHERE username='" + username + "' ";
      try {
        SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql2);
        result = sQLQuery.executeUpdate();
      } catch (Exception ex) {
        ex.printStackTrace();
      } 
    } else {
      System.out.println("Please Insert FcmId:::");
    } 
    return result;
  }
  
  public List<Object[]> getEllapsetelecallerID() {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Timestamp timestamp = new Timestamp((new Date()).getTime());
    Calendar cal1 = Calendar.getInstance();
    cal1.set(13, 0);
    cal1.add(12, -15);
    timestamp = new Timestamp(cal1.getTime().getTime());
    System.out.println("15 Mins Back::" + timestamp);
    String reportDate = df.format(timestamp);
    System.out.println("reportDate::" + reportDate);
    String query = "SELECT * FROM lead WHERE sheduleDate BETWEEN  '" + reportDate + "'  AND CURRENT_TIMESTAMP  ";
    System.out.println("query::" + query);
    List<Object[]> lst = null;
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    System.out.println("Ellapse_Telecaller_lst:::" + lst.toString() + "Size::" + lst.size());
    if (lst.size() == 0)
      return null; 
    return lst;
  }
  
  public List getLeadSuccessStatus(int compId) {
    return this.sessionFactory.getCurrentSession()
      .createQuery("SELECT s.id,s.statusName FROM Successstatus as s WHERE compId=" + compId).list();
  }
  
  public List SuccessStatusSearch(String query) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public Object saveObject(Object lead) {
    return this.sessionFactory.getCurrentSession().save(lead);
  }
  
  public List<Object[]> getTelecallerCount(int companyId) {
    String query = "SELECT COUNT(*),username FROM  tallycaller WHERE companyId=" + companyId + " ";
    System.out.println("getTelecallerCount-->>" + query);
    List<Object[]> lst = null;
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    System.out.println("getTelecallerCount:::" + lst.toString() + "Size::" + lst.size());
    return lst;
  }
  
  public List<Object[]> getUserLimit(Long uid) {
    String query = "SELECT limit_tally_caller,username FROM  users WHERE uid=" + uid + " ";
    System.out.println("getTelecallerCount-->>" + query);
    List<Object[]> lst = null;
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    System.out.println("getTelecallerCount:::" + lst.toString() + "Size::" + lst.size());
    return lst;
  }
  
  public String getSMSTemplate(long compID, String temp_Name) {
	  List lst = null;
    String qury = "";
    qury = "SELECT temp_Text FROM smstemplate where comp_Id= " + compID + " AND temp_Name='" + temp_Name + "' ";
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(qury).list();
    System.out.println(
        "Data_list:::" + lst.toString() + "Size::" + lst.size() + " " + "Data::" + lst.get(0).toString());
    return lst.get(0).toString();
  }
  
  public List getAndroidTallyCallerLead(String tallycallerName, int limit, int pageNo) {
    int i = limit * pageNo;
    System.out.println("i : " + i);
    int final_PageNo = Math.abs(limit - i);
    System.out.println("Final OFFSET : " + final_PageNo);
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tagName,lead.dialState,lead.altmobileNo FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE  tallycaller.username='" + 
      
      tallycallerName + "' AND leadState='Open' ORDER BY lead.createDate DESC LIMIT " + limit + 
      " OFFSET " + final_PageNo + " ";
    System.out.println("query--" + query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getAndroidCategoryLead(String tallyCallername, String catname, int limit, int pageNo) {
    int i = limit * pageNo;
    System.out.println("i : " + i);
    int final_PageNo = Math.abs(limit - i);
    System.out.println("Fina OFFSET : " + final_PageNo);
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tagName,lead.dialState,lead.altmobileNo FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid    WHERE categorymanager.categortName='" + 
      
      catname + "' AND tallycaller.username='" + tallyCallername + 
      "'   AND leadState='Open' LIMIT " + limit + " OFFSET " + final_PageNo + " ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List getAndroidLeadByCompanyIdNDmobileNo(String mobNo, int companyId, int limit, int pageNo) {
    int i = limit * pageNo;
    System.out.println("i : " + i);
    int final_PageNo = Math.abs(limit - i);
    System.out.println("Fina OFFSET : " + final_PageNo);
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,  lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  WHERE lead.companyId=" + 
      
      companyId + " AND lead.mobileNo='" + mobNo + "' LIMIT " + limit + 
      " OFFSET " + final_PageNo + " ";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public String getAndroTellyCount(String tallycallerName) {
	  List lst = null;
    String qury = "";
    qury = "SELECT COUNT(*) FROM lead  INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid \n WHERE  tallycaller.username='" + 
      tallycallerName + "'  AND leadState='Open' ";
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(qury).list();
    System.out.println(
        "Data_list:::" + lst.toString() + "Size::" + lst.size() + " " + "Data::" + lst.get(0).toString());
    return lst.get(0).toString();
  }
  
  public String getAndroidCountByCompanyIdNDmobileNo(String mobNo, int companyId) {
	  List lst = null;
    String qury = "";
    qury = "SELECT COUNT(*) FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  WHERE lead.companyId=" + 
      companyId + " AND lead.mobileNo='" + mobNo + "' ";
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(qury).list();
    System.out.println(
        "Data_list:::" + lst.toString() + "Size::" + lst.size() + " " + "Data::" + lst.get(0).toString());
    return lst.get(0).toString();
  }
  
  public void addLeadToAutoDialManager(int leadId, int autoDialVal) {
    String query = "UPDATE lead SET lead.autoDial=" + autoDialVal + " WHERE lead.leaadId=" + leadId;
    this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public List<Object[]> GetLeadStateByManager(int managerId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT stateId,stateName FROM leadprocesstate WHERE companyId=" + managerId).list();
  }
  
  public List<Object[]> getTallyCallerByManager(int managerId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM tallycaller WHERE companyId=" + managerId + " ").list();
  }
  
  public String getTallyCallerLeadCount(int teleId, String leadStateId) {
	  List lst = null;
    String qury = "";
    qury = "SELECT COUNT(*) FROM lead WHERE leadProcessStatus='" + leadStateId + "' AND tallyCalletId=" + teleId + 
      " ";
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(qury).list();
    return lst.get(0).toString();
  }
  
  public String getLeadStateCount(String leadStateId, int managerId) {
	  List lst = null;
    String qury = "";
    qury = "SELECT COUNT(*) FROM lead WHERE leadProcessStatus='" + leadStateId + "' AND companyId=" + managerId + 
      " ";
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(qury).list();
    return lst.get(0).toString();
  }
  
  public List getLeadByLeadState(int teleId, String leadStateId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT lead.leaadId,CONCAT(lead.firstName , ' ' , lead.lastName) AS NAME,lead.email,lead.mobileNo,categorymanager.categortName ,\n       lead.leadProcessStatus,lead.createDate,lead.sheduleDate,CONCAT(tallycaller.firstname,' ',tallycaller.lastname),lead.leadState,lead.csvData,lead.companyName,lead.website,lead.Country,\n       lead.state,lead.city,lead.tallyCalletId,lead.leadcomment,lead.tagName,lead.firstName,lead.lastName FROM lead \n       LEFT JOIN   categorymanager ON lead.categoryId=categorymanager.categotyId  \n       LEFT JOIN  tallycaller ON lead.tallyCalletId=tallycaller.tcallerid  \n       WHERE lead.leadProcessStatus='" + 
        
        leadStateId + "' AND  lead.tallyCalletId=" + teleId + 
        " ")
      .list();
  }
  
  public List getFollowupsByLead(int leadId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(
        "SELECT fh.followupsId,tc.username,fh.callingTime,fh.remark,fh.sheduleTime,fh.newStatus,fh.callDuration,fh.callStatus,fh.followsUpType,fh.failstatus,fh.successStatus FROM followupshistory fh JOIN tallycaller tc ON fh.tallycallerId=tc.tcallerid WHERE fh.leadId=" + 
        leadId + " ")
      .list();
  }
  
  public List<Object[]> getTallyCallerByMobile(String mobileNo) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT * FROM tallycaller WHERE mobno='" + mobileNo + "'  ").list();
  }
  
  public List<Object[]> getWhiteDetails(String domain) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT logoImage,image2 FROM displaysetting WHERE url='" + domain + "' ").list();
  }
  
  public int tcallerChangePass(int tcId, String password) {
    int result = 0;
    String qry = "UPDATE tallycaller SET passwrd  ='" + password + "' WHERE tcallerid=" + tcId + " ";
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(qry);
      result = sQLQuery.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
    return result;
  }
  
  public List<Object[]> getLeadSuccessStatByLeadStat(String leadStatId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery("SELECT id,statusName FROM successstatus WHERE leadStateId='" + leadStatId + "' ")
      .list();
  }
  
  public List getCategoryByManager(String managerId) {
    return this.sessionFactory.getCurrentSession()
      .createSQLQuery(
        "SELECT categotyId,categortName FROM categorymanager WHERE companyId='" + managerId + "' ")
      .list();
  }
  
  public List GetCatColumns(int id) {
    String sql = "SELECT ef.fid,cm.categortName,REPLACE( ef.fieldName, '_', ' ' ) AS fieldName,ef.catId FROM extrafields ef JOIN categorymanager cm ON ef.catId=cm.categotyId WHERE ef.companyId=" + 
      id;
    return this.sessionFactory.getCurrentSession().createSQLQuery(sql).list();
  }
  
  public void deleteCatCol(Long colId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM extrafields WHERE fid='" + colId + "' ")
      .executeUpdate();
  }
  
  public List getAndroFailCallList(String query) {
    List failList = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    System.out.println("product_list_size::" + failList.size());
    return failList;
  }
  
  public List<ExtraFields> getFieldsByUId(int Id, int catId) {
    return this.sessionFactory.getCurrentSession()
      .createQuery("FROM ExtraFields WHERE catId=" + catId + " and companyId=" + Id).list();
  }
  
  public void saveExtraFields(ExtraFields extrafields) {
    this.sessionFactory.getCurrentSession().save(extrafields);
  }
  
  public List<Object[]> getLeadByCatComp(int compId, int catId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery("SELECT leaadId,csvData FROM lead WHERE categoryId=" + catId + " AND companyId=" + compId + " ").list();
  }
  
  public int updateCSVJSON(int leadId, String json) {
    String hql2 = "UPDATE lead SET csvData ='" + json + "' WHERE leaadId='" + leadId + "' ";
    int result = 0;
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql2);
      result = sQLQuery.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
    return result;
  }
  
  public List GetAndroidCatAutoDialLead(int tcId, int cat) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,lead.mobileNo,  lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.tagName,lead.dialState,lead.altmobileNo  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid     WHERE  tallycaller.tcallerid=" + 
      
      tcId + "  AND autoDial=1 AND lead.categoryId=" + cat + 
      " AND lead.leadState='Open' ORDER BY lead.leaadId  ";
    System.out.println("query_Android :: " + query);
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List<Object[]> getLeadByCatTele(int tcId, int catId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM lead WHERE categoryId=" + catId + " AND tallyCalletId=" + tcId + " ").list();
  }
  
  public List<ExtraFields> getFieldsByUId(int Id) {
    return this.sessionFactory.getCurrentSession().createQuery("FROM ExtraFields WHERE companyId=" + Id).list();
  }
  
  public CategoryManager getCategory(int uid) {
    CategoryManager category = (CategoryManager)this.sessionFactory.getCurrentSession()
      .createQuery("from CategoryManager where categotyId=" + uid).uniqueResult();
    return category;
  }
  
  public List<Object[]> getLeadProStateById(int stId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM leadprocesstate WHERE stateId=" + stId + " ").list();
  }
  
  public int extraFieldCount(int catId, int compId, String fieldname) {
    List myVidCount = null;
    myVidCount = this.sessionFactory.getCurrentSession().createSQLQuery("SELECT COUNT(*) FROM extrafields WHERE catId=" + catId + " AND companyId=" + compId + " AND fieldName='" + fieldname + "' ").list();
    return Integer.parseInt(myVidCount.get(0).toString());
  }
  
  public List AndroidLeadSearch(String query, int limit, int pageNo) {
    int i = limit * pageNo;
    System.out.println("i : " + i);
    int final_PageNo = Math.abs(limit - i);
    System.out.println("Final OFFSET : " + final_PageNo);
    String querynw = String.valueOf(query) + "LIMIT " + limit + " OFFSET " + final_PageNo + " ";
    System.out.println("querynw--" + querynw);
    return this.sessionFactory.getCurrentSession().createSQLQuery(querynw).list();
  }
  
  public void DeleteLeadBeforeAssign(int leadId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM lead WHERE leaadId = " + leadId).executeUpdate();
  }
  
  public List<Object[]> getHashmukhLeadCount(String categoryId, String contactNo) {
    return this.sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM lead WHERE categoryId=" + categoryId + " AND mobileNo=" + contactNo + " ").list();
  }
  
  public int updateFailCallLeadStat(String query) {
    int result = 0;
    try {
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(query);
      result = sQLQuery.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
    return result;
  }
  
  public int getCountTele(String query) {
	  List lst = null;
    lst = this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
    String count = lst.get(0).toString();
    return Integer.parseInt(count);
  }
  
  public int updateTelecallerAccessDate(int tcId, String apiKey) {
    int result = 0;
    try {
      Date dt = new Date();
      String date1 = this.df1.format(dt);
      String hql2 = "UPDATE apikey SET accessDate='" + date1 + "' WHERE keyvalue='" + apiKey + "' AND uid=" + tcId + " ";
      SQLQuery sQLQuery = this.sessionFactory.getCurrentSession().createSQLQuery(hql2);
      result = sQLQuery.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
    } 
    return result;
  }
  
  public List<Object[]> getTeleLastAccess(int compId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery("SELECT tc.tcallerid,tc.username,CONCAT(tc.firstname,' ',tc.lastname)AS tcName,tc.mobno,ak.accessDate FROM tallycaller tc JOIN apikey ak ON tc.tcallerid=ak.uid WHERE tc.companyId=" + compId + " ").list();
  }
  
  public List<Object[]> getExtraParamsByCat(int compId, int catId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(" SELECT * FROM extrafields WHERE extrafields.companyId=" + compId + " AND catId=" + catId + " ").list();
  }
  
  public void deleteUserRole(int userId) {
    this.sessionFactory.getCurrentSession().createSQLQuery(" DELETE FROM user_roles WHERE uid=" + userId + " ").executeUpdate();
  }
  
  public List GetCatAutoDialSingalLead(int teleId, int cat) {
    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,lead.mobileNo,  lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city  FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId  INNER JOIN tallycaller ON lead.tallyCalletId=tallycaller.tcallerid     WHERE  tallycaller.tcallerid=" + 
      
      teleId + "  AND autoDial=1 AND lead.categoryId=" + cat + 
      " AND lead.leadState='Open' ORDER BY lead.leaadId LIMIT 1";
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public List<Object[]> getLeadCatAdvAutoDial(String uname, int catId, String stDate, String endDate) {
    if (catId == 0 && stDate.equals("NA") && endDate.equals("NA"))
      return this.sessionFactory.getCurrentSession().createSQLQuery(" SELECT categorymanager.categortName,COUNT(lead.categoryId) AS LeadCount,categoryId FROM lead  JOIN categorymanager \r\n      ON lead.categoryId=categorymanager.categotyId WHERE lead.leadState='Open' AND lead.autoDial=1  AND \r\n      lead.tallyCalletId=(SELECT tcallerid FROM tallycaller WHERE username='" + 
          
          uname + "') GROUP BY lead.categoryId ").list(); 
    if (catId == 0)
      return this.sessionFactory.getCurrentSession().createSQLQuery(" SELECT categorymanager.categortName,COUNT(lead.categoryId) AS LeadCount,categoryId FROM lead  JOIN categorymanager \r\n      ON lead.categoryId=categorymanager.categotyId WHERE lead.leadState='Open' AND lead.autoDial=1  AND \r\n      lead.tallyCalletId=(SELECT tcallerid FROM tallycaller WHERE username='" + 
          
          uname + "')  \r\n" + 
          "      AND (lead.sheduleDate BETWEEN '" + stDate + "' AND ADDDATE('" + endDate + "',INTERVAL 1 DAY))  GROUP BY lead.categoryId ").list(); 
    return this.sessionFactory.getCurrentSession().createSQLQuery(" SELECT categorymanager.categortName,COUNT(lead.categoryId) AS LeadCount,categoryId FROM lead  JOIN categorymanager \r\n      ON lead.categoryId=categorymanager.categotyId WHERE lead.leadState='Open' AND lead.autoDial=1  AND \r\n      lead.tallyCalletId=(SELECT tcallerid FROM tallycaller WHERE username='" + 
        
        uname + "') AND lead.categoryId=" + catId + " \r\n" + 
        "      AND (lead.sheduleDate BETWEEN '" + stDate + "' AND ADDDATE('" + endDate + "',INTERVAL 1 DAY))  GROUP BY lead.categoryId ").list();
  }
  
  public List GetAdvanceCatAutoDialLead(int parseInt, int cat, String stDate, String endDate) {
    String query = "NA";
    if (stDate.equals("NA") && endDate.equals("NA")) {
      query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,\r\n       lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo \r\n       FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON \r\n       lead.tallyCalletId=tallycaller.tcallerid WHERE  tallycaller.tcallerid=" + 
        
        parseInt + "  AND autoDial=1 AND lead.categoryId=" + cat + "\r\n" + 
        "\t    AND lead.leadState='Open' ORDER BY lead.leaadId LIMIT 1";
    } else if (stDate.equals("undefined") && endDate.equals("undefined")) {
      query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,\r\n       lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo \r\n       FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON \r\n       lead.tallyCalletId=tallycaller.tcallerid WHERE  tallycaller.tcallerid=" + 
        
        parseInt + "  AND autoDial=1 AND lead.categoryId=" + cat + "\r\n" + 
        "\t    AND lead.leadState='Open' ORDER BY lead.leaadId LIMIT 1";
    } else {
      query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState,\r\n       lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.altmobileNo \r\n       FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId INNER JOIN tallycaller ON \r\n       lead.tallyCalletId=tallycaller.tcallerid WHERE  tallycaller.tcallerid=" + 
        
        parseInt + "  AND autoDial=1 AND lead.categoryId=" + cat + "\r\n" + 
        "\t    AND lead.leadState='Open' AND (lead.sheduleDate BETWEEN '" + stDate + "' AND ADDDATE('" + endDate + "',INTERVAL 1 DAY)) ORDER BY lead.leaadId LIMIT 1";
    }   
    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
  }
  
  public void updateLead(String query) {
    int query1 = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
  }
  
  public List<Lead> getDuplicateLeads(String mobileNo, int CompId) {
    return this.sessionFactory.getCurrentSession().createSQLQuery(" SELECT lead.leaadId FROM lead WHERE lead.companyId=" + CompId + " AND lead.mobileNo='" + mobileNo + "' AND lead.leadType='API' ORDER BY lead.`leaadId` ASC ").list();
  }
  
  public void deleteLeadById(int leadId) {
    this.sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM lead WHERE lead.leaadId=" + leadId + " ");
  }
  
  public void AddAutoDial(String query) {
	    System.out.println("query::" + query);
	    this.sessionFactory.getCurrentSession().createSQLQuery(query);
	  }
	  
	  public List getDueTodayLead(int tcId) {
	    String query = "SELECT lead.leaadId,categorymanager.categortName,lead.createDate,lead.email,lead.firstName,lead.lastName,lead.leadProcessStatus,lead.leadState, lead.mobileNo,lead.sheduleDate,lead.csvData,lead.leadType,lead.autoDial,lead.companyName,lead.website,lead.Country,lead.state,lead.city,lead.dialState   FROM lead INNER JOIN categorymanager ON lead.categoryId=categorymanager.categotyId WHERE DATE(lead.sheduleDate)=CURDATE() AND lead.tallyCalletId=" + 
	      
	      tcId;
	    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
	  }
	  
	  public List GetAllLeads(String query) {
		    System.out.println("query::" + query);
		    return this.sessionFactory.getCurrentSession().createSQLQuery(query).list();
		  }
	  
	 /* public void UpdateLead(int id, String fname, String lname, String email, String mobno, String cat, String sataus, String compName, String webAdd, String cont, String stat, String cty, int tcaller, String updatealtmobileNo) {
		    String query = "update lead set categoryId=" + cat + ",firstName='" + fname + "',lastName='" + lname + 
		      "',mobileNo='" + mobno + "',email='" + email + "',leadProcessStatus='" + sataus + "',companyName='" + 
		      compName + "',website='" + webAdd + "',Country='" + cont + "',state='" + stat + "',city='" + cty + 
		      "',tallyCalletId=" + tcaller + ",altmobileNo='" + updatealtmobileNo + "' where leaadId=" + id;
		    System.out.println(query);
		    int query1 = this.sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		  }*/
}

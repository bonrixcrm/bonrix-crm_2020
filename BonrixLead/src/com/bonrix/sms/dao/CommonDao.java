package com.bonrix.sms.dao;

import java.math.BigInteger;
import java.util.List;

import com.bonrix.sms.model.AutoDialLog;
import com.bonrix.sms.model.SystemParameter;

/**
 * @author Bhavesh
 *
 */
public interface CommonDao {

	List createSqlQuery(String query);

	List createQuery(String query);

	int createupdateQuery(String query);

	Object saveObject(Object obj);

	int createupdateSqlQuery(String query);

	int updateObject(Object obj);
	
	public BigInteger getcount(String query);
	
	List createSqlQuery(String query,int first,int max);
	
	public int updateResponce(int folId,String reponce);
	
	public List<SystemParameter> getSysParam();
	
	public int saveRetObject(Object log);
	
	Object GetObject(String query);


	public Object getSingleObject(String query);


}

package com.bonrix.sms.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.SystemParameter;



/**
 * @author Sajan
 *
 */
@Repository("CommonDao")
public class CommonDaoImpl implements CommonDao {
	@Autowired
	SessionFactory sessionFactory;

	final SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");



	@Override
	public List createSqlQuery(String query) {
		System.out.println("In createSqlQuery");
		Query  query2=	 sessionFactory.getCurrentSession().createSQLQuery(query);
		System.out.println("In createSqlQuery :: "+query2.list());
		return query2.list();
	}

	@Override
	public List createQuery(String query) {
	Query  query2=	 sessionFactory.getCurrentSession().createQuery(query);	
	return query2.list();
	}

	@Override
	public int createupdateQuery(String query) {
		Query  query2=	 sessionFactory.getCurrentSession().createQuery(query);	
		return query2.executeUpdate();
	}

	@Override
	public Object saveObject(Object obj) {
		 try{
			 sessionFactory.getCurrentSession().save(obj);
			 return obj;
		 }catch(Exception e) {
			return obj;
		}
	}
	
	
	@Override
	public int createupdateSqlQuery(String query) {
		Query query2=	 sessionFactory.getCurrentSession().createSQLQuery(query);	
		return query2.executeUpdate();
	}

	@Override
	public int updateObject(Object obj) {
		 try{
			 sessionFactory.getCurrentSession().update(obj);
			 return 1;
		 }catch(Exception e) {
			return 0;
		}
	}

	
	
	@Override
	public BigInteger getcount(String query) {
		Query query2 = sessionFactory.getCurrentSession()
				.createSQLQuery(query);
		return (BigInteger) query2.uniqueResult();
	}

	@Override
	public List createSqlQuery(String query, int first, int max) {
		return sessionFactory.getCurrentSession().createSQLQuery(query).setMaxResults(max).setFirstResult(first).list();		
	}

	@Override
	public int updateResponce(int folId, String reponce) {
		// TODO Auto-generated method stub
		String hql = "UPDATE AndroidLog set responce = :res "  + 
	             "WHERE folloId = :folloId";
	Query query = sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("folloId", folId);
	query.setParameter("res", reponce);
	int result = query.executeUpdate();
	System.out.println("Rows affected: " + result);
		return result;
	}

	@Override
	public List<SystemParameter> getSysParam() {
		List<SystemParameter>	sysparam =sessionFactory.getCurrentSession().createQuery("FROM SystemParameter").list();
		return sysparam;
	}

	@Override
	public int saveRetObject(Object log) {
		return (Integer)sessionFactory.getCurrentSession().save(log);
	}

	@Override
	public Object GetObject(String query) {
		Object obj1 =sessionFactory.getCurrentSession().createQuery(query)
				.uniqueResult();
		return obj1;
	}

	@Override
	public Object getSingleObject(String query) {
		Query qry = sessionFactory.getCurrentSession().createSQLQuery(query);
		return qry.uniqueResult();
	}
	

	
}

package com.bonrix.sms.dao;

import java.util.List;

import com.bonrix.sms.model.MediaMaster;
import com.bonrix.sms.model.Tinyurlhit;

public interface MediaDAO {

	List findMediabyUid(Long userid);

	void saveMedia(MediaMaster user);

	MediaMaster getMediabytinyurl(String mediaurl);

	int saveTinyHit(Tinyurlhit th);

	String getHitSummarybyUid(Long uid);

	String getHitSummarybyMid(Long mid);

}

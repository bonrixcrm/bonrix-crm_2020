package com.bonrix.sms.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.MediaMaster;
import com.bonrix.sms.model.Tinyurlhit;
import com.google.gson.Gson;

@Repository("MediaDAO")
public class MediaDAOImpl implements MediaDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List findMediabyUid(Long userid) {
		String q = "SELECT m.mediaid,m.medianame,m.mtype,m.oldmsize,m.newmsize,m.tinyurl,DATE_FORMAT(m.uploadedon,'%d/%m/%Y %H:%i'), COUNT(t.hitid) AS hitcnt,m.isactive,1"
				+ " FROM mediamaster AS m LEFT JOIN tinyurlhit AS t ON m.mediaid = t.mediaid WHERE m.userid=" + userid
				+ " GROUP BY m.mediaid ORDER BY m.uploadedon DESC";
		// System.out.print("QQQ::"+q);
		return sessionFactory.getCurrentSession().createSQLQuery(q).list();
	}

	@Override
	public void saveMedia(MediaMaster user) {
		// TODO Auto-generated method stub
		System.out.println("In MediaUSER:::" + user.getMainurl());
		sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public MediaMaster getMediabytinyurl(String mediaurl) {
		List data = null;
		MediaMaster mmaster = null;
		try {
			mmaster = (MediaMaster) sessionFactory.getCurrentSession()
					.createQuery("from MediaMaster where tinyurl=:TURL").setString("TURL", mediaurl).list().get(0);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return mmaster;
	}

	@Override
	public int saveTinyHit(Tinyurlhit th) {
		sessionFactory.getCurrentSession().save(th);
		return 1;
	}

	@Override
	public String getHitSummarybyUid(Long uid) {
		String q = "SELECT mm.medianame,COUNT(*) AS cnt, DATE_FORMAT(hiton,'%Y-%m-%d') AS hiton FROM tinyurlhit th,mediamaster mm WHERE mm.mediaid=th.mediaid and mm.userid="
				+ uid + " GROUP BY DATE(hiton);";
		return new Gson().toJson(sessionFactory.getCurrentSession().createSQLQuery(q).list());
	}

	@Override
	public String getHitSummarybyMid(Long mid) {

		String q = "SELECT mm.medianame,COUNT(*) AS cnt, DATE_FORMAT(hiton,'%Y-%m-%d') AS hiton FROM tinyurlhit th,mediamaster mm WHERE mm.mediaid=th.mediaid and th.mediaid="
				+ mid + " GROUP BY DATE(hiton);";
		return new Gson().toJson(sessionFactory.getCurrentSession().createSQLQuery(q).list());
	}

}

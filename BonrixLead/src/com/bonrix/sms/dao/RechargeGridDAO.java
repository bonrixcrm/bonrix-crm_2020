package com.bonrix.sms.dao;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.RechargeLogMaster;
import com.bonrix.sms.model.RechargeNotifyMaster;

public interface RechargeGridDAO {

	int saveRechargeLog(RechargeLogMaster rgm);

	ApiKey gettAPiByUid(long uid, String key);

	int saveRechargeNotify(RechargeNotifyMaster rnm);

}

package com.bonrix.sms.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.RechargeLogMaster;
import com.bonrix.sms.model.RechargeNotifyMaster;

@Repository("RechargeGridDAO")
public class RechargeGridDAOImpl implements RechargeGridDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public int saveRechargeLog(RechargeLogMaster rgm) {
		try {
			sessionFactory.getCurrentSession().save(rgm);
			return 1;
		} catch (Exception ex) {
			return 0;
		}

	}

	@Override
	public ApiKey gettAPiByUid(long uid, String key) {

		ApiKey apiKey = new ApiKey();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ApiKey.class)
				.add(Restrictions.eq("uid", uid)).add(Restrictions.eq("keyValue", key));
		apiKey = (ApiKey) criteria.uniqueResult();
		return apiKey;
	}

	@Override
	public int saveRechargeNotify(RechargeNotifyMaster rnm) {
		try {
			sessionFactory.getCurrentSession().save(rnm);
			return 1;
		} catch (Exception ex) {
			return 0;
		}

	}

}

package com.bonrix.sms.dao;

import java.util.Date;
import java.util.List;

import com.bonrix.sms.model.WhiteListNumber;
import com.bonrix.sms.model.CampaignLog;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsDlrSummary;
import com.bonrix.sms.model.SmsHistory;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.SystemParameter;
import com.bonrix.sms.model.failedSmsQueue;
import com.bonrix.sms.model.SpamWords;

public interface SMSDao {

	void saveSmsQueue(List<SmsQueue> smsQueue);

	int saveFailedSmsQueue(List<failedSmsQueue> smsQueue);

	List<SmsQueue> getSmsQueue(int total, int isdnd);

	List<SmsQueue> getSmsQueueByUid(Long isdnd);

	List<SmsApi> getSMSApi(Long uid, int sid);

	int creditupdate(int crd, Long uid, Long stid);

	SenderName getSenderName(Long sendid);

	void saveSmsHistory(SmsHistory smshistory);

	int deletesmsq(Long qid);

	int deleteMultismsQ(String qid); // multipel

	String getSMSCreditByUid(Long uid);

	String getSMSQAdmin();

	String getSmsHistoryByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile, int service);

	String getSeduleMessageByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile, int service);

	String getSmsHistoryByAdmin(int page, int listSize, long uid, Date sdate, Date edate, String mobile);

	List getSmsHistoryByAdmin4Report(int page, int listSize, long uid, Date sdate, Date edate, String mobile);

	String getsmscountbyuid(Long lg, String sdate, String edate);

	SystemParameter getSysParameterByName(String sysname);

	List<SmsHistory> getSmsHistoryforDLR(int day, int limit);

	List<SmsHistory> getSmsHistorybyUid(int uid, int start, int max);

	SmsApi getSMSApibyApiId(Long apiid);

	int DLRUpdate(Long hid, String status, String message);

	int updateSystemParameterbyName(SystemParameter sm);

	List<Long> checkDndNumbers(String number);

	int DLRUpdateByMsgId(Long hid, String status, String msgid);

	int DelayDLRUpdate(String status, int hour);

	int updatesmppConfig(int cnfigid, int status, int threadid, String connecterror);

	List getSmpp();

	int resetSmppConfig();

	List getQueueCountByUser(long userid);

	List<SmsDlrSummary> updateSmsSummary(Long uid, String sdate, String edate);

	SmsHistory getHistoryByMsgid(Long msgid);

	HostSetting getHostSetting(String user);

	SenderName getSenderNameByUid(Long uid, String Sendername);

	CampaignLog saveCampaign(CampaignLog cl);

	List<SmsHistory> getSmsHistorybyCampid(int campid);

	CampaignLog getCampaignByMsgid(String campid);

	int saveSpamWord(SpamWords sm);

	List<SpamWords> getSpamWords();

	int saveWhiteList(WhiteListNumber sm);

	List<String> getWhiteList(int type);

	List<WhiteListNumber> getWhiteListObj(int type);

	int deleteBlackWhiteList(int sm);

	String getBlockedNumbers(int page, int listSize, String mobile);

}

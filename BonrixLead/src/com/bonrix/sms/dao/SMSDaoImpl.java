package com.bonrix.sms.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.WhiteListNumber;
import com.bonrix.sms.model.CampaignLog;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsDlrSummary;
import com.bonrix.sms.model.SmsHistory;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.SpamWords;
import com.bonrix.sms.model.SystemParameter;
import com.bonrix.sms.model.failedSmsQueue;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Repository("SMSDao")
public class SMSDaoImpl implements SMSDao {

	@Autowired
	SessionFactory sessionFactory;

	final SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

	final SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public void saveSmsQueue(List<SmsQueue> contacts) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		for (int i = 0; i < contacts.size(); i++) {
			session.save(contacts.get(i));
			if (i % 100 == 0) { // 20, same as the JDBC batch size
				// flush a batch of inserts and release memory:
				session.flush();
				session.clear();
			}
		}
		tx.commit();
		session.close();
	}

	@Override
	public List<SmsQueue> getSmsQueue(int total, int isdnd) {
		return sessionFactory.getCurrentSession()
				.createQuery("from SmsQueue WHERE isdndchecked=" + isdnd
						+ " and createdate < NOW() ORDER BY priority,createdate ASC")
				.setMaxResults(total).setCacheable(true).list();
	}

	@Override
	public List<SmsApi> getSMSApi(Long userid, int sid) { // sid=serviceid
		return sessionFactory.getCurrentSession()
				.createQuery("from SmsApi Where uId=:UID AND serviceid=:SID ORDER BY uId DESC").setLong("UID", userid)
				.setInteger("SID", sid).setCacheable(true).list();
	}

	@Override
	public int creditupdate(int crd, Long uid, Long stid) {
		String hql = "UPDATE smscredit set smscredit = smscredit -" + crd + " WHERE  smscredit > " + crd + " AND stid="
				+ stid + " AND uid=" + uid;
		// System.out.print("CREDIT333333333333333333333333333333333333333333333::
		// "+uid+" UPDATE::"+hql);
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

	@Override
	public SenderName getSenderName(Long sendid) {
		return (SenderName) sessionFactory.getCurrentSession().createQuery("from SenderName Where sid=:SID")
				.setLong("SID", sendid).setCacheable(true).list().get(0);
	}

	@Override
	public void saveSmsHistory(SmsHistory smshistory) {
		sessionFactory.getCurrentSession().save(smshistory);

	}

	@Override
	public int deletesmsq(Long qid) {

		String hql = "DELETE FROM SmsQueue " + "WHERE qid = :QID";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("QID", qid);
		int result = query.executeUpdate();
		// System.out.println("Rows affected: " + result);

		return result;
	}

	@Override
	public int deleteMultismsQ(String qid) {

		String hql = "DELETE FROM SmsQueue " + "WHERE qid IN (" + qid + ")";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		int result = query.executeUpdate();
		// System.out.println("Rows affected: " + result);

		return result;
	}

	@Override
	public String getSMSCreditByUid(Long uid) {
		List query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT smscredit,TYPE FROM smscredit sc,servicetype st WHERE sc.stid=st.stid AND sc.uid=" + uid)
				.list();
		return new Gson().toJson(query);
	}

	@Override
	public String getSMSQAdmin() {
		List query = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"SELECT  us.username,COUNT(*)  FROM smsqueue sq,users	us WHERE us.uid=sq.uid GROUP BY us.username")
				.list();
		return new Gson().toJson(query);

	}

	@Override
	public SystemParameter getSysParameterByName(String paraname) {
		SystemParameter syspara = new SystemParameter();
		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SystemParameter.class)
					.add(Restrictions.eq("paraname", paraname)).setCacheable(true);
			syspara = (SystemParameter) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return syspara;
	}

	@Override
	public String getSmsHistoryByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile,
			int service) {
		String q = "";
		String q2 = "";
		String q3 = "";
		List lst = null;
		if (sdate != null) {
			q += " sentdatetime between '" + df.format(sdate) + "' AND '" + df.format(edate) + "' AND ";
		}
		/*
		 * if(uid!=0){ q+=" uid="+uid+" AND"; }
		 */

		if (!mobile.isEmpty()) {
			q += " sh.mobileNumber='" + mobile + "' AND ";
		}

		if (service != 0) {
			q += " sh.stid=" + service + " AND ";
		}

		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select count(*) from smshistory sh where " + q + " sh.uid=" + uid);
		BigInteger count = (BigInteger) query.uniqueResult();

		String qury = "SELECT sh.hid,sh.status,sh.message,sh.mobileNumber,sh.result,DATE_FORMAT(sh.sentdatetime,'%d/%m/%Y %H:%i:%S'),DATE_FORMAT(sh.submitdatetime,'%d/%m/%Y %H:%i:%S'),sh.dlrstatus,st.type,sh.sid,sh.msgcount FROM smshistory sh,servicetype st WHERE "
				+ q + "st.stid=sh.stid AND sh.uid=" + uid + " ORDER BY sh.sentdatetime DESC";

		// System.out.println(qury);
		lst = sessionFactory.getCurrentSession().createSQLQuery(qury).setMaxResults(listSize)
				.setFirstResult(((page - 1) * listSize)).list();

		DatatableJsonObject personJsonObject = new DatatableJsonObject();
		personJsonObject.setRecordsFiltered(count.intValue());
		personJsonObject.setRecordsTotal(count.intValue());
		personJsonObject.setData(lst);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json2 = gson.toJson(personJsonObject);
		return json2;
	}

	@Override
	public String getSeduleMessageByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile,
			int service) {
		String q = "";
		String q2 = "";
		String q3 = "";
		List lst = null;
		if (sdate != null) {
			q += " sh.createdate between '" + df.format(sdate) + "' AND '" + df.format(edate) + "' AND ";
		}
		/*
		 * if(uid!=0){ q+=" uid="+uid+" AND"; }
		 */

		if (!mobile.isEmpty()) {
			q += " sh.number='" + mobile + "' AND ";
		}

		if (service != 0) {
			q += " sh.stid=" + service + " AND ";
		}

		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select count(*) from smsqueue sh where " + q + " sh.uid=" + uid);
		BigInteger count = (BigInteger) query.uniqueResult();

		String qury = "SELECT sh.qid,sh.number,sh.message,DATE_FORMAT(sh.createdate,'%d/%m/%Y %H:%i'),sh.senderid,st.type,sh.msgcredit FROM smsqueue sh,servicetype st WHERE "
				+ q + "st.stid=sh.stid AND sh.uid=" + uid + " ORDER BY sh.createdate DESC";

		// System.out.println(qury);
		lst = sessionFactory.getCurrentSession().createSQLQuery(qury).setMaxResults(listSize)
				.setFirstResult(((page - 1) * listSize)).list();

		DatatableJsonObject personJsonObject = new DatatableJsonObject();
		personJsonObject.setRecordsFiltered(count.intValue());
		personJsonObject.setRecordsTotal(count.intValue());
		personJsonObject.setData(lst);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json2 = gson.toJson(personJsonObject);
		return json2;
	}

	@Override
	public String getsmscountbyuid(Long lg, String sdate, String edate) {

		JsonArray jary = new JsonArray();

		if (edate.equalsIgnoreCase(df2.format(new Date()))) {
			String q = "SELECT DATE(sh.sentdatetime),"
					+ "SUM(sh.msgcount) AS TOTAL,SUM(CASE WHEN sh.dlrstatus = 'DELIVRD' THEN sh.msgcount ELSE 0 END ) AS DELIVERD,"
					+ "SUM(CASE WHEN sh.dlrstatus = 'REJECTED' || sh.dlrstatus='ERROR' || sh.dlrstatus='DND' THEN sh.msgcount ELSE 0 END) AS REJECTED,"
					+ "SUM(CASE WHEN sh.dlrstatus <> 'REJECTED' && sh.dlrstatus<>'ERROR' && sh.dlrstatus<>'DND' && sh.dlrstatus <> 'DELIVRD'  THEN sh.msgcount ELSE 0 END) AS SUBMITED"
					+ "  FROM smshistory sh WHERE DATE(sh.sentdatetime)=DATE(NOW()) AND sh.uid=" + lg
					+ " GROUP BY DATE(sh.sentdatetime)";

			if (lg == 0) {
				q = "SELECT DATE(sh.sentdatetime),"
						+ "SUM(sh.msgcount) AS TOTAL,SUM(CASE WHEN sh.dlrstatus = 'DELIVRD' THEN sh.msgcount ELSE 0 END ) AS DELIVERD,"
						+ "SUM(CASE WHEN sh.dlrstatus = 'REJECTED' || sh.dlrstatus='ERROR' || sh.dlrstatus='DND' THEN sh.msgcount ELSE 0 END) AS REJECTED,"
						+ "SUM(CASE WHEN sh.dlrstatus <> 'REJECTED' && sh.dlrstatus<>'ERROR' && sh.dlrstatus<>'DND' && sh.dlrstatus <> 'DELIVRD'  THEN sh.msgcount ELSE 0 END) AS SUBMITED"
						+ "  FROM smshistory sh WHERE DATE(sh.sentdatetime)=DATE(NOW())  GROUP BY DATE(sh.sentdatetime)";
			}
			List lst = sessionFactory.getCurrentSession().createSQLQuery(q).list();

			for (int l = 0; l < lst.size(); l++) {

				Object[] obj = (Object[]) lst.get(l);
				JsonObject jo = new JsonObject();

				jo.addProperty("TOTAL", obj[1].toString());
				jo.addProperty("DELIVRD", obj[2].toString());
				jo.addProperty("SUBMITED", obj[4].toString());
				jo.addProperty("REJECTED", obj[3].toString());
				jo.addProperty("DATE", obj[0].toString());
				jary.add(jo);
			}

		}
		String q2 = "SELECT SUM(total),SUM(DELIVRD),SUM(SUBMITED),SUM(REJECTD),ondate FROM sms_dlr_summary WHERE ondate >= '"
				+ sdate + "' AND ondate <= '" + edate + "' AND ondate AND  uid=" + lg + " GROUP BY ondate";
		// System.out.println(q2);

		if (lg == 0) {
			q2 = "SELECT SUM(total),SUM(DELIVRD),SUM(SUBMITED),SUM(REJECTD),ondate FROM sms_dlr_summary where ondate >= '"
					+ sdate + "' AND ondate <= '" + edate + "' GROUP BY ondate";
		}

		List lst2 = sessionFactory.getCurrentSession().createSQLQuery(q2).list();
		for (int j = 0; j < lst2.size(); j++) {

			Object[] obj = (Object[]) lst2.get(j);
			JsonObject jo = new JsonObject();
			jo.addProperty("TOTAL", obj[0].toString());
			jo.addProperty("DELIVRD", obj[1].toString());
			jo.addProperty("SUBMITED", obj[2].toString());
			jo.addProperty("REJECTED", obj[3].toString());
			jo.addProperty("DATE", obj[4].toString());
			jary.add(jo);

		}

		return jary.toString();
	}

	@Override
	public List<SmsDlrSummary> updateSmsSummary(Long uid, String sdate, String edate) {
		String q = "SELECT DATE(sh.sentdatetime), sh.uid,sh.stid,"
				+ "SUM(sh.msgcount) AS TOTAL,SUM(CASE WHEN sh.dlrstatus = 'DELIVRD' THEN sh.msgcount ELSE 0 END ) AS DELIVERD,"
				+ "SUM(CASE WHEN sh.dlrstatus = 'REJECTED' || sh.dlrstatus='ERROR' || sh.dlrstatus='DND' THEN sh.msgcount ELSE 0 END) AS REJECTED,"
				+ "SUM(CASE WHEN sh.dlrstatus <> 'REJECTED' && sh.dlrstatus<>'ERROR' && sh.dlrstatus<>'DND' && sh.dlrstatus <> 'DELIVRD'  THEN sh.msgcount ELSE 0 END) AS SUBMITED"
				+ " FROM smshistory sh WHERE DATE(sh.sentdatetime) BETWEEN '" + sdate + "' AND '" + edate
				+ "' GROUP BY DATE(sh.sentdatetime),uid,stid";

		List lst = sessionFactory.getCurrentSession().createSQLQuery(q).list();

		List<SmsDlrSummary> sdrlst = null;
		SmsDlrSummary sdr = new SmsDlrSummary();
		System.out.println("UpdateingSummary:SIZE:" + lst.size() + "--" + q);

		for (int l = 0; l < lst.size(); l++) {

			Object[] obj = (Object[]) lst.get(l);

			System.out.println("Saving date +" + obj[0].toString());
			sdr.setOndate(obj[0].toString());
			sdr.setUid(Long.parseLong(obj[1].toString()));
			sdr.setServiceid(Integer.parseInt(obj[2].toString()));
			sdr.setTotal(Integer.parseInt(obj[3].toString()));
			sdr.setDelivrd(Integer.parseInt(obj[4].toString()));
			sdr.setRejectd(Integer.parseInt(obj[5].toString()));
			sdr.setSubmited(Integer.parseInt(obj[6].toString()));
			sdr.setUpdateon(new Date());

			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			try {
				session.save(sdr);
				tx.commit();
				session.close();
			} catch (Exception ex) {
				tx.rollback();
				// sessionFactory.getCurrentSession().getTransaction().rollback();
				System.out.println("Duplicated :" + ex.getMessage());
				session.close();

				SmsDlrSummary ssmary = (SmsDlrSummary) sessionFactory.getCurrentSession()
						.createQuery("from SmsDlrSummary WHERE uid=" + Long.parseLong(obj[1].toString())
								+ " AND serviceid=" + Integer.parseInt(obj[2].toString()) + "  AND ondate='"
								+ obj[0].toString() + "'")
						.uniqueResult();
				sdr.setCrdid(ssmary.getCrdid());

				Session session2 = sessionFactory.openSession();
				Transaction tx2 = session2.beginTransaction();
				try {
					session2.update(sdr);
					tx2.commit();
				} catch (Exception ex2) {
					tx2.rollback();
					ex2.printStackTrace();
				} finally {
					session2.close();
				}
			}
			// sdrlst.add(sdr);

		}

		return sdrlst;
	}

	@Override
	public List<SmsHistory> getSmsHistoryforDLR(int day, int limit) {
		List<SmsHistory> shist = null;
		day = -day;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, day);

		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SmsHistory.class)
					.add(Restrictions.eq("status", "MSGID")).add(Restrictions.gt("sentDateTime", cal.getTime()))
					.addOrder(Order.desc("sentDateTime")).setMaxResults(limit);
			shist = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shist;

	}

	@Override
	public List<SmsHistory> getSmsHistorybyUid(int uid, int start, int max) {
		List<SmsHistory> shist = null;

		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SmsHistory.class)
					.add(Restrictions.eq("userId", (long) uid)).setMaxResults(max).setFirstResult(start);
			shist = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shist;

	}

	@Override
	public List<SmsHistory> getSmsHistorybyCampid(int campid) {
		List<SmsHistory> shist = null;

		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SmsHistory.class)
					.add(Restrictions.eq("bunchId", campid));

			shist = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shist;

	}

	@Override
	public CampaignLog getCampaignByMsgid(String campid) {
		CampaignLog shist = null;

		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CampaignLog.class)
					.add(Restrictions.eq("encryptedid", campid));

			shist = (CampaignLog) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shist;

	}

	@Override
	public SmsApi getSMSApibyApiId(Long apiid) {
		return (SmsApi) sessionFactory.getCurrentSession().createQuery("from SmsApi Where apiid=:UID")
				.setLong("UID", apiid).uniqueResult();
	}

	@Override
	public int DLRUpdate(Long hid, String status, String message) {
		String hql = "UPDATE smshistory set dlrstatus = '" + status + "',status='" + status + "'" + " WHERE  hid = "
				+ hid;
		// System.out.print("CREDIT333333333333333333333333333333333333333333333::
		// "+uid+" UPDATE::"+hql);
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

	@Override
	public int DelayDLRUpdate(String status, int hour) {
		String hql = "UPDATE smshistory set result='CTD',dlrstatus = '" + status + "',status='" + status + "'"
				+ " WHERE result='CT' AND sentdatetime <= DATE_SUB(NOW(), INTERVAL " + hour + " HOUR)";
		// System.out.print("CREDIT333333333333333333333333333333333333333333333::
		// "+uid+" UPDATE::"+hql);
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

	@Override
	public int DLRUpdateByMsgId(Long hid, String status, String msgid) {
		String hql = "UPDATE smshistory set dlrstatus = '" + status + "',status='" + status + "'" + " WHERE  result = '"
				+ msgid + "'";
		// System.out.print("CREDIT333333333333333333333333333333333333333333333::
		// "+uid+" UPDATE::"+hql);
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

	@Override
	public String getSmsHistoryByAdmin(int page, int listSize, long uid, Date sdate, Date edate, String mobile) {
		String q = "";
		String q2 = "";
		String q3 = "";
		List lst = null;
		Long t1 = new Date().getTime();
		if (sdate != null) {
			q += " sh.sentdatetime between '" + df.format(sdate) + "' AND '" + df.format(edate) + "' AND";
		}
		if (!mobile.isEmpty()) {
			q += " sh.mobileNumber='" + mobile + "' AND ";
		}

		if (uid != 0) {
			q2 = "AND sh.uid=" + uid;

			q3 = "select count(*) from smshistory sh where " + q + " uid=" + uid;
		} else {
			q2 = "AND sh.uid<>0";
			q3 = "select count(*) from smshistory sh where " + q + " 1=1";
		}
		Query query = sessionFactory.getCurrentSession().createSQLQuery(q3);

		BigInteger count = (BigInteger) query.uniqueResult();
		Long t2 = new Date().getTime();

		System.out.println("total Time:for Total:" + (t2 - t1) + "QQQ:" + q3);
		long t3 = new Date().getTime();
		String qury = "SELECT sh.hid,sh.status,sh.message,sh.mobileNumber,sh.result,DATE_FORMAT(sh.sentdatetime,'%d/%m/%Y %H:%i:%S'),DATE_FORMAT(sh.submitdatetime,'%d/%m/%Y %H:%i:%S'),sh.dlrstatus,st.type,sh.sid,sh.uid FROM smshistory sh,servicetype st WHERE "
				+ q + " st.stid=sh.stid " + q2 + " ORDER BY sh.sentdatetime DESC";

		/*
		 * String qury =
		 * "SELECT sh.hid,sh.status,sh.message,sh.mobileNumber,sh.result,DATE_FORMAT(sh.sentdatetime,'%d/%m/%Y %H:%i:%S'),DATE_FORMAT(sh.submitdatetime,'%d/%m/%Y %H:%i:%S'),sh.dlrstatus,st.type,sh.sid,us.username FROM smshistory sh,servicetype st,users us WHERE "
		 * + q + " st.stid=sh.stid ANd us.uid=sh.uid " + q2 +
		 * " ORDER BY sh.sentdatetime DESC";
		 */
		System.out.println(qury);
		lst = sessionFactory.getCurrentSession().createSQLQuery(qury).setMaxResults(listSize)
				.setFirstResult(((page - 1) * listSize)).list();

		System.out.println("Total TIME IN FETCH DATA::" + (new Date().getTime() - t3) + "QQ:" + qury);
		DatatableJsonObject jo = new DatatableJsonObject();
		jo.setRecordsFiltered(count.intValue());
		jo.setRecordsTotal(count.intValue());
		jo.setData(lst);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json2 = gson.toJson(jo);
		return json2;
	}

	@Override
	public List getSmsHistoryByAdmin4Report(int service, int listSize, long uid, Date sdate, Date edate,
			String sendername) {
		String q = "";
		String q2 = "";
		String q3 = "";
		List lst = null;
		if (sdate != null) {
			q += " sentdatetime between '" + df.format(sdate) + "' AND '" + df.format(edate) + "' AND";
		}
		if (!sendername.isEmpty()) {
			q += " sh.sid='" + sendername + "' AND ";
		}

		if (service != 0) {
			q += " sh.stid='" + service + "' AND ";
		}
		if (uid != 0) {
			q2 = "AND sh.uid=" + uid;

			q3 = "select count(*) from smshistory where " + q + " uid=" + uid;
		} else {
			q3 = "select count(*) from smshistory where " + q + " 1=1";
		}
		// Query query = sessionFactory.getCurrentSession().createSQLQuery(q3);

		// BigInteger count = (BigInteger) query.uniqueResult();
		String qury = "SELECT sh.hid,sh.status,sh.message,sh.mobileNumber,sh.result,DATE_FORMAT(sh.sentdatetime,'%d/%m/%Y %H:%i:%S'),DATE_FORMAT(sh.submitdatetime,'%d/%m/%Y %H:%i:%S'),sh.dlrstatus,st.type,sh.sid,us.username FROM smshistory sh,servicetype st,users us WHERE "
				+ q + " st.stid=sh.stid ANd us.uid=sh.uid " + q2 + " ORDER BY sh.sentdatetime DESC";
		System.out.println(qury);
		lst = sessionFactory.getCurrentSession().createSQLQuery(qury).list();
		// .setMaxResults(listSize)
		// .setFirstResult(((page - 1) * listSize)).list();
		/*
		 * DatatableJsonObject jo = new DatatableJsonObject();
		 * jo.setRecordsFiltered(count.intValue());
		 * jo.setRecordsTotal(count.intValue()); jo.setData(lst); Gson gson =
		 * new GsonBuilder().setPrettyPrinting().create(); String json2 =
		 * gson.toJson(jo);
		 */
		return lst;
	}

	@Override
	public int updateSystemParameterbyName(SystemParameter sm) {
		sessionFactory.getCurrentSession().saveOrUpdate(sm);
		return 1;
	}

	@Override
	public List<Long> checkDndNumbers(String number) {

		List<Long> list = sessionFactory.getCurrentSession()
				.createSQLQuery("SELECT MobileNo FROM blockednumbers WHERE MobileNo IN (" + number + ")").list();

		return list;
	}

	@Override
	public List<SmsQueue> getSmsQueueByUid(Long uid) {
		return sessionFactory.getCurrentSession().createQuery("from SmsQueue WHERE uid=" + uid).list();
	}

	@Override
	public int updatesmppConfig(int cnfigid, int status, int threadid, String connecterror) {
		String hql = "UPDATE smppconfig set connecterror='" + connecterror + "',threadid = '" + threadid
				+ "',isrunning=isrunning+" + status + " WHERE  stid = " + cnfigid;

		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
			result = query.executeUpdate();
		} catch (Exception ex) {
			// ex.printStackTrace();

		}

		return result;
	}

	@Override
	public List getSmpp() {
		String query = "SELECT stid,smppname,systemid,syspassowrd,isactive,isrunning,systemtype,channelno,connecterror,ip,maxconnect,port,tps,cthread,1 FROM smppconfig";
		return sessionFactory.getCurrentSession().createSQLQuery(query).list();
	}

	public int resetSmppConfig() {
		String hql = "UPDATE smppconfig set isrunning=0";

		String shql = "truncate table smppconfig_child";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(shql);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

	@Override
	public List getQueueCountByUser(long userid) {
		List query = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"SELECT COUNT(*) AS cnt,st.type,usr.username FROM smsqueue sq,servicetype st,users usr WHERE usr.uid=sq.uid AND st.stid=sq.stid  GROUP BY sq.stid,sq.uid")
				.list();
		return query;
	}

	@Override
	public SmsHistory getHistoryByMsgid(Long msgid) {
		SmsHistory shist = null;
		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SmsHistory.class)
					.add(Restrictions.eq("trxId", msgid));
			shist = (SmsHistory) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shist;
	}

	@Override
	public HostSetting getHostSetting(String user) {

		/* System.out.println("----------------getHostSetting-------------"); */
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(HostSetting.class)
				.add(Restrictions.eq("user", user));
		HostSetting hostSetting = (HostSetting) criteria.uniqueResult();

		return hostSetting;
	}

	@Override
	public SenderName getSenderNameByUid(Long uid, String Sendername) {
		SenderName shist = null;
		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SenderName.class)
					.add(Restrictions.eq("senderName", Sendername)).add(Restrictions.eq("uid", uid));
			shist = (SenderName) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shist;
	}

	@Override
	public CampaignLog saveCampaign(CampaignLog cl) {
		sessionFactory.getCurrentSession().save(cl);
		return cl;

	}

	@Override
	public int saveSpamWord(SpamWords sm) {
		try {
			sessionFactory.getCurrentSession().save(sm);
			return 1;
		} catch (Exception ex) {
			return 0;
		}
	}

	@Override
	public List<SpamWords> getSpamWords() {

		return sessionFactory.getCurrentSession().createQuery("from SpamWords WHERE isactive =1").list();

	}

	@Override
	public int saveWhiteList(WhiteListNumber sm) {
		try {
			sessionFactory.getCurrentSession().save(sm);
			return 1;
		} catch (Exception ex) {
			return 0;
		}
	}

	@Override
	public List<String> getWhiteList(int type) {
		return sessionFactory.getCurrentSession().createQuery("select whitenumber from WhiteListNumber").list();

	}

	@Override
	public List<WhiteListNumber> getWhiteListObj(int type) {
		return sessionFactory.getCurrentSession().createQuery("from WhiteListNumber").list();

	}

	@Override
	public int deleteBlackWhiteList(int sm) {
		String hql = "DELETE FROM WhiteListNumber " + "WHERE wid = :QID";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("QID", sm);
		int result = query.executeUpdate();
		// System.out.println("Rows affected: " + result);

		return result;
	}

	@Override
	public String getBlockedNumbers(int page, int listSize, String mobile) { // for
																				// dnd
																				// Filter
		String q = "";
		String q2 = "";
		String q3 = "";
		List<String> lst = null;
		Long t1 = new Date().getTime();

		q3 = "select count(*) from blockednumbers";

		Query query = sessionFactory.getCurrentSession().createSQLQuery(q3);

		BigInteger count = (BigInteger) query.uniqueResult();
		Long t2 = new Date().getTime();

		System.out.println("total Time:for Total:" + (t2 - t1) + "QQQ:" + q3);
		long t3 = new Date().getTime();
		String qury = "from BlockedNumbers";

		if (mobile != "" && mobile != null) {
			qury = "from BlockedNumbers  WHERE number=" + mobile;
		}

		lst = sessionFactory.getCurrentSession().createQuery(qury).setMaxResults(listSize)
				.setFirstResult(((page - 1) * listSize)).list();
		System.out.println("Total TIME IN FETCH DATA::" + (new Date().getTime() - t3) + "QQ:" + qury);
		DatatableJsonObject jo = new DatatableJsonObject();
		jo.setRecordsFiltered(count.intValue());
		jo.setRecordsTotal(count.intValue());
		jo.setData(lst);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json2 = gson.toJson(jo);
		return json2;
	}

	@Override
	public int saveFailedSmsQueue(List<failedSmsQueue> smsQueue) {
		// TODO Auto-generated method stub
		return 0;
	}

}

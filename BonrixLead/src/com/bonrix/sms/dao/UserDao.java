package com.bonrix.sms.dao;

import java.util.List;

import com.bonrix.sms.model.AegeCustDetail;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.LoginLog;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.ServiceType;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;

/**
 * @author Niraj Thakar
 *
 */
public interface UserDao {

	AegeCustDetail findAgentByemailId(String emailId);

	User findByUserName(String username);

	Long saveRegUser(User user);

	SmsQueue saveSms(SmsQueue smsQueue);

	void saveSmsApi(SmsApi smsApi);

	String getQueueCount(long userid);

	String getSentSms(long userid);

	String getSentSmsToday(long userid, String date);

	void updateUser(User user);

	List<Tamplate> getTemplateByuser(long userid);

	ServiceType getServiceTypeById(Long senderId);

	ServiceType getServiceTypeByName(String senderName);

	void saveApiKey(ApiKey apiKey);

	ApiKey gettAPiByUid(long uid, String key, boolean ishidden);

	void deteleApiKey(String aid);

	List getSmsHistory(int page, int listSize);

	String findHttpGateway(long uid);

	void deleteHttpGateway(Long aid);

	List<String> getContactByGid(long parseLong);

	void saveEmailTemplate(EmailTemplate emailTemplate);

	void saveMessageTemplate(MessageTemplate messageTemplate);

	void updateSmsQue(List<SmsQueue> nonDndNOs);

	void saveLoginLog(LoginLog lg);

	public void addUserRole(UserRole role);
	
	public ApiKey gettAPiByUidForNotification(int tcallerid, String api, boolean b);
	
    public List<Object[]> leadByLeadId(int leadid);
	
	public List<Object[]> getSMSTempByLeadStat(String leadStat);
	
	public List<Object[]> CheckSMSStat(String uid);
	
	public List<Object[]> getExtraParam(int leadId);
	
	public int updateExtraParams(String paramJson,int leadId);
	
	public List<Object[]> getAudioFilesPath(int teleId, String stDate, String endDate,int compId);

}

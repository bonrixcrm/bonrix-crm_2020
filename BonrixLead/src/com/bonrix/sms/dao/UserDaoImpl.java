package com.bonrix.sms.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.AegeCustDetail;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.DatatableJsonObject;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.LoginLog;
import com.bonrix.sms.model.ServiceType;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;
import com.google.gson.Gson;

/**
 * @author Niraj Thakar
 *
 */
@Repository("UserDao")
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {

		List<User> users = new ArrayList<User>();

		users = sessionFactory.getCurrentSession().createQuery("from User where username=?").setParameter(0, username)
				.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

	@Override
	public Long saveRegUser(User user) {

		return (Long) sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public SmsQueue saveSms(SmsQueue sq) {
		sessionFactory.getCurrentSession().save(sq);
		return sq;
	}

	@Override
	public void saveSmsApi(SmsApi smsApi) {
		sessionFactory.getCurrentSession().save(smsApi);
	}

	@Override
	public String getQueueCount(long userid) {

		List query = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"SELECT COUNT(*) AS cnt,st.type FROM smsqueue sq,servicetype st WHERE st.stid=sq.stid AND sq.uid="
								+ userid + " GROUP BY sq.stid")
				.list();
		return new Gson().toJson(query);
	}

	@Override
	public String getSentSms(long userid) {
		List query = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"SELECT COUNT(*) AS cnt,st.type FROM smshistory sq,servicetype st WHERE st.stid=sq.stid AND sq.uid="
								+ userid + " GROUP BY sq.stid")
				.list();
		return new Gson().toJson(query);
	}

	@Override
	public String getSentSmsToday(long userid, String date) {

		List query = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"SELECT COUNT(*) AS cnt,st.type FROM smshistory sq,servicetype st WHERE st.stid=sq.stid AND sq.uid="
								+ userid + " and DATE(sentdatetime)=DATE('" + date + "')  GROUP BY sq.stid")
				.list();
		return new Gson().toJson(query);
	}

	@Override
	public void updateUser(User user) {
		sessionFactory.getCurrentSession().update(user);

	}

	@Override
	public List<Tamplate> getTemplateByuser(long userid) {
		List<Tamplate> tamplates = new ArrayList<Tamplate>();
		tamplates = sessionFactory.getCurrentSession().createCriteria(Tamplate.class)
				.add(Restrictions.eq("uid", userid)).list();

		return tamplates;
	}

	@Override
	public ServiceType getServiceTypeById(Long senderId) {
		ServiceType serviceType = (ServiceType) sessionFactory.getCurrentSession().get(ServiceType.class, senderId);
		return serviceType;
	}

	@Override
	public ServiceType getServiceTypeByName(String senderName) {

		ServiceType serviceType = null;
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ServiceType.class)
				.add(Restrictions.eq("type", senderName));
		serviceType = (ServiceType) criteria.uniqueResult();
		return serviceType;
	}

	@Override
	public void saveApiKey(ApiKey apiKey) {
		//ApiKey api=(ApiKey) sessionFactory.getCurrentSession().get(ApiKey.class, apiKey.getUid());
		ApiKey api = (ApiKey) sessionFactory.getCurrentSession().get(ApiKey.class, apiKey.getUid());
		//System.out.println(api.getUid()+" API IS "+api.getKeyValue());
		System.out.println("UID "+apiKey.getUid());
		
/*		sessionFactory.getCurrentSession().close();
		sessionFactory.getCurrentSession().beginTransaction();*/
		System.out.println(api);
		if(api==null)
		sessionFactory.getCurrentSession().save(apiKey);
		else
		{
			String hql = "UPDATE ApiKey set keyValue = :kval , accessDate = :adate "  + 
		             "WHERE uid = :tc_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("kval", apiKey.getKeyValue());
		query.setParameter("tc_id", apiKey.getUid());
		query.setParameter("adate", new Date());
	
		int result = query.executeUpdate();
		System.out.println("Rows affected: " + result);
		}
	}

	@Override
	public ApiKey gettAPiByUid(long uid, String key, boolean ishidden) {

		ApiKey apiKey = new ApiKey();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ApiKey.class)
				.add(Restrictions.eq("uid", uid)).add(Restrictions.eq("keyValue", key))
				.add(Restrictions.eq("ishidden", ishidden));
		apiKey = (ApiKey) criteria.uniqueResult();
		
		if(apiKey!=null)
		{
			
		String hql = "UPDATE ApiKey set accessDate = :adate WHERE uid = :tc_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("tc_id", apiKey.getUid());
		query.setParameter("adate", new Date());
		int result = query.executeUpdate();
		System.out.println("Rows affected update : " + result);
		}
		System.out.println("apiKey_update : " + apiKey);

		return apiKey;
	}

	@Override
	public void deteleApiKey(String aid) {

		ApiKey apiKey = new ApiKey();
		apiKey = (ApiKey) sessionFactory.getCurrentSession().get(ApiKey.class, Long.parseLong(aid));
		sessionFactory.getCurrentSession().delete(apiKey);
	}

	@Override
	public List getSmsHistory(int page, int listSize) {

		/*
		 * System.out.println(
		 * "SELECT mobileNumber,message FROM smshistory LIMIT " +((page-1)*10)+
		 * " , "+listSize);
		 */

		/*
		 * List query = sessionFactory .getCurrentSession() .createSQLQuery(
		 * "SELECT mobileNumber,message  FROM smshistory LIMIT " +((page-1)*10)+
		 * " , "+listSize).list();
		 */

		// List list = ;
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from smshistory")
				.setMaxResults(listSize).setFirstResult(((page - 1) * listSize));
		// list=query.list();

		return query.list();

	}

	@Override
	public String findHttpGateway(long uid) {

		Gson gson = new Gson();
		List outlst = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"SELECT s.apiid,u.username,s.apitext,s.priority,s.successmsg,st.type,s.isfailover FROM smsapi s,users u,servicetype st WHERE u.uid=s.uid AND s.serviceid=st.stid AND s.isfailover="
								+ uid)
				.list();

		DatatableJsonObject personJsonObject = new DatatableJsonObject();
		personJsonObject.setRecordsFiltered(outlst.size());
		personJsonObject.setRecordsTotal(outlst.size());
		personJsonObject.setData(outlst);
		return gson.toJson(personJsonObject);
	}

	@Override
	public void deleteHttpGateway(Long aid) {
		SmsApi apiKey = new SmsApi();
		apiKey = (SmsApi) sessionFactory.getCurrentSession().get(SmsApi.class, aid);
		sessionFactory.getCurrentSession().delete(apiKey);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getContactByGid(long number) {

		List<String> list = new ArrayList<String>();
		list = sessionFactory.getCurrentSession().createSQLQuery("select number from contacts where gid=" + number)
				.list();
		return list;
	}

	@Override
	public void updateSmsQue(List<SmsQueue> nonDndNOs) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		for (int i = 0; i < nonDndNOs.size(); i++) {
			session.update(nonDndNOs.get(i));
			if (i % 100 == 0) { // 20, same as the JDBC batch size
				// flush a batch of inserts and release memory:
				session.flush();
				session.clear();
			}
		}
		tx.commit();
		session.close();
	}

	@Override

	public void saveEmailTemplate(EmailTemplate emailTemplate) {
		sessionFactory.getCurrentSession().save(emailTemplate);

	}

	@Override
	public void saveMessageTemplate(MessageTemplate messageTemplate) {
		sessionFactory.getCurrentSession().save(messageTemplate);
	}

	public void saveLoginLog(LoginLog lg) {

		sessionFactory.getCurrentSession().save(lg);

	}

	@Override
	public void addUserRole(UserRole role) {
		// TODO Auto-generated method stub
		// return (Long) sessionFactory.getCurrentSession().save(user);
		sessionFactory.getCurrentSession().save(role);

	}

	@SuppressWarnings("unchecked")
	@Override
	public AegeCustDetail findAgentByemailId(String emailId) {
		// TODO Auto-generated method stub
		List<AegeCustDetail> agent = new ArrayList<AegeCustDetail>();

		agent = sessionFactory.getCurrentSession().createQuery("from AegeCustDetail where Email=?")
				.setParameter(0, emailId).list();

		System.out.println("Agents Size : " + agent.size());

		if (agent.size() > 0) {
			return agent.get(0);
		} else {
			return null;
		}
	}

	@Override
	public ApiKey gettAPiByUidForNotification(int tcallerid, String api, boolean b) {
		// TODO Auto-generated method stub
		ApiKey apiKey = new ApiKey();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ApiKey.class)
				.add(Restrictions.eq("uid",(long)(int) tcallerid)).add(Restrictions.eq("keyValue", (api)));
		apiKey = (ApiKey) criteria.uniqueResult();
		return apiKey;
	}
	
	@Override
	public List<Object[]> leadByLeadId(int leadid) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM lead WHERE leaadId="+leadid+" ").list();
	}

	@Override
	public List<Object[]> getSMSTempByLeadStat(String leadStat) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM smstemplate WHERE temp_Name='"+leadStat+"' ").list();
	}

	@Override
	public List<Object[]> CheckSMSStat(String uid) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM systempara WHERE uid='"+uid+"' AND isActive=TRUE").list();
	}

	@Override
	public List<Object[]> getExtraParam(int leadId) {
		return sessionFactory.getCurrentSession().createSQLQuery(" SELECT leaadId,csvData FROM lead WHERE leaadId="+leadId+" ").list();
	}

	@Override
	public int updateExtraParams(String paramJson,int leadId) {
		String hql2 = "UPDATE lead SET csvData='"+paramJson+"' WHERE leaadId="+ leadId+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Object[]> getAudioFilesPath(int teleId, String stDate, String endDate,int compId) {
		String query;
		if(teleId==0) {
			query = "SELECT followupsId,audoiFilePath FROM followupshistory WHERE companyId="+compId+" AND callingTime BETWEEN '"+stDate+"' AND '"+endDate+"' AND audoiFilePath!='NA' " ;
		}else {
			query ="SELECT followupsId,audoiFilePath FROM followupshistory WHERE tallycallerId="+teleId+" AND callingTime BETWEEN '"+stDate+"' AND '"+endDate+"' AND audoiFilePath!='NA' " ;
		}
		System.out.println("Audio_Query ::::"+query);
		return sessionFactory.getCurrentSession().createSQLQuery(query).list();
	}
	

}

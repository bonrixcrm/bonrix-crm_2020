package com.bonrix.sms.dao;

import java.util.List;

import com.bonrix.sms.model.WhatsAppSettings;
import com.bonrix.sms.model.WhatsAppTemplate;
import com.bonrix.sms.model.WhatsappMedia;

public interface WhatsAppDao { 
	
	public  void SaveWhatsAppSetting( WhatsAppSettings setting);
	  
	List GetWhatsAppSettingsByUserId(long userid); 
	
	public List  getWhatsAppTemplateByuser(long userid);
	
	void saveWhatsAppTemplate(WhatsAppTemplate template);
	
	WhatsAppTemplate GetWhatsAppTemplate(long templateId);
	
	void deleteTemplate(long templateId);
	
	void updateTemplateMedia(long templateId,String mediaUrl);
	
	WhatsAppTemplate GetWhatsAppTemplateByName(long template_id);
	
	void deleteWhatsAppSettings(long settingId);
	
	

}

package com.bonrix.sms.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.SmsCredit;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.WhatsAppSettings;
import com.bonrix.sms.model.WhatsAppTemplate;
import com.bonrix.sms.model.WhatsappMedia;

@Repository("WhatsAppDao")
public class WhatsAppDaoImpl implements WhatsAppDao {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public void SaveWhatsAppSetting( WhatsAppSettings setting) {
		 sessionFactory.getCurrentSession().save(setting);
	}
  
	@Override
	public List GetWhatsAppSettingsByUserId(long userid) {
		String q = "SELECT id,whatsapp_number,token,phone_number_id,whatsapp_business_account_id,media_url FROM crmbonrix.whatsappsettings WHERE user_id="+userid+"";
		return sessionFactory.getCurrentSession().createSQLQuery(q).list();
	}
	
	@Override
	public List  getWhatsAppTemplateByuser(long userid) {
		String q = "SELECT template_id,template_name,template_body,template_status,userid,media_template_type,header,media_url FROM whatsapptemplate WHERE userid="+userid+"";
		return sessionFactory.getCurrentSession().createSQLQuery(q).list();
	}
 
	@Override
	public void saveWhatsAppTemplate(WhatsAppTemplate template) {
		sessionFactory.getCurrentSession().saveOrUpdate(template);
		
	}

	@Override
	public WhatsAppTemplate GetWhatsAppTemplate(long templateId) {
		WhatsAppTemplate template = null;
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WhatsAppTemplate.class)
				.add(Restrictions.eq("template_id", templateId));
		template = (WhatsAppTemplate) criteria.uniqueResult();
		return template;
	}

	@Override
	public void deleteTemplate(long templateId) {
		
		WhatsAppTemplate template = new WhatsAppTemplate();
		template = (WhatsAppTemplate) sessionFactory.getCurrentSession().get(WhatsAppTemplate.class, templateId);
		if(template!=null)
		sessionFactory.getCurrentSession().delete(template);
		
	} 

	@Override
	public void updateTemplateMedia(long templateId, String mediaUrl) {
		WhatsAppTemplate template = new WhatsAppTemplate();
		template = (WhatsAppTemplate) sessionFactory.getCurrentSession().get(WhatsAppTemplate.class, templateId);
		template.setMedia_url(mediaUrl);
		sessionFactory.getCurrentSession().update(template);
		
	}

	@Override
	public WhatsAppTemplate GetWhatsAppTemplateByName(long template_id) {
		WhatsAppTemplate template = null;
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WhatsAppTemplate.class)
				.add(Restrictions.eq("template_id", template_id));
		template = (WhatsAppTemplate) criteria.uniqueResult();
		return template;
	}

	@Override
	public void deleteWhatsAppSettings(long settingId) {
		sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM `whatsappsettings` WHERE id=" + settingId).executeUpdate();
	}

	

}

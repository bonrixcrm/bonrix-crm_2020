package com.bonrix.sms.dao;

import java.util.List;

import com.bonrix.sms.model.WhatsappMedia;

public interface WhatsAppMediaDao {

	void saveWhatsAppMedia(WhatsappMedia media);
	 
	List<WhatsappMedia> getWhatsAppMedia(long id);
	
	public WhatsappMedia getWhatsAppMediaTemplate(long id);
}

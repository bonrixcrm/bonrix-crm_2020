package com.bonrix.sms.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.WhatsappMedia;

@Repository("WhatsAppMediaDao")
public class WhatsAppMediaDaoImpl implements WhatsAppMediaDao {

	@Autowired
	SessionFactory sessionFactory; 
	@Override
	public void saveWhatsAppMedia(WhatsappMedia media) {
		sessionFactory.getCurrentSession().save(media);
		
	}
	@Override
	public List<WhatsappMedia> getWhatsAppMedia(long id) {
		 List<WhatsappMedia> smsCredit = new ArrayList<>();
		    smsCredit = this.sessionFactory.getCurrentSession().createQuery("from WhatsappMedia where template_id=" + id).list();
		    return smsCredit;
		
	}
	
	@Override
	public WhatsappMedia getWhatsAppMediaTemplate(long id) {
		WhatsappMedia smsCredit = new WhatsappMedia();
		    smsCredit = (WhatsappMedia)this.sessionFactory.getCurrentSession()
		      .createQuery("from WhatsappMedia where id=" + id ).uniqueResult();
		    return smsCredit;
		
	}
}

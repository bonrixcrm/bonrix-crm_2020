package com.bonrix.sms.dao;

import java.util.Date;
import java.util.List;

import com.bonrix.sms.model.WhatsappOutbox;

public interface WhatsappOutboxDao {

	void saveWhatsappOutbox(WhatsappOutbox outboxLog);

	public List<WhatsappOutbox> getWhatsappOutbox(Long companyId, Date sendtime);
	
	public WhatsappOutbox getMessage(String id);
 
	public void updateStatus(WhatsappOutbox outboxLog);
	
	public List<WhatsappOutbox> getTelecallerWhatsappOutbox(Long telecallerId, Date sendtime);
}

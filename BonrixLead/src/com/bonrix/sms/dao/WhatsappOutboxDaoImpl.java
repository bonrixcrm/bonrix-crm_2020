package com.bonrix.sms.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bonrix.sms.model.WhatsappOutbox;

@Repository("WhatsappOutboxDao")
public class WhatsappOutboxDaoImpl implements WhatsappOutboxDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void saveWhatsappOutbox(WhatsappOutbox outboxLog) {
		sessionFactory.getCurrentSession().save(outboxLog);
	}

	@Override
	public List<WhatsappOutbox> getWhatsappOutbox(Long companyId, Date sendtime) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WhatsappOutbox.class)
				.add(Restrictions.eq("companyId", companyId)).add(Restrictions.gt("sendTime", sendtime));
		return criteria.list();
	}

	@Override
	public WhatsappOutbox getMessage(String id) {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WhatsappOutbox.class)
					.add(Restrictions.eq("msgId", id)).setCacheable(true);
			WhatsappOutbox otbox = (WhatsappOutbox) criteria.uniqueResult();
			return otbox;
		}

	@Override
	public void updateStatus(WhatsappOutbox outboxLog) {
		sessionFactory.getCurrentSession().update(outboxLog);
	}

	@Override
	public List<WhatsappOutbox> getTelecallerWhatsappOutbox(Long telecallerId, Date sendtime) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(WhatsappOutbox.class)
				.add(Restrictions.eq("telecallerId", telecallerId)).add(Restrictions.gt("sendTime", sendtime));
		return criteria.list();
	}
	
}

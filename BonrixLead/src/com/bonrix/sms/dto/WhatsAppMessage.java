package com.bonrix.sms.dto;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class WhatsAppMessage {

    @JsonProperty("messaging_product")
    private String messagingProduct;

    @JsonProperty("contacts")
    private List<Contact> contacts;

    @JsonProperty("messages")
    private List<Message> messages;

     

    public String getMessagingProduct() {
		return messagingProduct;
	}

	public void setMessagingProduct(String messagingProduct) {
		this.messagingProduct = messagingProduct;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public static class Contact {

        @JsonProperty("input")
        private String input;

        @JsonProperty("wa_id")
        private String waId;

		public String getInput() {
			return input;
		}

		public void setInput(String input) {
			this.input = input;
		}

		public String getWaId() {
			return waId;
		}

		public void setWaId(String waId) {
			this.waId = waId;
		}

        
    }

    public static class Message {

        @JsonProperty("id")
        private String id;

        @JsonProperty("message_status")
        private String messageStatus;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getMessageStatus() {
			return messageStatus;
		}

		public void setMessageStatus(String messageStatus) {
			this.messageStatus = messageStatus;
		}

         
    }
}

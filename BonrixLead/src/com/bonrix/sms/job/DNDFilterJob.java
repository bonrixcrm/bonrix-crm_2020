package com.bonrix.sms.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bonrix.sms.model.SmsHistory;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.service.SMSService;
import com.bonrix.sms.service.UserService;
import com.bonrix.sms.utils.StringUtils;

public class DNDFilterJob extends Thread {

	private SMSService smsservice;
	private UserService userservice;

	public DNDFilterJob(SMSService sc, UserService uc) {
		smsservice = sc;
		userservice = uc;
	}

	public void run() {
		while (true) {
			List<SmsQueue> smslst2 = smsservice.getSmsQueue(125, 0);
			if (!smslst2.isEmpty()) {
				try {
					System.out.println("Checking DND1 Before size::" + smslst2.size() + "::ON" + new Date());
					List<SmsQueue> newsmslst = checkDndNos(smslst2);
					System.out.println("Checking DND1 After size::" + newsmslst.size() + "::ON" + new Date());
				} catch (Exception e) {
					System.out.println("Some ERROR::" + e.getMessage());

					try {
						Thread.sleep(2000);
					} catch (InterruptedException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			} else {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	public List<SmsQueue> checkDndNos(List<SmsQueue> list) {
		StringBuilder number = new StringBuilder();
		Map<String, SmsQueue> sq = new HashMap<String, SmsQueue>();

		for (int i = 0; i < list.size(); i++) {
			number.append(list.get(i).getNumber());
			if (i != list.size() - 1) {
				number.append(",");
			}
			sq.put(list.get(i).getNumber(), list.get(i));

		}

		List<Long> newList = smsservice.checkDndNumbers(number.toString());

		/* System.out.println("******" + newList.size()); */

		List<SmsQueue> nonDndNOs = new ArrayList<SmsQueue>();
		if (newList.isEmpty()) {
			for (int i = 0; i < list.size(); i++) {
				SmsQueue sqq = list.get(i);
				sqq.setDndChecked(true);
				nonDndNOs.add(sqq);
			}
			userservice.updateSmsQue(nonDndNOs);
		} else {
			for (int i = 0; i < newList.size(); i++) {

				int k = 0;

				SmsQueue sqq = sq.get(newList.get(i) + "");
				smsservice.deletesmsq(sqq.getQid());
				boolean isUnicode = StringUtils.isUnicode(sqq.getMessage());
				int crdeduct = sqq.getMsgcredit();

				crdeduct = -crdeduct;
				int crdup = smsservice.creditUpdate(crdeduct, sqq.getUid(), sqq.getStid());

				// boolean retval2 =
				// newList.contains(Long.valueOf(list.get(i).getNumber()));
				/* System.out.println("p - - --------" + sq.size()); */

				Long l = new Long(list.get(i).getNumber());

				SmsQueue sms = new SmsQueue();
				SmsHistory sh = new SmsHistory();
				sms = sq.get(newList.get(i) + "");
				// SenderName sn = smsService.getSenderName(sms.getSenderid());
				sh.setApiId(1);
				sh.setClientIp("");
				sh.setMessage(sms.getMessage());
				sh.setMobileNumber(sms.getNumber());
				sh.setUserId(sms.getUid());
				sh.setStid(sms.getStid());
				sh.setSentDateTime(new Date());
				sh.setSubmitDateTime(sms.getCreatDate());
				sh.setBunchId(sms.getCampid());
				sh.setSenderId(sms.getSenderid());
				sh.setDlrStatus("REJECTED");
				sh.setTrxId(sms.getQid());
				sh.setStatus("DND");
				sh.setMsgcount(sms.getMsgcredit());
				smsservice.saveSmsHistory(sh);

				// System.out.println(sq.containsKey(newList.get(i) + ""));

				sq.remove(newList.get(i) + "");

			}

			for (Map.Entry<String, SmsQueue> entry : sq.entrySet()) {
				SmsQueue q = new SmsQueue();
				q = entry.getValue();

				nonDndNOs.add(q);
				q.setDndChecked(true);

				// Do things with the list
			}
			if (nonDndNOs.size() >= 1) {
				userservice.updateSmsQue(nonDndNOs);
			}
		}
		return nonDndNOs;

	}
}

package com.bonrix.sms.job;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessagePreparator;

import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.HostSetting;

public class EmailSenderThread implements Runnable {

	private EmailTemplate et;
	private HostSetting hostSetting;
	private String email;

	public EmailSenderThread(EmailTemplate etpl, HostSetting hs, String em) {
		et = etpl;
		hostSetting = hs;
		email = em;
	}

	@Override
	public void run() {
		try {
			JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

			javaMailSender.setHost(hostSetting.getHostName());
			javaMailSender.setUsername(hostSetting.getUsername());
			javaMailSender.setPassword(hostSetting.getPassword());
			javaMailSender.setJavaMailProperties(getMailProperties());

			System.out.println("in MailSendController " + email + "::" + et.getMessage());

			MimeMessagePreparator preparator = new MimeMessagePreparator() {

				public void prepare(MimeMessage mimeMessage) throws Exception {

					mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
					mimeMessage.setFrom(new InternetAddress(hostSetting.getFrom()));
					mimeMessage.setContent(et.getMessage(), "text/html");

					mimeMessage.setSubject(et.getSubject());

				}
			};

			javaMailSender.send(preparator);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Properties getMailProperties() {
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		/* properties.setProperty("mail.debug", "true"); */
		properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
		properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.setProperty("templateMode", "HTML5l");
		/* mimeMessage.setContent(htmlMsg, "text/html"); */
		properties.setProperty("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");

		return properties;
	}

}

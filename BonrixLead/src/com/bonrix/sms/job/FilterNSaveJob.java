package com.bonrix.sms.job;

import java.util.Collections;
import java.util.List;

import com.bonrix.sms.model.MediUser;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.User;
import com.bonrix.sms.service.SMSService;

public class FilterNSaveJob {

	public int filterNSave(List<SmsQueue> lstq, SMSService sc, MediUser usr) {

		int total = lstq.size();
		int totalcut = 0;
		if (usr.getMinnumber() != 0 && usr.getMinnumber() < total) {
			List<String> white = sc.getWhiteList(1);
			totalcut = (int) (total * usr.getCutting() * 0.01);
			for (int i = 0; i < totalcut; i++) {
				Collections.shuffle(lstq); // Shuffle the list.
				SmsQueue sms = lstq.remove(0);
				if (!white.contains(sms.getNumber())) {
					sms.setIscutted(true);
				} else {
					sms.setIscutted(false);
				}
				lstq.add(sms);
			}
		}
		sc.saveSmsQueue(lstq);
		return total - totalcut;
	}

	public int filterNSave(List<SmsQueue> lstq, SMSService sc, User usr) {

		int total = lstq.size();
		int totalcut = 0;
		if (usr.getMinNumber() != 0 && usr.getMinNumber() < total) {
			List<String> white = sc.getWhiteList(1);
			totalcut = (int) (total * usr.getCutting() * 0.01);
			for (int i = 0; i < totalcut; i++) {
				Collections.shuffle(lstq); // Shuffle the list.
				SmsQueue sms = lstq.remove(0);
				if (!white.contains(sms.getNumber())) {
					sms.setIscutted(true);
				} else {
					sms.setIscutted(false);
				}
				lstq.add(sms);
			}
		}
		sc.saveSmsQueue(lstq);
		return total - totalcut;
	}
}

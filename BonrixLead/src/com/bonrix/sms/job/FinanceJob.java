package com.bonrix.sms.job;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bonrix.sms.controller.FinanceCRMEmailController;
import com.bonrix.sms.dao.AdminDao;
import com.bonrix.sms.model.AndroidLog;
import com.bonrix.sms.model.Sentsmslog;
import com.bonrix.sms.model.SystemParameter;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.service.SMSService;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;  

/**
 * @author Sajan
 *
 */
@Component
public class FinanceJob  {

	@Autowired
	AdminService adminService;

	@Autowired
	AdminDao dao;
	
	@Autowired
	CommonService cservice;
	
	@Autowired
	FinanceCRMEmailController fEmailCtrl;
	
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
	   LocalDateTime now = LocalDateTime.now();  
	  // System.out.println(dtf.format(now));  
	   
	//@Scheduled(fixedRate = 5000)
	
	// @Scheduled(cron = "0/20 * * * * ?")
	/*@Scheduled(cron="0 48 2 * * *",zone = "Indian/Maldives")
	public void sajan() {
		List<SystemParameter> paramList=cservice.getSysParam();
		
		for(SystemParameter param :paramList)
		{
			if(param.getUid()==263 && param.getIsactive()==1)
			{
				System.out.println("Hello Sahjan");
			}
		}
		
	}*/

	//@Scheduled(cron="0 0 12 * * ?",zone = "Indian/Maldives")
	//@Scheduled(cron="0 15,30,45 * ? * *",zone = "Indian/Maldives")
	/*@Scheduled(cron="0 0 12 * * ?",zone = "Indian/Maldives")
	public void EveryDayEmailschedule() {
		EveryDayEmailscheduleImp();
	}
	*/
	//@Scheduled(cron="0 0 8 */1 * *",zone = "Indian/Maldives")
	/*public void schedule1() {
	   sajan();
	}*/

	//@Scheduled(cron="0 10 15 */1 * *",zone = "Indian/Maldives")
	/*public void schedule2() {
	   sajan();
	}*/
	
	/*@Scheduled(cron="* * * ? * *",zone = "Indian/Maldives")
	public void schedule3() {
	   sajan();
	}*/
	
	//Every Minute
	//@Scheduled(cron="0 * * ? * *",zone = "Indian/Maldives")
	
	//9 AM daily
	//@Scheduled(cron="0 0 9 * * ?",zone = "Indian/Maldives")
	//@Scheduled(cron="0 30 13 1/1 * ?",zone = "Indian/Maldives")
	 //  @Scheduled(cron="0 * * ? * *",zone = "Indian/Maldives")
	  // @Scheduled(cron="0 45 15 * * ?",zone = "Indian/Maldives")
	   @Scheduled(cron="0 0 9 * * ?",zone = "Asia/Kolkata")
	/*public void schedule4() {
		System.out.println("SendFinanceCRMEmail Scheduler is called..."+new Date().toString());
		SendFinanceCRMEmailController();
	}*/
	
	/*@Scheduled(cron="0 0 5 ? * * ",zone = "Indian/Maldives")
	public void schedule5() {
		generateScheduleLeadTeminder();
	}*/

	/*public void sajan() {

		//System.out.println("Hello Sahjan");
	}*/
	
	@Scheduled(cron="0 */2 * ? * *",zone = "Indian/Maldives")
	public void sendScheduleLeadTeminder() {
		generateScheduleLeadTeminder();
	}
	
	/*public void EveryDayEmailscheduleImp()
	{
		//System.out.println("Hello Sahjan 12 PM Schedule :: "+dtf.format(now));
	}*/
	
	public void SendFinanceCRMEmailController()
	{
		System.out.println("Hello SendFinanceCRMEmailController :: "+dtf.format(now));
		try {
			fEmailCtrl.sendScheduleWiseLeadToFinanceMarket();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void generateScheduleLeadTeminder()
	{
		List<SystemParameter> paramList=cservice.getSysParam();
		for(SystemParameter param :paramList)
		{
			if(param.getUid()==263 && param.getIsactive()==1)
			{
				System.out.println("Hello Sahjan");
		List list=adminService.getGetScheduleLeadScheduler(263, -5);
		System.out.println("Every Minute :: "+" :: "+dtf.format(now)+" :: "+list.size());
		for (int i = 0; i < list.size(); i++) {
			Object[] result = (Object[]) list.get(i);
			System.out.println(result[1].toString()+"  :: "+result[2].toString()+"  :: "+result[3].toString());
			String msg="Reminder To Call <name> <mobile>".replace("<name>",result[1].toString()).replace("<mobile>",result[2].toString());
		System.out.println(msg);
		final Sentsmslog log = new Sentsmslog();
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		asyncHttpClient.prepareGet("http://websms.highspeedsms.com/sendSMS?username=finance&message="+msg+"&sendername=FINMKT&smstype=TRANS&numbers="+result[3].toString()+"&apikey=72c49c01-bbf9-4a3d-94d1-0201124bea86" + 
				"").execute(new AsyncCompletionHandler<Response>() {
			@Override
			public Response onCompleted(Response responce) throws Exception {
				
				log.setTallycallerId(0);
				log.setStaffId(0);
				log.setCompayId(0);
				log.setMobileNo(new Long(result[2].toString()));
				log.setMessage(msg);
				log.setSendDate(new Date());
				String ststus = "";
				String res = responce.getResponseBody();
				if (!res.contains("invalidnumber") && !res.contains("INVALID_USER") && !res.contains("INVALID_KEY")) {
					log.setMSGStatus("SUCCESS");
				} else {
					log.setMSGStatus("FAIL");
				}

				try {
					log.setAPIResponse(res);
					adminService.saveLog(log);

				} catch (Exception e) {
					 System.out.println("Save Error : " + e);
				}
				return responce;
			}

			@Override
			public void onThrowable(Throwable t) {
				// Something wrong happened.
				t.printStackTrace();
				log.setAPIResponse(t.getMessage().substring(0, 100).trim());
				log.setMSGStatus("FAIL");
				adminService.saveLog(log);
			}

		});
		}
		}
	}
	}
}

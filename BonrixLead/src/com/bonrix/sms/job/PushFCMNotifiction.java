package com.bonrix.sms.job;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.sun.istack.internal.logging.Logger;



public class PushFCMNotifiction {
	public final static String AUTH_KEY_FCM = "AIzaSyBk4I9Envhr0GZq1rCStKwwT0jVDtTJnjs";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
	public static final Logger log = Logger.getLogger(PushFCMNotifiction.class);

	public static String sendPushNotification(String[] deviceToken,int leadID,String fullName,String mobile)throws IOException, JSONException {
	    URL url = new URL(API_URL_FCM);
	    
	    log.info("deviceToken_length::: "+deviceToken.length);
	    
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setUseCaches(false);
	    conn.setDoInput(true);
	    conn.setDoOutput(true);
	    conn.setRequestMethod("POST");
	    conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM);
	    conn.setRequestProperty("Content-Type", "application/json");
	    JSONObject json = new JSONObject();
	    json.put("registration_ids",  deviceToken);
	    
	    JSONObject info = new JSONObject();
	   
	 	info.put("title",  URLEncoder.encode("UpComing Call Notification","UTF-8")); // Notification title
	    info.put("message", URLEncoder.encode("Your Next Call is "+" "+fullName+" "+(mobile)+" "+" After 15 Mins","UTF-8"));
	    info.put("timestamp",new Date().getTime()+"");
	    info.put("leadID", String.valueOf(leadID));
	    json.put("data", info);   // body
	    //json.put("timestamp",new Date());
	    JSONObject payload = new JSONObject();
	    json.put("payload", payload);
	   log.info("NotifictionHashmap::: SENDJSON::: "+json.toString());
	   
	    try {
	        OutputStreamWriter wr = new OutputStreamWriter(
	                conn.getOutputStream());
	        wr.write(json.toString());
	        wr.flush();
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));
	        String output;
	       log.info("NotifictionHashmap::: Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	          log.info("NotifictionHashmap::: FCMResponce::: "+output);
	            if (output != null) {
	                    JSONObject jsonObj = new JSONObject(output);
	                   log.info("SucessfailLOGS:: "+jsonObj.get("success")+"   "+jsonObj.get("failure"));
	                    org.json.JSONArray jarry=jsonObj.getJSONArray("results");
	                    for (int i = 0; i < jarry.length(); i++) 
	                    {
							if(jarry.getJSONObject(i).opt("error")!=null)
							{
							log.info("NotifictionHashmap::: "+jarry.getJSONObject(i).get("error"));
								//new GCMclientDAO().delete(deviceToken[i]);
								
							}
						}
	                 
	            }
	        }
	       log.info("NotifictionHashmap::: FCM Notification is sent successfully");
	        return "FCM Notification is sent successfully";
	    } catch (Exception e) {
	    	 log.info("PushFCMNotifiction "+e.getMessage());
	        e.printStackTrace();
	        return "error";
	    }
	   
	}
	
	
	
	public static String sendElapsePushNotification(String[] deviceToken,int leadID,String fullname_elapse,String mobile_elapse)throws IOException, JSONException {
	    URL url = new URL(API_URL_FCM);
	    
	    log.info("deviceToken_length::: "+deviceToken.length);
	    
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setUseCaches(false);
	    conn.setDoInput(true);
	    conn.setDoOutput(true);
	    conn.setRequestMethod("POST");
	    conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM);
	    conn.setRequestProperty("Content-Type", "application/json");
	    JSONObject json = new JSONObject();
	    json.put("registration_ids",  deviceToken);
	    
	    JSONObject info = new JSONObject();
	   
	 	info.put("title",  URLEncoder.encode("Elapse Call Notification","UTF-8")); // Notification title
	    info.put("message", URLEncoder.encode("Your Previous Call Was"+" "+fullname_elapse+" "+(mobile_elapse)+" "+ "Before 15 Mins","UTF-8"));
	    info.put("timestamp",new Date().getTime()+"");
	    info.put("leadID", String.valueOf(leadID));
	    json.put("data", info);   // body
	    JSONObject payload = new JSONObject();
	    json.put("payload", payload);
	   log.info("NotifictionHashmap::: SENDJSON::: "+json.toString());
	   
	    try {
	        OutputStreamWriter wr = new OutputStreamWriter(
	                conn.getOutputStream());
	        wr.write(json.toString());
	        wr.flush();
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));
	        String output;
	       log.info("NotifictionHashmap::: Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	          log.info("NotifictionHashmap::: FCMResponce::: "+output);
	            if (output != null) {
	                    JSONObject jsonObj = new JSONObject(output);
	                   log.info("SucessfailLOGS:: "+jsonObj.get("success")+"   "+jsonObj.get("failure"));
	                    org.json.JSONArray jarry=jsonObj.getJSONArray("results");
	                    for (int i = 0; i < jarry.length(); i++) 
	                    {
							if(jarry.getJSONObject(i).opt("error")!=null)
							{
							log.info("NotifictionHashmap::: "+jarry.getJSONObject(i).get("error"));
								//new GCMclientDAO().delete(deviceToken[i]);
								
							}
						}
	                 
	            }
	        }
	       log.info("NotifictionHashmap::: FCM Notification is sent successfully");
	        return "FCM Notification is sent successfully";
	    } catch (Exception e) {
	    	 log.info("PushFCMNotifiction "+e.getMessage());
	        e.printStackTrace();
	        return "error";
	    }
	   
	}
	
	
	
public static void main(String[] args) {
	/*	try {
			sendPushNotification(device_Id,"hii","everyone","http://tracker24.in/images/icon/truck/red_new/truck0.png");
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}*/
	}


}

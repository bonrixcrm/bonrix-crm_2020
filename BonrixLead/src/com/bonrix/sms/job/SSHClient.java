package com.bonrix.sms.job;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bonrix.sms.config.core.ApplicationContextHolder;
import com.bonrix.sms.model.WebSocketObj;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.CommonService;
import com.bonrix.sms.utils.AndroidResponceHashMap;
import com.bonrix.sms.utils.CRMHashMap;


public class SSHClient extends WebSocketServer {

	
	public SSHClient(InetSocketAddress address) {
		super(address);
	}
	private String client;	
	private WebSocket session;	
	private static Logger LOG = LoggerFactory.getLogger("SMPPINFO");
	

	CommonService cservice = ApplicationContextHolder.getContext().getBean(CommonService.class);
	
	
	
	@Override
	public void onOpen(final WebSocket conn, ClientHandshake handshake) {
		try {
			System.out.println("new connection to " + conn.getRemoteSocketAddress());
			LOG.info("NEW Coonection from" + conn.getRemoteSocketAddress());
			this.session = conn;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClose(final WebSocket con, int code, String reason, boolean remote) {
		
		System.out.println("Closing socket and remo ving Client :"+client+"::"+reason+":Code:"+code);
		LOG.info("SOCKET CLOSE:{},Reason:{},remote:{}"+code,reason,remote);
	}

	@Override
	public void onMessage(final WebSocket session, final String data) {

		System.out.println("RECEIVED DATA:"+data);
		LOG.info("RECEIVED DATA:"+data);
		try {
			JSONObject json = new JSONObject(data);
			String type = json.getString("Type");
			
			client = json.getString("TelecallerId").toString();
			
			if (type.equals("PING-PONG")) {
				WebSocketObj wsobj = CRMHashMap.getInstance().connectedClient.get(client);
				if (wsobj.getSkt() != null) {
					session.send(
							"[{\"Type\":\"PING-PONG\",\"Status\":\"PING-PONG  Received  Success\",\"Msg\":\"PONG\"}]");
				} else {

					session.close();
				}
			} else if (type.equals("Authorization")) {

		     String authotp=json.getString("ClientCode").toString();
		     
		     Map<String, WebSocketObj> clint= CRMHashMap.getInstance().connectedClient;
		     
		 	 for(Map.Entry<String, WebSocketObj> entry1 : clint.entrySet())
				{	

					WebSocketObj wskt = entry1.getValue();
		 		 
		 		 if(wskt.getClientCode().equalsIgnoreCase(authotp)){
					
						System.out.println("WEBSOCKET:"+wskt.toString());
					
					WebSocketObj newWobj = new WebSocketObj();
					newWobj.setTcalletId(wskt.getTcalletId());
					newWobj.setClientCode(wskt.getClientCode());
					newWobj.setSkt(session);
					newWobj.setStatus(true);
					
					CRMHashMap.getInstance().AddClient(wskt.getTcalletId(), newWobj);					
					
					sendMessage("" + wskt.getTcalletId(), "Authorization",session);
					
					return;
					}
			
				}

		 	sendMessage("-1", "Authorization",session);
			}

			else if (type.equals("AutoDialRequest_Accepted")) {

				System.out.println("AutoDialRequest_Accepted:" + data);
				
				 AndroidResponceHashMap.getInstance().sendsuccess.put(client,
						 json.getJSONObject("TelecallerReqInfo").toString());
			}

			else if (type.equals("AutoDialDisconnectRequest_Accepted")) {

		AndroidResponceHashMap.getInstance().RemoveClient(client);

		
		
		
	  String fid=  json.getString("FID");
	  
	 JSONObject jobj=  json.getJSONObject("TelecallerReqInfo");
	 
	  int fiid=0;
	  if(fid!=null){
		  fiid=Integer.parseInt(fid);
	  }
	  
	  String strs="UPDATE followupshistory SET callDuration='"+jobj.getString("Callduration")+"',audoiFilePath='"+jobj.getString("Audiofilename")+"',callStatus='"+jobj.getString("Callstatus")+"' WHERE followupsId="+fiid;
	  
	  System.out.println(strs);
		 
	cservice.createupdateSqlQuery(strs);
				
//{"TelecallerReqInfo":{'lastcallno': '7201045500','Callstatus': 'Success','Callduration': '12','Audiofilename': 'bonrix123_7201045500_2017-07-08_16-32-56.3gp'},"TelecallerId":"3","Type":"AutoDialDisconnectRequest_Accepted","FID":"1563"}
		
		
		AndroidResponceHashMap.getInstance().AddClient(client,data);
				 
			}

			else if (type.equals("Logout_Request")) {

				WebSocketObj wskt = CRMHashMap.getInstance().connectedClient.get(client);
				CRMHashMap.getInstance().RemoveClient(client);
				AndroidResponceHashMap.getInstance().RemoveClient(client);
				//CRMHashMap.getInstance().otpauth.remove(authotp);
				wskt.getSkt().close();
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error In Message : " + e.getMessage());
		}

	}

	public void sendMessage(String msg, String msgType,WebSocket wsk) {

		try {
			

			AdminService adminService = ApplicationContextHolder.getContext().getBean(AdminService.class);
			Object[] result = null;
			String sendstr="";
			try {
				if (msgType.equals("Authorization")) {
					if (msg.equals("0")) {
						
						sendstr="[{\"Type\":\"Authorization\",\"Status\":\"Authorization  Fail\",\"TelecallerId\":\"N/A\",\"TelecallerName\":\"N/A\"}]";
						LOG.info("SENDING:"+sendstr);
						wsk.send(sendstr);
						return;
						// session.close();
					} else if (msg.equals("-1")) {
						
						sendstr="[{\"Type\":\"Authorization\",\"Status\":\"Invalid Authorization Code \",\"TelecallerId\":\"N/A\",\"TelecallerName\":\"N/A\"}]";
						LOG.info("SENDING:"+sendstr);
						wsk.send(sendstr);
						return;
						// session.close();
					} else {
						List list = adminService.getTeleCallerByUid(new Long(msg));
						result = (Object[]) list.get(0);
						sendstr="[{\"Type\":\"Authorization\",\"Status\":\"Authorization  Success\",\"TelecallerId\":\""
								+ result[0] + "\",\"TelecallerName\":\"" + result[1] + "\"}]";
						wsk.send(sendstr);
						LOG.info("SENDING:"+sendstr);
						return;
						// session.close();
					}
				}

				else if (msgType.equals("MOBILE_NO_SEND")) {
					sendstr="[{\"Type\":\"MOBILE_NO_SEND\",\"Status\":\"Message Received  Success\",\"TelecallerId\":\""
							+ msg + "\",\"TelecallerName\":\"" + msg + "\"}]";
					LOG.info("SENDING:"+sendstr);
					wsk.send(sendstr);

				}

				else if (msgType.equals("PING-PONG")) {
					sendstr="[{\"Type\":\"PING-PONG\",\"Status\":\"PING-PONG  Received  Fail\",\"Msg\":\"Invalid Telecaller\"}]";
					LOG.info("SENDING:"+sendstr);
					wsk.send(sendstr);
					return;
				} else {
					wsk.send("Invalid Message Type..");
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				LOG.info("SENDING ERROR:"+e.getMessage());
				System.out.println("Error In Send  Message : " + e.getMessage());
			}
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.info("SENDING ERROR:"+e.getMessage());
			System.out.println("Error In Send  Message : " + e.getMessage());
//			try {
//				session.send("Error In Send  Message : " + e.getMessage());
//
//			} catch (Exception e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}

		}

	}

	@Override
	public void onError(final WebSocket conn, Exception ex) {
		LOG.info("SOCKET ERROR:"+ex.getMessage());
	}

	public static void main(String[] args) {
		

		// WebSocketServer server = new WSClient(new InetSocketAddress(host,
		// port));
		// server.run();
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub

	}
}
package com.bonrix.sms.job;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package javax.websocket;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.bonrix.sms.config.core.ApplicationContextHolder;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.RechargeLogMaster;
import com.bonrix.sms.model.RechargeMessage;
import com.bonrix.sms.model.RechargeNotifyMaster;
import com.bonrix.sms.model.User;
import com.bonrix.sms.queue.ConnectedProviderNofityList;
import com.bonrix.sms.queue.ConnectedUserNotifyList;
import com.bonrix.sms.queue.NonConnectedUserNotifyList;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.RechargeGridService;
import com.bonrix.sms.service.UserService;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@ServerEndpoint("/wsprovidernotify")

public class WebSocketProviderNotify {
	String email;
	private String uname;
	String mo_no;
	String client;
	private static final Gson gson = new Gson();
	private static final Type collectionType = new TypeToken<List<RechargeMessage>>() {
	}.getType();
	/*
	 * AdminService calc =
	 * ApplicationContextHolder.getContext().getBean(AdminService.class);
	 * UserService userService =
	 * ApplicationContextHolder.getContext().getBean(UserService.class);
	 */
	AdminService adminservice = ApplicationContextHolder.getContext().getBean(AdminService.class);
	// UserService userService =
	// ApplicationContextHolder.getContext().getBean(UserService.class);
	RechargeGridService rgservice = ApplicationContextHolder.getContext().getBean(RechargeGridService.class);

	@OnError
	public void onError(Session session, Throwable t) {

		System.out.println(t.getMessage());
		System.out.println("Removing client due to Error: " + client);

		ConnectedProviderNofityList.getInstance().RemoveClient(client);
	}

	@OnMessage
	public void onMessage(String message, final Session session) {
		System.out.println(message);

		// Type listType = new TypeToken<List<RechargeMessage>>(){}.getType();

		List<RechargeMessage> rml = gson.fromJson(message, collectionType);

		RechargeMessage rm = rml.get(0);
		System.out.println("FORM:" + rm.getFrom());
		System.out.println("MESSAGE:" + rm.getMessage());
		System.out.println("TO:" + rm.getTo());
		System.out.println("TYPE:" + rm.getType());

		final String msg[] = rm.getMessage().split(",");
		if (rm.getType().equalsIgnoreCase("LOGIN")) {

			String[] auth = rm.getMessage().split(",");

			User userObj = adminservice.getUserByUserName(auth[0]);
			if (userObj != null) {
				ApiKey apiKey = rgservice.gettAPiByUid(userObj.getUid(), auth[1]);
				if (apiKey != null) {
					try {
						uname = msg[1];
						rm.setMessage("LOGIN_SUCCESS");
						rm.setType("LOGIN_RESPONSE");
						session.getBasicRemote().sendText(gson.toJson(rm));

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					client = msg[1];
					ConnectedProviderNofityList.getInstance().AddClient(msg[0], session);

				} else {
					try {
						rm.setMessage("INVALID_APIKEY");
						rm.setType("LOGIN_RESPONSE");
						session.getBasicRemote().sendText(gson.toJson(rm));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} else {
				try {
					rm.setMessage("INVALID_USER");
					rm.setType("LOGIN_RESPONSE");
					session.getBasicRemote().sendText(gson.toJson(rm));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else if (msg[0].equalsIgnoreCase("PONG")) {

			System.out.println("PONG::" + msg[0]);
		} else if (rm.getType().equalsIgnoreCase("NOTIFY_RESPONSE")) {

			for (RechargeMessage rms : rml) {
				try {
					System.out.println("NOTIFYSENDING::" + gson.toJson(rms));

					try {
						ConnectedUserNotifyList.connectedClient.get(rms.getTo()).getBasicRemote()
								.sendText(gson.toJson(rms));
					} catch (Exception ex) {
						NonConnectedUserNotifyList.getInstance().AddQueue(rms);
					}

					RechargeNotifyMaster rnm = new RechargeNotifyMaster();
					rnm.setFromclient(rms.getFrom());
					rnm.setToclient(rms.getTo());
					rnm.setMessage(rms.getMessage());
					rnm.setAcceptdate(new Date());
					rnm.setSrcIP("0.0.0.0");
					rgservice.saveRechargeNotify(rnm);

				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}

			}
			/*
			 * try { System.out.println("RESPONSE TO ORIGIN:"+gson.toJson(rm));
			 * session.getBasicRemote().sendText(gson.toJson(rm)); } catch
			 * (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 */
		} else {
			System.out.println(message);
			/*
			 * email=msg[0]; uname=msg[1]; mo_no=msg[2]; client=msg[3];
			 * 
			 * // session.getBasicRemote().sendText(msg[0]+" "+msg[1]+"     "
			 * +msg[2]);
			 * 
			 * 
			 * try { session.getBasicRemote().sendText("asdgasdgasgasdgasdgasd"+
			 * message); } catch (Exception e) { // TODO Auto-generated catch
			 * block e.printStackTrace(); }
			 * 
			 * 
			 * clist.AddClient(client+"_"+mo_no, session);
			 * 
			 * System.out.println(msg[0]+" "+msg[1]+" "+msg[2]);
			 * 
			 * 
			 * ArrayList<String> al=new ArrayList<String>();
			 * 
			 * ArrayList<String>
			 * temp=Non_ConnectedList.getInstance().connectedClient.get(client+
			 * "_"+mo_no); Non_ConnectedList remove=new Non_ConnectedList();
			 * if(temp!=null) { for(int j=0;j<temp.size();j++) { try {
			 * 
			 * ConnectedList.getInstance().getClient(client+"_"+mo_no).
			 * getBasicRemote().sendText(temp.get(j)); } catch (IOException e) {
			 * // TODO Auto-generated catch block e.printStackTrace(); }
			 * remove.RemoveClient(client+"_"+mo_no); }
			 * 
			 * 
			 * }
			 * 
			 * }
			 */
		}

	}

	@OnOpen
	public void onOpen() {
		System.out.println("Provider Notity connected");
		// GCMSendMessage que=new GCMSendMessage();
		// que.queWebSocket();

	}

	@OnClose
	public void onClose(Session session) {

		System.out.println("Removing client due to Close: " + client + new Date());

		ConnectedProviderNofityList.getInstance().RemoveClient(client);

	}

}
package com.bonrix.sms.livelog;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.servlet.ServletOutputStream;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

public class LMAppender
  extends AppenderSkeleton
{
  protected ServletOutputStream out;
  boolean connClosed;
  private Layout appLayout;
  private SimpleDateFormat sdf;
  protected List<LogMessageBean> messages;
  protected Lock msgLock = new ReentrantLock(true);
  protected int maxMessageQLength = 1000;
  
  public LMAppender(ServletOutputStream out)
    throws IOException
  {
    this.out = out;
    this.connClosed = false;
    this.appLayout = new PatternLayout("%d %5p - %m%n");
    this.sdf = new SimpleDateFormat("HH:mm:ss");
    this.messages = Collections.synchronizedList(new ArrayList());
  }
  
  public void append(LoggingEvent event)
  {
    if ((!this.connClosed) && (this.messages.size() < this.maxMessageQLength))
    {
      String message = this.appLayout.format(event).replaceAll("\n", "<br/>").replaceAll("\r", "").replaceAll(" ", "&nbsp;");
      this.messages.add(new LogMessageBean(message, event.getLevel().toString()));
    }
  }
  
  public void setResuming(boolean resuming, Level level)
    throws IOException
  {
    if (resuming) {
      addLText("Resuming Live Logger (" + level + ")");
    } else {
      addLText("Starting Live Logger (" + level + ")");
    }
    runScript("lmg.stopped=false;window.status='Showing Live Log';parent.lmc.disableButtons(false);parent.lmc.setLogLevel('" + level + "');");
  }
  
  public void close()
  {
    if (!this.connClosed)
    {
      this.connClosed = true;
      try
      {
        runScript("lmg.stopped=true;window.status='Live logger is stopped';parent.lmc.disableButtons(true);");
        addLText("Live logger is stopped");
      }
      catch (Exception e) {}
    }
  }
  
  public boolean requiresLayout()
  {
    return true;
  }
  
  public void setLogLevel(String level)
    throws IOException
  {
    runScript("parent.lmc.setLogLevel('" + level + "');");
  }
  
  public void addLText(String text)
    throws IOException
  {
    runScript("lmg.addLText('" + this.sdf.format(new Date()) + " " + text.replaceAll("'", "\\\\'") + "');");
  }
  
  protected void insertText()
    throws IOException
  {
    runScript("lmg.insertText();");
  }
  
  protected void runScript(String text)
    throws IOException
  {
    this.out.print("<script type='text/javascript'>" + text + "</script>");
    this.out.flush();
  }
}

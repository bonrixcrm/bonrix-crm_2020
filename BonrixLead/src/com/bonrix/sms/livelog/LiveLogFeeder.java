package com.bonrix.sms.livelog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class LiveLogFeeder
  extends HttpServlet
{
  private static final Logger log = Logger.getLogger("LiveLogFeeder");
  private static final Lock lock = new ReentrantLock(true);
  private static final List<Long> runningIds = Collections.synchronizedList(new ArrayList());
  private static final List<Long> closingIds = Collections.synchronizedList(new ArrayList());
  private static int noOfClientsAllowed = 10;
  private static int maxNoOfMessagesToWriteOnce = 100;
  private static int intervalBetweenWrites = 500;
  
  public void init(ServletConfig config)
    throws ServletException
  {
    ServletContext ctx = config.getServletContext();
    String temp = ctx.getInitParameter("LIVE_LOGGER_MAX_CLIENTS");
    if (temp != null) {
      try
      {
        noOfClientsAllowed = Integer.parseInt(temp);
      }
      catch (NumberFormatException e) {}
    }
  }
  
  public void destroy() {}
  
  private void removeId(Long rid)
  {
    int noOfTries = 0;
    runningIds.remove(rid);
    closingIds.add(rid);
    while ((closingIds.contains(rid)) && (noOfTries < 20))
    {
      try
      {
        Thread.sleep(50L);
      }
      catch (InterruptedException e)
      {
        break;
      }
      noOfTries++;
    }
    if (closingIds.contains(rid)) {
      closingIds.remove(rid);
    }
  }
  
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    String action = null;
    String logLevel = null;
    long runningId = 0L;
    Logger appLog = null;
    ServletOutputStream out = null;
    LMAppender appender = null;
    String loggerName = null;
    long lastRunTime = 0L;
    long rid = 0L;
    
    action = request.getParameter("action");
    logLevel = request.getParameter("logLevel");
    loggerName = request.getParameter("loggerName");
    try
    {
      rid = Long.parseLong(request.getParameter("runningId"));
    }
    catch (Exception e)
    {
      rid = 0L;
    }
    if (rid == 0L)
    {
      try
      {
        lock.lock();
        if (runningIds.size() >= noOfClientsAllowed)
        {
          log.trace("Allowed no. of clients exceeded. Kicking out first one");
          removeId((Long)runningIds.get(0));
        }
        runningId = System.nanoTime();
        runningIds.add(Long.valueOf(runningId));
        log.trace("New Client. Assigning running id to " + runningId + " (" + runningIds.size() + ")");
      }
      finally
      {
        lock.unlock();
      }
    }
    else
    {
      removeId(Long.valueOf(rid));
      if (!"stop".equals(action)) {
        try
        {
          lock.lock();
          runningId = System.nanoTime();
          runningIds.add(Long.valueOf(runningId));
          log.trace("Existing Client. Assigning new running id to " + runningId + " (" + runningIds.size() + ")");
        }
        finally
        {
          lock.unlock();
        }
      }
    }
    out = response.getOutputStream();
    response.setContentType("text/html");
    out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
    out.println("<html><head><script type='text/javascript'>");
    ResourceIO.writeResource(out, "livelog.js");
    out.println("lmg.runningId=" + runningId + ";");
    out.println("</script><style type='text/css'>");
    ResourceIO.writeResource(out, "livelog.css");
    out.println("</style></head><body>");
    out.println("<!-- ");
    for (int i = 0; i <= 20; i++) {
      out.print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
    out.println("-->");
    out.flush();
    
    appender = new LMAppender(out);
    if ("stop".equals(action)) {
      appender.close();
    } else if (loggerName == null) {
      appender.addLText("Please specify a logger name");
    } else if ("root".equals(loggerName)) {
      appLog = Logger.getRootLogger();
    } else {
      appLog = Logger.getLogger(loggerName);
    }
    if (appLog != null)
    {
      appender.setName("CustomAppender" + runningId);
      if ("resume".equals(action))
      {
        if (logLevel != null)
        {
          appLog.setLevel(Level.toLevel(logLevel));
          Enumeration appenders = appLog.getAllAppenders();
          while (appenders.hasMoreElements())
          {
            Appender apndr = (Appender)appenders.nextElement();
            if ((apndr instanceof LMAppender))
            {
              LMAppender myApndr = (LMAppender)apndr;
              myApndr.setLogLevel(logLevel);
              myApndr.addLText("Changing log level to " + logLevel);
            }
          }
        }
        appender.setResuming(true, appLog.getEffectiveLevel());
      }
      else
      {
        appender.setResuming(false, appLog.getEffectiveLevel());
      }
      appLog.addAppender(appender);
      for (;;)
      {
        if (runningIds.contains(Long.valueOf(runningId))) {
          try
          {
            if (appender.messages.size() > 0)
            {
              writeMessagePortion(appender);
              lastRunTime = System.currentTimeMillis();
            }
            else if ((System.currentTimeMillis() - lastRunTime) / 1000L >= 60L)
            {
              appender.out.print(" ");
              appender.out.flush();
              lastRunTime = System.currentTimeMillis();
            }
            Thread.sleep(intervalBetweenWrites);
          }
          catch (Throwable e)
          {
            runningIds.remove(Long.valueOf(runningId));
          }
        }
      }
     // log.trace("Out of the loop for " + runningId + " (" + runningIds.size() + ")");
     // closingIds.remove(Long.valueOf(runningId));
     // appender.close();
     // appLog.removeAppender(appender);
    }
    out.println("</body></html>");
    out.close();
  }
  
  private void writeMessagePortion(LMAppender appender)
    throws Throwable
  {
    LogMessageBean message = null;
    
    int len = appender.messages.size();
    if (len > maxNoOfMessagesToWriteOnce) {
      len = maxNoOfMessagesToWriteOnce;
    }
    StringBuilder sb = new StringBuilder();
    for (int cnt = 0; cnt < len; cnt++)
    {
      message = (LogMessageBean)appender.messages.get(cnt);
      sb.append("lmg.addText('" + message.getMessage().replaceAll("'", "\\\\'") + "','l_" + message.getLevel().toLowerCase() + "');");
    }
    if (message != null)
    {
      appender.runScript(sb.toString());
      appender.insertText();
    }
    sb = null;
    appender.messages.subList(0, len).clear();
  }
}

package com.bonrix.sms.livelog;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LiveLogger
  extends HttpServlet
{
  private void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    String fileName = request.getPathInfo().substring(1);
    String ext = fileName.split("\\.")[1];
    response.setContentType(getContentType(ext) + ";charset=UTF-8");
    ServletOutputStream out = response.getOutputStream();
    try
    {
      ResourceIO.writeResource(out, fileName);
    }
    finally
    {
      out.close();
    }
  }
  
  private String getContentType(String ext)
  {
    String ct = null;
    if ("css".equals(ext)) {
      ct = "text/css";
    } else if ("js".equals(ext)) {
      ct = "text/javascript";
    } else {
      ct = "text/html";
    }
    return ct;
  }
  
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
}
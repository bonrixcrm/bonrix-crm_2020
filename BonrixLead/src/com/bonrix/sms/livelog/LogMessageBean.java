package com.bonrix.sms.livelog;
public class LogMessageBean
{
  private String message;
  private String level;
  
  public LogMessageBean(String message, String level)
  {
    this.message = message;
    this.level = level;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String message)
  {
    this.message = message;
  }
  
  public String getLevel()
  {
    return this.level;
  }
  
  public void setLevel(String level)
  {
    this.level = level;
  }
}

package com.bonrix.sms.livelog;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerRepository;

public class LogSelector
  extends HttpServlet
{
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    try
    {
      Logger rootLogger = Logger.getRootLogger();
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Please select the Logger(s) you wish to monitor</title>");
      out.println("<link type='text/css' rel='stylesheet' href='logger/livelog.css'/>");
      out.println("</head>");
      out.println("<body class='tah'>");
      out.println("<form method='GET' action='logger/livelog.html'>");
      out.println("<div id='crc'><div id='crl'>Live Log Monitor v0.1</div><div id='crr'>&copy; <a href='http://www.jaimon.co.uk' target='_blank'>Jaimon Mathew</a></div></div>");
      out.println("<h4>Please select the Logger you wish to monitor</h4>");
      out.println("<select name='loggerName' id='loggerName' class='logsel'>");
      out.println("<option value='root'>Root Logger (" + rootLogger.getEffectiveLevel() + ")</option>");
      Enumeration allLoggers = rootLogger.getLoggerRepository().getRootLogger().getLoggerRepository().getCurrentLoggers();
      while (allLoggers.hasMoreElements())
      {
        Logger logger = (Logger)allLoggers.nextElement();
        String level = logger.getEffectiveLevel() == null ? "Not set" : logger.getEffectiveLevel().toString();
        out.println("<option value='" + logger.getName() + "'>" + logger.getName() + " (" + level + ")</option>");
      }
      out.println("</select><p />");
      out.println("<input type='submit' value='Start monitoring' class='sbbtn'/>");
      out.println("</form>");
      out.println("</body>");
      out.println("</html>");
    }
    finally
    {
      out.close();
    }
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  public String getServletInfo()
  {
    return "Log Selection Servlet";
  }
}

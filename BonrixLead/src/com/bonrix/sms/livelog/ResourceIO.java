package com.bonrix.sms.livelog;
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletOutputStream;

public class ResourceIO
{
  public static void writeResource(ServletOutputStream out, String filename)
    throws IOException
  {
    InputStream is = ResourceIO.class.getClassLoader().getResourceAsStream("resources/" + filename);
    if (is == null) {
      throw new FileNotFoundException(filename);
    }
    BufferedInputStream bis = new BufferedInputStream(is);
    byte[] input = new byte['?'];
    boolean eof = false;
    while (!eof)
    {
      int length = bis.read(input);
      if (length == -1) {
        eof = true;
      } else {
        out.write(input, 0, length);
      }
    }
    is.close();
  }
}
package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "aegecustdetail")
public class AegeCustDetail {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id")
	private int Id;

	@Column(name = "NAME")
	private String NAME;

	@Column(name = "Email")
	private String Email;

	@Column(name = "Title")
	private String Title;

	@Column(name = "phoneNo")
	private String phoneNo;

	@Column(name = "agentlevel")
	private String agentlevel;

	@Column(name = "timeZone")
	private Date timeZone;

	@Column(name = "agentlanguage")
	private String agentlanguage;

	@Column(name = "signature")
	private String signature;

	@Column(name = "roleAdScope")
	private int roleAdScope;

	@Column(name = "agentRole")
	private String agentRole;

	@Column(name = "agentType")
	private int agentType;

	@Column(name = "companyId")
	private int companyId;

	@Column(name = "isActive")
	private boolean isActive;

	@Column(name = "PASSWORD")
	private String PASSWORD;

	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "TYPE")
	private int TYPE;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getAgentlevel() {
		return agentlevel;
	}

	public void setAgentlevel(String agentlevel) {
		this.agentlevel = agentlevel;
	}

	public Date getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(Date timeZone) {
		this.timeZone = timeZone;
	}

	public String getAgentlanguage() {
		return agentlanguage;
	}

	public void setAgentlanguage(String agentlanguage) {
		this.agentlanguage = agentlanguage;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public int getRoleAdScope() {
		return roleAdScope;
	}

	public void setRoleAdScope(int roleAdScope) {
		this.roleAdScope = roleAdScope;
	}

	public String getAgentRole() {
		return agentRole;
	}

	public void setAgentRole(String agentRole) {
		this.agentRole = agentRole;
	}

	public int getAgentType() {
		return agentType;
	}

	public void setAgentType(int agentType) {
		this.agentType = agentType;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getTYPE() {
		return TYPE;
	}

	public void setTYPE(int tYPE) {
		TYPE = tYPE;
	}

}

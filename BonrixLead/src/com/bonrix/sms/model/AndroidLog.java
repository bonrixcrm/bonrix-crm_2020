package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "androidLog")
public class AndroidLog {

	private int id;
	private int folloId;
	private int leadid;
	private int teleId;
	private String request;
	private String responce;
	private Date cretaeDate;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "folloId")
	public int getFolloId() {
		return folloId;
	}
	public void setFolloId(int folloId) {
		this.folloId = folloId;
	}
	
	@Column(name = "leadid")
	public int getLeadid() {
		return leadid;
	}
	public void setLeadid(int leadid) {
		this.leadid = leadid;
	}
	
	@Column(name = "request")
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	
	@Column(name = "responce")
	public String getResponce() {
		return responce;
	}
	public void setResponce(String responce) {
		this.responce = responce;
	}
	
	@Column(name = "cretaeDate")
	public Date getCretaeDate() {
		return cretaeDate;
	}
	public void setCretaeDate(Date cretaeDate) {
		this.cretaeDate = cretaeDate;
	}
	
	@Column(name = "teleId")
	public int getTeleId() {
		return teleId;
	}
	public void setTeleId(int teleId) {
		this.teleId = teleId;
	}
	
	
	
	
	
	
}

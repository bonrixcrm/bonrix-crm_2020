package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "android_mode")
public class Android_mode {
	
	@Id
	@GeneratedValue
	@Column(name = "mode_id")
	private int mode_id;
	
	@Column(name = "mode_name")
	private String mode_name;
	
	@Column(name = "mode_const")
	private String mode_const;

	public int getMode_id() {
		return mode_id;
	}

	public void setMode_id(int mode_id) {
		this.mode_id = mode_id;
	}

	public String getMode_name() {
		return mode_name;
	}

	public void setMode_name(String mode_name) {
		this.mode_name = mode_name;
	}

	public String getMode_const() {
		return mode_const;
	}

	public void setMode_const(String mode_const) {
		this.mode_const = mode_const;
	}
	
	
	
	
}

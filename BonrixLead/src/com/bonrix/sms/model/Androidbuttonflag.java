package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="androidbuttonflag")
public class Androidbuttonflag {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "falg")
	private String falg;
	
	@Column(name = "flag_val")
	private String flag_val;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFalg() {
		return falg;
	}

	public void setFalg(String falg) {
		this.falg = falg;
	}

	public String getFlag_val() {
		return flag_val;
	}

	public void setFlag_val(String flag_val) {
		this.flag_val = flag_val;
	}
}

package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "callStatus")
public class CallStatus {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "cstatusName")
	private String cstatusName;

	@Column(name = "compId")
	private int compId;

	public CallStatus(int id, String cstatusName, int compId) {
		super();
		this.id = id;
		this.cstatusName = cstatusName;
		this.compId = compId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CallStatus [id=" + id + ", cstatusName=" + cstatusName + ", compId=" + compId + "]";
	}

	/**
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the cstatusName
	 */
	public String getCstatusName() {
		return cstatusName;
	}

	/**
	 * @param cstatusName
	 *            the cstatusName to set
	 */
	public void setCstatusName(String cstatusName) {
		this.cstatusName = cstatusName;
	}

	/**
	 * 
	 * @return the compId
	 */
	public int getCompId() {
		return compId;
	}

	/**
	 * @param compId
	 *            the compId to set
	 */
	public void setCompId(int compId) {
		this.compId = compId;
	}

}

/**
 * 
 */
package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Bhavesh Patel
 *
 */
@Entity
@Table(name = "campaignlog")
public class CampaignLog {
	@Id
	@GeneratedValue
	@Column(name = "campid")
	private int campid;

	@Column(name = "uid")
	private long uid;

	@Column(name = "campname")
	private String campname;

	@Column(name = "encryptedid")
	private String encryptedid;

	@Column(name = "createdate")
	private Date createDate;

	@Column(name = "completedate")
	private Date completeDate;

	@Column(name = "validno")
	private int validno;

	@Column(name = "invalidno")
	private int invalidno;

	@Column(name = "creditcount")
	private int creditcount;

	@Column(name = "camptype")
	private String camptype;

	@Column(name = "invalidlog")
	private String invalidlog;

	@Column(name = "isdeleted")
	private Boolean isdeleted;

	public int getCampid() {
		return campid;
	}

	public void setCampid(int campid) {
		this.campid = campid;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getCampname() {
		return campname;
	}

	public void setCampname(String campname) {
		this.campname = campname;
	}

	public String getEncryptedid() {
		return encryptedid;
	}

	public void setEncryptedid(String encryptedid) {
		this.encryptedid = encryptedid;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public int getValidno() {
		return validno;
	}

	public void setValidno(int validno) {
		this.validno = validno;
	}

	public int getInvalidno() {
		return invalidno;
	}

	public void setInvalidno(int invalidno) {
		this.invalidno = invalidno;
	}

	public String getCamptype() {
		return camptype;
	}

	public void setCamptype(String camptype) {
		this.camptype = camptype;
	}

	public String getInvalidlog() {
		return invalidlog;
	}

	public void setInvalidlog(String invalidlog) {
		this.invalidlog = invalidlog;
	}

	public Boolean getIsdeleted() {
		return isdeleted;
	}

	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	public CampaignLog(long uid, String campname, String encryptedid, Date createDate, Date completeDate, int validno,
			int invalidno, String camptype, String invalidlog, int credit) {
		this.uid = uid;
		this.campname = campname;
		this.encryptedid = encryptedid;
		this.createDate = createDate;
		this.completeDate = completeDate;
		this.validno = validno;
		this.invalidno = invalidno;
		this.camptype = camptype;
		this.invalidlog = invalidlog;
		this.isdeleted = false;
		this.creditcount = credit;
	}

	public CampaignLog() {
	}

	public int getCreditcount() {
		return creditcount;
	}

	public void setCreditcount(int creditcount) {
		this.creditcount = creditcount;
	};

}

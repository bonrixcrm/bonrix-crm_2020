package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categorymanager")
public class CategoryManager {

	private Long categotyId;
	private String categortName;
	private Long companyId;
	private int defaultAssignId;
	private int defaulttempId;
	private int defaulttEmailpId;


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "categotyId")
	public Long getCategotyId() {
		return categotyId;
	}

	public void setCategotyId(Long categotyId) {
		this.categotyId = categotyId;
	}

	@Column(name = "categortName")
	public String getCategortName() {
		return categortName;
	}

	public void setCategortName(String categortName) {
		this.categortName = categortName;
	}

	@Column(name = "companyId")
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@Column(name = "defaultAssignId")
	public int getDefaultAssignId() {
		return defaultAssignId;
	}

	public void setDefaultAssignId(int defaultAssignId) {
		this.defaultAssignId = defaultAssignId;
	}
	
	@Column(name = "defaulttempId")
	public int getdefaulttempId() {
		return defaulttempId;
	}

	public void setdefaulttempId(int defaulttempId) {
		this.defaulttempId = defaulttempId;
	}
	
	@Column(name = "defaulttEmailpId")
	public int getdefaulttEmailpId() {
		return defaulttEmailpId;
	}

	public void setdefaulttEmailpId(int defaulttEmailpId) {
		this.defaulttEmailpId = defaulttEmailpId;
	}
	

}

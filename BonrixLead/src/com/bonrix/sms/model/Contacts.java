/**
 * 
 */
package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Table(name = "contacts")
public class Contacts {

	@Id
	@GeneratedValue
	@Column(name = "cid")
	private long cid;

	@Override
	public String toString() {
		return "Contacts [cid=" + cid + ", name=" + name + ", number=" + number + ", email=" + email + ", gid=" + gid
				+ "]";
	}

	@Column(name = "name")
	private String name;

	@Column(name = "number")
	private String number;

	@Column(name = "email")
	private String email;

	@Column(name = "gid")
	private long gid;

	/**
	 * 
	 * @return the cid
	 */
	public long getCid() {
		return cid;
	}

	/**
	 * @param cid
	 *            the cid to set
	 */
	public void setCid(long cid) {
		this.cid = cid;
	}

	/**
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return the gid
	 */
	public long getGid() {
		return gid;
	}

	/**
	 * @param gid
	 *            the gid to set
	 */
	public void setGid(long gid) {
		this.gid = gid;
	}

}

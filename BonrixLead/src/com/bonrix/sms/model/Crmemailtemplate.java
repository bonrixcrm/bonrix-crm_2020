package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "crmemailtemplate")
public class Crmemailtemplate {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "temp_Id")
	private int temp_Id;

	@Column(name = "temp_Name")
	private String temp_Name;

	@Column(name = "temp_Text")
	private String temp_Text;

	@Column(name = "emailSubject")
	private String emailSubject;

	@Column(name = "STATUS")
	private boolean STATUS;

	@Column(name = "comp_Id")
	private int comp_Id;

	@Column(name = "staff_Id")
	private int staff_Id;

	/*public Crmemailtemplate(int temp_Id, String temp_Name, String temp_Text, String sUBJECT, boolean sTATUS,
			int comp_Id, int staff_Id) {
		super();
		this.temp_Id = temp_Id;
		this.temp_Name = temp_Name;
		this.temp_Text = temp_Text;
		SUBJECT = sUBJECT;
		STATUS = sTATUS;
		this.comp_Id = comp_Id;
		this.staff_Id = staff_Id;
	}*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Crmemailtemplate [temp_Id=" + temp_Id + ", temp_Name=" + temp_Name + ", temp_Text=" + temp_Text
				+ ", SUBJECT=" + emailSubject + ", STATUS=" + STATUS + ", comp_Id=" + comp_Id + ", staff_Id=" + staff_Id
				+ "]";
	}

	/**
	 * 
	 * @return the temp_Id
	 */
	public int getTemp_Id() {
		return temp_Id;
	}

	/**
	 * @param temp_Id
	 *            the temp_Id to set
	 */
	public void setTemp_Id(int temp_Id) {
		this.temp_Id = temp_Id;
	}

	/**
	 * 
	 * @return the temp_Name
	 */
	public String getTemp_Name() {
		return temp_Name;
	}

	/**
	 * @param temp_Name
	 *            the temp_Name to set
	 */
	public void setTemp_Name(String temp_Name) {
		this.temp_Name = temp_Name;
	}

	/**
	 * 
	 * @return the temp_Text
	 */
	public String getTemp_Text() {
		return temp_Text;
	}

	/**
	 * @param temp_Text
	 *            the temp_Text to set
	 */
	public void setTemp_Text(String temp_Text) {
		this.temp_Text = temp_Text;
	}

	/**
	 * 
	 * @return the sUBJECT
	 */
	public String getSUBJECT() {
		return emailSubject;
	}

	/**
	 * @param sUBJECT
	 *            the sUBJECT to set
	 */
	public void setSUBJECT(String sUBJECT) {
		emailSubject = sUBJECT;
	}

	/**
	 * 
	 * @return the sTATUS
	 */
	public boolean isSTATUS() {
		return STATUS;
	}

	/**
	 * @param sTATUS
	 *            the sTATUS to set
	 */
	public void setSTATUS(boolean sTATUS) {
		STATUS = sTATUS;
	}

	/**
	 * 
	 * @return the comp_Id
	 */
	public int getComp_Id() {
		return comp_Id;
	}

	/**
	 * @param comp_Id
	 *            the comp_Id to set
	 */
	public void setComp_Id(int comp_Id) {
		this.comp_Id = comp_Id;
	}

	/**
	 * 
	 * @return the staff_Id
	 */
	public int getStaff_Id() {
		return staff_Id;
	}

	/**
	 * @param staff_Id
	 *            the staff_Id to set
	 */
	public void setStaff_Id(int staff_Id) {
		this.staff_Id = staff_Id;
	}

}

package com.bonrix.sms.model;

public class DailyLeadCount {
	
	private String Month;
    private int LeadCount;
    
	public String getMonth() {
		return Month;
	}
	public void setMonth(String month) {
		Month = month;
	}
	public int getLeadCount() {
		return LeadCount;
	}
	public void setLeadCount(int leadCount) {
		LeadCount = leadCount;
	}
    
    
    

}

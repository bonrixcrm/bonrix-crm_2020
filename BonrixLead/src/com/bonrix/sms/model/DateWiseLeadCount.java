package com.bonrix.sms.model;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DateWiseLeadCount {
   
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	Date ctDate;
	BigInteger ctCount;
	
	public Date getCtDate() {
		return ctDate;
	}
	public void setCtDate(Date ctDate) {
		this.ctDate = ctDate;
	}
	public BigInteger getCtCount() {
		return ctCount;
	}
	public void setCtCount(BigInteger ctCount) {
		this.ctCount = ctCount;
	}
	
	
	
	
}

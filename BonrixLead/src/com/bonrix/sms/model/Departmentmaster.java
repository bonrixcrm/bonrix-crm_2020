package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "departmentmaster")
public class Departmentmaster {

	@Id
	@GeneratedValue
	@Column(name = "departmentId")
	private int departmentId;

	@Column(name = "departmentName")
	private String departmentName;

	@Column(name = "compantId")
	private int compantId;

	@Column(name = "departmentDesc")
	private String departmentDesc;

}

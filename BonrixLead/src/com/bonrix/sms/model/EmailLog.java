package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "emaillog")
public class EmailLog {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;
	
	@Column(name = "lead_id")
	private int lead_id;
	
	@Column(name = "mail_body")
	private String mail_body;
	
	@Column(name = "subject")
	private String subject;
	
	@Column(name = "telecallet_id")
	private int telecallet_id;
	
	@Column(name = "send_date")
	private Date send_date;
	
	@Column(name = "attached_file")
	private String attached_file;
	
	@Column(name = "status")
	private String status;

	@Column(name = "company_id")
	private int company_id;
	
	@Column(name = "mail_error")
	private String mail_error;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getLead_id() {
		return lead_id;
	}

	public void setLead_id(int lead_id) {
		this.lead_id = lead_id;
	}

	public String getMail_body() {
		return mail_body;
	}   

	public void setMail_body(String mail_body) {
		this.mail_body = mail_body;
	}

	public String getAttached_file() {
		return attached_file;
	}

	public void setAttached_file(String attached_file) {
		this.attached_file = attached_file;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getTelecallet_id() {
		return telecallet_id;
	}

	public void setTelecallet_id(int telecallet_id) {
		this.telecallet_id = telecallet_id;
	}

	public Date getSend_date() {
		return send_date;
	}

	public void setSend_date(Date send_date) {
		this.send_date = send_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	
	
	
	
	


}

package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "extrafields")
public class ExtraFields {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "fId")
	private int fId;
	
	@Column(name = "fieldName")
	private String fieldName;
	
	@Column(name = "catId")
	private int catId;
	
	@Column(name = "companyId")
	private int companyId;

	public int getfId() {
		return fId;
	}

	public void setfId(int fId) {
		this.fId = fId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public int getCatId() {
		return catId;
	}

	public void setCatId(int catId) {
		this.catId = catId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
}

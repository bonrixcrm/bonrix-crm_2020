package com.bonrix.sms.model;



public class FileNameJSON  {
	 private String Audio_File_Name;

	    private String Telecaller_Name;
	    
	    private int srNo;

	    public String getAudio_File_Name ()
	    {
	        return Audio_File_Name;
	    }

	    public void setAudio_File_Name (String Audio_File_Name)
	    {
	        this.Audio_File_Name = Audio_File_Name;
	    }

	    public String getTelecaller_Name ()
	    {
	        return Telecaller_Name;
	    }

	    public void setTelecaller_Name (String Telecaller_Name)
	    {
	        this.Telecaller_Name = Telecaller_Name;
	    }
	    
	    

	    public int getSrNo() {
			return srNo;
		}

		public void setSrNo(int srNo) {
			this.srNo = srNo;
		}

		
		@Override
	    public String toString()
	    {
	        return "ClassPojo [Audio_File_Name = "+Audio_File_Name+", Telecaller_Name = "+Telecaller_Name+"]";
	    }
}

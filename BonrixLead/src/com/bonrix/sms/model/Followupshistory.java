package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "followupshistory")
public class Followupshistory {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "followupsId")
	private int followupsId;

	@Column(name = "tallycallerId")
	private int tallycallerId;

	@Column(name = "leadId")
	private int leadId;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSS",timezone = "IST")
	@Column(name = "callingTime")
	@DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date callingTime;

	@Column(name = "remark")
	private String remark;

	@Column(name = "sheduleTime")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSS",timezone = "IST")
	private Date sheduleTime;

	@Column(name = "newStatus")
	private String newStatus;

	@Column(name = "audoiFilePath")
	private String audoiFilePath;

	@Column(name = "callDuration")
	private int callDuration;

	@Column(name = "callStatus")
	private String callStatus;
	
	@Column(name = "companyId")
	private int companyId;
	
	@Column(name = "followsUpType")
	private String followsUpType;
	
	@Column(name = "failstatus")
	private String failstatus;
	
	@Column(name = "successStatus")
	private String successStatus;
	

	@Column(name = "followAddSecond")
	private int followAddSecond;
	
	@Column(name = "followupDevice")
	private String followupDevice;
	
	
	
	
	/**
	 * 
	 * @return the followupDevice
	 */
	public String getFollowupDevice() {
		return followupDevice;
	}

	/**
	 * @param followupDevice the followupDevice to set
	 */
	public void setFollowupDevice(String followupDevice) {
		this.followupDevice = followupDevice;
	}

	public int getFollowupsId() {
		return followupsId;
	}

	public void setFollowupsId(int followupsId) {
		this.followupsId = followupsId;
	}

	public int getTallycallerId() {
		return tallycallerId;
	}

	public void setTallycallerId(int tallycallerId) {
		this.tallycallerId = tallycallerId;
	}

	public Date getCallingTime() {
		return callingTime;
	}

	public void setCallingTime(Date callingTime) {
		this.callingTime = callingTime;
	}

	public int getLeadId() {
		return leadId;
	}

	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getSheduleTime() {
		return sheduleTime;
	}

	public void setSheduleTime(Date sheduleTime) {
		this.sheduleTime = sheduleTime;
	}

	public String getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public String getAudoiFilePath() {
		return audoiFilePath;
	}

	public void setAudoiFilePath(String audoiFilePath) {
		this.audoiFilePath = audoiFilePath;
	}

	public int getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getFollowsUpType() {
		return followsUpType;
	}

	public void setFollowsUpType(String followsUpType) {
		this.followsUpType = followsUpType;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the failstatus
	 */
	public String getFailstatus() {
		return failstatus;
	}

	/**
	 * @param failstatus the failstatus to set
	 */
	public void setFailstatus(String failstatus) {
		this.failstatus = failstatus;
	}

	/**
	 * @return the successStatus
	 */
	public String getSuccessStatus() {
		return successStatus;
	}

	/**
	 * @param successStatus the successStatus to set
	 */
	public void setSuccessStatus(String successStatus) {
		this.successStatus = successStatus;
	}

	/**
	 * 
	 * @return the followAddSecond
	 */
	public int getFollowAddSecond() {
		return followAddSecond;
	}

	/**
	 * @param followAddSecond the followAddSecond to set
	 */
	public void setFollowAddSecond(int followAddSecond) {
		this.followAddSecond = followAddSecond;
	}

}

package com.bonrix.sms.model;

import org.bson.types.ObjectId;

public class Friend {

	public ObjectId _id;
	private String name;
	private int age;

	public Friend() {
	};

	/*
	 * public Friend(@JsonProperty("name") String name1, @JsonProperty("age")
	 * int age1) { name=name1; age=age1;
	 * 
	 * }
	 */

	public Friend(String string, int i) {
		// TODO Auto-generated constructor stub
		name = string;
		age = i;

	}

	// private Address address;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	// constructor with attributes here
}

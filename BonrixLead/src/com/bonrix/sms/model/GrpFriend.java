package com.bonrix.sms.model;

import org.bson.types.ObjectId;

public class GrpFriend {

	public ObjectId _id;

	private int count;

	private String grpname;

	public String getGrpname() {
		return grpname;
	}

	public void setGrpname(String grpname) {
		this.grpname = grpname;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public GrpFriend() {
	};

	/*
	 * public Friend(@JsonProperty("name") String name1, @JsonProperty("age")
	 * int age1) { name=name1; age=age1;
	 * 
	 * }
	 */

	// constructor with attributes here
}

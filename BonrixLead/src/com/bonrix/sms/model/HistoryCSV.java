package com.bonrix.sms.model;

public class HistoryCSV {

	public long srno;
	public String MobileNo;
	public String Message;
	public String SentOn;
	public String Status;
	public String Service;
	public String SenderName;

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getSentOn() {
		return SentOn;
	}

	public void setSentOn(String sentOn) {
		SentOn = sentOn;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getService() {
		return Service;
	}

	public void setService(String service) {
		Service = service;
	}

	public String getSenderName() {
		return SenderName;
	}

	public void setSenderName(String senderName) {
		SenderName = senderName;
	}

	public HistoryCSV(long srno, String mobileNo, String message, String sentOn, String status, String service,
			String senderName) {
		super();
		this.srno = srno;
		MobileNo = mobileNo;
		Message = message;
		SentOn = sentOn;
		Status = status;
		Service = service;
		SenderName = senderName;
	}

}

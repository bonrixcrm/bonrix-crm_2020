package com.bonrix.sms.model;

import com.bonrix.sms.model.Commondata;
import com.bonrix.sms.model.Lead;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "lead")
public class Lead extends Commondata {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "leaadId")
  private int leaadId;
  
  @Column(name = "firstName", columnDefinition = "varchar(255) default 'N/A'")
  private String firstName;
  
  @Column(name = "lastName", columnDefinition = "varchar(255) default 'N/A'")
  private String lastName;
  
  @Column(name = "categoryId")
  private String categoryId;
  
  @Column(name = "email", columnDefinition = "varchar(255) default 'N/A'")
  private String email;
  
  @Column(name = "mobileNo")
  private String mobileNo;
  
  @Column(name = "leadProcessStatus", columnDefinition = "varchar(255) default 'None'")
  private String leadProcessStatus;
  
  @Column(name = "staffId")
  private int staffId;
  
  @Column(name = "companyId")
  private int companyId;
  
  @Column(name = "sheduleDate")
  @ColumnDefault("1980-06-30 17:36:00")
  private Date sheduleDate;
  
  @Column(name = "createDate")
  private Date createDate;
  
  @Column(name = "leadState", columnDefinition = "varchar(255) default 'Open'")
  private String leadState;
  
  @Column(name = "tallyCalletId")
  private int tallyCalletId;
  
  @Column(name = "csvData", columnDefinition = "varchar(255) default 'N/A'")
  private String csvData;
  
  @Column(name = "leadType", columnDefinition = "varchar(255) default 'N/A'")
  private String leadType;
  
  @Column(name = "autoDial")
  private boolean autoDial;
  
  @Column(name = "companyName", columnDefinition = "varchar(255) default 'N/A'")
  private String companyName;
  
  @Column(name = "website", columnDefinition = "varchar(255) default 'N/A'")
  private String website;
  
  @Column(name = "Country", columnDefinition = "varchar(255) default 'N/A'")
  private String Country;
  
  @Column(name = "state", columnDefinition = "varchar(255) default 'N/A'")
  private String state;
  
  @Column(name = "city", columnDefinition = "varchar(255) default 'N/A'")
  private String city;
  
  @Column(name = "leadcomment")
  private String leadcomment;
  
  @Column(name = "tagName")
  private String tagName;
  
  @Column(name = "notification_flag", columnDefinition = "varchar(255) default 'FALSE'")
  private String notification_flag;
  
  @Column(name = "assignDate", columnDefinition = "DATETIME default '1980-06-30 17:36:00' ")
  private Date assignDate;
  
  @Column(name = "dialState", columnDefinition = "TINYINT(1) default 0")
  private boolean dialState;
  
  @Column(name = "altmobileNo")
  private String altmobileNo;
  
  public int getCompanyId() {
    return this.companyId;
  }
  
  public void setCompanyId(int companyId) {
    this.companyId = companyId;
  }
  
  public boolean isDialState() {
    return this.dialState;
  }
  
  public void setDialState(boolean dialState) {
    this.dialState = dialState;
  }
  
  public String getTagName() {
    return this.tagName;
  }
  
  public void setTagName(String tagName) {
    this.tagName = tagName;
  }
  
  public int getLeaadId() {
    return this.leaadId;
  }
  
  public void setLeaadId(int leaadId) {
    this.leaadId = leaadId;
  }
  
  public String getFirstName() {
    return this.firstName;
  }
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  
  public String getLastName() {
    return this.lastName;
  }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  
  public String getcategoryId() {
    return this.categoryId;
  }
  
  public void setcategoryId(String categoryId) {
    this.categoryId = categoryId;
  }
  
  public String getemail() {
    return this.email;
  }
  
  public void setemail(String email) {
    this.email = email;
  }
  
  public String getMobileNo() {
    return this.mobileNo;
  }
  
  public void setMobileNo(String mobileNo) {
    this.mobileNo = mobileNo;
  }
  
  public String getLeadProcessStatus() {
    return this.leadProcessStatus;
  }
  
  public void setLeadProcessStatus(String leadProcessStatus) {
    this.leadProcessStatus = leadProcessStatus;
  }
  
  public int getstaffId() {
    return this.staffId;
  }
  
  public void setstaffId(int staffId) {
    this.staffId = staffId;
  }
  
  public Date getSheduleDate() {
    return this.sheduleDate;
  }
  
  public void setSheduleDate(Date sheduleDate) {
    this.sheduleDate = sheduleDate;
  }
  
  public Date getCreateDate() {
    return this.createDate;
  }
  
  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }
  
  public String getLeadState() {
    return this.leadState;
  }
  
  public void setLeadState(String leadState) {
    this.leadState = leadState;
  }
  
  public int getTallyCalletId() {
    return this.tallyCalletId;
  }
  
  public void setTallyCalletId(int tallyCalletId) {
    this.tallyCalletId = tallyCalletId;
  }
  
  public String getCsvData() {
    return this.csvData;
  }
  
  public void setCsvData(String csvData) {
    this.csvData = csvData;
  }
  
  public String getLeadType() {
    return this.leadType;
  }
  
  public void setLeadType(String leadType) {
    this.leadType = leadType;
  }
  
  public String getCategoryId() {
    return this.categoryId;
  }
  
  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public int getStaffId() {
    return this.staffId;
  }
  
  public void setStaffId(int staffId) {
    this.staffId = staffId;
  }
  
  public boolean isAutoDial() {
    return this.autoDial;
  }
  
  public void setAutoDial(boolean autoDial) {
    this.autoDial = autoDial;
  }
  
  public String getCompanyName() {
    return this.companyName;
  }
  
  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }
  
  public String getWebsite() {
    return this.website;
  }
  
  public void setWebsite(String website) {
    this.website = website;
  }
  
  public String getCountry() {
    return this.Country;
  }
  
  public void setCountry(String country) {
    this.Country = country;
  }
  
  public String getState() {
    return this.state;
  }
  
  public void setState(String state) {
    this.state = state;
  }
  
  public String getCity() {
    return this.city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  public String getLeadcomment() {
    return this.leadcomment;
  }
  
  public void setLeadcomment(String leadcomment) {
    this.leadcomment = leadcomment;
  }
  
  public String getNotification_flag() {
    return this.notification_flag;
  }
  
  public void setNotification_flag(String notification_flag) {
    this.notification_flag = notification_flag;
  }
  
  public Date getAssignDate() {
    return this.assignDate;
  }
  
  public void setAssignDate(Date assignDate) {
    this.assignDate = assignDate;
  }
  
  public String getAltmobileNo() {
    return this.altmobileNo;
  }
  
  public void setAltmobileNo(String altmobileNo) {
    this.altmobileNo = altmobileNo;
  }
}


package com.bonrix.sms.model;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

@SuppressWarnings("unused")
public class LeadAliasToBean {
	
	public LeadAliasToBean()
	{
		
	}

	private int leaadId;
	
	private String firstName;

	private String lastName;

	private String categortName;

	private String email;

	private String mobileNo;

	private String leadProcessStatus;

	private int staffId;

	private int companyId;

	private String sheduleDate;

	private Date createDate;

	private String leadState;

	private int tallyCalletId;

	private String csvData;

	private String leadType;

	private boolean autoDial;
   
	private String companyName;

	private String website;
  
	private String Country;

	private String state;

	private String city;
	
	private String leadcomment;
	
	private String tagName;
	
	private String failstatus;
	
	private Date callingTime;
	
	private String newStatus;
	
	private String sheduleTime;
	
	private String notification_flag;
	
	private Date assignDate;
	
	private boolean dialState;

	public void setLeaadId(int leaadId) {
		this.leaadId = leaadId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setCategoryId(String categortName) {
		this.categortName = categortName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public void setLeadProcessStatus(String leadProcessStatus) {
		this.leadProcessStatus = leadProcessStatus;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public void setLeadState(String leadState) {
		this.leadState = leadState;
	}

	public void setTallyCalletId(int tallyCalletId) {
		this.tallyCalletId = tallyCalletId;
	}

	public void setCsvData(String csvData) {
		this.csvData = csvData;
	}

	public void setLeadType(String leadType) {
		this.leadType = leadType;
	}

	public void setAutoDial(boolean autoDial) {
		this.autoDial = autoDial;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCity(String city) {
		this.city = city;
	}   

	public void setLeadcomment(String leadcomment) {
		this.leadcomment = leadcomment;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public void setNotification_flag(String notification_flag) {
		this.notification_flag = notification_flag;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	public void setCategortName(String categortName) {
		this.categortName = categortName;
	}

	public void setDialState(boolean dialState) {
		this.dialState = dialState;
	}

	public String getFailstatus() {
		return failstatus;
	}

	public void setFailstatus(String failstatus) {
		this.failstatus = failstatus;
	}


	public String getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public Date getCallingTime() {
		return callingTime;
	}

	public void setCallingTime(Date callingTime) {
		this.callingTime = callingTime;
	}

	public String getSheduleDate() {
		return sheduleDate;
	}

	public void setSheduleDate(String sheduleDate) {
		this.sheduleDate = sheduleDate;
	}

	public void setSheduleTime(String sheduleTime) {
		this.sheduleTime = sheduleTime;
	}
	
	
	
	
}

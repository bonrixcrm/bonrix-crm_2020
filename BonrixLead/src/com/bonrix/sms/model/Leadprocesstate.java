package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "leadprocesstate")
public class Leadprocesstate {
	private int stateId;
	private String stateName;
	private int companyId;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "stateId")
	public int getstateId() {
		return stateId;
	}

	public void setstateId(int stateId) {
		this.stateId = stateId;
	}

	@Column(name = "stateName")
	public String getstateName() {
		return stateName;
	}

	public void setstateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "companyId")
	public int getcompanyId() {
		return companyId;
	}

	public void setcompanyId(int companyId) {
		this.companyId = companyId;
	}

}

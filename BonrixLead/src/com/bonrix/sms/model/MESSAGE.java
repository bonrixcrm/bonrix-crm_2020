package com.bonrix.sms.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MESSAGE")
@XmlAccessorType(XmlAccessType.FIELD)
public class MESSAGE {

	public String USERNAME;
	public String PASSWORD;
	public String TEXT;
	public String PRIORITY;
	public String SENDER;
	public String MSGTYPE;
	public String ADDRESS;

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public String getTEXT() {
		return TEXT;
	}

	public void setTEXT(String tEXT) {
		TEXT = tEXT;
	}

	public String getPRIORITY() {
		return PRIORITY;
	}

	public void setPRIORITY(String pRIORITY) {
		PRIORITY = pRIORITY;
	}

	public String getSENDER() {
		return SENDER;
	}

	public void setSENDER(String sENDER) {
		SENDER = sENDER;
	}

	public String getMSGTYPE() {
		return MSGTYPE;
	}

	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

}

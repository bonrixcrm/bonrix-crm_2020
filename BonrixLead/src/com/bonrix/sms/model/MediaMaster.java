/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonrix.sms.model;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abc
 */
@Entity
@Table(name = "mediamaster")
@XmlRootElement
public class MediaMaster implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "mediaid")
	private Integer mediaid;
	@Column(name = "userid")
	private Long userid;
	@Column(name = "oldmsize")
	private Long oldmsize;
	@Column(name = "medianame")
	private String mnane;
	@Column(name = "mtype")
	private String mtype;
	@Column(name = "newmsize")
	private Long newmsize;
	@Column(name = "oldpath")
	private String oldpath;
	@Column(name = "newpath")
	private String newpath;
	@Column(name = "newmname")
	private String newmname;
	@Column(name = "mainurl")
	private String mainurl;
	@Column(name = "tinyurl")
	private String tinyurl;
	@Basic(optional = false)
	@Column(name = "uploadedon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date uploadedon;
	@Column(name = "processtime")
	private Long processtime;
	@Column(name = "isactive")
	private Boolean isactive;
	@Column(name = "hitcount")
	private Integer hitcount;

	public MediaMaster() {
	}

	public MediaMaster(Integer mediaid) {
		this.mediaid = mediaid;
	}

	public MediaMaster(Integer mediaid, Date uploadedon) {
		this.mediaid = mediaid;
		this.uploadedon = uploadedon;
	}

	public Integer getMediaid() {
		return mediaid;
	}

	public void setMediaid(Integer mediaid) {
		this.mediaid = mediaid;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getOldmsize() {
		return oldmsize;
	}

	public void setOldmsize(Long oldmsize) {
		this.oldmsize = oldmsize;
	}

	public String getMnane() {
		return mnane;
	}

	public void setMnane(String mnane) {
		this.mnane = mnane;
	}

	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	public Long getNewmsize() {
		return newmsize;
	}

	public void setNewmsize(Long newmsize) {
		this.newmsize = newmsize;
	}

	public String getOldpath() {
		return oldpath;
	}

	public void setOldpath(String oldpath) {
		this.oldpath = oldpath;
	}

	public String getNewpath() {
		return newpath;
	}

	public void setNewpath(String newpath) {
		this.newpath = newpath;
	}

	public String getNewmname() {
		return newmname;
	}

	public void setNewmname(String newmname) {
		this.newmname = newmname;
	}

	public String getMainurl() {
		return mainurl;
	}

	public void setMainurl(String mainurl) {
		this.mainurl = mainurl;
	}

	public String getTinyurl() {
		return tinyurl;
	}

	public void setTinyurl(String tinyurl) {
		this.tinyurl = tinyurl;
	}

	public Date getUploadedon() {
		return uploadedon;
	}

	public void setUploadedon(Date uploadedon) {
		this.uploadedon = uploadedon;
	}

	public Long getProcesstime() {
		return processtime;
	}

	public void setProcesstime(Long processtime) {
		this.processtime = processtime;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Integer getHitcount() {
		return hitcount;
	}

	public void setHitcount(Integer hitcount) {
		this.hitcount = hitcount;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (mediaid != null ? mediaid.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof MediaMaster)) {
			return false;
		}
		MediaMaster other = (MediaMaster) object;
		if ((this.mediaid == null && other.mediaid != null)
				|| (this.mediaid != null && !this.mediaid.equals(other.mediaid))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bonrix.sms.model.Uploadedmedia[ mediaid=" + mediaid + " ]";
	}

}

/**
 * 
 */
package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author NirajThakar
 *
 */
@Entity
@Table(name = "messageaudit")
public class MessageAudit {

	@Id
	@GeneratedValue
	@Column(name = "auid")
	private long auid;

	@Column(name = "uid")
	private long uid;

	// @ param userId : parent userId(logged user id)

	@Column(name = "userId")
	private long userid;

	@Column(name = "lastcridit")
	private long lastCridit;

	@Column(name = "newcridit")
	private long newCridit;

	@Column(name = "lastvalidity")
	private Date lastValidity;

	@Column(name = "newvalidity")
	private Date newValidity;

	@Column(name = "comments")
	private String comments;

	@Column(name = "transtime")
	private Date transtime;

	@Column(name = "serviceid")
	private String serviceid;

	@Column(name = "transtype")
	private String transtype;

	public String getTranstype() {
		return transtype;
	}

	public void setTranstype(String transtype) {
		this.transtype = transtype;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	/**
	 * @return the auid
	 */
	public long getAuid() {
		return auid;
	}

	/**
	 * @param auid
	 *            the auid to set
	 */
	public void setAuid(long auid) {
		this.auid = auid;
	}

	/**
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(long uid) {
		this.uid = uid;
	}

	/**
	 * @return the userid
	 */
	public long getUserid() {
		return userid;
	}

	/**
	 * @param userid
	 *            the userid to set
	 */
	public void setUserid(long userid) {
		this.userid = userid;
	}

	/**
	 * @return the lastCridit
	 */
	public long getLastCridit() {
		return lastCridit;
	}

	/**
	 * @param lastCridit
	 *            the lastCridit to set
	 */
	public void setLastCridit(long lastCridit) {
		this.lastCridit = lastCridit;
	}

	/**
	 * @return the newCridit
	 */
	public long getNewCridit() {
		return newCridit;
	}

	/**
	 * @param newCridit
	 *            the newCridit to set
	 */
	public void setNewCridit(long newCridit) {
		this.newCridit = newCridit;
	}

	/**
	 * @return the lastValidity
	 */
	public Date getLastValidity() {
		return lastValidity;
	}

	/**
	 * @param lastValidity
	 *            the lastValidity to set
	 */
	public void setLastValidity(Date lastValidity) {
		this.lastValidity = lastValidity;
	}

	/**
	 * @return the newValidity
	 */
	public Date getNewValidity() {
		return newValidity;
	}

	/**
	 * @param newValidity
	 *            the newValidity to set
	 */
	public void setNewValidity(Date newValidity) {
		this.newValidity = newValidity;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getTranstime() {
		return transtime;
	}

	public void setTranstime(Date transtime) {
		this.transtime = transtime;
	}

}

/**
 * 
 */
package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Table(name = "messagetemplate")
public class MessageTemplate {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;

	@Column(name = "uid")
	private long uid;

	@Column(name = "templateName")
	private String templateName;

	@Column(name = "message")
	private String message;

	@Column(name = "ishidden")
	private boolean ishidden;

	/**
	 * 
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(long uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return the ishidden
	 */
	public boolean isIshidden() {
		return ishidden;
	}

	/**
	 * @param ishidden
	 *            the ishidden to set
	 */
	public void setIshidden(boolean ishidden) {
		this.ishidden = ishidden;
	}

}

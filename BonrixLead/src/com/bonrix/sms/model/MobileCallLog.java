package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mobilecalllog")
public class MobileCallLog {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "mobileNo")
	private String mobileNo;
	
	@Column(name = "CallTime")
	private Date CallTime;
	
	@Column(name = "entryDate")
	private Date entryDate;
	
	@Column(name = "callStatus")
	private String callStatus;
	
	@Column(name = "audioFile")
	private String  audioFile;
	
	@Column(name = "callDuration")
	private int callDuration;
	
	@Column(name = "telecallerId")
	private int telecallerId;
	
	@Column(name = "companyId")
	private int companyId;
	
	@Column(name = "nane")
	private String nane;
	
	@Column(name = "leadCount")
	private int leadCount;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	public String getCallStatus() {
		return callStatus;
	}
	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}
	public String getAudioFile() {
		return audioFile;
	}
	public void setAudioFile(String audioFile) {
		this.audioFile = audioFile;
	}
	public int getCallDuration() {
		return callDuration;
	}
	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}
	public int getTelecallerId() {
		return telecallerId;
	}
	public void setTelecallerId(int telecallerId) {
		this.telecallerId = telecallerId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getNane() {
		return nane;
	}
	public void setNane(String nane) {
		this.nane = nane;
	}
	public int getLeadCount() {
		return leadCount;
	}
	public void setLeadCount(int leadCount) {
		this.leadCount = leadCount;
	}
	public Date getCallTime() {
		return CallTime;
	}
	public void setCallTime(Date callTime) {
		CallTime = callTime;
	}
	
	
	
	

}

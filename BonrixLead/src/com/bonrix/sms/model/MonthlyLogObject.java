package com.bonrix.sms.model;
public class MonthlyLogObject  {
	 

	    private String Month;

	    private int SuccessCall;
	    
	    private int FailCall;
	    
	    private int MonthCount;

	    public int getFailCall ()
	    {
	        return FailCall;
	    }

	    public void setFailCall (int FailCall)
	    {
	        this.FailCall = FailCall;
	    }

	    public String getMonth ()
	    {
	        return Month;
	    }

	    public void setMonth (String Month)
	    {
	        this.Month = Month;
	    }

	    public int getSuccessCall ()
	    {
	        return SuccessCall;
	    }

	    public void setSuccessCall (int SuccessCall)
	    {
	        this.SuccessCall = SuccessCall;
	    }
	    
	    

	    public int getMonthCount() {
			return MonthCount;
		}

		public void setMonthCount(int monthCount) {
			MonthCount = monthCount;
		}
		
		@Override
	    public String toString()
	    {
	    	  return "{FailCall:"+FailCall+",Month:"+Month+", SuccessCall:"+SuccessCall+"}";
	    	  
	       // return "ClassPojo [FailCall = "+FailCall+", Month = "+Month+", SuccessCall = "+SuccessCall+"]";
	    }
}

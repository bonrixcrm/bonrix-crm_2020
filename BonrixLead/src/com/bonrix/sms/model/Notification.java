package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notification")
public class Notification {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "notification")
	private String notification;
	
	@Column(name = "status",columnDefinition = "varchar(255) default 'FALSE'")
	private String status;
	
	@Column(name = "dateTime")
	private Date dateTime;
	
	@Column(name = "telecallerId")
	private int telecallerId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public int getTelecallerId() {
		return telecallerId;
	}
	public void setTelecallerId(int telecallerId) {
		this.telecallerId = telecallerId;
	}
	
	
	

}

/**
 * 
 */
package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Bhavesh Patel
 *
 */
@Entity
@Table(name = "notifylogmaster")
public class NotifyLogMaster {
	@Id
	@GeneratedValue
	@Column(name = "notifyid")
	private long notifyid;

	@Column(name = "fromclient")
	private String fromclient;

	@Column(name = "toclient")
	private String toclient;

	@Column(name = "message")
	private String message;

	@Column(name = "receivedtime")
	private Date receivedtime;

	@Column(name = "nstatus")
	private String nstatus;

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abc
 */
@Entity
@Table(name = "notify_master")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "NotifyMaster.findAll", query = "SELECT n FROM NotifyMaster n") })
public class NotifyMaster implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "notifyid")
	private Integer notifyid;
	@Column(name = "uid")
	private BigInteger uid;
	@Column(name = "email")
	private String email;
	@Column(name = "subject")
	private String subject;
	@Lob
	@Column(name = "messagebody")
	private String messagebody;
	@Column(name = "smstext")
	private String smstext;
	@Column(name = "mobile")
	private String mobile;
	@Basic(optional = false)
	@Column(name = "senton")
	@Temporal(TemporalType.TIMESTAMP)
	private Date senton;
	@Column(name = "notifytype")
	private String notifytype;
	@Column(name = "clientname")
	private String clientname;

	public NotifyMaster() {
	}

	public NotifyMaster(Integer notifyid) {
		this.notifyid = notifyid;
	}

	public NotifyMaster(Integer notifyid, Date senton) {
		this.notifyid = notifyid;
		this.senton = senton;
	}

	public Integer getNotifyid() {
		return notifyid;
	}

	public void setNotifyid(Integer notifyid) {
		this.notifyid = notifyid;
	}

	public BigInteger getUid() {
		return uid;
	}

	public void setUid(BigInteger uid) {
		this.uid = uid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessagebody() {
		return messagebody;
	}

	public void setMessagebody(String messagebody) {
		this.messagebody = messagebody;
	}

	public String getSmstext() {
		return smstext;
	}

	public void setSmstext(String smstext) {
		this.smstext = smstext;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getSenton() {
		return senton;
	}

	public void setSenton(Date senton) {
		this.senton = senton;
	}

	public String getNotifytype() {
		return notifytype;
	}

	public void setNotifytype(String notifytype) {
		this.notifytype = notifytype;
	}

	public String getClientname() {
		return clientname;
	}

	public void setClientname(String clientname) {
		this.clientname = clientname;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (notifyid != null ? notifyid.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof NotifyMaster)) {
			return false;
		}
		NotifyMaster other = (NotifyMaster) object;
		if ((this.notifyid == null && other.notifyid != null)
				|| (this.notifyid != null && !this.notifyid.equals(other.notifyid))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bonrix.sms.model.NotifyMaster[ notifyid=" + notifyid + " ]";
	}

}

package com.bonrix.sms.model;

public class PropertyPOJO {
	 private int Count;

	    private String Property;

	    public int getCount ()
	    {
	        return Count;
	    }

	    public void setCount (int Count)
	    {
	        this.Count = Count;
	    }

	    public String getProperty ()
	    {
	        return Property;
	    }

	    public void setProperty (String Property)
	    {
	        this.Property = Property;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [Count = "+Count+", Property = "+Property+"]";
	    }

		
		
}

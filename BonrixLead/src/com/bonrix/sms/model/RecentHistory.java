package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "recentactivitymaster")
public class RecentHistory {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "activityId")
	private int activityId;

	@Column(name = "ticketId")
	private int ticketId;

	@Column(name = "activityOperation")
	private String activityOperation;

	@Column(name = "agentId")
	private long agentId;

	@Column(name = "SUBJECT")
	private String SUBJECT;

	@Column(name = "updateField")
	private String updateField;

	@Column(name = "updateFieldValue")
	private String updateFieldValue;

	@Column(name = "createDateTime")
	private Date createDateTime;

	@Column(name = "companyId")
	private long companyId;

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public String getActivityOperation() {
		return activityOperation;
	}

	public void setActivityOperation(String activityOperation) {
		this.activityOperation = activityOperation;
	}

	public long getAgentId() {
		return agentId;
	}

	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	public String getSUBJECT() {
		return SUBJECT;
	}

	public void setSUBJECT(String sUBJECT) {
		SUBJECT = sUBJECT;
	}

	public String getUpdateField() {
		return updateField;
	}

	public void setUpdateField(String updateField) {
		this.updateField = updateField;
	}

	public String getUpdateFieldValue() {
		return updateFieldValue;
	}

	public void setUpdateFieldValue(String updateFieldValue) {
		this.updateFieldValue = updateFieldValue;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

}

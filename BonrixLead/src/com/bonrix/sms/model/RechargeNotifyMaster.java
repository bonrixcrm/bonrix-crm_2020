/**
 * 
 */
package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Bhavesh Patel
 *
 */
@Entity
@Table(name = "rechargenotifymaster")
public class RechargeNotifyMaster {
	@Id
	@GeneratedValue
	@Column(name = "rgid")
	private long rgid;

	@Column(name = "fromclient")
	private String fromclient;

	@Column(name = "toclient")
	private String toclient;

	@Column(name = "message")
	private String message;

	@Column(name = "srcip") // source unique rechargeid
	private String srcIP;

	@Column(name = "acceptdate")
	private Date acceptdate;

	/*
	 * @Column(name = "rechargeresponse") private String rechargeresponse;
	 */

	@Column(name = "tstatus")
	private String tStatus;

	public long getRgid() {
		return rgid;
	}

	public void setRgid(long rgid) {
		this.rgid = rgid;
	}

	public String getFromclient() {
		return fromclient;
	}

	public void setFromclient(String fromclient) {
		this.fromclient = fromclient;
	}

	public String getToclient() {
		return toclient;
	}

	public void setToclient(String toclient) {
		this.toclient = toclient;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSrcIP() {
		return srcIP;
	}

	public void setSrcIP(String srcIP) {
		this.srcIP = srcIP;
	}

	public Date getAcceptdate() {
		return acceptdate;
	}

	public void setAcceptdate(Date acceptdate) {
		this.acceptdate = acceptdate;
	}

	public String gettStatus() {
		return tStatus;
	}

	public void settStatus(String tStatus) {
		this.tStatus = tStatus;
	}

}

package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "regexpatten")
public class Regexpatten {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pattenId")
	private int pattenId;

	@Column(name = "namePatten")
	private String namePatten;

	@Column(name = "emailPatten")
	private String emailPatten;

	@Column(name = "cnoPatten")
	private String cnoPatten;

	@Column(name = "fromEmail")
	private String fromEmail;

	@Column(name = "emailSubject")
	private String emailSubject;

	@Column(name = "categoryId")
	private int categoryId;
	
	@Column(name = "companyId")
	private int companyId;

	@Column(name = "staffId")
	private int staffId;

	@Column(name = "tallycallerId")
	private int tallycallerId;

	/**
	 * 
	 * @return the pattenId
	 */
	public int getPattenId() {
		return pattenId;
	}

	/**
	 * @param pattenId
	 *            the pattenId to set
	 */
	public void setPattenId(int pattenId) {
		this.pattenId = pattenId;
	}

	/**
	 * 
	 * @return the namePatten
	 */
	public String getNamePatten() {
		return namePatten;
	}

	/**
	 * @param namePatten
	 *            the namePatten to set
	 */
	public void setNamePatten(String namePatten) {
		this.namePatten = namePatten;
	}

	/**
	 * 
	 * @return the emailPatten
	 */
	public String getEmailPatten() {
		return emailPatten;
	}

	/**
	 * @param emailPatten
	 *            the emailPatten to set
	 */
	public void setEmailPatten(String emailPatten) {
		this.emailPatten = emailPatten;
	}

	/**
	 * 
	 * @return the cnoPatten
	 */
	public String getCnoPatten() {
		return cnoPatten;
	}

	/**
	 * @param cnoPatten
	 *            the cnoPatten to set
	 */
	public void setCnoPatten(String cnoPatten) {
		this.cnoPatten = cnoPatten;
	}

	/**
	 * 
	 * @return the fromEmail
	 */
	public String getFromEmail() {
		return fromEmail;
	}

	/**
	 * @param fromEmail
	 *            the fromEmail to set
	 */
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	/**
	 * 
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}

	/**
	 * @param emailSubject
	 *            the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	/**
	 * 
	 * @return the categoryId
	 */
	public int getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * 
	 * @return the companyId
	 */
	public int getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	/**
	 * 
	 * @return the staffId
	 */
	public int getStaffId() {
		return staffId;
	}

	/**
	 * @param staffId
	 *            the staffId to set
	 */
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	/**
	 * 
	 * @return the tallycallerId
	 */
	public int getTallycallerId() {
		return tallycallerId;
	}

	/**
	 * @param tallycallerId
	 *            the tallycallerId to set
	 */
	public void setTallycallerId(int tallycallerId) {
		this.tallycallerId = tallycallerId;
	}

}

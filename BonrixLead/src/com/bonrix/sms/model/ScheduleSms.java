package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "schedulesms")
@Table(name = "schedulesms")
public class ScheduleSms {
	@Id
	@GeneratedValue
	@Column(name = "qid")
	private long qid;

	@Column(name = "uid")
	private long uid;

	@Column(name = "priority")
	private int priority;

	@Column(name = "message")
	private String message;

	@Column(name = "number")
	private String number;

	@Column(name = "stid")
	private long stid;

	@Column(name = "isdndchecked")
	private boolean isDndChecked;

	@Column(name = "iscutted")
	private boolean iscutted;

	@Column(name = "isunicode")
	private boolean isunicode;

	public boolean isIscutted() {
		return iscutted;
	}

	public void setIscutted(boolean iscutted) {
		this.iscutted = iscutted;
	}

	public boolean isIsunicode() {
		return isunicode;
	}

	public void setIsunicode(boolean isunicode) {
		this.isunicode = isunicode;
	}

	public long getSenderid() {
		return senderid;
	}

	public void setSenderid(long senderid) {
		this.senderid = senderid;
	}

	@Column(name = "senderid")
	private long senderid;

	@Column(name = "createdate")
	private Date creatDate;

	@Column(name = "status")
	private int status;

	@Column(name = "retrycount")
	private int retryCount;

	/**
	 * 
	 * @return the qid
	 */
	public long getQid() {
		return qid;
	}

	/**
	 * @param qid
	 *            the qid to set
	 */
	public void setQid(long qid) {
		this.qid = qid;
	}

	/**
	 * 
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(long uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return the stid
	 */
	public long getStid() {
		return stid;
	}

	/**
	 * @param stid
	 *            the stid to set
	 */
	public void setStid(long stid) {
		this.stid = stid;
	}

	/**
	 * 
	 * @return the creatDate
	 */
	public Date getCreatDate() {
		return creatDate;
	}

	/**
	 * @param creatDate
	 *            the creatDate to set
	 */
	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}

	/**
	 * 
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * 
	 * @return the retryCount
	 */
	public int getRetryCount() {
		return retryCount;
	}

	/**
	 * @param retryCount
	 *            the retryCount to set
	 */
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * 
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 
	 * @return the isDndChecked
	 */
	public boolean isDndChecked() {
		return isDndChecked;
	}

	/**
	 * @param isDndChecked
	 *            the isDndChecked to set
	 */
	public void setDndChecked(boolean isDndChecked) {
		this.isDndChecked = isDndChecked;
	}

}

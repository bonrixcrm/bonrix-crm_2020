/**
 * 
 */
package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Table(name = "servicetype")
public class ServiceType {

	@Id
	@GeneratedValue
	@Column(name = "stid")
	private long stid;

	@Column(name = "type")
	private String type;

	@Column(name = "notes")
	private String notes;

	@Column(name = "dndcheck")
	private Boolean dndcheck;

	@Column(name = "templatecheck")
	private Boolean templatecheck;

	@Column(name = "spamcheck")
	private Boolean spamcheck;

	@Column(name = "isactive")
	private Boolean isactive;

	@Column(name = "creditcnt")
	private int creditcnt;

	@Column(name = "isvisible")
	private Boolean isvisible;

	@Column(name = "priority")
	private int priority;

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Boolean getIsvisible() {
		return isvisible;
	}

	public void setIsvisible(Boolean isvisible) {
		this.isvisible = isvisible;
	}

	public Boolean getDndcheck() {
		return dndcheck;
	}

	public void setDndcheck(Boolean dndcheck) {
		this.dndcheck = dndcheck;
	}

	public Boolean getTemplatecheck() {
		return templatecheck;
	}

	public void setTemplatecheck(Boolean templatecheck) {
		this.templatecheck = templatecheck;
	}

	public Boolean getSpamcheck() {
		return spamcheck;
	}

	public void setSpamcheck(Boolean spamcheck) {
		this.spamcheck = spamcheck;
	}

	public Boolean getIsactivet() {
		return isactive;
	}

	public void setIsactivet(Boolean isactivet) {
		this.isactive = isactivet;
	}

	public int getCreditcnt() {
		return creditcnt;
	}

	public void setCreditcnt(int creditcnt) {
		this.creditcnt = creditcnt;
	}

	/**
	 * 
	 * @return the stid
	 */
	public long getStid() {
		return stid;
	}

	/**
	 * @param stid
	 *            the stid to set
	 */
	public void setStid(long stid) {
		this.stid = stid;
	}

	/**
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

}

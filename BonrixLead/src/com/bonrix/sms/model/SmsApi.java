/**
 * 
 */
package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "smsapi")
@Table(name = "smsapi")
public class SmsApi {
	@Id
	@GeneratedValue
	@Column(name = "apiid")
	private long apiId;

	@Column(name = "uid")
	private long uId;

	@Column(name = "apitext")
	private String apiText;

	@Column(name = "successmsg")
	private String successMsg;

	@Column(name = "priority")
	private int priorty;

	@Column(name = "isfailover")
	private int isFailOver;

	@Column(name = "serviceid")
	private int serviceid;

	@Column(name = "dlrapi")
	private String dlrapi;

	public String getDlrapi() {
		return dlrapi;
	}

	public void setDlrapi(String dlrapi) {
		this.dlrapi = dlrapi;
	}

	public int getServiceid() {
		return serviceid;
	}

	public void setServiceid(int serviceid) {
		this.serviceid = serviceid;
	}

	/**
	 * 
	 * @return the apiId
	 */
	public long getApiId() {
		return apiId;
	}

	/**
	 * @param apiId
	 *            the apiId to set
	 */
	public void setApiId(long apiId) {
		this.apiId = apiId;
	}

	/**
	 * 
	 * @return the uId
	 */
	public long getuId() {
		return uId;
	}

	/**
	 * @param uId
	 *            the uId to set
	 */
	public void setuId(long uId) {
		this.uId = uId;
	}

	/**
	 * 
	 * @return the apiText
	 */
	public String getApiText() {
		return apiText;
	}

	/**
	 * @param apiText
	 *            the apiText to set
	 */
	public void setApiText(String apiText) {
		this.apiText = apiText;
	}

	/**
	 * 
	 * @return the successMsg
	 */
	public String getSuccessMsg() {
		return successMsg;
	}

	/**
	 * @param successMsg
	 *            the successMsg to set
	 */
	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}

	/**
	 * 
	 * @return the priorty
	 */
	public int getPriorty() {
		return priorty;
	}

	/**
	 * @param priorty
	 *            the priorty to set
	 */
	public void setPriorty(int priorty) {
		this.priorty = priorty;
	}

	/**
	 * 
	 * @return the isFailOver
	 */
	public int getIsFailOver() {
		return isFailOver;
	}

	/**
	 * @param isFailOver
	 *            the isFailOver to set
	 */
	public void setIsFailOver(int isFailOver) {
		this.isFailOver = isFailOver;
	}

}

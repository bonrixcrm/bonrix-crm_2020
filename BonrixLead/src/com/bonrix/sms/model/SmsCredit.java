/**
 * 
 */
package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Table(name = "smscredit")
public class SmsCredit {

	@Id
	@GeneratedValue
	@Column(name = "scid")
	private long scId;

	@Column(name = "stid")
	private long stId;

	@Column(name = "uid")
	private long uId;

	@Column(name = "smscredit")
	private long smsCredit;

	@Column(name = "validity")
	private Date validity;

	/**
	 * 
	 * @return the scId
	 */
	public long getScId() {
		return scId;
	}

	/**
	 * @param scId
	 *            the scId to set
	 */
	public void setScId(long scId) {
		this.scId = scId;
	}

	/**
	 * 
	 * @return the stId
	 */
	public long getStId() {
		return stId;
	}

	/**
	 * @param stId
	 *            the stId to set
	 */
	public void setStId(long stId) {
		this.stId = stId;
	}

	/**
	 * 
	 * @return the uId
	 */
	public long getuId() {
		return uId;
	}

	/**
	 * @param uId
	 *            the uId to set
	 */
	public void setuId(long uId) {
		this.uId = uId;
	}

	/**
	 * 
	 * @return the smsCredit
	 */
	public long getSmsCredit() {
		return smsCredit;
	}

	/**
	 * @param smsCredit
	 *            the smsCredit to set
	 */
	public void setSmsCredit(long smsCredit) {
		this.smsCredit = smsCredit;
	}

	/**
	 * 
	 * @return the validity
	 */
	public Date getValidity() {
		return validity;
	}

	/**
	 * @param validity
	 *            the validity to set
	 */
	public void setValidity(Date validity) {
		this.validity = validity;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abc
 */
@Entity
@Table(name = "sms_dlr_summary")
@XmlRootElement
public class SmsDlrSummary implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "crdid")
	private Long crdid;
	@Column(name = "uid")
	private Long uid;
	@Column(name = "total")
	private Integer total;
	@Column(name = "DELIVRD")
	private Integer delivrd;
	@Column(name = "ERROR")
	private Integer error;
	@Column(name = "EXPIRED")
	private Integer expired;
	@Column(name = "UNDELIV")
	private Integer undeliv;
	@Column(name = "UNKNOWN")
	private Integer unknown;
	@Column(name = "REJECTD")
	private Integer rejectd;
	@Column(name = "DND")
	private Integer dnd;
	@Column(name = "SUBMITED")
	private Integer submited;
	@Column(name = "ondate")
	private String ondate;
	@Column(name = "updateon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateon;

	@Column(name = "serviceid")
	private Integer serviceid;

	public SmsDlrSummary() {
	}

	public SmsDlrSummary(Long crdid) {
		this.crdid = crdid;
	}

	public Long getCrdid() {
		return crdid;
	}

	public void setCrdid(Long crdid) {
		this.crdid = crdid;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getDelivrd() {
		return delivrd;
	}

	public void setDelivrd(Integer delivrd) {
		this.delivrd = delivrd;
	}

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	public Integer getExpired() {
		return expired;
	}

	public void setExpired(Integer expired) {
		this.expired = expired;
	}

	public Integer getUndeliv() {
		return undeliv;
	}

	public void setUndeliv(Integer undeliv) {
		this.undeliv = undeliv;
	}

	public Integer getUnknown() {
		return unknown;
	}

	public void setUnknown(Integer unknown) {
		this.unknown = unknown;
	}

	public Integer getRejectd() {
		return rejectd;
	}

	public void setRejectd(Integer rejectd) {
		this.rejectd = rejectd;
	}

	public Integer getDnd() {
		return dnd;
	}

	public void setDnd(Integer dnd) {
		this.dnd = dnd;
	}

	public Integer getSubmited() {
		return submited;
	}

	public void setSubmited(Integer submited) {
		this.submited = submited;
	}

	public String getOndate() {
		return ondate;
	}

	public void setOndate(String ondate) {
		this.ondate = ondate;
	}

	public Date getUpdateon() {
		return updateon;
	}

	public void setUpdateon(Date updateon) {
		this.updateon = updateon;
	}

	public Integer getServiceid() {
		return serviceid;
	}

	public void setServiceid(Integer serviceid) {
		this.serviceid = serviceid;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (crdid != null ? crdid.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof SmsDlrSummary)) {
			return false;
		}
		SmsDlrSummary other = (SmsDlrSummary) object;
		if ((this.crdid == null && other.crdid != null) || (this.crdid != null && !this.crdid.equals(other.crdid))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bonrix.sms.model.SmsDlrSummary[ crdid=" + crdid + " ]";
	}

}


package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.bson.types.ObjectId;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Table(name = "smshistory")
public class SmsHistory {

	@Id
	@Column(name = "hid")
	private long hid;

	@Override
	public String toString() {
		return "SmsHistory [hid=" + hid + ", userId=" + userId + ", stid=" + stid + ", bunchId=" + bunchId
				+ ", mobileNumber=" + mobileNumber + ", senderId=" + senderId + ", clientIp=" + clientIp
				+ ", submitDateTime=" + submitDateTime + ", sentDateTime=" + sentDateTime + ", retryCount=" + retryCount
				+ ", message=" + message + ", status=" + status + ", dlrStatus=" + dlrStatus + ", result=" + result
				+ ", apiId=" + apiId + ", trxId=" + trxId + "]";
	}

	// public ObjectId _id;

	@Column(name = "uid")
	private long userId;

	@Column(name = "stid")
	private Long stid;

	@Column(name = "bid")
	private int bunchId;

	@Column(name = "mobileNumber")
	private String mobileNumber;

	@Column(name = "sid")
	private String senderId;

	@Column(name = "clientip")
	private String clientIp;

	@Column(name = "submitdatetime")
	private Date submitDateTime;

	public Date getSubmitDateTime() {
		return submitDateTime;
	}

	public void setSubmitDateTime(Date submitDateTime) {
		this.submitDateTime = submitDateTime;
	}

	@Column(name = "sentdatetime")
	private Date sentDateTime;

	@Column(name = "retryCount")
	private long retryCount;

	@Column(name = "message")
	private String message;

	@Column(name = "status")
	private String status;

	@Column(name = "dlrstatus")
	private String dlrStatus;

	@Column(name = "result")
	private String result;

	@Column(name = "aid")
	private long apiId;

	@Column(name = "msgcount")
	private int msgcount;

	public int getMsgcount() {
		return msgcount;
	}

	public void setMsgcount(int msgcount) {
		this.msgcount = msgcount;
	}

	public long getTrxId() {
		return trxId;
	}

	public void setTrxId(long trxId) {
		this.trxId = trxId;
	}

	@Column(name = "trxid")
	private long trxId;

	/**
	 * 
	 * @return the hid
	 */
	public long getHid() {
		return hid;
	}

	/**
	 * @param hid
	 *            the hid to set
	 */
	public void setHid(long hid) {
		this.hid = hid;
	}

	/**
	 * 
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * 
	 * @return the type
	 */

	/**
	 * 
	 * @return the bunchId
	 */
	public int getBunchId() {
		return bunchId;
	}

	/**
	 * 
	 * @return the stid
	 */
	public Long getStid() {
		return stid;
	}

	/**
	 * @param stid
	 *            the stid to set
	 */
	public void setStid(Long stid) {
		this.stid = stid;
	}

	/**
	 * @param bunchId
	 *            the bunchId to set
	 */
	public void setBunchId(int bunchId) {
		this.bunchId = bunchId;
	}

	/**
	 * 
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * 
	 * @return the senderId
	 */
	public String getSenderId() {
		return senderId;
	}

	/**
	 * @param senderId
	 *            the senderId to set
	 */
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	/**
	 * 
	 * @return the clientIp
	 */
	public String getClientIp() {
		return clientIp;
	}

	/**
	 * @param clientIp
	 *            the clientIp to set
	 */
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	/**
	 * 
	 * @return the sentDateTime
	 */
	public Date getSentDateTime() {
		return sentDateTime;
	}

	/**
	 * @param sentDateTime
	 *            the sentDateTime to set
	 */
	public void setSentDateTime(Date sentDateTime) {
		this.sentDateTime = sentDateTime;
	}

	/**
	 * 
	 * @return the retryCount
	 */
	public long getRetryCount() {
		return retryCount;
	}

	/**
	 * @param retryCount
	 *            the retryCount to set
	 */
	public void setRetryCount(long retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return the dlrStatus
	 */
	public String getDlrStatus() {
		return dlrStatus;
	}

	/**
	 * @param dlrStatus
	 *            the dlrStatus to set
	 */
	public void setDlrStatus(String dlrStatus) {
		this.dlrStatus = dlrStatus;
	}

	/**
	 * 
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 
	 * @return the apiId
	 */
	public long getApiId() {
		return apiId;
	}

	/**
	 * @param apiId
	 *            the apiId to set
	 */
	public void setApiId(long apiId) {
		this.apiId = apiId;
	}

}

package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "smsqueue")
@Table(name = "smsqueue")
public class SmsQueue {
	@Override
	public String toString() {
		return "SmsQueue [qid=" + qid + ", uid=" + uid + ", priority=" + priority + ", message=" + message + ", number="
				+ number + ", stid=" + stid + ", isDndChecked=" + isDndChecked + ", iscutted=" + iscutted
				+ ", isunicode=" + isunicode + ", senderid=" + senderid + ", creatDate=" + creatDate + ", status="
				+ status + ", retryCount=" + retryCount + "]";
	}

	@Id
	@GeneratedValue
	@Column(name = "qid")
	private long qid;

	@Column(name = "uid")
	private long uid;

	@Column(name = "priority")
	private int priority;

	@Column(name = "message")
	private String message;

	@Column(name = "number")
	private String number;

	@Column(name = "stid")
	private long stid;

	@Column(name = "isdndchecked")
	private boolean isDndChecked;

	@Column(name = "iscutted")
	private boolean iscutted;

	@Column(name = "isunicode")
	private boolean isunicode;

	@Column(name = "senderid")
	private String senderid;

	@Column(name = "createdate")
	private Date creatDate;

	@Column(name = "status")
	private int status;

	@Column(name = "retrycount")
	private int retryCount;

	@Column(name = "channelid")
	private int channelid;

	@Column(name = "campid")
	private int campid;

	@Column(name = "msgcredit")
	private int msgcredit;

	public int getMsgcredit() {
		return msgcredit;
	}

	public void setMsgcredit(int msgcredit) {
		this.msgcredit = msgcredit;
	}

	public int getChannelid() {
		return channelid;
	}

	public void setChannelid(int channelid) {
		this.channelid = channelid;
	}

	public boolean isIscutted() {
		return iscutted;
	}

	public void setIscutted(boolean iscutted) {
		this.iscutted = iscutted;
	}

	public boolean isIsunicode() {
		return isunicode;
	}

	public void setIsunicode(boolean isunicode) {
		this.isunicode = isunicode;
	}

	public String getSenderid() {
		return senderid;
	}

	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}

	/**
	 * 
	 * @return the qid
	 */
	public long getQid() {
		return qid;
	}

	/**
	 * @param qid
	 *            the qid to set
	 */
	public void setQid(long qid) {
		this.qid = qid;
	}

	/**
	 * 
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(long uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return the stid
	 */
	public long getStid() {
		return stid;
	}

	/**
	 * @param stid
	 *            the stid to set
	 */
	public void setStid(long stid) {
		this.stid = stid;
	}

	/**
	 * 
	 * @return the creatDate
	 */
	public Date getCreatDate() {
		return creatDate;
	}

	/**
	 * @param creatDate
	 *            the creatDate to set
	 */
	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}

	/**
	 * 
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * 
	 * @return the retryCount
	 */
	public int getRetryCount() {
		return retryCount;
	}

	/**
	 * @param retryCount
	 *            the retryCount to set
	 */
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * 
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 
	 * @return the isDndChecked
	 */
	public boolean isDndChecked() {
		return isDndChecked;
	}

	/**
	 * @param isDndChecked
	 *            the isDndChecked to set
	 */
	public void setDndChecked(boolean isDndChecked) {
		this.isDndChecked = isDndChecked;
	}

	public int getCampid() {
		return campid;
	}

	public void setCampid(int campid) {
		this.campid = campid;
	}

	public SmsQueue(long uid, int priority, String message, String number, long stid, boolean isDndChecked,
			boolean iscutted, boolean isunicode, String senderid, Date creatDate, int channelid, int campid,
			int msgcredit) {

		this.uid = uid;
		this.priority = priority;
		this.message = message;
		this.number = number;
		this.stid = stid;
		this.isDndChecked = isDndChecked;
		this.iscutted = iscutted;
		this.isunicode = isunicode;
		this.senderid = senderid;
		this.creatDate = creatDate;
		this.status = 1;
		this.retryCount = 1;
		this.channelid = channelid;
		this.campid = campid;
		this.msgcredit = msgcredit;
	}

	public SmsQueue() {

	}

}

/**
 * 
 */
package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Bhavesh
 *
 */
@Entity
@Table(name = "spamwords")
public class SpamWords {
	@Id
	@GeneratedValue
	@Column(name = "spamid")
	private int spamid;

	@Column(name = "addedby")
	private long addedby;

	@Column(name = "spamtext")
	private String spamtext;

	@Column(name = "addedon")
	private Date addedon;
	/*
	 * @Column(name = "notes") private String notes;
	 */

	@Column(name = "isactive")
	private Boolean isactive;

	@Column(name = "serviceid")
	private int serviceid;

	public int getSpamid() {
		return spamid;
	}

	public void setSpamid(int spamid) {
		this.spamid = spamid;
	}

	public long getAddedby() {
		return addedby;
	}

	public void setAddedby(long addedby) {
		this.addedby = addedby;
	}

	public String getSpamtext() {
		return spamtext;
	}

	public void setSpamtext(String spamtext) {
		this.spamtext = spamtext;
	}

	public Date getAddedon() {
		return addedon;
	}

	public void setAddedon(Date addedon) {
		this.addedon = addedon;
	}

	/*
	 * public String getNotes() { return notes; }
	 * 
	 * public void setNotes(String notes) { this.notes = notes; }
	 */

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public int getServiceid() {
		return serviceid;
	}

	public void setServiceid(int serviceid) {
		this.serviceid = serviceid;
	}

}

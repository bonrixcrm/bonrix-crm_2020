package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "successstatus")
public class Successstatus {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "statusName")
	private String statusName;

	@Column(name = "compId")
	private int compId;
	
	@Column(name = "leadStateId")
	private int leadStateId;

	/*public Successstatus(int id, String statusName, int compId) {
		super();
		this.id = id;
		this.statusName = statusName;
		this.compId = compId;
	}*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Successstatus [id=" + id + ", statusName=" + statusName + ", compId=" + compId + "]";
	}

	/**
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName
	 *            the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/**
	 * 
	 * @return the compId
	 */
	public int getCompId() {
		return compId;
	}

	/**
	 * @param compId
	 *            the compId to set
	 */
	public void setCompId(int compId) {
		this.compId = compId;
	}

	public int getLeadStateId() {
		return leadStateId;
	}

	public void setLeadStateId(int leadStateId) {
		this.leadStateId = leadStateId;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abc
 */
@Entity
@Table(name = "system_notification")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "SystemNotification.findAll", query = "SELECT s FROM SystemNotification s") })
public class SystemNotification implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "slog_id")
	private Integer slogId;
	@Column(name = "sys_log_type")
	private String sysLogType;
	@Lob
	@Column(name = "sys_log_text")
	private String sysLogText;
	@Basic(optional = false)
	@Column(name = "sys_log_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sysLogDate;
	@Column(name = "sys_log_user")
	private String sysLogUser;
	@Column(name = "sys_log_count")
	private String sysLogCount;

	public SystemNotification() {
	}

	public SystemNotification(Integer slogId) {
		this.slogId = slogId;
	}

	public SystemNotification(Integer slogId, Date sysLogDate) {
		this.slogId = slogId;
		this.sysLogDate = sysLogDate;
	}

	public Integer getSlogId() {
		return slogId;
	}

	public void setSlogId(Integer slogId) {
		this.slogId = slogId;
	}

	public String getSysLogType() {
		return sysLogType;
	}

	public void setSysLogType(String sysLogType) {
		this.sysLogType = sysLogType;
	}

	public String getSysLogText() {
		return sysLogText;
	}

	public void setSysLogText(String sysLogText) {
		this.sysLogText = sysLogText;
	}

	public Date getSysLogDate() {
		return sysLogDate;
	}

	public void setSysLogDate(Date sysLogDate) {
		this.sysLogDate = sysLogDate;
	}

	public String getSysLogUser() {
		return sysLogUser;
	}

	public void setSysLogUser(String sysLogUser) {
		this.sysLogUser = sysLogUser;
	}

	public String getSysLogCount() {
		return sysLogCount;
	}

	public void setSysLogCount(String sysLogCount) {
		this.sysLogCount = sysLogCount;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (slogId != null ? slogId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof SystemNotification)) {
			return false;
		}
		SystemNotification other = (SystemNotification) object;
		if ((this.slogId == null && other.slogId != null)
				|| (this.slogId != null && !this.slogId.equals(other.slogId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bonrix.sms.model.SystemNotification[ slogId=" + slogId + " ]";
	}

}

/**
 * 
 */
package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author Bhavesh
 *
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "systempara")
@Table(name = "systempara")
public class SystemParameter {
	@Id
	@GeneratedValue
	@Column(name = "paraid")
	private long paraid;

	@Column(name = "uid")
	private long uid;

	@Column(name = "paraname")
	private String paraname;

	@Column(name = "paravalue")
	private String paravalue;

	public long getParaid() {
		return paraid;
	}

	public void setParaid(long paraid) {
		this.paraid = paraid;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getParaname() {
		return paraname;
	}

	public void setParaname(String paraname) {
		this.paraname = paraname;
	}

	public String getParavalue() {
		return paravalue;
	}

	public void setParavalue(String paravalue) {
		this.paravalue = paravalue;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getAddedon() {
		return addedon;
	}

	public void setAddedon(Date addedon) {
		this.addedon = addedon;
	}

	public int getIsactive() {
		return isactive;
	}

	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}

	@Column(name = "notes")
	private String notes;

	@Column(name = "addedon")
	private Date addedon;

	@Column(name = "isactive")
	private int isactive;

}

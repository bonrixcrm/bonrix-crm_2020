package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tallycaller")

public class Tallycaller {

	@Id
	@GeneratedValue
	@Column(name = "tcallerid")
	private int tcallerid;

	@Column(name = "username")
	private String username;

	@Column(name = "passwrd")
	private String passwrd;

	@Column(name = "firstname")
	private String firstname;

	@Column(name = "lastname")
	private String lastname;

	@Column(name = "mobno")
	private String mobno;

	@Column(name = "email")
	private String email;

	@Column(name = "companyId")
	private int companyId;

	@Column(name = "active")
	private int active;

	@Column(name = "allocate_staff_id")
	private int allocate_staff_id;

	@Column(name = "regDate")
	private Date regDate;
	
	@Column(name = "apikey")
	private String apikey;
	
	

	public int getTcallerid() {
		return tcallerid;
	}

	public void setTcallerid(int tcallerid) {
		this.tcallerid = tcallerid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswrd() {
		return passwrd;
	}

	public void setPasswrd(String passwrd) {
		this.passwrd = passwrd;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMobno() {
		return mobno;
	}

	public void setMobno(String mobno) {
		this.mobno = mobno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getAllocate_staff_id() {
		return allocate_staff_id;
	}

	public void setAllocate_staff_id(int allocate_staff_id) {
		this.allocate_staff_id = allocate_staff_id;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * 
	 * @return the apikey
	 */
	public String getApikey() {
		return apikey;
	}

	/**
	 * @param apikey the apikey to set
	 */
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	

}

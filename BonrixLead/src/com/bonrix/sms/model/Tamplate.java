package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Table(name = "tamplate")
public class Tamplate {
	@Id
	@GeneratedValue
	@Column(name = "tid")
	private long tid;

	@Column(name = "tname")
	private String tname;

	@Column(name = "uid")
	private long uid;

	@Column(name = "tmessage")
	private String message;

	@Column(name = "isactive")
	private int isactive;

	@Column(name = "istrans")
	private Boolean istrans;

	/**
	 * 
	 * @return the tid
	 */
	public long getTid() {
		return tid;
	}

	/**
	 * @param tid
	 *            the tid to set
	 */
	public void setTid(long tid) {
		this.tid = tid;
	}

	/**
	 * 
	 * @return the tname
	 */
	public String getTname() {
		return tname;
	}

	/**
	 * @param tname
	 *            the tname to set
	 */
	public void setTname(String tname) {
		this.tname = tname;
	}

	/**
	 * 
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(long uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return the isactive
	 */
	public int getIsactive() {
		return isactive;
	}

	/**
	 * @param isactive
	 *            the isactive to set
	 */
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}

	public Boolean getIstrans() {
		return istrans;
	}

	public void setIstrans(Boolean istrans) {
		this.istrans = istrans;
	}

}

package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "telecallerlog")
public class TelecallerLog {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "tcallerid")
	private String tcallerid;

	@Column(name = "mobileno")
	private String mobileno;

	@Column(name = "request")
	private String request;
	
	@Column(name = "reponce")
	private String reponce;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTcallerid() {
		return tcallerid;
	}

	public void setTcallerid(String tcallerid) {
		this.tcallerid = tcallerid;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getReponce() {
		return reponce;
	}

	public void setReponce(String reponce) {
		this.reponce = reponce;
	}
	
	

}

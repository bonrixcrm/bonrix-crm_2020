package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "telecaller_break")
public class Telecaller_break {
	

	private int Break_Id;
	private Date Start_Break;
	private Date End_Break;
	private int Telecaller_Id;
	/**
	 * 
	 * @return the break_Id
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Break_Id")
	public int getBreak_Id() {
		return Break_Id;
	}
	/**
	 * @param break_Id the break_Id to set
	 */
	public void setBreak_Id(int break_Id) {
		Break_Id = break_Id;
	}
	/**
	 * 
	 * @return the start_Break
	 */
	@Column(name = "Start_Break")
	public Date getStart_Break() {
		return Start_Break;
	}
	/**
	 * @param start_Break the start_Break to set
	 */
	public void setStart_Break(Date start_Break) {
		Start_Break = start_Break;
	}
	/**
	 * 
	 * @return the end_Break
	 */
	@Column(name = "End_Break")
	public Date getEnd_Break() {
		return End_Break;
	}
	/**
	 * @param end_Break the end_Break to set
	 */
	public void setEnd_Break(Date end_Break) {
		End_Break = end_Break;
	}
	/**
	 * 
	 * @return the telecaller_Id
	 */
	@Column(name = "Telecaller_Id")
	public int getTelecaller_Id() {
		return Telecaller_Id;
	}
	/**
	 * @param telecaller_Id the telecaller_Id to set
	 */
	public void setTelecaller_Id(int telecaller_Id) {
		Telecaller_Id = telecaller_Id;
	}
	
	

}

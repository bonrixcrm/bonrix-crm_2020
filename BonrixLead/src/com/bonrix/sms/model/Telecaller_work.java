package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "telecaller_work")
public class Telecaller_work {
	
	private int WorkId;
	private Date Start_Time;
	private Date End_Time;
	private int companyId;
	private int Telecaller_Id;
	/**
	 * 
	 * @return the workId
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "WorkId")
	public int getWorkId() {
		return WorkId;
	}
	/**
	 * @param workId the workId to set
	 */
	public void setWorkId(int workId) {
		WorkId = workId;
	}
	/**
	 * 
	 * @return the start_Time
	 */
	@Column(name = "Start_Time")
	public Date getStart_Time() {
		return Start_Time;
	}
	/**
	 * @param start_Time the start_Time to set
	 */
	public void setStart_Time(Date start_Time) {
		Start_Time = start_Time;
	}
	/**
	 * 
	 * @return the end_Time
	 */
	@Column(name = "End_Time")
	public Date getEnd_Time() {
		return End_Time;
	}
	/**
	 * @param end_Time the end_Time to set
	 */
	public void setEnd_Time(Date end_Time) {
		End_Time = end_Time;
	}
	/**
	 * 
	 * @return the telecaller_Id
	 */
	@Column(name = "Telecaller_Id")
	public int getTelecaller_Id() {
		return Telecaller_Id;
	}
	/**
	 * @param telecaller_Id the telecaller_Id to set
	 */
	public void setTelecaller_Id(int telecaller_Id) {
		Telecaller_Id = telecaller_Id;
	}
	/**
	 * 
	 * @return the loginDate
	 */
	public int getcompanyId() {
		return companyId;
	}
	/**
	 * @param loginDate the loginDate to set
	 */
	public void setcompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
	
	
	


}

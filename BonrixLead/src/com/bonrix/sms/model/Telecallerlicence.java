package com.bonrix.sms.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "telecallerlicence")
public class Telecallerlicence {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "LicenceId")
	private int LicenceId;

	@Column(name = "CompanyId")
	private int CompanyId;

	@Column(name = "TelecallerId")
	private int TelecallerId;
	
	@Column(name = "TelecallerCount")
	private int TelecallerCount;

	/**
	 * 
	 * @return the licenceId
	 */
	public int getLicenceId() {
		return LicenceId;
	}

	/**
	 * @param licenceId the licenceId to set
	 */
	public void setLicenceId(int licenceId) {
		LicenceId = licenceId;
	}

	/**
	 * 
	 * @return the companyId
	 */
	public int getCompanyId() {
		return CompanyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(int companyId) {
		CompanyId = companyId;
	}

	/**
	 * 
	 * @return the telecallerId
	 */
	public int getTelecallerId() {
		return TelecallerId;
	}

	/**
	 * @param telecallerId the telecallerId to set
	 */
	public void setTelecallerId(int telecallerId) {
		TelecallerId = telecallerId;
	}

	/**
	 * 
	 * @return the telecallerCount
	 */
	public int getTelecallerCount() {
		return TelecallerCount;
	}

	/**
	 * @param telecallerCount the telecallerCount to set
	 */
	public void setTelecallerCount(int telecallerCount) {
		TelecallerCount = telecallerCount;
	}
	
	
	

}

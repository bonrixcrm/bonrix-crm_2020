package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ticketchildmaster")
public class Ticketchildmaster {

	@Id
	@GeneratedValue
	@Column(name = "ticketChileId")
	private int ticketChileId;

	@Column(name = "ticketId")
	private int ticketId;

	@Column(name = "folloUps")
	private String folloUps;

	@Column(name = "isVisible")
	private boolean isVisible;

	@Column(name = "updatedBy")
	private int updatedBy;

	@Column(name = "updateOn")
	private Date updateOn;

	@Column(name = "SenderId")
	private int SenderId;

	public int getTicketChileId() {
		return ticketChileId;
	}

	public void setTicketChileId(int ticketChileId) {
		this.ticketChileId = ticketChileId;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getFolloUps() {
		return folloUps;
	}

	public void setFolloUps(String folloUps) {
		this.folloUps = folloUps;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}

	public int getSenderId() {
		return SenderId;
	}

	public void setSenderId(int senderId) {
		SenderId = senderId;
	}

}

package com.bonrix.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ticketmaster")
public class Ticketmaster {

	@Id
	@GeneratedValue
	@Column(name = "ticketId")
	private int ticketId;

	@Column(name = "companyId")
	private int companyId;

	@Column(name = "userEmailId")
	private String userEmailId;

	@Column(name = "priority")
	private String priority;

	@Column(name = "ticketStatus")
	private String ticketStatus;

	@Column(name = "subject")
	private String subject;

	@Column(name = "source")
	private String source;

	@Column(name = "ticketType")
	private String ticketType;

	@Column(name = "agentId")
	private int agentId;

	@Column(name = "departmentId")
	private int departmentId;

	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "dueOn")
	private Date dueOn;

	@Column(name = "idDeleted")
	private int idDeleted;

	@Column(name = "fileName")
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getTicketId() {
		return ticketId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public int getDepartment() {
		return departmentId;
	}

	public void setDepartment(int departmentId) {
		this.departmentId = departmentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getDueOn() {
		return dueOn;
	}

	public void setDueOn(Date dueOn) {
		this.dueOn = dueOn;
	}

	public int getIdDeleted() {
		return idDeleted;
	}

	public void setIdDeleted(int idDeleted) {
		this.idDeleted = idDeleted;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonrix.sms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abc
 */
@Entity
@Table(name = "tinyurlhit")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Tinyurlhit.findAll", query = "SELECT t FROM Tinyurlhit t") })
public class Tinyurlhit implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "hitid")
	private Integer hitid;
	@Column(name = "mediaid")
	private Integer mediaid;
	@Column(name = "ipaddress")
	private String ipaddress;
	@Column(name = "country")
	private String country;
	@Column(name = "city")
	private String city;
	@Basic(optional = false)
	@Column(name = "hiton")
	@Temporal(TemporalType.TIMESTAMP)
	private Date hiton;
	@Column(name = "device")
	private String device;
	@Column(name = "browser")
	private String browser;
	@Column(name = "isp")
	private String isp;

	public Tinyurlhit() {
	}

	public Tinyurlhit(Integer hitid) {
		this.hitid = hitid;
	}

	public Tinyurlhit(Integer hitid, Date hiton) {
		this.hitid = hitid;
		this.hiton = hiton;
	}

	public Integer getHitid() {
		return hitid;
	}

	public void setHitid(Integer hitid) {
		this.hitid = hitid;
	}

	public Integer getMediaid() {
		return mediaid;
	}

	public void setMediaid(Integer mediaid) {
		this.mediaid = mediaid;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getHiton() {
		return hiton;
	}

	public void setHiton(Date hiton) {
		this.hiton = hiton;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (hitid != null ? hitid.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Tinyurlhit)) {
			return false;
		}
		Tinyurlhit other = (Tinyurlhit) object;
		if ((this.hitid == null && other.hitid != null) || (this.hitid != null && !this.hitid.equals(other.hitid))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bonrix.sms.model.Tinyurlhit[ hitid=" + hitid + " ]";
	}

}

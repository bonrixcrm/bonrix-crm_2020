package com.bonrix.sms.model;

import javax.websocket.Session;

import org.java_websocket.WebSocket;

public class WebSocketObj {

	
	@Override
	public String toString() {
		return "WebSocketObj [tcalletId=" + tcalletId + ", clientCode=" + clientCode + ", status=" + status + ", skt="
				+ skt + "]";
	}
	public String tcalletId;
	public String clientCode;
	public Boolean status;
	public WebSocket skt;
	/**
	 * 
	 * @return the tcalletId
	 */
	public String getTcalletId() {
		return tcalletId;
	}
	/**
	 * @param tcalletId the tcalletId to set
	 */
	public void setTcalletId(String tcalletId) {
		this.tcalletId = tcalletId;
	}
	/**
	 * 
	 * @return the clientCode
	 */
	public String getClientCode() {
		return clientCode;
	}
	/**
	 * @param clientCode the clientCode to set
	 */
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	/**
	 * 
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}
	/**
	 * 
	 * @return the skt
	 */
	public WebSocket getSkt() {
		return skt;
	}
	/**
	 * @param skt the skt to set
	 */
	public void setSkt(WebSocket skt) {
		this.skt = skt;
	}
	
	
	
}

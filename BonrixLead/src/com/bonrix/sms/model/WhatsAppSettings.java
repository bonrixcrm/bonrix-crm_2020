package com.bonrix.sms.model;

import com.bonrix.sms.model.WhatsAppSettings;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "whatsappsettings")
public class WhatsAppSettings {
  @Id
  @GeneratedValue
  @Column(name = "id")
  private long id;
  
  @Column(name = "whatsapp_number")
  private String whatsapp_number;
  
  @Column(name = "token")
  private String token;
  
  @Column(name = "phone_number_id")
  private String phone_number_id;
  
  @Column(name = "whatsapp_business_account_id")
  private String whatsapp_business_account_id;
  
  @Column(name = "media_url")
  private String media_url;
  
  @Column(name = "user_id")
  private long user_id;
  
  public long getId() {
    return this.id;
  }
  
  public void setId(long id) {
    this.id = id;
  }
  
  public String getWhatsapp_number() {
    return this.whatsapp_number;
  }
  
  public void setWhatsapp_number(String whatsapp_number) {
    this.whatsapp_number = whatsapp_number;
  }
  
  public String getToken() {
    return this.token;
  }
  
  public void setToken(String token) {
    this.token = token;
  }
  
  public String getPhone_number_id() {
    return this.phone_number_id;
  }
  
  public void setPhone_number_id(String phone_number_id) {
    this.phone_number_id = phone_number_id;
  }
  
  public String getWhatsapp_business_account_id() {
    return this.whatsapp_business_account_id;
  }
  
  public void setWhatsapp_business_account_id(String whatsapp_business_account_id) {
    this.whatsapp_business_account_id = whatsapp_business_account_id;
  }
  
  public String getMedia_url() {
    return this.media_url;
  }
  
  public void setMedia_url(String media_url) {
    this.media_url = media_url;
  }
  
  public long getUser_id() {
    return this.user_id;
  }
  
  public void setUser_id(long user_id) {
    this.user_id = user_id;
  }
}

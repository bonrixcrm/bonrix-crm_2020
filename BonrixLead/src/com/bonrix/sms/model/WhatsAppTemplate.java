package com.bonrix.sms.model;

import com.bonrix.sms.model.WhatsAppTemplate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "whatsapptemplate")
public class WhatsAppTemplate {
  @Id
  @Column(name = "template_id")
  private long template_id;
  
  @Column(name = "template_name")
  private String template_name;
  
  @Lob
  @Column(name = "template_body", columnDefinition = "BLOB")
  private byte[] template_body;
  
  @Column(name = "template_status")
  private String template_status;
  
  @Column(name = "userid")
  private long userid;
  
  @Lob
  @Column(name = "header", columnDefinition = "BLOB")
  private byte[] header;
  
  @Column(name = "media_url")
  private String media_url;
  
  @Column(name = "media_template_type")
  private String media_template_type;
  
  @Column(name = "language")
  private String language;
  
  public long getTemplate_id() {
    return this.template_id;
  }
  
  public String getLanguage() {
    return this.language;
  }
  
  public void setLanguage(String language) {
    this.language = language;
  }
  
  public void setTemplate_id(long template_id) {
    this.template_id = template_id;
  }
  
  public String getTemplate_name() {
    return this.template_name;
  }
  
  public void setTemplate_name(String template_name) {
    this.template_name = template_name;
  }
  
  public byte[] getTemplate_body() {
    return this.template_body;
  }
  
  public void setTemplate_body(byte[] template_body) {
    this.template_body = template_body;
  }
  
  public String getTemplate_status() {
    return this.template_status;
  }
  
  public void setTemplate_status(String template_status) {
    this.template_status = template_status;
  }
  
  public long getUserid() {
    return this.userid;
  }
  
  public void setUserid(long userid) {
    this.userid = userid;
  }
  
  public byte[] getHeader() {
    return this.header;
  }
  
  public void setHeader(byte[] header) {
    this.header = header;
  }
  
  public String getMedia_url() {
    return this.media_url;
  }
  
  public void setMedia_url(String media_url) {
    this.media_url = media_url;
  }
  
  public String getMedia_template_type() {
    return this.media_template_type;
  }
  
  public void setMedia_template_type(String media_template_type) {
    this.media_template_type = media_template_type;
  }
}

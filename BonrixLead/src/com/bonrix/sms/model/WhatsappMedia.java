package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "whatsappmedia")
public class WhatsappMedia {

	@Id 
	@GeneratedValue
	@Column(name = "id")
	private long id; 
	
	@Column(name = "name")
	private String name; 
	
	@Column(name = "path")
	private String path;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "template_id", nullable = false)
	WhatsAppTemplate whatsAppTemplate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public WhatsAppTemplate getWhatsAppTemplate() {
		return whatsAppTemplate;
	}

	public void setWhatsAppTemplate(WhatsAppTemplate whatsAppTemplate) {
		this.whatsAppTemplate = whatsAppTemplate;
	}
	
	
}

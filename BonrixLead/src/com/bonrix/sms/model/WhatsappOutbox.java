package com.bonrix.sms.model;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "whatsappoutbox")
public class WhatsappOutbox {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
  
    @Column(name = "template_id", nullable = false)
    private Long templateId;

    @Column(name = "lead_id", nullable = false)
    private Long leadId;

    @Column(name = "company_id", nullable = false)
    private Long companyId;

    @Column(name = "telecaller_id", nullable = false)
    private Long telecallerId;

    @Column(name = "media_id", nullable = false)
    private Long mediaId;

    @Column(name = "sendtime", nullable = false)
    private Date sendTime;

    @Column(name = "request",  nullable = false)
    private String request;

    @Column(name = "response",  nullable = false)
    private String response;

    @Column(name = "status",  nullable = false)
    private String status;

    @Column(name = "msg_id", nullable = false)
    private String msgId;

    @Column(name = "to_number", nullable = false)
    private String toNumber;

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public Long getLeadId() {
        return leadId;
    }

    public void setLeadId(Long leadId) {
        this.leadId = leadId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getTelecallerId() {
        return telecallerId;
    }

    public void setTelecallerId(Long telecallerId) {
        this.telecallerId = telecallerId;
    }

    public Long getMediaId() {
        return mediaId;
    }

    public void setMediaId(Long mediaId) {
        this.mediaId = mediaId;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }
}

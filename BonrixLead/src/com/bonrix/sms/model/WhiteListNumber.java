/**
 * 
 */
package com.bonrix.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Bhavesh
 *
 */
@Entity
@Table(name = "whitelistnumber")
public class WhiteListNumber {
	@Id
	@GeneratedValue
	@Column(name = "wid")
	private int wid;

	@Column(name = "addedby")
	private long addedby;

	@Column(name = "whitenumber")
	private String whitenumber;

	public int getWid() {
		return wid;
	}

	public void setWid(int wid) {
		this.wid = wid;
	}

	public long getAddedby() {
		return addedby;
	}

	public void setAddedby(long addedby) {
		this.addedby = addedby;
	}

	public String getWhitenumber() {
		return whitenumber;
	}

	public void setWhitenumber(String whitenumber) {
		this.whitenumber = whitenumber;
	}

}

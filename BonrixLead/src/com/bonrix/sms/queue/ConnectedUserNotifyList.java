/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.queue;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.Session;

/**
 *
 * @author Bonrix
 */
public class ConnectedUserNotifyList {
	public static Map<String, Session> connectedClient = new ConcurrentHashMap<String, Session>();
	private static ConnectedUserNotifyList instance;

	public static ConnectedUserNotifyList getInstance() {
		synchronized (ConnectedUserNotifyList.class) {
			if (instance == null) {
				instance = new ConnectedUserNotifyList();
			}
		}
		return instance;
	}

	public Session AddClient(String key, Session value) {

		System.out.println("Adding to Notification userlist::" + key + "==" + new Date());
		return connectedClient.put(key, value);

	}

	public Session getClient(String key) {
		return connectedClient.get(key);
	}

	public Session RemoveClient(String key) {
		return connectedClient.remove(key);
	}

	public int getClientSize() {
		return connectedClient.size();

	}

}
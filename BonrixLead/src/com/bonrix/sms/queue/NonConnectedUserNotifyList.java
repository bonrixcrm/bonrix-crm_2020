/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.queue;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.bonrix.sms.model.RechargeMessage;
import com.bonrix.sms.model.SmsQueue;

/**
 *
 * @author Bonrix
 */
public class NonConnectedUserNotifyList {
	// public static Queue<E> connectedClient = new ConcurrentHashMap<String,
	// RechargeMessage>();
	public Queue<RechargeMessage> notifyqueue = new ConcurrentLinkedQueue<RechargeMessage>();
	private static NonConnectedUserNotifyList instance;

	public static NonConnectedUserNotifyList getInstance() {
		synchronized (NonConnectedUserNotifyList.class) {
			if (instance == null) {
				instance = new NonConnectedUserNotifyList();
			}
		}
		return instance;
	}

	public Boolean AddQueue(RechargeMessage value) {
		System.out.println("added to queue due to not connected: " + value.getTo());
		return this.notifyqueue.add(value);
	}

	public Boolean RemoveQueue(String key) {
		return notifyqueue.remove(key);
	}

	public int getClientSize() {
		return notifyqueue.size();

	}

}
package com.bonrix.sms.queue;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.Session;

public class Non_ConnectedList {

	public static Map<String, ArrayList<String>> connectedClient = new ConcurrentHashMap<String, ArrayList<String>>();
	private static Non_ConnectedList non_instance;

	public static Non_ConnectedList getInstance() {
		synchronized (Non_ConnectedList.class) {
			if (non_instance == null) {
				non_instance = new Non_ConnectedList();
			}
		}
		return non_instance;
	}

	public String AddClient(String key, ArrayList<String> value) {
		for (int i = 0; i < value.size(); i++) {
			connectedClient.put(key, value);
		}
		return connectedClient.toString();
	}

	public ArrayList<String> getClient(String key) {
		return connectedClient.get(key);
	}

	public ArrayList<String> RemoveClient(String key) {
		return connectedClient.remove(key);
	}

	public int getClientSize() {
		return connectedClient.size();

	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.queue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.bonrix.sms.model.SmsQueue;

/**
 *
 * @author bonrix
 */
public class QueueHigh {
	public Queue<SmsQueue> high = new ConcurrentLinkedQueue<SmsQueue>();
	private static QueueHigh instance;
	private static boolean isWorking = true;
	public static boolean isPaused = false;

	public static QueueHigh getInstance() {
		synchronized (QueueHigh.class) {
			if (instance == null) {
				isWorking = true;
				instance = new QueueHigh();
			}
		}
		return instance;
	}

	public static void stopFetching() {
		isWorking = false;
	}

	public static void startFetching() {
		isWorking = true;
	}

	public SmsQueue getSmsObject(int threadNo) {
		SmsQueue msgObj = null;
		try {
			// System.out.println("==QueueHigh: thread:["+threadNo+"]");
			if (isWorking) {
				if (high.size() > 0) {
					msgObj = high.poll();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return msgObj;
	}

	public int getQueueSize() {
		return high.size();

	}

}

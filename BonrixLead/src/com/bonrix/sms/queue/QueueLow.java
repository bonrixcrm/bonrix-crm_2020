/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.queue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.bonrix.sms.model.SmsQueue;

/**
 *
 * @author bonrix
 */
public class QueueLow {
	public static boolean isPaused = false;

	// private LinkedList low = new LinkedList();
	public Queue<SmsQueue> low = new ConcurrentLinkedQueue<SmsQueue>();
	private static QueueLow instance;
	private static boolean isWorking = true;

	public static QueueLow getInstance() {
		synchronized (QueueLow.class) {
			if (instance == null) {
				isWorking = true;
				instance = new QueueLow();
			}
		}
		return instance;
	}

	public static void stopFetching() {
		isWorking = false;
	}

	public static void startFetching() {
		isWorking = true;
	}

	public SmsQueue getSmsObject(int threadNo) {
		SmsQueue msgObj = null;
		try {
			// System.out.println("==QueueLow: thread:["+threadNo+"]");
			if (isWorking) {
				if (low.size() > 0) {
					msgObj = low.poll();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return msgObj;
	}

	public int getQueueSize() {
		return low.size();

	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.queue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.bonrix.sms.model.SmsQueue;

/**
 *
 * @author bonrix
 */
public class QueueMedium {
	public static boolean isPaused = false;
	// private LinkedList medium = new LinkedList();
	public Queue<SmsQueue> medium = new ConcurrentLinkedQueue<SmsQueue>();
	private static QueueMedium instance;
	private static boolean isWorking = true;

	public static QueueMedium getInstance() {
		synchronized (QueueMedium.class) {
			if (instance == null) {
				isWorking = true;
				instance = new QueueMedium();
			}
		}
		return instance;
	}

	public SmsQueue getSmsObject(int threadNo) {
		SmsQueue msgObj = null;
		try {
			// System.out.println("==QueueMedium: thread:["+threadNo+"]");
			if (isWorking) {
				if (medium.size() > 0) {
					msgObj = medium.poll();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return msgObj;
	}

	public static void stopFetching() {
		isWorking = false;
	}

	public static void startFetching() {
		isWorking = true;
	}

	public int getQueueSize() {
		return medium.size();
	}

}

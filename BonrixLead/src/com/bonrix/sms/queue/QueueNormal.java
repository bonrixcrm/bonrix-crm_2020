//****************************       QueueNormal      *************************************

package com.bonrix.sms.queue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.bonrix.sms.model.SmsQueue;

public class QueueNormal {
	public static boolean isPaused = false;

	// private LinkedList nor = new LinkedList();
	public Queue<SmsQueue> nor = new ConcurrentLinkedQueue<SmsQueue>();
	private static QueueNormal instance;
	private static boolean isWorking = true;

	public static QueueNormal getInstance() {
		synchronized (QueueNormal.class) {
			if (instance == null) {
				isWorking = true;
				instance = new QueueNormal();
			}
		}
		return instance;
	}

	public static void stopFetching() {
		isWorking = false;
	}

	public static void startFetching() {
		isWorking = true;
	}

	public int getQueueSize() {
		return nor.size();
	}

	public SmsQueue getSmsObject(int threadNo) {
		SmsQueue msgObj = null;
		try {
			// System.out.println("==QueueNormal: thread:["+threadNo+"]");
			if (isWorking) {
				if (nor.size() > 0) {
					msgObj = nor.poll();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return msgObj;
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////

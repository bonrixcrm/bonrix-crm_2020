/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.queue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.bonrix.sms.model.SmsQueue;
import com.google.common.util.concurrent.AtomicLongMap;

/**
 *
 * @author bonrix
 */
public class QueueUltraHigh {
	public static boolean isPaused = false;
	// private LinkedList ultraHigh = new LinkedList();
	public Queue<SmsQueue> ultraHigh1 = new ConcurrentLinkedQueue<SmsQueue>();
	public Queue<SmsQueue> ultraHigh2 = new ConcurrentLinkedQueue<SmsQueue>();
	public Queue<SmsQueue> ultraHigh3 = new ConcurrentLinkedQueue<SmsQueue>();
	public Queue<SmsQueue> ultraHigh4 = new ConcurrentLinkedQueue<SmsQueue>();
	private static QueueUltraHigh instance;

	public AtomicLongMap<String> stopsmpp = AtomicLongMap.create();

	private static boolean isWorking = true;

	public static QueueUltraHigh getInstance() {
		if (instance == null) {
			instance = new QueueUltraHigh();
		}
		return instance;
	}

	public SmsQueue getSmsObject(int channel) {

		if (isWorking) {
			if (channel == 1) {
				return getchannel1();
			} else if (channel == 2) {
				return getchannel2();
			} else if (channel == 3) {
				return getchannel3();
			} else {
				return getchannel4();
			}
		} else {
			return null;
		}
	}

	public SmsQueue getchannel1() {
		return ultraHigh1.poll();
	}

	public synchronized SmsQueue getchannel2() {
		return ultraHigh2.poll();
	}

	public synchronized SmsQueue getchannel3() {
		return ultraHigh3.poll();
	}

	public synchronized SmsQueue getchannel4() {
		return ultraHigh4.poll();
	}

	public static void stopFetching() {
		isWorking = false;
	}

	public static void startFetching() {
		isWorking = true;
	}

}

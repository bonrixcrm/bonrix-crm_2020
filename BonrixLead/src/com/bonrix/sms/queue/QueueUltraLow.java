/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bonrix.sms.queue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.bonrix.sms.model.SmsQueue;

/**
 *
 * @author bonrix
 */
public class QueueUltraLow {

	public static boolean isPaused = false;
	// private LinkedList ultraLow = new LinkedList();
	public Queue<SmsQueue> ultraLow = new ConcurrentLinkedQueue<SmsQueue>();
	private static QueueUltraLow instance;
	private static boolean isWorking = true;

	public static QueueUltraLow getInstance() {
		synchronized (QueueUltraLow.class) {
			if (instance == null) {
				isWorking = true;
				instance = new QueueUltraLow();
			}
		}
		return instance;
	}

	public static void stopFetching() {
		isWorking = false;
	}

	public static void startFetching() {
		isWorking = true;
	}

	public SmsQueue getSmsObject(int threadNo) {
		SmsQueue msgObj = null;
		try {
			// System.out.println("==QueueUltraLow: thread:["+threadNo+"]");
			if (isWorking) {
				if (ultraLow.size() > 0) {
					msgObj = ultraLow.poll();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return msgObj;
	}

	public int getQueueSize() {
		return ultraLow.size();
	}
}

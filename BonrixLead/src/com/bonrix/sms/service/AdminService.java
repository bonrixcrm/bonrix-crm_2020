package com.bonrix.sms.service;

import com.bonrix.sms.dto.GetScheduleWiseLead;
import com.bonrix.sms.dto.LeadDTO;
import com.bonrix.sms.model.AegeCustDetail;
import com.bonrix.sms.model.Android_mode;
import com.bonrix.sms.model.Androidbuttonflag;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.AutoDialLog;
import com.bonrix.sms.model.CategoryManager;
import com.bonrix.sms.model.Contacts;
import com.bonrix.sms.model.Crmemailsetting;
import com.bonrix.sms.model.Crmemailtemplate;
import com.bonrix.sms.model.DisplaySetting;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.ExtraFields;
import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.GroupName;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.Leadprocesstate;
import com.bonrix.sms.model.MessageAudit;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.MobileCallLog;
import com.bonrix.sms.model.Regexpatten;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.Sentsmslog;
import com.bonrix.sms.model.SmsCredit;
import com.bonrix.sms.model.Smssettings;
import com.bonrix.sms.model.Smstemplate;
import com.bonrix.sms.model.SystemNotification;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.Ticketmaster;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;
import com.bonrix.sms.service.AdminService;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface AdminService {
  void updateAssignCredit(SmsCredit paramSmsCredit);
  
  SmsCredit getSmsCredit(long paramLong1, long paramLong2);
  
  void saveMessageAudit(MessageAudit paramMessageAudit);
  
  User getUserByUid(Long paramLong);
  
  void deleteUser(Long paramLong);
  
  void deleteCredit(Long paramLong);
  
  void saveSenderName(SenderName paramSenderName);
  
  SenderName getSenderNameById(Long paramLong);
  
  void deleteSenderName(Long paramLong);
  
  void saveOrUpdateTamplate(Tamplate paramTamplate);
  
  Tamplate getTamplateById(Long paramLong);
  
  void deleteTamplate(Long paramLong);
  
  User getUserByUserName(String paramString);
  
  List getCreditByUid(long paramLong);
  
  List getUsersByUid(long paramLong);
  
  List getSenderNamesByUId(long paramLong);
  
  List getTamplatesByUid(long paramLong, boolean paramBoolean);
  
  void saveGroup(GroupName paramGroupName);
  
  int saveContects(List<Contacts> paramList);
  
  List getGroupNamesByUId(long paramLong);
  
  GroupName getGroupNameByGId(Long paramLong);
  
  void updateGroup(GroupName paramGroupName);
  
  void deleteUserByGroupId(Long paramLong);
  
  void deleteGroupByGroupId(Long paramLong);
  
  void updateSenderName(SenderName paramSenderName);
  
  String getContectsByGid(int paramInt1, int paramInt2, long paramLong, String paramString1, String paramString2, String paramString3, String paramString4);
  
  void deleteContactbyid(long paramLong);
  
  List getDisplaySetting();
  
  String getSmsAuditByAdmin(int paramInt1, int paramInt2, long paramLong, Date paramDate1, Date paramDate2, String paramString);
  
  void saveDisplaySetting(DisplaySetting paramDisplaySetting);
  
  DisplaySetting getDisplaySettingById(long paramLong);
  
  void updateDisplaySetting(DisplaySetting paramDisplaySetting);
  
  void deleteDisplaySettingById(long paramLong);
  
  void resetPassword(String paramString1, String paramString2);
  
  long saveHostSetting(HostSetting paramHostSetting);
  
  List getHostSettings();
  
  List getCRMHostSettings(int paramInt);
  
  HostSetting getHostSetting(String paramString);
  
  long updateHostSetting(HostSetting paramHostSetting);
  
  void deleteHostSettingById(long paramLong);
  
  int updateStatusUser(Long paramLong, int paramInt);
  
  int updateDefaultSender(Long paramLong, int paramInt);
  
  SystemNotification saveSystemNotification(SystemNotification paramSystemNotification);
  
  MessageTemplate getMessageTemplate(long paramLong, String paramString);
  
  EmailTemplate getEmailTemplate(long paramLong, String paramString);
  
  void deleteEmailTemplate(Long paramLong);
  
  void deleteMessageTemplate(Long paramLong);
  
  List<SmsCredit> getSmsCreditByUid(Long paramLong);
  
  List<User> getChildByUid(long paramLong);
  
  List<EmailTemplate> getEmailTemplatebyUid(long paramLong);
  
  List<MessageTemplate> getMessageTemplatebyUid(long paramLong);
  
  Long saveMessageTemplate(MessageTemplate paramMessageTemplate);
  
  Long saveEmailTemplate(EmailTemplate paramEmailTemplate);
  
  MessageTemplate getMessageTemplateById(long paramLong);
  
  int updateMessageTemplate(MessageTemplate paramMessageTemplate);
  
  EmailTemplate getMessageEmailById(long paramLong);
  
  int updateEmailTemplate(EmailTemplate paramEmailTemplate);
  
  GroupName getGroupByNamenUid(String paramString, Long paramLong);
  
  DisplaySetting getDisplaySettingByURL(String paramString);
  
  void addTallyCaller(Tallycaller paramTallycaller);
  
  List getTeleCallerByUid(long paramLong);
  
  List getTeleCallerForAdmin(long paramLong);
  
  void UnassignTallyCallerStaff(Long paramLong);
  
  void assignTallyCallerStaff(Long paramLong1, Long paramLong2);
  
  List getAssignTellyCaller(Long paramLong);
  
  List getCategoryList(Long paramLong);
  
  void DeleteCategory(Long paramLong);
  
  void UpdateCategory(CategoryManager paramCategoryManager);
  
  void AddCategory(CategoryManager paramCategoryManager);
  
  List GetAssignLeadState(int paramInt);
  
  void deleteState(int paramInt);
  
  void UpdateLeadstate(Leadprocesstate paramLeadprocesstate);
  
  void AddState(Leadprocesstate paramLeadprocesstate);
  
  List GetCategory(int paramInt);
  
  void AddLead(Lead paramLead);
  
  List GetLead(int paramInt1, int paramInt2, int paramInt3);
  
  List AndroidLeadSearch(String paramString, int paramInt1, int paramInt2);
  
  void deleteLead(int paramInt);
  
  List GetCompanyLead(int paramInt);
  
  List GetLeadCategoryName(Long paramLong);
  
  List GetLeadState(Long paramLong);
  
  void UpdateLead(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, int paramInt2, String paramString12);
  
  List getTellyCaller(Long paramLong);
  
  void UpdateTallycaller(String paramString);
  
  void deleteTellyCaller(int paramInt);
  
  void addTellyCaller(Tallycaller paramTallycaller);
  
  List getLeadForAssign(long paramLong, int paramInt);
  
  List<Object[]> getTelecallerCount(int paramInt);
  
  List<Object[]> getUserLimit(Long paramLong);
  
  void UnassignLeadUpdate(Long paramLong);
  
  void assignLeadUpdate(Long paramLong1, Long paramLong2);
  
  void AddCSVDataField(String paramString1, String paramString2);
  
  Tallycaller TallyCallerByName(String paramString);
  
  List getCategoryLeadCount(String paramString);
  
  List getCategoryLead(String paramString1, String paramString2);
  
  List getTallyCallerLead(String paramString);
  
  int updateSheduleDate(int paramInt, String paramString);
  
  int updateLeadProcessState(int paramInt, String paramString);
  
  int getfollowupsCount(int paramInt);
  
  List getFollowupsHistory(int paramInt);
  
  List getLeadData(int paramInt);
  
  void addFollowupsHistory(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt1, String paramString7, int paramInt2, String paramString8, int paramInt3, int paramInt4, String paramString9, String paramString10);
  
  List getPaggingLead(String paramString, int paramInt1, int paramInt2);
  
  List getPaggingLeadByCategory(String paramString1, String paramString2, int paramInt1, int paramInt2);
  
  List getLeadByContactNo(String paramString);
  
  int addLeadData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13);
  
  List remainLead(int paramInt);
  
  List GetContact(long paramLong);
  
  String updateContactInfo(int paramInt, String paramString1, String paramString2, long paramLong);
  
  String deleteContactInfo(int paramInt);
  
  void addContactInfo(String paramString1, String paramString2, String paramString3, int paramInt);
  
  User getUidByUserName(String paramString);
  
  List GetLeadToAddCustomer(int paramInt);
  
  int compareLeadContact(String paramString, int paramInt);
  
  Ticketmaster addTicket(String paramString1, String paramString2, String paramString3, String paramString4);
  
  List getAgentByCompany(int paramInt);
  
  List getDepartment(int paramInt);
  
  List getAgentByCompanyId(int paramInt);
  
  List getRequestInfo(String paramString);
  
  List getRecentHistory();
  
  List GetOpenTickect();
  
  List GetUnassignTickect();
  
  List GetOverdueticket();
  
  List GetTickectByTicketId(int paramInt);
  
  List GetTicketDetail(int paramInt);
  
  List getContactInfoByEmail(String paramString);
  
  List GetTicketChatHistory(int paramInt);
  
  void updateTicketProprities(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3);
  
  List GetContactInfo(int paramInt);
  
  List GetTickectByEmailId(String paramString1, String paramString2, String paramString3, String paramString4);
  
  List getTicketIdByEmailId(String paramString);
  
  void addTicketchildMST(int paramInt1, int paramInt2, String paramString);
  
  Ticketmaster getTicketObjectById(int paramInt);
  
  void sendTicketWithStatus(int paramInt, String paramString);
  
  List GetAllTickect();
  
  List GetContactInfoDetail(int paramInt);
  
  void updateContactInfoDetail(int paramInt, String paramString1, String paramString2, String paramString3);
  
  void deleteContactInfoDetail(int paramInt);
  
  void AddContactInfoDetail(int paramInt, String paramString1, String paramString2, String paramString3);
  
  void addFullTicket(Ticketmaster paramTicketmaster);
  
  AegeCustDetail getCompanyIdByAgentId(int paramInt);
  
  List GetDueToday();
  
  List GetStaff(int paramInt);
  
  User AddStaff(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt);
  
  void addRole(String paramString);
  
  Tallycaller GetTellyCallerByName(String paramString);
  
  Lead GetLeadObjectByLeadId(int paramInt);
  
  Tallycaller GetTellyCallerById(int paramInt);
  
  void addRegeX(String paramString1, String paramString2, int paramInt1, String paramString3, String paramString4, String paramString5, int paramInt2, int paramInt3);
  
  List GetTellyCallerBycompanyId(int paramInt);
  
  List GetImapSettingByStaffId(int paramInt);
  
  void UpdateImapSetting(String paramString1, String paramString2, int paramInt);
  
  void addImapSetting(String paramString1, String paramString2, int paramInt);
  
  List getIMAPSetting();
  
  Regexpatten getregexpatten(String paramString1, String paramString2);
  
  List GetTemplate(int paramInt, String paramString);
  
  void updateTemplate(int paramInt, String paramString1, String paramString2);
  
  void deleteTemplate(int paramInt);
  
  void deactiveTemplate(int paramInt);
  
  void activeTemplate(int paramInt);
  
  UserRole getUserRole(int paramInt);
  
  void addTemplate(Smstemplate paramSmstemplate);
  
  List getTemplateAPI(Tallycaller paramTallycaller);
  
  List getTemplateByTemplateId(String paramString);
  
  void saveLog(Sentsmslog paramSentsmslog);
  
  List GetAPI(int paramInt);
  
  List GetAllAPI(int paramInt);
  
  List<Tallycaller> getTallycallerById(int paramInt);
  
  List getLeadProcessState(int paramInt);
  
  List getLeadSuccessStatus(int paramInt);
  
  void addAPI(Smssettings paramSmssettings);
  
  Smssettings getSMSSettings(int paramInt);
  
  void updateAPI(int paramInt, String paramString1, String paramString2, String paramString3);
  
  void deleteAPI(int paramInt);
  
  void activeAPI(int paramInt1, int paramInt2);
  
  List GetContactInfoDetailSMS(int paramInt);
  
  List getLeadDataSMS(int paramInt1, int paramInt2, String paramString1, String paramString2);
  
  void CloseLead(int paramInt);
  
  List getSentSMSLog(int paramInt1, String paramString1, String paramString2, String paramString3, int paramInt2);
  
  List getTodaySentSMSLog(int paramInt, String paramString1, String paramString2, String paramString3);
  
  List GetCloseStatusAPI(int paramInt);
  
  void AddFailFollowUpAPI(Followupshistory paramFollowupshistory);
  
  List GetSuccessStatusAPI(int paramInt);
  
  void UpdateStaff(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt2);
  
  void DeleteStaff(int paramInt);
  
  List getCRMEmailTemplate(int paramInt, String paramString);
  
  void AddLeadToAutoDial(int paramInt, String paramString);
  
  List GetPaggingAutoDialLead(int paramInt1, int paramInt2, int paramInt3);
  
  String GetAllAutoDialLead(int paramInt1, int paramInt2, int paramInt3);
  
  List GetAllAutoDialLead(int paramInt);
  
  List GetAutoDialLead(int paramInt);
  
  void AddAllLeadToAutoDial(int paramInt, String paramString);
  
  void updateEmailTemplateDetail(int paramInt, String paramString1, String paramString2, String paramString3);
  
  void addContactInfoByRol(String paramString1, String paramString2, long paramLong, int paramInt, String paramString3);
  
  List GetLeadByLeadIdAPI(int paramInt);
  
  List GetLeadParameter(int paramInt);
  
  List GetTelecaller(int paramInt);
  
  List GetLeadByCategoryId(String paramString);
  
  void AssignLeadToTelecaller(int paramInt1, int paramInt2);
  
  void DeleteLeadBeforeAssign(int paramInt);
  
  ApiKey ValidateAPI(String paramString);
  
  List GetContactDetaails(long paramLong, String paramString);
  
  List ValidateLead(String paramString1, String paramString2, long paramLong);
  
  List getAllLeadOfContactNo(String paramString1, String paramString2);
  
  List getLeadIdAndCategoryName(String paramString, int paramInt);
  
  List GetLeadDetailByMobileNoAPI(String paramString);
  
  List getCategoryAPI(int paramInt);
  
  List getCategoryById(int paramInt);
  
  List GetSuccessCallLog(int paramInt);
  
  List GetImapSettingByCompanyId(int paramInt);
  
  List getTallyCallerByCompanyId(int paramInt);
  
  List GetInOutBoundSuccessCallLog(int paramInt1, String paramString1, String paramString2, int paramInt2, int paramInt3);
  
  List GetDateRangeSuccessCallLog(int paramInt, String paramString1, String paramString2);
  
  List GetFailCallLog(int paramInt);
  
  List GetInOutBoundFailCallLog(int paramInt1, String paramString1, String paramString2, int paramInt2, int paramInt3);
  
  List GetDateRangeFailCallLog(int paramInt, String paramString1, String paramString2);
  
  List getAllTimeCallLog(int paramInt);
  
  List getStatistic(String paramString, int paramInt);
  
  List getTodayTimeCallLog(int paramInt);
  
  List getOpenColseLeadCountByTelecaller(int paramInt);
  
  List getOpenCloseLeadCountByCategory(int paramInt);
  
  void AddcategoryLeadToAutoDial(int paramInt1, int paramInt2);
  
  int AddTelecallrLeadToAutoDial(int paramInt, String paramString);
  
  String DefaultLeadAssignToTelecaller(int paramInt1, int paramInt2);
  
  List GeneraetDashboard(String paramString, int paramInt);
  
  List GetCategoryLeadCount(int paramInt);
  
  void AddEmailTemplateDetail(int paramInt, String paramString1, String paramString2, String paramString3);
  
  Crmemailtemplate getTemplateInfo(int paramInt);
  
  List getEmailTemplateAPI(int paramInt);
  
  Crmemailsetting getCrmEmailSetting(int paramInt);
  
  Smstemplate getSmsTemplateByCompIdNdCatId(int paramInt1, int paramInt2);
  
  List getMessageTemplateByCompanyId(int paramInt);
  
  String AssignDefaultMsgTemp(int paramInt1, int paramInt2);
  
  List getEmailTemplateByCompanyId(int paramInt);
  
  String AssignDefaultEmailTemp(int paramInt1, int paramInt2);
  
  List GetLeadByAdvanceSearch(String paramString);
  
  List GetSuccessStatus(long paramLong);
  
  void UpdateSuccessState(int paramInt1, String paramString, int paramInt2);
  
  void deleteSuccessStae(Long paramLong);
  
  void AddSuccessState(String paramString, int paramInt1, int paramInt2);
  
  void DeleteContact(long paramLong);
  
  List GetLeadParameterForSearch(String paramString);
  
  List SuccessStatusSearch(String paramString);
  
  List getLeadForCSV(int paramInt, String paramString1, String paramString2);
  
  List getTelecallerStateLeadCount(String paramString);
  
  User GetUserData(int paramInt);
  
  void updateMasterPassword(String paramString);
  
  List getDueTodayLead(int paramInt, String paramString1, String paramString2);
  
  void AddTelecallerStartWork(int paramInt);
  
  void AddTelecallerBreakTime(int paramInt);
  
  void AddTelecallerEndWork(int paramInt);
  
  void AddTelecallerEndBreakTime(int paramInt);
  
  List getTelecallerFollowupReport(int paramInt, String paramString1, String paramString2);
  
  Tallycaller validateDesktopAPI(String paramString);
  
  List getLeadByCompanyIdNDmobileNo(String paramString, int paramInt);
  
  Tallycaller GetTelecallerByMobileNo(String paramString);
  
  String ConvertDataToWebJSON(String paramString1, String paramString2, int paramInt1, int paramInt2);
  
  List GetTelecallerWorkDetails(int paramInt, String paramString);
  
  Tallycaller validateTelecallerDesktopAPI(String paramString);
  
  List GetTelecallerDateWiseDetails(int paramInt1, int paramInt2, String paramString);
  
  void assignIVRFOllowup(long paramLong1, long paramLong2);
  
  List GetIVRWorkDetails(int paramInt, String paramString);
  
  void updateLeadComment(String paramString, int paramInt);
  
  List getTag(int paramInt);
  
  List getAllTag(int paramInt);
  
  void addTag(int paramInt, String paramString);
  
  void addTagToLead(int paramInt, String paramString);
  
  void updateTagToCompany(int paramInt, String paramString);
  
  void deleteTagToCompany(int paramInt);
  
  void updateNotificationFlag(int paramInt);
  
  void addNotification(int paramInt, String paramString);
  
  List getNotification(int paramInt);
  
  void UpdateNotificationSatus(int paramInt);
  
  void saveEmailLog(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, String paramString4, int paramInt3, String paramString5);
  
  void saveMobileCallLog(MobileCallLog paramMobileCallLog);
  
  int getLeadCountByMobileNo(String paramString, int paramInt);
  
  void SendCRMMail(Crmemailsetting paramCrmemailsetting, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, Crmemailtemplate paramCrmemailtemplate, String paramString6, Tallycaller paramTallycaller, AdminService paramAdminService, String paramString7);
  
  void deleteCRMEmailTemplate(long paramLong);
  
  void updateCRMHostSetting(String paramString1, String paramString2, String paramString3, int paramInt);
  
  void deleteCRMHostSetting(int paramInt);
  
  void addCRMHostSetting(Crmemailsetting paramCrmemailsetting);
  
  int validateNoCategory(String paramString, int paramInt);
  
  List geLeadByMobileNumber(int paramInt, String paramString);
  
  List geLeadByMobileNumberAPI(int paramInt, String paramString);
  
  void updateCSVdata(int paramInt, String paramString);
  
  List GetMonthlyLeadCount(int paramInt);
  
  Tallycaller getTallycallerByIdMdel(int paramInt);
  
  User getComapnyByTeclId(int paramInt);
  
  List GetMonthlySuccessFailLogCount(int paramInt);
  
  List GetDailySuccessFailLogCount(int paramInt);
  
  Followupshistory getFollowupsHistoryByFID(int paramInt);
  
  int updateFollowUpByAudioFile(int paramInt1, String paramString, int paramInt2);
  
  List GetCountByLeadState(int paramInt);
  
  List GetCountBySuccessState(int paramInt);
  
  List GetCountByFailState(int paramInt);
  
  List GetTelecallerCallCount(int paramInt);
  
  String updateCallStatus(int paramInt, String paramString);
  
  Followupshistory findById(int paramInt);
  
  Followupshistory GetCallLog(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void AddLeadToAhtoDialByCompanyId(int paramInt);
  
  List<Lead> getLeadsByCompanyId(int paramInt);
  
  List<Lead> getUnassignLeads(int paramInt);
  
  List<Lead> getElapseSheduleLeads(int paramInt);
  
  int addRemailSheduleLeadsToAutoDial(int paramInt);
  
  List<Lead> GetMyLead(int paramInt);
  
  void AddRemoveLeadToAutoDialByLeadId(int paramInt1, int paramInt2);
  
  int AddUassignLeadToAutoDial(int paramInt);
  
  List<Android_mode> GetConstant();
  
  List<Lead> GetScheduleTodayLead(int paramInt);
  
  List<Lead> GetTodayLead(int paramInt);
  
  void addAllMyLeadsToAutoDial(int paramInt);
  
  int addScheduleTodayLeadsToAutoDial(int paramInt);
  
  void addAllTodayLeadsLeadsToAutoDial(int paramInt);
  
  void addAllTodayAssignLeadsLeadsToAutoDial(int paramInt);
  
  List<Lead> GetTodayAssignLead(int paramInt);
  
  List GetDailyLeadCount(int paramInt);
  
  int UpdateDemoQuery(String paramString);
  
  List<Androidbuttonflag> GetAndroidButtonConstant();
  
  List<Lead> getAutoDialLeadObject(int paramInt);
  
  List excuteListQuery(String paramString);
  
  int addOrRemoveLeadsFromAutoDial(String paramString, int paramInt);
  
  void sstest();
  
  List<GetScheduleWiseLead> getScheduleWiseLead(int paramInt);
  
  List getGetScheduleLeadScheduler(int paramInt1, int paramInt2);
  
  List<LeadDTO> getFininceScheduleWiseLead(int paramInt);
  
  List<Lead> incredibleservicesLeads(int paramInt);
  
  void addAllLeadsToAutoDial(int paramInt);
  
  AutoDialLog GetAutoDial(int paramInt);
  
  CategoryManager getCategoryByNaame(String paramString, int paramInt);
  
  List GetMissCallLead(int paramInt);
  
  List GetCatAutoDialLead(int paramInt1, int paramInt2);
  
  List<String> getUsersBySchedule();
  
  List<String> gettocken(String paramString);
  
  List<Object[]> gettelecallerID();
  
  int updatefcmid(String paramString1, String paramString2);
  
  List<Object[]> getEllapsetelecallerID();
  
  Object saveObject(Object paramObject);
  
  String getSMSTemplate(long paramLong, String paramString);
  
  List getAndroidTallyCallerLead(String paramString, int paramInt1, int paramInt2);
  
  List getAndroidCategoryLead(String paramString1, String paramString2, int paramInt1, int paramInt2);
  
  List getAndroidLeadByCompanyIdNDmobileNo(String paramString, int paramInt1, int paramInt2, int paramInt3);
  
  String getAndroTellyCount(String paramString);
  
  String getAndroidCountByCompanyIdNDmobileNo(String paramString, int paramInt);
  
  void addLeadToAutoDialManager(int paramInt1, int paramInt2);
  
  List<Object[]> GetLeadStateByManager(int paramInt);
  
  List<Object[]> getTallyCallerByManager(int paramInt);
  
  String getTallyCallerLeadCount(int paramInt, String paramString);
  
  String getLeadStateCount(String paramString, int paramInt);
  
  List getLeadByLeadState(int paramInt, String paramString);
  
  List getFollowupsByLead(int paramInt);
  
  List<Object[]> getTallyCallerByMobile(String paramString);
  
  List<Object[]> getWhiteDetails(String paramString);
  
  int tcallerChangePass(int paramInt, String paramString);
  
  List<Object[]> getLeadSuccessStatByLeadStat(String paramString);
  
  List getCategoryByManager(String paramString);
  
  void saveExtraFields(ExtraFields paramExtraFields);
  
  List GetCatColumns(int paramInt);
  
  void deleteCatCol(Long paramLong);
  
  List getAndroFailCallList(String paramString);
  
  List<ExtraFields> getFieldsByUId(int paramInt1, int paramInt2);
  
  List<Object[]> getLeadByCatComp(int paramInt1, int paramInt2);
  
  int updateCSVJSON(int paramInt, String paramString);
  
  List getCategoryAutoDialLeadCount(String paramString);
  
  List GetAndroidCatAutoDialLead(int paramInt1, int paramInt2);
  
  List<Object[]> getLeadByCatTele(int paramInt1, int paramInt2);
  
  List<ExtraFields> getFieldsByUId(int paramInt);
  
  CategoryManager getCategory(int paramInt);
  
  List<Object[]> getLeadProStateById(int paramInt);
    
  int extraFieldCount(int paramInt1, int paramInt2, String paramString);
  
  List<Object[]> getHashmukhLeadCount(String paramString1, String paramString2);
  
  int updateFailCallLeadStat(String paramString);
  
  int getCountTele(String paramString);
  
  int updateTelecallerAccessDate(int paramInt, String paramString);
  
  List<Object[]> getTeleLastAccess(int paramInt);
  
  List<Object[]> getExtraParamsByCat(int paramInt1, int paramInt2);
  
  void deleteUserRole(int paramInt);
  
  List GetCatAutoDialSingalLead(int paramInt1, int paramInt2);
  
  List<Object[]> getLeadCatAdvAutoDial(String paramString1, int paramInt, String paramString2, String paramString3);
  
  List GetAdvanceCatAutoDialLead(int paramInt1, int paramInt2, String paramString1, String paramString2);
  
  void updateLead(String paramString);
  
  List<Lead> getDuplicateLeads(String paramString, int paramInt);
  
  void deleteLeadById(int paramInt);
  
  void AddAutoDial(String paramString);
  
  List getDueTodayLead(int paramInt);
  
  List GetAllLeads(String paramString);
  
 // void UpdateLead(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, int paramInt2, String paramString12);
}

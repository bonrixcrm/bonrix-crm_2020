package com.bonrix.sms.service;

import com.bonrix.sms.controller.SendCRMMail;
import com.bonrix.sms.dao.AdminDao;
import com.bonrix.sms.dao.UserDao;
import com.bonrix.sms.dto.GetScheduleWiseLead;
import com.bonrix.sms.dto.LeadDTO;
import com.bonrix.sms.model.AegeCustDetail;
import com.bonrix.sms.model.Android_mode;
import com.bonrix.sms.model.Androidbuttonflag;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.AutoDialLog;
import com.bonrix.sms.model.CategoryManager;
import com.bonrix.sms.model.Contacts;
import com.bonrix.sms.model.Crmemailsetting;
import com.bonrix.sms.model.Crmemailtemplate;
import com.bonrix.sms.model.DisplaySetting;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.ExtraFields;
import com.bonrix.sms.model.Followupshistory;
import com.bonrix.sms.model.GroupName;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.Lead;
import com.bonrix.sms.model.Leadprocesstate;
import com.bonrix.sms.model.MessageAudit;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.MobileCallLog;
import com.bonrix.sms.model.Regexpatten;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.Sentsmslog;
import com.bonrix.sms.model.SmsCredit;
import com.bonrix.sms.model.Smssettings;
import com.bonrix.sms.model.Smstemplate;
import com.bonrix.sms.model.SystemNotification;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.Ticketmaster;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;
import com.bonrix.sms.service.AdminService;
import com.bonrix.sms.service.AdminServiceImpl;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("AdminService")
@Transactional
public class AdminServiceImpl implements AdminService {
  @Autowired
  private UserDao userDao;
  
  @Autowired
  private AdminDao adminDao;
  
  public void updateAssignCredit(SmsCredit smsCredit) {
    this.adminDao.updateAssignCredit(smsCredit);
  }
  
  public SmsCredit getSmsCredit(long uid, long stid) {
    return this.adminDao.getSmsCredit(uid, stid);
  }
  
  public void saveMessageAudit(MessageAudit messageAudit) {
    this.adminDao.saveMessageAudit(messageAudit);
  }
  
  public User getUserByUid(Long uid) {
    return this.adminDao.getUserByUid(uid);
  }
  
  public void deleteUser(Long id) {
    this.adminDao.deleteUser(id);
  }
  
  public void deleteCredit(Long id) {
    this.adminDao.deleteCredit(id);
  }
  
  public void saveSenderName(SenderName senderName2) {
    this.adminDao.saveSenderName(senderName2);
  }
  
  public SenderName getSenderNameById(Long sid) {
    return this.adminDao.getSenderNameById(sid);
  }
  
  public void deleteSenderName(Long sid) {
    this.adminDao.deleteSenderName(sid);
  }
  
  public void saveOrUpdateTamplate(Tamplate tamplate) {
    this.adminDao.saveOrUpdateTamplate(tamplate);
  }
  
  public Tamplate getTamplateById(Long tid) {
    return this.adminDao.getTamplateById(tid);
  }
  
  public void deleteTamplate(Long tid) {
    this.adminDao.deleteTamplate(tid);
  }
  
  public User getUserByUserName(String userName) {
    return this.adminDao.getUserByUserName(userName);
  }
  
  public List getCreditByUid(long userid) {
    return this.adminDao.getCreditByUid(userid);
  }
  
  public List getUsersByUid(long userid) {
    return this.adminDao.getUsersByUid(userid);
  }
  
  public List getSenderNamesByUId(long l) {
    return this.adminDao.getSenderNamesByUId(l);
  }
  
  public List getTamplatesByUid(long l, boolean istrans) {
    return this.adminDao.getTamplatesByUid(l, istrans);
  }
  
  public void saveGroup(GroupName grouname) {
    this.adminDao.saveGroup(grouname);
  }
  
  public int saveContects(List<Contacts> contacts) {
    return this.adminDao.saveContects(contacts);
  }
  
  public List getGroupNamesByUId(long userid) {
    return this.adminDao.getGroupNamesByUId(userid);
  }
  
  public GroupName getGroupNameByGId(Long id) {
    return this.adminDao.getGroupNameByGId(id);
  }
  
  public void updateGroup(GroupName grouname) {
    this.adminDao.updateGroup(grouname);
  }
  
  public void deleteUserByGroupId(Long id) {
    this.adminDao.deleteUserByGroupId(id);
  }
  
  public void deleteGroupByGroupId(Long id) {
    this.adminDao.deleteGroupByGroupId(id);
  }
  
  public void updateSenderName(SenderName senderName) {
    this.adminDao.updateSenderName(senderName);
  }
  
  public String getContectsByGid(int page, int listSize, long userid, String name, String email, String number, String gid) {
    return this.adminDao.getContectsByGid(page, listSize, userid, name, email, number, gid);
  }
  
  public void deleteContactbyid(long parseLong) {
    this.adminDao.deleteContactbyid(parseLong);
  }
  
  public List getDisplaySetting() {
    return this.adminDao.getDisplaySetting();
  }
  
  public void saveDisplaySetting(DisplaySetting displaySetting) {
    this.adminDao.saveDisplaySetting(displaySetting);
  }
  
  public DisplaySetting getDisplaySettingById(long parseLong) {
    return this.adminDao.getDisplaySettingById(parseLong);
  }
  
  public void updateDisplaySetting(DisplaySetting displaySetting) {
    this.adminDao.updateDisplaySetting(displaySetting);
  }
  
  public void deleteDisplaySettingById(long l) {
    this.adminDao.deleteDisplaySettingById(l);
  }
  
  public void resetPassword(String username, String password) {
    this.adminDao.resetPassword(username, password);
  }
  
  public long saveHostSetting(HostSetting hostSetting) {
    return this.adminDao.saveHostSetting(hostSetting);
  }
  
  public List getHostSettings() {
    return this.adminDao.getHostSettings();
  }
  
  public List getCRMHostSettings(int id) {
    return this.adminDao.getCRMHostSettings(id);
  }
  
  public HostSetting getHostSetting(String user) {
    return this.adminDao.getHostSetting(user);
  }
  
  public long updateHostSetting(HostSetting hostSetting) {
    return this.adminDao.updateHostSetting(hostSetting);
  }
  
  public void deleteHostSettingById(long l) {
    this.adminDao.deleteHostSettingById(l);
  }
  
  public int updateStatusUser(Long id, int status) {
    return this.adminDao.updateStatusUser(id, status);
  }
  
  public MessageTemplate getMessageTemplate(long userid, String type) {
    return this.adminDao.getMessageTemplate(userid, type);
  }
  
  public EmailTemplate getEmailTemplate(long userid, String type) {
    return this.adminDao.getEmailTemplate(userid, type);
  }
  
  public void deleteEmailTemplate(Long id) {
    this.adminDao.deleteEmailTemplate(id);
  }
  
  public void deleteMessageTemplate(Long id) {
    this.adminDao.deleteMessageTemplate(id);
  }
  
  public List<SmsCredit> getSmsCreditByUid(Long id) {
    return this.adminDao.getSmsCreditByUid(id);
  }
  
  public int updateDefaultSender(Long id, int status) {
    return this.adminDao.updateDefaultSender(id, status);
  }
  
  public SystemNotification saveSystemNotification(SystemNotification sysnf) {
    return this.adminDao.saveSystemNotification(sysnf);
  }
  
  public String getSmsAuditByAdmin(int page, int listSize, long uid, Date sdate, Date edate, String transtype) {
    return this.adminDao.getSmsAuditByAdmin(page, listSize, uid, sdate, edate, transtype);
  }
  
  public List<User> getChildByUid(long userid) {
    return this.adminDao.getChildByUid(userid);
  }
  
  public List<EmailTemplate> getEmailTemplatebyUid(long uid) {
    return this.adminDao.getEmailTemplatebyUid(uid);
  }
  
  public List<MessageTemplate> getMessageTemplatebyUid(long uid) {
    return this.adminDao.getMessageTemplatebyUid(uid);
  }
  
  public Long saveMessageTemplate(MessageTemplate messageTemplate) {
    return this.adminDao.saveMessageTemplate(messageTemplate);
  }
  
  public Long saveEmailTemplate(EmailTemplate emailTemplate) {
    return this.adminDao.saveEmailTemplate(emailTemplate);
  }
  
  public MessageTemplate getMessageTemplateById(long id) {
    return this.adminDao.getMessageTemplateById(id);
  }
  
  public int updateMessageTemplate(MessageTemplate messageTemplate) {
    return this.adminDao.updateMessageTemplate(messageTemplate);
  }
  
  public EmailTemplate getMessageEmailById(long id) {
    return this.adminDao.getMessageEmailById(id);
  }
  
  public int updateEmailTemplate(EmailTemplate emailTemplate) {
    return this.adminDao.updateEmailTemplate(emailTemplate);
  }
  
  public GroupName getGroupByNamenUid(String gname, Long uid) {
    return this.adminDao.getGroupByNamenUid(gname, uid);
  }
  
  public DisplaySetting getDisplaySettingByURL(String url) {
    return this.adminDao.getDisplaySettingByURL(url);
  }
  
  public void addTallyCaller(Tallycaller tcaller) {
    this.adminDao.addtallyCaller(tcaller);
  }
  
  public List getTeleCallerByUid(long userid) {
    return this.adminDao.getTeleCallerByUid(userid);
  }
  
  public List getTeleCallerForAdmin(long userid) {
    return this.adminDao.getTeleCallerForAdmin(userid);
  }
  
  public void UnassignTallyCallerStaff(Long id) {
    System.out.println("Long Id Admin :" + id);
    this.adminDao.UnassignTallyCallerStaff(id);
  }
  
  public void assignTallyCallerStaff(Long id, Long TallyCaller_id) {
    this.adminDao.assignTallyCallerStaff(id, TallyCaller_id);
  }
  
  public List getAssignTellyCaller(Long id) {
    return null;
  }
  
  public List getCategoryList(Long Id) {
    return this.adminDao.getCategoryList(Id);
  }
  
  public void DeleteCategory(Long catId) {
    this.adminDao.DeleteCategory(catId);
  }
  
  public void UpdateCategory(CategoryManager catmanager) {
    this.adminDao.UpdateCategory(catmanager);
  }
  
  public void AddCategory(CategoryManager catmanager) {
    this.adminDao.AddCategory(catmanager);
  }
  
  public List GetAssignLeadState(int compId) {
    return this.adminDao.GetAssignLeadState(compId);
  }
  
  public void deleteState(int ststeId) {
    this.adminDao.deleteState(ststeId);
  }
  
  public void UpdateLeadstate(Leadprocesstate lpsd) {
    this.adminDao.UpdateLeadstate(lpsd);
  }
  
  public void AddState(Leadprocesstate ldst) {
    this.adminDao.AddState(ldst);
  }
  
  public List GetCategory(int CompId) {
    return this.adminDao.GetCategory(CompId);
  }
  
  public void AddLead(Lead lead) {
    this.adminDao.AddLead(lead);
  }
  
  public List GetLead(int CompId, int page, int listSize) {
    return this.adminDao.GetLead(CompId, page, listSize);
  }
  
  public void deleteLead(int leadId) {
    this.adminDao.deleteLead(leadId);
  }
  
  public List GetCompanyLead(int leadId) {
    return this.adminDao.GetCompanyLead(leadId);
  }
  
  public List GetLeadCategoryName(Long id) {
    return this.adminDao.GetLeadCategoryName(id);
  }
  
  public List GetLeadState(Long id) {
    return this.adminDao.GetLeadState(id);
  }
  
  public void UpdateLead(int id, String fname, String lname, String email, String mobno, String cat, String sataus, String compName, String webAdd, String cont, String stat, String cty, int tcaller, String updatealtmobileNo) {
    this.adminDao.UpdateLead(id, fname, lname, email, mobno, cat, sataus, compName, webAdd, cont, stat, cty, tcaller, updatealtmobileNo);
  }
  
  public List getTellyCaller(Long id) {
    return this.adminDao.getTellyCaller(id.longValue());
  }
  
  public void UpdateTallycaller(String comastr) {
    this.adminDao.UpdateTallycaller(comastr);
  }
  
  public void deleteTellyCaller(int tcId) {
    this.adminDao.deleteTellyCaller(tcId);
  }
  
  public void addTellyCaller(Tallycaller tc) {
    this.adminDao.addTellyCaller(tc);
  }
  
  public List getLeadForAssign(long userid, int companyId) {
    return this.adminDao.getLeadForAssign(userid, companyId);
  }
  
  public void UnassignLeadUpdate(Long LeadId) {
    this.adminDao.UnassignLeadUpdate(LeadId);
  }
  
  public void assignLeadUpdate(Long LeadId, Long tcId) {
    this.adminDao.assignLeadUpdate(LeadId, tcId);
  }
  
  public void AddCSVDataField(String field, String value) {}
  
  public Tallycaller TallyCallerByName(String uname) {
    return this.adminDao.TallyCallerByName(uname);
  }
  
  public List getCategoryLeadCount(String uname) {
    return this.adminDao.getCategoryLeadCount(uname);
  }
  
  public List getCategoryLead(String username, String catname) {
    return this.adminDao.getCategoryLead(username, catname);
  }
  
  public List getTallyCallerLead(String uname) {
    return this.adminDao.getTallyCallerLead(uname);
  }
  
  public int updateSheduleDate(int leadId, String sheDate) {
    int rowcont = this.adminDao.updateSheduleDate(leadId, sheDate);
    return rowcont;
  }
  
  public int getfollowupsCount(int leadId) {
    int rowcont = this.adminDao.getfollowupsCount(leadId);
    return rowcont;
  }
  
  public int updateLeadProcessState(int leadId, String state) {
    return this.adminDao.updateLeadProcessState(leadId, state);
  }
  
  public List getFollowupsHistory(int lead_id) {
    return this.adminDao.getFollowupsHistory(lead_id);
  }
  
  public List getLeadData(int lead_id) {
    return this.adminDao.getLeadData(lead_id);
  }
  
  public void addFollowupsHistory(String tallycalleName, String callingTime, String remark, String sheduleTime, String newStatus, String audoiFilePath, int callDuration, String callStatus, int leadId, String followupType, int compId, int second, String successStatus, String followupDevice) {
    this.adminDao.addFollowupsHistory(tallycalleName, callingTime, remark, sheduleTime, newStatus, audoiFilePath, 
        callDuration, callStatus, leadId, followupType, compId, second, successStatus, followupDevice);
  }
  
  public List getPaggingLead(String userName, int limit, int pageNo) {
    return this.adminDao.getPaggingLead(userName, limit, pageNo);
  }
  
  public List getPaggingLeadByCategory(String userName, String catName, int limt, int pageNo) {
    return this.adminDao.getPaggingLeadByCategory(userName, catName, limt, pageNo);
  }
  
  public List getLeadByContactNo(String contactNo) {
    return this.adminDao.getLeadByContactNo(contactNo);
  }
  
  public int addLeadData(String userName, String firstName, String lastName, String email, String companytNam, String catName, String mobileNo, String webSite, String company, String country, String state, String city, String remark) {
    int i = this.adminDao.addLeadData(userName, firstName, lastName, email, companytNam, catName, mobileNo, webSite, 
        company, country, state, city, remark);
    return i;
  }
  
  public List remainLead(int tcId) {
    return this.adminDao.remainLead(tcId);
  }
  
  public List GetContact(long uid) {
    return this.adminDao.GetContact(uid);
  }
  
  public String updateContactInfo(int cid, String cname, String email, long cno) {
    return this.adminDao.updateContactInfo(cid, cname, email, cno);
  }
  
  public String deleteContactInfo(int cid) {
    return this.adminDao.deleteContactInfo(cid);
  }
  
  public void addContactInfo(String cname, String email, String cno, int cmpId) {
    this.adminDao.addContactInfo(cname, email, cno, cmpId);
  }
  
  public User getUidByUserName(String userName) {
    return this.adminDao.getUidByUserName(userName);
  }
  
  public List GetLeadToAddCustomer(int compId) {
    return this.adminDao.GetLeadToAddCustomer(compId);
  }
  
  public int compareLeadContact(String contactNo, int uid) {
    return this.adminDao.compareLeadContact(contactNo, uid);
  }
  
  public Ticketmaster addTicket(String email, String subject, String filepath, String desc) {
    return this.adminDao.addTicket(email, subject, filepath, desc);
  }
  
  public List getDepartment(int companyId) {
    return this.adminDao.getDepartment(companyId);
  }
  
  public List getAgentByCompany(int id) {
    return this.adminDao.getAgentByCompany(id);
  }
  
  public List getAgentByCompanyId(int companyId) {
    return this.adminDao.getAgentByCompanyId(companyId);
  }
  
  public List getRequestInfo(String email) {
    return this.adminDao.getRequestInfo(email);
  }
  
  public List getRecentHistory() {
    return this.adminDao.getRecentHistory();
  }
  
  public List GetOpenTickect() {
    return this.adminDao.GetOpenTickect();
  }
  
  public List GetUnassignTickect() {
    return this.adminDao.GetUnassignTickect();
  }
  
  public List GetOverdueticket() {
    return this.adminDao.GetOverdueticket();
  }
  
  public List GetTickectByTicketId(int tkid) {
    return this.adminDao.GetTickectByTicketId(tkid);
  }
  
  public List GetTicketDetail(int tcid) {
    return this.adminDao.GetTicketDetail(tcid);
  }
  
  public List getContactInfoByEmail(String email) {
    return this.adminDao.getContactInfoByEmail(email);
  }
  
  public List GetTicketChatHistory(int tkid) {
    return this.adminDao.GetTicketChatHistory(tkid);
  }
  
  public void updateTicketProprities(String status, String priority, int tickId, int departmentId, int agentId) {
    this.adminDao.updateTicketProprities(status, priority, tickId, departmentId, agentId);
  }
  
  public List GetContactInfo(int agentId) {
    return this.adminDao.GetContactInfo(agentId);
  }
  
  public List GetTickectByEmailId(String emailId, String todate, String fromdate, String status) {
    return this.adminDao.GetTickectByEmailId(emailId, todate, fromdate, status);
  }
  
  public List getTicketIdByEmailId(String emailId) {
    return this.adminDao.getTicketIdByEmailId(emailId);
  }
  
  public void addTicketchildMST(int tickId, int agentId, String msg) {
    this.adminDao.addTicketchildMST(tickId, agentId, msg);
  }
  
  public Ticketmaster getTicketObjectById(int id) {
    return this.adminDao.getTicketObjectById(id);
  }
  
  public void sendTicketWithStatus(int tkid, String status) {
    this.adminDao.sendTicketWithStatus(tkid, status);
  }
  
  public List GetAllTickect() {
    return this.adminDao.GetAllTickect();
  }
  
  public List GetContactInfoDetail(int agentId) {
    return this.adminDao.GetContactInfoDetail(agentId);
  }
  
  public void updateContactInfoDetail(int id, String name, String email, String cno) {
    this.adminDao.updateContactInfoDetail(id, name, email, cno);
  }
  
  public void deleteContactInfoDetail(int cid) {
    this.adminDao.deleteContactInfo(cid);
  }
  
  public void AddContactInfoDetail(int agentid, String name, String email, String cno) {
    this.adminDao.AddContactInfoDetail(agentid, name, email, cno);
  }
  
  public void addFullTicket(Ticketmaster tmst) {
    this.adminDao.addFullTicket(tmst);
  }
  
  public AegeCustDetail getCompanyIdByAgentId(int agentid) {
    return this.adminDao.getCompanyIdByAgentId(agentid);
  }
  
  public List GetDueToday() {
    return this.adminDao.GetDueToday();
  }
  
  public List GetStaff(int id) {
    return this.adminDao.GetStaff(id);
  }
  
  public User AddStaff(String username, String email, String name, String mob, String pass, String city, int companyId) {
    return this.adminDao.AddStaff(username, email, name, mob, pass, city, companyId);
  }
  
  public void addRole(String uname) {
    this.adminDao.addRole(uname);
  }
  
  public Tallycaller GetTellyCallerByName(String tcallerName) {
    return this.adminDao.GetTellyCallerByName(tcallerName);
  }
  
  public Lead GetLeadObjectByLeadId(int leadId) {
    return this.adminDao.GetLeadObjectByLeadId(leadId);
  }
  
  public Tallycaller GetTellyCallerById(int tcallerId) {
    return this.adminDao.GetTellyCallerById(tcallerId);
  }
  
  public void addRegeX(String from, String subject, int category, String namepatten, String emailpatten, String mobilepatten, int tallycallerId, int companyId) {
    this.adminDao.addRegeX(from, subject, category, namepatten, emailpatten, mobilepatten, tallycallerId, companyId);
  }
  
  public List GetTellyCallerBycompanyId(int companyId) {
    return this.adminDao.GetTellyCallerBycompanyId(companyId);
  }
  
  public List GetImapSettingByStaffId(int StaffId) {
    return this.adminDao.GetImapSettingByStaffId(StaffId);
  }
  
  public void UpdateImapSetting(String email, String password, int staffId) {
    this.adminDao.UpdateImapSetting(email, password, staffId);
  }
  
  public void addImapSetting(String email, String password, int staffId) {
    this.adminDao.addImapSetting(email, password, staffId);
  }
  
  public List getIMAPSetting() {
    return this.adminDao.getIMAPSetting();
  }
  
  public Regexpatten getregexpatten(String from, String subject) {
    return this.adminDao.getregexpatten(from, subject);
  }
  
  public List GetTemplate(int Id, String rol) {
    return this.adminDao.GetTemplate(Id, rol);
  }
  
  public void updateTemplate(int tempid, String tempname, String tempdesc) {
    this.adminDao.updateTemplate(tempid, tempname, tempdesc);
  }
  
  public void deleteTemplate(int tempid) {
    this.adminDao.deleteTemplate(tempid);
  }
  
  public void deactiveTemplate(int tempid) {
    this.adminDao.deactiveTemplate(tempid);
  }
  
  public void activeTemplate(int tempid) {
    this.adminDao.activeTemplate(tempid);
  }
  
  public UserRole getUserRole(int uid) {
    return this.adminDao.getUserRole(uid);
  }
  
  public void addTemplate(Smstemplate temp) {
    this.adminDao.addTemplate(temp);
  }
  
  public List getTemplateAPI(Tallycaller tcaler) {
    return this.adminDao.getTemplateAPI(tcaler);
  }
  
  public List getTemplateByTemplateId(String id) {
    return this.adminDao.getTemplateByTemplateId(id);
  }
  
  public void saveLog(Sentsmslog log) {
    this.adminDao.saveLog(log);
  }
  
  public List GetAPI(int id) {
    return this.adminDao.GetAPI(id);
  }
  
  public List GetAllAPI(int id) {
    return this.adminDao.GetAllAPI(id);
  }
  
  public List<Tallycaller> getTallycallerById(int Id) {
    return this.adminDao.getTallycallerById(Id);
  }
  
  public List getLeadProcessState(int compId) {
    return this.adminDao.getLeadProcessState(compId);
  }
  
  public void addAPI(Smssettings setings) {
    this.adminDao.addAPI(setings);
  }
  
  public Smssettings getSMSSettings(int id) {
    return this.adminDao.getSMSSettings(id);
  }
  
  public void updateAPI(int id, String key, String provider, String desc) {
    this.adminDao.updateAPI(id, key, provider, desc);
  }
  
  public void deleteAPI(int id) {
    this.adminDao.deleteAPI(id);
  }
  
  public void activeAPI(int APIid, int Id) {
    this.adminDao.activeAPI(APIid, Id);
  }
  
  public List GetContactInfoDetailSMS(int uid) {
    return this.adminDao.GetContactInfoDetailSMS(uid);
  }
  
  public List getLeadDataSMS(int uid, int catId, String lpstste, String lstate) {
    return this.adminDao.getLeadDataSMS(uid, catId, lpstste, lstate);
  }
  
  public void CloseLead(int leadId) {
    this.adminDao.CloseLead(leadId);
  }
  
  public List getSentSMSLog(int id, String startDate, String endDate, String role, int mobNo) {
    return this.adminDao.getSentSMSLog(id, startDate, endDate, role, mobNo);
  }
  
  public List getTodaySentSMSLog(int id, String stDate, String endDate, String rol) {
    return this.adminDao.getTodaySentSMSLog(id, stDate, endDate, rol);
  }
  
  public List GetCloseStatusAPI(int tcId) {
    return this.adminDao.GetCloseStatusAPI(tcId);
  }
  
  public void AddFailFollowUpAPI(Followupshistory history) {
    this.adminDao.AddFailFollowUpAPI(history);
  }
  
  public List GetSuccessStatusAPI(int tcId) {
    return this.adminDao.GetSuccessStatusAPI(tcId);
  }
  
  public void UpdateStaff(int sid, String username, String email, String name, String mob, String password, String city, int companyId) {
    this.adminDao.UpdateStaff(sid, username, email, name, mob, password, city, companyId);
  }
  
  public void DeleteStaff(int sid) {
    this.adminDao.DeleteStaff(sid);
  }
  
  public List getCRMEmailTemplate(int finalId, String rol) {
    return this.adminDao.getCRMEmailTemplate(finalId, rol);
  }
  
  public void AddLeadToAutoDial(int lid, String status) {
    this.adminDao.AddLeadToAutoDial(lid, status);
  }
  
  public List GetPaggingAutoDialLead(int tcid, int limit, int pgno) {
    return this.adminDao.GetPaggingAutoDialLead(tcid, limit, pgno);
  }
  
  public String GetAllAutoDialLead(int tcid, int page, int listSize) {
    return this.adminDao.GetAllAutoDialLead(tcid, page, listSize).toString();
  }
  
  public List GetAllAutoDialLead(int tcid) {
    return this.adminDao.GetAllAutoDialLead(tcid);
  }
  
  public void AddAllLeadToAutoDial(int tcid, String status) {
    this.adminDao.AddAllLeadToAutoDial(tcid, status);
  }
  
  public void updateEmailTemplateDetail(int tid, String tsubject, String tText, String tempName) {
    this.adminDao.updateEmailTemplateDetail(tid, tsubject, tText, tempName);
  }
  
  public void addContactInfoByRol(String cname, String email, long cno, int companyId, String roll) {
    this.adminDao.addContactInfoByRol(cname, email, cno, companyId, roll);
  }
  
  public List GetLeadByLeadIdAPI(int leadId) {
    return this.adminDao.GetLeadByLeadIdAPI(leadId);
  }
  
  public List GetLeadParameter(int company_id) {
    return this.adminDao.GetLeadParameter(company_id);
  }
  
  public List GetTelecaller(int id) {
    return this.adminDao.GetTelecaller(id);
  }
  
  public List GetLeadByCategoryId(String query) {
    return this.adminDao.GetLeadByCategoryId(query);
  }
  
  public void AssignLeadToTelecaller(int leadId, int tcid) {
    this.adminDao.AssignLeadToTelecaller(leadId, tcid);
  }
  
  public ApiKey ValidateAPI(String api) {
    return this.adminDao.ValidateAPI(api);
  }
  
  public List GetContactDetaails(long uid, String mobile) {
    return this.adminDao.GetContactDetaails(uid, mobile);
  }
  
  public List ValidateLead(String mobile, String categoty, long tcId) {
    return this.adminDao.ValidateLead(mobile, categoty, tcId);
  }
  
  public List getAllLeadOfContactNo(String mobileNo, String telecallerId) {
    return this.adminDao.getAllLeadOfContactNo(mobileNo, telecallerId);
  }
  
  public List getLeadIdAndCategoryName(String mobno, int CompanyId) {
    return this.adminDao.getLeadIdAndCategoryName(mobno, CompanyId);
  }
  
  public List GetLeadDetailByMobileNoAPI(String parseInt) {
    return this.adminDao.GetLeadDetailByMobileNoAPI(parseInt);
  }
  
  public List getCategoryAPI(int companyId) {
    return this.adminDao.GetCategory(companyId);
  }
  
  public List GetImapSettingByCompanyId(int companyId) {
    return this.adminDao.GetImapSettingByCompanyId(companyId);
  }
  
  public List getTallyCallerByCompanyId(int companyId) {
    return this.adminDao.getTallyCallerByCompanyId(companyId);
  }
  
  public List GetSuccessCallLog(int userId) {
    return this.adminDao.GetSuccessCallLog(userId);
  }
  
  public List GetInOutBoundSuccessCallLog(int userId, String callType, String stDate, int catId, int tcId) {
    return this.adminDao.GetInOutBoundSuccessCallLog(userId, callType, stDate, catId, tcId);
  }
  
  public List GetDateRangeSuccessCallLog(int userId, String stdate, String endate) {
    return this.adminDao.GetDateRangeSuccessCallLog(userId, stdate, endate);
  }
  
  public List GetFailCallLog(int userId) {
    return this.adminDao.GetFailCallLog(userId);
  }
  
  public List GetInOutBoundFailCallLog(int userId, String callType, String startDt, int catId, int tcId) {
    return this.adminDao.GetInOutBoundFailCallLog(userId, callType, startDt, catId, tcId);
  }
  
  public List GetDateRangeFailCallLog(int userId, String stdate, String endate) {
    return this.adminDao.GetDateRangeFailCallLog(userId, stdate, endate);
  }
  
  public List getAllTimeCallLog(int userId) {
    return this.adminDao.getAllTimeCallLog(userId);
  }
  
  public List getStatistic(String statisticType, int userId) {
    return this.adminDao.getStatistic(statisticType, userId);
  }
  
  public List getTodayTimeCallLog(int userId) {
    return this.adminDao.getTodayTimeCallLog(userId);
  }
  
  public List getOpenColseLeadCountByTelecaller(int userId) {
    return this.adminDao.getOpenColseLeadCountByTelecaller(userId);
  }
  
  public List getOpenCloseLeadCountByCategory(int userId) {
    return this.adminDao.getOpenCloseLeadCountByCategory(userId);
  }
  
  public void AddcategoryLeadToAutoDial(int catId, int autoDialVal) {
    this.adminDao.AddcategoryLeadToAutoDial(catId, autoDialVal);
  }
  
  public int AddTelecallrLeadToAutoDial(int tcId, String status) {
    return this.adminDao.AddTelecallrLeadToAutoDial(tcId, status);
  }
  
  public String DefaultLeadAssignToTelecaller(int catId, int tcId) {
    return this.adminDao.DefaultLeadAssignToTelecaller(catId, tcId);
  }
  
  public List GeneraetDashboard(String leadProcessStatus, int userId) {
    return this.adminDao.GeneraetDashboard(leadProcessStatus, userId);
  }
  
  public List GetCategoryLeadCount(int userId) {
    return this.adminDao.GetCategoryLeadCount(userId);
  }
  
  public void AddEmailTemplateDetail(int userId, String tempName, String subject, String text) {
    this.adminDao.AddEmailTemplateDetail(userId, tempName, subject, text);
  }
  
  public Crmemailtemplate getTemplateInfo(int tempId) {
    return this.adminDao.getTemplateInfo(tempId);
  }
  
  public List getEmailTemplateAPI(int compId) {
    return this.adminDao.getEmailTemplateAPI(compId);
  }
  
  public Crmemailsetting getCrmEmailSetting(int companyId) {
    return this.adminDao.getCrmEmailSetting(companyId);
  }
  
  public Smstemplate getSmsTemplateByCompIdNdCatId(int compId, int catId) {
    return this.adminDao.getSmsTemplateByCompIdNdCatId(compId, catId);
  }
  
  public List getCategoryById(int catId) {
    return this.adminDao.getCategoryById(catId);
  }
  
  public List getMessageTemplateByCompanyId(int userId) {
    return this.adminDao.getMessageTemplateByCompanyId(userId);
  }
  
  public String AssignDefaultMsgTemp(int catId, int tempId) {
    return this.adminDao.AssignDefaultMsgTemp(catId, tempId);
  }
  
  public List getEmailTemplateByCompanyId(int userId) {
    return this.adminDao.getEmailTemplateByCompanyId(userId);
  }
  
  public String AssignDefaultEmailTemp(int catId, int tempId) {
    return this.adminDao.AssignDefaultEmailTemp(catId, tempId);
  }
  
  public List GetLeadByAdvanceSearch(String sb) {
    return this.adminDao.GetLeadByAdvanceSearch(sb);
  }
  
  public List GetSuccessStatus(long id) {
    return this.adminDao.GetSuccessStatus(id);
  }
  
  public void UpdateSuccessState(int ststeId, String stateName, int leadStatId) {
    this.adminDao.UpdateSuccessState(ststeId, stateName, leadStatId);
  }
  
  public void deleteSuccessStae(Long statusId) {
    this.adminDao.deleteSuccessStae(statusId);
  }
  
  public void AddSuccessState(String stateName, int CompId, int leadStateId) {
    this.adminDao.AddSuccessState(stateName, CompId, leadStateId);
  }
  
  public void DeleteContact(long contactId) {
    this.adminDao.DeleteCategory(Long.valueOf(contactId));
  }
  
  public List GetLeadParameterForSearch(String query) {
    return this.adminDao.GetLeadParameterForSearch(query);
  }
  
  public List getLeadForCSV(int compId, String catId, String mobileNo) {
    return this.adminDao.getLeadForCSV(compId, catId, mobileNo);
  }
  
  public List getTelecallerStateLeadCount(String query) {
    return this.adminDao.getTelecallerStateLeadCount(query);
  }
  
  public User GetUserData(int company_id) {
    return this.adminDao.GetUserData(company_id);
  }
  
  public void updateMasterPassword(String query) {
    this.adminDao.updateMasterPassword(query);
  }
  
  public List getDueTodayLead(int tcId, String stDate, String enDate) {
    return this.adminDao.getDueTodayLead(tcId, stDate, enDate);
  }
  
  public void AddTelecallerStartWork(int tcId) {
    this.adminDao.AddTelecallerStartWork(tcId);
  }
  
  public void AddTelecallerBreakTime(int tcId) {
    this.adminDao.AddTelecallerBreakTime(tcId);
  }
  
  public void AddTelecallerEndWork(int tcId) {
    this.adminDao.AddTelecallerEndWork(tcId);
  }
  
  public void AddTelecallerEndBreakTime(int parseInt) {
    this.adminDao.AddTelecallerEndBreakTime(parseInt);
  }
  
  public List GetAutoDialLead(int tcid) {
    return this.adminDao.GetAutoDialLead(tcid);
  }
  
  public List getTelecallerFollowupReport(int companyId, String startDate, String endDate) {
    return this.adminDao.getTelecallerFollowupReport(companyId, startDate, endDate);
  }
  
  public Tallycaller validateDesktopAPI(String apikey) {
    return this.adminDao.validateDesktopAPI(apikey);
  }
  
  public List getLeadByCompanyIdNDmobileNo(String mobNo, int companyId) {
    return this.adminDao.getLeadByCompanyIdNDmobileNo(mobNo, companyId);
  }
  
  public Tallycaller GetTelecallerByMobileNo(String mobNo) {
    return this.adminDao.GetTelecallerByMobileNo(mobNo);
  }
  
  public String ConvertDataToWebJSON(String queryCOunt, String query, int page, int listSize) {
    return this.adminDao.ConvertDataToWebJSON(queryCOunt, query, page, listSize);
  }
  
  public List GetTelecallerWorkDetails(int companyId, String getDate) {
    return this.adminDao.GetTelecallerWorkDetails(companyId, getDate);
  }
  
  public Tallycaller validateTelecallerDesktopAPI(String api) {
    return this.adminDao.validateTelecallerDesktopAPI(api);
  }
  
  public List GetTelecallerDateWiseDetails(int compid, int tcId, String getDate) {
    return this.adminDao.GetTelecallerDateWiseDetails(compid, tcId, getDate);
  }
  
  public void assignIVRFOllowup(long leadId, long tcId) {
    this.adminDao.assignIVRFOllowup(leadId, tcId);
  }
  
  public List GetIVRWorkDetails(int tcId, String getDate) {
    return this.adminDao.GetIVRWorkDetails(tcId, getDate);
  }
  
  public void updateLeadComment(String comment, int id) {
    this.adminDao.updateLeadComment(comment, id);
  }
  
  public List getTag(int leadId) {
    return this.adminDao.getTag(leadId);
  }
  
  public List getAllTag(int companyId) {
    return this.adminDao.getAllTag(companyId);
  }
  
  public void addTag(int companyId, String tagName) {
    this.adminDao.addTag(companyId, tagName);
  }
  
  public void addTagToLead(int leadId, String tagList) {
    this.adminDao.addTagToLead(leadId, tagList);
  }
  
  public void updateTagToCompany(int tagId, String tagName) {
    this.adminDao.updateTagToCompany(tagId, tagName);
  }
  
  public void deleteTagToCompany(int tagId) {
    this.adminDao.deleteTagToCompany(tagId);
  }
  
  public void updateNotificationFlag(int leadId) {
    this.adminDao.updateNotificationFlag(leadId);
  }
  
  public void addNotification(int tcallerid, String html_body) {
    this.adminDao.addNotification(tcallerid, html_body);
  }
  
  public List getNotification(int tcallerid) {
    return this.adminDao.getNotification(tcallerid);
  }
  
  public void UpdateNotificationSatus(int notiId) {
    this.adminDao.UpdateNotificationSatus(notiId);
  }
  
  public void saveEmailLog(String filesname, int leadId, String temp_Text, String sub, int tallycallerId, String Status, int CompanyId, String mail_error) {
    this.adminDao.saveEmailLog(filesname, leadId, temp_Text, sub, tallycallerId, Status, CompanyId, mail_error);
  }
  
  public void saveMobileCallLog(MobileCallLog log) {
    this.adminDao.saveMobileCallLog(log);
  }
  
  public int getLeadCountByMobileNo(String mobileNo, int compantId) {
    return this.adminDao.getLeadCountByMobileNo(mobileNo, compantId);
  }
  
  @Async
  public void SendCRMMail(Crmemailsetting emailSetting, String sendto, String sub, String tallycallerId, String leadId, String filesname, Crmemailtemplate temp, String filePath, Tallycaller tcaler, AdminService adminService, String status) {
    (new SendCRMMail()).send(emailSetting, sendto, sub, tallycallerId, leadId, filesname, 
        temp, filePath, tcaler, adminService, status);
  }
  
  public void deleteCRMEmailTemplate(long id) {
    this.adminDao.deleteCRMEmailTemplate(id);
  }
  
  public void updateCRMHostSetting(String hostName, String emailId, String password, int id) {
    this.adminDao.updateCRMHostSetting(hostName, emailId, password, id);
  }
  
  public void deleteCRMHostSetting(int id) {
    this.adminDao.deleteCRMHostSetting(id);
  }
  
  public void addCRMHostSetting(Crmemailsetting setting) {
    this.adminDao.addCRMHostSetting(setting);
  }
  
  public int validateNoCategory(String string, int id) {
    return this.adminDao.validateNoCategory(string, id);
  }
  
  public List geLeadByMobileNumber(int userid, String mobileNo) {
    return this.adminDao.geLeadByMobileNumber(userid, mobileNo);
  }
  
  public List geLeadByMobileNumberAPI(int tcallerid, String mobileNo) {
    return this.adminDao.geLeadByMobileNumberAPI(tcallerid, mobileNo);
  }
  
  public void updateCSVdata(int leadId, String csv) {
    this.adminDao.updateCSVdata(leadId, csv);
  }
  
  public List GetMonthlyLeadCount(int userId) {
    return this.adminDao.GetMonthlyLeadCount(userId);
  }
  
  public Tallycaller getTallycallerByIdMdel(int teleId) {
    return this.adminDao.getTallycallerByIdMdel(teleId);
  }
  
  public User getComapnyByTeclId(int companyId) {
    return this.adminDao.getComapnyByTeclId(companyId);
  }
  
  public List GetMonthlySuccessFailLogCount(int userId) {
    return this.adminDao.GetMonthlySuccessFailLogCount(userId);
  }
  
  public List GetDailySuccessFailLogCount(int userId) {
    return this.adminDao.GetDailySuccessFailLogCount(userId);
  }
  
  public Followupshistory getFollowupsHistoryByFID(int fId) {
    return this.adminDao.getFollowupsHistoryByFID(fId);
  }
  
  public int updateFollowUpByAudioFile(int fId, String fileName, int callDuration) {
    return this.adminDao.updateFollowUpByAudioFile(fId, fileName, callDuration);
  }
  
  public List GetCountByLeadState(int userId) {
    return this.adminDao.GetCountByLeadState(userId);
  }
  
  public List GetCountBySuccessState(int userId) {
    return this.adminDao.GetCountBySuccessState(userId);
  }
  
  public List GetCountByFailState(int userId) {
    return this.adminDao.GetCountByFailState(userId);
  }
  
  public List GetTelecallerCallCount(int userId) {
    return this.adminDao.GetTelecallerCallCount(userId);
  }
  
  public String updateCallStatus(int parseInt, String string) {
    return this.adminDao.updateCallStatus(parseInt, string);
  }
  
  public Followupshistory findById(int id) {
    return this.adminDao.findById(id);
  }
  
  public Followupshistory GetCallLog(int id, int start_date, int end_date, int call_satus) {
    return this.adminDao.GetCallLog(id, start_date, end_date, call_satus);
  }
  
  public void AddLeadToAhtoDialByCompanyId(int companyId) {
    this.adminDao.AddLeadToAhtoDialByCompanyId(companyId);
  }
  
  public List<Lead> getLeadsByCompanyId(int compId) {
    return this.adminDao.getLeadsByCompanyId(compId);
  }
  
  public List<Lead> getUnassignLeads(int companyId) {
    return this.adminDao.getUnassignLeads(companyId);
  }
  
  public List<Lead> getElapseSheduleLeads(int companyId) {
    return this.adminDao.getElapseSheduleLeads(companyId);
  }
  
  public int addRemailSheduleLeadsToAutoDial(int tcallerid) {
    return this.adminDao.addRemailSheduleLeadsToAutoDial(tcallerid);
  }
  
  public List<Lead> GetMyLead(int tcallerid) {
    return this.adminDao.GetMyLead(tcallerid);
  }
  
  public void AddRemoveLeadToAutoDialByLeadId(int leadId, int i) {
    this.adminDao.AddRemoveLeadToAutoDialByLeadId(leadId, i);
  }
  
  public int AddUassignLeadToAutoDial(int companyId) {
    return this.adminDao.AddUassignLeadToAutoDial(companyId);
  }
  
  public List<Android_mode> GetConstant() {
    return this.adminDao.GetConstant();
  }
  
  public List<Lead> GetScheduleTodayLead(int tcallerid) {
    return this.adminDao.GetScheduleTodayLead(tcallerid);
  }
  
  public List<Lead> GetTodayLead(int tcallerid) {
    return this.adminDao.GetTodayLead(tcallerid);
  }
  
  public void addAllMyLeadsToAutoDial(int tcallerid) {
    this.adminDao.addAllMyLeadsToAutoDial(tcallerid);
  }
  
  public int addScheduleTodayLeadsToAutoDial(int tcallerid) {
    return this.adminDao.addScheduleTodayLeadsToAutoDial(tcallerid);
  }
  
  public void addAllTodayLeadsLeadsToAutoDial(int tcallerid) {
    this.adminDao.addAllTodayLeadsLeadsToAutoDial(tcallerid);
  }
  
  public void addAllTodayAssignLeadsLeadsToAutoDial(int tcallerid) {
    this.adminDao.addAllTodayAssignLeadsLeadsToAutoDial(tcallerid);
  }
  
  public List<Lead> GetTodayAssignLead(int tcallerid) {
    return this.adminDao.GetTodayAssignLead(tcallerid);
  }
  
  public List GetDailyLeadCount(int tallycallerId) {
    return this.adminDao.GetDailyLeadCount(tallycallerId);
  }
  
  public int UpdateDemoQuery(String query) {
    return this.adminDao.UpdateDemoQuery(query);
  }
  
  public List<Androidbuttonflag> GetAndroidButtonConstant() {
    return this.adminDao.GetAndroidButtonConstant();
  }
  
  public List<Lead> getAutoDialLeadObject(int tcallerid) {
    return this.adminDao.getAutoDialLeadObject(tcallerid);
  }
  
  public List excuteListQuery(String query) {
    return this.adminDao.excuteListQuery(query);
  }
  
  public int addOrRemoveLeadsFromAutoDial(String tallycallerId, int status) {
    return this.adminDao.addOrRemoveLeadsFromAutoDial(tallycallerId, status);
  }
  
  public void sstest() {
    this.adminDao.sstest();
  }
  
  public List<GetScheduleWiseLead> getScheduleWiseLead(int tcallerid) {
    return this.adminDao.getScheduleWiseLead(tcallerid);
  }
  
  public List getGetScheduleLeadScheduler(int cmpId, int interval) {
    return this.adminDao.getGetScheduleLeadScheduler(cmpId, interval);
  }
  
  public List<LeadDTO> getFininceScheduleWiseLead(int tcId) {
    return this.adminDao.getFininceScheduleWiseLead(tcId);
  }
  
  public List<Lead> incredibleservicesLeads(int companyId) {
    return this.adminDao.incredibleservicesLeads(companyId);
  }
  
  public void addAllLeadsToAutoDial(int companyId) {
    this.adminDao.addAllLeadsToAutoDial(companyId);
  }
  
  public AutoDialLog GetAutoDial(int responseId) {
    return this.adminDao.GetAutoDial(responseId);
  }
  
  public CategoryManager getCategoryByNaame(String categoryName, int uid) {
    return this.adminDao.getCategoryByNaame(categoryName, uid);
  }
  
  public List GetMissCallLead(int company_id) {
    return this.adminDao.GetMissCallLead(company_id);
  }
  
  public List GetCatAutoDialLead(int parseInt, int cat) {
    return this.adminDao.GetCatAutoDialLead(parseInt, cat);
  }
  
  public List<String> getUsersBySchedule() {
    return this.adminDao.getUsersBySchedule();
  }
  
  public List<String> gettocken(String userid) {
    return this.adminDao.gettocken(userid);
  }
  
  public List<Object[]> gettelecallerID() {
    return this.adminDao.gettelecallerID();
  }
  
  public int updatefcmid(String fcmid, String username) {
    return this.adminDao.updatefcmid(fcmid, username);
  }
  
  public List<Object[]> getEllapsetelecallerID() {
    return this.adminDao.getEllapsetelecallerID();
  }
  
  public List getLeadSuccessStatus(int compId) {
    return this.adminDao.getLeadSuccessStatus(compId);
  }
  
  public List SuccessStatusSearch(String query) {
    return this.adminDao.SuccessStatusSearch(query);
  }
  
  public Object saveObject(Object lead) {
    return this.adminDao.saveObject(lead);
  }
  
  public List<Object[]> getTelecallerCount(int companyId) {
    return this.adminDao.getTelecallerCount(companyId);
  }
  
  public List<Object[]> getUserLimit(Long uid) {
    return this.adminDao.getUserLimit(uid);
  }
  
  public String getSMSTemplate(long compID, String temp_Name) {
    return this.adminDao.getSMSTemplate(compID, temp_Name);
  }
  
  public List getAndroidTallyCallerLead(String uname, int limit, int pageNo) {
    return this.adminDao.getAndroidTallyCallerLead(uname, limit, pageNo);
  }
  
  public List getAndroidCategoryLead(String username, String catname, int limit, int pageNo) {
    return this.adminDao.getAndroidCategoryLead(username, catname, limit, pageNo);
  }
  
  public List getAndroidLeadByCompanyIdNDmobileNo(String mobNo, int companyId, int limit, int pageNo) {
    return this.adminDao.getAndroidLeadByCompanyIdNDmobileNo(mobNo, companyId, limit, pageNo);
  }
  
  public String getAndroTellyCount(String tallycallerName) {
    return this.adminDao.getAndroTellyCount(tallycallerName);
  }
  
  public String getAndroidCountByCompanyIdNDmobileNo(String mobNo, int companyId) {
    return this.adminDao.getAndroidCountByCompanyIdNDmobileNo(mobNo, companyId);
  }
  
  public void addLeadToAutoDialManager(int leadId, int autoDialVal) {
    this.adminDao.addLeadToAutoDialManager(leadId, autoDialVal);
  }
  
  public List<Object[]> GetLeadStateByManager(int managerId) {
    return this.adminDao.GetLeadStateByManager(managerId);
  }
  
  public List<Object[]> getTallyCallerByManager(int managerId) {
    return this.adminDao.getTallyCallerByManager(managerId);
  }
  
  public String getTallyCallerLeadCount(int teleId, String leadStateId) {
    return this.adminDao.getTallyCallerLeadCount(teleId, leadStateId);
  }
  
  public String getLeadStateCount(String leadStateId, int managerId) {
    return this.adminDao.getLeadStateCount(leadStateId, managerId);
  }
  
  public List<Object[]> getLeadByLeadState(int teleId, String leadStateId) {
    return this.adminDao.getLeadByLeadState(teleId, leadStateId);
  }
  
  public List getFollowupsByLead(int leadId) {
    return this.adminDao.getFollowupsByLead(leadId);
  }
  
  public List<Object[]> getTallyCallerByMobile(String mobileNo) {
    return this.adminDao.getTallyCallerByMobile(mobileNo);
  }
  
  public List<Object[]> getWhiteDetails(String domain) {
    return this.adminDao.getWhiteDetails(domain);
  }
  
  public int tcallerChangePass(int tcId, String password) {
    return this.adminDao.tcallerChangePass(tcId, password);
  }
  
  public List<Object[]> getLeadSuccessStatByLeadStat(String leadStatId) {
    return this.adminDao.getLeadSuccessStatByLeadStat(leadStatId);
  }
  
  public List getCategoryByManager(String managerId) {
    return this.adminDao.getCategoryByManager(managerId);
  }
  
  public List GetCatColumns(int id) {
    return this.adminDao.GetCatColumns(id);
  }
  
  public void deleteCatCol(Long colId) {
    this.adminDao.deleteCatCol(colId);
  }
  
  public List getAndroFailCallList(String query) {
    return this.adminDao.getAndroFailCallList(query);
  }
  
  public List<ExtraFields> getFieldsByUId(int Id, int catId) {
    return this.adminDao.getFieldsByUId(Id, catId);
  }
  
  public void saveExtraFields(ExtraFields extrafields) {
    this.adminDao.saveExtraFields(extrafields);
  }
  
  public List<Object[]> getLeadByCatComp(int compId, int catId) {
    return this.adminDao.getLeadByCatComp(compId, catId);
  }
  
  public int updateCSVJSON(int leadId, String json) {
    return this.adminDao.updateCSVJSON(leadId, json);
  }
  
  public List getCategoryAutoDialLeadCount(String uname) {
    return this.adminDao.getCategoryAutoDialLeadCount(uname);
  }
  
  public List GetAndroidCatAutoDialLead(int tcId, int cat) {
    return this.adminDao.GetAndroidCatAutoDialLead(tcId, cat);
  }
  
  public List<Object[]> getLeadByCatTele(int tcId, int catId) {
    return this.adminDao.getLeadByCatTele(tcId, catId);
  }
  
  public List<ExtraFields> getFieldsByUId(int Id) {
    return this.adminDao.getFieldsByUId(Id);
  }
  
  public CategoryManager getCategory(int uid) {
    return this.adminDao.getCategory(uid);
  }
  
  public List<Object[]> getLeadProStateById(int stId) {
    return this.adminDao.getLeadProStateById(stId);
  }
  
  public int extraFieldCount(int catId, int compId, String fieldname) {
    return this.adminDao.extraFieldCount(catId, compId, fieldname);
  }
  
  public List AndroidLeadSearch(String query, int page, int listSize) {
    return this.adminDao.AndroidLeadSearch(query, page, listSize);
  }
  
  public void DeleteLeadBeforeAssign(int leadId) {
    this.adminDao.DeleteLeadBeforeAssign(leadId);
  }
  
  public List<Object[]> getHashmukhLeadCount(String categoryId, String contactNo) {
    return this.adminDao.getHashmukhLeadCount(categoryId, contactNo);
  }
  
  public int updateFailCallLeadStat(String query) {
    return this.adminDao.updateFailCallLeadStat(query);
  }
  
  public int getCountTele(String query) {
    return this.adminDao.getCountTele(query);
  }
  
  public int updateTelecallerAccessDate(int tcId, String apiKey) {
    return this.adminDao.updateTelecallerAccessDate(tcId, apiKey);
  }
  
  public List<Object[]> getTeleLastAccess(int compId) {
    return this.adminDao.getTeleLastAccess(compId);
  }
  
  public List<Object[]> getExtraParamsByCat(int compId, int catId) {
    return this.adminDao.getExtraParamsByCat(compId, catId);
  }
  
  public void deleteUserRole(int userId) {
    this.adminDao.deleteUserRole(userId);
  }
  
  public List GetCatAutoDialSingalLead(int teleId, int cat) {
    return this.adminDao.GetCatAutoDialSingalLead(teleId, cat);
  }  
  
  public List<Object[]> getLeadCatAdvAutoDial(String uname, int catId, String stDate, String endDate) {
    return this.adminDao.getLeadCatAdvAutoDial(uname, catId, stDate, endDate);
  }
  
  public List GetAdvanceCatAutoDialLead(int parseInt, int cat, String stDate, String endDate) {
    return this.adminDao.GetAdvanceCatAutoDialLead(parseInt, cat, stDate, endDate);
  }
  
  public void updateLead(String query) {
    this.adminDao.updateLead(query);
  }
  
  public List<Lead> getDuplicateLeads(String mobileNo, int CompId) {
    return this.adminDao.getDuplicateLeads(mobileNo, CompId);
  }
  
  public void deleteLeadById(int leadId) {
    this.adminDao.deleteLeadById(leadId);
  }
  
  public void AddAutoDial(String query) {
	    this.adminDao.AddAutoDial(query);
	  }
	  
	  public List getDueTodayLead(int tcId) {
	    return this.adminDao.getDueTodayLead(tcId);
	  }
	  
	  public List GetAllLeads(String query) {
		    return this.adminDao.GetAllLeads(query);
		  }
	  
	/*  public void UpdateLead(int id, String fname, String lname, String email, String mobno, String cat, String sataus, String compName, String webAdd, String cont, String stat, String cty, int tcaller, String updatealtmobileNo) {
		    this.adminDao.UpdateLead(id, fname, lname, email, mobno, cat, sataus, compName, webAdd, cont, stat, cty, tcaller, updatealtmobileNo);
		  }*/
}

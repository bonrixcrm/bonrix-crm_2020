package com.bonrix.sms.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bonrix.sms.model.SystemParameter;


@Service
public interface CommonService {


	public List createSqlQuery(String query);
	public List createQuery(String query);
	public int createupdateQuery(String query);

	Object saveObject(Object obj);
	
	Object GetObject(String query);
	
	int createupdateSqlQuery(String query);
	
	public int updateObject(Object obj);
	public BigInteger getcount(String query);
	public List createSqlQuery(String query, int first, int max);
	public int updateResponce(int folId,String reponce);
	public List<SystemParameter> getSysParam();
	public int saveRetObject(Object log);
	
	public Object getSingleObject(String query);
}

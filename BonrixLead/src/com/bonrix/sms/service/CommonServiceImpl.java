package com.bonrix.sms.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.CommonDao;
import com.bonrix.sms.model.AutoDialLog;
import com.bonrix.sms.model.SystemParameter;

/**
 * @author Sajan
 *
 */
@Service("CommonService")
@Transactional
public class CommonServiceImpl implements CommonService {

	@Autowired
	private CommonDao dao;

	@Override
	public List createSqlQuery(String query) {
		return dao.createSqlQuery(query);
	}

	@Override
	public List createQuery(String query) {
		return dao.createQuery(query);
	}

	@Override
	public int createupdateQuery(String query) {
		return dao.createupdateQuery(query);
	}

	@Override
	public Object saveObject(Object obj) {
		return dao.saveObject(obj);
	}

	@Override
	public int createupdateSqlQuery(String query) {
		return dao.createupdateSqlQuery(query);
	}

	@Override
	public int updateObject(Object obj) {
		return dao.updateObject(obj);
	}

	@Override
	public BigInteger getcount(String query) {
		return dao.getcount(query);
	}

	@Override
	public List createSqlQuery(String query, int first, int max) {
	    return dao.createSqlQuery(query, first, max);
	}

	@Override
	public int updateResponce(int folId, String reponce) {
		// TODO Auto-generated method stub
		return dao.updateResponce(folId, reponce);
	}

	@Override
	public List<SystemParameter> getSysParam() {
		// TODO Auto-generated method stub
		return dao.getSysParam();
	}

	@Override
	public int saveRetObject(Object log) {
		return dao.saveRetObject(log);
	}

	@Override
	public Object GetObject(String query) {
		return dao.GetObject(query);
	}

	@Override
	public Object getSingleObject(String query) {
		return dao.getSingleObject(query);
	}

	
	
	
	


}

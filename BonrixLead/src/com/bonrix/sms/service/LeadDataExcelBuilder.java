package com.bonrix.sms.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONObject;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class LeadDataExcelBuilder extends AbstractExcelView {
	  protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
	    System.out.println("LeadDataExcelBuilder...." + model.get("type").toString());
	    if (model.get("type").toString().equalsIgnoreCase("All") && model.get("extra").toString().equalsIgnoreCase("0")) {
	      try {
	    	  Sheet hSSFSheet = workbook.createSheet("Report");
	        Row row = hSSFSheet.createRow(0);
	        row.createCell(0).setCellValue("Sr.no");
	        row.createCell(1).setCellValue("Client Name");
	        row.createCell(2).setCellValue("Mobile No");
	        row.createCell(3).setCellValue("Email");
	        row.createCell(4).setCellValue("Company Name");
	        row.createCell(5).setCellValue("Web Url");
	        row.createCell(6).setCellValue("Country");
	        row.createCell(7).setCellValue("State");
	        row.createCell(8).setCellValue("City");
	        row.createCell(9).setCellValue("Lead State");
	        row.createCell(10).setCellValue("Lead Process State");
	        row.createCell(11).setCellValue("Lead Sub State");
	        row.createCell(12).setCellValue("Lead Entry");
	        row.createCell(13).setCellValue("Telecaller");
	        row.createCell(14).setCellValue("Calling Time");
	        row.createCell(15).setCellValue("Call Duration");
	        row.createCell(16).setCellValue("Remark");
	        row.createCell(17).setCellValue("CallState");
	        row.createCell(18).setCellValue("FailState");
	        row.createCell(19).setCellValue("Category");
	        row.createCell(20).setCellValue("sheduleTime");
	        List<Object[]> listq = (List)model.get("excelList");
	        System.out.println("listq::" + listq.size());
	        int i = 0;
	        if (listq.size() != 0) {
	          Object[] result = null;
	          for (i = 0; i < listq.size(); i++) {
	            result = listq.get(i);
	            Row rowadd = hSSFSheet.createRow((short)i + 1);
	            rowadd.createCell(0).setCellValue((i + 1));
	            rowadd.createCell(1).setCellValue((result[0].toString() == null) ? "N/A" : result[0].toString());
	            rowadd.createCell(2).setCellValue((result[1].toString() == null) ? "N/A" : result[1].toString());
	            rowadd.createCell(3).setCellValue((result[2].toString() == null) ? "N/A" : result[2].toString());
	            rowadd.createCell(4).setCellValue((result[3].toString() == null) ? "N/A" : result[3].toString());
	            rowadd.createCell(5).setCellValue((result[4].toString() == null) ? "N/A" : result[4].toString());
	            rowadd.createCell(6).setCellValue((result[5].toString() == null) ? "N/A" : result[5].toString());
	            rowadd.createCell(7).setCellValue((result[6].toString() == null) ? "N/A" : result[6].toString());
	            rowadd.createCell(8).setCellValue((result[7].toString() == null) ? "N/A" : result[7].toString());
	            rowadd.createCell(9).setCellValue((result[8].toString() == null) ? "N/A" : result[8].toString());
	            rowadd.createCell(10).setCellValue((result[9].toString() == null) ? "N/A" : result[9].toString());
	            rowadd.createCell(11).setCellValue((result[10] == null) ? "N/A" : result[10].toString());
	            rowadd.createCell(12).setCellValue((result[11].toString() == null) ? "N/A" : result[11].toString());
	            rowadd.createCell(13).setCellValue((result[12].toString() == null) ? "N/A" : result[12].toString());
	            rowadd.createCell(14).setCellValue((result[13].toString() == null) ? "N/A" : result[13].toString());
	            rowadd.createCell(15).setCellValue((result[14].toString() == null) ? "N/A" : result[14].toString());
	            rowadd.createCell(16).setCellValue((result[15].toString() == null) ? "N/A" : result[15].toString());
	            rowadd.createCell(17).setCellValue((result[16] == null) ? "N/A" : result[16].toString());
	            rowadd.createCell(18).setCellValue((result[17].toString() == null) ? "N/A" : result[17].toString());
	            rowadd.createCell(19).setCellValue((result[18].toString() == null) ? "N/A" : result[18].toString());
	            rowadd.createCell(20).setCellValue((result[18].toString() == null) ? "N/A" : result[19].toString());
	          } 
	        }   
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	    } else if (model.get("type").toString().equalsIgnoreCase("All") && model.get("extra").toString().equalsIgnoreCase("1")) {
	      try {
	    	  Sheet hSSFSheet = workbook.createSheet("Report");
	        Row row = hSSFSheet.createRow(0);
	        row.createCell(0).setCellValue("Sr.no");
	        row.createCell(1).setCellValue("Client Name");
	        row.createCell(2).setCellValue("Mobile No");
	        row.createCell(3).setCellValue("Email");
	        row.createCell(4).setCellValue("Company Name");
	        row.createCell(5).setCellValue("Web Url");
	        row.createCell(6).setCellValue("Country");
	        row.createCell(7).setCellValue("State");
	        row.createCell(8).setCellValue("City");
	        row.createCell(9).setCellValue("Lead State");
	        row.createCell(10).setCellValue("Lead Process State");
	        row.createCell(11).setCellValue("Lead Sub State");
	        row.createCell(12).setCellValue("Lead Entry");
	        row.createCell(13).setCellValue("Telecaller");
	        row.createCell(14).setCellValue("Calling Time");
	        row.createCell(15).setCellValue("Call Duration");
	        row.createCell(16).setCellValue("Remark");
	        row.createCell(17).setCellValue("CallState");
	        row.createCell(18).setCellValue("FailState");
	        row.createCell(19).setCellValue("Category");
	        row.createCell(20).setCellValue("sheduleTime");
	        List<Object[]> listq = (List)model.get("excelList");
	        System.out.println("listq::" + listq.size());
	        int hc = 21;
	        Object[] rs = null;
	        ArrayList<String> ar = new ArrayList<>();
	        ArrayList<String> arval = new ArrayList<>();
	        for (int j = 0; j < listq.size(); j++) {
	          rs = listq.get(j);
	          String leadJson = rs[20].toString();
	          JSONObject jsonObj = new JSONObject(leadJson);
	          Iterator<String> keys = jsonObj.keys();
	          String str5 = "NA";
	          String val = "NA";
	          while (keys.hasNext()) {
	            str5 = keys.next();
	            val = jsonObj.getString(str5);
	            if (!ar.contains(str5.replaceAll("_", " ")))
	              ar.add(str5.replaceAll("_", " ")); 
	            arval.add(val);
	          } 
	        } 
	        System.out.println("arvalSize::" + arval.size());
	        for (int l = 0; l < ar.size(); l++) {
	          row.createCell(hc).setCellValue(ar.get(l));
	          hc++;
	        } 
	        int i = 0;
	        if (listq.size() != 0) {
	          Object[] result = null;
	          int v1 = 0;
	          for (i = 0; i < listq.size(); i++) {
	            result = listq.get(i);
	            Row rowadd = hSSFSheet.createRow((short)i + 1);
	            rowadd.createCell(0).setCellValue((i + 1));
	            rowadd.createCell(1).setCellValue((result[0].toString() == null) ? "N/A" : result[0].toString());
	            rowadd.createCell(2).setCellValue((result[1].toString() == null) ? "N/A" : result[1].toString());
	            rowadd.createCell(3).setCellValue((result[2].toString() == null) ? "N/A" : result[2].toString());
	            rowadd.createCell(4).setCellValue((result[3].toString() == null) ? "N/A" : result[3].toString());
	            rowadd.createCell(5).setCellValue((result[4].toString() == null) ? "N/A" : result[4].toString());
	            rowadd.createCell(6).setCellValue((result[5].toString() == null) ? "N/A" : result[5].toString());
	            rowadd.createCell(7).setCellValue((result[6].toString() == null) ? "N/A" : result[6].toString());
	            rowadd.createCell(8).setCellValue((result[7].toString() == null) ? "N/A" : result[7].toString());
	            rowadd.createCell(9).setCellValue((result[8].toString() == null) ? "N/A" : result[8].toString());
	            rowadd.createCell(10).setCellValue((result[9].toString() == null) ? "N/A" : result[9].toString());
	            rowadd.createCell(11).setCellValue((result[10] == null) ? "N/A" : result[10].toString());
	            rowadd.createCell(12).setCellValue((result[11].toString() == null) ? "N/A" : result[11].toString());
	            rowadd.createCell(13).setCellValue((result[12].toString() == null) ? "N/A" : result[12].toString());
	            rowadd.createCell(14).setCellValue((result[13].toString() == null) ? "N/A" : result[13].toString());
	            rowadd.createCell(15).setCellValue((result[14].toString() == null) ? "N/A" : result[14].toString());
	            rowadd.createCell(16).setCellValue((result[15] == null) ? "N/A" : result[15].toString());
	            rowadd.createCell(17).setCellValue((result[16].toString() == null) ? "N/A" : result[16].toString());
	            rowadd.createCell(18).setCellValue((result[17].toString() == null) ? "N/A" : result[17].toString());
	            rowadd.createCell(19).setCellValue((result[18].toString() == null) ? "N/A" : result[18].toString());
	            rowadd.createCell(20).setCellValue((result[19].toString() == null) ? "N/A" : result[19].toString());
	            int cv = 21;
	            for (int v = 0; v < ar.size(); v++) {
	              rowadd.createCell(cv).setCellValue(((String)arval.get(v1)).toString());
	              cv++;
	              v1++;
	            } 
	            v1 = v1;
	          } 
	        } 
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	    } else if (model.get("type").toString().equalsIgnoreCase("Success") && model.get("extra").toString().equalsIgnoreCase("1")) {
	      try {
	    	  Sheet hSSFSheet = workbook.createSheet("Report");
	        Row row = hSSFSheet.createRow(0);
	        row.createCell(0).setCellValue("Sr.no");
	        row.createCell(1).setCellValue("Client Name");
	        row.createCell(2).setCellValue("Mobile No");
	        row.createCell(3).setCellValue("Email");
	        row.createCell(4).setCellValue("Company Name");
	        row.createCell(5).setCellValue("Web Url");
	        row.createCell(6).setCellValue("Country");
	        row.createCell(7).setCellValue("State");
	        row.createCell(8).setCellValue("City");
	        row.createCell(9).setCellValue("Lead State");
	        row.createCell(10).setCellValue("Lead Process State");
	        row.createCell(11).setCellValue("Category");
	        row.createCell(12).setCellValue("Lead Entry");
	        row.createCell(13).setCellValue("Telecaller");
	        row.createCell(14).setCellValue("Calling Time");
	        row.createCell(15).setCellValue("Call Duration");
	        row.createCell(16).setCellValue("Remark");
	        row.createCell(17).setCellValue("sheduleTime");
	        List<Object[]> listq = (List)model.get("excelList");
	        System.out.println("listqSize..::" + listq.size());
	        int hc = 18;
	        Object[] rs = null;
	        ArrayList<String> ar = new ArrayList<>();
	        ArrayList<String> arval = new ArrayList<>();
	        for (int j = 0; j < listq.size(); j++) {
	          rs = listq.get(j);
	          String leadJson = rs[17].toString();
	          JSONObject jsonObj = new JSONObject(leadJson);
	          Iterator<String> keys = jsonObj.keys();
	          String str5 = "NA";
	          String val = "NA";
	          while (keys.hasNext()) {
	            str5 = keys.next();
	            val = jsonObj.getString(str5);
	            if (!ar.contains(str5.replaceAll("_", " ")))
	              ar.add(str5.replaceAll("_", " ")); 
	            arval.add(val);
	          } 
	        } 
	        for (int l = 0; l < ar.size(); l++) {
	          row.createCell(hc).setCellValue(ar.get(l));
	          hc++;
	        } 
	        int i = 0;
	        if (listq.size() != 0) {
	          Object[] result = null;
	          int v1 = 0;
	          for (i = 0; i < listq.size(); i++) {
	            result = listq.get(i);
	            Row rowadd = hSSFSheet.createRow((short)i + 1);
	            rowadd.createCell(0).setCellValue((i + 1));
	            rowadd.createCell(1).setCellValue((result[0].toString() == null) ? "N/A" : result[0].toString());
	            rowadd.createCell(2).setCellValue((result[1].toString() == null) ? "N/A" : result[1].toString());
	            rowadd.createCell(3).setCellValue((result[2].toString() == null) ? "N/A" : result[2].toString());
	            rowadd.createCell(4).setCellValue((result[3].toString() == null) ? "N/A" : result[3].toString());
	            rowadd.createCell(5).setCellValue((result[4].toString() == null) ? "N/A" : result[4].toString());
	            rowadd.createCell(6).setCellValue((result[5].toString() == null) ? "N/A" : result[5].toString());
	            rowadd.createCell(7).setCellValue((result[6].toString() == null) ? "N/A" : result[6].toString());
	            rowadd.createCell(8).setCellValue((result[7].toString() == null) ? "N/A" : result[7].toString());
	            rowadd.createCell(9).setCellValue((result[8].toString() == null) ? "N/A" : result[8].toString());
	            rowadd.createCell(10).setCellValue((result[9].toString() == null) ? "N/A" : result[9].toString());
	            rowadd.createCell(11).setCellValue((result[10].toString() == null) ? "N/A" : result[10].toString());
	            rowadd.createCell(12).setCellValue((result[11].toString() == null) ? "N/A" : result[11].toString());
	            rowadd.createCell(13).setCellValue((result[12].toString() == null) ? "N/A" : result[12].toString());
	            rowadd.createCell(14).setCellValue((result[13].toString() == null) ? "N/A" : result[13].toString());
	            rowadd.createCell(15).setCellValue((result[14].toString() == null) ? "N/A" : result[14].toString());
	            rowadd.createCell(16).setCellValue((result[15] == null) ? "N/A" : result[15].toString());
	            rowadd.createCell(17).setCellValue((result[16].toString() == null) ? "N/A" : result[16].toString());
	            int cv = 18;
	            for (int v = 0; v < ar.size(); v++) {
	              rowadd.createCell(cv).setCellValue(((String)arval.get(v1)).toString());
	              cv++;
	              v1++;
	            } 
	            v1 = v1;
	          } 
	        } 
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	    } else if (model.get("type").toString().equalsIgnoreCase("Success") && model.get("extra").toString().equalsIgnoreCase("0")) {
	      try {
	    	  Sheet hSSFSheet = workbook.createSheet("Report");
	        Row row = hSSFSheet.createRow(0);
	        row.createCell(0).setCellValue("Sr.no");
	        row.createCell(1).setCellValue("Client Name");
	        row.createCell(2).setCellValue("Mobile No");
	        row.createCell(3).setCellValue("Email");
	        row.createCell(4).setCellValue("Company Name");
	        row.createCell(5).setCellValue("Web Url");
	        row.createCell(6).setCellValue("Country");
	        row.createCell(7).setCellValue("State");
	        row.createCell(8).setCellValue("City");
	        row.createCell(9).setCellValue("Lead State");
	        row.createCell(10).setCellValue("Lead Process State");
	        row.createCell(11).setCellValue("Category");
	        row.createCell(12).setCellValue("Lead Entry");
	        row.createCell(13).setCellValue("Telecaller");
	        row.createCell(14).setCellValue("Calling Time");
	        row.createCell(15).setCellValue("Call Duration");
	        row.createCell(16).setCellValue("Remark");
	        row.createCell(17).setCellValue("sheduleTime");
	        List<Object[]> listq = (List)model.get("excelList");
	        int i = 0;
	        if (listq.size() != 0) {
	          Object[] result = null;
	          for (i = 0; i < listq.size(); i++) {
	            result = listq.get(i);
	            Row rowadd = hSSFSheet.createRow((short)i + 1);
	            rowadd.createCell(0).setCellValue((i + 1));
	            rowadd.createCell(1).setCellValue((result[0].toString() == null) ? "N/A" : result[0].toString());
	            rowadd.createCell(2).setCellValue((result[1].toString() == null) ? "N/A" : result[1].toString());
	            rowadd.createCell(3).setCellValue((result[2].toString() == null) ? "N/A" : result[2].toString());
	            rowadd.createCell(4).setCellValue((result[3].toString() == null) ? "N/A" : result[3].toString());
	            rowadd.createCell(5).setCellValue((result[4].toString() == null) ? "N/A" : result[4].toString());
	            rowadd.createCell(6).setCellValue((result[5].toString() == null) ? "N/A" : result[5].toString());
	            rowadd.createCell(7).setCellValue((result[6].toString() == null) ? "N/A" : result[6].toString());
	            rowadd.createCell(8).setCellValue((result[7].toString() == null) ? "N/A" : result[7].toString());
	            rowadd.createCell(9).setCellValue((result[8].toString() == null) ? "N/A" : result[8].toString());
	            rowadd.createCell(10).setCellValue((result[9].toString() == null) ? "N/A" : result[9].toString());
	            rowadd.createCell(11).setCellValue((result[10].toString() == null) ? "N/A" : result[10].toString());
	            rowadd.createCell(12).setCellValue((result[11].toString() == null) ? "N/A" : result[11].toString());
	            rowadd.createCell(13).setCellValue((result[12].toString() == null) ? "N/A" : result[12].toString());
	            rowadd.createCell(14).setCellValue((result[13].toString() == null) ? "N/A" : result[13].toString());
	            rowadd.createCell(15).setCellValue((result[14].toString() == null) ? "N/A" : result[14].toString());
	            rowadd.createCell(16).setCellValue((result[15] == null) ? "N/A" : result[15].toString());
	            rowadd.createCell(17).setCellValue((result[15].toString() == null) ? "N/A" : result[16].toString());
	          } 
	        } 
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	    } else if (model.get("type").toString().equalsIgnoreCase("Fail") && model.get("extra").toString().equalsIgnoreCase("0")) {
	      try {
	    	  Sheet hSSFSheet = workbook.createSheet("Report");
	        Row row = hSSFSheet.createRow(0);
	        row.createCell(0).setCellValue("Sr.no");
	        row.createCell(1).setCellValue("Client Name");
	        row.createCell(2).setCellValue("Mobile No");
	        row.createCell(3).setCellValue("Email");
	        row.createCell(4).setCellValue("Company Name");
	        row.createCell(5).setCellValue("Web Url");
	        row.createCell(6).setCellValue("Country");
	        row.createCell(7).setCellValue("State");
	        row.createCell(8).setCellValue("City");
	        row.createCell(9).setCellValue("Lead State");
	        row.createCell(10).setCellValue("Lead Process State");
	        row.createCell(11).setCellValue("Category");
	        row.createCell(12).setCellValue("Lead Entry");
	        row.createCell(13).setCellValue("Telecaller");
	        row.createCell(14).setCellValue("Calling Time");
	        row.createCell(15).setCellValue("Call Duration");
	        row.createCell(16).setCellValue("Remark");
	        row.createCell(17).setCellValue("FailState");
	        row.createCell(18).setCellValue("sheduleTime");
	        List<Object[]> listq = (List)model.get("excelList");
	        int i = 0;
	        if (listq.size() != 0) {
	          Object[] result = null;
	          for (i = 0; i < listq.size(); i++) {
	            result = listq.get(i);
	            Row rowadd = hSSFSheet.createRow((short)i + 1);
	            rowadd.createCell(0).setCellValue((i + 1));
	            rowadd.createCell(1).setCellValue((result[0].toString() == null) ? "N/A" : result[0].toString());
	            rowadd.createCell(2).setCellValue((result[1].toString() == null) ? "N/A" : result[1].toString());
	            rowadd.createCell(3).setCellValue((result[2].toString() == null) ? "N/A" : result[2].toString());
	            rowadd.createCell(4).setCellValue((result[3].toString() == null) ? "N/A" : result[3].toString());
	            rowadd.createCell(5).setCellValue((result[4].toString() == null) ? "N/A" : result[4].toString());
	            rowadd.createCell(6).setCellValue((result[5].toString() == null) ? "N/A" : result[5].toString());
	            rowadd.createCell(7).setCellValue((result[6].toString() == null) ? "N/A" : result[6].toString());
	            rowadd.createCell(8).setCellValue((result[7].toString() == null) ? "N/A" : result[7].toString());
	            rowadd.createCell(9).setCellValue((result[8].toString() == null) ? "N/A" : result[8].toString());
	            rowadd.createCell(10).setCellValue((result[9].toString() == null) ? "N/A" : result[9].toString());
	            rowadd.createCell(11).setCellValue((result[10].toString() == null) ? "N/A" : result[10].toString());
	            rowadd.createCell(12).setCellValue((result[11].toString() == null) ? "N/A" : result[11].toString());
	            rowadd.createCell(13).setCellValue((result[12].toString() == null) ? "N/A" : result[12].toString());
	            rowadd.createCell(14).setCellValue((result[13].toString() == null) ? "N/A" : result[13].toString());
	            rowadd.createCell(15).setCellValue((result[14].toString() == null) ? "N/A" : result[14].toString());
	            rowadd.createCell(16).setCellValue((result[15] == null) ? "N/A" : result[15].toString());
	            rowadd.createCell(17).setCellValue((result[16].toString() == null) ? "N/A" : result[16].toString());
	            rowadd.createCell(18).setCellValue((result[15].toString() == null) ? "N/A" : result[17].toString());
	          } 
	        } 
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	    } else if (model.get("type").toString().equalsIgnoreCase("Fail") && model.get("extra").toString().equalsIgnoreCase("1")) {
	      try {
	    	  Sheet hSSFSheet = workbook.createSheet("Report");
	        Row row = hSSFSheet.createRow(0);
	        row.createCell(0).setCellValue("Sr.no");
	        row.createCell(1).setCellValue("Client Name");
	        row.createCell(2).setCellValue("Mobile No");
	        row.createCell(3).setCellValue("Email");
	        row.createCell(4).setCellValue("Company Name");
	        row.createCell(5).setCellValue("Web Url");
	        row.createCell(6).setCellValue("Country");
	        row.createCell(7).setCellValue("State");
	        row.createCell(8).setCellValue("City");
	        row.createCell(9).setCellValue("Lead State");
	        row.createCell(10).setCellValue("Lead Process State");
	        row.createCell(11).setCellValue("Category");
	        row.createCell(12).setCellValue("Lead Entry");
	        row.createCell(13).setCellValue("Telecaller");
	        row.createCell(14).setCellValue("Calling Time");
	        row.createCell(15).setCellValue("Call Duration");
	        row.createCell(16).setCellValue("Remark");
	        row.createCell(17).setCellValue("FailState");
	        row.createCell(18).setCellValue("sheduleTime");
	        List<Object[]> listq = (List)model.get("excelList");
	        int hc = 19;
	        Object[] rs = null;
	        ArrayList<String> ar = new ArrayList<>();
	        ArrayList<String> arval = new ArrayList<>();
	        for (int j = 0; j < listq.size(); j++) {
	          rs = listq.get(j);
	          String leadJson = rs[18].toString();
	          JSONObject jsonObj = new JSONObject(leadJson);
	          Iterator<String> keys = jsonObj.keys();
	          String str5 = "NA";
	          String val = "NA";
	          while (keys.hasNext()) {
	            str5 = keys.next();
	            val = jsonObj.getString(str5);
	            if (!ar.contains(str5.replaceAll("_", " ")))
	              ar.add(str5.replaceAll("_", " ")); 
	            arval.add(val);
	          } 
	        } 
	        for (int l = 0; l < ar.size(); l++) {
	          row.createCell(hc).setCellValue(ar.get(l));
	          hc++;
	        } 
	        int i = 0;
	        if (listq.size() != 0) {
	          Object[] result = null;
	          int v1 = 0;
	          for (i = 0; i < listq.size(); i++) {
	            result = listq.get(i);
	            Row rowadd = hSSFSheet.createRow((short)i + 1);
	            rowadd.createCell(0).setCellValue((i + 1));
	            rowadd.createCell(1).setCellValue((result[0].toString() == null) ? "N/A" : result[0].toString());
	            rowadd.createCell(2).setCellValue((result[1].toString() == null) ? "N/A" : result[1].toString());
	            rowadd.createCell(3).setCellValue((result[2].toString() == null) ? "N/A" : result[2].toString());
	            rowadd.createCell(4).setCellValue((result[3].toString() == null) ? "N/A" : result[3].toString());
	            rowadd.createCell(5).setCellValue((result[4].toString() == null) ? "N/A" : result[4].toString());
	            rowadd.createCell(6).setCellValue((result[5].toString() == null) ? "N/A" : result[5].toString());
	            rowadd.createCell(7).setCellValue((result[6].toString() == null) ? "N/A" : result[6].toString());
	            rowadd.createCell(8).setCellValue((result[7].toString() == null) ? "N/A" : result[7].toString());
	            rowadd.createCell(9).setCellValue((result[8].toString() == null) ? "N/A" : result[8].toString());
	            rowadd.createCell(10).setCellValue((result[9].toString() == null) ? "N/A" : result[9].toString());
	            rowadd.createCell(11).setCellValue((result[10].toString() == null) ? "N/A" : result[10].toString());
	            rowadd.createCell(12).setCellValue((result[11].toString() == null) ? "N/A" : result[11].toString());
	            rowadd.createCell(13).setCellValue((result[12].toString() == null) ? "N/A" : result[12].toString());
	            rowadd.createCell(14).setCellValue((result[13].toString() == null) ? "N/A" : result[13].toString());
	            rowadd.createCell(15).setCellValue((result[14].toString() == null) ? "N/A" : result[14].toString());
	            rowadd.createCell(16).setCellValue((result[15] == null) ? "N/A" : result[15].toString());
	            rowadd.createCell(17).setCellValue((result[16].toString() == null) ? "N/A" : result[16].toString());
	            rowadd.createCell(18).setCellValue((result[16].toString() == null) ? "N/A" : result[17].toString());
	            int cv = 19;
	            for (int v = 0; v < ar.size(); v++) {
	              rowadd.createCell(cv).setCellValue(((String)arval.get(v1)).toString());
	              cv++;
	              v1++;
	            } 
	            v1 = v1;
	          } 
	        } 
	      } catch (Exception e) {
	        e.printStackTrace();
	      } 
	    } 
	  }
	}

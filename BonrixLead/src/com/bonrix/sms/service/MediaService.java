package com.bonrix.sms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bonrix.sms.model.MediaMaster;
import com.bonrix.sms.model.Tinyurlhit;

@Service
public interface MediaService {

	public void saveMedia(MediaMaster user);

	public MediaMaster getMediabytinyurl(String mediaurl);

	public int saveTinyHit(Tinyurlhit th);

	public String getHitSummarybyUid(Long uid);

	public String getHitSummarybyMid(Long mid);

	public List findMediabyUid(Long uid);

}

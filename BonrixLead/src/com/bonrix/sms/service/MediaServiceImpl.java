package com.bonrix.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.MediaDAO;
import com.bonrix.sms.model.MediaMaster;
import com.bonrix.sms.model.Tinyurlhit;

@Service("MediaService")
@Transactional
public class MediaServiceImpl implements MediaService {

	@Autowired
	private MediaDAO mediaDao;

	public void saveMedia(MediaMaster user) {
		mediaDao.saveMedia(user);
	}

	@Override
	public MediaMaster getMediabytinyurl(String mediaurl) {
		return mediaDao.getMediabytinyurl(mediaurl);
	}

	@Override
	public int saveTinyHit(Tinyurlhit th) {
		mediaDao.saveTinyHit(th);
		return 1;
	}

	@Override
	public String getHitSummarybyUid(Long uid) {
		// TODO Auto-generated method stub
		return mediaDao.getHitSummarybyUid(uid);
	}

	@Override
	public String getHitSummarybyMid(Long mid) {
		// TODO Auto-generated method stub
		return mediaDao.getHitSummarybyMid(mid);
	}

	@Override
	public List findMediabyUid(Long uid) {
		return mediaDao.findMediabyUid(uid);
	}

}
package com.bonrix.sms.service;

import java.net.InetSocketAddress;

import org.springframework.stereotype.Service;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.RechargeLogMaster;
import com.bonrix.sms.model.RechargeNotifyMaster;

@Service
public interface RechargeGridService {

	public int saveRechargeLog(RechargeLogMaster rgm);

	public ApiKey gettAPiByUid(long uid, String key);

	int saveRechargeNotify(RechargeNotifyMaster rnm);
	public void startSshClient(InetSocketAddress address);
}

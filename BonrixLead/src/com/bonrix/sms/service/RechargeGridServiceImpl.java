package com.bonrix.sms.service;

import java.net.InetSocketAddress;

import org.java_websocket.server.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.RechargeGridDAO;
import com.bonrix.sms.job.SSHClient;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.RechargeLogMaster;
import com.bonrix.sms.model.RechargeNotifyMaster;

@Service("RechargeGridService")
@Transactional
@EnableAsync
public class RechargeGridServiceImpl implements RechargeGridService {

	@Autowired
	private RechargeGridDAO rechargeDao;

	@Override
	public int saveRechargeLog(RechargeLogMaster rgm) {
		return rechargeDao.saveRechargeLog(rgm);
	}

	@Override
	public ApiKey gettAPiByUid(long uid, String key) {
		return rechargeDao.gettAPiByUid(uid, key);
	}

	@Override
	public int saveRechargeNotify(RechargeNotifyMaster rnm) {

		return rechargeDao.saveRechargeNotify(rnm);
	}

	

	@Async
	public void startSshClient(InetSocketAddress address) {
		  WebSocketServer server = new SSHClient(address);
	        server.run();
		
	}
	
	
}

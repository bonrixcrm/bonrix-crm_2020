package com.bonrix.sms.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bonrix.sms.model.WhiteListNumber;
import com.bonrix.sms.model.CampaignLog;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsDlrSummary;
import com.bonrix.sms.model.SmsHistory;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.SpamWords;
import com.bonrix.sms.model.SystemParameter;
import com.bonrix.sms.model.failedSmsQueue;

@Service
public interface SMSService {

	public void saveSmsQueue(List<SmsQueue> smsQueue);

	public int saveFailedSmsQueue(List<failedSmsQueue> smsQueue);

	public int moveToFailedSmsQueue(Long uid);

	public List<SmsQueue> getSmsQueue(int total, int isdnd);

	public List<SmsQueue> getSmsQueueByUid(Long uid);

	public List<SmsApi> getSMSApi(Long uid, int sid);

	public SmsApi getSMSApibyApiId(Long apiid);

	public int creditUpdate(int cnt, Long uid, Long stid);

	public SenderName getSenderName(Long sendid);

	public void saveSmsHistory(SmsHistory smshistory);

	public int deletesmsq(Long qid);

	public String getSMSCreditByUid(Long uid);

	public String getSMSQAdmin();

	public String getSmsHistoryByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile,
			int service);

	public String getsmscountbyuid(Long lg, String sdate, String edate);

	public SystemParameter getSysParameterByName(String sysname);

	public List<SmsHistory> getSmsHistoryforDLR(int day, int limit);

	public int DLRUpdate(Long l, String status, String message);

	int DelayDLRUpdate(String status, int hour);

	public String getSmsHistoryByAdmin(int page, int listSize, long uid, Date sdate, Date edate, String mobile);

	public List getSmsHistoryByAdmin4Report(int page, int listSize, long uid, Date sdate, Date edate, String mobile);

	public int updateSystemParameterbyName(SystemParameter sp);

	public List<Long> checkDndNumbers(String number);

	List<SmsHistory> getSmsHistorybyUid(int uid, int start, int max);

	public int DLRUpdateByMsgId(Long hid, String status, String msgid);

	public int updatesmppConfig(int cnfigid, int status, int threadid, String connecterror);

	public int resetSmppConfig();

	public List getQueueCountByUser(long userid);

	public List<SmsDlrSummary> updateSmsSummary(Long uid, String sdate, String edate);

	public SmsHistory getHistoryByMsgid(Long msgid);

	public HostSetting getHostSetting(String user);

	public SenderName getSenderNameByUid(Long uid, String Sendername);

	public CampaignLog saveCampaign(CampaignLog cl);

	public List<SmsHistory> getSmsHistorybyCampid(int campid);

	public CampaignLog getCampaignByMsgid(String campid);

	public String getSeduleMessageByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile,
			int service);

	public int deleteMultismsQ(String qid);

	public int saveSpamWord(SpamWords sm);

	public List<SpamWords> getSpamWords();

	int saveWhiteList(WhiteListNumber sm);

	public List<String> getWhiteList(int type);

	public List<WhiteListNumber> getWhiteListObj();

	int deleteWhiteList(int sm);

	public String getBlockedNumbers(int page, int listSize, String mobile);

}

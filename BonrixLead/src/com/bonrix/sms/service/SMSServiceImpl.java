package com.bonrix.sms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.SMSDao;
import com.bonrix.sms.model.WhiteListNumber;
import com.bonrix.sms.model.CampaignLog;
import com.bonrix.sms.model.HostSetting;
import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsDlrSummary;
import com.bonrix.sms.model.SmsHistory;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.SpamWords;
import com.bonrix.sms.model.SystemParameter;
import com.bonrix.sms.model.failedSmsQueue;

@Service("SMSService")
@Transactional
public class SMSServiceImpl implements SMSService {

	@Autowired
	private SMSDao smsDao;

	@Override
	public List<SmsQueue> getSmsQueue(int total, int isdnd) {
		return smsDao.getSmsQueue(total, isdnd);
	}

	@Override
	public List<SmsApi> getSMSApi(Long uid, int sid) {
		return smsDao.getSMSApi(uid, sid);
	}

	@Override
	public synchronized int creditUpdate(int cnt, Long uid, Long stid) {
		return smsDao.creditupdate(cnt, uid, stid);

	}

	@Override
	public SenderName getSenderName(Long sendid) {
		return smsDao.getSenderName(sendid);
	}

	@Override
	public void saveSmsHistory(SmsHistory smshistory) {
		smsDao.saveSmsHistory(smshistory);
	}

	@Override
	public int deletesmsq(Long qid) {
		return smsDao.deletesmsq(qid);
	}

	@Override
	public String getSMSCreditByUid(Long uid) {
		return smsDao.getSMSCreditByUid(uid);
	}

	@Override
	public String getSMSQAdmin() {
		return smsDao.getSMSQAdmin();
	}

	@Override
	public String getSmsHistoryByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile,
			int service) {
		return smsDao.getSmsHistoryByUid(page, listSize, uid, sdate, edate, mobile, service);
	}

	@Override
	public String getsmscountbyuid(Long lg, String sdate, String edate) {
		return smsDao.getsmscountbyuid(lg, sdate, edate);
	}

	@Override
	public SystemParameter getSysParameterByName(String sysname) {
		return smsDao.getSysParameterByName(sysname);
	}

	@Override
	public List<SmsHistory> getSmsHistoryforDLR(int day, int limit) {
		return smsDao.getSmsHistoryforDLR(day, limit);
	}

	@Override
	public SmsApi getSMSApibyApiId(Long apiid) {
		return smsDao.getSMSApibyApiId(apiid);
	}

	@Override
	public int DLRUpdate(Long hid, String status, String message) {
		return smsDao.DLRUpdate(hid, status, message);
	}

	@Override
	public String getSmsHistoryByAdmin(int page, int listSize, long uid, Date sdate, Date edate, String mobile) {
		return smsDao.getSmsHistoryByAdmin(page, listSize, uid, sdate, edate, mobile);
	}

	@Override
	public List getSmsHistoryByAdmin4Report(int page, int listSize, long uid, Date sdate, Date edate, String mobile) {
		return smsDao.getSmsHistoryByAdmin4Report(page, listSize, uid, sdate, edate, mobile);
	}

	@Override
	public int updateSystemParameterbyName(SystemParameter sp) {
		return smsDao.updateSystemParameterbyName(sp);
	}

	@Override
	public List<Long> checkDndNumbers(String number) {

		return smsDao.checkDndNumbers(number);
	}

	@Override
	public void saveSmsQueue(List<SmsQueue> smsQueue) {
		smsDao.saveSmsQueue(smsQueue);
	}

	@Override
	public int moveToFailedSmsQueue(Long uid) {
		List<SmsQueue> sl = smsDao.getSmsQueueByUid(uid);
		List<failedSmsQueue> fqlst = new ArrayList<failedSmsQueue>();

		for (int i = 0; i < sl.size(); i++) {
			SmsQueue sq = sl.get(i);
			smsDao.deletesmsq(sq.getQid());
			failedSmsQueue fq = new failedSmsQueue();
			fq.setCreatDate(sq.getCreatDate());
			fq.setDndChecked(sq.isDndChecked());
			fq.setMessage(sq.getMessage());
			fq.setNumber(sq.getNumber());
			fq.setPriority(sq.getPriority());
			// fq.setQid(sq.getQid());
			fq.setRetryCount(2);
			fq.setSenderid(sq.getSenderid());
			fq.setStatus(sq.getStatus());
			fq.setStid(sq.getStid());
			fq.setUid(sq.getUid());
			fqlst.add(fq);

		}
		smsDao.saveFailedSmsQueue(fqlst);
		return 0;
	}

	@Override
	public List<SmsQueue> getSmsQueueByUid(Long uid) {
		return smsDao.getSmsQueueByUid(uid);
	}

	@Override
	public List<SmsHistory> getSmsHistorybyUid(int uid, int start, int max) {
		return smsDao.getSmsHistorybyUid(uid, start, max);
	}

	@Override
	public int DLRUpdateByMsgId(Long hid, String status, String msgid) {
		return smsDao.DLRUpdateByMsgId(hid, status, msgid);
	}

	@Override
	public int updatesmppConfig(int cnfigid, int status, int threadid, String connecterror) {
		return smsDao.updatesmppConfig(cnfigid, status, threadid, connecterror);
	}

	public int resetSmppConfig() {
		return smsDao.resetSmppConfig();
	}

	@Override
	public List getQueueCountByUser(long userid) {
		// TODO Auto-generated method stub
		return smsDao.getQueueCountByUser(userid);
	}

	@Override
	public List<SmsDlrSummary> updateSmsSummary(Long uid, String sdate, String edate) {
		return smsDao.updateSmsSummary(uid, sdate, edate);
	}

	@Override
	public SmsHistory getHistoryByMsgid(Long msgid) {
		return smsDao.getHistoryByMsgid(msgid);
	}

	@Override
	public HostSetting getHostSetting(String user) {
		return smsDao.getHostSetting(user);
	}

	@Override
	public SenderName getSenderNameByUid(Long uid, String Sendername) {
		return smsDao.getSenderNameByUid(uid, Sendername);
	}

	@Override
	public CampaignLog saveCampaign(CampaignLog cl) {
		return smsDao.saveCampaign(cl);
	}

	@Override
	public List<SmsHistory> getSmsHistorybyCampid(int campid) {
		return smsDao.getSmsHistorybyCampid(campid);
	}

	@Override
	public CampaignLog getCampaignByMsgid(String campid) {
		return smsDao.getCampaignByMsgid(campid);
	}

	@Override
	public int DelayDLRUpdate(String status, int hour) {
		return smsDao.DelayDLRUpdate(status, hour);
	}

	@Override
	public String getSeduleMessageByUid(int page, int listSize, long uid, Date sdate, Date edate, String mobile,
			int service) {
		return smsDao.getSeduleMessageByUid(page, listSize, uid, sdate, edate, mobile, service);
	}

	@Override
	public int deleteMultismsQ(String qid) {
		return smsDao.deleteMultismsQ(qid);
	}

	@Override
	public int saveSpamWord(SpamWords sm) {
		return smsDao.saveSpamWord(sm);
	}

	@Override
	public List<SpamWords> getSpamWords() {
		return smsDao.getSpamWords();
	}

	@Override
	public int saveWhiteList(WhiteListNumber sm) {
		return smsDao.saveWhiteList(sm);
	}

	@Override
	public List<String> getWhiteList(int type) {
		return smsDao.getWhiteList(type);
	}

	@Override
	public int deleteWhiteList(int sm) {

		return smsDao.deleteBlackWhiteList(sm);
	}

	@Override
	public List<WhiteListNumber> getWhiteListObj() {
		// TODO Auto-generated method stub
		return smsDao.getWhiteListObj(0);
	}

	@Override
	public String getBlockedNumbers(int page, int listSize, String mobile) {
		return smsDao.getBlockedNumbers(page, listSize, mobile);
	}

	@Override
	public int saveFailedSmsQueue(List<failedSmsQueue> smsQueue) {
		// TODO Auto-generated method stub
		return 0;
	}

}

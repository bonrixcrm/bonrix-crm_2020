package com.bonrix.sms.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;  
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class SmsExcelBuilder extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if(model.get("type").toString().equalsIgnoreCase("followUpData")) {
			
			System.out.println("this is followup excel for download");
			
			try {
			
			Sheet sheet = workbook.createSheet("new sheet");
			
			  Row row = sheet.createRow((short)0);
			  row.createCell(0).setCellValue("Sr. No.");
			  row.createCell(1).setCellValue("First Name");
		      row.createCell(2).setCellValue("Last Name");
		      row.createCell(3).setCellValue("Email");
		      row.createCell(4).setCellValue("Mobile No");
		      row.createCell(5).setCellValue("Company");
		      row.createCell(6).setCellValue("UserName");
		      row.createCell(7).setCellValue("Lead State");
		      row.createCell(8).setCellValue("Sheduale Date");
		      row.createCell(9).setCellValue("Lead Process Status");
		      row.createCell(10).setCellValue("Calling Time");
		      row.createCell(11).setCellValue("Call Duration");
		      row.createCell(12).setCellValue("Call Status");
		      row.createCell(13).setCellValue("satus");
		      row.createCell(14).setCellValue("FollowUp Type");
		      row.createCell(15).setCellValue("FollowUp Device");
		      row.createCell(16).setCellValue("Fail Status");
		      
		      
		      List listq = (List) model.get("excelList");
		      
		      int i=0;
				if (listq.size() != 0) {
					 Object[] result = null;
					for (i = 0; i < listq.size(); i++) {
						result = (Object[]) listq.get(i);
					 Row rowadd = sheet.createRow((short)i+1);

					    rowadd.createCell(0).setCellValue((i + 1));
						rowadd.createCell(1).setCellValue(result[0]== null || "(NULL)".equals(result[0].toString())  ? "N/A" : result[0].toString());
			            rowadd.createCell(2).setCellValue(result[1]== null || "(NULL)".equals(result[1].toString())  ? "N/A" : result[1].toString());
			            rowadd.createCell(3).setCellValue(result[2]== null || "(NULL)".equals(result[2].toString())   ? "N/A" : result[2].toString());
			            rowadd.createCell(4).setCellValue(result[5]== null || "(NULL)".equals(result[5].toString())  ? "N/A" :  result[5].toString());
			            rowadd.createCell(5).setCellValue(result[6]== null || "(NULL)".equals(result[6].toString())   ? "N/A" : result[6].toString());
			            rowadd.createCell(6).setCellValue(result[8]== null || "(NULL)".equals(result[8].toString())   ? "N/A" : result[8].toString());
			            rowadd.createCell(7).setCellValue(result[4]== null || "(NULL)".equals(result[4].toString())  ? "N/A" : result[4].toString());
			            rowadd.createCell(8).setCellValue(result[7]== null || "(NULL)".equals(result[7].toString())   ? "N/A" : result[7].toString());
			            rowadd.createCell(9).setCellValue(result[3]== null || "(NULL)".equals(result[3].toString())   ? "N/A" : result[3].toString());
			            rowadd.createCell(10).setCellValue(result[9]== null || "(NULL)".equals(result[9].toString())  ? "N/A" : result[9].toString());
		                rowadd.createCell(11).setCellValue(result[11]== null || "(NULL)".equals(result[11].toString())  ? "N/A" : result[11].toString());
		                rowadd.createCell(12).setCellValue(result[12]== null || "(NULL)".equals(result[12].toString())  ? "N/A" : result[12].toString());
			            rowadd.createCell(13).setCellValue(result[10]== null || "(NULL)".equals(result[10].toString())  ? "N/A" : result[10].toString());
			            rowadd.createCell(14).setCellValue((result[13] == null || "(NULL)".equals(result[13].toString())) ? "N/A" : result[13].toString());
			            rowadd.createCell(15).setCellValue(result[14]== null || "(NULL)".equals(result[14].toString())   ? "N/A" : result[14].toString());
			            rowadd.createCell(16).setCellValue(result[15]== null || "(NULL)".equals(result[15].toString())   ? "N/A" : result[15].toString());
			            
//			            rowadd.createCell(1).setCellValue((result[0].toString() == null) ? "N/A" : result[0].toString());
			            
					}
				}
					}catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
					}
		}else {
		try {
	
		      Sheet sheet = workbook.createSheet("new sheet");
			
			  Row row = sheet.createRow((short)0);
			  row.createCell(0).setCellValue("Id");
			  row.createCell(1).setCellValue("First Name");
		      row.createCell(2).setCellValue("Last Name");
		      row.createCell(3).setCellValue("Category");
		      row.createCell(4).setCellValue("Email");
		      row.createCell(5).setCellValue("Mobile No");
		      row.createCell(6).setCellValue("Lead State");
		      row.createCell(7).setCellValue("Process State");
		      row.createCell(8).setCellValue("Created Date");
		      row.createCell(9).setCellValue("Schedule Date");
		      row.createCell(10).setCellValue("Company");
		      row.createCell(11).setCellValue("City");
		      row.createCell(12).setCellValue("State");
		      row.createCell(13).setCellValue("Country");
		      row.createCell(14).setCellValue("CompanyName");
		      row.createCell(15).setCellValue("Website");
		      row.createCell(16).setCellValue("CSVData");
		      row.createCell(17).setCellValue("Telecaller");
		      row.createCell(18).setCellValue("Remark");
		      row.createCell(19).setCellValue("Call Status");
		      row.createCell(20).setCellValue("Success Status");
		      row.createCell(21).setCellValue("Fail Status");
		      row.createCell(22).setCellValue("Last CallingTime");
		      row.createCell(23).setCellValue("Call Duration");
		      
		  
		      List listq = (List) model.get("excelList");
		      
		      int i=0;
				if (listq.size() != 0) {
					 Object[] result = null;
					for (i = 0; i < listq.size(); i++) {
						result = (Object[]) listq.get(i);
					 Row rowadd = sheet.createRow((short)i+1);

					    rowadd.createCell(0).setCellValue(result[0]== null || "(NULL)".equals(result[0].toString())? "N/A" : result[0].toString());
						rowadd.createCell(1).setCellValue(result[1]== null || "(NULL)".equals(result[1].toString()) ? "N/A" : result[1].toString());
			            rowadd.createCell(2).setCellValue(result[2]== null || "(NULL)".equals(result[2].toString()) ? "N/A" : result[2].toString());
			            rowadd.createCell(3).setCellValue(result[3]== null || "(NULL)".equals(result[3].toString())  ? "N/A" : result[3].toString());
			            rowadd.createCell(4).setCellValue(result[4]== null || "(NULL)".equals(result[4].toString())  ? "N/A" :  result[4].toString());
			            rowadd.createCell(5).setCellValue(result[5]== null || "(NULL)".equals(result[5].toString()) ? "N/A" : result[5].toString());
			            rowadd.createCell(6).setCellValue(result[6]== null || "(NULL)".equals(result[6].toString())  ? "N/A" : result[6].toString());
			            rowadd.createCell(7).setCellValue(result[7]== null || "(NULL)".equals(result[7].toString()) ? "N/A" : result[7].toString());
			            rowadd.createCell(8).setCellValue(result[8]== null || "(NULL)".equals(result[8].toString())  ? "N/A" : result[8].toString());
//			            rowadd.createCell(9).setCellValue(result[9]== null || "(NULL)".equals(result[9].toString())  ? "N/A" : result[9].toString());
			            rowadd.createCell(10).setCellValue(result[14]== null || "(NULL)".equals(result[14].toString()) ? "N/A" : result[14].toString());
		                rowadd.createCell(10).setCellValue(result[10]== null || "(NULL)".equals(result[10].toString()) ? "N/A" : result[10].toString());
		                rowadd.createCell(11).setCellValue(result[11]== null || "(NULL)".equals(result[11].toString()) ? "N/A" : result[11].toString());
			            rowadd.createCell(12).setCellValue(result[12]== null || "(NULL)".equals(result[12].toString()) ? "N/A" : result[12].toString());
			            rowadd.createCell(13).setCellValue(result[13]== null || "(NULL)".equals(result[13].toString()) ? "N/A" : result[13].toString());
			            rowadd.createCell(14).setCellValue(result[14]== null || "(NULL)".equals(result[14].toString()) ? "N/A" : result[14].toString());
			            rowadd.createCell(15).setCellValue(result[15]== null || "(NULL)".equals(result[15].toString())  ? "N/A" : result[15].toString());
			            rowadd.createCell(16).setCellValue(result[16]== null || "(NULL)".equals(result[16].toString()) ? "N/A" : result[16].toString());
			            rowadd.createCell(17).setCellValue(result[17]== null || "(NULL)".equals(result[17].toString())  ? "N/A" : result[17].toString());
			            rowadd.createCell(18).setCellValue(result[18]== null || "(NULL)".equals(result[18].toString())  ? "N/A" : result[18].toString());
			            rowadd.createCell(19).setCellValue(result[19]== null || "(NULL)".equals(result[19].toString()) ? "N/A" : result[19].toString());
			            rowadd.createCell(20).setCellValue(result[20]== null || "(NULL)".equals(result[20].toString())  ? "N/A" : result[20].toString());
			            rowadd.createCell(21).setCellValue(result[21]== null || "(NULL)".equals(result[21].toString())  ? "N/A" : result[21].toString());
			            rowadd.createCell(22).setCellValue(result[22]== null || "(NULL)".equals(result[22].toString())  ? "N/A" : result[22].toString());
			            rowadd.createCell(23).setCellValue(result[23]== null || "(NULL)".equals(result[23].toString()) ? "N/A" : result[23].toString());
			          //  rowadd.createCell(24).setCellValue(result[24].toString()== null ? "N/A" : result[24].toString());
					}
				}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}
}
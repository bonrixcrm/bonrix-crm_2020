package com.bonrix.sms.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;

import com.bonrix.sms.model.SmsHistory;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

//http://www.codejava.net/frameworks/spring/spring-mvc-with-excel-view-example-apache-poi-and-jexcelapi

public class SmsHistoryFilesBuilder {

	public void fileMaker(List listSmsHistory, String path, String fileNM, HttpServletResponse response)
			throws Exception {

		// XLS
		// /////////////////////////////////////////////////////////////////////////////////////////////
		String xlsFileName = new String();
		try {

			HSSFWorkbook workbook = new HSSFWorkbook();
			xlsFileName = fileNM + ".xls";

			FileOutputStream fileOut = new FileOutputStream(path + "\\" + xlsFileName);

			// create a new Excel sheet
			HSSFSheet sheet = workbook.createSheet(xlsFileName);
			sheet.setDefaultColumnWidth(30);

			// create style for header cells
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setFontName("Arial");
			style.setFillForegroundColor(HSSFColor.BLUE.index);
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.WHITE.index);
			style.setFont(font);

			// create header row
			HSSFRow header = sheet.createRow(0);

			header.createCell(0).setCellValue("SrNo");
			header.getCell(0).setCellStyle(style);

			header.createCell(1).setCellValue("Mobile no");
			header.getCell(1).setCellStyle(style);

			header.createCell(2).setCellValue("Message");
			header.getCell(2).setCellStyle(style);

			header.createCell(3).setCellValue("Sent On");
			header.getCell(3).setCellStyle(style);

			header.createCell(4).setCellValue("Status");
			header.getCell(4).setCellStyle(style);

			header.createCell(5).setCellValue("Service");
			header.getCell(5).setCellStyle(style);

			header.createCell(6).setCellValue("SenderName");
			header.getCell(6).setCellStyle(style);

			/*
			 * header.createCell(6).setCellValue("UserName");
			 * header.getCell(6).setCellStyle(style);
			 */

			// create data rows
			int rowCount = 1;
			for (int i = 0; i < listSmsHistory.size(); i++) {

				Object[] smsHistory = (Object[]) listSmsHistory.get(i);
				HSSFRow aRow = sheet.createRow(rowCount++);
				aRow.createCell(0).setCellValue(i + "");
				aRow.createCell(1).setCellValue(smsHistory[3].toString());
				aRow.createCell(2).setCellValue(smsHistory[2].toString());
				aRow.createCell(3).setCellValue(smsHistory[5].toString());
				aRow.createCell(4).setCellValue(smsHistory[7].toString());
				aRow.createCell(5).setCellValue(smsHistory[8].toString());
				aRow.createCell(6).setCellValue(smsHistory[9].toString());
			}

			workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// CSV
		// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// StringBuilder sb=new StringBuilder();
		//
		// sb.append("Message, Mobile No, Sent On, Status, Service, SenderName,
		// UserName \n");
		//
		// for (SmsHistory smsHistory : listSmsHistory) {
		// sb.append(smsHistory.getMessage()).append(",")
		// .append(smsHistory.getMobileNumber()).append(",")
		// .append(smsHistory.getSentDateTime()).append(",")
		// .append(smsHistory.getStatus()).append(",");
		// // .append(smsHistory.getService()).append(",")
		// // .append(smsHistory.getSenderName()).append(",")
		// // .append(smsHistory.getUserName()).append("\n");
		// }
		// FileOutputStream fileOut1 = new FileOutputStream(path + "\\"+ fileNM
		// + ".csv");
		// fileOut1.write(sb.toString().getBytes());
		// fileOut1.flush();
		// fileOut1.close();

		// PDF
		// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		String pdfFileName = new String();
		try {
			pdfFileName = fileNM + ".pdf";

			OutputStream file = new FileOutputStream(new File(path + "\\" + pdfFileName));
			Document document = new Document();
			PdfWriter.getInstance(document, file);
			document.open();
			document.add(new Paragraph(pdfFileName));

			PdfPTable table = new PdfPTable(7);
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 1.0f, 3.0f, 5.0f, 2.5f, 2.5f, 2.0f, 2.0f });
			table.setSpacingBefore(10);

			// define font for table header row
			// Font font = (Font) FontFactory.getFont(FontFactory.HELVETICA);
			// font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(new BaseColor(140, 221, 8));
			cell.setPadding(5);

			// write table header
			table.addCell("SrNo");
			table.addCell("Mobile no");
			table.addCell("Message");

			table.addCell("Sent On");
			table.addCell("Status");
			table.addCell("Service");
			table.addCell("SenderName");
			table.addCell("UserName");

			for (int i = 0; i < listSmsHistory.size(); i++) {
				Object[] smsHistory = (Object[]) listSmsHistory.get(i);
				table.addCell(i + "");
				table.addCell(smsHistory[3].toString());
				table.addCell(smsHistory[2].toString());
				table.addCell(smsHistory[5].toString());
				table.addCell(smsHistory[7].toString());
				table.addCell(smsHistory[8].toString());
				table.addCell(smsHistory[9].toString());
				// table.addCell(smsHistory[10].toString());
			}
			document.add(table);
			document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

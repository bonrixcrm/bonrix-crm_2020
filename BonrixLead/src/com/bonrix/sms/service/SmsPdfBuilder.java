package com.bonrix.sms.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bonrix.sms.model.SmsHistory;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * This view class generates a PDF document 'on the fly' based on the data
 * contained in the model.
 */
public class SmsPdfBuilder extends AbstractITextPdfView {

	@Override
	public void buildPdfDocument(Map<String, Object> model, Document doc, PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			System.out.println("in tho the download pdf" + new Date());
			// get data model which is passed by the Spring container
		        List listq = (List) model.get("pdflList");
		
		        System.out.println("List Size : "+listq.size());
			String title = new String("Leads Detail");

			doc.add(new Paragraph(title));

			PdfPTable table = new PdfPTable(15);
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 50.0f, 80.0f, 80.0f, 80.5f, 80.5f, 80.0f, 80.0f , 80.0f, 80.0f, 80.0f, 80.0f, 80.0f, 80.0f, 80.0f, 80.0f});
			table.setSpacingBefore(5);

			// define font for table header row
			Font font = FontFactory.getFont(FontFactory.HELVETICA);
			font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();

			cell.setBackgroundColor(BaseColor.BLUE);

			cell.setPhrase(new Phrase("Id", font));

			table.addCell(cell);
			cell.setPhrase(new Phrase("Category", font));
			table.addCell(cell);

			// write table header
			cell.setPhrase(new Phrase("Company", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Created Date", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Email", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("First Name", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Last Name", font));
			table.addCell(cell);
			
			cell.setPhrase(new Phrase("Process State", font));
			table.addCell(cell);
			
			cell.setPhrase(new Phrase("Lead State", font));
			table.addCell(cell);
			
			
			cell.setPhrase(new Phrase("Mobile No", font));
			table.addCell(cell);
			
			/*cell.setPhrase(new Phrase("Telecaller", font));
			table.addCell(cell);
*/			
		/*	cell.setPhrase(new Phrase("Other Data", font));
			table.addCell(cell);*/
			
			cell.setPhrase(new Phrase("CompanyName", font));
			table.addCell(cell);
			
			cell.setPhrase(new Phrase("Website", font));
			table.addCell(cell);
			
			cell.setPhrase(new Phrase("Country", font));
			table.addCell(cell);
			
			cell.setPhrase(new Phrase("State", font));
			table.addCell(cell);
			
			cell.setPhrase(new Phrase("City", font));
			table.addCell(cell);
			
			
			

			// cell.setPhrase(new Phrase("UserName", font));
			// table.addCell(cell);

			// write table row data
			/*for (int i = 0; i < listSmsHistoryList.size(); i++) {*/
				// for (Object[] smsHistory : (Object[])listSmsHistoryList) {
				// sh.hid,sh.status,sh.message,sh.mobileNumber,sh.result,DATE_FORMAT(sh.sentdatetime,'%d/%m/%Y
				// %H:%i:%S'),
				// DATE_FORMAT(sh.submitdatetime,'%d/%m/%Y
				// %H:%i:%S'),sh.dlrstatus,st.type,sh.sid,us.username

				/*Object[] smsHistory = (Object[]) listSmsHistoryList.get(i);
				table.addCell(i + "");
				table.addCell(smsHistory[3].toString());
				table.addCell(smsHistory[2].toString());
				table.addCell(smsHistory[5].toString());
				table.addCell(smsHistory[7].toString());
				table.addCell(smsHistory[8].toString());
				table.addCell(smsHistory[9].toString());*/
				// table.addCell(smsHistory[10].toString());
		      Object[] result = null;

				if (listq.size() != 0) {

					for (int i = 0; i < listq.size(); i++) {
				        System.out.println("List Size : "+listq.size());

						result = (Object[]) listq.get(i);
						
						System.out.println(result[0].toString());
						System.out.println(result[3].toString());
						System.out.println(result[10].toString());
						System.out.println(result[8].toString());
						System.out.println(result[4].toString());
						System.out.println(result[1].toString());
						System.out.println(result[2].toString());
						System.out.println(result[7].toString());
						System.out.println(result[6].toString());
						System.out.println(result[5].toString());
						System.out.println(result[17].toString());
						//System.out.println(result[16].toString());
						System.out.println(result[14].toString());
						//System.out.println(result[15].toString());
						System.out.println(result[13].toString());
						System.out.println(result[12].toString());
						System.out.println(result[11].toString());
						System.out.println("=======================================");
			            
			            
						table.addCell(result[0].toString()== null ? "N/A" : result[0].toString());
			            table.addCell(result[3].toString()== null ? "N/A" : result[3].toString());
			            table.addCell(result[10].toString()== null ? "N/A" : result[10].toString());
			            table.addCell(result[8].toString()== null ? "N/A" : result[8].toString());
			            table.addCell(result[4].toString()== null ? "N/A" : result[4].toString());
			            table.addCell(result[1].toString()== null ? "N/A" : result[1].toString());
			            table.addCell(result[2].toString()== null ? "N/A" : result[2].toString());
			            table.addCell(result[7].toString()== null ? "N/A" : result[7].toString());
			            table.addCell(result[6].toString()== null ? "N/A" : result[6].toString());
			            table.addCell(result[5].toString()== null ? "N/A" : result[5].toString());
		                table.addCell(result[17].toString()== null ? "N/A" : result[17].toString());
			          //  table.addCell(result[16].toString()== null ? "N/A" : result[16].toString());
			            table.addCell(result[14].toString()== null ? "N/A" : result[14].toString());
			          //  table.addCell(result[15].toString()== null ? "N/A" : result[15].toString());
			            table.addCell(result[13].toString()== null ? "N/A" : result[13].toString());
			            table.addCell(result[12].toString()== null ? "N/A" : result[12].toString());
			            table.addCell(result[11].toString()== null ? "N/A" : result[11].toString());
			        

					}

				}
			

			doc.add(table);
			doc.close();
			// file.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}

package com.bonrix.sms.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.LoginLog;
import com.bonrix.sms.model.ServiceType;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;

@Service
public interface UserService {

	public Long saveRegUser(User user);

	UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException;

	public SmsQueue saveSms(SmsQueue smsQueue);

	// public void saveSmsqueue(List<SmsQueue> smsQueue);
	public void saveHttpGateway(SmsApi smsApi);

	public String findHttpGateway(long uid);

	public void deleteHttpGateway(Long aid);

	public String getQueueCount(long userid);

	public String getSentSms(long userid);

	public String getSentSmsToday(long userid, String date);

	public void updateUser(User user);

	public List<Tamplate> getTemplateByuser(long userid);

	public ServiceType getServiceTypeById(Long senderId);

	public ServiceType getServiceTypeByName(String senderName);

	public void saveApiKey(ApiKey apiKey);

	public ApiKey gettAPiByUid(long uid, String key, boolean ishidden);

	public void deteleApiKey(String aid);

	public List getSmsHistory(int page, int listSize);

	public List<String> getContactByGid(long parseLong);

	public void updateSmsQue(List<SmsQueue> nonDndNOs);

	public void saveEmailTemplate(EmailTemplate emailTemplate);

	public void saveMessageTemplate(MessageTemplate messageTemplate);

	public void saveLoginLog(LoginLog lg);

	public void addUserRole(UserRole role);

	public ApiKey gettAPiByUidForNotification(int tcallerid, String api, boolean b);

	public User findByUserName(String userName);
	
	public List<Object[]> leadByLeadId(int leadid);
	
	public List<Object[]> getSMSTempByLeadStat(String leadStat);
	
	public List<Object[]> CheckSMSStat(String uid);
	
	public List<Object[]> getExtraParam(int leadId);
	
	public int updateExtraParams(String paramJson,int leadId);
	
	public List<Object[]> getAudioFilesPath(int teleId, String stDate, String endDate,int compId);
	
}

package com.bonrix.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.UserDao;
import com.bonrix.sms.model.ApiKey;
import com.bonrix.sms.model.EmailTemplate;
import com.bonrix.sms.model.MessageTemplate;
import com.bonrix.sms.model.LoginLog;
import com.bonrix.sms.model.ServiceType;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.User;
import com.bonrix.sms.model.UserRole;

/**
 * @author Niraj Thakar
 *
 */
@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		return null;
	}

	public Long saveRegUser(User user) {

		return userDao.saveRegUser(user);

	}

	public SmsQueue saveSms(SmsQueue smsQueue) {
		return userDao.saveSms(smsQueue);
	}

	@Override
	public void saveHttpGateway(SmsApi smsApi) {
		userDao.saveSmsApi(smsApi);

	}

	@Override
	public String getQueueCount(long userid) {
		return userDao.getQueueCount(userid);
	}

	@Override
	public String getSentSms(long userid) {

		return userDao.getSentSms(userid);
	}

	@Override
	public String getSentSmsToday(long userid, String date) {

		return userDao.getSentSmsToday(userid, date);
	}

	@Override
	public void updateUser(User user) {
		userDao.updateUser(user);

	}

	@Override
	public List<Tamplate> getTemplateByuser(long userid) {
		return userDao.getTemplateByuser(userid);
	}

	@Override
	public ServiceType getServiceTypeById(Long senderId) {
		return userDao.getServiceTypeById(senderId);
	}

	@Override
	public ServiceType getServiceTypeByName(String senderName) {

		return userDao.getServiceTypeByName(senderName);
	}

	@Override
	public void saveApiKey(ApiKey apiKey) {
		userDao.saveApiKey(apiKey);
	}

	@Override
	public ApiKey gettAPiByUid(long uid, String key, boolean ishidden) {
		return userDao.gettAPiByUid(uid, key, ishidden);
	}

	@Override
	public void deteleApiKey(String aid) {
		userDao.deteleApiKey(aid);
	}

	@Override
	public List getSmsHistory(int page, int listSize) {
		return userDao.getSmsHistory(page, listSize);
	}

	@Override
	public String findHttpGateway(long uid) {
		// TODO Auto-generated method stub
		return userDao.findHttpGateway(uid);
	}

	@Override
	public void deleteHttpGateway(Long aid) {
		userDao.deleteHttpGateway(aid);
	}

	/*
	 * @Override public void saveSmsqueue(List<SmsQueue> smsQueue) {
	 * userDao.saveSmsQueue(smsQueue); }
	 */

	@Override
	public List<String> getContactByGid(long parseLong) {
		return userDao.getContactByGid(parseLong);
	}

	@Override
	public void updateSmsQue(List<SmsQueue> nonDndNOs) {
		userDao.updateSmsQue(nonDndNOs);
	}

	@Override
	public void saveEmailTemplate(EmailTemplate emailTemplate) {
		userDao.saveEmailTemplate(emailTemplate);
	}

	@Override
	public void saveMessageTemplate(MessageTemplate messageTemplate) {
		userDao.saveMessageTemplate(messageTemplate);
	}

	public void saveLoginLog(LoginLog lg) {
		userDao.saveLoginLog(lg);
	}

	@Override
	public void addUserRole(UserRole role) {
		userDao.addUserRole(role);
	}

	@Override
	public ApiKey gettAPiByUidForNotification(int tcallerid, String api, boolean b) {
		return userDao.gettAPiByUidForNotification(tcallerid,api,b);
	}

	@Override
	public User findByUserName(String userName) {
		return userDao.findByUserName(userName);
	}
	
	@Override
	public List<Object[]> leadByLeadId(int leadid) {
		return userDao.leadByLeadId(leadid);
	}

	@Override
	public List<Object[]> getSMSTempByLeadStat(String leadStat) {
		return userDao.getSMSTempByLeadStat(leadStat);
	}

	@Override
	public List<Object[]> CheckSMSStat(String uid) {
		return userDao.CheckSMSStat(uid);
	}

	@Override
	public List<Object[]> getExtraParam(int leadId) {
		return userDao.getExtraParam(leadId);
	}

	@Override
	public int updateExtraParams(String paramJson,int leadId) {
		return userDao.updateExtraParams(paramJson,leadId);
	}

	@Override
	public List<Object[]> getAudioFilesPath(int teleId, String stDate, String endDate,int compId) {
		return userDao.getAudioFilesPath(teleId, stDate, endDate,compId);
	}

}

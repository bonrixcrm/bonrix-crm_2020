package com.bonrix.sms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bonrix.sms.model.WhatsappMedia;

@Service
public interface WhatsAppMediaService {
	
	void saveWhatsAppMedia(WhatsappMedia media);
	 
	List<WhatsappMedia> getWhatsAppMedia(long id);
	
	public WhatsappMedia getWhatsAppMediaTemplate(long id);

}

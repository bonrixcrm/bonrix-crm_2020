package com.bonrix.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.WhatsAppMediaDao;
import com.bonrix.sms.model.WhatsappMedia;

@Service("WhatsAppMediaService") 
@Transactional  
public class WhatsAppMediaServiceImpl implements WhatsAppMediaService {

	@Autowired 
	WhatsAppMediaDao dao;  
	
	@Override
	public void saveWhatsAppMedia(WhatsappMedia media) {
		dao.saveWhatsAppMedia(media);
	
	}

	@Override
	public List<WhatsappMedia> getWhatsAppMedia(long id) {
		return dao.getWhatsAppMedia(id);
	}

	@Override
	public WhatsappMedia getWhatsAppMediaTemplate(long id) {
		return dao.getWhatsAppMediaTemplate(id);
	}
}

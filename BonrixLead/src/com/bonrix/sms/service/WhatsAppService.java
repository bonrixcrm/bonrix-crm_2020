package com.bonrix.sms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bonrix.sms.model.Tamplate;
import com.bonrix.sms.model.WhatsAppSettings;
import com.bonrix.sms.model.WhatsAppTemplate;
import com.bonrix.sms.model.WhatsappMedia;

@Service
public interface WhatsAppService {  
	
	void saveWhatsAppSettings(WhatsAppSettings settings); 
	
	List GetWhatsAppSettingsByUserId(long userid);
	
	public List  getWhatsAppTemplateByuser(long userid);
	
	void saveWhatsAppTemplate(WhatsAppTemplate template);
	
	WhatsAppTemplate GetWhatsAppTemplate(long templateId);
	
	void deleteTemplate(long templateId);
	
	void updateTemplateMedia(long templateId,String mediaUrl);
	
	WhatsAppTemplate GetWhatsAppTemplateByName(long template_id);
	
	void deleteWhatsAppSettings(long settingId);
	

	
	
	
	

}

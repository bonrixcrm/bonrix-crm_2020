package com.bonrix.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.WhatsAppDao;
import com.bonrix.sms.model.WhatsAppSettings;
import com.bonrix.sms.model.WhatsAppTemplate;

@Service("WhatsAppService") 
@Transactional  
public class WhatsAppServiceImpl implements WhatsAppService {

	@Autowired
	WhatsAppDao dao;
	@Override
	public void saveWhatsAppSettings(WhatsAppSettings settings) {
		dao.SaveWhatsAppSetting(settings);
		
	}
	@Override
	public List GetWhatsAppSettingsByUserId(long userid) {
		return dao.GetWhatsAppSettingsByUserId(userid);
	}
	@Override
	public List  getWhatsAppTemplateByuser(long userid) {
		return dao.getWhatsAppTemplateByuser(userid);
	}
	@Override
	public void saveWhatsAppTemplate(WhatsAppTemplate template) {
		dao.saveWhatsAppTemplate(template);
		 
	}
	@Override
	public WhatsAppTemplate GetWhatsAppTemplate(long templateId) {
		return dao.GetWhatsAppTemplate(templateId);
	}
	@Override
	public void deleteTemplate(long templateId) {
		dao.deleteTemplate(templateId);
		
	}
	@Override
	public void updateTemplateMedia(long templateId, String mediaUrl) {
		dao.updateTemplateMedia(templateId, mediaUrl);
	}
	@Override
	public WhatsAppTemplate GetWhatsAppTemplateByName(long template_id) {
		return dao.GetWhatsAppTemplateByName(template_id);
	}
	@Override
	public void deleteWhatsAppSettings(long settingId) {
		 dao.deleteWhatsAppSettings(settingId);
	}
	

}

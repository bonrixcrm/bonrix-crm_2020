package com.bonrix.sms.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bonrix.sms.model.WhatsappOutbox;

@Service
public interface WhatsappOutboxService {

	void saveWhatsappOutbox(WhatsappOutbox outboxLog);

	public List<WhatsappOutbox> getWhatsappOutbox(long companyId, Date sendtime);
	
	public WhatsappOutbox getMessage(String id);
	
	public void updateStatus(WhatsappOutbox outboxLog);
	
	public List<WhatsappOutbox> getTelecallerWhatsappOutbox(Long telecallerId, Date sendtime);

}

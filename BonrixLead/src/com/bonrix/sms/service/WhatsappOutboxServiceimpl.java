package com.bonrix.sms.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bonrix.sms.dao.WhatsappOutboxDao;
import com.bonrix.sms.model.WhatsappOutbox;

@Service("WhatsappOutboxService")
@Transactional
public class WhatsappOutboxServiceimpl implements WhatsappOutboxService {

	@Autowired
	WhatsappOutboxDao outbox;

	@Override
	public void saveWhatsappOutbox(WhatsappOutbox outboxLog) {
		outbox.saveWhatsappOutbox(outboxLog);
	}

	@Override
	public List<WhatsappOutbox> getWhatsappOutbox(long companyId, Date sendtime) {
		return outbox.getWhatsappOutbox(companyId, sendtime);
	}

	@Override
	public WhatsappOutbox getMessage(String id) {
		return outbox.getMessage(id);
	}

	@Override
	public void updateStatus(WhatsappOutbox outboxLog) {
    outbox.updateStatus(outboxLog);		
	}

	@Override
	public List<WhatsappOutbox> getTelecallerWhatsappOutbox(Long telecallerId, Date sendtime) {
		return outbox.getTelecallerWhatsappOutbox(telecallerId, sendtime);
	}

}

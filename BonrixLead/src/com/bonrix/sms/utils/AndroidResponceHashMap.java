package com.bonrix.sms.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class AndroidResponceHashMap {
	public static Map<String, String> connectedAndroidClient = new ConcurrentHashMap<String, String>();
	
	public static Map<String, String> sendsuccess = new ConcurrentHashMap<String, String>();
	
	
    private  static AndroidResponceHashMap instance;

    public static AndroidResponceHashMap getInstance()
     {
        synchronized (AndroidResponceHashMap.class)
        {
            if (instance == null)
            {
            	instance = new AndroidResponceHashMap();
            }
        }
       return instance;
    }
    public  String AddClient(String key,String value) 
    {   
     return connectedAndroidClient.put(key, value); 
     
    }
    
    public  String getClient(String key) 
    {   
     return connectedAndroidClient.get(key);                
    }
    
    public  String RemoveClient(String key) 
    {   
     return connectedAndroidClient.remove(key);                
    }
    
    public int getClientSize() 
    {
        return connectedAndroidClient.size();
        
    }
}

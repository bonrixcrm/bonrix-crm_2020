package com.bonrix.sms.utils;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.Session;

import com.bonrix.sms.model.WebSocketObj;


public class CRMHashMap {
	
	public static Map<String, WebSocketObj> connectedClient = new ConcurrentHashMap<String, WebSocketObj>();
	
	public static Map<Integer, Integer> otpauth = new ConcurrentHashMap<Integer, Integer>();
    private  static CRMHashMap instance;
//com.bonrix.sms.utils.CRMHashMap map=null;   
    public static CRMHashMap getInstance()
     {
        synchronized (CRMHashMap.class)
        {
            if (instance == null)
            {
            	instance = new CRMHashMap();
            }
        }
       return instance;
    }
    public  WebSocketObj AddClient(String key,WebSocketObj value) 
    {   
    	
     return connectedClient.put(key, value); 
     
    }
    
    public  WebSocketObj getClient(String key) 
    {   
     return connectedClient.get(key);                
    }
    
    public  WebSocketObj RemoveClient(String key) 
    {   
     return connectedClient.remove(key);                
    }
    
    public int getClientSize() 
    {
        return connectedClient.size();
        
    }


}

package com.bonrix.sms.utils;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.Session;


public class CRMHashMap2 {
	
	public static Map<String, ArrayList<String>> connectedClient = new ConcurrentHashMap<String, ArrayList<String>>();
    private  static CRMHashMap2 instance;
//com.bonrix.sms.utils.CRMHashMap map=null;   
    public static CRMHashMap2 getInstance()
     {
        synchronized (CRMHashMap2.class)
        {
            if (instance == null)
            {
            	instance = new CRMHashMap2();
            }
        }
       return instance;
    }
    public  ArrayList<String> AddClient(String key,ArrayList<String> value) 
    {   
     return connectedClient.put(key, value); 
     
    }
    
    public  ArrayList<String> getClient(String key) 
    {   
     return connectedClient.get(key);                
    }
    
    public  ArrayList<String> RemoveClient(String key) 
    {   
     return connectedClient.remove(key);                
    }
    
    public int getClientSize() 
    {
        return connectedClient.size();
        
    }


}

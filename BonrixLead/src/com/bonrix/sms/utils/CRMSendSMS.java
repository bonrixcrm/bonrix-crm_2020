package com.bonrix.sms.utils;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.hibernate.exception.spi.TemplatedViolatedConstraintNameExtracter;
import org.json.JSONObject;

import com.bonrix.sms.model.Sentsmslog;
import com.bonrix.sms.model.Smstemplate;
import com.bonrix.sms.model.Tallycaller;
import com.bonrix.sms.model.User;
import com.bonrix.sms.service.AdminService;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

public class CRMSendSMS {
	
	
	
	public void SendWelcomeSMS(final AdminService adminService,String name,final int compId,int catId,String mobNo,String telecallerId)
	{
		System.out.println("Send SMS is Called..................");
		List categorylist = adminService.getCategoryById(catId);
		int tempId=0;
		for (int i = 0; i < categorylist.size(); i++) {

			Object[] result = (Object[]) categorylist.get(i);
			tempId=(int) result[4];
			System.out.println("hi  "+result[4]);
			
			}
		
		Smstemplate smstemp=new Smstemplate();
		smstemp=adminService.getSmsTemplateByCompIdNdCatId(tempId, catId);
		String url=null;
		String message=null;
		message=smstemp.getTemp_Text();
		String message1=message.replace("<name>", name);
		System.out.println(message1);
		List SMSApilist = adminService.GetAPI(Integer.parseInt(""+compId));
		 String url1=null;
			System.out.println(SMSApilist.size());
			for (int i = 0; i < SMSApilist.size(); i++) {

				JSONObject leadmap = new JSONObject();
				Object[] result = (Object[]) SMSApilist.get(i);
				// uid=(int) result[0];
				System.out.println("hi  "+result[4]);
				boolean status = (boolean) result[4];
				if (status) {
					url=(String) result[1];
					 System.out.println("SMSURL1:"+result[1]);
					 url1=url.replace("<mobileno>", mobNo).replace("<message>", message1);
					 System.out.println(url1);
				}
				}


				
		if (url != null) {
			AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

			// final Tallycaller
			// tcaler1=adminService.GetTellyCallerById(Integer.parseInt(tallycallerId));
			final Sentsmslog log = new Sentsmslog();
				log.setTallycallerId(0);
				log.setStaffId(0);
				log.setCompayId(0);
			

			log.setMobileNo(Long.valueOf(mobNo));
			log.setMessage(message1);
			log.setSendDate(new Date());
				
			// System.out.println(log.getMobileNo()+"\n"+log.getMessage()+"\n"+log.getSendDate()+"\n"+log.getTallycallerId()+"\n"+log.getCompayId()+"\n"+log.getStaffId());
			 System.out.println("CallingURL:"+url1);
			asyncHttpClient.prepareGet(url1).execute(new AsyncCompletionHandler<Response>() {
				
				

				@Override
				public Response onCompleted(Response responce) throws Exception {
					// TODO Auto-generated method stub

					// System.out.println("Comp Id : "+tcaler.getCompanyId());

					User user = new User();
					user = adminService.getUserByUid(new Long(compId));

					List list = adminService.GetAPI(user.getCompanyId());
					String ststus = "";
					// System.out.println("List Size : "+list.size());
					int uid = 0;
					// System.out.println(list.size());
					for (int i = 0; i < list.size(); i++) {
						JSONObject leadmap = new JSONObject();
						Object[] result = (Object[]) list.get(i);
						uid = (int) result[0];
						boolean status = (boolean) result[4];
						if (status == true) {
							ststus = result[2].toString();
							// System.out.println("URL ID : "+uid);
						}
					}

					// System.out.println("Status : "+ststus);
					String res = responce.getResponseBody().replaceAll(" ", "");
					if (res.contains(ststus)) {
						System.out.println("NoErrorinMSG");
						log.setMSGStatus("SUCCESS");
					} else {
						System.out.println("ErrorinMSG");

						log.setMSGStatus("FAIL");
					}

					// if(res.length()>255)
					// {
					// log.setAPIResponse(res.toString().substring(0, 254));
					// }
					// else
					// {
					// log.setAPIResponse(res.toString());
					// }

					try {
						log.setAPIResponse(res);
						System.out.println(log.getAPIResponse() + "\n" + log.getMSGStatus());

						adminService.saveLog(log);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("SaveError:" + e);
					}
					System.out.println(responce.getResponseBody());
					return responce;
				}

				@Override
				public void onThrowable(Throwable t) {
					// Something wrong happened.
					t.printStackTrace();
					System.out.println("======="+t.getMessage());
					// System.out.println(statusCode+"::"+new Date());
					log.setAPIResponse(t.getMessage().substring(0, 100).trim());
					log.setMSGStatus("FAIL");
					adminService.saveLog(log);
				}

			});
		}
		else
		{
			//return "SMS Url Not Generated.. ";
		}
	}

}

package com.bonrix.sms.utils;


import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.util.ResourceBundle;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class EncryptDecrypt {

	private static String algorithm = "DES";
    private static SecretKey key = null;
    private static Cipher cipher = null;
    private  String DEFAULT_KEY ="";
  private static ResourceBundle resourceBundle = ResourceBundle.getBundle("application");
    private static String lickey = resourceBundle.getString("lickey");
 //   private static String DEFAULT_KEY ="46.4.9.238";// resourceBundle.getString(""); 

    // private static String DEFAULT_KEY = "223.30.60.194"; 
    public EncryptDecrypt() throws Exception
    { 

    	try {
            InetAddress ipAddr = InetAddress.getLocalHost();
            DEFAULT_KEY=ipAddr.getHostAddress();
            System.out.println(ipAddr.getHostAddress());
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
        DESKeySpec keySpec = new DESKeySpec(DEFAULT_KEY.getBytes("UTF8"));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(algorithm);
        key = keyFactory.generateSecret(keySpec);        
        cipher = Cipher.getInstance(algorithm);
    }


    public static void main(String[] args) throws Exception {
    	try {
            InetAddress ipAddr = InetAddress.getLocalHost();
            System.out.println(ipAddr.getHostAddress());
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    	
    	// dba701097ffa5bae5ba6b193388d170ec7047168e8098af1
    	
    	//dfd46e472ba4f5ec
    	// validity Date:: 2025-12-12 00:00:00
      String accpt= new EncryptDecrypt().encryptString("10");
     // String accpt= new EncryptDecrypt().decryptString("adb2820e9cccf002");
       // System.out.println("encr::::"+   new EncryptDecrypt().getmaxtelecaller());

        System.out.println("25 : "+accpt);
        String accpt2= new EncryptDecrypt().decryptString("0afd6ba6859bbe41");
        // String accpt= new EncryptDecrypt().decryptString("adb2820e9cccf002");
       //    System.out.println("String::::"+accpt2);
           
           
    }

    public int getmaxtelecaller(){
    	
    	try {
		  return Integer.parseInt(decryptString(lickey));
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
			return 0;
		}
    	
    }
    public String encryptString(String input)

            throws
            InvalidKeyException,
            BadPaddingException,
            IllegalBlockSizeException{

        String encryptString = null;
        byte[] encryptionBytes = null;
        encryptionBytes = encrypt(input);

        encryptString = "";

        for(int i=0;i<encryptionBytes.length;i++){

             encryptString += Integer.toString((encryptionBytes[i] & 0xff) + 0x100, 16).substring(1);
        }

        return encryptString;

    }

    public String decryptString(String input) 
            throws
            IllegalBlockSizeException,
            InvalidKeyException,
            BadPaddingException
    {

        String decryptString = null;
        byte[] decryptionBytes = hexStringToByteArray(input);
        decryptString = decrypt(decryptionBytes);
        return decryptString;

    }


    private static byte[] encrypt(String input) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] inputBytes = input.getBytes();
        return cipher.doFinal(inputBytes);

    }

    private static String decrypt(byte[] encryptionBytes) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] recoveredBytes = cipher.doFinal(encryptionBytes);
            String recovered = new String(recoveredBytes);
            return recovered;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

}

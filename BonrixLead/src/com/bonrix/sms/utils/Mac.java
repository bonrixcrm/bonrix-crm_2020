package com.bonrix.sms.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

public class Mac {
	public static String[] getmac() {
		String[] str = new String[2];
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			//str[0] = ip.getHostAddress();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			str[0] = sb.toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return str;
	}

	public static boolean checkmac(String key) {
		String[] mc = getmac();
		System.out.println(mc[0]+" "+mc[1]);
		String en = Hashing.sha1().hashString("B0" + mc[0] + "nr" + "ix", Charsets.UTF_8).toString();

		System.out.println("BSP::" + en + "==" + key);
		return en.equals(key);
	}
	
	 public static void main(String[] str){	  
		  checkmac("3C-97-0E-97-32-D4");
		  
		  }
}

package com.bonrix.sms.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.bonrix.sms.model.SenderName;
import com.bonrix.sms.model.SmsApi;
import com.bonrix.sms.model.SmsHistory;
import com.bonrix.sms.model.SmsQueue;
import com.bonrix.sms.service.SMSService;

@Controller
public class SendSms {

	String httpResponse = "";

	@Autowired
	private SMSService smsService;

	private static final Logger log = LoggerFactory.getLogger(SendSms.class);

	public SendSms() {
	}

	public int SendSingleSMSV3(SmsQueue smsq, List<SmsApi> smsapilst, SenderName sn) {
		int value = 0;
		try {

			log.info("SENDERID:" + smsq.toString() + "::" + smsq.getSenderid());
			System.out.println(smsq.toString() + "::::::::::::::" + smsq.getSenderid());

			String msg = smsq.getMessage();
			String To = smsq.getNumber();

			String[] tonos = smsq.getNumber().split(",");
			String finalmobNo = "";
			SmsApi smsapi = smsapilst.get(0);

			int maxMno = 1;
			for (int i = 0; i < tonos.length; i++) {
				if (To.length() == 12) {
					finalmobNo += tonos[i];
				} else {
					finalmobNo += "91" + tonos[i];
				}
				if (i != tonos.length - 1) {
					finalmobNo += ",";
				}
			}

			msg = URLEncoder.encode(msg, "UTF-8");
			String username = smsapi.getApiText();

			username = username.replaceAll("\\<mobile_number\\>", finalmobNo.trim());
			username = username.replaceAll("\\<sender_name\\>", sn.getSenderName());
			// username = username.replaceAll("\\<template\\>", templateid);
			username = username.replaceAll("\\<message\\>", msg.trim());
			// log.info("COMPLETE URL" + username);

			log.info("COMPLATE URL:" + username);

			int n = 0;
			String smsURL = "", token = "";
			URL oracle = new URL(username);
			httpResponse = "";
			URLConnection yc = oracle.openConnection();
			yc.setReadTimeout(50000);
			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				httpResponse += inputLine;
			}
			value = 1;

			// log.error("Conformation From SMS Provider(Driver) :" +
			// httpResponse+"::::::"+new Date());
		} catch (Exception ex) {
			ex.printStackTrace();

			httpResponse = "ERROR";
			value = 0;
		}

		SmsHistory sh = new SmsHistory();

		sh.setApiId(1);
		sh.setClientIp("");
		sh.setMessage(smsq.getMessage());
		sh.setMobileNumber(smsq.getNumber());
		sh.setUserId(smsq.getUid());
		sh.setStid(smsq.getStid());
		sh.setSentDateTime(new Date());
		sh.setSubmitDateTime(smsq.getCreatDate());
		sh.setBunchId(0);
		sh.setDlrStatus("SUBMITED");
		sh.setTrxId(smsq.getQid());
		sh.setStatus("submited");
		sh.setResult(httpResponse);

		System.out.println("OUT:::" + sh.toString());
		log.info(sh.getSubmitDateTime().toString());
		smsService.saveSmsHistory(sh);

		return value;
	}

}

package com.bonrix.sms.utils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;

public class TarGZIPDemo {

 public static void main(String[] args) {
 // String SOURCE_FOLDER = "C:\\Users\\Sudhir\\Desktop\\NewCopyRecs\\Rec_17-03-2020";
  String SOURCE_FOLDER = "E:\\sudhir\\Office_Proj_BackUp\\CRM_22_11_2019\\BonrixLead\\WebContent\\Sound\\79";
  TarGZIPDemo tGzipDemo = new TarGZIPDemo();
  tGzipDemo.createTarFile(SOURCE_FOLDER);
 }
 
 private void createTarFile(String sourceDir){
  TarArchiveOutputStream tarOs = null;
  try {
   File source = new File(sourceDir);
   // Using input name to create output name
   System.out.println("Absolute_Path----"+source.getAbsolutePath().concat(".tar.gz"));
   FileOutputStream fos = new FileOutputStream(source.getAbsolutePath().concat(".tar.gz"));
      GZIPOutputStream gos = new GZIPOutputStream(new BufferedOutputStream(fos));
      tarOs = new TarArchiveOutputStream(gos);
      addFilesToTarGZ(sourceDir, "", tarOs);     
  } catch (IOException e) {
      e.printStackTrace();
  }finally{
   try {
       tarOs.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
   }
 }
 
 public void addFilesToTarGZ(String filePath, String parent, TarArchiveOutputStream tarArchive) throws IOException {
  File file = new File(filePath);
  // Create entry name relative to parent file path 
  String entryName = parent + file.getName();
  // add tar ArchiveEntry
  tarArchive.putArchiveEntry(new TarArchiveEntry(file, entryName));
  if(file.isFile()){
   FileInputStream fis = new FileInputStream(file);
   BufferedInputStream bis = new BufferedInputStream(fis);
   // Write file content to archive
   IOUtils.copy(bis, tarArchive);
   tarArchive.closeArchiveEntry();
   bis.close();
  }else if(file.isDirectory()){
   // no need to copy any content since it is
   // a directory, just close the outputstream
   tarArchive.closeArchiveEntry();
   // for files in the directories
   for(File f : file.listFiles()){        
    // recursively call the method for all the subdirectories
    addFilesToTarGZ(f.getAbsolutePath(), entryName+File.separator, tarArchive);
   }
  }          
 }
 
 
}
package com.bonrix.sms.utils;

import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

public class TestSynchronization {
	public static void main(String args[]){  
		/*final Test obj = new Test();//only one object  
		  
		Thread t1=new Thread(){  
		public void run(){  
		obj.printTable(5);  
		}  
		};  
		Thread t2=new Thread(){  
		public void run(){  
		obj.printTable(100);  
		}  
		};  
		  
		t1.start();  
		t2.start();  */
		String raw = "\\xF0\\x9F\\x8E\\x99\\xEF\\xB8"; // The raw string as you have it
		String processed = raw.replace("\\x", ""); // Remove the escape characters to parse the hex string correctly
		byte[] bytes = DatatypeConverter.parseHexBinary(processed); // Convert the hex string to a byte array

		// Convert the byte array to a String
		String emojiString = new String(bytes, StandardCharsets.UTF_8);
	    System.out.println(emojiString);
		}  
	
}

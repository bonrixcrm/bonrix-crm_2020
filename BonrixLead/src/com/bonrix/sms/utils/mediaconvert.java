package com.bonrix.sms.utils;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.VideoAttributes;
import it.sauronsoftware.jave.VideoSize;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

public class mediaconvert {

	public File convertvideofile(File source) {

		String ext = getExtension(source.getName()).toLowerCase();

		System.out.println(":::::::::::::::::::::::::::::::::::::::");
		System.out.println("GET FILE NAME:::::::::::" + ext);
		System.out.println(":::::::::::::::::::::::::::::::::::::::");
		if (ext.equalsIgnoreCase("mp3") || ext.equalsIgnoreCase("amr") || ext.equalsIgnoreCase("waw")) {

			return source;
		}

		if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("jpeg") || ext.equalsIgnoreCase("png")
				|| ext.equalsIgnoreCase("gif")) {

			return convertImageFile(source);
		}

		long start = new Date().getTime();
		// File source = new File(filepath);

		File target = new File(source.getAbsolutePath() + new Date().getTime() + ".mp4");
		AudioAttributes audio = new AudioAttributes();

		audio.setCodec("libfaac");
		audio.setBitRate(new Integer(128000));
		audio.setSamplingRate(new Integer(44100));
		audio.setChannels(new Integer(2));
		VideoAttributes video = new VideoAttributes();
		video.setCodec("mpeg4");
		video.setBitRate(new Integer(160000));
		video.setFrameRate(new Integer(15));
		video.setSize(new VideoSize(400, 300));
		EncodingAttributes attrs = new EncodingAttributes();
		attrs.setFormat("mp4");
		attrs.setAudioAttributes(audio);
		attrs.setVideoAttributes(video);
		Encoder encoder = new Encoder();
		try {
			encoder.encode(source, target, attrs);
			long end = new Date().getTime();
			System.out.print("Total Time taken:" + (end - start) + " milli seconds");
			return target;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static File convertImageFile(File source) {

		long start = new Date().getTime();
		// File source = new File(filepath);

		// File target = new File(source.getAbsolutePath()+new
		// Date().getTime()+".mp4");
		// File input = new File("digital_image_processing.jpg");
		try {
			BufferedImage image = ImageIO.read(source);

			File compressedImageFile = new File(source.getAbsolutePath() + new Date().getTime() + ".jpg");
			OutputStream os = new FileOutputStream(compressedImageFile);

			Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
			ImageWriter writer = (ImageWriter) writers.next();

			ImageOutputStream ios = ImageIO.createImageOutputStream(os);
			writer.setOutput(ios);

			ImageWriteParam param = writer.getDefaultWriteParam();
			param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			param.setCompressionQuality(0.50f);
			writer.write(null, new IIOImage(image, null, null), param);
			os.close();
			ios.close();
			writer.dispose();

			// encoder.encode(source, target, attrs);
			long end = new Date().getTime();
			System.out.print("Total Time taken:" + (end - start) + " milli seconds");
			return compressedImageFile;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static String getExtension(final String filename) {
		if (filename == null)
			return null;
		final String afterLastSlash = filename.substring(filename.lastIndexOf('/') + 1);
		final int afterLastBackslash = afterLastSlash.lastIndexOf('\\') + 1;
		final int dotIndex = afterLastSlash.indexOf('.', afterLastBackslash);
		return (dotIndex == -1) ? "" : afterLastSlash.substring(dotIndex + 1);
	}

}
